--liquibase formatted sql

--changeset postgres:add_data_to_instance_and_domain_instance_tables context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-135 Add data to CS instance table



INSERT INTO core."instance"
(id, "name", "server", port, creation_timestamp, creator_id, is_void, tenant_id, notes)
VALUES
(1, 'CIMMYT-CB-MAIZE', '172.18.8.1', '22', now(), 1, false, 1, 'Tenant 1 - CB Maize'),
(2, 'CIMMYT-CB-WHEAT', '172.18.8.1', '22', now(), 1, false, 1, 'Tenant 1 - CB Wheat'),
(3, 'IRRI-CB-RICE', '172.18.8.1', '22', now(), 1, false, 2, 'Tenant 2 - CB Rice');
;

INSERT INTO "security".domain_instance
(instance_id, domain_id, context, creation_timestamp, creator_id, is_ssl, is_void, tenant_id)
VALUES
(1, 1, 'https://ebs-staging-maize.cimmyt.org/index.php', now(), 1, false, false, 1),
(2, 1, 'https://ebs-staging-wheat.cimmyt.org/index.php', now(), 1, false, false, 1),
(3, 1, 'https://ebs-staging-wheat.cimmyt.org/index.php', now(), 1, false, false, 2);

SELECT setval('core.instance_id_seq', (SELECT MAX(id) FROM core."instance"));
SELECT setval('"security".domain_instance_id_seq', (SELECT MAX(id) FROM "security".domain_instance));


--Revert Changes
--rollback TRUNCATE core."instance" CASCADE;
--rollback TRUNCATE "security".domain_instance;