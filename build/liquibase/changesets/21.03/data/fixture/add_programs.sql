--liquibase formatted sql

--changeset postgres:add_programs context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-168 Add programs



INSERT INTO "program"."program" (id, code,"name","type",status,description,notes,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,tenant_id,crop_id,owner_id) VALUES 
(4, 'AP','Admin Program','Admin Program','Avtive','Administrative Program','Admin Program','2021-03-30 14:03:10.040',NULL,1,NULL,false,1,1,1)
;

UPDATE "program"."program"
SET code='GWP', "name"='Global Wheat Program', "type"='Breeding Program', status='Active', description='Global Wheat Program', notes='', creation_timestamp=now(), modification_timestamp=null, creator_id=1, modifier_id=null, is_void=false, tenant_id=1, crop_id=2, owner_id=1
WHERE id=1;

INSERT INTO "program"."program" 
(id, code,"name","type",status,description,notes,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,tenant_id, crop_id, owner_id) VALUES 
(2, 'GMP','Global Maize Program','Breeding Program','Active','Global Maize Program',NULL,'2021-02-11 18:14:29.004',NULL,1,NULL,false,1,1,1),
(3, 'DW','Durum Wheat','Breeding Program','Active','Durum Wheat',NULL,'2021-02-11 18:14:29.004',NULL,1,NULL,false,1,2,1)
;

UPDATE "security".functional_unit
SET program_id=4
WHERE id=1;


SELECT setval('"program".program_id_seq', (SELECT MAX(id) FROM "program"."program"));


--Revert Changes
--rollback UPDATE "program"."program" SET code='AP', "name"='Admin Program', "type"='Admin Program', status='Active', description='Administrative Program', notes='', creation_timestamp=now(), modification_timestamp=null, creator_id=1, modifier_id=null, is_void=false WHERE id=1;
--rollback UPDATE "security".functional_unit SET program_id=1 WHERE id=1;

--rollback delete from "program"."program" where id = 2;
--rollback delete from "program"."program" where id = 3;
--rollback delete from "program"."program" where id = 4;

