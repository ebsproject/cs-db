--liquibase formatted sql

--changeset postgres:add_user_person_and_role_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-120 Create Sample data for different tables in security Module


--
-- Inserting data into table crm.person_status
--
INSERT INTO crm.person_status
(id, "name", description, tenant_id, creation_timestamp, creator_id, is_void)
VALUES
(1, 'Active', 'Active person', 1, now(), 1, false);

--
-- Inserting data into table crm.person
--
INSERT INTO crm.person
(id, given_name, family_name, additional_name, email, official_email, gender, has_credential, job_title, knows_about, phone, person_status_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, tenant_id, language_id)
VALUES
(1, 'Ernesto', 'Briones', null, 'e.briones@gmail.com', 'e.briones@cimmyt.org', 'male', true, 'IT Team Lead', 'IT', '5959572906', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(2, 'Marko', 'Karkkainen', null, 'm.karkkainen@irri.org', 'm.karkkainen@irri.org', 'male', true, 'IT Team Lead', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(3, 'Jahzeel', 'Ramos', null, 'jp.ramos@irri.org', 'jp.ramos@irri.org', 'female', true, 'Software Project Manager', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(4, 'Karen', 'Delarosa', null, 'k.delarosa@irri.org', 'k.delarosa@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(5, 'Joanie', 'Antonio', null, 'j.antonio@irri.org', 'j.antonio@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(6, 'Larise', 'Gallardo', null, 'l.gallardo@irri.org', 'l.gallardo@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(7, 'Erica', 'Banasihan', null, 'e.banasihan@irri.org', 'e.banasihan@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(8, 'Maria Johsah', 'Pasang', null, 'm.pasang@irri.org', 'm.pasang@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(9, 'Aldi', 'Crisostomo', null, 'a.crisostomo@irri.org', 'a.crisostomo@irri.org', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(10, 'Alex', 'Caneda', null, 'a.caneda@irri.org', 'a.caneda@irri.org', 'male', true, 'Software Project Manager', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(11, 'Yanii', 'Calaminos', null, 'v.calaminos@irri.org', 'v.calaminos@irri.org', 'female', true, 'Software Project Manager', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(12, 'Nikki', 'Carumba', null, 'n.carumba@irri.org', 'n.carumba@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(13, 'Migui', 'Castillo', null, 'j.f.castillo@irri.org', 'j.f.castillo@irri.org', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(14, 'Jaymar', 'Bantay', null, 'j.bantay@irri.org', 'j.bantay@irri.org', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(15, 'Jack', 'Lagare', null, 'j.lagare@irri.org', 'j.lagare@irri.org', 'male', true, 'DevOps Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(16, 'Ken', 'Ana', null, 'k.ana@irri.org', 'k.ana@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(17, 'Eugene', 'Tenorio', null, 'e.tenorio@irri.org', 'e.tenorio@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(18, 'Renee', 'Lat', null, 'r.lat@irri.org', 'r.lat@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(19, 'Argem', 'Flores', null, 'a.flores@irri.org', 'a.flores@irri.org', 'male', true, 'Software Project Manager', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(20, 'Ruth', 'Carpio', null, 'r.e.carpio@irri.org', 'r.e.carpio@irri.org', 'female', true, 'IT Project Coordinator', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(21, 'Gene', 'Romuga', null, 'g.romuga@irri.org', 'g.romuga@irri.org', 'male', true, 'Database Engineering', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(22, 'Doris', 'Pagtananan', null, 'd.pagtananan@irri.org', 'd.pagtananan@irri.org', 'female', true, 'Software Project Manager', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(23, 'PJ', 'Sinohin', null, 'p.sinohin@irri.org', 'p.sinohin@irri.org', 'male', true, 'Software Tester', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(24, 'Lois', 'Go', null, 'l.b.go@irri.org', 'l.b.go@irri.org', 'male', true, 'Software Tester', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(25, 'May', 'Sallan', null, 'm.sallan@irri.org', 'm.sallan@irri.org', 'female', true, 'Research Requirements Analyst', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(26, 'Connie', 'Lotho', null, 'm.lotho@irri.org', 'm.lotho@irri.org', 'female', true, 'Research Requirements Analyst', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(27, 'Tom', 'Hagen', null, 't.hagen@cimmyt.org', 't.hagen@cimmyt.org', 'male', true, 'Project Leader', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(28, 'Diego', 'Castanon', null, 'd.castanon@cimmyt.org', 'd.castanon@cimmyt.org', 'male', true, 'IT Project Coordinator', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(29, 'Richi', 'Leon Trevino', null, 'l.leon@cimmyt.org', 'l.leon@cimmyt.org', 'male', true, 'Research Requirements Analyst', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(30, 'Pedro', 'Medeiros Barbosa', null, 'p.medeiros@cimmyt.org', 'p.medeiros@cimmyt.org', 'male', true, 'Research Requirements Analyst', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(31, 'Rosemary', 'Shrestha', null, 'r.shrestha2@cimmyt.org', 'r.shrestha2@cimmyt.org', 'female', true, 'Research Requirements Analyst', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(32, 'Kate', 'Dreher', null, 'k.dreher@cimmyt.org', 'k.dreher@cimmyt.org', 'female', true, 'Research Requirements Analyst', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(33, 'Cesar', 'Petroli', null, 'c.petroli@cimmyt.org', 'c.petroli@cimmyt.org', 'male', true, 'Research Requirements Analyst', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(34, 'Luis', 'Puebla Luna', null, 'l.puebla@cimmyt.org', 'l.puebla@cimmyt.org', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(35, 'Salvador', 'Ortega Lucas', null, 's.ortega@cimmyt.org', 's.ortega@cimmyt.org', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(36, 'Juan', 'Suarez Sosa', null, 'j.s.sosa@cimmyt.org', 'j.s.sosa@cimmyt.org', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(37, 'Gerardo', 'Copado Ruiz', null, 'g.copado@cimmyt.org', 'g.copado@cimmyt.org', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(38, 'Victor', 'Ulat', null, 'v.ulat@cimmyt.org', 'v.ulat@cimmyt.org', 'male', true, 'Software Project Manager', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(39, 'Newman', 'Montes Samayoa', null, 'n.montes@cimmyt.org', 'n.montes@cimmyt.org', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(40, 'Jorge', 'Rojas Olivares', null, 'j.a.rojas@cimmyt.org', 'j.a.rojas@cimmyt.org', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(41, 'Erika', 'Cisneros Munoz', null, 'e.cisneros@cimmyt.org', 'e.cisneros@cimmyt.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(42, 'Frangel', 'Consolacion', null, 'f.consolacion@cimmyt.org', 'f.consolacion@cimmyt.org', 'male', true, 'DevOps Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(43, 'Francisco', 'Vergara Espinoza', null, 'j.f.vergara@cimmyt.org', 'j.f.vergara@cimmyt.org', 'male', true, 'Software Tester', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(44, 'Sam', 'Storr', null, 's.storr@cimmyt.org', 's.storr@cimmyt.org', 'male', true, 'UI/UX Expert', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(45, 'Star', 'Yanxin Gao', null, 'yg28@cornell.edu', 'yg28@cornell.edu', 'female', true, 'Research Requirements Analyst', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(46, 'Kevin', 'Palis', null, 'kdp44@cornell.edu', 'kdp44@cornell.edu', 'male', true, 'IT Technical Architect', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(47, 'Angel', 'Villanueva Raquel', null, 'avr33@cornell.edu', 'avr33@cornell.edu', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(48, 'Josh', 'Lamos-Sweeney', null, 'jdl232@cornell.edu', 'jdl232@cornell.edu', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(49, 'Vishnu', 'Govindaraj', null, 'vg249@cornell.edu', 'vg249@cornell.edu', 'male', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(50, 'Roy', 'Lawrence Petrie', null, 'rlp243@cornell.edu', 'rlp243@cornell.edu', 'male', true, 'DevOps Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(51, 'Yaw', 'Nti-Addae', null, 'yn259@cornell.edu', 'yn259@cornell.edu', 'male', true, 'IT Team Lead', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(52, 'Terry', 'Molnar', null, 't.molnar@cimmyt.org', 't.molnar@cimmyt.org', 'male', true, 'CIMMYT GRP Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(53, 'Thanda', 'Dhliwayo', null, 'd.thanda@cimmyt.org', 'd.thanda@cimmyt.org', 'male', true, 'CIMMYT GMP Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(54, 'Bhoja', 'Basnet', null, 'b.r.basnet@cimmyt.org', 'b.r.basnet@cimmyt.org', 'male', true, 'CIMMYT GWP Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(55, 'Damien', 'Platten', null, 'j.platten@irri.org', 'j.platten@irri.org', 'male', true, 'IRRI Rice Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(56, 'Kelly', 'Robbins', null, 'krr73@cornell.edu', 'krr73@cornell.edu', 'female', true, 'Module 5 Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(57, 'Mike', 'Olsen', null, 'm.olsen@cimmyt.org', 'm.olsen@cimmyt.org', 'male', true, 'Module 3 Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(58, 'Eduardo', 'Covarrubias', null, 'g.covarrubias@cimmyt.org', 'g.covarrubias@cimmyt.org', 'male', true, 'Module 2 Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(59, 'Eng', 'Hwa', null, 'n.enghwa@cimmyt.org', 'n.enghwa@cimmyt.org', 'male', true, 'Module 3 Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(60, 'Liz', 'Jones', null, 'ej245@cornell.edu', 'ej245@cornell.edu', 'female', true, 'Module 5 Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(61, 'Gustavo', 'Teixeira', null, 'g.teixeira@cimmyt.org', 'g.teixeira@cimmyt.org', 'male', true, 'Module 4 Representative', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(62, 'Aleine', 'Gulles', null, 'a.gulles@irri.org', 'a.gulles@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(63, 'Rose', 'Morantte', null, 'r.morantte@irri.org', 'r.morantte@irri.org', 'female', true, 'Software Engineer', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(64, 'Fernando', 'Toledo', null, 'F.TOLEDO@cimmyt.org', 'F.TOLEDO@cimmyt.org', 'male', true, '', 'IT', '5959521900', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1, 1),
(65, 'Dominic', 'Lapitan', null, 'domlapitan@gmail.com', 'domlapitan@gmail.com', 'male', true, 'Software Developer', 'IT', null, 1, '2021-02-11 18:14:50.4991120', null, 1, null, false, 1, 1);

--
-- Inserting data into table "security"."role"
--
INSERT INTO "security"."role"
(id, name, description, security_group, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void) 
VALUES
(1, 'Default', 'default', 'default', 1, '2021-02-11 18:13:58.1262470', null, 1, null, false),
(2, 'Admin', 'admin', 'EBS_Administratoris', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false),
(3, 'Guest', 'guest', 'Other', 1, '2021-02-11 18:14:42.9135190', null, 1, null, false),
(4, 'Power User', 'poweruser', 'EBS', 1, '2021-02-11 18:14:42.9135190', null, 1, null, false),
(5, 'User', 'user', 'Other', 1, '2021-02-11 18:14:42.9135190', null, 1, null, false);


--
-- Inserting data into table program.crop
--
INSERT INTO program.crop(id, code, name, description, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, notes) VALUES
(1, 'M', 'Maize', 'Maize', '2021-02-11 18:14:12.4781440', null, 1, null, false, null),
(2, 'W', 'Wheat', 'Wheat', '2021-02-11 18:14:12.4781440', null, 1, null, false, null),
(3, 'R', 'Rice', 'Rice', '2021-02-11 18:14:12.4781440', null, 1, null, false, null);


INSERT INTO "program"."program"
(code, "name", "type", status, description, notes, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, tenant_id, crop_id)
VALUES('AP', 'Admin Program', 'Admin Program', 'Avtive', 'Administrative Program', 'Admin Program', now(), null, 1, null, false, 1, 1);

--
-- Inserting data into table "security".functional_unit
--
INSERT INTO "security".functional_unit
(id, code, "name", description, notes, "type", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES
(1, 'ADM', 'Administrator', 'Administrator', 'Administrator Team', 'Team', 1, '2021-02-11 18:14:36.6167940', null, 1, null, false);

UPDATE "security".functional_unit
SET parent_func_unit_id=null
WHERE id=1;

--
-- Inserting data into table "security"."user"
--
INSERT INTO "security"."user"
(id, user_name, user_password, last_access, default_role, is_i_s, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, person_id)
VALUES
(1, 'admin', 'NA', null, 1, 1, '2021-02-11 18:13:58.1262470', '2021-02-11 18:13:58.1262470', 1, 1, false,1),
(2, 'e.briones@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 1),
(3, 'm.karkkainen@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 2),
(4, 'jp.ramos@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 3),
(5, 'k.delarosa@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 4),
(6, 'j.antonio@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 5),
(7, 'l.gallardo@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 6),
(8, 'e.banasihan@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 7),
(9, 'm.pasang@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 8),
(10, 'a.crisostomo@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 9),
(11, 'a.caneda@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 10),
(12, 'v.calaminos@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 11),
(13, 'n.carumba@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 12),
(14, 'j.f.castillo@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 13),
(15, 'j.bantay@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 14),
(16, 'j.lagare@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 15),
(17, 'k.ana@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 16),
(18, 'e.tenorio@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 17),
(19, 'r.lat@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 18),
(20, 'a.flores@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 19),
(21, 'r.e.carpio@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 20),
(22, 'g.romuga@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 21),
(23, 'd.pagtananan@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 22),
(24, 'p.sinohin@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 23),
(25, 'l.b.go@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 24),
(26, 'm.sallan@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 25),
(27, 'm.lotho@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 26),
(28, 't.hagen@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 27),
(29, 'd.castanon@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 28),
(30, 'l.leon@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 29),
(31, 'p.medeiros@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 30),
(32, 'r.shrestha2@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 31),
(33, 'k.dreher@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 32),
(34, 'c.petroli@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 33),
(35, 'l.puebla@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 34),
(36, 's.ortega@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 35),
(37, 'j.s.sosa@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 36),
(38, 'g.copado@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 37),
(39, 'v.ulat@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 38),
(40, 'n.montes@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 39),
(41, 'j.a.rojas@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 40),
(42, 'e.cisneros@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 41),
(43, 'f.consolacion@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 42),
(44, 'j.f.vergara@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 43),
(45, 's.storr@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 44),
(46, 'yg28@cornell.edu', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 45),
(47, 'kdp44@cornell.edu', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 46),
(48, 'avr33@cornell.edu', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 47),
(49, 'jdl232@cornell.edu', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 48),
(50, 'vg249@cornell.edu', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 49),
(51, 'rlp243@cornell.edu', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 50),
(52, 'yn259@cornell.edu', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 51),
(53, 't.molnar@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 52),
(54, 'd.thanda@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 53),
(55, 'b.r.basnet@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 54),
(56, 'j.platten@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 55),
(57, 'krr73@cornell.edu', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 56),
(58, 'm.olsen@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 57),
(59, 'g.covarrubias@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 58),
(60, 'n.enghwa@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 59),
(61, 'ej245@cornell.edu', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 60),
(62, 'g.teixeira@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 61),
(63, 'a.gulles@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 62),
(64, 'r.morantte@irri.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 63),
(65, 'F.TOLEDO@cimmyt.org', 'NA', null, 2, 1, '2021-02-11 18:14:36.6167940', null, 1, null, false, 64),
(66, 'domlapitan@gmail.com', 'NA', null, 2, 1, '2021-02-11 18:14:50.4991120', null, 1, null, false, 65);

--
-- Inserting data into table "security".functional_unit_user
--
INSERT INTO "security".functional_unit_user(user_id, functional_unit_id) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1);

--
-- Inserting data into table "security".user_role
--
INSERT INTO "security".user_role
(role_id, user_id)
VALUES
(1, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(2, 42),
(2, 43),
(2, 44),
(2, 45),
(2, 46),
(2, 47),
(2, 48),
(2, 49),
(2, 50),
(2, 51),
(2, 52),
(2, 53),
(2, 54),
(2, 55),
(2, 56),
(2, 57),
(2, 58),
(2, 59),
(2, 60),
(2, 61),
(2, 62),
(2, 63),
(2, 64),
(2, 65),
(2, 66);

SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));
SELECT setval('"security".functional_unit_id_seq', (SELECT MAX(id) FROM "security".functional_unit));
SELECT setval('"security".role_id_seq', (SELECT MAX(id) FROM "security"."role"));
SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));
SELECT setval('crm.person_status_id_seq', (SELECT MAX(id) FROM crm.person_status));

--Revert Changes
--rollback Truncate "security".user_role CASCADE;
--rollback Truncate "security".functional_unit_user CASCADE;
--rollback Truncate "security"."user" CASCADE;
--rollback Truncate "security".functional_unit CASCADE;
--rollback Truncate "security"."role" CASCADE;
--rollback Truncate crm.person CASCADE;
--rollback Truncate crm.person_status CASCADE;
--rollback TRUNCATE program.crop CASCADE;
--rollback TRUNCATE "program"."program" CASCADE;