--liquibase formatted sql

--changeset postgres:set_values_to_functional_unit_program_id context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-150 Fix model authorization



UPDATE "security".functional_unit
SET program_id=1
WHERE id=1;

UPDATE "program"."program"
SET owner_id=1
WHERE id=1;

--Revert Chabges
--rollback UPDATE "security".functional_unit SET program_id=null WHERE id=1;

--rollback UPDATE "program"."program" SET owner_id=null WHERE id=1;

