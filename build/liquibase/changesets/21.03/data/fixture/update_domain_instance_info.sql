--liquibase formatted sql

--changeset postgres:update_domain_instance_info context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-168 Update domain_instance info



INSERT INTO core.domain_instance (id, instance_id,domain_id,context,creation_timestamp,creator_id,is_ssl,is_void,modification_timestamp,modifier_id,tenant_id,sg_project) VALUES
	 (4, 2,4,'https://ebs-staging-wheat.cimmyt.org/index.php','2021-03-22 15:18:37.743',1,false,false,NULL,NULL,1,'http://localhost:8082/graphql'),
	 (5, 1,2,'https://ebs-staging-wheat.cimmyt.org/index.php','2021-03-22 15:18:37.743',1,false,false,NULL,NULL,1,'http://localhost:8080/graphql');


INSERT INTO core.domain_entity_reference (entityreference_id,domain_id) VALUES
	 (60,2), --program
	 (65,4); --purpose

SELECT setval('core.domain_instance_id_seq', (SELECT MAX(id) FROM core.domain_instance));

--Revert Changes
--rollback TRUNCATE core.domain_entity_reference;
--rollback DELETE from core.domain_instance WHERE ID = 4; 
--rollback DELETE from core.domain_instance WHERE ID = 5;