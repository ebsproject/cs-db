--liquibase formatted sql

--changeset postgres:add_domain_products_organization, customer and tenant context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-133 Provide catalog items for product, product_function and domain tables
--validCheckSum: 8:1191666f311771efed5f14f1a14021c0


-- ALTER TABLE core.organization disable trigger all;
-- ALTER TABLE core.customer disable trigger all;
SET session_replication_role = 'replica';

INSERT INTO core.html_tag
(id, tag_name, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES
(1, 'ent.Component.att.name', '2021-02-11 18:14:40.7512610', '2021-02-11 18:14:40.7512610', 1, 1, false);

INSERT INTO core."domain"
(id, "name", info, icon, creation_timestamp, creator_id, is_void, htmltag_id)
VALUES
(1, 'Core Breeding', 'Core Breeding System', 'CB', now(), 1, false, 1),
(2, 'Core System', 'Core System', 'CS', now(), 1, false, 1),
(3, 'Breeding Analytics', 'Breeding Analytics', 'BA', now(), 1, false, 1),
(4, 'Service Management', 'Service Management', 'SM', now(), 1, false, 1)
;

ALTER SEQUENCE core.product_id_seq restart with 1; 
--Core Breeding Products
INSERT INTO core.product
("name", description, help, main_entity, icon, creation_timestamp, creator_id, is_void, domain_id, htmltag_id)
VALUES
('Experiment Creation', 'The Experiment Creation tool enables end-users to create Breeding 
Trial experiments with various statistical designs and field layouts, Intentional Crossing 
Nursery experiments, Cross Parent Nursery experiments, and Generation Nursery experiments 
for advancing segregated populations, seed increase.', 'Experiment CReation', 'Experiment', 'NA', now(), 1, false, 1, 1),
('Experiment Manager', 'Experiment manager acts as an orchestrator and enables users to 
search, view, manage, monitor and suppress Occurrences and Locations. ', 'Experiment Manager', 'Experiment', 'NA', now(), 1, false, 1, 1),
('Harvest Manager', 'Harvest Manager', 'Harvest Manager', 'Harvest', 'NA', now(), 1, false, 1, 1),
('Planting Instruction Manager', 'help users generate an accurate Planted Array: informing 
tools further down the workflow of a plot’s contents. ', 'Planting Instruction Manager', 'Planting Instructions', 'NA', now(), 1, false, 1, 1),
('Pollination Instructions', 'The Pollination Instructions tool will facilitate 
the main users (breeders) to specify exact female and male plots that are used in pollinations 
for making desirable crosses.', 'Pollination Instructions', 'Pollination', 'NA', now(), 1, false, 1, 1),
('Search Seeds', 'Search Seeds', 'Search Seeds', 'Seed', 'NA', now(), 1, false, 1, 1),
('Seed Inventory.', 'Seed Inventory', 'Seed Inventory', 'Inventory', 'NA', now(), 1, false, 1, 1),
('KDX data transfer', 'KDX data transfer', 'KDX data transfer', 'KDX', 'NA', now(), 1, false, 1, 1),

--Core SYstem Products
('User Management', 'User Management', 'User Management', 'User', 'NA', now(), 1, false, 2, 1),
('Tenant Management', 'Tenant Management', 'Tenant Management', 'Tenant', 'NA', now(), 1, false, 2, 1),
('CRM', 'Customer Relationship Management', 'Customer Relationship Management', 'Tenant', 'NA', now(), 1, false, 2, 1),

--Breeding Analytics Products
('Analytical Request Manager', 'Analytical Request Manager', 'Analytical Request Manager', 'Request', 'NA', now(), 1, false, 3, 1),
('Analytical Framework', 'Analytical Framework', 'Analytical Framework', 'AF', 'NA', now(), 1, false, 3, 1),

--Service Management Products
('Request Manager', 'Request Manager', 'Request Manager', 'Request', 'NA', now(), 1, false, 4, 1),
('GOBii', 'GOBii', 'GOBii', 'GOBiiAF', 'NA', now(), 1, false, 4, 1)
;

INSERT INTO core.organization
(id, legal_name, phone, web_page, slogan, "name", logo, is_active, default_authentication, default_theme, code, creation_timestamp, creator_id, is_void, customer_id)
VALUES
(1, 'Default', null, 'NA', 'Default', 'Default', 'default', true, 1, 1, 'default', now(), 1, false, 1),
(2, 'CGIAR', 12028625600, 'www.cgiar.org', 'Consultative Group for International Agricultural Research', 
'CGIAR', 'cgiar', true, 1, 1, 'cg', now(), 1, false, 1);

INSERT INTO core.customer
(id, "name", phone, official_email, job_title, language_preference, phone_extension, is_active, creation_timestamp, creator_id, is_void, organization_id)
VALUES
(1, 'Default', null, 'Default', 'Default', 'English', 'NA', true, now(), 1, false, 1),
(2, 'CIMMYT', 525558042004, 'e.briones@cgiar.org', 'IT Team Lead', 'English', '1327', true, now(), 1, false, 2),
(3, 'IRRI', 63285805600, 'info@irri.org', 'IRRI Contact', 'English', '1327', true, now(), 1, false, 2);

INSERT INTO core.tenant
(id, "name", expiration, expired, creation_timestamp, creator_id, modifier_id, is_void, customer_id, organization_id)
VALUES
(1, 'CGIAR-CIMMYT-1', '01-01-2050', false, now(), 1, 1, false, 2, 2),
(2, 'CGIAR-IRRI-2', '01-01-2050', false, now(), 1, 1, false, 3, 2);

-- ALTER TABLE core.organization enable trigger all;
-- ALTER TABLE core.customer enable trigger all;

SELECT setval('core.tenant_id_seq', (SELECT MAX(id) FROM core.tenant));
SELECT setval('core.customer_id_seq', (SELECT MAX(id) FROM core.customer));
SELECT setval('core.organization_id_seq', (SELECT MAX(id) FROM core.organization));
SELECT setval('core.product_id_seq', (SELECT MAX(id) FROM core.product));
SELECT setval('core.domain_id_seq', (SELECT MAX(id) FROM core.domain));
SELECT setval('core.html_tag_id_seq', (SELECT MAX(id) FROM core.html_tag));

SET session_replication_role = 'origin';



--Revert Changes
--rollback Truncate core.tenant  CASCADE;
--rollback Truncate core.customer  CASCADE; 
--rollback Truncate core.organization  CASCADE;
--rollback Truncate core.product  CASCADE;
--rollback Truncate core.domain  CASCADE;
--rollback Truncate core.html_tag  CASCADE;

