--liquibase formatted sql

--changeset postgres:add_menu_order_data_into_product context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-176 Add menu_order data into core.product



UPDATE core.product SET menu_order = 1  WHERE id=1;
UPDATE core.product SET menu_order = 2  WHERE id=2;
UPDATE core.product SET menu_order = 3  WHERE id=3;
UPDATE core.product SET menu_order = 4  WHERE id=4;
UPDATE core.product SET menu_order = 5  WHERE id=5;
UPDATE core.product SET menu_order = 6  WHERE id=6;
UPDATE core.product SET menu_order = 7  WHERE id=7;
UPDATE core.product SET menu_order = 8  WHERE id=8;
UPDATE core.product SET menu_order = 2  WHERE id=9;
UPDATE core.product SET menu_order = 1  WHERE id=10;
UPDATE core.product SET menu_order = 3  WHERE id=11;
UPDATE core.product SET menu_order = 1  WHERE id=12;
UPDATE core.product SET menu_order = 2  WHERE id=13;
UPDATE core.product SET menu_order = 1  WHERE id=14;
UPDATE core.product SET menu_order = 2  WHERE id=20;
UPDATE core.product SET menu_order = 4  WHERE id=21;
UPDATE core.product SET menu_order = 3  WHERE id=16;
UPDATE core.product SET menu_order = 4  WHERE id=17;
UPDATE core.product SET menu_order = 5  WHERE id=18;
UPDATE core.product SET menu_order = 6  WHERE id=19;

DELETE FROM core.product WHERE id =15;


--Revert Changes
--rollback UPDATE core.product SET menu_order = NULL;
--rollback INSERT INTO core.product (id, "name", description, help, main_entity, icon, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, domain_id, htmltag_id, menu_order, "path")
--rollback VALUES(15, 'GOBii', 'GOBii', 'GOBii', 'GOBiiAF', 'NA', '2021-04-06 12:57:59.804', NULL, 1, NULL, false, 4, 1, NULL, NULL);
