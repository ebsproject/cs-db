--liquibase formatted sql

--changeset postgres:add_missing_entity_reference context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-168 Fil out entity references and attributes


INSERT INTO core.entity_reference
(entity, textfield, valuefield, storefield, entity_schema, tenant_id, creation_timestamp, creator_id, is_void)
VALUES
('Not_Reference', '', '', '', '', 1, now(), 1, false),
('Service', 'code', 'id', '', 'services', 1, now(), 1, false),
('Purpose', 'code', 'id', '', 'services', 1, now(), 1, false),
('Batch_code', 'code', 'id', '', 'sample', 1, now(), 1, false),
('Request', 'request_code', 'id', '', 'sample', 1, now(), 1, false),
('Service_type', 'code', 'id', '', 'services', 1, now(), 1, false),
('Service_provider', 'code', 'id', '', 'services', 1, now(), 1, false)
;

UPDATE core.entity_reference --User
SET textfield='user_name'
WHERE id=55;

UPDATE core.entity_reference --Program
SET textfield='code'
WHERE id=48;

UPDATE core.entity_reference --Crop
SET textfield='code'
WHERE id=51;


--Revert changes
--rollback DELETE from core.entity_reference WHERE id = 62;
--rollback DELETE from core.entity_reference WHERE id = 63;
--rollback DELETE from core.entity_reference WHERE id = 64;
--rollback DELETE from core.entity_reference WHERE id = 65;
--rollback DELETE from core.entity_reference WHERE id = 66;
--rollback DELETE from core.entity_reference WHERE id = 67;
--rollback DELETE from core.entity_reference WHERE id = 68;

--rollback UPDATE core.entity_reference SET textfield='' WHERE id=35;
--rollback UPDATE core.entity_reference SET textfield='' WHERE id=60;
--rollback UPDATE core.entity_reference SET textfield='' WHERE id=57;