--liquibase formatted sql

--changeset postgres:add_sequence_rules context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-168 Add sequence rules

ALTER SEQUENCE core.number_sequence_rule_id_seq restart with 1;
ALTER SEQUENCE core.formula_type_id_seq restart with 1;
ALTER SEQUENCE core.number_sequence_rule_segment_id_seq restart with 1; 
ALTER SEQUENCE core.segment_id_seq restart with 1; 

INSERT INTO core.number_sequence_rule
("name", lowest, next_rec, format, last_code, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, tenant_id)
VALUES 
('PlateRule',1,1,'1','1','2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',1,1,false,1)
,('RequestRule',1,1,'1','1','2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',1,1,false,1)
,('BatchRule',1,1,'1','1','2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',1,1,false,1)
,('SingleBatchRule',1,1,'1','1','2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',1,1,false,1)
;

INSERT INTO core.formula_type ("name",description,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void) VALUES
('foreignAPI','APIs request diferent than Tenant',1,'2021-03-22 11:39:57.762','2020-11-04 00:00:00.000',1,1,false);


INSERT INTO core.segment
("name", tag, is_a_p_i, is_auto_num, is_static, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, entityreference_id, formulatype_id)
VALUES
('username','{username}',true,false,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,55,1),
('crop','{crop}',true,false,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,51,1),
('currentyear','{year}',false,false,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,62,1),
('program','{program}',true,false,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,48,1),
('autonumreq','{autonum}',false,true,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,62,1),
('separator','{separator}',false,false,true,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,62,1),
('dateyyyymmdd','{date}',false,false,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,62,1),
('prefix','{requestprefix}',false,false,true,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,62,1),
('labrequest','{labrequest}',false,false,true,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,66,1),
('servicecode','{servicecode}',true,false,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,63,1),
('purpose','{purpose}',true,false,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,64,1),
('autonumbatch','{autonumbatch}',false,false,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,62,1),
('yearyy','{yearyy}',false,false,false,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,62,1);



INSERT INTO core.number_sequence_rule_segment 
(length,custom_field,"position",formula,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,numbersequencerule_id,segment_id) 
VALUES
     (2,NULL,1,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,1,1),
     (2,NULL,4,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,1,5),
     (2,NULL,5,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,1,4),
     (4,NULL,6,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,1,2),
     (2,NULL,3,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,1,3),
     (1,NULL,1,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,2,8),
     (2,NULL,2,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,2,9),
     (0,NULL,3,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,2,6),
     (8,NULL,4,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,2,7),
     (0,NULL,5,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,2,6),
     (4,NULL,6,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,2,5),
     (2,NULL,2,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,3,4),
     (2,NULL,3,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,3,11),
     (6,NULL,1,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,4,10),
     (2,NULL,2,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,4,4),
     (2,NULL,3,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,4,13),
     (2,NULL,4,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,4,11),
     (6,NULL,5,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,4,12),
     (0,'-',2,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',2,2,false,1,6),
     (1,'B',1,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,3,8),
     (2,'PI',4,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,3,8),
     (2,'2021',5,NULL,1,'2020-01-01 00:00:00.000','2020-01-01 00:00:00.000',42,42,false,3,8);



INSERT INTO core.number_sequence_rule_entity_reference
(entityreference_id, numbersequencerule_id)
VALUES
(62,1)
,(66,2)
,(65,3)
,(65,4)
;


--Revert Changes
--rollback TRUNCATE core.number_sequence_rule_entity_reference;
--rollback TRUNCATE core.number_sequence_rule_segment; 
--rollback TRUNCATE core.segment CASCADE;
--rollback TRUNCATE core.formula_type CASCADE ;
--rollback TRUNCATE core.number_sequence_rule CASCADE;





