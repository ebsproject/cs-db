--liquibase formatted sql

--changeset postgres:create_permissions_for_arm context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-173 Create_permissions_for_ARM


INSERT INTO "security".product_function
(id, description, system_type, "action", creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, stage_id, node_id, product_id)
VALUES(1, 'Create Request', true, 'Create Request', 'now()', NULL, 1, NULL, true, NULL, NULL, 12);

INSERT INTO "security".product_function
(id, description, system_type, "action", creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, stage_id, node_id, product_id)
VALUES(2, 'List Request', true, 'List Request', 'now()', NULL, 1, NULL, true, NULL, NULL, 12);

INSERT INTO "security".product_function
(id, description, system_type, "action", creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, stage_id, node_id, product_id)
VALUES(3, 'Download Request', true, 'Download Request', 'now()', NULL, 1, NULL, true, NULL, NULL, 12);

SELECT setval('"security".product_function_id_seq', (SELECT MAX(id) FROM "security".product_function));

--Rever Changes
--rollback DELETE FROM "security".product_function WHERE id=1;
--rollback DELETE FROM "security".product_function WHERE id=2;
--rollback DELETE FROM "security".product_function WHERE id=3;