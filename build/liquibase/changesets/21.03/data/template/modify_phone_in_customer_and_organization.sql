--liquibase formatted sql

--changeset postgres:modify_phone_in_customer_and_organization context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-139 Changes in Tenant, Organization and Customer table



UPDATE core.customer
SET phone_extension = null
WHERE id = 1;

UPDATE core.customer
SET phone = 0
WHERE id = 1;

UPDATE core.organization
SET phone = 0
WHERE id = 1;

--Revert Changes
--rollback UPDATE core.customer SET phone_extension = 'NA' WHERE id = 1;