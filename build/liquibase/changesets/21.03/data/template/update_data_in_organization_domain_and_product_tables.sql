--liquibase formatted sql

--changeset postgres:update_data_in_organization_domain_and_product_tables context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-175 Update data in Organization, Product and Domain tables



UPDATE core."domain" 
SET icon='FieldImplementation.svg'
WHERE id=1;

UPDATE core."domain"
SET icon='coreSystem.svg'
WHERE id=2;

UPDATE core."domain"
SET icon='BreedingAnalytic.svg'
WHERE id=3;

UPDATE core."domain"
SET icon='AnalyticalSampling.svg'
WHERE id=4;

INSERT INTO core.organization
(id, legal_name, phone, web_page, slogan, "name", logo, is_active, default_authentication, default_theme, code, creation_timestamp, creator_id, is_void, customer_id)
values
(3, 'CIMMYT', 525558042004, 'www.cimmyt.org', 'Maize and wheat science for improved livelihoods.', 'CIMMYT', 'cimmyt.png', true, 1, 1, 'CM', now(), 1, false, 1);

UPDATE core.organization SET logo='cgiar.org' WHERE id=2;

UPDATE core.customer SET logo='cimmyt.png' WHERE id= 2;
UPDATE core.customer SET logo='irri.png' WHERE id= 3;

UPDATE core.product SET path = '/sm/requestmanager' WHERE id=14;
UPDATE core.product SET path = 'usermanagement', icon = 'people' WHERE id=9;
UPDATE core.product SET path = 'tenantmanagement', icon = 'account_balance' WHERE id=10;
UPDATE core.product SET path = 'dashboard', icon = 'contact_mail' WHERE id=11;

INSERT INTO core.product
("name", description, help, main_entity, icon, path, creation_timestamp, creator_id, is_void, domain_id, htmltag_id)
VALUES
('Service Catalog', 'Service Catalog', 'Available servies', 'Services','SC', '/', now(), 1, false, 4, 1),
('PLIMS', 'PLIMS', 'PLIMS', 'PLIMS', 'PLIMS', '/', now(), 1, false, 4, 1),
('QLIMS', 'QLIMS', 'QLIMS', 'QLIMS', 'QLIMS', '/', now(), 1, false, 4, 1),
('Shipment Tool', 'Shipment Tool', 'Shipment Tool', 'Shipment', 'ST', '/', now(), 1, false, 4, 1),
('Genotyping Service Manager', 'Genotyping Service Manager', 'Genotyping Service Manager', 'Genotyping', 'GSM', '/', now(), 1, false, 4, 1),
('Settings', 'Settings', 'Settings', 'Settings', 'settings', 'setting', now(), 1, false, 2, 1);


SELECT setval('core.product_id_seq', (SELECT MAX(id) FROM core.product));
SELECT setval('core.organization_id_seq', (SELECT MAX(id) FROM core.organization));


--Revert Changes
--rollback delete from core.product where id > 15;
--rollback UPDATE core.organization SET logo='' WHERE id=2;
--rollback UPDATE core.customer SET logo= NULL WHERE id= 2;
--rollback UPDATE core.customer SET logo= NULL WHERE id= 3;
--rollback UPDATE core.product SET path = '' WHERE id=14;
--rollback UPDATE core.product SET path = '', icon = '' WHERE id=9;
--rollback UPDATE core.product SET path = '', icon = '' WHERE id=10;
--rollback UPDATE core.product SET path = '', icon = '' WHERE id=11;
--rollback DELETE FROM core.organization WHERE id = 3;

--rollback UPDATE core."domain" SET icon='CB' WHERE id=1;
--rollback UPDATE core."domain" SET icon='CS' WHERE id=2;
--rollback UPDATE core."domain" SET icon='BA' WHERE id=3;
--rollback UPDATE core."domain" SET icon='SM' WHERE id=4;