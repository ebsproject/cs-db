--liquibase formatted sql

--changeset postgres:Include_sgProject_in_domain_instance context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-145 Include_sgProject_in_domain_instance



ALTER TABLE core.domain_instance 
 ADD COLUMN sg_project varchar(500) NULL;

 --Revert changes
 --rollback ALTER TABLE core.domain_instance drop COLUMN sg_project;
