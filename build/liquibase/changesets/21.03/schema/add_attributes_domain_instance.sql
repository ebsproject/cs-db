--liquibase formatted sql

--changeset postgres:add_attributes_domain_instance context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-136 Create intermediate table domain_instance



ALTER TABLE core.language 
 RENAME COLUMN code_i_s_o TO code_iso;

ALTER TABLE core.organization 
 RENAME COLUMN tax_i_d TO tax_id;

ALTER TABLE security."user" 
 RENAME COLUMN is_i_s TO is_is;

ALTER TABLE security.domain_instance 
  DROP  CONSTRAINT "FK_domain_instance_domain";

ALTER TABLE security.domain_instance 
  DROP  CONSTRAINT "FK_domain_instance_instance";

ALTER TABLE security.domain_instance 
  DROP  CONSTRAINT "PK_domain_instance";

ALTER TABLE security.domain_instance 
 ADD COLUMN context varchar(150) NOT NULL;	-- It can be Core Breeding, SM, AF.;

ALTER TABLE security.domain_instance 
 ADD COLUMN creation_timestamp timestamp without time zone NOT NULL   DEFAULT now();	-- Timestamp when the record was added to the table

ALTER TABLE security.domain_instance 
 ADD COLUMN creator_id integer NOT NULL;	-- ID of the user who added the record to the table

ALTER TABLE security.domain_instance 
 ADD COLUMN id integer NOT NULL   DEFAULT NEXTVAL(('security."domain_instance_id_seq"'::text)::regclass);

ALTER TABLE security.domain_instance 
 ADD COLUMN is_ssl boolean NULL   DEFAULT false;	-- URL Security Certificate

ALTER TABLE security.domain_instance 
 ADD COLUMN is_void boolean NOT NULL   DEFAULT false;	-- Indicator whether the record is deleted (true) or not (false)

ALTER TABLE security.domain_instance 
 ADD COLUMN modification_timestamp timestamp without time zone NULL;	-- Timestamp when the record was last modified

ALTER TABLE security.domain_instance 
 ADD COLUMN modifier_id integer NULL;	-- ID of the user who last modified the record

ALTER TABLE security.domain_instance 
 ADD COLUMN tenant_id integer NOT NULL;	-- Id of the selected Tenant

ALTER TABLE security.domain_instance ADD CONSTRAINT "FK_domain_instance_domain"
	FOREIGN KEY (domain_id) REFERENCES core.domain (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.domain_instance ADD CONSTRAINT "FK_domain_instance_instance"
	FOREIGN KEY (instance_id) REFERENCES core.instance (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.domain_instance ADD CONSTRAINT "PK_domain_instance"
	PRIMARY KEY (id)
;

CREATE SEQUENCE security.domain_instance_id_seq INCREMENT 1 START 1;

COMMENT ON COLUMN security.domain_instance.context
	IS 'It can be Core Breeding, SM, AF.'
;

COMMENT ON COLUMN security.domain_instance.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN security.domain_instance.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN security.domain_instance.is_ssl
	IS 'URL Security Certificate'
;


COMMENT ON COLUMN security.domain_instance.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN security.domain_instance.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN security.domain_instance.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN security.domain_instance.tenant_id
	IS 'Id of the selected Tenant'
;


--Revert Changes

--rollback ALTER TABLE security.domain_instance DROP CONSTRAINT "FK_domain_instance_domain";
--rollback ALTER TABLE security.domain_instance DROP CONSTRAINT "FK_domain_instance_instance";
--rollback ALTER TABLE security.domain_instance DROP CONSTRAINT "PK_domain_instance";

--rollback ALTER TABLE "security".domain_instance ADD CONSTRAINT "FK_domain_instance_domain" FOREIGN KEY (domain_id) REFERENCES core.domain(id);
--rollback ALTER TABLE "security".domain_instance ADD CONSTRAINT "FK_domain_instance_instance" FOREIGN KEY (instance_id) REFERENCES core.instance(id);
--rollback ALTER TABLE "security".domain_instance ADD CONSTRAINT "PK_domain_instance" PRIMARY KEY (instance_id, domain_id);

--rollback ALTER TABLE security.domain_instance 
--rollback DROP COLUMN context;

--rollback ALTER TABLE security.domain_instance 
--rollback DROP COLUMN creation_timestamp;

--rollback ALTER TABLE security.domain_instance 
--rollback DROP COLUMN creator_id;

--rollback ALTER TABLE security.domain_instance 
--rollback DROP COLUMN id;

--rollback ALTER TABLE security.domain_instance 
--rollback DROP COLUMN is_ssl;

--rollback ALTER TABLE security.domain_instance 
--rollback DROP COLUMN is_void;

--rollback ALTER TABLE security.domain_instance 
--rollback DROP COLUMN modification_timestamp;

--rollback ALTER TABLE security.domain_instance 
--rollback DROP COLUMN modifier_id;

--rollback ALTER TABLE security.domain_instance 
--rollback DROP COLUMN tenant_id;

--rollback ALTER TABLE core.language 
--rollback RENAME COLUMN code_iso TO code_i_s_o;

--rollback ALTER TABLE core.organization 
--rollback RENAME COLUMN tax_id TO tax_i_d;

--rollback ALTER TABLE security."user" 
--rollback RENAME COLUMN is_is TO is_i_s;

--rollback DROP SEQUENCE security.domain_instance_id_seq;