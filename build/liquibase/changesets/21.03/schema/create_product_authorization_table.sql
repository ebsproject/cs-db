--liquibase formatted sql

--changeset postgres:create_product_authorization_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-144 Add new table to associate: tenant, product and functional unit



CREATE TABLE security.product_authorization
(
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('security."product_authorization_id_seq"'::text)::regclass),
	functional_unit_id integer NOT NULL,
	product_id integer NOT NULL
)
;

CREATE SEQUENCE security.product_authorization_id_seq INCREMENT 1 START 1;

ALTER TABLE security.product_authorization ADD CONSTRAINT "PK_product_authorization"
	PRIMARY KEY (id)
;

ALTER TABLE security.product_authorization 
  ADD CONSTRAINT tenant_product_unit UNIQUE (tenant_id,product_id,functional_unit_id)
;

ALTER TABLE security.product_authorization ADD CONSTRAINT "FK_product_authorization_functional_unit"
	FOREIGN KEY (functional_unit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.product_authorization ADD CONSTRAINT "FK_product_authorization_product"
	FOREIGN KEY (product_id) REFERENCES core.product (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.product_authorization ADD CONSTRAINT "FK_product_authorization_tenant"
	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action
;


COMMENT ON COLUMN security.product_authorization.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN security.product_authorization.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN security.product_authorization.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN security.product_authorization.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN security.product_authorization.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN security.product_authorization.tenant_id
	IS 'Id of the selected Tenant'
;

--Revert Changes
--rollback DROP TABLE security.product_authorization;
--rollback DROP SEQUENCE security.product_authorization_id_seq;