--liquibase formatted sql

--changeset postgres:logo_phone_path_sg_context_update context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-174 Data type changes in logo, phone, path and sg_context


ALTER TABLE core.domain_instance 
 RENAME COLUMN sg_project TO sg_context;

ALTER TABLE core.customer 
 ALTER COLUMN phone TYPE varchar(50);

ALTER TABLE crm.person 
 ALTER COLUMN phone TYPE varchar(50);

ALTER TABLE core.customer 
 ADD COLUMN logo varchar(50) NULL;

ALTER TABLE core.product 
 ADD COLUMN menu_order integer NULL;

ALTER TABLE core.product 
 ADD COLUMN path varchar(250) NULL;

COMMENT ON COLUMN core.customer.is_active
	IS 'Status of the customer, it can be active or not.'
;


COMMENT ON COLUMN core.customer.name
	IS 'Name of the customer'
;

COMMENT ON COLUMN core.customer.official_email
	IS 'Institution/Official email of the customer'
;

COMMENT ON COLUMN core.customer.phone
	IS 'Phone number of the customer'
;

COMMENT ON COLUMN core.customer.phone_extension
	IS 'Extension number of the customer within his/her Organization.'
;

COMMENT ON COLUMN core.domain_instance.context
	IS 'It can be Core Breeding, SM, AF.'
;


COMMENT ON COLUMN core.product.description
	IS 'Description or additional information of the module.'
;

COMMENT ON COLUMN core.product.help
	IS 'Additional information about the product.'
;


COMMENT ON COLUMN core.product.icon
	IS 'Icon displayed in the side bar.'
;

COMMENT ON COLUMN core.product.menu_order
	IS 'Show the order in the UI'
;

COMMENT ON COLUMN core.product.name
	IS 'Name of the module.'
;

COMMENT ON COLUMN core.product.path
	IS 'Shows the path og the product'
;

COMMENT ON COLUMN crm.person.family_name
	IS 'First name of the person'
;

COMMENT ON COLUMN crm.person.gender
	IS 'Gender of the person. Male/Female.'
;

COMMENT ON COLUMN crm.person.given_name
	IS 'Last name of the person.'
;

COMMENT ON COLUMN crm.person.has_credential
	IS 'Specifies if the person has or not credentials in the System.'
;

COMMENT ON COLUMN crm.person.phone
	IS 'Phone number of the person.'
;


--Rever Changes
--rollback ALTER TABLE core.domain_instance RENAME COLUMN sg_context TO sg_project;
--rollback ALTER TABLE core.customer ALTER COLUMN phone TYPE bigint USING phone::bigint;
--rollback ALTER TABLE core.customer DROP COLUMN logo;
--rollback ALTER TABLE core.product DROP COLUMN menu_order;
--rollback ALTER TABLE core.product DROP COLUMN path;