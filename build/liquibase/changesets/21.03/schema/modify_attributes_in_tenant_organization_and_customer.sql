--liquibase formatted sql

--changeset postgres:modify_attributes_in_tenant_organization_and_customer context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-139 Changes in Tenant, Organization and Customer tables



ALTER TABLE core.style_theme 
 DROP COLUMN IF EXISTS "default";

ALTER TABLE core.customer 
 ALTER COLUMN is_active SET NOT NULL;

ALTER TABLE core.customer 
 ALTER COLUMN name SET NOT NULL;

ALTER TABLE core.customer 
 ALTER COLUMN official_email SET NOT NULL;

ALTER TABLE core.customer 
 ALTER COLUMN phone SET NOT NULL;

ALTER TABLE core.customer 
 ALTER COLUMN phone_extension TYPE bigint USING phone_extension::bigint;

ALTER TABLE core.organization 
 ALTER COLUMN code SET NOT NULL;

ALTER TABLE core.organization 
 ALTER COLUMN is_active SET NOT NULL;

ALTER TABLE core.organization 
 ALTER COLUMN legal_name SET NOT NULL;

ALTER TABLE core.organization 
 ALTER COLUMN name SET NOT NULL;

ALTER TABLE core.organization 
 ALTER COLUMN phone SET NOT NULL;

ALTER TABLE core.tenant 
 ALTER COLUMN expired SET NOT NULL;

ALTER TABLE core.tenant 
 ALTER COLUMN expired SET DEFAULT false;

ALTER TABLE core.style_theme 
 ADD COLUMN is_default boolean NULL   DEFAULT false;	-- Specifies if the theme is the default one to use in the system.

ALTER TABLE security.domain_instance SET SCHEMA core;

ALTER SEQUENCE security.domain_instance_id_seq set schema core;

ALTER TABLE core.domain_instance ALTER COLUMN id SET DEFAULT nextval('core.domain_instance_id_seq');

COMMENT ON COLUMN core.customer.is_active
	IS 'Status of the customer, it can be active or not.'
;

COMMENT ON COLUMN core.customer.name
	IS 'Name of the customer'
;

COMMENT ON COLUMN core.customer.official_email
	IS 'Institution/Official email of the customer'
;

COMMENT ON COLUMN core.customer.phone
	IS 'Phone number of the customer'
;

COMMENT ON COLUMN core.customer.phone_extension
	IS 'Extension number of the customer within his/her Organization.'
;

COMMENT ON COLUMN core.domain_instance.context
	IS 'It can be Core Breeding, SM, AF.'
;

COMMENT ON COLUMN core.organization.code
	IS 'Unique identifier code for the Organization.'
;

COMMENT ON COLUMN core.organization.is_active
	IS 'Specifies the status of the Organization within the system, it can be active or not.'
;

COMMENT ON COLUMN core.organization.legal_name
	IS 'Official name of the Organization.'
;

COMMENT ON COLUMN core.organization.name
	IS 'Short name of the organization.'
;

COMMENT ON COLUMN core.organization.phone
	IS 'Official phone number of the organization.'
;

COMMENT ON COLUMN core.organization.slogan
	IS 'Slogan of the Organization.'
;

COMMENT ON COLUMN core.organization.web_page
	IS 'Official web site of the Organization.'
;

COMMENT ON COLUMN core.style_theme.is_default
	IS 'Specifies if the theme is the default one to use in the system.'
;

COMMENT ON COLUMN core.tenant.expired
	IS 'Specifies if the tenant instance is expired or not.'
;


--Revert Changes
--rollback ALTER TABLE core.style_theme RENAME is_default TO "default";
--rollback ALTER TABLE core.customer ALTER COLUMN is_active DROP NOT NULL;
--rollback ALTER TABLE core.customer ALTER COLUMN name DROP NOT NULL;
--rollback ALTER TABLE core.customer ALTER COLUMN official_email DROP NOT NULL;
--rollback ALTER TABLE core.customer ALTER COLUMN phone DROP NOT NULL;
--rollback ALTER TABLE core.customer ALTER COLUMN phone_extension TYPE varchar(50);
--rollback ALTER TABLE core.organization ALTER COLUMN code DROP NOT NULL;
--rollback ALTER TABLE core.organization ALTER COLUMN is_active DROP NOT NULL;
--rollback ALTER TABLE core.organization ALTER COLUMN legal_name DROP NOT NULL;
--rollback ALTER TABLE core.organization ALTER COLUMN name DROP NOT NULL;
--rollback ALTER TABLE core.organization ALTER COLUMN phone DROP NOT NULL;
--rollback ALTER TABLE core.tenant ALTER COLUMN expired DROP NOT NULL;
--rollback ALTER TABLE core.tenant ALTER COLUMN expired DROP DEFAULT;
--rollback ALTER TABLE core.domain_instance SET SCHEMA "security";
--rollback ALTER SEQUENCE core.domain_instance_id_seq set schema "security";