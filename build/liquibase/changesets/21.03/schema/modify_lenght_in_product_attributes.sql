--liquibase formatted sql

--changeset postgres:modify_lenght_in_attributes context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-129 Apply several changes in CS baseline Database


ALTER TABLE core.product 
 ALTER COLUMN help TYPE varchar(500);

ALTER TABLE core.product 
 ALTER COLUMN description TYPE varchar(500);

 ALTER TABLE core.organization 
 ALTER COLUMN phone TYPE bigint;

 ALTER TABLE core.customer 
 ALTER COLUMN phone TYPE bigint;
 

--Revert Changes
--rollback ALTER TABLE core.product 
--rollback  ALTER COLUMN help TYPE varchar(50);

--rollback ALTER TABLE core.product 
--rollback  ALTER COLUMN description TYPE varchar(50);

--rollback ALTER TABLE core.organization 
--rollback ALTER COLUMN phone TYPE int;

--rollback ALTER TABLE core.customer 
--rollback ALTER COLUMN phone TYPE int;
