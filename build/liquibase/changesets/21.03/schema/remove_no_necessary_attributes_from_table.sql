--liquibase formatted sql

--changeset postgres:Remove_no_necessary_attributes_from_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-129 Apply several changes in CS baseline Database



ALTER TABLE core.domain 
 DROP COLUMN IF EXISTS tenant_id;

ALTER TABLE core.product 
 DROP COLUMN IF EXISTS tenant_id;

ALTER TABLE security.product_function 
 DROP COLUMN IF EXISTS tenant_id;

ALTER TABLE core.domain 
 DROP COLUMN IF EXISTS host;

ALTER TABLE core.domain 
 DROP COLUMN IF EXISTS port;

ALTER TABLE core.domain 
 DROP COLUMN IF EXISTS release_no;

ALTER TABLE core.domain 
 DROP COLUMN IF EXISTS version;

ALTER TABLE core.instance 
 DROP COLUMN IF EXISTS health;

ALTER TABLE core.product 
 DROP COLUMN IF EXISTS container;

ALTER TABLE core.product 
 DROP COLUMN IF EXISTS route;

COMMENT ON COLUMN security.functional_unit.description
	IS 'Additional information about the functional unit.'
;


--rollback ALTER TABLE IF EXISTS core.domain 
--rollback ADD COLUMN tenant_id integer NOT NULL;

--rollback ALTER TABLE IF EXISTS core.product 
--rollback ADD COLUMN tenant_id integer NOT NULL;

--rollback ALTER TABLE IF EXISTS security.product_function 
--rollback ADD COLUMN tenant_id integer NOT NULL;

--rollback ALTER TABLE core.domain 
--rollback  ADD COLUMN host varchar(50) NOT NULL;

--rollback ALTER TABLE core.domain 
--rollback ADD COLUMN port varchar(50) NOT NULL;

--rollback ALTER TABLE core.domain 
--rollback ADD COLUMN release_no varchar(50);

--rollback ALTER TABLE core.domain 
--rollback ADD COLUMN version varchar(50) NULL;

--rollback ALTER TABLE core.instance 
--rollback ADD COLUMN health varchar(50) NULL;

--rollback ALTER TABLE core.product 
--rollback ADD COLUMN container varchar(50) NULL;

--rollback ALTER TABLE core.product 
--rollback ADD COLUMN route varchar(50) NULL;
