--liquibase formatted sql

--changeset postgres:remove_program_functional_unit_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-146 Change relationship between program and functional_unit



DROP TABLE IF EXISTS security.program_functional_unit;

--Revert Changes
--rollback CREATE TABLE "security".program_functional_unit (
--rollback program_id int4 NOT NULL,
--rollback functional_unit_id int4 NOT NULL);

--rollback ALTER TABLE security.program_functional_unit ADD CONSTRAINT "FK_program_functional_unit_functional_unit" FOREIGN KEY (functional_unit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.program_functional_unit ADD CONSTRAINT "FK_program_functional_unit_program" FOREIGN KEY (program_id) REFERENCES program.program (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.program_functional_unit ADD CONSTRAINT "PK_program_functional_unit" PRIMARY KEY (functional_unit_id,program_id);