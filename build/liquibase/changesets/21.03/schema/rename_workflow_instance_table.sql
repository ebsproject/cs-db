--liquibase formatted sql

--changeset postgres:Rename_workflow_instance_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-129 Apply several changes in CS baseline Database



ALTER TABLE workflow.event 
  DROP  CONSTRAINT "FK_event_instance";

ALTER TABLE workflow.request 
  DROP  CONSTRAINT "FK_request_instance";

ALTER TABLE workflow.status 
  DROP  CONSTRAINT "FK_status_instance";

ALTER TABLE workflow.instance 
  DROP  CONSTRAINT "FK_instance_workflow";

ALTER TABLE workflow.instance 
  DROP  CONSTRAINT "PK_instance";

ALTER TABLE workflow.event 
 ADD COLUMN wf_instance_id integer NULL;

ALTER TABLE workflow.request 
 ADD COLUMN wf_instance_id integer NULL;

ALTER TABLE workflow.status 
 ADD COLUMN wf_instance_id integer NULL;

ALTER TABLE workflow.instance RENAME TO wf_instance;

ALTER TABLE workflow.wf_instance ADD CONSTRAINT "PK_wf_instance"
	PRIMARY KEY (id)
;

ALTER TABLE workflow.event ADD CONSTRAINT "FK_event_wf_instance"
	FOREIGN KEY (wf_instance_id) REFERENCES workflow.wf_instance (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.request ADD CONSTRAINT "FK_request_wf_instance"
	FOREIGN KEY (wf_instance_id) REFERENCES workflow.wf_instance (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.status ADD CONSTRAINT "FK_status_wf_instance"
	FOREIGN KEY (wf_instance_id) REFERENCES workflow.wf_instance (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.wf_instance ADD CONSTRAINT "FK_wf_instance_workflow"
	FOREIGN KEY (workflow_id) REFERENCES workflow.workflow (id) ON DELETE No Action ON UPDATE No Action
;

--Reverse changes

--rollback ALTER TABLE workflow.wf_instance DROP CONSTRAINT "FK_wf_instance_workflow";

--rollback ALTER TABLE workflow.event DROP CONSTRAINT "FK_event_wf_instance";

--rollback ALTER TABLE workflow.request DROP CONSTRAINT "FK_request_wf_instance";

--rollback ALTER TABLE workflow.status DROP CONSTRAINT "FK_status_wf_instance";

--rollback ALTER TABLE workflow.wf_instance DROP CONSTRAINT "PK_wf_instance";

--rollback ALTER TABLE workflow.wf_instance RENAME TO instance;

--rollback ALTER TABLE workflow.event DROP COLUMN wf_instance_id;

--rollback ALTER TABLE workflow.request DROP COLUMN wf_instance_id;

--rollback ALTER TABLE workflow.status DROP COLUMN wf_instance_id;

--rollback ALTER TABLE workflow."instance" ADD CONSTRAINT "PK_instance" PRIMARY KEY (id);

--rollback ALTER TABLE workflow."instance" ADD CONSTRAINT "FK_instance_workflow" FOREIGN KEY (workflow_id) REFERENCES workflow.workflow(id);

--rollback ALTER TABLE workflow."event" ADD CONSTRAINT "FK_event_instance" FOREIGN KEY (instance_id) REFERENCES workflow.instance(id);

--rollback ALTER TABLE workflow.request ADD CONSTRAINT "FK_request_instance" FOREIGN KEY (instance_id) REFERENCES workflow.instance(id);

--rollback ALTER TABLE workflow.status ADD CONSTRAINT "FK_status_instance" FOREIGN KEY (instance_id) REFERENCES workflow.instance(id);
