--liquibase formatted sql

--changeset postgres:set_not_null_attributes_in_security_module context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-129 Apply several changes in CS baseline Database



ALTER TABLE core.customer 
  DROP  CONSTRAINT IF EXISTS "FK_customer_organization";

ALTER TABLE core.organization 
  DROP  CONSTRAINT IF EXISTS "FK_organization_customer";

ALTER TABLE crm.person 
  DROP  CONSTRAINT "FK_person_person_status";

ALTER TABLE crm.person 
  DROP  CONSTRAINT "FK_person_tenant";

ALTER TABLE crm.person_type 
  DROP  CONSTRAINT "FK_person_type_person";

ALTER TABLE crm.person_type 
  DROP  CONSTRAINT "FK_person_type_type";

ALTER TABLE security.functional_unit 
  DROP  CONSTRAINT "FK_functional_unit_functional_unit";

ALTER TABLE security.functional_unit_user 
  DROP  CONSTRAINT "FK_functional_unit_user_functional_unit";

ALTER TABLE security.functional_unit_user 
  DROP  CONSTRAINT "PK_functionalunit_user";

ALTER TABLE security.program_functional_unit 
  DROP  CONSTRAINT "FK_program_functional_unit_functional_unit";

ALTER TABLE security.role_product_function 
  DROP  CONSTRAINT "FK_role_product_function_product_function";

ALTER TABLE security.role_product_function 
  DROP  CONSTRAINT "FK_role_product_function_role";

ALTER TABLE crm.person 
 DROP COLUMN IF EXISTS personstatus_id;

ALTER TABLE security.functional_unit 
 DROP COLUMN IF EXISTS functionalunit_id;

ALTER TABLE security.functional_unit_user 
 DROP COLUMN IF EXISTS functionalunit_id;

ALTER TABLE security.program_functional_unit 
 DROP COLUMN IF EXISTS functionalunit_id;

ALTER TABLE security.role_product_function 
 DROP COLUMN IF EXISTS productfunction_id;

ALTER TABLE core.customer 
 ALTER COLUMN is_active TYPE boolean USING is_active::boolean;

ALTER TABLE core.customer 
 ALTER COLUMN organization_id DROP NOT NULL;

ALTER TABLE core.customer 
 ALTER COLUMN phone TYPE integer USING phone::integer;

ALTER TABLE core.organization 
 ALTER COLUMN code TYPE varchar(50);

ALTER TABLE core.organization 
 ALTER COLUMN customer_id DROP NOT NULL;

ALTER TABLE core.organization 
 ALTER COLUMN is_active TYPE boolean USING is_active::boolean;

ALTER TABLE core.organization 
 ALTER COLUMN legal_name TYPE varchar(200);

ALTER TABLE core.organization 
 ALTER COLUMN phone TYPE integer USING phone::integer;

ALTER TABLE core.organization 
 ALTER COLUMN slogan TYPE varchar(200);

ALTER TABLE core.organization 
 ALTER COLUMN web_page TYPE varchar(200);

ALTER TABLE core.product 
 ALTER COLUMN icon SET NOT NULL;

ALTER TABLE core.product 
 ALTER COLUMN name SET NOT NULL;

ALTER TABLE core.tenant 
 ALTER COLUMN expiration SET NOT NULL;

ALTER TABLE core.tenant 
 ALTER COLUMN name SET NOT NULL;

ALTER TABLE crm.person 
 ALTER COLUMN family_name SET NOT NULL;

ALTER TABLE crm.person 
 ALTER COLUMN gender SET NOT NULL;

ALTER TABLE crm.person 
 ALTER COLUMN given_name SET NOT NULL;

ALTER TABLE crm.person 
 ALTER COLUMN has_credential SET NOT NULL;

ALTER TABLE crm.person 
 ALTER COLUMN tenant_id SET NOT NULL;

ALTER TABLE crm.person_type 
 ALTER COLUMN person_id SET NOT NULL;

ALTER TABLE crm.person_type 
 ALTER COLUMN type_id SET NOT NULL;

ALTER TABLE security.functional_unit 
 ALTER COLUMN code SET NOT NULL;

ALTER TABLE security.functional_unit 
 ALTER COLUMN description TYPE varchar(200);

ALTER TABLE security.functional_unit 
 ALTER COLUMN name SET NOT NULL;

ALTER TABLE security.functional_unit 
 ALTER COLUMN notes TYPE varchar(200);

ALTER TABLE security.functional_unit 
 ALTER COLUMN type SET NOT NULL;

ALTER TABLE security.product_function 
 ALTER COLUMN action TYPE varchar(200);

ALTER TABLE security.product_function 
 ALTER COLUMN description TYPE varchar(200);

ALTER TABLE security.product_function 
 ALTER COLUMN system_type SET NOT NULL;

ALTER TABLE security.role_product_function 
 ALTER COLUMN role_id SET NOT NULL;

ALTER TABLE security."user" 
 ALTER COLUMN last_access TYPE timestamp without time zone;

ALTER TABLE core.customer 
 ALTER COLUMN is_active SET DEFAULT true;

ALTER TABLE core.organization 
 ALTER COLUMN is_active SET DEFAULT true;

ALTER TABLE crm.person 
 ALTER COLUMN has_credential SET DEFAULT false;

ALTER TABLE security.product_function 
 ALTER COLUMN system_type SET DEFAULT false;

ALTER TABLE core.instance 
 ADD COLUMN name varchar(100) NULL;

ALTER TABLE core.instance 
 ADD COLUMN notes varchar(150) NULL;

ALTER TABLE crm.person 
 ADD COLUMN person_status_id integer NOT NULL;

ALTER TABLE security.functional_unit 
 ADD COLUMN parent_func_unit_id integer NULL;

ALTER TABLE security.functional_unit_user 
 ADD COLUMN functional_unit_id integer NOT NULL;

ALTER TABLE security.program_functional_unit 
 ADD COLUMN functional_unit_id integer NOT NULL;

ALTER TABLE security.role 
 ADD COLUMN name varchar(50) NOT NULL;

ALTER TABLE security.role_product_function 
 ADD COLUMN product_function_id integer NOT NULL;

ALTER TABLE core.customer ADD CONSTRAINT "FK_customer_organization"
	FOREIGN KEY (organization_id) REFERENCES core.organization (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.organization ADD CONSTRAINT "FK_organization_customer"
	FOREIGN KEY (customer_id) REFERENCES core.customer (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.person ADD CONSTRAINT "FK_person_person_status"
	FOREIGN KEY (person_status_id) REFERENCES crm.person_status (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.person ADD CONSTRAINT "FK_person_tenant"
	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.person_type ADD CONSTRAINT "FK_person_type_person"
	FOREIGN KEY (person_id) REFERENCES crm.person (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.person_type ADD CONSTRAINT "FK_person_type_type"
	FOREIGN KEY (type_id) REFERENCES crm.type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.functional_unit ADD CONSTRAINT "FK_functional_unit_functional_unit"
	FOREIGN KEY (parent_func_unit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.functional_unit_user ADD CONSTRAINT "FK_functional_unit_user_functional_unit"
	FOREIGN KEY (functional_unit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.functional_unit_user ADD CONSTRAINT "PK_functionalunit_user"
	PRIMARY KEY (user_id,functional_unit_id)
;

ALTER TABLE security.program_functional_unit ADD CONSTRAINT "FK_program_functional_unit_functional_unit"
	FOREIGN KEY (functional_unit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.role_product_function ADD CONSTRAINT "FK_role_product_function_product_function"
	FOREIGN KEY (product_function_id) REFERENCES security.product_function (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.role_product_function ADD CONSTRAINT "FK_role_product_function_role"
	FOREIGN KEY (role_id) REFERENCES security.role (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.email_template_entity_reference ADD CONSTRAINT "PK_email_template_entity_reference"
	PRIMARY KEY (entityreference_id,emailtemplate_id)
;

ALTER TABLE core.number_sequence_rule_entity_reference ADD CONSTRAINT "PK_number_sequence_rule_entity_reference"
	PRIMARY KEY (entityreference_id,numbersequencerule_id)
;

ALTER TABLE crm.address_person ADD CONSTRAINT "PK_address_person"
	PRIMARY KEY (person_id,address_id)
;

ALTER TABLE crm.person_type ADD CONSTRAINT "PK_person_type"
	PRIMARY KEY (type_id,person_id)
;

ALTER TABLE security.domain_instance ADD CONSTRAINT "PK_domain_instance"
	PRIMARY KEY (instance_id,domain_id)
;

ALTER TABLE security.program_functional_unit ADD CONSTRAINT "PK_program_functional_unit"
	PRIMARY KEY (functional_unit_id,program_id)
;

ALTER TABLE security.role_product_function ADD CONSTRAINT "PK_role_product_function"
	PRIMARY KEY (product_function_id,role_id)
;

COMMENT ON COLUMN core.customer.is_active
	IS 'Status of the customer, it can be active or not.'
;

COMMENT ON COLUMN core.customer.phone
	IS 'Phone number of the customer'
;

COMMENT ON COLUMN core.organization.code
	IS 'Unique identifier code for the Organization.'
;

COMMENT ON COLUMN core.organization.is_active
	IS 'Specifies the status of the Organization within the system, it can be active or not.'
;

COMMENT ON COLUMN core.organization.legal_name
	IS 'Official name of the Organization.'
;

COMMENT ON COLUMN core.organization.phone
	IS 'Official phone number of the organization.'
;

COMMENT ON COLUMN core.organization.slogan
	IS 'Slogan of the Organization.'
;

COMMENT ON COLUMN core.organization.web_page
	IS 'Official web site of the Organization.'
;

COMMENT ON COLUMN core.product.icon
	IS 'Icon displayed in the side bar.'
;

COMMENT ON COLUMN core.product.name
	IS 'Name of the module.'
;

COMMENT ON COLUMN core.tenant.expiration
	IS 'Expiration date of the instance by user.'
;

COMMENT ON COLUMN crm.person.family_name
	IS 'First name of the person'
;

COMMENT ON COLUMN crm.person.gender
	IS 'Gender of the person. Male/Female.'
;

COMMENT ON COLUMN crm.person.given_name
	IS 'Last name of the person.'
;

COMMENT ON COLUMN crm.person.has_credential
	IS 'Specifies if the person has or not credentials in the System.'
;

COMMENT ON COLUMN security.functional_unit.code
	IS 'Textual identifier of the functional unit.'
;

COMMENT ON COLUMN security.functional_unit.description
	IS 'Additional information about the functional unit.'
;

COMMENT ON COLUMN security.functional_unit.name
	IS 'Full name of the functional unit.'
;

COMMENT ON COLUMN security.functional_unit.notes
	IS 'Technical details about the record.'
;

COMMENT ON COLUMN security.functional_unit.type
	IS 'It can be unit, sub-unit, teams, group'
;

COMMENT ON COLUMN security.product_function.action
	IS 'Name of the action, it can be an option module. e.g. Request module action: Create request.'
;

COMMENT ON COLUMN security.product_function.description
	IS 'description/Additional information about the action.'
;

COMMENT ON COLUMN security.product_function.system_type
	IS 'Type of the action, it can be a human task or a system action.'
;

COMMENT ON COLUMN security."user".last_access
	IS 'Date of the latest access of the user.'
;


--revert changes

--rollback ALTER TABLE core.customer DROP CONSTRAINT IF EXISTS "FK_customer_organization";

--rollback ALTER TABLE core.organization DROP CONSTRAINT IF EXISTS "FK_organization_customer";

--rollback ALTER TABLE crm.person DROP CONSTRAINT "FK_person_person_status";

--rollback ALTER TABLE crm.person DROP CONSTRAINT "FK_person_tenant";

--rollback ALTER TABLE crm.person_type DROP CONSTRAINT "FK_person_type_person";

--rollback ALTER TABLE crm.person_type DROP CONSTRAINT "FK_person_type_type";

--rollback ALTER TABLE security.functional_unit DROP CONSTRAINT "FK_functional_unit_functional_unit";

--rollback ALTER TABLE security.functional_unit_user DROP CONSTRAINT "FK_functional_unit_user_functional_unit";

--rollback ALTER TABLE security.functional_unit_user DROP CONSTRAINT "PK_functionalunit_user";

--rollback ALTER TABLE security.program_functional_unit DROP CONSTRAINT "FK_program_functional_unit_functional_unit";

--rollback ALTER TABLE security.role_product_function DROP CONSTRAINT "FK_role_product_function_product_function";

--rollback ALTER TABLE security.role_product_function DROP CONSTRAINT "FK_role_product_function_role";

--rollback ALTER TABLE core.email_template_entity_reference DROP CONSTRAINT "PK_email_template_entity_reference";

--rollback ALTER TABLE core.number_sequence_rule_entity_reference DROP CONSTRAINT "PK_number_sequence_rule_entity_reference";

--rollback ALTER TABLE crm.address_person DROP CONSTRAINT "PK_address_person";

--rollback ALTER TABLE crm.person_type DROP CONSTRAINT "PK_person_type";

--rollback ALTER TABLE security.domain_instance DROP CONSTRAINT "PK_domain_instance";

--rollback ALTER TABLE security.program_functional_unit DROP CONSTRAINT "PK_program_functional_unit";

--rollback ALTER TABLE security.role_product_function DROP CONSTRAINT "PK_role_product_function";

--rollback ALTER TABLE crm.person 
--rollback  ADD COLUMN personstatus_id integer NOT NULL;

--rollback ALTER TABLE security.functional_unit 
--rollback  ADD COLUMN functionalunit_id integer NOT NULL;

--rollback ALTER TABLE security.functional_unit_user 
--rollback ADD COLUMN functionalunit_id integer NOT NULL;

--rollback ALTER TABLE security.program_functional_unit 
--rollback  ADD COLUMN functionalunit_id integer NOT NULL;

--rollback ALTER TABLE security.role_product_function 
--rollback  ADD COLUMN productfunction_id integer NOT NULL;

--rollback ALTER TABLE core.customer 
--rollback  ALTER COLUMN is_active TYPE varchar(50);

--rollback ALTER TABLE core.customer 
--rollback  ALTER COLUMN organization_id SET NOT NULL;

--rollback ALTER TABLE core.customer 
--rollback  ALTER COLUMN phone TYPE varchar(50);

--rollback ALTER TABLE core.organization 
--rollback  ALTER COLUMN code TYPE integer USING code::integer;

--rollback ALTER TABLE core.organization 
--rollback  ALTER COLUMN customer_id SET NOT NULL;

--rollback ALTER TABLE core.organization 
--rollback  ALTER COLUMN is_active TYPE varchar(50);

--rollback ALTER TABLE core.organization 
--rollback  ALTER COLUMN legal_name TYPE varchar(50);

--rollback ALTER TABLE core.organization 
--rollback  ALTER COLUMN phone TYPE varchar(50);

--rollback ALTER TABLE core.organization 
--rollback  ALTER COLUMN slogan TYPE varchar(50);

--rollback ALTER TABLE core.organization 
--rollback  ALTER COLUMN web_page TYPE varchar(50);

--rollback ALTER TABLE core.product 
--rollback  ALTER COLUMN icon DROP NOT NULL;

--rollback ALTER TABLE core.product 
--rollback  ALTER COLUMN name DROP NOT NULL;

--rollback ALTER TABLE core.tenant 
--rollback  ALTER COLUMN expiration DROP NOT NULL;

--rollback ALTER TABLE core.tenant 
--rollback  ALTER COLUMN name DROP NOT NULL;

--rollback ALTER TABLE crm.person 
--rollback  ALTER COLUMN family_name DROP NOT NULL;

--rollback ALTER TABLE crm.person 
--rollback  ALTER COLUMN gender DROP NOT NULL;

--rollback ALTER TABLE crm.person 
--rollback  ALTER COLUMN given_name DROP NOT NULL;

--rollback ALTER TABLE crm.person 
--rollback  ALTER COLUMN has_credential DROP NOT NULL;

--rollback ALTER TABLE crm.person 
--rollback  ALTER COLUMN tenant_id DROP NOT NULL;

--rollback ALTER TABLE crm.person_type 
--rollback  ALTER COLUMN person_id DROP NOT NULL;

--rollback ALTER TABLE crm.person_type 
--rollback  ALTER COLUMN type_id DROP NOT NULL;

--rollback ALTER TABLE security.functional_unit 
--rollback  ALTER COLUMN code DROP NOT NULL;

--rollback ALTER TABLE security.functional_unit 
--rollback  ALTER COLUMN description TYPE varchar(50);

--rollback ALTER TABLE security.functional_unit 
--rollback  ALTER COLUMN name DROP NOT NULL;

--rollback ALTER TABLE security.functional_unit 
--rollback  ALTER COLUMN notes TYPE varchar(50);

--rollback ALTER TABLE security.functional_unit 
--rollback  ALTER COLUMN type DROP NOT NULL;

--rollback ALTER TABLE security.product_function 
--rollback  ALTER COLUMN action TYPE varchar(50);

--rollback ALTER TABLE security.product_function 
--rollback  ALTER COLUMN description TYPE varchar(50);

--rollback ALTER TABLE security.product_function 
--rollback  ALTER COLUMN system_type DROP NOT NULL;

--rollback ALTER TABLE security.role_product_function 
--rollback  ALTER COLUMN role_id DROP NOT NULL;

--rollback ALTER TABLE security."user" 
--rollback  ALTER COLUMN last_access TYPE timestamp;

--rollback ALTER TABLE core.customer 
--rollback  ALTER COLUMN is_active DROP DEFAULT;

--rollback ALTER TABLE core.organization 
--rollback  ALTER COLUMN is_active DROP DEFAULT;

--rollback ALTER TABLE crm.person 
--rollback  ALTER COLUMN has_credential DROP DEFAULT;

--rollback ALTER TABLE security.product_function 
--rollback  ALTER COLUMN system_type DROP DEFAULT;

--rollback ALTER TABLE core.instance 
--rollback  DROP COLUMN name;

--rollback ALTER TABLE core.instance 
--rollback  DROP COLUMN notes;

--rollback ALTER TABLE crm.person 
--rollback  DROP COLUMN person_status_id;

--rollback ALTER TABLE security.functional_unit 
--rollback  DROP COLUMN parent_func_unit_id;

--rollback ALTER TABLE security.functional_unit_user 
--rollback  DROP COLUMN functional_unit_id;

--rollback ALTER TABLE security.program_functional_unit 
--rollback  DROP COLUMN functional_unit_id;

--rollback ALTER TABLE security.role 
--rollback  DROP COLUMN name;

--rollback ALTER TABLE security.role_product_function 
--rollback  DROP COLUMN product_function_id;

--rollback ALTER TABLE crm.person ADD CONSTRAINT "FK_person_person_status" FOREIGN KEY (personstatus_id) REFERENCES crm.person_status(id);

--rollback ALTER TABLE crm.person ADD CONSTRAINT "FK_person_tenant" FOREIGN KEY (tenant_id) REFERENCES core.tenant(id);

--rollback ALTER TABLE crm.person_type ADD CONSTRAINT "FK_person_type_person" FOREIGN KEY (person_id) REFERENCES crm.person(id);

--rollback ALTER TABLE crm.person_type ADD CONSTRAINT "FK_person_type_type" FOREIGN KEY (type_id) REFERENCES crm.type(id);

--rollback ALTER TABLE "security".functional_unit ADD CONSTRAINT "FK_functional_unit_functional_unit" 
--rollback FOREIGN KEY (functionalunit_id) REFERENCES security.functional_unit(id);

--rollback ALTER TABLE "security".functional_unit_user ADD CONSTRAINT "FK_functional_unit_user_functional_unit" 
--rollback FOREIGN KEY (functionalunit_id) REFERENCES security.functional_unit(id);

--rollback ALTER TABLE "security".functional_unit_user ADD CONSTRAINT "PK_functionalunit_user" PRIMARY KEY (user_id, functionalunit_id);

--rollback ALTER TABLE "security".program_functional_unit ADD CONSTRAINT "FK_program_functional_unit_functional_unit" 
--rollback FOREIGN KEY (functionalunit_id) REFERENCES security.functional_unit(id);

--rollback ALTER TABLE "security".role_product_function ADD CONSTRAINT "FK_role_product_function_product_function" 
--rollback FOREIGN KEY (productfunction_id) REFERENCES security.product_function(id);

--rollback ALTER TABLE "security".role_product_function ADD CONSTRAINT "FK_role_product_function_role" FOREIGN KEY (role_id) REFERENCES security.role(id);
