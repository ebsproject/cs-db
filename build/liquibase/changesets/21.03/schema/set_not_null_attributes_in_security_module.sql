--liquibase formatted sql

--changeset postgres:set_not_null_attributes_in_security_module context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-129 Apply several changes in CS baseline Database


ALTER TABLE IF EXISTS core.domain_entityreference 
  DROP  CONSTRAINT IF EXISTS "FK_domain_entityreference_domain";

ALTER TABLE IF EXISTS core.domain_entityreference 
  DROP  CONSTRAINT IF EXISTS "FK_domain_entityreference_entity_reference";

ALTER TABLE IF EXISTS core.email_template 
  DROP  CONSTRAINT IF EXISTS "FK_email_template_person";

ALTER TABLE IF EXISTS core.emailtemplate_entityreference 
  DROP  CONSTRAINT IF EXISTS "FK_emailtemplate_entityreference_email_template";

ALTER TABLE IF EXISTS core.emailtemplate_entityreference 
  DROP  CONSTRAINT IF EXISTS "FK_emailtemplate_entityreference_entity_reference";

ALTER TABLE IF EXISTS core.numbersequencerule_entityreference 
  DROP  CONSTRAINT IF EXISTS "FK_numbersequencerule_entityreference_entity_reference";

ALTER TABLE IF EXISTS core.numbersequencerule_entityreference 
  DROP  CONSTRAINT IF EXISTS "FK_numbersequencerule_entityreference_number_sequence_rule";

ALTER TABLE IF EXISTS crm.address_person 
  DROP  CONSTRAINT IF EXISTS "FK_address_person_address";

ALTER TABLE IF EXISTS crm.address_person 
  DROP  CONSTRAINT IF EXISTS "FK_address_person_person";

ALTER TABLE IF EXISTS crm.person_type 
  DROP  CONSTRAINT IF EXISTS "FK_person_type_person";

ALTER TABLE IF EXISTS security.domain_instance 
  DROP  CONSTRAINT IF EXISTS "FK_domain_instance_domain";

ALTER TABLE IF EXISTS security.domain_instance 
  DROP  CONSTRAINT IF EXISTS "FK_domain_instance_instance";

ALTER TABLE IF EXISTS security.functionalunit_user 
  DROP  CONSTRAINT IF EXISTS "FK_functionalunit_user_functional_unit";

ALTER TABLE IF EXISTS security.functionalunit_user 
  DROP  CONSTRAINT IF EXISTS "FK_functionalunit_user_user";

ALTER TABLE IF EXISTS security.program_functionalunit 
  DROP  CONSTRAINT IF EXISTS "FK_program_functionalunit_functional_unit";

ALTER TABLE IF EXISTS security.role_productfunction 
  DROP  CONSTRAINT IF EXISTS "FK_role_productfunction_product_function";

ALTER TABLE IF EXISTS security.role_productfunction 
  DROP  CONSTRAINT IF EXISTS "FK_role_productfunction_role";

ALTER TABLE IF EXISTS security.tenant_user 
  DROP  CONSTRAINT IF EXISTS "FK_tenant_user_tenant";

ALTER TABLE IF EXISTS security.tenant_user 
  DROP  CONSTRAINT IF EXISTS "FK_tenant_user_user";

ALTER TABLE IF EXISTS security."user" 
  DROP  CONSTRAINT IF EXISTS "FK_user_person";

ALTER TABLE IF EXISTS security.user_role 
  DROP  CONSTRAINT IF EXISTS "FK_user_role_role";

ALTER TABLE IF EXISTS security.user_role 
  DROP  CONSTRAINT IF EXISTS "FK_user_role_user";

ALTER TABLE IF EXISTS workflow.request 
  DROP  CONSTRAINT IF EXISTS "FK_request_person";

ALTER TABLE IF EXISTS workflow.servicetype_workflow 
  DROP  CONSTRAINT IF EXISTS "FK_servicetype_workflow_workflow";

ALTER TABLE IF EXISTS security.functional_unit 
 DROP COLUMN IF EXISTS event_log;

ALTER TABLE IF EXISTS security.functional_unit 
 DROP COLUMN IF EXISTS team_code;

ALTER TABLE IF EXISTS security.functional_unit 
 DROP COLUMN IF EXISTS team_name;

ALTER TABLE IF EXISTS core.domain_entityreference RENAME TO domain_entity_reference;

ALTER TABLE IF EXISTS core.emailtemplate_entityreference RENAME TO email_template_entity_reference;

ALTER TABLE IF EXISTS core.numbersequencerule_entityreference RENAME TO number_sequence_rule_entity_reference;

ALTER TABLE IF EXISTS security.functionalunit_user RENAME TO functional_unit_user;

ALTER TABLE IF EXISTS security.program_functionalunit RENAME TO program_functional_unit;

ALTER TABLE IF EXISTS security.role_productfunction RENAME TO role_product_function;

ALTER TABLE IF EXISTS workflow.servicetype_workflow RENAME TO service_type_workflow;

ALTER TABLE IF EXISTS security.person SET SCHEMA crm;

ALTER TABLE IF EXISTS core.domain_entity_reference 
 ALTER COLUMN domain_id SET NOT null;

ALTER TABLE IF EXISTS core.domain_entity_reference 
 ALTER COLUMN entityreference_id SET NOT null;

ALTER TABLE IF EXISTS core.email_template_entity_reference 
 ALTER COLUMN emailtemplate_id SET NOT null;

ALTER TABLE IF EXISTS core.email_template_entity_reference 
 ALTER COLUMN entityreference_id SET NOT null;

ALTER TABLE IF EXISTS core.number_sequence_rule_entity_reference 
 ALTER COLUMN entityreference_id SET NOT null;

ALTER TABLE IF EXISTS core.number_sequence_rule_entity_reference 
 ALTER COLUMN numbersequencerule_id SET NOT null;

ALTER TABLE IF EXISTS crm.address_person 
 ALTER COLUMN address_id SET NOT null;

ALTER TABLE IF EXISTS crm.address_person 
 ALTER COLUMN person_id SET NOT null;

ALTER TABLE IF EXISTS security.domain_instance 
 ALTER COLUMN domain_id SET NOT null;

ALTER TABLE IF EXISTS security.domain_instance 
 ALTER COLUMN instance_id SET NOT null;

ALTER TABLE IF EXISTS security.functional_unit_user 
 ALTER COLUMN functionalunit_id SET NOT null;

ALTER TABLE IF EXISTS security.functional_unit_user 
 ALTER COLUMN user_id SET NOT null;

ALTER TABLE IF EXISTS security.program_functional_unit 
 ALTER COLUMN functionalunit_id SET NOT null;

ALTER TABLE IF EXISTS security.role_product_function 
 ALTER COLUMN productfunction_id SET NOT null;

ALTER TABLE IF EXISTS security.tenant_user 
 ALTER COLUMN tenant_id SET NOT null;

ALTER TABLE IF EXISTS security.tenant_user 
 ALTER COLUMN user_id SET NOT null;

ALTER TABLE IF EXISTS security.user_role 
 ALTER COLUMN role_id SET NOT null;

ALTER TABLE IF EXISTS security.user_role 
 ALTER COLUMN user_id SET NOT null;

ALTER TABLE IF EXISTS security.functional_unit 
 ADD COLUMN code varchar(50) null;	-- Textual identifier of the team.

ALTER TABLE IF EXISTS security.functional_unit 
 ADD COLUMN name varchar(50) null;	-- Full name of the team.

ALTER TABLE IF EXISTS security.program_functional_unit 
 ADD COLUMN program_id integer NOT null;

-- Create Schema "Program"
CREATE SCHEMA program;

-- Create Tables in Program schema
CREATE TABLE program.crop
(
	code varchar(50) NULL,
	name varchar(50) NULL,
	description varchar(50) NULL,
	notes varchar(50) NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('program."crop_id_seq"'::text)::regclass)
)
;

CREATE TABLE program.program
(
	code varchar(50) NULL,
	name varchar(50) NULL,
	type varchar(50) NULL,
	status varchar(50) NULL,
	description varchar(50) NULL,
	notes varchar(50) NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('program."program_id_seq"'::text)::regclass),
	tenant_id integer NULL,
	crop_id integer NOT NULL
)
;

CREATE TABLE program.project
(
	code varchar(50) NULL,
	name varchar(50) NULL,
	status varchar(50) NULL,
	description varchar(50) NULL,
	notes varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('program."project_id_seq"'::text)::regclass),
	person_id integer NULL,
	program_id integer NULL
)
;

CREATE TABLE program.season
(
	code varchar(50) NULL,
	name varchar(50) NULL,
	description varchar(50) NULL,
	notes varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('program."season_id_seq"'::text)::regclass)
)
;

CREATE SEQUENCE program.crop_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE program.program_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE program.project_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE program.season_id_seq INCREMENT 1 START 1;

ALTER SEQUENCE security.person_id_seq set schema crm;

ALTER TABLE IF EXISTS program.crop ADD CONSTRAINT "PK_crop"
	PRIMARY KEY (id)
;

ALTER TABLE IF EXISTS program.program ADD CONSTRAINT "PK_program"
	PRIMARY KEY (id)
;

ALTER TABLE IF EXISTS program.project ADD CONSTRAINT "PK_project"
	PRIMARY KEY (id)
;

ALTER TABLE IF EXISTS program.season ADD CONSTRAINT "PK_season"
	PRIMARY KEY (id)
;

ALTER TABLE IF EXISTS program.program ADD CONSTRAINT "FK_program_crop"
	FOREIGN KEY (crop_id) REFERENCES program.crop (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS program.program ADD CONSTRAINT "FK_program_tenant"
	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS program.project ADD CONSTRAINT "FK_project_person"
	FOREIGN KEY (person_id) REFERENCES crm.person (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS program.project ADD CONSTRAINT "FK_project_program"
	FOREIGN KEY (program_id) REFERENCES program.program (id) ON DELETE No Action ON UPDATE No Action
;

-- Add constraints

ALTER TABLE IF EXISTS core.email_template ADD CONSTRAINT "FK_email_template_person"
	FOREIGN KEY (person_id) REFERENCES crm.person (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS crm.address_person ADD CONSTRAINT "FK_address_person_address"
	FOREIGN KEY (address_id) REFERENCES crm.address (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS crm.address_person ADD CONSTRAINT "FK_address_person_person"
	FOREIGN KEY (person_id) REFERENCES crm.person (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS crm.person_type ADD CONSTRAINT "FK_person_type_person"
	FOREIGN KEY (person_id) REFERENCES crm.person (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.domain_instance ADD CONSTRAINT "FK_domain_instance_domain"
	FOREIGN KEY (domain_id) REFERENCES core.domain (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.domain_instance ADD CONSTRAINT "FK_domain_instance_instance"
	FOREIGN KEY (instance_id) REFERENCES core.instance (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.tenant_user ADD CONSTRAINT "FK_tenant_user_tenant"
	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.tenant_user ADD CONSTRAINT "FK_tenant_user_user"
	FOREIGN KEY (user_id) REFERENCES security."user" (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security."user" ADD CONSTRAINT "FK_user_person"
	FOREIGN KEY (person_id) REFERENCES crm.person (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.user_role ADD CONSTRAINT "FK_user_role_role"
	FOREIGN KEY (role_id) REFERENCES security.role (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.user_role ADD CONSTRAINT "FK_user_role_user"
	FOREIGN KEY (user_id) REFERENCES security."user" (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS workflow.request ADD CONSTRAINT "FK_request_person"
	FOREIGN KEY (person_id) REFERENCES crm.person (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS core.domain_entity_reference ADD CONSTRAINT "PK_domain_entity_reference"
	PRIMARY KEY (entityreference_id,domain_id)
;

ALTER TABLE IF EXISTS security.functional_unit_user ADD CONSTRAINT "PK_functionalunit_user"
	PRIMARY KEY (user_id,functionalunit_id)
;

ALTER TABLE IF EXISTS core.domain_entity_reference ADD CONSTRAINT "FK_domain_entity_reference_domain"
	FOREIGN KEY (domain_id) REFERENCES core.domain (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS core.domain_entity_reference ADD CONSTRAINT "FK_domain_entity_reference_entity_reference"
	FOREIGN KEY (entityreference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS core.email_template_entity_reference ADD CONSTRAINT "FK_email_template_entity_reference_email_template"
	FOREIGN KEY (emailtemplate_id) REFERENCES core.email_template (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS core.email_template_entity_reference ADD CONSTRAINT "FK_email_template_entity_reference_entity_reference"
	FOREIGN KEY (entityreference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS core.number_sequence_rule_entity_reference ADD CONSTRAINT "FK_number_sequence_rule_entity_reference_entity_reference"
	FOREIGN KEY (entityreference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS core.number_sequence_rule_entity_reference ADD CONSTRAINT "FK_number_sequence_rule_entity_reference_number_sequence_rule"
	FOREIGN KEY (numbersequencerule_id) REFERENCES core.number_sequence_rule (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.functional_unit_user ADD CONSTRAINT "FK_functional_unit_user_functional_unit"
	FOREIGN KEY (functionalunit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.functional_unit_user ADD CONSTRAINT "FK_functional_unit_user_user"
	FOREIGN KEY (user_id) REFERENCES security."user" (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.program_functional_unit ADD CONSTRAINT "FK_program_functional_unit_functional_unit"
	FOREIGN KEY (functionalunit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.program_functional_unit ADD CONSTRAINT "FK_program_functional_unit_program"
	FOREIGN KEY (program_id) REFERENCES program.program (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.role_product_function ADD CONSTRAINT "FK_role_product_function_product_function"
	FOREIGN KEY (productfunction_id) REFERENCES security.product_function (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS security.role_product_function ADD CONSTRAINT "FK_role_product_function_role"
	FOREIGN KEY (role_id) REFERENCES security.role (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE IF EXISTS workflow.service_type_workflow ADD CONSTRAINT "FK_service_type_workflow_workflow"
	FOREIGN KEY (workflow_id) REFERENCES workflow.workflow (id) ON DELETE No Action ON UPDATE No Action
;

-- Add comments to tables

COMMENT ON COLUMN core.product.help
	IS 'Additional information about the product.'
;

COMMENT ON COLUMN core.product.route
	IS 'url/Path  of the Product'
;

COMMENT ON COLUMN program.crop.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN program.crop.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN program.crop.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN program.crop.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN program.crop.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN program.program.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN program.program.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN program.program.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN program.program.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN program.program.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN program.project.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN program.project.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN program.project.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN program.project.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN program.project.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN program.project.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN program.season.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN program.season.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN program.season.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN program.season.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN program.season.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN program.season.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN security.functional_unit.code
	IS 'Textual identifier of the functional unit.'
;

COMMENT ON COLUMN security.functional_unit.name
	IS 'Full name of the functional unit.'
;

--rollback COMMENT ON COLUMN core.product.help IS 'Additional information about the module.';
--rollback COMMENT ON COLUMN core.product.route IS 'url/Path  of the Module';

--rollback ALTER TABLE IF EXISTS workflow.service_type_workflow DROP CONSTRAINT "FK_service_type_workflow_workflow";
--rollback ALTER TABLE IF EXISTS security.role_product_function DROP CONSTRAINT "FK_role_product_function_role";
--rollback ALTER TABLE IF EXISTS security.role_product_function DROP CONSTRAINT "FK_role_product_function_product_function";
--rollback ALTER TABLE IF EXISTS security.program_functional_unit DROP CONSTRAINT "FK_program_functional_unit_program";
--rollback ALTER TABLE IF EXISTS security.program_functional_unit DROP CONSTRAINT "FK_program_functional_unit_functional_unit";
--rollback ALTER TABLE IF EXISTS security.functional_unit_user DROP CONSTRAINT "FK_functional_unit_user_user";
--rollback ALTER TABLE IF EXISTS security.functional_unit_user DROP CONSTRAINT "FK_functional_unit_user_functional_unit";
--rollback ALTER TABLE IF EXISTS core.number_sequence_rule_entity_reference DROP CONSTRAINT "FK_number_sequence_rule_entity_reference_number_sequence_rule";
--rollback ALTER TABLE IF EXISTS core.number_sequence_rule_entity_reference DROP CONSTRAINT "FK_number_sequence_rule_entity_reference_entity_reference";
--rollback ALTER TABLE IF EXISTS core.email_template_entity_reference DROP CONSTRAINT "FK_email_template_entity_reference_entity_reference";
--rollback ALTER TABLE IF EXISTS core.email_template_entity_reference DROP CONSTRAINT "FK_email_template_entity_reference_email_template";
--rollback ALTER TABLE IF EXISTS core.domain_entity_reference DROP CONSTRAINT "FK_domain_entity_reference_entity_reference";
--rollback ALTER TABLE IF EXISTS core.domain_entity_reference DROP CONSTRAINT "FK_domain_entity_reference_domain";
--rollback ALTER TABLE IF EXISTS security.functional_unit_user DROP CONSTRAINT "PK_functionalunit_user";
--rollback ALTER TABLE IF EXISTS core.domain_entity_reference DROP CONSTRAINT "PK_domain_entity_reference";
--rollback ALTER TABLE IF EXISTS workflow.request DROP CONSTRAINT "FK_request_person";
--rollback ALTER TABLE IF EXISTS security.user_role DROP CONSTRAINT "FK_user_role_user";
--rollback ALTER TABLE IF EXISTS security.user_role DROP CONSTRAINT "FK_user_role_role";
--rollback ALTER TABLE IF EXISTS security."user" DROP CONSTRAINT "FK_user_person";
--rollback ALTER TABLE IF EXISTS security.tenant_user DROP CONSTRAINT "FK_tenant_user_user";
--rollback ALTER TABLE IF EXISTS security.tenant_user DROP CONSTRAINT "FK_tenant_user_tenant";
--rollback ALTER TABLE IF EXISTS security.domain_instance DROP CONSTRAINT "FK_domain_instance_instance";
--rollback ALTER TABLE IF EXISTS security.domain_instance DROP CONSTRAINT "FK_domain_instance_domain";
--rollback ALTER TABLE IF EXISTS crm.person_type DROP CONSTRAINT "FK_person_type_person";
--rollback ALTER TABLE IF EXISTS crm.address_person DROP CONSTRAINT "FK_address_person_person";
--rollback ALTER TABLE IF EXISTS crm.address_person DROP CONSTRAINT "FK_address_person_address";
--rollback ALTER TABLE IF EXISTS core.email_template DROP CONSTRAINT "FK_email_template_person";

--rollback DROP SCHEMA program CASCADE;

--rollback ALTER TABLE IF EXISTS security.program_functional_unit DROP COLUMN program_id;
--rollback ALTER TABLE IF EXISTS security.functional_unit DROP COLUMN name;
--rollback ALTER TABLE IF EXISTS security.functional_unit DROP COLUMN code;
--rollback ALTER TABLE IF EXISTS security.user_role ALTER COLUMN user_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS core.domain_entity_reference ALTER COLUMN domain_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS core.domain_entity_reference ALTER COLUMN entityreference_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS core.email_template_entity_reference ALTER COLUMN emailtemplate_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS core.email_template_entity_reference ALTER COLUMN entityreference_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS core.number_sequence_rule_entity_reference ALTER COLUMN entityreference_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS core.number_sequence_rule_entity_reference ALTER COLUMN numbersequencerule_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS crm.address_person ALTER COLUMN address_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS crm.address_person ALTER COLUMN person_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS security.domain_instance ALTER COLUMN domain_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS security.domain_instance ALTER COLUMN instance_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS security.functional_unit_user ALTER COLUMN functionalunit_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS security.functional_unit_user ALTER COLUMN user_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS security.program_functional_unit ALTER COLUMN functionalunit_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS security.role_product_function ALTER COLUMN productfunction_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS security.tenant_user ALTER COLUMN tenant_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS security.tenant_user ALTER COLUMN user_id DROP NOT null;
--rollback ALTER TABLE IF EXISTS security.user_role ALTER COLUMN role_id DROP NOT null;

--rollback ALTER TABLE IF EXISTS crm.person SET SCHEMA security;
--rollback ALTER SEQUENCE crm.person_id_seq set schema security;

--rollback ALTER TABLE IF EXISTS workflow.service_type_workflow RENAME TO servicetype_workflow;
--rollback ALTER TABLE IF EXISTS security.role_product_function RENAME TO role_productfunction;
--rollback ALTER TABLE IF EXISTS security.program_functional_unit RENAME TO program_functionalunit;
--rollback ALTER TABLE IF EXISTS security.functional_unit_user RENAME TO functionalunit_user;
--rollback ALTER TABLE IF EXISTS core.number_sequence_rule_entity_reference RENAME TO numbersequencerule_entityreference;
--rollback ALTER TABLE IF EXISTS core.email_template_entity_reference RENAME TO emailtemplate_entityreference;
--rollback ALTER TABLE IF EXISTS core.domain_entity_reference RENAME TO domain_entityreference;

--rollback ALTER TABLE IF EXISTS security.functional_unit ADD COLUMN event_log varchar(50) NULL;
--rollback ALTER TABLE IF EXISTS security.functional_unit ADD COLUMN team_code varchar(50) NULL;
--rollback ALTER TABLE IF EXISTS security.functional_unit ADD COLUMN team_name varchar(50) NULL;

--rollback ALTER TABLE core.domain_entityreference ADD CONSTRAINT "FK_domain_entityreference_domain"
--rollback FOREIGN KEY (domain_id) REFERENCES core.domain (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE core.domain_entityreference ADD CONSTRAINT "FK_domain_entityreference_entity_reference"
--rollback 	FOREIGN KEY (entityreference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE core.email_template ADD CONSTRAINT "FK_email_template_person"
--rollback 	FOREIGN KEY (person_id) REFERENCES security.person (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE core.emailtemplate_entityreference ADD CONSTRAINT "FK_emailtemplate_entityreference_email_template"
--rollback 	FOREIGN KEY (emailtemplate_id) REFERENCES core.email_template (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE core.emailtemplate_entityreference ADD CONSTRAINT "FK_emailtemplate_entityreference_entity_reference"
--rollback 	FOREIGN KEY (entityreference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE core.numbersequencerule_entityreference ADD CONSTRAINT "FK_numbersequencerule_entityreference_entity_reference"
--rollback 	FOREIGN KEY (entityreference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE core.numbersequencerule_entityreference ADD CONSTRAINT "FK_numbersequencerule_entityreference_number_sequence_rule"
--rollback 	FOREIGN KEY (numbersequencerule_id) REFERENCES core.number_sequence_rule (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE crm.address_person ADD CONSTRAINT "FK_address_person_address"
--rollback 	FOREIGN KEY (address_id) REFERENCES crm.address (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE crm.address_person ADD CONSTRAINT "FK_address_person_person"
--rollback 	FOREIGN KEY (person_id) REFERENCES security.person (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE crm.person_type ADD CONSTRAINT "FK_person_type_person"
--rollback 	FOREIGN KEY (person_id) REFERENCES security.person (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.domain_instance ADD CONSTRAINT "FK_domain_instance_domain"
--rollback 	FOREIGN KEY (domain_id) REFERENCES core.domain (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.domain_instance ADD CONSTRAINT "FK_domain_instance_instance"
--rollback 	FOREIGN KEY (instance_id) REFERENCES core.instance (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.functionalunit_user ADD CONSTRAINT "FK_functionalunit_user_functional_unit"
--rollback 	FOREIGN KEY (functionalunit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.functionalunit_user ADD CONSTRAINT "FK_functionalunit_user_user"
--rollback 	FOREIGN KEY (user_id) REFERENCES security."user" (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.program_functionalunit ADD CONSTRAINT "FK_program_functionalunit_functional_unit"
--rollback 	FOREIGN KEY (functionalunit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.role_productfunction ADD CONSTRAINT "FK_role_productfunction_product_function"
--rollback 	FOREIGN KEY (productfunction_id) REFERENCES security.product_function (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.role_productfunction ADD CONSTRAINT "FK_role_productfunction_role"
--rollback 	FOREIGN KEY (role_id) REFERENCES security.role (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.tenant_user ADD CONSTRAINT "FK_tenant_user_tenant"
--rollback 	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.tenant_user ADD CONSTRAINT "FK_tenant_user_user"
--rollback 	FOREIGN KEY (user_id) REFERENCES security."user" (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security."user" ADD CONSTRAINT "FK_user_person"
--rollback 	FOREIGN KEY (person_id) REFERENCES security.person (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.user_role ADD CONSTRAINT "FK_user_role_role"
--rollback 	FOREIGN KEY (role_id) REFERENCES security.role (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE security.user_role ADD CONSTRAINT "FK_user_role_user"
--rollback 	FOREIGN KEY (user_id) REFERENCES security."user" (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE workflow.request ADD CONSTRAINT "FK_request_person"
--rollback 	FOREIGN KEY (person_id) REFERENCES security.person (id) ON DELETE No Action ON UPDATE No Action;

--rollback ALTER TABLE workflow.servicetype_workflow ADD CONSTRAINT "FK_servicetype_workflow_workflow"
--rollback 	FOREIGN KEY (workflow_id) REFERENCES workflow.workflow (id) ON DELETE No Action ON UPDATE No Action;

