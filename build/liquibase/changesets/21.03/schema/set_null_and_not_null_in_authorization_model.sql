--liquibase formatted sql

--changeset postgres:set_null_and_not_null_in_authorization_model context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-150 Fix model authorization



ALTER TABLE security.product_authorization 
 ALTER COLUMN functional_unit_id DROP NOT NULL;

ALTER TABLE security.functional_unit 
 ALTER COLUMN program_id SET NOT NULL;

ALTER TABLE program.program 
 ALTER COLUMN owner_id SET NOT NULL;



--Revert Changes
--rollback ALTER TABLE security.product_authorization ALTER COLUMN functional_unit_id SET NOT NULL;
--rollback ALTER TABLE security.functional_unit ALTER COLUMN program_id DROP NOT NULL;
--rollback ALTER TABLE program.program ALTER COLUMN owner_id DROP NOT NULL;
