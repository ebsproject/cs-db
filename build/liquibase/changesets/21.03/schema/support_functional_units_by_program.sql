--liquibase formatted sql

--changeset postgres:support_functional_units_by_program context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-141 update model: support functional units by program



ALTER TABLE program.program 
 ADD COLUMN owner_id integer NULL;

 ALTER TABLE security.functional_unit 
 ADD COLUMN program_id integer NULL;

 ALTER TABLE program.program ADD CONSTRAINT "FK_program_user"
	FOREIGN KEY (owner_id) REFERENCES security."user" (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.functional_unit ADD CONSTRAINT "FK_functional_unit_program"
	FOREIGN KEY (program_id) REFERENCES program.program (id) ON DELETE No Action ON UPDATE No Action
;

--Revert Changes
--rollback ALTER TABLE program.program DROP COLUMN owner_id;
--rollback ALTER TABLE security.functional_unit DROP COLUMN program_id;
