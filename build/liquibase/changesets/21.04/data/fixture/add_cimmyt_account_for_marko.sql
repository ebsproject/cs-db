--liquibase formatted sql

--changeset postgres:add_cimmyt_account_for_marko context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-260 Add CIMMYT account for Marko Karkkainen in csdb



INSERT INTO "security"."user"
(id, user_name, last_access, user_password, default_role, is_is, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, person_id)
VALUES(67, 'm.karkkainen@cimmyt.org', NULL, 'NA', 2, 1, now(), NULL, 1, NULL, false, 2);

INSERT INTO "security".functional_unit_user(user_id, functional_unit_id) VALUES
(67, 1);

INSERT INTO "security".user_role
(role_id, user_id)
VALUES
(2, 67);

SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));

--Revert changes
--rollback DELETE FROM "security".functional_unit_user WHERE user_id =67;
--rollback DELETE FROM "security".user_role WHERE user_id =67;
--rollback DELETE FROM "security"."user" WHERE id = 67;

