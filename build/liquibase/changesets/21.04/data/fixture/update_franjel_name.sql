--liquibase formatted sql

--changeset postgres:update_franjel_name context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-259 Update Franjel's name in CS Person table



UPDATE crm.person
SET given_name='Franjel'
WHERE id=42;

--Rever Changes
--rollback UPDATE crm.person SET given_name='Frangel' WHERE id=42;