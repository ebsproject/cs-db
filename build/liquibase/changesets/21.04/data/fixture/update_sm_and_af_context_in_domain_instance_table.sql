--liquibase formatted sql

--changeset postgres:update_sm_and_af_context_in_domain_instance_table context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-258 Update SM and AF context in CS domain_instance table



UPDATE core.domain_instance
SET context='http://localhost:3000'
WHERE id=4;

UPDATE core.domain_instance
SET context='http://localhost:3000'
WHERE id=5;

--Revert changes
--rollback UPDATE core.domain_instance SET context='https://ebs-staging-wheat.cimmyt.org/index.php' WHERE id=4;
--rollback UPDATE core.domain_instance SET context='https://ebs-staging-wheat.cimmyt.org/index.php' WHERE id=5;
