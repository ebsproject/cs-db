--liquibase formatted sql

--changeset postgres:update_path_for genotyping_sm_in_product_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-261 Update path in CS product for Genotyping Service Manager



UPDATE core.product
SET  "path"='/sm/vendor'
WHERE id=20;

--Rever Changes
--rollback UPDATE core.product SET "path"='/' WHERE id=20;
