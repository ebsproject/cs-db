--liquibase formatted sql

--changeset postgres:update_sm_menu_order_products context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-323 Update SM menu_order in cs.products


UPDATE core.product SET menu_order=3 WHERE id=22;
UPDATE core.product SET menu_order=4 WHERE id=23;
UPDATE core.product SET menu_order=5 WHERE id=24;

UPDATE core.product SET menu_order=6 WHERE id=16;
UPDATE core.product SET menu_order=7 WHERE id=17;
UPDATE core.product SET menu_order=8 WHERE id=18;
UPDATE core.product SET menu_order=9 WHERE id=19;

--Revert Changes
--rollback UPDATE core.product SET menu_order=NULL WHERE id=22;
--rollback UPDATE core.product SET menu_order=NULL WHERE id=23;
--rollback UPDATE core.product SET menu_order=NULL WHERE id=24;

--rollback UPDATE core.product SET menu_order=3 WHERE id=16;
--rollback UPDATE core.product SET menu_order=4 WHERE id=17;
--rollback UPDATE core.product SET menu_order=5 WHERE id=18;
--rollback UPDATE core.product SET menu_order=6 WHERE id=19;
