--liquibase formatted sql

--changeset postgres:update_sm_products_and_paths context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-306 Add SM Products and Update path in smdb



UPDATE core.product SET path = '/sm/review' WHERE id=20;

INSERT INTO core.product
(id,"name", description, help, main_entity, icon, path, creation_timestamp, creator_id, is_void, domain_id, htmltag_id)
VALUES
(22,'Inspect', ' SM Inspect', 'SM Inspect', 'Inspect', 'GSM', '/sm/inspect', now(), 1, false, 4, 1),
(23,'Design', ' SM Design', 'SM Design', 'Design', 'GSM', '/sm/design', now(), 1, false, 4, 1),
(24,'Vendor', ' SM Vendor', 'SM Vendor', 'Vendor', 'GSM', '/sm/vendor', now(), 1, false, 4, 1);

SELECT setval('core.product_id_seq', (SELECT MAX(id) FROM core.product));

--Revert Changes
--rollback UPDATE core.product SET path = '/' WHERE id=20;
--rollback DELETE FROM core.product WHERE ID in (22,23,24);
