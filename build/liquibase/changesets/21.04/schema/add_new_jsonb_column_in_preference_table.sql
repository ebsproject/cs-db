--liquibase formatted sql

--changeset postgres:add_new_jsonb_column_in_preference_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-262 Add new jsonb column in CS Preference table



ALTER TABLE core.preference 
 ADD COLUMN preference jsonb NULL;


--Revert Changes
--rollback ALTER TABLE core.preference DROP COLUMN preference;
