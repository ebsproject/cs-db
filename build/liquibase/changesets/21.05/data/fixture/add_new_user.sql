--liquibase formatted sql

--changeset postgres:add_new_user context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-450 Add new user in csdb


INSERT INTO crm.person
(id, given_name, family_name, additional_name, email, official_email, gender, has_credential, job_title, knows_about, phone, person_status_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, tenant_id, language_id)
VALUES
(66, 'Juan', 'Moreno', null, 'j.m.sanchez@cimmyt.org', 'j.m.sanchez@cimmyt.org', 'male', true, 'Systems Software Engineer', 'IT', '5959521900', 1, now(), null, 1, null, false, 1, 1);


INSERT INTO "security"."user"
(id, user_name, last_access, default_role_id, is_is, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, person_id)
VALUES
(68, 'j.m.sanchez@cimmyt.org', null, 2, 1, now(), null, 1, null, false, 66);


INSERT INTO "security".functional_unit_user(user_id, functional_unit_id) VALUES
(68, 1);

INSERT INTO "security".user_role
(role_id, user_id)
VALUES
(2, 68);


SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));
SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));

--Revert Changes

--rollback DELETE FROM "security".user_role where role_id = 2 and user_id = 68;
--rollback DELETE FROM "security".functional_unit_user where user_id = 68 and functional_unit_id = 1;
--rollback DELETE FROM "security"."user" where id = 68;
--rollback DELETE FROM crm.person where id = 66;