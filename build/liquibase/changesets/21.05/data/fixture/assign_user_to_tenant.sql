--liquibase formatted sql

--changeset postgres:assign_user_to_tenant context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-468 Add user to tenant


INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(68, 1);
INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(68, 2);


--Revert Changes
--rollback DELETE FROM "security".tenant_user WHERE user_id=68 and tenant_id=1;
--rollback DELETE FROM "security".tenant_user WHERE user_id=68 and tenant_id=2;