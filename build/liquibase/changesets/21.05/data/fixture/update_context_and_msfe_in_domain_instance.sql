--liquibase formatted sql

--changeset postgres:update_context_and_msfe_in_domain_instance context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-435 Update context and mfe in CS domain_instance table



UPDATE core.domain_instance
SET context='https://cb-dev.ebsproject.org', sg_context='https://cbapi-dev.ebsproject.org', is_mfe= false
WHERE id=1;
UPDATE core.domain_instance
SET context='https://cb-dev.ebsproject.org', sg_context='https://cbapi-dev.ebsproject.org', is_mfe= false
WHERE id=2;
UPDATE core.domain_instance
SET context='https://cb-dev.ebsproject.org', sg_context='https://cbapi-dev.ebsproject.org', is_mfe= false
WHERE id=3;


--Revert Changes
--rollback UPDATE core.domain_instance SET context='https://ebs-staging-maize.cimmyt.org/index.php', sg_context=NULL, is_mfe= true WHERE id=1;
--rollback UPDATE core.domain_instance SET context='https://ebs-staging-wheat.cimmyt.org/index.php', sg_context=NULL, is_mfe= true WHERE id=2;
--rollback UPDATE core.domain_instance SET context='https://ebs-staging-wheat.cimmyt.org/index.php', sg_context=NULL, is_mfe= true WHERE id=3;
