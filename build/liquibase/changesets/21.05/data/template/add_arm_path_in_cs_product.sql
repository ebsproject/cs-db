--liquibase formatted sql

--changeset postgres:add_arm_path_in_cs_product context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-353 Add ARM path in cs.Product

UPDATE core.product
SET menu_order=1, "path"= '/arm'
WHERE id=13;


UPDATE core.product
SET menu_order=2, "path"= '/arm/add'
WHERE id=12;



--Revert Changes
--rollback UPDATE core.product SET menu_order=2, "path"= NULL WHERE id=13;
--rollback UPDATE core.product SET menu_order=1, "path"= NULL WHERE id=12;
