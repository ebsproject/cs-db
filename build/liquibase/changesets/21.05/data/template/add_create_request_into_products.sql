--liquibase formatted sql

--changeset postgres:add_create_request_into_products context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-467 Add Create Request into CS Products


INSERT INTO core.product
("name", description, help, main_entity, icon, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, domain_id, htmltag_id, menu_order, "path")
VALUES('Create Request', ' SM Create Request', 'SM Create Request', 'Create Request', '', now(), NULL, 1, NULL, false, 25, 4, 1, 2, '/sm/requestmanager/create-request-service');


UPDATE core.product SET menu_order=3 WHERE id=20;
UPDATE core.product SET menu_order=4 WHERE id=22;
UPDATE core.product SET menu_order=5 WHERE id=23;
UPDATE core.product SET menu_order=6 WHERE id=24;


UPDATE core.product SET menu_order=7 WHERE id=16;
UPDATE core.product SET menu_order=8 WHERE id=17;
UPDATE core.product SET menu_order=9 WHERE id=18;
UPDATE core.product SET menu_order=10 WHERE id=19;


SELECT setval('core.product_id_seq', (SELECT MAX(id) FROM core.product));


--Revert Changes
--rollback DELETE FROM core.product WHERE ID =25;

--rollback UPDATE core.product SET menu_order=2 WHERE id=20;
--rollback UPDATE core.product SET menu_order=3 WHERE id=22;
--rollback UPDATE core.product SET menu_order=4 WHERE id=23;
--rollback UPDATE core.product SET menu_order=5 WHERE id=24;

--rollback UPDATE core.product SET menu_order=6 WHERE id=16;
--rollback UPDATE core.product SET menu_order=7 WHERE id=17;
--rollback UPDATE core.product SET menu_order=8 WHERE id=18;
--rollback UPDATE core.product SET menu_order=9 WHERE id=19;