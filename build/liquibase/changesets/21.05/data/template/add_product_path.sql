--liquibase formatted sql

--changeset postgres:add_product_path context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-374 add_product_path into existing records


UPDATE core.product
SET  "path"= '/'
WHERE id <= 8;


--Rever Changes
--rollback UPDATE core.product SET  "path"= NULL WHERE id <= 8;