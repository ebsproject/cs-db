--liquibase formatted sql

--changeset postgres:remove_af_from_products context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-451 Remove analytical framework from products list in csdb


DELETE FROM core.product
WHERE id=13;


--Revert Changes
--rollback INSERT INTO core.product ("name", description, help, main_entity, icon, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, domain_id, htmltag_id, menu_order, "path") VALUES('Analytical Framework', 'Analytical Framework', 'Analytical Framework', 'AF', 'NA', '2021-04-21 22:03:18.658', NULL, 1, NULL, false, 13, 3, 1, 2, '/arm');

