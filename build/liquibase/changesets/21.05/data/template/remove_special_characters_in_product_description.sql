--liquibase formatted sql

--changeset postgres:remove_special_characters_in_product_description context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-395 Remove special characters in core.product.description



UPDATE core.product
SET description='The Experiment Creation tool enables end-users to create Breeding Trial experiments with various statistical designs and field layouts, Intentional Crossing Nursery experiments, Cross Parent Nursery experiments, and Generation Nursery experiments for advancing segregated populations, seed increase.'
WHERE id=1;

UPDATE core.product
SET description='Experiment manager acts as an orchestrator and enables users to search, view, manage, monitor and suppress Occurrences and Locations.'
WHERE id=2;

UPDATE core.product
SET description='help users generate an accurate Planted Array: informing tools further down the workflow of a plots contents.'
WHERE id=4;

UPDATE core.product
SET description='The Pollination Instructions tool will facilitate the main users (breeders) to specify exact female and ale plots that are used in pollinations for making desirable crosses.'
WHERE id=5;

--Revert Changes
--rollback UPDATE core.product
--rollback SET description='The Experiment Creation tool enables end-users to create Breeding 
--rollback Trial experiments with various statistical designs and field layouts, Intentional Crossing 
--rollback Nursery experiments, Cross Parent Nursery experiments, and Generation Nursery experiments 
--rollback for advancing segregated populations, seed increase.'
--rollback WHERE id=1;

--rollback UPDATE core.product
--rollback SET description='Experiment manager acts as an orchestrator and enables users to 
--rollback search, view, manage, monitor and suppress Occurrences and Locations. '
--rollback WHERE id=2;

--rollback UPDATE core.product
--rollback SET description='help users generate an accurate Planted Array: informing 
--rollback tools further down the workflow of a plot’s contents. '
--rollback WHERE id=4;

--rollback UPDATE core.product
--rollback SET description='The Pollination Instructions tool will facilitate 
--rollback the main users (breeders) to specify exact female and male plots that are used in pollinations 
--rollback for making desirable crosses.'
--rollback WHERE id=5;
