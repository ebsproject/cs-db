--liquibase formatted sql

--changeset postgres:update_path_for_settings_in_product_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-430 Update path for settings in product table



UPDATE core.product
SET  "path"='settings'
WHERE id=21;


--Revert Changes
--rollback UPDATE core.product SET "path"='setting' WHERE id=21;


