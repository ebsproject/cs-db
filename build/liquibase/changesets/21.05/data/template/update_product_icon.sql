--liquibase formatted sql

--changeset postgres:update_product_icon context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-436 Update icon in CS product table



UPDATE core.product
SET icon=''
WHERE domain_id <> 2;


--Revert Changes
--rollback UPDATE core.product SET icon='GSM' WHERE id=22;
--rollback UPDATE core.product SET icon='GSM' WHERE id=23;
--rollback UPDATE core.product SET icon='GSM' WHERE id=24;
--rollback UPDATE core.product SET icon='GSM' WHERE id=20;
--rollback UPDATE core.product SET icon='NA' WHERE id=8;
--rollback UPDATE core.product SET icon='NA' WHERE id=4;
--rollback UPDATE core.product SET icon='NA' WHERE id=5;
--rollback UPDATE core.product SET icon='NA' WHERE id=14;
--rollback UPDATE core.product SET icon='NA' WHERE id=6;
--rollback UPDATE core.product SET icon='NA' WHERE id=7;
--rollback UPDATE core.product SET icon='NA' WHERE id=1;
--rollback UPDATE core.product SET icon='NA' WHERE id=2;
--rollback UPDATE core.product SET icon='NA' WHERE id=13;
--rollback UPDATE core.product SET icon='NA' WHERE id=12;
--rollback UPDATE core.product SET icon='NA' WHERE id=3;
--rollback UPDATE core.product SET icon='PLIMS' WHERE id=17;
--rollback UPDATE core.product SET icon='QLIMS' WHERE id=18;
--rollback UPDATE core.product SET icon='SC' WHERE id=16;
--rollback UPDATE core.product SET icon='ST' WHERE id=19;