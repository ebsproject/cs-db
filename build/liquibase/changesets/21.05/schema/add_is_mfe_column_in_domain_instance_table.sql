--liquibase formatted sql

--changeset postgres:add_is_mfe_column_in_domain_instance_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-428 Add new column is_mfe to cs.core.domain_instance table


ALTER TABLE core.domain_instance 
 ADD COLUMN is_mfe boolean NOT NULL DEFAULT true

 --Revert Changes
 --rollback ALTER TABLE core.domain_instance DROP COLUMN is_mfe;

