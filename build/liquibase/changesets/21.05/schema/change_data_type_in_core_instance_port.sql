--liquibase formatted sql

--changeset postgres:change_data_type_in_core_instance_port context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-393 Change data type in core.instance.port


ALTER TABLE core.instance 
 ALTER COLUMN port TYPE integer USING port::integer;

 --Revert Changes
 --rollback ALTER TABLE core.instance ALTER COLUMN port TYPE varchar(50);