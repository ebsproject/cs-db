--liquibase formatted sql

--changeset postgres:create_printout_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-375 Create printout tables



CREATE TABLE core.printout_template
(
	name varchar(50) NULL,
	description varchar(500) NULL,
	zpl varchar(500) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('core."printout_template_id_seq"'::text)::regclass)
)
;

CREATE TABLE core.printout_template_product
(
	product_id integer NOT NULL,
	printout_template_id integer NOT NULL
)
;

CREATE TABLE core.printout_template_program
(
	program_id integer NOT NULL,
	printout_template_id integer NOT NULL
)
;

CREATE SEQUENCE core.printout_template_id_seq INCREMENT 1 START 1;

ALTER TABLE core.printout_template ADD CONSTRAINT "PK_printout_template"
	PRIMARY KEY (id)
;

ALTER TABLE core.printout_template_product ADD CONSTRAINT "PK_printout_template_product"
	PRIMARY KEY (product_id)
;

ALTER TABLE core.printout_template_program ADD CONSTRAINT "PK_printout_template_program"
	PRIMARY KEY (program_id,printout_template_id)
;

ALTER TABLE core.printout_template_product ADD CONSTRAINT "FK_printout_template_product_printout_template"
	FOREIGN KEY (printout_template_id) REFERENCES core.printout_template (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.printout_template_product ADD CONSTRAINT "FK_printout_template_product_product"
	FOREIGN KEY (product_id) REFERENCES core.product (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.printout_template_program ADD CONSTRAINT "FK_printout_template_program_printout_template"
	FOREIGN KEY (printout_template_id) REFERENCES core.printout_template (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.printout_template_program ADD CONSTRAINT "FK_printout_template_program_program"
	FOREIGN KEY (program_id) REFERENCES program.program (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN core.printout_template.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.printout_template.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.printout_template.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.printout_template.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.printout_template.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.printout_template.tenant_id
	IS 'Id of the selected Tenant'
;

--Revert Changes
--rollback DROP SEQUENCE core.printout_template_id_seq;
--rollback DROP TABLE core.printout_template_product;
--rollback DROP TABLE core.printout_template_program;
--rollback DROP TABLE core.printout_template;