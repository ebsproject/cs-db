--liquibase formatted sql

--changeset postgres:rename_column_in_core_domain_entity_ref context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-394 Rename column in core.domain_entity_reference



ALTER TABLE core.domain_entity_reference 
  DROP  CONSTRAINT "FK_domain_entity_reference_entity_reference";

ALTER TABLE core.domain_entity_reference 
  DROP  CONSTRAINT "PK_domain_entity_reference";

ALTER TABLE core.domain_entity_reference 
 RENAME COLUMN entityreference_id TO entity_reference_id;

ALTER TABLE core.domain_entity_reference ADD CONSTRAINT "FK_domain_entity_reference_entity_reference"
	FOREIGN KEY (entity_reference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.domain_entity_reference ADD CONSTRAINT "PK_domain_entity_reference"
	PRIMARY KEY (domain_id,entity_reference_id)
;

--Revert Changes
--rollback ALTER TABLE core.domain_entity_reference DROP  CONSTRAINT "FK_domain_entity_reference_entity_reference";
--rollback ALTER TABLE core.domain_entity_reference DROP  CONSTRAINT "PK_domain_entity_reference";
--rollback ALTER TABLE core.domain_entity_reference RENAME COLUMN entity_reference_id TO entityreference_id;
--rollback ALTER TABLE core.domain_entity_reference ADD CONSTRAINT "FK_domain_entity_reference_entity_reference" FOREIGN KEY (entityreference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action;
--rollback ALTER TABLE core.domain_entity_reference ADD CONSTRAINT "PK_domain_entity_reference"	PRIMARY KEY (domain_id,entityreference_id);