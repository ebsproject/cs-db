--liquibase formatted sql

--changeset postgres:rename_default_role_insecurity_user context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-402 Rename security.user.default_role to default_role_id.


ALTER TABLE security."user" 
 RENAME COLUMN default_role TO default_role_id;

ALTER TABLE security."user" ADD CONSTRAINT "FK_user_role"
	FOREIGN KEY (default_role_id) REFERENCES security.role (id) ON DELETE No Action ON UPDATE No Action;


--Revert Chnages
--rollback ALTER TABLE security."user" DROP CONSTRAINT "FK_user_role";
--rollback ALTER TABLE security."user" RENAME COLUMN default_role_id TO default_role;
