--liquibase formatted sql

--changeset postgres:set_attributes_not_null context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-374 Set not null default values in csdb


ALTER TABLE security."user" 
 DROP COLUMN IF EXISTS user_password;

ALTER TABLE core.domain 
 ALTER COLUMN htmltag_id SET NOT NULL;

ALTER TABLE core.domain 
 ALTER COLUMN info SET NOT NULL;

ALTER TABLE core.domain 
 ALTER COLUMN name SET NOT NULL;

ALTER TABLE core.instance 
 ALTER COLUMN name SET NOT NULL;

ALTER TABLE core.product 
 ALTER COLUMN description SET NOT NULL;

ALTER TABLE core.product 
 ALTER COLUMN help SET NOT NULL;

ALTER TABLE core.product 
 ALTER COLUMN htmltag_id SET NOT NULL;

ALTER TABLE core.product 
 ALTER COLUMN main_entity SET NOT NULL;

ALTER TABLE core.product 
 ALTER COLUMN menu_order SET NOT NULL;

ALTER TABLE core.product 
 ALTER COLUMN path SET NOT NULL;

ALTER TABLE security."user" 
 ALTER COLUMN user_name SET NOT NULL;

 --Revert Changes
 --rollback ALTER TABLE security."user" ADD COLUMN user_password varchar(50);
 --rollback ALTER TABLE core.domain ALTER COLUMN htmltag_id DROP NOT NULL;
 --rollback ALTER TABLE core.domain ALTER COLUMN info DROP NOT NULL;
 --rollback ALTER TABLE core.domain ALTER COLUMN name DROP NOT NULL;
 --rollback ALTER TABLE core.instance ALTER COLUMN name DROP NOT NULL;
 --rollback ALTER TABLE core.product ALTER COLUMN description DROP NOT NULL;
 --rollback ALTER TABLE core.product ALTER COLUMN help DROP NOT NULL;
 --rollback ALTER TABLE core.product ALTER COLUMN htmltag_id DROP NOT NULL;
 --rollback ALTER TABLE core.product ALTER COLUMN main_entity DROP NOT NULL;
 --rollback ALTER TABLE core.product ALTER COLUMN menu_order DROP NOT NULL;
 --rollback ALTER TABLE core.product ALTER COLUMN path DROP NOT NULL;
 --rollback ALTER TABLE security."user" ALTER COLUMN user_name DROP NOT NULL;
