--liquibase formatted sql

--changeset postgres:set_attributes_not_null context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-374 Set not null default values in csdb


ALTER TABLE core.printout_template 
 ALTER COLUMN description SET NOT NULL;

 ALTER TABLE core.printout_template 
 ALTER COLUMN name SET NOT NULL;


 --Revert Changes
 --rollback ALTER TABLE core.printout_template ALTER COLUMN description DROP NOT NULL;
 --rollback   ALTER TABLE core.printout_template ALTER COLUMN name DROP NOT NULL;