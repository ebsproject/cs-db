--liquibase formatted sql

--changeset postgres:update_pk_in_core.printout_template_product context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-429 Update primary key in core.printout_template_product



ALTER TABLE core.printout_template_product DROP CONSTRAINT "PK_printout_template_product";
ALTER TABLE core.printout_template_product ADD CONSTRAINT "PK_printout_template_product" PRIMARY KEY (product_id,printout_template_id);


--Revert Changes
--rollback ALTER TABLE core.printout_template_product DROP CONSTRAINT "PK_printout_template_product";
--rollback ALTER TABLE core.printout_template_product ADD CONSTRAINT "PK_printout_template_product" PRIMARY KEY (product_id);