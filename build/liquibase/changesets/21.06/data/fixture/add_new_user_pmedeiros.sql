--liquibase formatted sql

--changeset postgres:add_new_user_pmedeiros context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-541 Add new user in csdb


INSERT INTO "security"."user"
(id, user_name, last_access, default_role_id, is_is, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, person_id)
VALUES
(69, 'P.MEDEIROS@cimmyt.onmicrosoft.com', null, 2, 1, now(), null, 1, null, false, 30);


INSERT INTO "security".functional_unit_user(user_id, functional_unit_id) VALUES
(69, 1);

INSERT INTO "security".user_role
(role_id, user_id)
VALUES
(2, 69);

INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(69, 1);
INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(69, 2);


SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));
SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));



--Revert Changes
--rollback DELETE FROM "security".tenant_user WHERE user_id=69 and tenant_id=1;
--rollback DELETE FROM "security".tenant_user WHERE user_id=69 and tenant_id=2;

--rollback DELETE FROM "security".user_role where role_id = 2 and user_id = 69;
--rollback DELETE FROM "security".functional_unit_user where user_id = 69 and functional_unit_id = 1;
--rollback DELETE FROM "security"."user" where id = 69;