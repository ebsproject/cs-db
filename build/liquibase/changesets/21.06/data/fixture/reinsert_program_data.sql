--liquibase formatted sql

--changeset postgres:reinsert_program_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-390 Create Mapping between Program in CS and CB


--Revert Changes
--rollback UPDATE "program"."program" SET crop_id = 2 WHERE id=1;
--rollback UPDATE "program"."program" SET crop_id = 1 WHERE id=2;
--rollback UPDATE "program"."program" SET crop_id = 2 WHERE id=3;
--rollback UPDATE "program"."program" SET crop_id = 1 WHERE id=4;