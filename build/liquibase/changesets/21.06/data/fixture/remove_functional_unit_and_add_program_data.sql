--liquibase formatted sql

--changeset postgres:remove_functional_unit_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-390 Create Mapping between Program in CS and CB



DELETE FROM "security".functional_unit_user;
DELETE FROM "security".functional_unit;
DELETE FROM "program"."program";
DELETE FROM "program".crop;


INSERT INTO "program".crop
(id, code, "name", description,  creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes)
VALUES(1, 'RICE', 'Rice', 'Rice crop', 1, '2021-05-14 01:50:45.767', NULL, NULL, false, NULL);
INSERT INTO "program".crop
(id, code, "name", description,  creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes)
VALUES(2, 'WHEAT', 'Wheat', 'Wheat crop', 1, '2021-05-14 02:05:53.513', NULL, NULL, false, NULL);
INSERT INTO "program".crop
(id, code, "name", description,  creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes)
VALUES(3, 'MAIZE', 'Maize', 'Maize crop', 1, '2021-05-14 02:05:53.513', NULL, NULL, false, NULL);
INSERT INTO "program".crop
(id, code, "name", description,  creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes)
VALUES(4, 'BARLEY', 'Barley', 'Barley crop', 1, '2021-05-14 02:08:22.042', NULL, NULL, false, NULL);


INSERT INTO "program".crop_program
(id, code, "name", description, organization_id, crop_id, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, tenant_id)
VALUES(1, 'RICE_PROG', 'Rice program', 'Rice crop', NULL, 1, 1, '2021-05-14 01:50:45.774', NULL, NULL, false, NULL, 2);
INSERT INTO "program".crop_program
(id, code, "name", description, organization_id, crop_id, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, tenant_id)
VALUES(2, 'GWP', 'Global Wheat Program', 'Wheat crop program', NULL, 2, 1, '2021-05-14 02:05:53.521', NULL, NULL, false, NULL, 1);
INSERT INTO "program".crop_program
(id, code, "name", description, organization_id, crop_id, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, tenant_id)
VALUES(3, 'GMP', 'Global Maize Program', 'Maize crop program', NULL, 3, 1, '2021-05-14 02:05:53.521', NULL, NULL, false, NULL, 1);
INSERT INTO "program".crop_program
(id, code, "name", description, organization_id, crop_id, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, tenant_id)
VALUES(4, 'BTCP', 'Barley Test Crop Program', NULL, NULL, 4, 1, '2021-05-14 02:08:22.074', NULL, NULL, false, NULL, 1);


INSERT INTO "program"."program"
(id, code, "name", "type", status, description, crop_program_id, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, tenant_id, owner_id)
VALUES(101, 'IRSEA', 'Irrigated South-East Asia', 'breeding', 'active', 'Breeding Irrigated Rice for South East Asia', 1, 1, '2021-05-14 01:50:45.795', NULL, NULL, false, NULL, 1, 1);
INSERT INTO "program"."program"
(id, code, "name", "type", status, description, crop_program_id, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, tenant_id, owner_id)
VALUES(102, 'BW', 'BW Wheat Breeding Program', 'breeding', 'active', 'BW Wheat Breeding Program', 2, 1, '2021-05-14 02:05:53.535', NULL, '2021-05-14 02:05:56.909', false, NULL, 1, 1);
INSERT INTO "program"."program"
(id, code, "name", "type", status, description, crop_program_id, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, tenant_id, owner_id)
VALUES(103, 'KE', 'KE Maize Breeding Program', 'breeding', 'active', 'KE Maize Breeding Program', 3, 1, '2021-05-14 02:05:53.535', NULL, '2021-05-14 02:05:56.909', false, NULL, 1, 1);
INSERT INTO "program"."program"
(id, code, "name", "type", status, description, crop_program_id, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, tenant_id, owner_id)
VALUES(104, 'BTP', 'Barley Test Program', 'breeding', 'active', NULL, 4, 1, '2021-05-14 02:08:22.097', NULL, NULL, false, NULL, 1, 1);


SELECT setval('"program".crop_program_id_seq', (SELECT MAX(id) FROM "program".crop_program));
SELECT setval('"program"."program_id_seq"', (SELECT MAX(id) FROM "program"."program"));

--Revert Changes
--rollback DELETE FROM "program"."program";
--rollback DELETE FROM "program".crop_program;
--rollback DELETE FROM "program".crop;

--rollback INSERT INTO "program".crop (code, "name", description, notes, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id) VALUES('M', 'Maize', 'Maize', NULL, '2021-02-11 18:14:12.478', NULL, 1, NULL, false, 1);
--rollback INSERT INTO "program".crop (code, "name", description, notes, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id) VALUES('W', 'Wheat', 'Wheat', NULL, '2021-02-11 18:14:12.478', NULL, 1, NULL, false, 2);
--rollback INSERT INTO "program".crop (code, "name", description, notes, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id) VALUES('R', 'Rice', 'Rice', NULL, '2021-02-11 18:14:12.478', NULL, 1, NULL, false, 3);

--rollback INSERT INTO "program"."program" (code, "name", "type", status, description, notes, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, tenant_id, owner_id, crop_program_id) VALUES('GWP', 'Global Wheat Program', 'Breeding Program', 'Active', 'Global Wheat Program', '', '2021-06-27 23:58:29.667', NULL, 1, NULL, false, 1, 1, 1, NULL);
--rollback INSERT INTO "program"."program" (code, "name", "type", status, description, notes, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, tenant_id, owner_id, crop_program_id) VALUES('GMP', 'Global Maize Program', 'Breeding Program', 'Active', 'Global Maize Program', NULL, '2021-02-11 18:14:29.004', NULL, 1, NULL, false, 2, 1, 1, NULL);
--rollback INSERT INTO "program"."program" (code, "name", "type", status, description, notes, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, tenant_id, owner_id, crop_program_id) VALUES('DW', 'Durum Wheat', 'Breeding Program', 'Active', 'Durum Wheat', NULL, '2021-02-11 18:14:29.004', NULL, 1, NULL, false, 3, 1, 1, NULL);
--rollback INSERT INTO "program"."program" (code, "name", "type", status, description, notes, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, tenant_id, owner_id, crop_program_id) VALUES('AP', 'Admin Program', 'Admin Program', 'Avtive', 'Administrative Program', 'Admin Program', '2021-03-30 14:03:10.040', NULL, 1, NULL, false, 4, 1, 1, NULL);

--rollback INSERT INTO "security".functional_unit (description, notes, "type", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, code, "name", parent_func_unit_id, program_id) VALUES('Administrator', 'Administrator Team', 'Team', 1, '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 1, 'ADM', 'Administrator', NULL, 4);

--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(1, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(2, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(3, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(4, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(5, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(6, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(7, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(8, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(9, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(10, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(11, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(12, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(13, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(14, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(15, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(16, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(17, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(18, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(19, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(20, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(21, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(22, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(23, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(24, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(25, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(26, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(27, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(28, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(29, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(30, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(31, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(32, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(33, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(34, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(35, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(36, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(37, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(38, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(39, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(40, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(41, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(42, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(43, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(44, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(45, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(46, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(47, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(48, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(49, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(50, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(51, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(52, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(53, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(54, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(55, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(56, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(57, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(58, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(59, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(60, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(61, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(62, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(63, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(64, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(65, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(66, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(67, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(68, 1);
--rollback INSERT INTO "security".functional_unit_user (user_id, functional_unit_id) VALUES(69, 1);

--rollback  SELECT setval('"program".crop_program_id_seq', (SELECT MAX(id) FROM "program".crop_program));
--rollback  SELECT setval('"program".program_id_seq', (SELECT MAX(id) FROM "program"."program"));
--rollback  SELECT setval('"security".functional_unit_id_seq', (SELECT MAX(id) FROM "security".functional_unit));