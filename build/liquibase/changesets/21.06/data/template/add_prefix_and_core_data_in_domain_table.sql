--liquibase formatted sql

--changeset postgres:add_prefix_and_core_data_in_domain_table context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-417 Add ARM path in cs.Product


UPDATE core."domain"
SET prefix = 'cb', is_core = true
WHERE id=1;

UPDATE core."domain"
SET prefix = 'cs', is_core = true
WHERE id=2;

UPDATE core."domain"
SET prefix = 'ba'
WHERE id=3;

UPDATE core."domain"
SET prefix = 'sm'
WHERE id=4;


--Revert Changes
--rollback UPDATE core."domain" SET prefix = null, is_core = false WHERE id=1;
--rollback UPDATE core."domain" SET prefix = null, is_core = false WHERE id=2;
--rollback UPDATE core."domain" SET prefix = null WHERE id=3;
--rollback UPDATE core."domain" SET prefix = null WHERE id=4;
