--liquibase formatted sql

--changeset postgres:add_constraint_in_user_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-447 Add constraint in user table


ALTER TABLE security."user" 
  ADD CONSTRAINT "UQ_user_name" UNIQUE (user_name);


--Revert CHanges
--rollback ALTER TABLE security."user" DROP CONSTRAINT "UQ_user_name";
