--liquibase formatted sql

--changeset postgres:add_prefix_and_core_column_in_domain_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-417 Add two new columns in domain table


ALTER TABLE core.domain 
 ADD COLUMN is_core boolean NOT NULL DEFAULT false;

ALTER TABLE core.domain 
 ADD COLUMN prefix varchar(5) NULL;

COMMENT ON COLUMN core.domain.is_core
	IS 'Indicates if it belongs to core'
;

COMMENT ON COLUMN core.domain.info
	IS 'Additional information about the component.'
;

COMMENT ON COLUMN core.domain.name
	IS 'Name of the component.'
;

COMMENT ON COLUMN core.domain.prefix
	IS 'Indicate the abbreviation of the domain'
;


--Revert Changes
--rollback ALTER TABLE core.domain DROP COLUMN is_core;
--rollback ALTER TABLE core.domain DROP COLUMN prefix;