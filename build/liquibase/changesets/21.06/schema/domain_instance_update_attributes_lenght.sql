--liquibase formatted sql

--changeset postgres:domain_instance_update_attributes_lenght context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-385 Review domain_instance_attributes



ALTER TABLE core.domain_instance 
 DROP COLUMN IF EXISTS is_ssl;

ALTER TABLE core.domain_instance 
 ALTER COLUMN context TYPE varchar(500);

ALTER TABLE core.domain_instance 
 ALTER COLUMN sg_context TYPE varchar(1000);


--Revert Changes

--rollback ALTER TABLE core.domain_instance ADD COLUMN is_ssl boolean;
--rollback ALTER TABLE core.domain_instance ALTER COLUMN context TYPE varchar(150);
--rollback ALTER TABLE core.domain_instance ALTER COLUMN sg_context TYPE varchar(500);