--liquibase formatted sql

--changeset postgres:set_crop_program_id_not_null context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-390 Create Mapping between Program in CS and CB 

ALTER TABLE "program"."program" 
 ALTER COLUMN crop_program_id SET NOT NULL;


--Revert Changes
--rollback  ALTER TABLE "program"."program" ALTER COLUMN crop_program_id DROP NOT NULL;