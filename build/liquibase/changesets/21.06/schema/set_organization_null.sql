--liquibase formatted sql

--changeset postgres:set_organization_null context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-390 Create Mapping between Program in CS and CB 



ALTER TABLE "program".crop_program 
 ALTER COLUMN organization_id DROP NOT NULL;


 --Revert Changes
 --rollback ALTER TABLE "program".crop_program   ALTER COLUMN organization_id SET NOT NULL;
