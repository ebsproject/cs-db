--liquibase formatted sql

--changeset postgres:set_prefix_not_null_in_domain_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-417 Add two new columns in domain table


ALTER TABLE core.domain 
 ALTER COLUMN prefix SET NOT NULL;


 --Revert Changes
 --rollback ALTER TABLE core.domain ALTER COLUMN prefix DROP NOT NULL;