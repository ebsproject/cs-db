--liquibase formatted sql

--changeset postgres:update_instance_port_and_server context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-386 Review instance_attributes



ALTER TABLE core.instance 
 ALTER COLUMN port SET NOT NULL;

ALTER TABLE core.instance 
 ALTER COLUMN server TYPE varchar(300);

 --Revert Changes
 --rollback ALTER TABLE core.instance ALTER COLUMN port DROP NOT NULL;
 --rollback ALTER TABLE core.instance ALTER COLUMN server TYPE varchar(50);