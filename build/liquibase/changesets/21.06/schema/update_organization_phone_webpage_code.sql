--liquibase formatted sql

--changeset postgres:update_organization_phone_webpage_code context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-387 Review organization attributes


ALTER TABLE core.organization 
 ALTER COLUMN phone TYPE varchar(50) USING phone::varchar;

ALTER TABLE core.organization 
 ALTER COLUMN web_page TYPE varchar(300);

ALTER TABLE core.organization 
  ADD CONSTRAINT "UQ_code" UNIQUE (code);



--Revert Changes
--rollback ALTER TABLE core.organization ALTER COLUMN phone TYPE int8 USING phone::int8;
--rollback ALTER TABLE core.organization ALTER COLUMN web_page TYPE varchar(200);
--rollback ALTER TABLE core.organization DROP CONSTRAINT "UQ_code";