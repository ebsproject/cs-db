--liquibase formatted sql

--changeset postgres:update_person_and_user context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-391 Update_person_and_user_tables

ALTER TABLE crm.person 
 ALTER COLUMN email TYPE varchar(128);

ALTER TABLE crm.person 
 ALTER COLUMN family_name TYPE varchar(128);

ALTER TABLE crm.person 
 ALTER COLUMN given_name TYPE varchar(128);

 ALTER TABLE crm.person
 ALTER COLUMN gender DROP NOT NULL;

ALTER TABLE security."user" 
 ALTER COLUMN user_name TYPE varchar(128);


--Revert Changes
--rollback ALTER TABLE crm.person ALTER COLUMN email TYPE varchar(50);
--rollback ALTER TABLE crm.person ALTER COLUMN family_name TYPE varchar(50);
--rollback ALTER TABLE crm.person ALTER COLUMN given_name TYPE varchar(50);
--rollback  ALTER TABLE crm.person ALTER COLUMN gender SET NOT NULL;
--rollback ALTER TABLE security."user" ALTER COLUMN user_name TYPE varchar(50);