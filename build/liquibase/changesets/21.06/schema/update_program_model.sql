--liquibase formatted sql

--changeset postgres:update_program_model context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-390 Create Mapping between Program in CS and CB 



ALTER TABLE program.program 
  DROP  CONSTRAINT "FK_program_crop";

ALTER TABLE program.program 
 DROP COLUMN IF EXISTS crop_id;

ALTER TABLE core.organization 
 ALTER COLUMN code TYPE varchar(64);

ALTER TABLE core.organization 
 ALTER COLUMN name TYPE varchar(128);

ALTER TABLE core.organization 
 ALTER COLUMN phone DROP NOT NULL;

ALTER TABLE program.program 
 ALTER COLUMN code SET NOT NULL;

ALTER TABLE program.program 
 ALTER COLUMN code TYPE varchar(64);

ALTER TABLE program.program 
 ALTER COLUMN description TYPE text;

ALTER TABLE program.program 
 ALTER COLUMN name TYPE varchar(128);

ALTER TABLE program.program 
 ALTER COLUMN name SET NOT NULL;

ALTER TABLE program.program 
 ALTER COLUMN status TYPE varchar(64);

ALTER TABLE program.program 
 ALTER COLUMN status SET NOT NULL;

ALTER TABLE program.program 
 ALTER COLUMN type TYPE varchar(64);

ALTER TABLE program.program 
 ALTER COLUMN type SET NOT NULL;

ALTER TABLE core.organization 
 ADD COLUMN description text NULL;

ALTER TABLE program.program 
 ADD COLUMN crop_program_id integer NULL;

CREATE TABLE program.crop_program
(
	id integer NOT NULL   DEFAULT NEXTVAL(('program."crop_program_id_seq"'::text)::regclass),
	code varchar(64) NOT NULL,	-- Crop Program Code: Textual identifier of the crop program.
	name varchar(128) NOT NULL,	-- Crop Program Name: Full name of the crop program.
	description text NULL,	-- Crop Program Description: Additional information about the crop program.
	notes varchar(50) NULL,	-- Notes: Technical details about the record.
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	crop_id integer NOT NULL,
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	organization_id integer NOT NULL
)
;

CREATE SEQUENCE program.crop_program_id_seq INCREMENT 1 START 1;

ALTER TABLE program.crop_program ADD CONSTRAINT "PK_crop_program"
	PRIMARY KEY (id)
;

ALTER TABLE program.crop_program ADD CONSTRAINT "FK_crop_program_crop"
	FOREIGN KEY (crop_id) REFERENCES program.crop (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE program.crop_program ADD CONSTRAINT "FK_crop_program_organization"
	FOREIGN KEY (organization_id) REFERENCES core.organization (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE program.program ADD CONSTRAINT "FK_program_crop_program"
	FOREIGN KEY (crop_program_id) REFERENCES program.crop_program (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON TABLE program.crop_program
	IS 'Crop Program: Crop-specific objectives of a program'
;

COMMENT ON TABLE program.program
	IS 'Program: Entity that conducts defined objectives for the production of the next generation of crops with desired characteristics'
;

COMMENT ON COLUMN core.organization.code
	IS 'Unique identifier code for the Organization.'
;

COMMENT ON COLUMN core.organization.description
	IS 'Additional information about the organization.'
;

COMMENT ON COLUMN core.organization.is_active
	IS 'Specifies the status of the Organization within the system, it can be active or not.'
;

COMMENT ON COLUMN core.organization.legal_name
	IS 'Official name of the Organization.'
;

COMMENT ON COLUMN core.organization.name
	IS 'Short name of the organization.'
;

COMMENT ON COLUMN core.organization.phone
	IS 'Official phone number of the organization.'
;

COMMENT ON COLUMN core.organization.slogan
	IS 'Slogan of the Organization.'
;

COMMENT ON COLUMN core.organization.web_page
	IS 'Official web site of the Organization.'
;

COMMENT ON COLUMN program.crop_program.code
	IS 'Crop Program Code: Textual identifier of the crop program.'
;

COMMENT ON COLUMN program.crop_program.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN program.crop_program.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN program.crop_program.description
	IS 'Crop Program Description: Additional information about the crop program.'
;

COMMENT ON COLUMN program.crop_program.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN program.crop_program.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN program.crop_program.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN program.crop_program.name
	IS 'Crop Program Name: Full name of the crop program.'
;

COMMENT ON COLUMN program.crop_program.notes
	IS 'Notes: Technical details about the record.'
;

COMMENT ON COLUMN program.crop_program.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN program.program.code
	IS 'Program Code: Textual identifier of the program.'
;

COMMENT ON COLUMN program.program.description
	IS 'Program Description: Additional information about the program.'
;

COMMENT ON COLUMN program.program.name
	IS 'Program Name: Full name of the program.'
;

COMMENT ON COLUMN program.program.notes
	IS 'Notes: Technical details about the record.'
;

COMMENT ON COLUMN program.program.status
	IS 'Program Status: Status of the program.'
;

COMMENT ON COLUMN program.program.type
	IS 'Program Type: Type of the program {breeding, molecular, seed health, etc.}'
;


--Revert Changes
--rollback ALTER TABLE "program"."program" ADD COLUMN crop_id int4 NULL;

--rollback ALTER TABLE "program"."program" ADD CONSTRAINT "FK_program_crop" FOREIGN KEY (crop_id) REFERENCES program.crop(id);
--rollback ALTER TABLE core.organization ALTER COLUMN code TYPE varchar(50);
--rollback ALTER TABLE core.organization ALTER COLUMN name TYPE varchar(200);
--rollback ALTER TABLE core.organization ALTER COLUMN phone SET NOT NULL;
--rollback ALTER TABLE program.program ALTER COLUMN code DROP NOT NULL;
--rollback ALTER TABLE program.program ALTER COLUMN code TYPE varchar(50);
--rollback ALTER TABLE program.program ALTER COLUMN description TYPE varchar(50);
--rollback ALTER TABLE program.program ALTER COLUMN name TYPE varchar(50);
--rollback ALTER TABLE program.program ALTER COLUMN name DROP NOT NULL;
--rollback ALTER TABLE program.program ALTER COLUMN status TYPE varchar(50);
--rollback ALTER TABLE program.program ALTER COLUMN status DROP NOT NULL;
--rollback ALTER TABLE program.program ALTER COLUMN type TYPE varchar(50);
--rollback ALTER TABLE program.program ALTER COLUMN type DROP NOT NULL;
--rollback ALTER TABLE core.organization DROP COLUMN description;

--rollback ALTER TABLE program.crop_program DROP CONSTRAINT "FK_crop_program_crop";
--rollback ALTER TABLE program.crop_program DROP CONSTRAINT "FK_crop_program_organization";
--rollback ALTER TABLE program.program DROP CONSTRAINT "FK_program_crop_program";

--rollback ALTER TABLE program.program DROP COLUMN crop_program_id;

--rollback DROP TABLE program.crop_program;

--rollback DROP SEQUENCE program.crop_program_id_seq;