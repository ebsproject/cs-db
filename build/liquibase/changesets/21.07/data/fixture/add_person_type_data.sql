--liquibase formatted sql

--changeset postgres:add_person_type_data context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-465 Add person_type data



INSERT INTO crm."type"
("type", contact_point, remarks, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Admin', 'Admin', 'Admin', 1, now(), NULL, 1, NULL, false, 1);
INSERT INTO crm."type"
("type", contact_point, remarks, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Person', 'Person', 'Person', 1, now(), NULL, 1, NULL, false, 2);
INSERT INTO crm."type"
("type", contact_point, remarks, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('User', 'User', 'User', 1, now(), NULL, 1, NULL, false, 3);
INSERT INTO crm."type"
("type", contact_point, remarks, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Donor', 'Donor', 'Donor', 1, now(), NULL, 1, NULL, false, 4);
INSERT INTO crm."type"
("type", contact_point, remarks, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Collaborator', 'Collaborator', 'Collaborator', 1, now(), NULL, 1, NULL, false, 5);

SELECT setval('crm.type_id_seq', (SELECT MAX(id) FROM crm."type"));

--Revert Changes
--rollback DELETE FROM crm."type";