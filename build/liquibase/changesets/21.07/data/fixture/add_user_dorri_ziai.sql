--liquibase formatted sql

--changeset postgres:add_user_dorri_ziai context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-564 Add user Dorri Ziai to csdb



SET session_replication_role = 'replica';

INSERT INTO crm.person
(id, given_name, family_name, additional_name, email, official_email, gender, has_credential, job_title, knows_about, phone, person_status_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, tenant_id, language_id)
VALUES
(67, 'Dorri', 'Ziai', null, 'daz28@cornell.edu', 'daz28@cornell.edu', 'female', true, 'UX/UI Designer', 'IT', '5959521900', 1, now(), null, 1, null, false, 1, 1);


INSERT INTO "security"."user"
(id, user_name, last_access, default_role_id, is_is, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, person_id)
VALUES
(70, 'daz28@cornell.edu', null, 2, 1, now(), null, 1, null, false, 67);

INSERT INTO "security".user_role
(role_id, user_id)
VALUES
(2, 70);

INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(70, 1);
INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(70, 2);


SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));
SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));

SET session_replication_role = 'origin';



--Revert Changes
--rollback DELETE FROM "security".tenant_user WHERE user_id=70 and tenant_id=1;
--rollback DELETE FROM "security".tenant_user WHERE user_id=70 and tenant_id=2;

--rollback DELETE FROM "security".user_role where role_id = 2 and user_id = 70;
--rollback DELETE FROM "security"."user" where id = 70;
--rollback DELETE FROM crm.person WHERE id = 67;