--liquibase formatted sql

--changeset postgres:add_default_address_type context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-466 Add default address type in Address table



INSERT INTO crm.address
("location", region, zip_code, street_address, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, "type")
VALUES('Default', 'Default', 'Default', 'Default', 1, '2021-07-07 13:50:47.991', NULL, 1, NULL, false, 1, 'Default');

SELECT setval('crm.address_id_seq', (SELECT MAX(id) FROM crm.address));


--Revert Changes
--rollback DELETE FROM crm.address WHERE id=1;