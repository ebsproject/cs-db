--liquibase formatted sql

--changeset postgres:change_arm_label context:template splitStatements:false rollbackSplitStatements:false
--comment: BA-551 Change ARM Label to Analysis Request Manager



UPDATE core.product
SET name='Analysis Request Manager', description='Analysis Request Manager', help='Analysis Request Manager' 
WHERE id=12;


--Revert Changes
--rollback UPDATE core.product SET name = 'Analytical Request Manager', description='Analytical Request Manager', help='Analytical Request Manager' WHERE id=12;