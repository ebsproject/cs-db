--liquibase formatted sql

--changeset postgres:add_column_type_in_address_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-467 Add new column "type" in Address table


ALTER TABLE crm.address 
 ADD COLUMN type varchar(50) NULL;

 COMMENT ON COLUMN crm.address.type IS 'Defines the type of address, e.g. default, office, other, etc.';


 --Revert Chages
 --rollback ALTER TABLE crm.address DROP COLUMN type;