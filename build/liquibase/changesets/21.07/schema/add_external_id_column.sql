--liquibase formatted sql

--changeset postgres:add_external_id_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-468 Add external_id column in program



ALTER TABLE "program"."program" ADD COLUMN external_id int4 NULL;
ALTER TABLE "program"."program" ADD CONSTRAINT "UQ_external_id" UNIQUE (external_id);

--comment: CS-469 Add external_id column in User

ALTER TABLE security."user" ADD COLUMN external_id int4 NULL;
ALTER TABLE security."user" ADD CONSTRAINT "UQ_external_id" UNIQUE (external_id);


--comment: CS-470 Add external_id column in Person

ALTER TABLE crm.person ADD COLUMN external_id int4 NULL;
ALTER TABLE crm.person ADD CONSTRAINT "UQ_external_id" UNIQUE (external_id);


--Revert Changes
--rollback ALTER TABLE security."user" DROP CONSTRAINT "UQ_external_id";
--rollback ALTER TABLE "program"."program" DROP CONSTRAINT "UQ_external_id";
--rollback ALTER TABLE crm.person DROP CONSTRAINT "UQ_external_id";
--rollback ALTER TABLE "program"."program" DROP COLUMN external_id;
--rollback ALTER TABLE security.user DROP COLUMN external_id;
--rollback ALTER TABLE crm.person DROP COLUMN external_id;
