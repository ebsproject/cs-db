--liquibase formatted sql

--changeset postgres:update_schema_in_person_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-465 update schema in person sequence



ALTER TABLE crm.person ALTER COLUMN id SET DEFAULT nextval('crm."person_id_seq"'::text::regclass);


--Revert Changes
--rollback ALTER TABLE crm.person ALTER COLUMN id SET DEFAULT nextval('security."person_id_seq"'::text::regclass);