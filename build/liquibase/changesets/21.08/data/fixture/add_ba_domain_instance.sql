--liquibase formatted sql

--changeset postgres:add_ba_domain_instance context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-640 Add BA in domain_instance table



SET session_replication_role = 'replica';

INSERT INTO core.domain_instance
(instance_id, domain_id, context, creation_timestamp, creator_id, id, is_void, modification_timestamp, modifier_id, tenant_id, sg_context, is_mfe)
VALUES(1, 3, 'https://dev.ebsproject.org', now(), 1, 6, false, NULL, NULL, 1, 'https://cbapi-dev.ebsproject.org', true);

SELECT setval('core.domain_instance_id_seq', (SELECT MAX(id) FROM core.domain_instance));

SET session_replication_role = 'origin';



--Revert Changes
--rollback DELETE FROM core.domain_instance where id = 6;