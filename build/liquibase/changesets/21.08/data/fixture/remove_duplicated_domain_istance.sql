--liquibase formatted sql

--changeset postgres:remove_duplicated_domain_istance context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-623 Remove duplicated domain_instace



DELETE FROM core.domain_instance
WHERE id=2;

DELETE FROM core.domain_instance
WHERE id=3;


--Revert Changes
--rollback INSERT INTO core.domain_instance (instance_id, domain_id, context, creation_timestamp, creator_id, id, is_void, modification_timestamp, modifier_id, tenant_id, sg_context, is_mfe) VALUES(1, 1, 'https://cb-dev.ebsproject.org', '2021-08-19 00:29:36.339', 1, 2, false, NULL, NULL, 1, 'https://cbapi-dev.ebsproject.org', false);
--rollback INSERT INTO core.domain_instance (instance_id, domain_id, context, creation_timestamp, creator_id, id, is_void, modification_timestamp, modifier_id, tenant_id, sg_context, is_mfe) VALUES(1, 1, 'https://cb-dev.ebsproject.org', '2021-08-19 00:29:36.339', 1, 3, false, NULL, NULL, 1, 'https://cbapi-dev.ebsproject.org', false);
