--liquibase formatted sql

--changeset postgres:update_instance_id_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-623 Update data



UPDATE core."instance"
SET tenant_id=1, "name"='Default', notes='Default instace'
WHERE id=1;

UPDATE core.domain_instance
SET instance_id=1;

DELETE FROM core.instance 
WHERE id = 2;

DELETE FROM core.instance 
WHERE id = 3;

DELETE FROM "security".tenant_user WHERE tenant_id = 2;



--Revert Changes
--rollback UPDATE core."instance" SET tenant_id=1, "name"='CIMMYT-CB-MAIZE', notes='Tenant 1 - CB Maize' WHERE id=1;
--rollback INSERT INTO core."instance" ("server", port, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, tenant_id, "name", notes) VALUES('172.18.8.1', 22, '2021-07-12 13:37:33.460', NULL, 1, NULL, false, 2, 1, 'CIMMYT-CB-WHEAT', 'Tenant 1 - CB Wheat');
--rollback INSERT INTO core."instance" ("server", port, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, tenant_id, "name", notes) VALUES('172.18.8.1', 22, '2021-07-12 13:37:33.460', NULL, 1, NULL, false, 3, 2, 'IRRI-CB-RICE', 'Tenant 2 - CB Rice');

--rollback UPDATE core.domain_instance SET instance_id=2 WHERE id=4;
--rollback UPDATE core.domain_instance SET instance_id=1 WHERE id=5;
--rollback UPDATE core.domain_instance SET instance_id=1 WHERE id=1;
--rollback UPDATE core.domain_instance SET instance_id=2 WHERE id=2;
--rollback UPDATE core.domain_instance SET instance_id=3 WHERE id=3;

--rollback INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES (1,2), (2,2), (3,2), (4,2), (5,2), (6,2), (7,2), (8,2), (9,2), (10,2), (11,2), (12,2), (13,2), (14,2), (15,2), (16,2), (17,2), (18,2), (19,2), (20,2), (21,2), (22,2), (23,2), (24,2), (25,2), (26,2), (27,2), (28,2), (29,2), (30,2), (31,2), (32,2), (33,2), (34,2), (35,2), (36,2), (37,2), (38,2), (39,2), (40,2), (41,2), (42,2), (43,2), (44,2), (45,2), (46,2), (47,2), (48,2), (49,2),  (50,2), (51,2), (52,2), (53,2), (54,2), (55,2), (56,2), (57,2), (58,2), (59,2), (60,2), (61,2), (62,2), (63,2), (64,2), (65,2), (66,2), (68,2),  (69,2), (70,2);