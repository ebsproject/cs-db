--liquibase formatted sql

--changeset postgres:update_tenant_id_in_domain_instance context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-623 Update tenant_id in domain_instance



UPDATE core.domain_instance
SET tenant_id=1 WHERE id=3;



--Revert Changes
--rollback UPDATE core.domain_instance SET tenant_id=2 WHERE id=3;

