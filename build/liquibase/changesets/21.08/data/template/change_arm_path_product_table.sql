--liquibase formatted sql

--changeset postgres:change_arm_path_product_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-641 Change arm path in CSDB Products table


UPDATE core.product
SET "path"='/ba/list'
WHERE id=12;



--Revert Changes
--rollback UPDATE core.product SET "path"='/arm/add' WHERE id=12;

