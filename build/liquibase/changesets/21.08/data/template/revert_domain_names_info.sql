--liquibase formatted sql

--changeset postgres:revert_domain_names_info context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-642 Revert domain_names in CSDB



UPDATE core."domain" SET "name"='Breeding Analytics' WHERE id=3;
UPDATE core."domain" SET "name"='Service Management'WHERE id=4;


--Revert CHanges
--rollback UPDATE core."domain" SET "name"='Breeding Services' WHERE id=3;
--rollback UPDATE core."domain" SET "name"='Breeding Services'WHERE id=4;
