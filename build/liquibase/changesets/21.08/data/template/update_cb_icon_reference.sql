--liquibase formatted sql

--changeset postgres:update_cb_icon_reference context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-633 Update Core Breeding icon reference in CSDB



UPDATE core."domain"
SET icon='ExperimentCreation.svg'
WHERE id=1;


--Revert Changes
--rollback UPDATE core."domain" SET icon='FieldImplementation.svg' WHERE id=1;