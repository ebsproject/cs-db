--liquibase formatted sql

--changeset postgres:update_domain_name_info context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-624 Update Domain names and info in CSDB



UPDATE core."domain" SET info='Breeding Management' WHERE id=1;
UPDATE core."domain" SET info='System Administrator' WHERE id=2;
UPDATE core."domain" SET "name"='Breeding Services' WHERE id=3;
UPDATE core."domain" SET "name"='Breeding Services'WHERE id=4;


--Revert CHanges
--rollback UPDATE core."domain" SET info='Core Breeding' WHERE id=1;
--rollback UPDATE core."domain" SET info='Core System' WHERE id=2;
--rollback UPDATE core."domain" SET "name"='Breeding Analytics' WHERE id=3;
--rollback UPDATE core."domain" SET "name"='Service Management'WHERE id=4;
