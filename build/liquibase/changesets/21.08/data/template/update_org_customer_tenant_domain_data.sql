--liquibase formatted sql

--changeset postgres:update_org_customer_tenant_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-623 Update organization, customer and tenant



INSERT INTO core.customer
("name", phone, official_email, alternate_email, job_title, language_preference, phone_extension, is_active, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, organization_id, logo)
VALUES('IITA', '08034035281', 'iita@cgiar.org', NULL, 'IITA Contact', 'Englih', NULL, true, now(), NULL, 1, NULL, false, 2, 'iita.png');


DELETE FROM core.organization 
WHERE id = 3;

DELETE FROM core.tenant 
WHERE id = 2;

UPDATE core.tenant SET "name"='Default', customer_id=1 
WHERE id=1;

--Revert Changes
--rollback DELETE FROM core.customer where name = 'IITA';
--rollback INSERT INTO core.organization (legal_name, phone, web_page, slogan, "name", tax_id, logo, is_active, default_authentication, default_theme, code, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, customer_id, description) VALUES('CIMMYT', '525558042004', 'www.cimmyt.org', 'Maize and wheat science for improved livelihoods.', 'CIMMYT', NULL, 'cimmyt.png', true, 1, 1, 'CM', '2021-07-12 13:37:33.722', NULL, 1, NULL, false, 3, 1, NULL);
--rollback INSERT INTO core.tenant ("name", expiration, expired, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, customer_id, organization_id) VALUES('CGIAR-IRRI-2', '2050-01-01', false, '2021-07-12 13:37:33.303', NULL, 1, 1, false, 2, 3, 2);
--rollback UPDATE core.tenant SET "name"='CGIAR-CIMMYT-1', customer_id=2 WHERE id=1;


