--liquibase formatted sql

--changeset postgres:increase_zpl_length context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-632 Increment length in printout_template.zpl column


ALTER TABLE core.printout_template ALTER COLUMN zpl TYPE varchar(4000) USING zpl::varchar;

--Revert Changes
--rollback ALTER TABLE core.printout_template ALTER COLUMN zpl TYPE varchar(500) USING zpl::varchar;
