--liquibase formatted sql

--changeset postgres:add_admin_user context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-694 Add admnin user



SET session_replication_role = 'replica';

INSERT INTO crm.person (id,family_name,given_name,email,official_email,gender,has_credential,job_title,knows_about,phone,creation_timestamp,creator_id,is_void,language_id,tenant_id,person_status_id)
	VALUES (68,'System','Account','admin@ebsproject.org','admin@ebsproject.org','NA',true,'System Administrator','IT','5959521900',now(),1,false,1,1,1);


UPDATE "security"."user"
SET user_name='admin@ebsproject.org', person_id =68, default_role_id =2
WHERE id=1;


SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));

SET session_replication_role = 'origin';



--Revert changes
--rollback UPDATE "security"."user" SET user_name='admin', person_id =1, default_role_id =1 WHERE id=1;
--rollback DELETE FROM crm.person WHERE id = 68;
--rollback SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));