--liquibase formatted sql

--changeset postgres:update_context_path context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-699 Update context path in CSDB



UPDATE core.domain_instance
SET context='https://dev.ebsproject.org'
WHERE id=5;

UPDATE core.domain_instance
SET context='https://dev.ebsproject.org'
WHERE id=4;

--Revert Changes
--rollback UPDATE core.domain_instance SET context='http://localhost:3000' WHERE id=5;
--rollback UPDATE core.domain_instance SET context='http://localhost:3000' WHERE id=4;