--liquibase formatted sql

--changeset postgres:update_database_comments context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-689 Update database documentation


DO LANGUAGE PLPGSQL $$
BEGIN
  EXECUTE FORMAT('COMMENT ON DATABASE %I IS %L', current_database(), 'Core System Database, Manages concepts accross different Domians such as security, crm, roles, instances and Tenant information.');
END;
$$;

COMMENT ON SCHEMA core IS 'Core and other master data';
COMMENT ON SCHEMA crm IS 'Customer relationship management, person, address, status';
COMMENT ON SCHEMA program IS 'Program, crop_program, Projects';
COMMENT ON SCHEMA security IS 'Users, roles, authorization, functional_units';
COMMENT ON SCHEMA workflow IS 'Nodes, stages, workflow status.';

COMMENT ON TABLE core.alert
	IS 'Manages user alerts set to users.'
;

COMMENT ON TABLE core.domain_entity_reference
	IS 'Relationship between, entity and entity_reference table. Stores the entities (table names) per domain.'
;

COMMENT ON TABLE core.domain_instance
	IS 'Relationship between, domain and instance table. Contains the relation of each domain and its respective instance.'
;

COMMENT ON TABLE core.email_template_entity_reference
	IS 'Relationship between email_template and entity_reference table.'
;

COMMENT ON TABLE core.formula_type
	IS 'Type of formulas to be used in the sequence rule, they can be: Java Script code, sql, etc.'
;

COMMENT ON TABLE core.number_sequence_rule_entity_reference
	IS 'Relationship between entity_reference and number_sequence_rule table and refers to the rule and its related table.'
;

COMMENT ON TABLE core.printout_template
	IS 'Contains Tabular Reports, labels, dashboard metadata.'
;

COMMENT ON TABLE core.printout_template_product
	IS 'Store the reports classified by product.'
;

COMMENT ON TABLE core.printout_template_program
	IS 'Contains the reports classified by breeding program.'
;

COMMENT ON TABLE crm.address_person
	IS 'Relationship between person and address table and refers to the address related to the person'
;

COMMENT ON TABLE crm.person_status
	IS 'Manages the different person status in different workflow. E.g. pending, approved, etc.'
;

COMMENT ON TABLE crm.person_type
	IS 'Manages person types, e.g. vendor, users, donor, collaborator, etc.'
;

COMMENT ON TABLE security.functional_unit_user
	IS 'Relationship between functional_unit and user. Determines the user associated to each functional_unit.'
;

COMMENT ON TABLE security.product_authorization
	IS 'Manages the authorization by product.'
;

COMMENT ON TABLE security.role_product_function
	IS 'Relationship between role and product_function table  and determines the functions allowed per role.'
;

COMMENT ON TABLE security.tenant_user
	IS 'Relationship between tenant and user table. Indicates the users associated to certain tenant.'
;

COMMENT ON TABLE security.user_role
	IS 'Relationship between user and role table. indicates the role associated to each user.'
;

COMMENT ON TABLE workflow.node_stage
	IS 'Relationship between node and stage table. Indicates the stage of the current node in a workflow process.'
;

COMMENT ON TABLE workflow.status
	IS 'Manages the different status used in the Workflow engine.'
;

COMMENT ON TABLE program.crop
	IS 'Cultivated plant that is grown as food and is the subject of research and development conducted by various programs.'
;

COMMENT ON TABLE program.program
	IS ' Entity that conducts defined objectives for the production of the next generation of crops with desired characteristics.'
;

COMMENT ON TABLE workflow.status_type
	IS 'Manages the type of status of the workflow engine.'
;

COMMENT ON COLUMN core.alert.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated.'
;

COMMENT ON COLUMN core.alert.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.alert.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.alert_rule.alert_id
	IS 'Id reference to alert table. Indicates the relation between the defined alert and its rule.'
;

COMMENT ON COLUMN core.alert_rule.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.alert_rule.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.attributes.entityreference_id
	IS 'Id reference to entity_reference table. Indicates the table name related to its attributes.'
;

COMMENT ON COLUMN core.attributes.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated to the attribute.'
;

COMMENT ON COLUMN core.attributes.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.audit_logs.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.audit_logs.instance_id
	IS 'Id reference to the instance table. Indicates the instance related to the log.'
;

COMMENT ON COLUMN core.audit_logs.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.customer.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.customer.logo
	IS 'Logo name of the customer (if exists)'
;

COMMENT ON COLUMN core.customer.organization_id
	IS 'Id reference to the Organization table. Indicates the organization where the customer belongs to.'
;

COMMENT ON COLUMN core.domain.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.domain.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated to the domain.'
;

COMMENT ON COLUMN core.domain_entity_reference.domain_id
	IS 'Id reference to the domain table.'
;

COMMENT ON COLUMN core.domain_entity_reference.entity_reference_id
	IS 'Id reference to the entity_reference table'
;

COMMENT ON COLUMN core.domain_instance.domain_id
	IS 'Id reference to the domain table.'
;

COMMENT ON COLUMN core.domain_instance.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.domain_instance.instance_id
	IS 'Id reference to the instance table.'
;

COMMENT ON COLUMN core.domain_instance.is_mfe
	IS 'Defines if the product is a Micro Front end.'
;

COMMENT ON COLUMN core.domain_instance.sg_context
	IS 'API end point of each domain.'
;

COMMENT ON COLUMN core.domain_instance.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.email_template.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.email_template.person_id
	IS 'Id reference to the Person table. Indicates the person_id related to the template.'
;

COMMENT ON COLUMN core.email_template.tenant_id
	IS 'Id reference to the Tenant table. Indicates the active tenant for the email_template.'
;

COMMENT ON COLUMN core.email_template_entity_reference.emailtemplate_id
	IS 'Id reference to the email_template table.'
;

COMMENT ON COLUMN core.email_template_entity_reference.entityreference_id
	IS 'Id reference to the entity_reference table.'
;

COMMENT ON COLUMN core.entity_reference.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.entity_reference.storefield
	IS 'Defines the field where the value will be stored, using the dynamic form.'
;

COMMENT ON COLUMN core.entity_reference.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.formula_type.description
	IS 'Brief description about the formula type and what it is used for.'
;

COMMENT ON COLUMN core.formula_type.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.formula_type.name
	IS 'Name of the formula type.'
;

COMMENT ON COLUMN core.formula_type.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.html_tag.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.instance.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.instance.name
	IS 'Name of the instance.'
;

COMMENT ON COLUMN core.instance.notes
	IS 'Additional information about the instance.'
;

COMMENT ON COLUMN core.instance.port
	IS 'Port number where the instance is located.'
;

COMMENT ON COLUMN core.instance.server
	IS 'Server name where the instance is located.'
;

COMMENT ON COLUMN core.instance.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.language.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.message.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated.'
;

COMMENT ON COLUMN core.message.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.number_sequence_rule.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.number_sequence_rule.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.number_sequence_rule_entity_reference.entityreference_id
	IS 'Id reference to the entity_reference table'
;

COMMENT ON COLUMN core.number_sequence_rule_entity_reference.numbersequencerule_id
	IS 'Id reference to the number_sequence_rule_table.'
;

COMMENT ON COLUMN core.number_sequence_rule_segment.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.number_sequence_rule_segment.numbersequencerule_id
	IS 'Id reference to the number_sequence_rule table'
;

COMMENT ON COLUMN core.number_sequence_rule_segment.segment_id
	IS 'Id reference to the segment table'
;

COMMENT ON COLUMN core.organization.customer_id
	IS 'Id reference to the customer table.'
;

COMMENT ON COLUMN core.organization.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.preference.alert_id
	IS 'Id reference to the alert table.'
;

COMMENT ON COLUMN core.preference.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.preference.preference
	IS 'Name of the preference.'
;

COMMENT ON COLUMN core.preference.styletheme_id
	IS 'Id reference to the style_theme table. Refers to the style theme selected on a particular preference.'
;

COMMENT ON COLUMN core.preference.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.preference.user_id
	IS 'Id reference to the user table. Indicates the user related to his/her preference'
;

COMMENT ON COLUMN core.printout_template.description
	IS 'Description of the template'
;

COMMENT ON COLUMN core.printout_template.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.printout_template.name
	IS 'Name identifier of the template.'
;

COMMENT ON COLUMN core.printout_template.zpl
	IS 'Stores the zpl code to be printed directly using zebra printers.'
;

COMMENT ON COLUMN core.printout_template_product.printout_template_id
	IS 'Id reference to the printout_template table.'
;

COMMENT ON COLUMN core.printout_template_product.product_id
	IS 'Id reference to the Product table.'
;

COMMENT ON COLUMN core.printout_template_program.printout_template_id
	IS 'Id reference to the printout_template table.'
;

COMMENT ON COLUMN core.printout_template_program.program_id
	IS 'Id reference to the Program table.'
;

COMMENT ON COLUMN core.process.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.product.domain_id
	IS 'Id reference to the Domain table.'
;

COMMENT ON COLUMN core.product.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated to the product.'
;

COMMENT ON COLUMN core.product.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.product.path
	IS 'Shows the path og the product'
;

COMMENT ON COLUMN core.segment.entityreference_id
	IS 'Id reference to the entity_reference table'
;

COMMENT ON COLUMN core.segment.formulatype_id
	IS 'Id reference to the formula_type'
;

COMMENT ON COLUMN core.segment.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.style_theme.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.tenant.customer_id
	IS 'Id reference to the customer of a particular Tenant.'
;

COMMENT ON COLUMN core.tenant.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.tenant.name
	IS 'Name identifier of the tenant.'
;

COMMENT ON COLUMN core.tenant.organization_id
	IS 'Organization id related to a particular Tenant.'
;

COMMENT ON COLUMN core.translation.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated to the translation.'
;

COMMENT ON COLUMN core.translation.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.translation.language_id
	IS 'Refers to the language id used in the translation.'
;

COMMENT ON COLUMN crm.address.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.address.type
	IS 'Defines the type of address, e.g. default, office, etc.'
;

COMMENT ON COLUMN crm.address_person.address_id
	IS 'Refers to the address id in the Address table.'
;

COMMENT ON COLUMN crm.address_person.person_id
	IS 'Refers to the person Id in the Person table.'
;

COMMENT ON COLUMN crm.person.external_id
	IS 'References the id of the original source.'
;

COMMENT ON COLUMN crm.person.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.person.language_id
	IS 'Reference to the language id in the Language table. Indicates the preffered language for that particular person.'
;

COMMENT ON COLUMN crm.person.person_status_id
	IS 'Refers to the person_status table and indicates the current status of a particular person.'
;

COMMENT ON COLUMN crm.person.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN crm.person_status.description
	IS 'Description of the status.'
;

COMMENT ON COLUMN crm.person_status.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.person_status.name
	IS 'Refers to the different types of status that a person can have.'
;

COMMENT ON COLUMN crm.person_type.person_id
	IS 'Id reference to the person id in the Person table.'
;

COMMENT ON COLUMN crm.person_type.type_id
	IS 'Id reference to the crm. type table. which indicates the type of a certain person. It can be vendor, person, collaborator, etc.'
;

COMMENT ON COLUMN crm.type.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN program.crop.code
	IS 'Textual identifier of the crop'
;

COMMENT ON COLUMN program.crop.description
	IS 'Additional information about the crop'
;

COMMENT ON COLUMN program.crop.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN program.crop.name
	IS 'Full name of the crop'
;

COMMENT ON COLUMN program.crop.notes
	IS 'Technical details about the record'
;

COMMENT ON COLUMN program.crop_program.crop_id
	IS 'Reference to the crop under research by the crop program'
;

COMMENT ON COLUMN program.crop_program.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN program.crop_program.organization_id
	IS 'Reference to the organization where the crop program is held.'
;

COMMENT ON COLUMN program.program.crop_program_id
	IS 'Reference to the crop program where the program belongs.'
;

COMMENT ON COLUMN program.program.external_id
	IS 'References the id of the original source.'
;

COMMENT ON COLUMN program.program.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN program.program.owner_id
	IS 'Reference to the user who is the lead of the selected program.'
;

COMMENT ON COLUMN program.program.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN program.project.code
	IS 'Textual identifier of the project'
;

COMMENT ON COLUMN program.project.description
	IS 'Additional information about the projec'
;

COMMENT ON COLUMN program.project.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN program.project.name
	IS 'Full name of the project'
;

COMMENT ON COLUMN program.project.notes
	IS 'Technical details about the record'
;

COMMENT ON COLUMN program.project.person_id
	IS 'Reference to the leader (person) who manages the project'
;

COMMENT ON COLUMN program.project.program_id
	IS 'Reference to the program where the project is created'
;

COMMENT ON COLUMN program.project.status
	IS ' Status of the project {draft, active, archived} '
;

COMMENT ON COLUMN program.season.code
	IS 'Textual identifier of the season'
;

COMMENT ON COLUMN program.season.description
	IS 'Additional information about the season'
;

COMMENT ON COLUMN program.season.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN program.season.name
	IS 'Full name of the season'
;

COMMENT ON COLUMN program.season.notes
	IS 'Technical details about the record'
;

COMMENT ON COLUMN security.delegation.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN security.delegation.user_id
	IS 'Reference to the user who is delegating the tasks or permissions.'
;

COMMENT ON COLUMN security.functional_unit.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN security.functional_unit.parent_func_unit_id
	IS 'Self reference to identify the parent unit if exists.'
;

COMMENT ON COLUMN security.functional_unit.program_id
	IS 'Reference to the Program where this functional unit belongs to.'
;

COMMENT ON COLUMN security.functional_unit_user.functional_unit_id
	IS 'Reference to the functional unit where the user belongs to.'
;

COMMENT ON COLUMN security.functional_unit_user.user_id
	IS 'Reference to the user that belong to a particular unit.'
;

COMMENT ON COLUMN security.product_authorization.functional_unit_id
	IS 'Reference to the functional_unit. A functional unit has specific permissions for each product.'
;

COMMENT ON COLUMN security.product_authorization.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN security.product_authorization.product_id
	IS 'Reference to the product id.'
;

COMMENT ON COLUMN security.product_function.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN security.product_function.node_id
	IS 'Reference to the workflow node'
;

COMMENT ON COLUMN security.product_function.stage_id
	IS 'Reference to the workflow stage'
;

COMMENT ON COLUMN security.product_function.product_id
	IS 'Reference to the product.'
;

COMMENT ON COLUMN security.role.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN security.role.name
	IS 'Name identifier of the role.'
;

COMMENT ON COLUMN security.role_product_function.product_function_id
	IS 'Reference to the product_function available for certain role.'
;

COMMENT ON COLUMN security.role_product_function.role_id
	IS 'Reference to the role'
;

COMMENT ON COLUMN security.tenant_user.tenant_id
	IS 'Reference to the Tenant associated to the user.'
;

COMMENT ON COLUMN security.tenant_user.user_id
	IS 'Reference to the user associated to a certain Tenant.'
;

COMMENT ON COLUMN security."user".default_role_id
	IS 'User name'
;

COMMENT ON COLUMN security."user".external_id
	IS 'References the id of the user in the original database where it was imported from.'
;

COMMENT ON COLUMN security."user".id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN security."user".person_id
	IS 'Reference to the person associated to the user.'
;

COMMENT ON COLUMN security.user_role.role_id
	IS 'Reference to the role.'
;

COMMENT ON COLUMN security.user_role.user_id
	IS 'Reference to the user.'
;

COMMENT ON COLUMN workflow.cf_type.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.cf_value.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.cf_value.request_id
	IS 'Reference to the request.'
;

COMMENT ON COLUMN workflow.event.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.event.instance_id
	IS 'Reference to the instance.'
;

COMMENT ON COLUMN workflow.event.node_id
	IS 'Reference to the node table that indicates the current node of an event.'
;

COMMENT ON COLUMN workflow.event.stage_id
	IS 'Reference to the stage table that indicates the current stage of an event.'
;

COMMENT ON COLUMN workflow.event.wf_instance_id
	IS 'Reference to the workflow instance.'
;

COMMENT ON COLUMN workflow.node.entityreference_id
	IS 'Reference to the entity_reference table that indicates the name of the table related to this node.'
;

COMMENT ON COLUMN workflow.node.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated.'
;

COMMENT ON COLUMN workflow.node.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.node.process_id
	IS 'Reference to the process.'
;

COMMENT ON COLUMN workflow.node.product_id
	IS 'Reference to the product related to this node.'
;

COMMENT ON COLUMN workflow.node.workflow_id
	IS 'Reference to workflow in progress.'
;

COMMENT ON COLUMN workflow.node_cf.cftype_id
	IS 'Reference to the type of the custom field.'
;

COMMENT ON COLUMN workflow.node_cf.entityreference_id
	IS 'Reference to the table name associated to a particular custom field.'
;

COMMENT ON COLUMN workflow.node_cf.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated.'
;

COMMENT ON COLUMN workflow.node_cf.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.node_cf.node_id
	IS 'Reference to the current node.'
;

COMMENT ON COLUMN workflow.node_stage.node_id
	IS 'Reference to the current node within a particular stage.'
;

COMMENT ON COLUMN workflow.node_stage.stage_id
	IS 'Reference to the stage id.'
;

COMMENT ON COLUMN workflow.phase.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated.'
;

COMMENT ON COLUMN workflow.phase.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.phase.workflow_id
	IS 'Reference to the current workflow in process'
;

COMMENT ON COLUMN workflow.request.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.request.instance_id
	IS 'Reference to the instance'
;

COMMENT ON COLUMN workflow.request.person_id
	IS 'Reference to the person who made the request.'
;

COMMENT ON COLUMN workflow.request.wf_instance_id
	IS 'Reference to the Workflow instance for this request.'
;

COMMENT ON COLUMN workflow.stage.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated.'
;

COMMENT ON COLUMN workflow.stage.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.stage.parent_id
	IS 'Recursive relation for stages who have a parent stage stored in the same table.'
;

COMMENT ON COLUMN workflow.stage.phase_id
	IS 'Reference of the current stage within the workflow.'
;

COMMENT ON COLUMN workflow.status.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.status.instance_id
	IS 'Reference to the instance'
;

COMMENT ON COLUMN workflow.status.wf_instance_id
	IS 'Reference to the workflow instance'
;

COMMENT ON COLUMN workflow.status_type.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.wf_instance.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.wf_instance.workflow_id
	IS 'Reference to the current workflow.'
;

COMMENT ON COLUMN workflow.workflow.entityreference_id
	IS 'Reference to the main entity (table) within a specific business case. E.g. An SM Request will refer to the Request table.'
;

COMMENT ON COLUMN workflow.workflow.htmltag_id
	IS 'Relation to html_tag table. Indicates the UI tag associated.'
;

COMMENT ON COLUMN workflow.workflow.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.workflow.node_id
	IS 'Reference to the current node of the workflow.'
;

COMMENT ON COLUMN workflow.workflow.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;


--Revert Changes
--rollback DO LANGUAGE PLPGSQL $$
--rollback BEGIN
--rollback   EXECUTE FORMAT('COMMENT ON DATABASE %I IS %L', current_database(), '');
--rollback END;
--rollback $$;

--rollback COMMENT ON SCHEMA core IS '';
--rollback COMMENT ON SCHEMA crm IS '';
--rollback COMMENT ON SCHEMA program IS '';
--rollback COMMENT ON SCHEMA security IS '';
--rollback COMMENT ON SCHEMA workflow IS '';

--rollback COMMENT ON TABLE core.alert IS '';
--rollback COMMENT ON TABLE core.domain_entity_reference IS '';
--rollback COMMENT ON TABLE core.domain_instance IS '';
--rollback COMMENT ON TABLE core.email_template_entity_reference IS '';
--rollback COMMENT ON TABLE core.formula_type  IS '';
--rollback COMMENT ON TABLE core.number_sequence_rule_entity_reference IS '';
--rollback COMMENT ON TABLE core.printout_template IS '';
--rollback COMMENT ON TABLE core.printout_template_product IS '';
--rollback COMMENT ON TABLE core.printout_template_program IS '';
--rollback COMMENT ON TABLE crm.address_person IS '';
--rollback COMMENT ON TABLE crm.person_status  IS '';
--rollback COMMENT ON TABLE crm.person_type  IS '';
--rollback COMMENT ON TABLE security.functional_unit_user  IS '';
--rollback COMMENT ON TABLE security.product_authorization IS '';
--rollback COMMENT ON TABLE security.role_product_function IS '';
--rollback COMMENT ON TABLE security.tenant_user IS '';
--rollback COMMENT ON TABLE security.user_role IS '';
--rollback COMMENT ON TABLE workflow.node_stage  IS '';
--rollback COMMENT ON TABLE workflow.status  IS '';
--rollback COMMENT ON TABLE program.crop IS '';
--rollback COMMENT ON TABLE program.program  IS '';
--rollback COMMENT ON TABLE workflow.status_type IS '';

--rollback COMMENT ON COLUMN core.alert.htmltag_id IS '';
--rollback COMMENT ON COLUMN core.alert.id IS '';
--rollback COMMENT ON COLUMN core.alert.tenant_id  IS '';
--rollback COMMENT ON COLUMN core.alert_rule.alert_id  IS '';
--rollback COMMENT ON COLUMN core.alert_rule.id  IS '';
--rollback COMMENT ON COLUMN core.alert_rule.tenant_id IS '';
--rollback COMMENT ON COLUMN core.attributes.entityreference_id IS '';
--rollback COMMENT ON COLUMN core.attributes.htmltag_id  IS '';
--rollback COMMENT ON COLUMN core.attributes.id  IS '';
--rollback COMMENT ON COLUMN core.audit_logs.id  IS '';
--rollback COMMENT ON COLUMN core.audit_logs.instance_id IS '';
--rollback COMMENT ON COLUMN core.audit_logs.tenant_id IS '';
--rollback COMMENT ON COLUMN core.customer.id  IS '';
--rollback COMMENT ON COLUMN core.customer.logo  IS '';
--rollback COMMENT ON COLUMN core.customer.organization_id IS '';
--rollback COMMENT ON COLUMN core.domain.id  IS '';
--rollback COMMENT ON COLUMN core.domain.htmltag_id  IS '';
--rollback COMMENT ON COLUMN core.domain_entity_reference.domain_id  IS '';
--rollback COMMENT ON COLUMN core.domain_entity_reference.entity_reference_id  IS '';
--rollback COMMENT ON COLUMN core.domain_instance.domain_id  IS '';
--rollback COMMENT ON COLUMN core.domain_instance.id IS '';
--rollback COMMENT ON COLUMN core.domain_instance.instance_id  IS '';
--rollback COMMENT ON COLUMN core.domain_instance.is_mfe IS '';
--rollback COMMENT ON COLUMN core.domain_instance.sg_context IS '';
--rollback COMMENT ON COLUMN core.domain_instance.tenant_id  IS '';
--rollback COMMENT ON COLUMN core.email_template.id  IS '';
--rollback COMMENT ON COLUMN core.email_template.person_id IS '';
--rollback COMMENT ON COLUMN core.email_template.tenant_id IS '';
--rollback COMMENT ON COLUMN core.email_template_entity_reference.emailtemplate_id IS '';
--rollback COMMENT ON COLUMN core.email_template_entity_reference.entityreference_id IS '';
--rollback COMMENT ON COLUMN core.entity_reference.id  IS '';
--rollback COMMENT ON COLUMN core.entity_reference.storefield  IS '';
--rollback COMMENT ON COLUMN core.entity_reference.tenant_id IS '';
--rollback COMMENT ON COLUMN core.formula_type.description IS '';
--rollback COMMENT ON COLUMN core.formula_type.id  IS '';
--rollback COMMENT ON COLUMN core.formula_type.name  IS '';
--rollback COMMENT ON COLUMN core.formula_type.tenant_id IS '';
--rollback COMMENT ON COLUMN core.html_tag.id  IS '';
--rollback COMMENT ON COLUMN core.instance.id  IS '';
--rollback COMMENT ON COLUMN core.instance.name  IS '';
--rollback COMMENT ON COLUMN core.instance.notes IS '';
--rollback COMMENT ON COLUMN core.instance.port  IS '';
--rollback COMMENT ON COLUMN core.instance.server  IS '';
--rollback COMMENT ON COLUMN core.instance.tenant_id IS '';
--rollback COMMENT ON COLUMN core.language.id  IS '';
--rollback COMMENT ON COLUMN core.message.htmltag_id IS '';
--rollback COMMENT ON COLUMN core.message.id IS '.';
--rollback COMMENT ON COLUMN core.number_sequence_rule.id  IS '';
--rollback COMMENT ON COLUMN core.number_sequence_rule.tenant_id IS '';
--rollback COMMENT ON COLUMN core.number_sequence_rule_entity_reference.entityreference_id IS '';
--rollback COMMENT ON COLUMN core.number_sequence_rule_entity_reference.numbersequencerule_id  IS '';
--rollback COMMENT ON COLUMN core.number_sequence_rule_segment.id  IS '';
--rollback COMMENT ON COLUMN core.number_sequence_rule_segment.numbersequencerule_id IS '';
--rollback COMMENT ON COLUMN core.number_sequence_rule_segment.segment_id  IS '';
--rollback COMMENT ON COLUMN core.organization.customer_id IS '';
--rollback COMMENT ON COLUMN core.organization.id  IS '';
--rollback COMMENT ON COLUMN core.preference.alert_id  IS '';
--rollback COMMENT ON COLUMN core.preference.id  IS '';
--rollback COMMENT ON COLUMN core.preference.preference  IS '';
--rollback COMMENT ON COLUMN core.preference.styletheme_id IS '';
--rollback COMMENT ON COLUMN core.preference.tenant_id IS '';
--rollback COMMENT ON COLUMN core.preference.user_id IS '';
--rollback COMMENT ON COLUMN core.printout_template.description  IS '';
--rollback COMMENT ON COLUMN core.printout_template.id IS '';
--rollback COMMENT ON COLUMN core.printout_template.name IS '';
--rollback COMMENT ON COLUMN core.printout_template.zpl  IS '';
--rollback COMMENT ON COLUMN core.printout_template_product.printout_template_id IS '';
--rollback COMMENT ON COLUMN core.printout_template_product.product_id IS '';
--rollback COMMENT ON COLUMN core.printout_template_program.printout_template_id IS '';
--rollback COMMENT ON COLUMN core.printout_template_program.program_id IS '';
--rollback COMMENT ON COLUMN core.process.id IS '';
--rollback COMMENT ON COLUMN core.product.domain_id  IS '';
--rollback COMMENT ON COLUMN core.product.htmltag_id IS '';
--rollback COMMENT ON COLUMN core.product.id IS '';
--rollback COMMENT ON COLUMN core.product.path IS '';
--rollback COMMENT ON COLUMN core.segment.entityreference_id IS '';
--rollback COMMENT ON COLUMN core.segment.formulatype_id IS '';
--rollback COMMENT ON COLUMN core.segment.id IS '';
--rollback COMMENT ON COLUMN core.style_theme.id IS '';
--rollback COMMENT ON COLUMN core.tenant.customer_id IS '';
--rollback COMMENT ON COLUMN core.tenant.id  IS '.';
--rollback COMMENT ON COLUMN core.tenant.name  IS '';
--rollback COMMENT ON COLUMN core.tenant.organization_id IS '';
--rollback COMMENT ON COLUMN core.translation.htmltag_id IS '';
--rollback COMMENT ON COLUMN core.translation.id IS '';
--rollback COMMENT ON COLUMN core.translation.language_id  IS '';
--rollback COMMENT ON COLUMN crm.address.id IS '';
--rollback COMMENT ON COLUMN crm.address.type  IS '';
--rollback COMMENT ON COLUMN crm.address_person.address_id IS '';
--rollback COMMENT ON COLUMN crm.address_person.person_id  IS '';
--rollback COMMENT ON COLUMN crm.person.external_id  IS '';
--rollback COMMENT ON COLUMN crm.person.id IS '';
--rollback COMMENT ON COLUMN crm.person.language_id  IS '';
--rollback COMMENT ON COLUMN crm.person.person_status_id IS '';
--rollback COMMENT ON COLUMN crm.person.tenant_id  IS '';
--rollback COMMENT ON COLUMN crm.person_status.description IS '';
--rollback COMMENT ON COLUMN crm.person_status.id  IS '';
--rollback COMMENT ON COLUMN crm.person_status.name  IS '';
--rollback COMMENT ON COLUMN crm.person_type.person_id IS '';
--rollback COMMENT ON COLUMN crm.person_type.type_id IS '';
--rollback COMMENT ON COLUMN crm.type.id IS '';
--rollback COMMENT ON COLUMN program.crop.code IS '';
--rollback COMMENT ON COLUMN program.crop.description  IS '';
--rollback COMMENT ON COLUMN program.crop.id IS '';
--rollback COMMENT ON COLUMN program.crop.name IS '';
--rollback COMMENT ON COLUMN program.crop.notes  IS '';
--rollback COMMENT ON COLUMN program.crop_program.crop_id  IS '';
--rollback COMMENT ON COLUMN program.crop_program.id IS '';
--rollback COMMENT ON COLUMN program.crop_program.organization_id  IS '';
--rollback COMMENT ON COLUMN program.program.crop_program_id IS '';
--rollback COMMENT ON COLUMN program.program.external_id IS '';
--rollback COMMENT ON COLUMN program.program.id  IS '';
--rollback COMMENT ON COLUMN program.program.owner_id  IS '';
--rollback COMMENT ON COLUMN program.program.tenant_id IS '';
--rollback COMMENT ON COLUMN program.project.code  IS '';
--rollback COMMENT ON COLUMN program.project.description IS '';
--rollback COMMENT ON COLUMN program.project.id  IS '';
--rollback COMMENT ON COLUMN program.project.name  IS '';
--rollback COMMENT ON COLUMN program.project.notes IS '';
--rollback COMMENT ON COLUMN program.project.person_id IS '';
--rollback COMMENT ON COLUMN program.project.program_id  IS '';
--rollback COMMENT ON COLUMN program.project.status  IS '';
--rollback COMMENT ON COLUMN program.season.code IS '';
--rollback COMMENT ON COLUMN program.season.description  IS '';
--rollback COMMENT ON COLUMN program.season.id IS '';
--rollback COMMENT ON COLUMN program.season.name IS '';
--rollback COMMENT ON COLUMN program.season.notes  IS '';
--rollback COMMENT ON COLUMN security.delegation.id  IS '';
--rollback COMMENT ON COLUMN security.delegation.user_id IS '';
--rollback COMMENT ON COLUMN security.functional_unit.id IS '';
--rollback COMMENT ON COLUMN security.functional_unit.parent_func_unit_id  IS '';
--rollback COMMENT ON COLUMN security.functional_unit.program_id IS '';
--rollback COMMENT ON COLUMN security.functional_unit_user.functional_unit_id  IS '';
--rollback COMMENT ON COLUMN security.functional_unit_user.user_id IS '';
--rollback COMMENT ON COLUMN security.product_authorization.functional_unit_id IS '';
--rollback COMMENT ON COLUMN security.product_authorization.id IS '';
--rollback COMMENT ON COLUMN security.product_authorization.product_id IS '';
--rollback COMMENT ON COLUMN security.product_function.id  IS '';
--rollback COMMENT ON COLUMN security.product_function.node_id IS '';
--rollback COMMENT ON COLUMN security.product_function.stage_id  IS '';
--rollback COMMENT ON COLUMN security.product_function.product_id  IS 'Reference to the product.';
--rollback COMMENT ON COLUMN security.role.id  IS '';
--rollback COMMENT ON COLUMN security.role.name  IS '';
--rollback COMMENT ON COLUMN security.role_product_function.product_function_id  IS '';
--rollback COMMENT ON COLUMN security.role_product_function.role_id  IS '';
--rollback COMMENT ON COLUMN security.tenant_user.tenant_id  IS '';
--rollback COMMENT ON COLUMN security.tenant_user.user_id  IS '';
--rollback COMMENT ON COLUMN security."user".default_role_id IS '';
--rollback COMMENT ON COLUMN security."user".external_id IS '';
--rollback COMMENT ON COLUMN security."user".id  IS '';
--rollback COMMENT ON COLUMN security."user".person_id IS '';
--rollback COMMENT ON COLUMN security.user_role.role_id  IS '.';
--rollback COMMENT ON COLUMN security.user_role.user_id  IS '';
--rollback COMMENT ON COLUMN workflow.cf_type.id IS '';
--rollback COMMENT ON COLUMN workflow.cf_value.id  IS '';
--rollback COMMENT ON COLUMN workflow.cf_value.request_id  IS '';
--rollback COMMENT ON COLUMN workflow.event.id IS '';
--rollback COMMENT ON COLUMN workflow.event.instance_id  IS '';
--rollback COMMENT ON COLUMN workflow.event.node_id  IS '';
--rollback COMMENT ON COLUMN workflow.event.stage_id IS '';
--rollback COMMENT ON COLUMN workflow.event.wf_instance_id IS '';
--rollback COMMENT ON COLUMN workflow.node.entityreference_id  IS '';
--rollback COMMENT ON COLUMN workflow.node.htmltag_id  IS '';
--rollback COMMENT ON COLUMN workflow.node.id  IS '';
--rollback COMMENT ON COLUMN workflow.node.process_id  IS '';
--rollback COMMENT ON COLUMN workflow.node.product_id  IS '';
--rollback COMMENT ON COLUMN workflow.node.workflow_id IS '';
--rollback COMMENT ON COLUMN workflow.node_cf.cftype_id  IS '';
--rollback COMMENT ON COLUMN workflow.node_cf.entityreference_id IS '';
--rollback COMMENT ON COLUMN workflow.node_cf.htmltag_id IS '';
--rollback COMMENT ON COLUMN workflow.node_cf.id IS '';
--rollback COMMENT ON COLUMN workflow.node_cf.node_id  IS '';
--rollback COMMENT ON COLUMN workflow.node_stage.node_id IS '';
--rollback COMMENT ON COLUMN workflow.node_stage.stage_id  IS '';
--rollback COMMENT ON COLUMN workflow.phase.htmltag_id IS '';
--rollback COMMENT ON COLUMN workflow.phase.id IS '';
--rollback COMMENT ON COLUMN workflow.phase.workflow_id  IS '';
--rollback COMMENT ON COLUMN workflow.request.id IS '';
--rollback COMMENT ON COLUMN workflow.request.instance_id  IS '';
--rollback COMMENT ON COLUMN workflow.request.person_id  IS '';
--rollback COMMENT ON COLUMN workflow.request.wf_instance_id IS '';
--rollback COMMENT ON COLUMN workflow.stage.htmltag_id IS '';
--rollback COMMENT ON COLUMN workflow.stage.id IS '';
--rollback COMMENT ON COLUMN workflow.stage.parent_id  IS '';
--rollback COMMENT ON COLUMN workflow.stage.phase_id IS '';
--rollback COMMENT ON COLUMN workflow.status.id  IS '';
--rollback COMMENT ON COLUMN workflow.status.instance_id IS '';
--rollback COMMENT ON COLUMN workflow.status.wf_instance_id  IS '';
--rollback COMMENT ON COLUMN workflow.status_type.id IS '';
--rollback COMMENT ON COLUMN workflow.wf_instance.id IS '';
--rollback COMMENT ON COLUMN workflow.wf_instance.workflow_id  IS '';
--rollback COMMENT ON COLUMN workflow.workflow.entityreference_id  IS '';
--rollback COMMENT ON COLUMN workflow.workflow.htmltag_id  IS '';
--rollback COMMENT ON COLUMN workflow.workflow.id  IS '';
--rollback COMMENT ON COLUMN workflow.workflow.node_id IS '';
--rollback COMMENT ON COLUMN workflow.workflow.tenant_id IS '';
