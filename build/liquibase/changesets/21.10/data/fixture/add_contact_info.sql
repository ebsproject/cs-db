--liquibase formatted sql

--changeset postgres:add_contact_info context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-734 Add contatc_info and link it to Person



INSERT INTO crm.contact
(id, category, tenant_id, creation_timestamp, creator_id, is_void)
(select id, 'Person', 1, now(), 1, false from crm.person);

UPDATE crm.person
SET contact_id= id;

UPDATE "security"."user"
SET contact_id = person_id;

select setval('crm.contact_id_seq',max(id)) from crm.contact;


--revert Changes
--rollback UPDATE crm.address SET contact_id = NULL WHERE id=1;
--rollback UPDATE crm.person SET contact_id= NULL;
--rollback UPDATE "security"."user" SET contact_id = NULL;

--rollback ALTER TABLE crm.address DROP CONSTRAINT "FK_address_contact";
--rollback ALTER TABLE crm.person DROP CONSTRAINT "FK_person_contact";
--rollback ALTER TABLE security."user" DROP CONSTRAINT "FK_user_contact";

--rollback truncate table crm.contact CASCADE;

--rollback ALTER TABLE crm.address ADD CONSTRAINT "FK_address_contact" FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action;
--rollback ALTER TABLE crm.person ADD CONSTRAINT "FK_person_contact" FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action;
--rollback ALTER TABLE security."user" ADD CONSTRAINT "FK_user_contact" FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action;

--rollback select setval('crm.contact_id_seq',max(id)) from crm.contact;