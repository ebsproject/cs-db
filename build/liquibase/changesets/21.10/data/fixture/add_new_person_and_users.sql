--liquibase formatted sql

--changeset postgres:add_new_person_and_users context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-715 Add new users to csdb



SET session_replication_role = 'replica';

INSERT INTO crm.person
(id, given_name, family_name, additional_name, email, official_email, gender, has_credential, job_title, knows_about, phone, person_status_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, tenant_id, language_id)
VALUES
(69, 'Rodolfo Christian', 'Escoto', 'Sam', 'r.escoto@irri.org', 'r.escoto@irri.org', 'male', true, 'SQA specialist', 'IT', '5959521900', 1, now(), null, 1, null, false, 1, 1),
(70, 'Christian', 'Vacalares', null, 'chris.vacalares@gmail.com', 'chris.vacalares@gmail.com', 'male', true, 'SQA specialist', 'IT', '5959521900', 1, now(), null, 1, null, false, 1, 1);


INSERT INTO "security"."user"
(id, user_name, last_access, default_role_id, is_is, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, person_id)
VALUES
(71, 'r.escoto@irri.org', null, 2, 1, now(), null, 1, null, false, 69),
(72, 'chris.vacalares@gmail.com', null, 2, 1, now(), null, 1, null, false, 70);

INSERT INTO "security".user_role
(role_id, user_id)
VALUES
(2, 71),
(2, 72);

INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(71, 1);
INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(72, 1);


SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));
SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));

SET session_replication_role = 'origin';



--Revert Changes
--rollback DELETE FROM "security".tenant_user WHERE user_id=71;
--rollback DELETE FROM "security".tenant_user WHERE user_id=72;

--rollback DELETE FROM "security".user_role where role_id = 2 and user_id = 71;
--rollback DELETE FROM "security".user_role where role_id = 2 and user_id = 72;
--rollback DELETE FROM "security"."user" where id = 71;
--rollback DELETE FROM "security"."user" where id = 72;
--rollback DELETE FROM crm.person WHERE id = 69;
--rollback DELETE FROM crm.person WHERE id = 70;