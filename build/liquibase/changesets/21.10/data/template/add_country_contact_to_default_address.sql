--liquibase formatted sql

--changeset postgres:add_country_contact_to_default_address context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-969 Update CS template database


INSERT INTO crm.contact
(id, category, tenant_id, creation_timestamp, creator_id, is_void)
VALUES (0, 'Person', 1, now(), 1, false);

UPDATE crm.address
SET contact_id=0 WHERE id=1;

UPDATE crm.address
SET country_id=1 WHERE id=1;


--rollback UPDATE crm.address SET country_id = NULL WHERE id=1;
--rollback DELETE FROM crm.contact WHERE id = 0;