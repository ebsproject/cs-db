--liquibase formatted sql

--changeset postgres:add_data_to_contact_type_and_info_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-729 Add data to contact_type and info_type

INSERT INTO crm.contact_type
(category, "name", tenant_id, creation_timestamp, creator_id, is_void)
VALUES
('Person', 'Collaborator', 1, now(), 1, false),
('Person', 'Donor', 1, now(), 1, false),
('Person', 'Customer', 1, now(), 1, false),
('Person', 'Vendor', 1, now(), 1, false),
('Institution', 'Sponsor', 1, now(), 1, false),
('Institution', 'Customer', 1, now(), 1, false),
('Institution', 'Vendor', 1, now(), 1, false),
('Institution', 'Collaborator', 1, now(), 1, false);


INSERT INTO crm.info_type
("name", description, tenant_id, creation_timestamp, creator_id, is_void)
VALUES
('Phone number', 'Personal Phone number, Office phone number, etc. ', 1, now(), 1, false),
('Email', 'Electronic Mail', 1, now(), 1, false),
('Web Page', 'Institution or personal web site', 1, now(), 1, false);


--Revert Changes
--rollback TRUNCATE table crm.contact_type CASCADE;
--rollback TRUNCATE table crm.info_type CASCADE;
--rollback alter sequence crm.contact_type_id_seq restart with 1;
--rollback alter sequence crm.info_type_id_seq restart with 1;