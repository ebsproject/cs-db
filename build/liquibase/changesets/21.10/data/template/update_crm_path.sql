--liquibase formatted sql

--changeset postgres:update_crm_path context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-735 Update crm path in Product table



UPDATE core.product
SET "path"='crm' WHERE id=11;



--Revert Changes
--rollback UPDATE core.product SET "path"='dashboard' WHERE id=11;