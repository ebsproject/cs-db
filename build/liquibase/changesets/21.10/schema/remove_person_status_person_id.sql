--liquibase formatted sql

--changeset postgres:remove_person_status_person_id context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-728 Remove person_status from Person and person_id from Institution



ALTER TABLE crm.person 
  DROP  CONSTRAINT "FK_person_person_status";

ALTER TABLE crm.person 
 DROP COLUMN IF EXISTS person_status_id;

ALTER TABLE crm.institution 
  DROP  CONSTRAINT "FK_institution_person";

ALTER TABLE crm.institution 
 DROP COLUMN IF EXISTS person_id;

ALTER TABLE crm.institution 
 ADD COLUMN principal_contact_id integer NOT NULL;

ALTER TABLE crm.institution ADD CONSTRAINT "FK_institution_principal_contact"
	FOREIGN KEY (principal_contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN crm.institution.principal_contact_id
	IS 'Principal contact point of the Institution'
;


--Revert Changes
--rollback ALTER TABLE crm.institution ADD COLUMN person_id int;
--rollback ALTER TABLE crm.person ADD COLUMN person_status_id int;

--rollback ALTER TABLE crm.institution ADD CONSTRAINT "FK_institution_person" FOREIGN KEY (person_id) REFERENCES crm.person(id);
--rollback ALTER TABLE crm.person ADD CONSTRAINT "FK_person_person_status" FOREIGN KEY (person_status_id) REFERENCES crm.person_status(id);

--rollback ALTER TABLE crm.institution DROP CONSTRAINT "FK_institution_principal_contact";
--rollback ALTER TABLE crm.institution DROP COLUMN principal_contact_id;

