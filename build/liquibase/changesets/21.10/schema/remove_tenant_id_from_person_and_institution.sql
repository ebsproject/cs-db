--liquibase formatted sql

--changeset postgres:remove_tenant_id_from_person_and_institution context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-726 Remove tenant_id form Person and Institution in CSDB


ALTER TABLE crm.person 
  DROP  CONSTRAINT "FK_person_tenant";

ALTER TABLE crm.institution 
 DROP COLUMN IF EXISTS tenant_id;

ALTER TABLE crm.person 
 DROP COLUMN IF EXISTS tenant_id;


--Revert Changes
--rollback ALTER TABLE crm.person ADD COLUMN tenant_id int;
--rollback ALTER TABLE crm.person ADD CONSTRAINT "FK_person_tenant" FOREIGN KEY (tenant_id) REFERENCES core.tenant(id);
--rollback ALTER TABLE crm.institution ADD COLUMN tenant_id int;