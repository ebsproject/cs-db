--liquibase formatted sql

--changeset postgres:update_address_person_not_null_values context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-733 Update not null values in address and person table



ALTER TABLE crm.address 
 ALTER COLUMN country_id SET NOT NULL;

ALTER TABLE crm.address 
 ALTER COLUMN location DROP NOT NULL;

ALTER TABLE crm.address 
 ALTER COLUMN region DROP NOT NULL;

ALTER TABLE crm.address 
 ALTER COLUMN zip_code DROP NOT NULL;

ALTER TABLE crm.person
 ALTER COLUMN contact_id SET NOT NULL;

ALTER TABLE crm.address 
 ALTER COLUMN contact_id SET NOT NULL;

ALTER TABLE "security"."user"
 ALTER COLUMN contact_id SET NOT NULL;

ALTER TABLE crm.country 
 ALTER COLUMN iso type varchar(2);




--Revert Changes
--rollback ALTER TABLE crm.address ALTER COLUMN country_id DROP NOT NULL;
--rollback ALTER TABLE crm.address ALTER COLUMN location SET NOT NULL;
--rollback ALTER TABLE crm.address ALTER COLUMN region SET NOT NULL;
--rollback ALTER TABLE crm.address ALTER COLUMN zip_code SET NOT NULL;
--rollback ALTER TABLE crm.country ALTER COLUMN iso type varchar(50);
--rollback ALTER TABLE crm.person ALTER COLUMN contact_id DROP NOT NULL;
--rollback ALTER TABLE crm.address ALTER COLUMN contact_id DROP NOT NULL;
--rollback ALTER TABLE "security"."user" ALTER COLUMN contact_id DROP NOT NULL;

