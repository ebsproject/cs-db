--liquibase formatted sql

--changeset postgres:update_crm_model context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-717 Update CRM Model



DROP TABLE IF EXISTS crm.address_person;

DROP TABLE IF EXISTS crm.person_type;

DROP TABLE IF EXISTS crm.type;

DROP TABLE IF EXISTS workflow.service_type_workflow;

ALTER TABLE crm.address 
 DROP COLUMN IF EXISTS type;

ALTER TABLE crm.person 
 DROP COLUMN IF EXISTS email;

ALTER TABLE crm.person 
 DROP COLUMN IF EXISTS external_id;

ALTER TABLE crm.person 
 DROP COLUMN IF EXISTS has_credential;

ALTER TABLE crm.person 
 DROP COLUMN IF EXISTS official_email;

ALTER TABLE crm.person 
 DROP COLUMN IF EXISTS phone;

ALTER TABLE crm.address 
 ALTER COLUMN location SET NOT NULL;

ALTER TABLE crm.address 
 ALTER COLUMN region SET NOT NULL;

ALTER TABLE crm.address 
 ALTER COLUMN street_address SET NOT NULL;

ALTER TABLE crm.address 
 ALTER COLUMN street_address TYPE varchar(300);

ALTER TABLE crm.address 
 ALTER COLUMN zip_code SET NOT NULL;

ALTER TABLE crm.person 
 ALTER COLUMN gender SET NOT NULL;

ALTER TABLE crm.person_status 
 ALTER COLUMN description SET NOT NULL;

ALTER TABLE crm.person_status 
 ALTER COLUMN name SET NOT NULL;

ALTER TABLE crm.address 
 ADD COLUMN contact_id integer NULL;

ALTER TABLE crm.address 
 ADD COLUMN country_id integer NULL;

ALTER TABLE crm.address 
 ADD COLUMN is_default boolean NOT NULL   DEFAULT false;

ALTER TABLE crm.person 
 ADD COLUMN contact_id integer NULL;

ALTER TABLE security."user" 
 ADD COLUMN contact_id integer NULL;

CREATE TABLE crm.contact
(
	id integer NOT NULL   DEFAULT NEXTVAL(('crm."contact_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	category varchar(11) NOT NULL,	-- Defines if its a Person or an Institution.
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE crm.contact_contact_type
(
	contact_type_id integer NOT NULL,
	contact_id integer NOT NULL
)
;

CREATE TABLE crm.contact_info
(
	id integer NOT NULL   DEFAULT NEXTVAL(('crm."contact_info_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	value varchar(500) NOT NULL,	-- Actual value of the phone or web site of the contact. e.g www.cimmyt.org, 5959521900, etc.
	is_default boolean NOT NULL   DEFAULT false,	-- Determines id it's the information default to contact the institution or the person. A person can have more that one phone registered but one of them is the principal.
	contact_id integer NOT NULL,
	info_type_id integer NOT NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE crm.contact_type
(
	id integer NOT NULL   DEFAULT NEXTVAL(('crm."contact_type_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	category varchar(11) NOT NULL,	-- Defines if the record applies to a Person or Institution.
	name varchar(50) NOT NULL,	-- Name of the relationship with the Contact, e.g Donor, Collaborator, Customer, etc.
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE crm.country
(
	id integer NOT NULL   DEFAULT NEXTVAL(('crm."country_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(150) NOT NULL,
	iso varchar(50) NOT NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE crm.info_type
(
	id integer NOT NULL   DEFAULT NEXTVAL(('crm."info_type_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(50) NOT NULL,	-- Name of the type of Information channel. Web site, phone, social network, etc.
	description varchar(250) NOT NULL,	-- Description of the information type.
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE crm.institution
(
	id integer NOT NULL   DEFAULT NEXTVAL(('crm."institution_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	common_name varchar(150) NOT NULL,	-- Common name of the institution e.g. CIMMYT, IRRI, etc.
	legal_name varchar(500) NOT NULL,	-- Legal name or full name of the Institution.
	person_id integer NOT NULL,
	contact_id integer NOT NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE SEQUENCE crm.contact_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE crm.contact_info_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE crm.contact_type_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE crm.country_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE crm.info_type_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE crm.institution_id_seq INCREMENT 1 START 1;


ALTER TABLE crm.contact ADD CONSTRAINT check_category CHECK (category='Person' OR category = 'Institution')
;

ALTER TABLE crm.contact ADD CONSTRAINT "PK_contact"
	PRIMARY KEY (id)
;

ALTER TABLE crm.contact_contact_type ADD CONSTRAINT "PK_person_type"
	PRIMARY KEY (contact_type_id,contact_id)
;

ALTER TABLE crm.contact_info ADD CONSTRAINT "PK_contact_info"
	PRIMARY KEY (id)
;

ALTER TABLE crm.contact_type ADD CONSTRAINT check_category CHECK (category='Person' OR category = 'Institution')
;

ALTER TABLE crm.contact_type ADD CONSTRAINT "PK_contact_type"
	PRIMARY KEY (id)
;

ALTER TABLE crm.country ADD CONSTRAINT "PK_country"
	PRIMARY KEY (id)
;

ALTER TABLE crm.info_type ADD CONSTRAINT "PK_info_type"
	PRIMARY KEY (id)
;

ALTER TABLE crm.institution ADD CONSTRAINT "PK_institution"
	PRIMARY KEY (id)
;

ALTER TABLE crm.address ADD CONSTRAINT "FK_address_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.address ADD CONSTRAINT "FK_address_country"
	FOREIGN KEY (country_id) REFERENCES crm.country (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact_contact_type ADD CONSTRAINT "FK_contact_contact_type_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact_contact_type ADD CONSTRAINT "FK_contact_contact_type_contact_type"
	FOREIGN KEY (contact_type_id) REFERENCES crm.contact_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact_info ADD CONSTRAINT "FK_contact_info_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact_info ADD CONSTRAINT "FK_contact_info_info_type"
	FOREIGN KEY (info_type_id) REFERENCES crm.info_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.institution ADD CONSTRAINT "FK_institution_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.institution ADD CONSTRAINT "FK_institution_person"
	FOREIGN KEY (person_id) REFERENCES crm.person (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.person ADD CONSTRAINT "FK_person_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security."user" ADD CONSTRAINT "FK_user_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;


COMMENT ON TABLE crm.contact
	IS 'Works as a point of reference to get the address, contact Info and contact type information of Person and Institution.'
;

COMMENT ON TABLE crm.contact_contact_type
	IS 'Manages person types, e.g. vendor, users, donor, collaborator, etc.'
;

COMMENT ON TABLE crm.contact_info
	IS 'Contains the contact Information, phone, web site, social network, etc. '
;

COMMENT ON TABLE crm.contact_type
	IS 'Stores different types of contacts depending of the category, e.g. An Institution can be a Collaborator, Vendor, Customer, etc. A Person can be a Sponsor, a Collaborator, etc.'
;

COMMENT ON TABLE crm.info_type
	IS 'Stores all different types of contact info. e.g Facebook, email, twitter, phone, web page etc.'
;

COMMENT ON TABLE crm.institution
	IS 'Manages all Institutions related to the System,'
;

COMMENT ON TABLE crm.person
	IS 'Stores the information of individuals (Persons) that are part or are related to the System.'
;

COMMENT ON TABLE crm.person_status
	IS 'Manages the different person status in different workflow. E.g. pending, approved, etc.'
;

COMMENT ON TABLE program.crop
	IS 'Cultivated plant that is grown as food and is the subject of research and development conducted by various programs '
;


COMMENT ON COLUMN crm.address.contact_id
	IS 'Reference to the Contact id related to the address'
;

COMMENT ON COLUMN crm.address.country_id
	IS 'Reference to the Country Id related to the address'
;

COMMENT ON COLUMN crm.address.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.address.is_default
	IS 'Determines if its the default address of a certain contact'
;

COMMENT ON COLUMN crm.address.location
	IS 'Location of the address'
;

COMMENT ON COLUMN crm.address.region
	IS 'Region of the address'
;

COMMENT ON COLUMN crm.address.street_address
	IS 'Street name'
;

COMMENT ON COLUMN crm.address.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN crm.address.zip_code
	IS 'Zip code'
;

COMMENT ON COLUMN crm.contact.category
	IS 'Defines if its a Person or an Institution.'
;

COMMENT ON COLUMN crm.contact.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN crm.contact.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN crm.contact.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.contact.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN crm.contact.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN crm.contact.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN crm.contact.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN crm.contact_info.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN crm.contact_info.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN crm.contact_info.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.contact_info.is_default
	IS 'Determines id it''s the information default to contact the institution or the person. A person can have more that one phone registered but one of them is the principal.'
;

COMMENT ON COLUMN crm.contact_info.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN crm.contact_info.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN crm.contact_info.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN crm.contact_info.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN crm.contact_info.value
	IS 'Actual value of the phone or web site of the contact. e.g www.cimmyt.org, 5959521900, etc.'
;

COMMENT ON COLUMN crm.contact_type.category
	IS 'Defines if the record applies to a Person or Institution.'
;

COMMENT ON COLUMN crm.contact_type.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN crm.contact_type.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN crm.contact_type.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.contact_type.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN crm.contact_type.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN crm.contact_type.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN crm.contact_type.name
	IS 'Name of the relationship with the Contact, e.g Donor, Collaborator, Customer, etc.'
;

COMMENT ON COLUMN crm.contact_type.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN crm.country.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN crm.country.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN crm.country.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.country.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN crm.country.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN crm.country.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN crm.info_type.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN crm.info_type.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN crm.info_type.description
	IS 'Description of the information type.'
;

COMMENT ON COLUMN crm.info_type.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.info_type.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN crm.info_type.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN crm.info_type.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN crm.info_type.name
	IS 'Name of the type of Information channel. Web site, phone, social network, etc.'
;

COMMENT ON COLUMN crm.info_type.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN crm.institution.common_name
	IS 'Common name of the institution e.g. CIMMYT, IRRI, etc.'
;

COMMENT ON COLUMN crm.institution.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN crm.institution.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN crm.institution.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.institution.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN crm.institution.legal_name
	IS 'Legal name or full name of the Institution.'
;

COMMENT ON COLUMN crm.institution.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN crm.institution.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN crm.institution.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN crm.person.contact_id
	IS 'Reference to the Contact id of the person'
;

COMMENT ON COLUMN crm.person.family_name
	IS 'First name of the person'
;

COMMENT ON COLUMN crm.person.gender
	IS 'Gender of the person. Male/Female.'
;

COMMENT ON COLUMN crm.person.given_name
	IS 'Last name of the person.'
;

COMMENT ON COLUMN crm.person.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.person.language_id
	IS 'Reference to the language id in the Language table. Indicates the preffered language for that particular person.'
;

COMMENT ON COLUMN crm.person.person_status_id
	IS 'Refers to the person_status table and indicates the current status of a particular person.'
;

COMMENT ON COLUMN crm.person.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN crm.person_status.description
	IS 'Description of the status.'
;

COMMENT ON COLUMN crm.person_status.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.person_status.name
	IS 'Refers to the different types of status that a person can have.'
;