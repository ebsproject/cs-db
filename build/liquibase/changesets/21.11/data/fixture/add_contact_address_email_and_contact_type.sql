--liquibase formatted sql

--changeset postgres:add_contact_address_email_and_conatact_type context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-812 Add contact address email and contact type
--validCheckSum: 8:86692f6d8059cb4ec9b7a18a72dbca25


INSERT INTO crm.contact_contact_type
(contact_type_id, contact_id)
(SELECT 9, id FROM crm.contact);

INSERT INTO crm.contact_info
(value, is_default, contact_id, info_type_id, creation_timestamp, creator_id, is_void)
(select user_name, true, contact_id, 2, now(), 1, false from "security"."user");


INSERT INTO crm.address
("location", region, zip_code, street_address, creation_timestamp, creator_id, is_void, contact_id, country_id, is_default)
(SELECT 'Loc', 'R', '9999', 
CASE 
    WHEN split_part(user_name, '@', 2) = 'irri.org' THEN 'Pili Drive, University of the Philippines Los Baños, Los Baños, 4030 Laguna, Filipinas'
    WHEN split_part(user_name, '@', 2) = 'gmail.com' THEN 'Pili Drive, University of the Philippines Los Baños, Los Baños, 4030 Laguna, Filipinas'
    WHEN split_part(user_name, '@', 2) = 'cornell.edu' THEN 'Ithaca, NY 14850, Estados Unidos'
    else 'Mexico-Veracruz, El Batan Km. 45, 56237 Mex.'
END,
now(), 1, false, contact_id, 
CASE 
    WHEN split_part(user_name, '@', 2) = 'irri.org' THEN 167
    WHEN split_part(user_name, '@', 2) = 'cornell.edu' THEN 222
    WHEN split_part(user_name, '@', 2) = 'gmail.com' THEN 167
    else 137
end,
true
FROM "security"."user");


--Revert Changes
--rollback DELETE FROM crm.contact_contact_type;
--rollback DELETE FROM crm.contact_info;
--rollback DELETE from crm.address where id > 1;
--rollback alter sequence crm.contact_info_id_seq restart with 1;
--rollback alter sequence crm.address_id_seq restart with 2;
