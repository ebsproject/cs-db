--liquibase formatted sql

--changeset postgres:add_new_dev_users context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-811 Add two new users into the csdb



SET session_replication_role = 'replica';

INSERT INTO crm.contact
(id, category, creation_timestamp, creator_id, is_void)
VALUES
(71, 'Person', now(), 1, false),
(72, 'Person', now(), 1, false)
;

INSERT INTO crm.person
(id, given_name, family_name, additional_name, gender, job_title, knows_about,  creation_timestamp, creator_id, is_void, language_id, contact_id)
VALUES
(71, 'Alfonso', 'Caballero Ramirez', 'Joseph',  'male', 'Systems Software Engineer', 'IT', now(), 1, false, 1, 71),
(72, 'Rey', 'Peralta Rodriguez', 'David',  'male', 'Systems Software Engineer', 'IT', now(), 1, false, 1, 72);

INSERT INTO "security"."user"
(id, user_name, default_role_id, is_is, creation_timestamp, creator_id, is_void, person_id, contact_id)
VALUES
(73, 'a.caballero@cgiar.org', 2, 1, now(), 1, false, 71, 71),
(74, 'r.peralta@cgiar.org', 2, 1, now(), 1, false, 72, 72);

INSERT INTO "security".user_role
(role_id, user_id)
VALUES
(2, 73),
(2, 74);

INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(73, 1);
INSERT INTO "security".tenant_user (user_id, tenant_id) VALUES(74, 1);


SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));
SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));
select setval('crm.contact_id_seq',max(id)) from crm.contact;

SET session_replication_role = 'origin';



--Revert Changes
--rollback DELETE FROM "security".tenant_user WHERE user_id=73;
--rollback DELETE FROM "security".tenant_user WHERE user_id=74;

--rollback DELETE FROM "security".user_role where role_id = 2 and user_id = 73;
--rollback DELETE FROM "security".user_role where role_id = 2 and user_id = 74;

--rollback DELETE FROM "security"."user" where id = 73;
--rollback DELETE FROM "security"."user" where id = 74;

--rollback DELETE FROM crm.person WHERE id = 71;
--rollback DELETE FROM crm.person WHERE id = 72;

--rollback DELETE FROM crm.contact WHERE id = 71;
--rollback DELETE FROM crm.contact WHERE id = 72;

--rollback SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));
--rollback SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));
--rollback SELECT setval('crm.contact_id_seq',max(id)) from crm.contact;