--liquibase formatted sql

--changeset postgres:add_phone_numbers_to_contacts context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-854 Insert phone numbers for contacts in CSDB



SET session_replication_role = 'replica';

SELECT setval('crm.contact_info_id_seq', (SELECT MAX(id) FROM crm.contact_info));

INSERT INTO crm.contact_info
(value, is_default, contact_id, info_type_id, creation_timestamp, creator_id, is_void)
VALUES
('733-716-0701', true, 1, 1, now(), 1, false),
('157-538-0713', true, 2, 1, now(), 1, false),
('388-674-4093', true, 3, 1, now(), 1, false),
('541-560-5019', true, 4, 1, now(), 1, false),
('206-418-9384', true, 5, 1, now(), 1, false),
('477-488-5214', true, 6, 1, now(), 1, false),
('847-943-4907', true, 7, 1, now(), 1, false),
('780-511-7732', true, 8, 1, now(), 1, false),
('829-256-4935', true, 9, 1, now(), 1, false),
('321-524-1356', true, 10, 1, now(), 1, false),
('213-352-8003', true, 11, 1, now(), 1, false),
('413-609-8915', true, 12, 1, now(), 1, false),
('714-154-8458', true, 13, 1, now(), 1, false),
('691-416-4235', true, 14, 1, now(), 1, false),
('174-332-7966', true, 15, 1, now(), 1, false),
('379-778-3419', true, 16, 1, now(), 1, false),
('523-144-7559', true, 17, 1, now(), 1, false),
('272-554-0103', true, 18, 1, now(), 1, false),
('833-292-6277', true, 19, 1, now(), 1, false),
('854-415-5872', true, 20, 1, now(), 1, false),
('533-317-6401', true, 21, 1, now(), 1, false),
('546-659-1724', true, 22, 1, now(), 1, false),
('297-500-4309', true, 23, 1, now(), 1, false),
('886-844-4223', true, 24, 1, now(), 1, false),
('247-204-6706', true, 25, 1, now(), 1, false),
('584-661-8289', true, 26, 1, now(), 1, false),
('204-119-5636', true, 27, 1, now(), 1, false),
('783-109-5906', true, 28, 1, now(), 1, false),
('460-536-5276', true, 29, 1, now(), 1, false),
('881-630-8337', true, 30, 1, now(), 1, false),
('904-798-6303', true, 31, 1, now(), 1, false),
('526-895-2389', true, 32, 1, now(), 1, false),
('801-382-0635', true, 33, 1, now(), 1, false),
('330-802-1417', true, 34, 1, now(), 1, false),
('944-916-0862', true, 35, 1, now(), 1, false),
('161-220-5102', true, 36, 1, now(), 1, false),
('565-894-3404', true, 37, 1, now(), 1, false),
('562-846-2070', true, 38, 1, now(), 1, false),
('833-166-7116', true, 39, 1, now(), 1, false),
('920-706-1612', true, 40, 1, now(), 1, false),
('463-665-0539', true, 41, 1, now(), 1, false),
('861-111-9502', true, 42, 1, now(), 1, false),
('840-277-3373', true, 43, 1, now(), 1, false),
('982-658-4563', true, 44, 1, now(), 1, false),
('252-301-3144', true, 45, 1, now(), 1, false),
('815-759-1245', true, 46, 1, now(), 1, false),
('947-737-3818', true, 47, 1, now(), 1, false),
('599-217-5918', true, 48, 1, now(), 1, false),
('556-481-5973', true, 49, 1, now(), 1, false),
('159-177-0153', true, 50, 1, now(), 1, false),
('134-661-3402', true, 51, 1, now(), 1, false),
('820-612-4968', true, 52, 1, now(), 1, false),
('534-624-5377', true, 53, 1, now(), 1, false),
('203-574-5121', true, 54, 1, now(), 1, false),
('443-638-7882', true, 55, 1, now(), 1, false),
('845-424-0991', true, 56, 1, now(), 1, false),
('140-294-9528', true, 57, 1, now(), 1, false),
('411-730-2022', true, 58, 1, now(), 1, false),
('493-545-8099', true, 59, 1, now(), 1, false),
('536-168-1804', true, 60, 1, now(), 1, false),
('695-884-8841', true, 61, 1, now(), 1, false),
('503-303-6144', true, 62, 1, now(), 1, false),
('514-333-6795', true, 63, 1, now(), 1, false),
('126-200-1114', true, 64, 1, now(), 1, false),
('236-470-2749', true, 65, 1, now(), 1, false),
('482-244-6029', true, 66, 1, now(), 1, false),
('276-807-0771', true, 67, 1, now(), 1, false),
('565-387-7993', true, 68, 1, now(), 1, false),
('214-862-4710', true, 69, 1, now(), 1, false),
('466-814-9844', true, 70, 1, now(), 1, false),
('795-677-3196', true, 71, 1, now(), 1, false),
('452-441-4562', true, 72, 1, now(), 1, false);

SELECT setval('crm.contact_info_id_seq', (SELECT MAX(id) FROM crm.contact_info));

SET session_replication_role = 'origin';



--Revert Changes
--rollback DELETE FROM crm.contact_info WHERE info_type_id = 1;
--rollback SELECT setval('crm.contact_info_id_seq', (SELECT MAX(id) FROM crm.contact_info));