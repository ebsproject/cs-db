--liquibase formatted sql

--changeset postgres:link_admin_person_to_contact context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-861 Add System Account as Contact type



INSERT INTO crm.person
(id, family_name, given_name, gender, job_title, knows_about, creation_timestamp, creator_id, is_void, language_id, contact_id)
VALUES(0, 'System', 'Account', 'System', 'System Account', 'IT', now(), 1, false, 1, 0);

INSERT INTO crm.contact_contact_type
(contact_type_id, contact_id)
VALUES(10, 0);


SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));


--Revert Changes
--rollback DELETE FROM crm.contact_contact_type where contact_type_id = 10 and contact_id = 0;
--rollback DELETE FROM crm.person WHERE id = 0;
--rollback SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));