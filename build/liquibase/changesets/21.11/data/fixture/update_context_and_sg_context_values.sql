--liquibase formatted sql

--changeset postgres:update_context_and_sg_context_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-847 update sg_context values on csdb domain_instance table


UPDATE core.domain_instance
SET sg_context='https://cbapi-dev.ebsproject.org/v3/' WHERE id=1;
UPDATE core.domain_instance
SET context='https://ebs-demo.cimmyt.org:81/api-cs', sg_context='https://ebs-demo.cimmyt.org:81/api-cs' WHERE id=5;
UPDATE core.domain_instance
SET sg_context='https://baapi-dev.ebsproject.org/v1/' WHERE id=6;
UPDATE core.domain_instance
SET sg_context='https://ebs-demo.cimmyt.org:81/api-sm' WHERE id=4;


--Revert Changes
--rollback UPDATE core.domain_instance SET sg_context='https://cbapi-dev.ebsproject.org' WHERE id=1;
--rollback UPDATE core.domain_instance SET context='https://dev.ebsproject.org', sg_context='http://localhost:8080/graphql' WHERE id=5;
--rollback UPDATE core.domain_instance SET sg_context='https://cbapi-dev.ebsproject.org' WHERE id=6;
--rollback UPDATE core.domain_instance SET sg_context='http://localhost:8082/graphql' WHERE id=4;
