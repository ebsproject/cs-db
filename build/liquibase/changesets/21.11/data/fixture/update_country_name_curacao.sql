--liquibase formatted sql

--changeset postgres:update_country_name_curacao context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-822 Update Country name Curacao


UPDATE crm.country
SET "name"='Curacao' WHERE id=244;


--Revert Changes
--rollback UPDATE crm.country SET "name"='CuraÃ§ao' WHERE id=244;