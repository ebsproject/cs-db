--liquibase formatted sql

--changeset postgres:update_person_contact_info_test_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-716 Clean CS Contacts related to users



SET session_replication_role = 'replica';

DELETE FROM crm.person;

INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Aguirre', 'Abel', 'Casey', 'Male', 'Database Engineer. ', 'IT', '2008-09-24 12:14:49.984', '2008-09-24 11:27:35.552', 1, 1, false, 1, 1, 1);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Lyons', 'Erick', 'Colleen', 'Female', 'Systems Administrator. ', 'IT', '2008-05-15 06:44:07.936', '2008-08-21 17:54:52.160', 1, 1, false, 2, 1, 2);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Hendricks', 'Janice', 'Bernard', 'Female', 'Database Engineer. ', 'IT', '2008-07-31 04:25:39.200', '2008-10-01 05:42:21.056', 1, 1, false, 3, 1, 3);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Sexton', 'Gretchen', 'Hector', 'Male', 'Systems Administrator. ', 'IT', '2008-03-17 07:45:56.736', '2008-01-04 12:34:50.560', 1, 1, false, 4, 1, 4);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Owens', 'Lawanda', 'Mike', 'Female', 'Database Administrator. ', 'IT', '2008-10-11 19:03:35.808', '2008-05-07 12:33:08.736', 1, 1, false, 5, 1, 5);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Mendoza', 'Robbie', 'Ruby', 'Male', 'Systems Administrator. ', 'IT', '2008-10-06 10:52:56.832', '2008-06-28 14:28:27.904', 1, 1, false, 6, 1, 6);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Patton', 'Carla', 'Duane', 'Female', 'Database Administrator. ', 'IT', '2008-02-12 02:08:03.840', '2008-11-24 09:40:54.912', 1, 1, false, 7, 1, 7);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Chang', 'Heath', NULL, 'Female', 'Systems Administrator. ', 'IT', '2008-03-14 11:11:35.424', '2008-09-28 19:53:49.440', 1, 1, false, 8, 1, 8);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Flores', 'Kendra', 'Salvatore', 'Female', 'Systems Administrator. ', 'IT', '2008-05-03 17:41:51.872', '2008-09-16 05:37:41.504', 1, 1, false, 9, 1, 9);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Snow', 'Brandie', 'Sammy', 'Male', 'Systems Engineer. ', 'IT', '2008-08-22 06:16:24.960', '2008-07-27 08:24:37.248', 1, 1, false, 10, 1, 10);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Zhang', 'Rose', 'Sarah', 'Male', 'Systems Administrator. ', 'IT', '2008-09-05 13:28:32.640', '2008-05-08 20:13:09.632', 1, 1, false, 11, 1, 11);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Conner', 'Ernest', 'Candice', 'Male', 'Systems Engineer. ', 'IT', '2008-01-20 06:52:59.264', '2008-12-14 16:07:55.136', 1, 1, false, 12, 1, 12);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Castro', 'Randal', 'Travis', 'Female', 'Systems Engineer. ', 'IT', '2008-07-17 11:51:50.656', '2008-02-14 04:32:42.240', 1, 1, false, 13, 1, 13);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Morse', 'Ismael', 'Guy', 'Female', 'Systems Administrator. ', 'IT', '2008-09-15 11:33:39.072', '2008-04-02 11:23:17.888', 1, 1, false, 14, 1, 14);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Burch', 'Keri', 'Cara', 'Male', 'Systems Engineer. ', 'IT', '2008-11-23 12:42:25.152', '2008-11-07 01:38:11.072', 1, 1, false, 15, 1, 15);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Dunlap', 'Cameron', 'Chris', 'Female', 'Systems Administrator. ', 'IT', '2008-02-15 20:51:19.936', '2008-09-11 23:44:22.656', 1, 1, false, 16, 1, 16);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Mc Clure', 'Roberta', 'Jennifer', 'Male', 'Database Engineer. ', 'IT', '2008-01-06 13:34:51.040', '2008-01-31 11:00:06.784', 1, 1, false, 17, 1, 17);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Mc Neil', 'Colby', 'Jeremiah', 'Female', 'Systems Administrator. ', 'IT', '2008-10-19 09:49:36.768', '2008-09-08 08:51:52.576', 1, 1, false, 18, 1, 18);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Heath', 'Cornelius', 'Gilbert', 'Female', 'Systems Engineer. ', 'IT', '2008-12-07 10:43:34.016', '2008-12-05 22:07:23.648', 1, 1, false, 19, 1, 19);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Erickson', 'Teddy', 'Lonnie', 'Male', 'Database Engineer. ', 'IT', '2008-10-19 04:22:14.400', '2008-02-10 00:09:56.480', 1, 1, false, 20, 1, 20);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Brewer', 'Lillian', 'Candice', 'Male', 'Database Administrator. ', 'IT', '2008-07-09 03:21:45.344', '2008-08-22 20:56:46.976', 1, 1, false, 21, 1, 21);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Novak', 'Dianna', 'Darrick', 'Female', 'Database Engineer. ', 'IT', '2008-07-07 12:40:11.648', '2008-02-12 11:05:24.224', 1, 1, false, 22, 1, 22);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Brock', 'Geoffrey', 'Jerome', 'Female', 'Database Engineer. ', 'IT', '2008-07-26 05:57:51.872', '2008-10-14 06:19:48.736', 1, 1, false, 23, 1, 23);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Mcpherson', 'Melissa', 'Chanda', 'Male', 'Database Administrator. ', 'IT', '2008-12-13 22:58:39.744', '2008-09-28 15:19:03.040', 1, 1, false, 24, 1, 24);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Mac Donald', 'Moses', 'Brad', 'Male', 'Database Administrator. ', 'IT', '2008-06-10 15:14:41.920', '2008-08-03 14:29:33.440', 1, 1, false, 25, 1, 25);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Bailey', 'Franklin', 'Alexander', 'Female', 'Database Administrator. ', 'IT', '2008-01-13 00:22:54.720', '2008-09-12 00:26:42.176', 1, 1, false, 26, 1, 26);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Brewer', 'Arlene', 'Ann', 'Female', 'Systems Administrator. ', 'IT', '2008-12-28 05:46:49.728', '2008-06-22 03:36:32.128', 1, 1, false, 27, 1, 27);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Crane', 'Kathleen', NULL, 'Male', 'Systems Administrator. ', 'IT', '2008-09-23 07:42:19.776', '2008-08-04 04:58:02.752', 1, 1, false, 28, 1, 28);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Leblanc', 'Sheldon', 'Esther', 'Female', 'Database Administrator. ', 'IT', '2008-09-07 19:44:49.792', '2008-04-03 02:50:25.152', 1, 1, false, 29, 1, 29);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Schaefer', 'Ivan', 'Jesse', 'Female', 'Database Engineer. ', 'IT', '2008-01-26 08:40:56.832', '2008-08-07 09:30:28.864', 1, 1, false, 30, 1, 30);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('O''Connell', 'Andre', 'Kellie', 'Male', 'Database Engineer. ', 'IT', '2008-07-30 01:43:17.888', '2008-05-20 02:03:23.136', 1, 1, false, 31, 1, 31);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Montgomery', 'Joni', 'Andre', 'Male', 'Systems Engineer. ', 'IT', '2008-08-09 00:24:37.248', '2008-08-16 11:38:25.792', 1, 1, false, 32, 1, 32);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Booth', 'Rex', 'Damien', 'Male', 'Database Administrator. ', 'IT', '2008-03-19 19:08:55.680', '2008-10-09 02:47:59.872', 1, 1, false, 33, 1, 33);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Park', 'Dwight', 'Alexander', 'Male', 'Database Administrator. ', 'IT', '2008-10-31 09:46:18.496', '2008-08-15 08:59:41.568', 1, 1, false, 34, 1, 34);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Randolph', 'Lucas', 'Miguel', 'Female', 'Systems Administrator. ', 'IT', '2008-09-08 23:26:34.624', '2008-03-21 12:01:19.872', 1, 1, false, 35, 1, 35);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Rose', 'Arnold', NULL, 'Male', 'Database Engineer. ', 'IT', '2008-03-27 21:42:47.552', '2008-09-18 01:52:32.896', 1, 1, false, 36, 1, 36);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Stephens', 'Kristen', 'Anna', 'Female', 'Systems Engineer. ', 'IT', '2008-05-06 12:49:08.224', '2008-04-24 14:30:43.072', 1, 1, false, 37, 1, 37);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Barr', 'Bobbi', 'Deanna', 'Female', 'Systems Administrator. ', 'IT', '2008-03-26 06:49:47.776', '2008-11-04 04:41:07.328', 1, 1, false, 38, 1, 38);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Mullins', 'Carl', 'Kristy', 'Male', 'Database Administrator. ', 'IT', '2008-07-03 05:13:24.352', '2008-11-13 04:46:17.600', 1, 1, false, 39, 1, 39);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Clayton', 'Frances', NULL, 'Female', 'Database Engineer. ', 'IT', '2008-10-13 04:34:39.872', '2008-09-07 12:42:33.984', 1, 1, false, 40, 1, 40);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Vargas', 'Elena', 'Scottie', 'Male', 'Database Engineer. ', 'IT', '2008-05-26 03:20:13.184', '2008-02-07 20:15:03.168', 1, 1, false, 41, 1, 41);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Sharp', 'Luz', 'Deborah', 'Female', 'Database Engineer. ', 'IT', '2008-03-18 15:59:04.704', '2008-12-22 13:15:04.064', 1, 1, false, 42, 1, 42);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Glover', 'Jami', NULL, 'Female', 'Systems Engineer. ', 'IT', '2008-02-15 02:23:15.968', '2008-06-10 12:39:19.424', 1, 1, false, 43, 1, 43);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Medina', 'Ruth', 'Jon', 'Male', 'Systems Engineer. ', 'IT', '2008-02-21 23:33:34.336', '2008-04-12 05:29:42.272', 1, 1, false, 44, 1, 44);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Rice', 'Noah', 'Latonya', 'Male', 'Systems Administrator. ', 'IT', '2008-04-06 14:17:50.976', '2008-07-10 07:27:16.608', 1, 1, false, 45, 1, 45);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Saunders', 'Emma', 'Kenny', 'Male', 'Database Engineer. ', 'IT', '2008-09-21 20:46:05.952', '2008-08-05 18:20:28.160', 1, 1, false, 46, 1, 46);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Mahoney', 'Damien', 'Melissa', 'Female', 'Database Administrator. ', 'IT', '2008-04-24 15:25:49.568', '2008-02-07 17:38:32.832', 1, 1, false, 47, 1, 47);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Frederick', 'Caroline', 'Claude', 'Female', 'Systems Administrator. ', 'IT', '2008-06-23 13:13:35.616', '2008-12-06 10:44:59.008', 1, 1, false, 48, 1, 48);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Horne', 'Elisabeth', 'Penny', 'Male', 'Systems Engineer. ', 'IT', '2008-09-02 12:10:52.416', '2008-08-06 16:12:12.800', 1, 1, false, 49, 1, 49);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Reid', 'Morgan', 'Bobbie', 'Female', 'Database Engineer. ', 'IT', '2008-05-25 10:07:15.584', '2008-02-02 17:26:06.336', 1, 1, false, 50, 1, 50);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Fisher', 'Clifford', 'Mandy', 'Female', 'Database Administrator. ', 'IT', '2008-06-11 23:14:30.656', '2008-09-30 21:57:23.200', 1, 1, false, 51, 1, 51);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Newman', 'Guadalupe', 'Lynn', 'Male', 'Systems Engineer. ', 'IT', '2008-02-20 10:48:18.688', '2008-01-01 05:44:51.794', 1, 1, false, 52, 1, 52);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('George', 'Sophia', 'Christi', 'Male', 'Systems Administrator. ', 'IT', '2008-06-08 21:28:29.568', '2008-05-23 10:26:55.232', 1, 1, false, 53, 1, 53);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Ellison', 'Angela', 'Xavier', 'Male', 'Systems Engineer. ', 'IT', '2008-06-04 14:12:23.296', '2008-10-10 13:12:20.864', 1, 1, false, 54, 1, 54);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Bowman', 'Byron', 'Sheri', 'Male', 'Database Administrator. ', 'IT', '2008-03-10 10:58:47.424', '2008-11-01 14:35:26.080', 1, 1, false, 55, 1, 55);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Wyatt', 'Jesse', 'Cedric', 'Female', 'Database Engineer. ', 'IT', '2008-03-27 13:29:39.072', '2008-11-11 15:33:09.248', 1, 1, false, 56, 1, 56);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Brown', 'Paige', 'Luke', 'Female', 'Systems Administrator. ', 'IT', '2008-01-08 09:58:09.792', '2008-06-18 15:41:25.504', 1, 1, false, 57, 1, 57);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Duncan', 'Herman', 'Elena', 'Female', 'Systems Engineer. ', 'IT', '2008-05-13 04:02:24.512', '2008-10-09 18:58:06.080', 1, 1, false, 58, 1, 58);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Lara', 'Clarissa', 'Manuel', 'Male', 'Database Engineer. ', 'IT', '2008-04-29 15:18:03.648', '2008-06-13 05:38:29.632', 1, 1, false, 59, 1, 59);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Arroyo', 'Loretta', 'Crystal', 'Female', 'Database Administrator. ', 'IT', '2008-05-12 14:06:43.328', '2008-06-13 19:48:07.424', 1, 1, false, 60, 1, 60);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Calderon', 'Austin', 'Chasity', 'Male', 'Systems Administrator. ', 'IT', '2008-04-08 04:27:29.280', '2008-11-30 03:17:21.536', 1, 1, false, 61, 1, 61);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Mann', 'Deana', 'Marianne', 'Male', 'Systems Administrator. ', 'IT', '2008-01-27 21:54:57.408', '2008-12-16 05:05:45.984', 1, 1, false, 62, 1, 62);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Steele', 'William', 'Devon', 'Female', 'Systems Administrator. ', 'IT', '2008-08-24 20:44:56.320', '2008-02-10 01:01:54.048', 1, 1, false, 63, 1, 63);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Ortiz', 'Bridgett', 'Jessie', 'Female', 'Systems Administrator. ', 'IT', '2008-05-01 09:04:46.720', '2008-08-30 05:45:08.992', 1, 1, false, 64, 1, 64);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Wilkinson', 'Tamiko', 'Trenton', 'Female', 'Database Administrator. ', 'IT', '2008-04-22 03:52:15.232', '2008-08-08 08:53:25.760', 1, 1, false, 65, 1, 65);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Chavez', 'Irma', 'Darin', 'Male', 'Database Engineer. ', 'IT', '2008-04-20 20:36:50.944', '2008-12-03 11:07:54.240', 1, 1, false, 66, 1, 66);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Duarte', 'Kellie', 'Rachel', 'Male', 'Database Engineer. ', 'IT', '2008-03-06 11:09:38.688', '2008-03-12 23:43:04.192', 1, 1, false, 67, 1, 67);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Chase', 'Alex', 'Carlton', 'Female', 'Systems Administrator. ', 'IT', '2008-08-12 12:41:59.168', '2008-09-28 01:05:45.088', 1, 1, false, 68, 1, 68);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Lowe', 'Whitney', 'Rita', 'Female', 'Systems Administrator. ', 'IT', '2008-11-22 21:26:43.456', '2008-08-10 15:51:27.616', 1, 1, false, 69, 1, 69);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Roy', 'Marc', 'Marcia', 'Male', 'Systems Engineer. ', 'IT', '2008-09-13 12:40:39.296', '2008-11-30 13:01:22.816', 1, 1, false, 70, 1, 70);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Snyder', 'Dwayne', 'Tyler', 'Female', 'Systems Administrator. ', 'IT', '2008-01-28 22:17:37.536', '2008-08-12 02:44:24.832', 1, 1, false, 71, 1, 71);
INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
VALUES('Bates', 'Theodore', 'Grace', 'Female', 'Systems Engineer. ', 'IT', '2008-08-02 15:03:52.704', '2008-01-17 20:17:39.840', 1, 1, false, 72, 1, 72);


DELETE FROM crm.contact_info;

INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(1, 'avbs.cpyhf@---dd-.net', true, 1, 2, '2008-03-05 15:16:21.120', '2008-05-15 06:44:07.936', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(2, 'khdg.pyhj@--fqhj.com', true, 2, 2, '2008-02-02 07:15:41.952', '2008-07-31 04:25:39.200', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(3, 'majg715@jl-e-x.org', true, 3, 2, '2008-08-11 23:12:34.944', '2008-03-17 07:45:56.736', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(4, 'qjxi740@-yn-s-.com', true, 4, 2, '2008-06-27 23:32:14.592', '2008-10-11 19:03:35.808', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(5, 'ghqj13@gav---.com', true, 5, 2, '2008-12-11 20:39:23.904', '2008-10-06 10:52:56.832', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(6, 'ihco2@yqo-fz.net', true, 6, 2, '2008-06-18 01:46:39.616', '2008-02-12 02:08:03.840', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(7, 'fwvh@--voxs.com', true, 7, 2, '2008-10-29 10:18:17.472', '2008-03-14 11:11:35.424', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(8, 'tjow@gl--wh.org', true, 8, 2, '2008-06-13 05:29:17.696', '2008-05-03 17:41:51.872', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(9, 'xmco@qvuablh.jb-oyu.com', true, 9, 2, '2008-06-05 12:41:00.800', '2008-08-22 06:16:24.960', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(10, 'exbjk40@--mlm-.org', true, 10, 2, '2008-09-12 09:42:17.472', '2008-09-05 13:28:32.640', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(11, 'kidb@--e-z-.com', true, 11, 2, '2008-10-02 11:37:47.904', '2008-01-20 06:52:59.264', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(12, 'cfki836@-yrw-o.net', true, 12, 2, '2008-04-11 16:54:28.224', '2008-07-17 11:51:50.656', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(13, 'zpnf.fwet@twjrlhkf.j---o-.com', true, 13, 2, '2008-05-06 14:02:37.568', '2008-09-15 11:33:39.072', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(14, 'xgqm851@o--q-h.org', true, 14, 2, '2008-10-05 04:29:42.912', '2008-11-23 12:42:25.152', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(15, 'cojx@x-----.com', true, 15, 2, '2008-11-03 02:30:34.752', '2008-02-15 20:51:19.936', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(16, 'zyyj@h--x--.net', true, 16, 2, '2008-03-21 05:50:36.544', '2008-01-06 13:34:51.040', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(17, 'lssm6@-n--zg.org', true, 17, 2, '2008-06-18 06:56:28.288', '2008-10-19 09:49:36.768', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(18, 'duup54@-i-e-v.net', true, 18, 2, '2008-12-02 20:54:58.816', '2008-12-07 10:43:34.016', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(19, 'nimj01@b-himp.net', true, 19, 2, '2008-09-24 18:45:23.200', '2008-10-19 04:22:14.400', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(20, 'kvmb@-v-lto.org', true, 20, 2, '2008-11-20 07:06:50.240', '2008-07-09 03:21:45.344', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(21, 'jksb5@s-cwqu.com', true, 21, 2, '2008-04-26 10:55:51.936', '2008-07-07 12:40:11.648', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(22, 'oivpx72@-xqg--.com', true, 22, 2, '2008-03-22 06:03:41.952', '2008-07-26 05:57:51.872', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(23, 'cegg958@v----x.com', true, 23, 2, '2008-04-28 21:59:48.608', '2008-12-13 22:58:39.744', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(24, 'cesm6@dml-l-.net', true, 24, 2, '2008-11-19 11:59:20.576', '2008-06-10 15:14:41.920', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(25, 'qwqyw@-t----.org', true, 25, 2, '2008-01-09 21:34:19.072', '2008-01-13 00:22:54.720', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(26, 'ofxb.ijim@---y--.com', true, 26, 2, '2008-06-09 21:15:56.928', '2008-12-28 05:46:49.728', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(27, 'losdb.ccsjstdiq@-sd-fw.com', true, 27, 2, '2008-04-25 23:29:31.776', '2008-09-23 07:42:19.776', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(28, 'fbxq6@mhpx.gbto-j.com', true, 28, 2, '2008-05-23 20:01:34.336', '2008-09-07 19:44:49.792', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(29, 'dizl@---b--.net', true, 29, 2, '2008-08-29 09:09:33.440', '2008-01-26 08:40:56.832', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(30, 'pbyk@---ptk.com', true, 30, 2, '2008-12-30 01:43:31.584', '2008-07-30 01:43:17.888', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(31, 'lehy.gcma@wzvo--.org', true, 31, 2, '2008-08-31 10:06:00.832', '2008-08-09 00:24:37.248', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(32, 'plwen@b--my-.com', true, 32, 2, '2008-11-18 15:06:10.304', '2008-03-19 19:08:55.680', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(33, 'ymhi78@iatnb-.org', true, 33, 2, '2008-03-14 04:26:28.736', '2008-10-31 09:46:18.496', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(34, 'ejnh@hzfla.f-c--l.net', true, 34, 2, '2008-04-13 16:10:46.784', '2008-09-08 23:26:34.624', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(35, 'kiby@mv-l-g.org', true, 35, 2, '2008-08-31 08:12:04.608', '2008-03-27 21:42:47.552', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(36, 'mbxnf3@-m--qi.com', true, 36, 2, '2008-03-31 16:53:51.744', '2008-05-06 12:49:08.224', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(37, 'doai@bdf--h.com', true, 37, 2, '2008-01-23 00:23:43.872', '2008-03-26 06:49:47.776', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(38, 'emfsk@----ds.net', true, 38, 2, '2008-08-05 06:52:06.144', '2008-07-03 05:13:24.352', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(39, 'tvbx@n--rqw.com', true, 39, 2, '2008-06-28 04:58:25.280', '2008-10-13 04:34:39.872', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(40, 'ktdf787@-j-m--.net', true, 40, 2, '2008-07-06 09:04:45.696', '2008-05-26 03:20:13.184', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(41, 'ndbd.hvtxr@----ed.com', true, 41, 2, '2008-07-01 08:47:02.784', '2008-03-18 15:59:04.704', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(42, 'kolie634@hpbr--.org', true, 42, 2, '2008-01-29 06:11:26.592', '2008-02-15 02:23:15.968', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(43, 'jojg477@k--i-j.org', true, 43, 2, '2008-07-21 15:21:56.096', '2008-02-21 23:33:34.336', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(44, 'tshi@q---r-.com', true, 44, 2, '2008-01-15 16:46:03.136', '2008-04-06 14:17:50.976', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(45, 'fsde@q--hw-.org', true, 45, 2, '2008-05-03 13:29:13.600', '2008-09-21 20:46:05.952', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(46, 'gdksw67@------.com', true, 46, 2, '2008-09-07 01:03:55.520', '2008-04-24 15:25:49.568', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(47, 'yusc.rgio@-rsj--.org', true, 47, 2, '2008-11-02 07:47:43.744', '2008-06-23 13:13:35.616', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(48, 'ehxb3@s---n-.net', true, 48, 2, '2008-06-14 00:14:51.520', '2008-09-02 12:10:52.416', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(49, 'vaaxx.pfhwef@h---n-.com', true, 49, 2, '2008-06-22 11:03:46.048', '2008-05-25 10:07:15.584', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(50, 'axcj223@uevx--.net', true, 50, 2, '2008-05-09 19:23:08.288', '2008-06-11 23:14:30.656', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(51, 'yymk@r--c--.net', true, 51, 2, '2008-09-22 20:37:55.456', '2008-02-20 10:48:18.688', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(52, 'phkrh.korfp@-s-g--.net', true, 52, 2, '2008-04-20 12:23:46.560', '2008-06-08 21:28:29.568', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(53, 'syax4@----ql.org', true, 53, 2, '2008-06-22 11:12:23.168', '2008-06-04 14:12:23.296', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(54, 'glwdz42@z--v-l.net', true, 54, 2, '2008-02-15 21:48:43.648', '2008-03-10 10:58:47.424', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(55, 'uauq8@s-ewf-.org', true, 55, 2, '2008-02-11 19:40:09.856', '2008-03-27 13:29:39.072', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(56, 'twkb@r-sr-j.com', true, 56, 2, '2008-05-08 01:07:57.184', '2008-01-08 09:58:09.792', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(57, 'nbuf@rp--cl.com', true, 57, 2, '2008-08-04 00:49:33.312', '2008-05-13 04:02:24.512', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(58, 'kifn@y-y---.com', true, 58, 2, '2008-09-10 10:19:21.600', '2008-04-29 15:18:03.648', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(59, 'bkrv75@--jf--.org', true, 59, 2, '2008-08-17 07:10:52.544', '2008-05-12 14:06:43.328', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(60, 'pqzk.bwsb@t-j-i-.net', true, 60, 2, '2008-03-15 02:57:01.440', '2008-04-08 04:27:29.280', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(61, 'fvps6@-d-lr-.com', true, 61, 2, '2008-09-21 20:27:21.600', '2008-01-27 21:54:57.408', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(62, 'qash5@-----c.com', true, 62, 2, '2008-01-13 11:35:46.176', '2008-08-24 20:44:56.320', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(63, 'dfkrd@--t-jh.com', true, 63, 2, '2008-04-27 23:05:54.560', '2008-05-01 09:04:46.720', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(64, 'qqkti@--ur-d.org', true, 64, 2, '2008-03-11 17:55:46.304', '2008-04-22 03:52:15.232', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(65, 'tfto5@------.net', true, 65, 2, '2008-02-28 18:09:59.296', '2008-04-20 20:36:50.944', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(66, 'xzck.scky@b-de--.com', true, 66, 2, '2008-03-26 00:49:51.104', '2008-03-06 11:09:38.688', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(67, 'ztdx@-b-w-r.com', true, 67, 2, '2008-12-30 00:53:14.880', '2008-08-12 12:41:59.168', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(68, 'kwbq591@-e-nre.net', true, 68, 2, '2008-01-15 01:40:57.088', '2008-11-22 21:26:43.456', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(69, 'gwyx@--bh-l.org', true, 69, 2, '2008-05-04 16:42:49.856', '2008-09-13 12:40:39.296', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(70, 'pbru@g--dhr.com', true, 70, 2, '2008-09-07 09:37:58.400', '2008-01-28 22:17:37.536', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(71, 'fpbq38@-o-z--.com', true, 71, 2, '2008-12-19 08:45:15.648', '2008-08-02 15:03:52.704', 1, 1, false);
INSERT INTO crm.contact_info
(id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(72, 'dkbqs.eoxm@--w-pb.com', true, 72, 2, '2008-09-24 09:53:10.784', '2008-09-24 12:14:49.984', 1, 1, false);

SET session_replication_role = 'origin';



-- Revert Changes
--rollback DELETE FROM crm.person;

--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Go', 'Lois', NULL, 'male', 'Software Tester', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 24, 1, 24);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Briones', 'Ernesto', NULL, 'male', 'IT Team Lead', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 1, 1, 1);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Karkkainen', 'Marko', NULL, 'male', 'IT Team Lead', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 2, 1, 2);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Ramos', 'Jahzeel', NULL, 'female', 'Software Project Manager', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 3, 1, 3);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Delarosa', 'Karen', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 4, 1, 4);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Antonio', 'Joanie', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 5, 1, 5);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Gallardo', 'Larise', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 6, 1, 6);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Banasihan', 'Erica', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 7, 1, 7);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Pasang', 'Maria Johsah', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 8, 1, 8);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Crisostomo', 'Aldi', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 9, 1, 9);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Caneda', 'Alex', NULL, 'male', 'Software Project Manager', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 10, 1, 10);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Calaminos', 'Yanii', NULL, 'female', 'Software Project Manager', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 11, 1, 11);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Carumba', 'Nikki', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 12, 1, 12);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Castillo', 'Migui', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 13, 1, 13);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Bantay', 'Jaymar', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 14, 1, 14);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Lagare', 'Jack', NULL, 'male', 'DevOps Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 15, 1, 15);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Ana', 'Ken', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 16, 1, 16);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Tenorio', 'Eugene', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 17, 1, 17);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Lat', 'Renee', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 18, 1, 18);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Flores', 'Argem', NULL, 'male', 'Software Project Manager', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 19, 1, 19);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Carpio', 'Ruth', NULL, 'female', 'IT Project Coordinator', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 20, 1, 20);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Romuga', 'Gene', NULL, 'male', 'Database Engineering', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 21, 1, 21);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Pagtananan', 'Doris', NULL, 'female', 'Software Project Manager', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 22, 1, 22);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Sinohin', 'PJ', NULL, 'male', 'Software Tester', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 23, 1, 23);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Sallan', 'May', NULL, 'female', 'Research Requirements Analyst', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 25, 1, 25);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Lotho', 'Connie', NULL, 'female', 'Research Requirements Analyst', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 26, 1, 26);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Hagen', 'Tom', NULL, 'male', 'Project Leader', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 27, 1, 27);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Castanon', 'Diego', NULL, 'male', 'IT Project Coordinator', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 28, 1, 28);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Leon Trevino', 'Richi', NULL, 'male', 'Research Requirements Analyst', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 29, 1, 29);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Medeiros Barbosa', 'Pedro', NULL, 'male', 'Research Requirements Analyst', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 30, 1, 30);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Shrestha', 'Rosemary', NULL, 'female', 'Research Requirements Analyst', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 31, 1, 31);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Dreher', 'Kate', NULL, 'female', 'Research Requirements Analyst', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 32, 1, 32);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Petroli', 'Cesar', NULL, 'male', 'Research Requirements Analyst', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 33, 1, 33);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Puebla Luna', 'Luis', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 34, 1, 34);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Ortega Lucas', 'Salvador', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 35, 1, 35);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Suarez Sosa', 'Juan', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 36, 1, 36);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Copado Ruiz', 'Gerardo', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 37, 1, 37);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Toledo', 'Fernando', NULL, 'male', '', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 64, 1, 64);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Ulat', 'Victor', NULL, 'male', 'Software Project Manager', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 38, 1, 38);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Montes Samayoa', 'Newman', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 39, 1, 39);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Rojas Olivares', 'Jorge', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 40, 1, 40);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Cisneros Munoz', 'Erika', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 41, 1, 41);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Vergara Espinoza', 'Francisco', NULL, 'male', 'Software Tester', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 43, 1, 43);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Storr', 'Sam', NULL, 'male', 'UI/UX Expert', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 44, 1, 44);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Yanxin Gao', 'Star', NULL, 'female', 'Research Requirements Analyst', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 45, 1, 45);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Palis', 'Kevin', NULL, 'male', 'IT Technical Architect', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 46, 1, 46);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Villanueva Raquel', 'Angel', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 47, 1, 47);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Lamos-Sweeney', 'Josh', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 48, 1, 48);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Govindaraj', 'Vishnu', NULL, 'male', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 49, 1, 49);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Lawrence Petrie', 'Roy', NULL, 'male', 'DevOps Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 50, 1, 50);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Nti-Addae', 'Yaw', NULL, 'male', 'IT Team Lead', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 51, 1, 51);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Molnar', 'Terry', NULL, 'male', 'CIMMYT GRP Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 52, 1, 52);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Dhliwayo', 'Thanda', NULL, 'male', 'CIMMYT GMP Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 53, 1, 53);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Basnet', 'Bhoja', NULL, 'male', 'CIMMYT GWP Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 54, 1, 54);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Platten', 'Damien', NULL, 'male', 'IRRI Rice Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 55, 1, 55);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Robbins', 'Kelly', NULL, 'female', 'Module 5 Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 56, 1, 56);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Olsen', 'Mike', NULL, 'male', 'Module 3 Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 57, 1, 57);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Covarrubias', 'Eduardo', NULL, 'male', 'Module 2 Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 58, 1, 58);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Hwa', 'Eng', NULL, 'male', 'Module 3 Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 59, 1, 59);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Jones', 'Liz', NULL, 'female', 'Module 5 Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 60, 1, 60);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Teixeira', 'Gustavo', NULL, 'male', 'Module 4 Representative', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 61, 1, 61);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Gulles', 'Aleine', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 62, 1, 62);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Morantte', 'Rose', NULL, 'female', 'Software Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 63, 1, 63);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Lapitan', 'Dominic', NULL, 'male', 'Software Developer', 'IT', '2021-02-11 18:14:50.499', NULL, 1, NULL, false, 65, 1, 65);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Consolacion', 'Franjel', NULL, 'male', 'DevOps Engineer', 'IT', '2021-02-11 18:14:36.616', NULL, 1, NULL, false, 42, 1, 42);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Moreno', 'Juan', NULL, 'male', 'Systems Software Engineer', 'IT', '2021-11-18 18:53:29.098', NULL, 1, NULL, false, 66, 1, 66);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Ziai', 'Dorri', NULL, 'female', 'UX/UI Designer', 'IT', '2021-11-18 18:53:29.316', NULL, 1, NULL, false, 67, 1, 67);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('System', 'Account', NULL, 'NA', 'System Administrator', 'IT', '2021-11-18 18:53:29.414', NULL, 1, NULL, false, 68, 1, 68);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Escoto', 'Rodolfo Christian', 'Sam', 'male', 'SQA specialist', 'IT', '2021-11-18 18:53:29.426', NULL, 1, NULL, false, 69, 1, 69);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Vacalares', 'Christian', NULL, 'male', 'SQA specialist', 'IT', '2021-11-18 18:53:29.426', NULL, 1, NULL, false, 70, 1, 70);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Caballero Ramirez', 'Alfonso', 'Joseph', 'male', 'Systems Software Engineer', 'IT', '2021-11-18 18:53:29.645', NULL, 1, NULL, false, 71, 1, 71);
--rollback INSERT INTO crm.person
--rollback (family_name, given_name, additional_name, gender, job_title, knows_about, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, language_id, contact_id)
--rollback VALUES('Peralta Rodriguez', 'Rey', 'David', 'male', 'Systems Software Engineer', 'IT', '2021-11-18 18:53:29.645', NULL, 1, NULL, false, 72, 1, 72);

--rollback DELETE FROM crm.contact_info;

--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(1, 'e.briones@cimmyt.org', true, 1, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(2, 'm.karkkainen@irri.org', true, 2, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(3, 'jp.ramos3@irri.org', true, 3, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(4, 'k.delarosa@irri.org', true, 4, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(5, 'j.antonio@irri.org', true, 5, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(6, 'l.gallardo@irri.org', true, 6, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(7, 'e.banasihan@irri.org', true, 7, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(8, 'm.pasang@irri.org', true, 8, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(9, 'a.crisostomo@irri.org', true, 9, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(10, 'a.caneda@irri.org', true, 10, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(11, 'v.calaminos@irri.org', true, 11, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(12, 'n.carumba@irri.org', true, 12, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(13, 'k.ana@irri.org', true, 16, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(14, 'j.f.castillo@irri.org', true, 13, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(15, 'j.bantay@irri.org', true, 14, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(16, 'j.lagare@irri.org', true, 15, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(17, 'e.tenorio@irri.org', true, 17, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(18, 'r.lat@irri.org', true, 18, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(19, 'a.flores@irri.org', true, 19, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(20, 'r.e.carpio@irri.org', true, 20, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(21, 'g.romuga@irri.org', true, 21, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(22, 'd.pagtananan@irri.org', true, 22, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(23, 'p.sinohin@irri.org', true, 23, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(24, 'l.b.go@irri.org', true, 24, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(25, 'm.sallan@irri.org', true, 25, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(26, 'm.lotho@irri.org', true, 26, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(27, 't.hagen@cimmyt.org', true, 27, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(28, 'd.castanon@cimmyt.org', true, 28, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(29, 'l.leon@cimmyt.org', true, 29, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(30, 'p.medeiros@cimmyt.org', true, 30, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(31, 'r.shrestha2@cimmyt.org', true, 31, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(32, 'k.dreher@cimmyt.org', true, 32, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(33, 'c.petroli@cimmyt.org', true, 33, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(34, 'l.puebla@cimmyt.org', true, 34, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(35, 's.ortega@cimmyt.org', true, 35, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(36, 'j.s.sosa@cimmyt.org', true, 36, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(37, 'g.copado@cimmyt.org', true, 37, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(38, 'v.ulat@cimmyt.org', true, 38, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(39, 'n.montes@cimmyt.org', true, 39, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(40, 'j.a.rojas@cimmyt.org', true, 40, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(41, 'e.cisneros@cimmyt.org', true, 41, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(42, 'f.consolacion@cimmyt.org', true, 42, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(43, 'j.f.vergara@cimmyt.org', true, 43, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(44, 's.storr@cimmyt.org', true, 44, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(45, 'yg28@cornell.edu', true, 45, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(46, 'kdp44@cornell.edu', true, 46, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(47, 'avr33@cornell.edu', true, 47, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(48, 'jdl232@cornell.edu', true, 48, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(49, 'vg249@cornell.edu', true, 49, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(50, 'rlp243@cornell.edu', true, 50, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(51, 'yn259@cornell.edu', true, 51, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(52, 't.molnar@cimmyt.org', true, 52, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(53, 'd.thanda@cimmyt.org', true, 53, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(54, 'b.r.basnet@cimmyt.org', true, 54, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(55, 'j.platten@irri.org', true, 55, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(56, 'krr73@cornell.edu', true, 56, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(57, 'm.olsen@cimmyt.org', true, 57, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(58, 'g.covarrubias@cimmyt.org', true, 58, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(59, 'n.enghwa@cimmyt.org', true, 59, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(60, 'ej245@cornell.edu', true, 60, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(61, 'g.teixeira@cimmyt.org', true, 61, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(62, 'a.gulles@irri.org', true, 62, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(63, 'r.morantte@irri.org', true, 63, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(64, 'F.TOLEDO@cimmyt.org', true, 64, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(65, 'domlapitan@gmail.com', true, 65, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(66, 'm.karkkainen@cimmyt.org', true, 2, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(67, 'j.m.sanchez@cimmyt.org', true, 66, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(68, 'P.MEDEIROS@cimmyt.onmicrosoft.com', true, 30, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(69, 'daz28@cornell.edu', true, 67, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(70, 'admin@ebsproject.org', true, 68, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(71, 'r.escoto@irri.org', true, 69, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(72, 'chris.vacalares@gmail.com', true, 70, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(73, 'a.caballero@cgiar.org', true, 71, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);
--rollback INSERT INTO crm.contact_info
--rollback (id, value, is_default, contact_id, info_type_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
--rollback VALUES(74, 'r.peralta@cgiar.org', true, 72, 2, '2021-11-18 18:53:29.663', NULL, 1, NULL, false);

--rollback SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));
--rollback SELECT setval('crm.contact_info_id_seq', (SELECT MAX(id) FROM crm.contact_info));