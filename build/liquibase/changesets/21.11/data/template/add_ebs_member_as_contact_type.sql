--liquibase formatted sql

--changeset postgres:add_ebs_member_as_contact_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-812 Add EBS member as contact type


INSERT INTO crm.contact_type
(category, "name", creation_timestamp, creator_id, is_void)
VALUES
('Person', 'EBS member', now(), 1, false);

select setval('crm.contact_type_id_seq',max(id)) from crm.contact_type;

--Revert Changes
--rollback DELETE FROM crm.contact_type where "name" = 'EBS member';
--rollback select setval('crm.contact_type_id_seq',max(id)) from crm.contact_type;