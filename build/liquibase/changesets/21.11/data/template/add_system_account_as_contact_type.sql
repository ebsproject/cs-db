--liquibase formatted sql

--changeset postgres:add_system_account_as_contact_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-861 Add System Account as Contact type


INSERT INTO crm.contact_type
(category, "name", creation_timestamp, creator_id, is_void)
VALUES
('Person', 'System Account', now(), 1, false);


select setval('crm.contact_type_id_seq',max(id)) from crm.contact_type;

--Revert Changes
--rollback DELETE FROM crm.contact_type where "name" = 'System Account';
--rollback select setval('crm.contact_type_id_seq',max(id)) from crm.contact_type;