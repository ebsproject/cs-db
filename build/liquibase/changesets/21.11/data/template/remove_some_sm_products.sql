--liquibase formatted sql

--changeset postgres:remove_some_sm_products context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-843 Remove some sm products from csdb



DELETE FROM core.product WHERE id=25;

DELETE FROM core.product WHERE id=17;

DELETE FROM core.product WHERE id=18;


--Revert Changes
--rollback INSERT INTO core.product ("name", description, help, main_entity, icon, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, domain_id, htmltag_id, menu_order, "path")
--rollback VALUES('Create Request', ' SM Create Request', 'SM Create Request', 'Create Request', '', '2021-11-18 18:53:29.124', NULL, 1, NULL, false, 25, 4, 1, 2, '/sm/requestmanager/create-request-service');
--rollback INSERT INTO core.product ("name", description, help, main_entity, icon, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, domain_id, htmltag_id, menu_order, "path")
--rollback VALUES('PLIMS', 'PLIMS', 'PLIMS', 'PLIMS', '', '2021-11-18 18:53:28.859', NULL, 1, NULL, false, 17, 4, 1, 8, '/');
--rollback INSERT INTO core.product ("name", description, help, main_entity, icon, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, domain_id, htmltag_id, menu_order, "path")
--rollback VALUES('QLIMS', 'QLIMS', 'QLIMS', 'QLIMS', '', '2021-11-18 18:53:28.859', NULL, 1, NULL, false, 18, 4, 1, 9, '/');