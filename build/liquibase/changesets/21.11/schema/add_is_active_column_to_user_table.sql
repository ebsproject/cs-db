--liquibase formatted sql

--changeset postgres:add_is_active_column_to_user_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-745 Add is_active column to user table


ALTER TABLE security."user" 
 ADD COLUMN is_active boolean NULL DEFAULT true;	-- Defines if the user is currently active or not

COMMENT ON COLUMN security."user".is_active IS 'Defines if the user is currently active or not';


--Revert Changes
--rollback ALTER TABLE security."user" DROP COLUMN is_active;