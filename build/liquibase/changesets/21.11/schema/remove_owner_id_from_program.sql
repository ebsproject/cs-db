--liquibase formatted sql

--changeset postgres:remove_owner_id_from_program context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-807 Remove owner_id form porgram table


ALTER TABLE program.program 
  DROP  CONSTRAINT "FK_program_user";

ALTER TABLE program.program 
 DROP COLUMN IF EXISTS owner_id;

--Revert Changes
--rollback ALTER TABLE program.program ADD COLUMN owner_id int4;
--rollback ALTER TABLE "program"."program" ADD CONSTRAINT "FK_program_user" FOREIGN KEY (owner_id) REFERENCES security."user"(id);
