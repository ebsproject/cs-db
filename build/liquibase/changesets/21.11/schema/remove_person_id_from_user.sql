--liquibase formatted sql

--changeset postgres:remove_person_id_from_user context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-716 Remove person_id from security.user


ALTER TABLE "security"."user" DROP COLUMN IF EXISTS person_id;


--Revert Changes
--rollback ALTER TABLE "security"."user" ADD COLUMN person_id int4 NULL;
--rollback ALTER TABLE "security"."user" ADD CONSTRAINT "FK_user_person" FOREIGN KEY (person_id) REFERENCES crm.person(id);
