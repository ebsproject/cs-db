--liquibase formatted sql

--changeset postgres:remove_tenant_id_from_crm_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-782 Remove tenant_id from CRM Tables



ALTER TABLE crm.address DROP COLUMN IF EXISTS tenant_id;
ALTER TABLE crm.contact_info DROP COLUMN IF EXISTS tenant_id;
ALTER TABLE crm.contact DROP COLUMN IF EXISTS tenant_id;
ALTER TABLE crm.contact_type DROP COLUMN IF EXISTS tenant_id;
ALTER TABLE crm.info_type DROP COLUMN IF EXISTS tenant_id;

--Revert Changes
--rollback ALTER TABLE crm.address ADD COLUMN tenant_id int4;
--rollback ALTER TABLE crm.contact_info ADD COLUMN tenant_id int4;
--rollback ALTER TABLE crm.contact ADD COLUMN tenant_id int4;
--rollback ALTER TABLE crm.contact_type ADD COLUMN tenant_id int4;
--rollback ALTER TABLE crm.info_type ADD COLUMN tenant_id int4;