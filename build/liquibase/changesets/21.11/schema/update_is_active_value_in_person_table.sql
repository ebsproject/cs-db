--liquibase formatted sql

--changeset postgres:update_is_active_value_in_person_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-860 Update is_active in Person table



ALTER TABLE "security"."user" ALTER COLUMN is_active SET NOT NULL;


--Revert Changes
--rollback ALTER TABLE "security"."user" ALTER COLUMN is_active DROP NOT NULL;