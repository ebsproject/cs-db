--liquibase formatted sql

--changeset postgres:update_context_fos_cs_sm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-909 Update context for sm and cs



UPDATE core.domain_instance
SET context='https://dev.ebsproject.org', sg_context='https://csapi-dev.ebsproject.org/' 
WHERE id=5;
UPDATE core.domain_instance
SET sg_context='https://smapi-dev.ebsproject.org/'
WHERE id=4;



--Revert Changes
--rollback UPDATE core.domain_instance SET context='https://dev.ebsproject.org', sg_context='https://ebs-demo.cimmyt.org:81/api-cs' WHERE id=5;
--rollback UPDATE core.domain_instance SET sg_context='https://ebs-demo.cimmyt.org:81/api-sm' WHERE id=4;