--liquibase formatted sql

--changeset postgres:add_printout_and_shipment_product context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-899 Add Printout and Shipment manager in Product table


UPDATE core.product
SET name='Printout', description='Printout', help='Printout', main_entity='Printout', "path"='printout'
WHERE id=21;

INSERT INTO core.product
(id, "name", description, help, main_entity, icon, creator_id, is_void, domain_id, htmltag_id, menu_order, "path")
VALUES(25, 'Shipment Manager', 'Shipment Manager', 'Shipment Manager', 'Shipment', '', 1, false, 2, 1, 5, 'shipment');


SELECT setval('core.product_id_seq', (SELECT MAX(id) FROM core.product));

--Revert Changes
--rollback UPDATE core.product SET "name"='Settings', description='Settings', help='Settings', main_entity='Settings', "path"='settings' WHERE id=21;
--rollback DELETE FROM core.product WHERE id = 25;
--rollback SELECT setval('core.product_id_seq', (SELECT MAX(id) FROM core.product));
