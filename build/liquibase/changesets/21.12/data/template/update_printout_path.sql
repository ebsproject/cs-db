--liquibase formatted sql

--changeset postgres:update_printout_path context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-909 Update printout path



UPDATE core.product
SET "path"='settings'
WHERE id=21;



--Revert Changes
--rollback UPDATE core.product SET "path"='printout' WHERE id=21;


