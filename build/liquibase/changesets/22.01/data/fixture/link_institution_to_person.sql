--liquibase formatted sql

--changeset postgres:link_institution_to_person context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-908 Add Institution data in csdb



SET session_replication_role = 'replica';

INSERT INTO crm.hierarchy (creator_id, parent_id, contact_id) 
SELECT 1, 1, id from crm.contact where id >=1 and id <=72;

UPDATE crm.hierarchy SET parent_id = (SELECT id from crm.contact WHERE email = 'cimmyt@cgiar.org') WHERE contact_id >= 1 and contact_id <=50;

UPDATE crm.hierarchy SET parent_id = (SELECT id from crm.contact WHERE email = 'irri@irri.org') WHERE contact_id >= 51 and contact_id <=72;

UPDATE crm.hierarchy SET is_principal_contact = TRUE WHERE contact_id =1;

UPDATE crm.hierarchy SET is_principal_contact = TRUE WHERE contact_id =72;

SET session_replication_role = 'origin';



--Revert Changes
--rollback DELETE FROM crm.hierarchy WHERE contact_id >=1;
--rollback UPDATE crm.hierarchy SET parent_id = NULL WHERE contact_id >= 1 and contact_id <=50;
--rollback UPDATE crm.hierarchy SET parent_id = NULL WHERE contact_id >= 51 and contact_id <=72;
--rollback UPDATE crm.hierarchy SET is_principal_contact = FALSE WHERE contact_id =1;
--rollback UPDATE crm.hierarchy SET is_principal_contact = FALSE WHERE contact_id =72;