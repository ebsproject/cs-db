--liquibase formatted sql

--changeset postgres:set_values_to_contact_purpose context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-878 Set values to Contact.purpose



UPDATE crm.contact SET purpose_id=1;


--Revert Changes
--rollback UPDATE crm.contact SET purpose_id = NULL;