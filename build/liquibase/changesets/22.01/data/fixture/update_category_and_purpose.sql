--liquibase formatted sql

--changeset postgres:update_category_and_purpose context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-913 Create Table category 



UPDATE crm.contact SET category_id = 1 WHERE id>=0 and id<=72;

UPDATE crm.address SET purpose_id = 7;


--Revert Changes
--rollback UPDATE crm.contact SET category_id = NULL WHERE id>=0 and id<=72;
--rollback UPDATE crm.address SET purpose_id = NULL;