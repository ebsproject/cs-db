--liquibase formatted sql

--changeset postgres:update_contact_email context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-917 Update contact email in csdb fixture



do $$
declare temprow record;
begin
for temprow in select ci.contact_id, ci.value from crm.contact_info ci where ci.info_type_id = 2
	loop
		UPDATE crm.contact SET email = temprow.value where id =temprow.contact_id;
	end loop;
end $$


--Revert changes 
--rollback UPDATE crm.contact SET email = NULL where id not in (73,74);