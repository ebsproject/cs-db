--liquibase formatted sql

--changeset postgres:add_institution context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-908 Add Institution data in csdb




INSERT INTO crm.contact
(category, creation_timestamp, creator_id, is_void, email, purpose_id)
VALUES
('Institution', now(), 0, false, 'cimmyt@cgiar.org', 4),
('Institution', now(), 0, false, 'irri@irri.org', 4);



INSERT INTO crm.institution
(id, common_name, legal_name, contact_id, creation_timestamp, creator_id, is_void)
VALUES
(1, 'CIMMYT', 'CIMMYT', (SELECT id from crm.contact where email = 'cimmyt@cgiar.org'), now(), 1, false),
(2, 'IRRI', 'IRRI', (SELECT id from crm.contact where email = 'irri@irri.org'), now(), 1, false);

SELECT setval('crm.institution_id_seq', (SELECT MAX(id) FROM crm.institution));


--Revert Changes
--rollback DELETE FROM crm.institution WHERE id = 1;
--rollback DELETE FROM crm.institution WHERE id = 2;

--rollback DELETE FROM crm.contact WHERE email = 'cimmyt@cgiar.org';
--rollback DELETE FROM crm.contact WHERE email = 'irri@irri.org';

--rollback SELECT setval('crm.contact_id_seq', (SELECT MAX(id) FROM crm.contact));
--rollback SELECT setval('crm.institution_id_seq', (SELECT MAX(id) FROM crm.institution));