--liquibase formatted sql

--changeset postgres:populate_category_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-913 Create Table category 



INSERT INTO crm.category
(id, "name", creation_timestamp, creator_id, is_void)
VALUES
(1, 'Person', now(), 1, false),
(2, 'Institution', now(), 1, false),
(3, 'Address', now(), 1, false);

SELECT setval('crm.category_id_seq', (SELECT MAX(id) FROM crm.category));


--Revert Changes
--rollback DELETE FROM crm.category WHERE id>=1 and id<=3;
