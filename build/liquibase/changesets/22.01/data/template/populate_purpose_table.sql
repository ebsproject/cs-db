--liquibase formatted sql

--changeset postgres:populate_purpose_table context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-878 Create Purpose catalog for Contact



INSERT INTO crm.purpose
(id, category, "name", creation_timestamp, creator_id, is_void)
VALUES
(1, 'Person', 'Default', now(), 1, false),
(2, 'Person', 'Admin', now(), 1, false),
(3, 'Person', 'Research', now(), 1, false),
(4, 'Institution', 'Default', now(), 1, false),
(5, 'Institution', 'Admin', now(), 1, false),
(6, 'Institution', 'Research', now(), 1, false);

UPDATE crm.contact SET purpose_id=1 WHERE id = 0;

SELECT setval('crm.purpose_id_seq', (SELECT MAX(id) FROM crm.purpose));

--Revert Changes
--rollback DELETE FROM crm.purpose WHERE id >= 1;
--rollback alter sequence crm.purpose_id_seq restart with 1;
--rollback UPDATE crm.contact SET purpose_id=NULL WHERE id = 0;
