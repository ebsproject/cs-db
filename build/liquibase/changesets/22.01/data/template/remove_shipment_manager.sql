--liquibase formatted sql

--changeset postgres:remove_shipment_manager context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-920 Remove shipment Manager from csdb Products



UPDATE core.product
SET "path"='/sm/shipment'
WHERE id=19;

DELETE FROM core.product WHERE id = 25;

SELECT setval('core.product_id_seq', (SELECT MAX(id) FROM core.product));

--Revert Changes
--rollback UPDATE core.product SET "path"='/' WHERE id=19;
--rollback INSERT INTO core.product (id, "name", description, help, main_entity, icon, creator_id, is_void, domain_id, htmltag_id, menu_order, "path")
--rollback VALUES(25, 'Shipment Manager', 'Shipment Manager', 'Shipment Manager', 'Shipment', '', 1, false, 2, 1, 5, 'shipment');
--rollback SELECT setval('core.product_id_seq', (SELECT MAX(id) FROM core.product));
