--liquibase formatted sql

--changeset postgres:update_category_and_purpose_template context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-913 Create Table category 



INSERT INTO crm.purpose
(id, category_id, "name", creation_timestamp, creator_id, is_void)
VALUES
(7, 3, 'Mail', now(), 1, false),
(8, 3, 'Shipping', now(), 1, false);

SELECT setval('crm.purpose_id_seq', (SELECT MAX(id) FROM crm.purpose));

UPDATE crm.contact_type SET category_id=1 WHERE id<=4;

UPDATE crm.contact_type SET category_id=2 WHERE id>=5 and id <=8;

UPDATE crm.contact_type SET category_id=1 WHERE id>=9;

UPDATE crm.purpose SET category_id=1 WHERE id>=1 and id <=3;

UPDATE crm.purpose SET category_id=2 WHERE id>=4 and id <=6;

UPDATE crm.contact SET category_id=2 WHERE email = 'cimmyt@cgiar.org';

UPDATE crm.contact SET category_id=2 WHERE email = 'irri@irri.org';

UPDATE crm.contact SET category_id=1 WHERE id=0;

UPDATE crm.address SET purpose_id = 7 WHERE id = 1;


--Revert Changes
--rollback DELETE FROM crm.purpose WHERE id = 7;
--rollback DELETE FROM crm.purpose WHERE id = 8;
--rollback UPDATE crm.contact_type SET category_id = NULL WHERE id<=4;
--rollback UPDATE crm.contact_type SET category_id = NULL WHERE id>=5 and id <=8;
--rollback UPDATE crm.contact_type SET category_id= NULL WHERE id>=9;
--rollback UPDATE crm.purpose SET category_id= NULL WHERE id>=1 and id <=3;
--rollback UPDATE crm.purpose SET category_id= NULL WHERE id>=4 and id <=6;
--rollback UPDATE crm.contact SET category_id= NULL WHERE email = 'cimmyt@cgiar.org';
--rollback UPDATE crm.contact SET category_id= NULL WHERE email = 'irri@irri.org';