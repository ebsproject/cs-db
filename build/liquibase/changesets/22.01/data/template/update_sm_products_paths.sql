--liquibase formatted sql

--changeset postgres:update_sm_products_paths context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-922 Update SM Products path



UPDATE core.product
SET "path"='/sm/under-construction'
WHERE id=19;


UPDATE core.product
SET "path"='/sm/under-construction'
WHERE id=16;



--Revert Changes
--rollback UPDATE core.product SET "path"='/sm/shipment' WHERE id=19;
--rollback UPDATE core.product SET "path"='/' WHERE id=16;