--liquibase formatted sql

--changeset postgres:add_contact_purpose_and_parent_id context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-864 Add purpose table and parent_id in Contact



ALTER TABLE crm.institution 
  DROP  CONSTRAINT "FK_institution_principal_contact";

ALTER TABLE crm.institution 
 DROP COLUMN IF EXISTS principal_contact_id;

ALTER TABLE crm.contact 
 ADD COLUMN email varchar(150) NULL;	-- Official or primary email of the contact

ALTER TABLE crm.contact 
 ADD COLUMN purpose_id integer NULL;

ALTER TABLE crm.address 
 ADD COLUMN purpose varchar(50) NULL;

CREATE TABLE crm.purpose
(
	id integer NOT NULL   DEFAULT NEXTVAL(('crm."purpose_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	category varchar(11) NULL,	-- Defines if the purpose belongs to a Person or an Institution.
	name varchar(50) NULL,	-- Name of the Purpose, it can be, Administrative, Technical, Research, etc.
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE crm.hierarchy
(
	contact_id integer NOT NULL,
	parent_id integer NOT NULL,
	is_principal_contact boolean NULL   DEFAULT false,	-- Specifies the principal contact of an Institution.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

ALTER TABLE crm.hierarchy ADD CONSTRAINT "PK_hierarchy"
	PRIMARY KEY (contact_id,parent_id)
;

ALTER TABLE crm.hierarchy ADD CONSTRAINT "FK_hierarchy_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.hierarchy ADD CONSTRAINT "FK_hierarchy_parent_contact"
	FOREIGN KEY (parent_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

CREATE SEQUENCE crm.purpose_id_seq INCREMENT 1 START 1;

ALTER TABLE crm.purpose ADD CONSTRAINT check_category CHECK ((category='Person' OR category = 'Institution'))
;

ALTER TABLE crm.purpose ADD CONSTRAINT "PK_purpose"
	PRIMARY KEY (id)
;

ALTER TABLE crm.contact ADD CONSTRAINT "FK_contact_purpose"
	FOREIGN KEY (purpose_id) REFERENCES crm.purpose (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON TABLE crm.purpose
	IS 'Defines the type of relationship pf the Person and Institution e.g Administrative, Technical, Research, etc.'
;

COMMENT ON COLUMN crm.contact.category
	IS 'Defines if its a Person or an Institution.'
;

COMMENT ON COLUMN crm.contact.email
	IS 'Official or primary email of the contact'
;

COMMENT ON COLUMN crm.contact.purpose_id
	IS 'Relation to Purpose table'
;

COMMENT ON COLUMN crm.purpose.category
	IS 'Defines if the purpose belongs to a Person or an Institution.'
;

COMMENT ON COLUMN crm.purpose.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN crm.purpose.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN crm.purpose.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.purpose.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN crm.purpose.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN crm.purpose.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN crm.purpose.name
	IS 'Name of the Purpose, it can be, Administrative, Technical, Research, etc.'
;

COMMENT ON TABLE crm.hierarchy
	IS 'Manages the relation between Institutions and Person.'
;

COMMENT ON COLUMN crm.hierarchy.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN crm.hierarchy.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN crm.hierarchy.is_principal_contact
	IS 'Specifies the principal contact of an Institution.'
;

COMMENT ON COLUMN crm.hierarchy.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN crm.hierarchy.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN crm.hierarchy.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN crm.address.purpose 
	IS 'Specifies the purpose of the address, whether is for mail, shipping etc.'
;

--Revert Changes
--rollback ALTER TABLE crm.institution ADD COLUMN principal_contact_id int4 NULL;
--rollback ALTER TABLE crm.institution ADD CONSTRAINT "FK_institution_principal_contact" FOREIGN KEY (principal_contact_id) REFERENCES crm.contact(id);
--rollback ALTER TABLE crm.contact DROP COLUMN IF EXISTS email;
--rollback ALTER TABLE crm.contact DROP COLUMN IF EXISTS purpose_id;

--rollback ALTER TABLE crm.address DROP COLUMN purpose;

--rollback DROP SEQUENCE crm.purpose_id_seq;
--rollback DROP TABLE crm.purpose;
--rollback DROP TABLE crm.hierarchy;