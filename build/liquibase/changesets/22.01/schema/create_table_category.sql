--liquibase formatted sql

--changeset postgres:create_table_category context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-913 Create Table category and remove check constraint


ALTER TABLE crm.contact 
  DROP  CONSTRAINT check_category;

ALTER TABLE crm.contact_type 
  DROP  CONSTRAINT check_category;

ALTER TABLE crm.purpose 
  DROP  CONSTRAINT check_category;

ALTER TABLE crm.address 
 DROP COLUMN IF EXISTS purpose;

ALTER TABLE crm.contact 
 DROP COLUMN IF EXISTS category;

ALTER TABLE crm.contact_type 
 DROP COLUMN IF EXISTS category;

ALTER TABLE crm.purpose 
 DROP COLUMN IF EXISTS category;

ALTER TABLE crm.address 
 ADD COLUMN purpose_id integer NULL;

ALTER TABLE crm.contact 
 ADD COLUMN category_id integer NULL;

ALTER TABLE crm.contact_type 
 ADD COLUMN category_id integer NULL;

ALTER TABLE crm.purpose 
 ADD COLUMN category_id integer NULL;

CREATE TABLE crm.category
(
	id integer NOT NULL   DEFAULT NEXTVAL(('crm."category_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(50) NULL,
	description varchar(250) NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE SEQUENCE crm.category_id_seq INCREMENT 1 START 1;

ALTER TABLE crm.category ADD CONSTRAINT "PK_category"
	PRIMARY KEY (id)
;

ALTER TABLE crm.address ADD CONSTRAINT "FK_address_purpose"
	FOREIGN KEY (purpose_id) REFERENCES crm.purpose (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact ADD CONSTRAINT "FK_contact_category"
	FOREIGN KEY (category_id) REFERENCES crm.category (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact_type ADD CONSTRAINT "FK_contact_type_category"
	FOREIGN KEY (category_id) REFERENCES crm.category (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.purpose ADD CONSTRAINT "FK_purpose_category"
	FOREIGN KEY (category_id) REFERENCES crm.category (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN crm.address.contact_id
	IS 'Reference to the contact related to the address'
;

COMMENT ON COLUMN crm.address.country_id
	IS 'Reference to the Country id'
;

COMMENT ON COLUMN crm.address.is_default
	IS 'Determine the default address of a certain contact'
;

COMMENT ON COLUMN crm.address.purpose_id
	IS 'Id reference to te purpose of the address, it can be for mail, shipping, ect.'
;

COMMENT ON COLUMN crm.address.street_address
	IS 'Street name of the address'
;

COMMENT ON COLUMN crm.category.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN crm.category.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN crm.category.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN crm.category.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN crm.category.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN crm.category.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN crm.category.description
	IS 'Definition of the category'
;

COMMENT ON COLUMN crm.category.name
	IS 'ID of the user who last modified the record'
;


COMMENT ON COLUMN crm.contact.category_id
	IS 'Reference to the category_id which defines if its a Person, an Institution or an Address'
;

COMMENT ON COLUMN crm.contact.email
	IS 'Official or primary email of the contact'
;

COMMENT ON COLUMN crm.contact.purpose_id
	IS 'Reference to the Purpose id which defines if the contact is type administrative, Technical, Research, etc.'
;

COMMENT ON COLUMN crm.contact_type.category_id
	IS 'Reference to the category_id which defines if its a Person, an Institution or an Address'
;

COMMENT ON COLUMN crm.contact_type.name
	IS 'Name of the relationship with the Contact, e.g Donor, Collaborator, Customer, etc.'
;

COMMENT ON COLUMN crm.purpose.category_id
	IS 'Reference to the category_id which defines if its a Person, an Institution or an Address'
;


--Revert Changes
--rollback ALTER TABLE crm.address DROP CONSTRAINT "FK_address_purpose";
--rollback ALTER TABLE crm.contact DROP CONSTRAINT "FK_contact_category";
--rollback ALTER TABLE crm.contact_type DROP CONSTRAINT "FK_contact_type_category";
--rollback ALTER TABLE crm.purpose DROP CONSTRAINT "FK_purpose_category";

--rollback ALTER TABLE crm.address DROP COLUMN purpose_id;
--rollback ALTER TABLE crm.contact DROP COLUMN category_id;
--rollback ALTER TABLE crm.contact_type DROP COLUMN category_id;
--rollback ALTER TABLE crm.purpose DROP COLUMN category_id;

--rollback ALTER TABLE crm.address ADD COLUMN purpose varchar(50);
--rollback ALTER TABLE crm.contact ADD COLUMN category varchar(11); 
--rollback ALTER TABLE crm.contact_type ADD COLUMN category varchar(11);
--rollback ALTER TABLE crm.purpose ADD COLUMN category varchar(11);

--rollback ALTER TABLE crm.contact ADD CONSTRAINT check_category CHECK ((((category)::text = 'Person'::text) OR ((category)::text = 'Institution'::text)));
--rollback ALTER TABLE crm.contact_type ADD CONSTRAINT check_category CHECK ((((category)::text = 'Person'::text) OR ((category)::text = 'Institution'::text)));
--rollback ALTER TABLE crm.purpose ADD CONSTRAINT check_category CHECK ((((category)::text = 'Person'::text) OR ((category)::text = 'Institution'::text)));

--rollback DROP SEQUENCE crm.category_id_seq;
--rollback DROP TABLE crm.category;