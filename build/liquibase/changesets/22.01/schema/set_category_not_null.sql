--liquibase formatted sql

--changeset postgres:set_category_not_null context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-921 Set not null values for category and purpose



ALTER TABLE crm.address 
 ALTER COLUMN purpose_id SET NOT NULL;

ALTER TABLE crm.contact 
 ALTER COLUMN category_id SET NOT NULL;

ALTER TABLE crm.contact_type 
 ALTER COLUMN category_id SET NOT NULL;

ALTER TABLE crm.purpose 
 ALTER COLUMN category_id SET NOT NULL;


--Revert Changes
--rollback ALTER TABLE crm.address ALTER COLUMN purpose_id DROP NOT NULL;
--rollback ALTER TABLE crm.contact ALTER COLUMN category_id DROP NOT NULL;
--rollback ALTER TABLE crm.contact_type ALTER COLUMN category_id DROP NOT NULL;
--rollback ALTER TABLE crm.purpose ALTER COLUMN category_id DROP NOT NULL;
