--liquibase formatted sql

--changeset postgres:set_purpose_not_null_in_contact_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-864 Add purpose table and parent_id in Contact



ALTER TABLE crm.contact ALTER COLUMN purpose_id SET NOT NULL;


--Revert Changes
--rollback ALTER TABLE crm.contact ALTER COLUMN purpose_id DROP NOT NULL;