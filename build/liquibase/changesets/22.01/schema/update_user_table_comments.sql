--liquibase formatted sql

--changeset postgres:update_user_table_comments context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-934 Update comments on user table


COMMENT ON COLUMN security."user".user_name IS 'Name of the user';
COMMENT ON COLUMN security."user".default_role_id IS 'Id reference to the default role in the role table';
COMMENT ON COLUMN security."user".contact_id IS 'Id reference to teh contact related to the user';


--Revert Changes
--rollback COMMENT ON COLUMN security."user".user_name IS 'Name of the ser';
--rollback COMMENT ON COLUMN security."user".default_role_id IS 'User name';
--rollback COMMENT ON COLUMN security."user".contact_id IS '';