--liquibase formatted sql

--changeset postgres:add_roles_and_product_functions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-959 Review user role Settings 



SET session_replication_role = 'replica';

--User role - Product Function
INSERT INTO "security".role_product_function
(role_id, product_function_id)
(SELECT 4, id from "security".product_function WHERE id >= 30);

SET session_replication_role = 'origin';



--Revert Changes
--Rollback DELETE FROM "security".role_product_function WHERE role_id = 4;

