--liquibase formatted sql

--changeset postgres:add_roles_and_product_functions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-946 Add roles and product functions 



SET session_replication_role = 'replica';

UPDATE "security"."role"
SET security_group='EBS_Administrators' --Admin
WHERE id=2;

UPDATE "security"."role"
SET description='user', security_group='EBS User', "name"='User' 
WHERE id=4;

UPDATE "security"."role"
SET description='Cose System Administrator', security_group='EBS Administrators', "name"='CS Admin' 
WHERE id=5;


INSERT INTO "security"."role"
(id, description, security_group, creation_timestamp, creator_id, is_void, name)
VALUES
(6, 'Breeding Analytics Administrator', 'EBS Administrators', now(), 1, false, 'BA Admin'),
(7, 'Service Management Administrator', 'EBS Administrators', now(), 1, false, 'SM Admin'),
(8, 'Core System User', 'EBS User', now(), 1, false, 'CS User'),
(9, 'Breeding Analytics User', 'EBS User', now(), 1, false, 'BA User'),
(10, 'Service Management User', 'EBS User', now(), 1, false, 'SM User'),
(11, 'Core Breeding User', 'EBS User', now(), 1, false, 'CB User');

--CS Products - User Management
UPDATE "security".product_function
SET description='Create', "action"='Create', product_id=9 WHERE id=1;

UPDATE "security".product_function
SET description='Modify', "action"='Modify', product_id=9 WHERE id=2;

UPDATE "security".product_function
SET description='Delete', "action"='Delete', product_id=9 WHERE id=3;


INSERT INTO "security".product_function
(id, description, system_type, "action", creation_timestamp, creator_id, is_void, product_id)
VALUES
(4, 'Export', true, 'Export', now(), 1, false, 9),
(5, 'Print', true, 'Print', now(), 1, false, 9),
(6, 'Search', true, 'Search', now(), 1, false, 9),
(7, 'Read', true, 'Read', now(), 1, false, 9),


--CS User Products - Tenant Management
(8, 'Create', true, 'Create', now(), 1, false, 10),
(9, 'Modify', true, 'Modify', now(), 1, false, 10),
(10, 'Delete', true, 'Delete', now(), 1, false, 10),
(11, 'Export', true, 'Export', now(), 1, false, 10),
(12, 'Print', true, 'Print', now(), 1, false, 10),
(13, 'Search', true, 'Search', now(), 1, false, 10),
(14, 'Read', true, 'Read', now(), 1, false, 10),

--CS User Products - CRM
(15, 'Create', true, 'Create', now(), 1, false, 11),
(16, 'Modify', true, 'Modify', now(), 1, false, 11),
(18, 'Delete', true, 'Delete', now(), 1, false, 11),
(19, 'Export', true, 'Export', now(), 1, false, 11),
(20, 'Print', true, 'Print', now(), 1, false, 11),
(21, 'Search', true, 'Search', now(), 1, false, 11),
(22, 'Read', true, 'Read', now(), 1, false, 11),

--CS User Products - Printout
(23, 'Create', true, 'Create', now(), 1, false, 21),
(24, 'Modify', true, 'Modify', now(), 1, false, 21),
(25, 'Delete', true, 'Delete', now(), 1, false, 21),
(26, 'Export', true, 'Export', now(), 1, false, 21),
(27, 'Print', true, 'Print', now(), 1, false, 21),
(28, 'Search', true, 'Search', now(), 1, false, 21),
(29, 'Read', true, 'Read', now(), 1, false, 21),

--BA Products - ARM
(30, 'Create', true, 'Create', now(), 1, false, 12),
(31, 'Modify', true, 'Modify', now(), 1, false, 12),
(32, 'Delete', true, 'Delete', now(), 1, false, 12),
(33, 'Export', true, 'Export', now(), 1, false, 12),
(34, 'Print', true, 'Print', now(), 1, false, 12),
(35, 'Search', true, 'Search', now(), 1, false, 12),
(36, 'Read', true, 'Read', now(), 1, false, 12),

--SM Products - Request Manager
(37, 'Create', true, 'Create', now(), 1, false, 14),
(38, 'Modify', true, 'Modify', now(), 1, false, 14),
(39, 'Delete', true, 'Delete', now(), 1, false, 14),
(40, 'Export', true, 'Export', now(), 1, false, 14),
(41, 'Print', true, 'Print', now(), 1, false, 14),
(42, 'Search', true, 'Search', now(), 1, false, 14),
(43, 'Read', true, 'Read', now(), 1, false, 14),

--SM Products - Genotyping Service Management
(44, 'Create', true, 'Create', now(), 1, false, 20),
(45, 'Modify', true, 'Modify', now(), 1, false, 20),
(46, 'Delete', true, 'Delete', now(), 1, false, 20),
(47, 'Export', true, 'Export', now(), 1, false, 20),
(48, 'Print', true, 'Print', now(), 1, false, 20),
(49, 'Search', true, 'Search', now(), 1, false, 20),
(50, 'Read', true, 'Read', now(), 1, false, 20),

--SM Products - Inspect
(51, 'Create', true, 'Create', now(), 1, false, 22),
(52, 'Modify', true, 'Modify', now(), 1, false, 22),
(53, 'Delete', true, 'Delete', now(), 1, false, 22),
(54, 'Export', true, 'Export', now(), 1, false, 22),
(55, 'Print', true, 'Print', now(), 1, false, 22),
(56, 'Search', true, 'Search', now(), 1, false, 22),
(57, 'Read', true, 'Read', now(), 1, false, 22),

--SM Products - Design
(58, 'Create', true, 'Create', now(), 1, false, 23),
(59, 'Modify', true, 'Modify', now(), 1, false, 23),
(60, 'Delete', true, 'Delete', now(), 1, false, 23),
(61, 'Export', true, 'Export', now(), 1, false, 23),
(62, 'Print', true, 'Print', now(), 1, false, 23),
(63, 'Search', true, 'Search', now(), 1, false, 23),
(64, 'Read', true, 'Read', now(), 1, false, 23),

--SM Products - Vendor
(65, 'Create', true, 'Create', now(), 1, false, 24),
(66, 'Modify', true, 'Modify', now(), 1, false, 24),
(67, 'Delete', true, 'Delete', now(), 1, false, 24),
(68, 'Export', true, 'Export', now(), 1, false, 24),
(69, 'Print', true, 'Print', now(), 1, false, 24),
(70, 'Search', true, 'Search', now(), 1, false, 24),
(71, 'Read', true, 'Read', now(), 1, false, 24),

--SM Products - Shipment Tool
(72, 'Create', true, 'Create', now(), 1, false, 19),
(73, 'Modify', true, 'Modify', now(), 1, false, 19),
(74, 'Delete', true, 'Delete', now(), 1, false, 19),
(75, 'Export', true, 'Export', now(), 1, false, 19),
(76, 'Print', true, 'Print', now(), 1, false, 19),
(77, 'Search', true, 'Search', now(), 1, false, 19),
(78, 'Read', true, 'Read', now(), 1, false, 19),

--SM Products - Service Ctalog
(79, 'Create', true, 'Create', now(), 1, false, 16),
(80, 'Modify', true, 'Modify', now(), 1, false, 16),
(81, 'Delete', true, 'Delete', now(), 1, false, 16),
(82, 'Export', true, 'Export', now(), 1, false, 16),
(83, 'Print', true, 'Print', now(), 1, false, 16),
(84, 'Search', true, 'Search', now(), 1, false, 16),
(85, 'Read', true, 'Read', now(), 1, false, 16),

--CB Products - Experiment Creation
(86, 'Read', true, 'Read', now(), 1, false, 1);


--Admin - All Product Function
INSERT INTO "security".role_product_function
(role_id, product_function_id)
(SELECT 2, id from "security".product_function WHERE id <= 86);


--CS Admin - CS Product Function
INSERT INTO "security".role_product_function
(role_id, product_function_id)
(SELECT 5, id from "security".product_function WHERE id <= 29);


--BA Admin - BA Product Function
INSERT INTO "security".role_product_function
(role_id, product_function_id)
(SELECT 6, id from "security".product_function WHERE id >= 30 and id <= 36);


--SM Admin - SM Product Function
INSERT INTO "security".role_product_function
(role_id, product_function_id)
(SELECT 7, id from "security".product_function WHERE id >= 37 and id <= 85);

--CS User - CS Product Function
INSERT INTO "security".role_product_function
(role_id, product_function_id)
VALUES
(8, 4),
(8, 11),
(8, 19),
(8, 26),
(8, 7),
(8, 14),
(8, 22),
(8, 29);

--BA User - BA Product Function
INSERT INTO "security".role_product_function
(role_id, product_function_id)
VALUES
(9, 33),
(9, 36);

--SM User - SM Product Function
INSERT INTO "security".role_product_function
(role_id, product_function_id)
(SELECT 10, pf.id FROM "security".product_function pf inner join core.product p on pf.product_id = p.id WHERE p.domain_id = 4 and pf.action = 'Export');

INSERT INTO "security".role_product_function
(role_id, product_function_id)
(SELECT 10, pf.id FROM "security".product_function pf inner join core.product p on pf.product_id = p.id WHERE p.domain_id = 4 and pf.action = 'Read');

--CB User - CB Experiment Creation
INSERT INTO "security".role_product_function
(role_id, product_function_id)
VALUES
(11, 86);


SELECT setval('"security".role_id_seq', (SELECT MAX(id) FROM "security"."role"));
SELECT setval('"security".product_function_id_seq', (SELECT MAX(id) FROM "security".product_function));

SET session_replication_role = 'origin';