--liquibase formatted sql

--changeset postgres:remove_product_prefix context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-967 Remove prefix in the cs product path


UPDATE core.product SET "path"='review' WHERE name='Genotyping Service Manager';

UPDATE core.product SET "path"='inspect' WHERE name='Inspect';

UPDATE core.product SET "path"='design' WHERE name='Design';

UPDATE core.product SET "path"='vendor' WHERE name='Vendor';

UPDATE core.product SET "path"='usermanagement' WHERE name='User Management';

UPDATE core.product SET "path"='tenantmanagement' WHERE name='Tenant Management';

UPDATE core.product SET "path"='requestmanager' WHERE name='Request Manager';

UPDATE core.product SET "path"='list' WHERE name='Analysis Request Manager';

UPDATE core.product SET "path"='crm' WHERE name='CRM';

UPDATE core.product SET "path"='settings' WHERE name='Printout';

UPDATE core.product SET "path"='sh/shipment' WHERE name='Shipment Tool';

UPDATE core.product SET "path"='under-construction' WHERE name='Service Catalog';


--Revert Changes
--rollback UPDATE core.product SET "path"='/sm/review' WHERE name='Genotyping Service Manager';
--rollback UPDATE core.product SET "path"='/sm/inspect' WHERE name='Inspect';
--rollback UPDATE core.product SET "path"='/sm/design' WHERE name='Design';
--rollback UPDATE core.product SET "path"='/sm/vendor' WHERE name='Vendor';
--rollback UPDATE core.product SET "path"='usermanagement' WHERE name='User Management';
--rollback UPDATE core.product SET "path"='tenantmanagement' WHERE name='Tenant Management';
--rollback UPDATE core.product SET "path"='/sm/requestmanager' WHERE name='Request Manager';
--rollback UPDATE core.product SET "path"='/ba/list' WHERE name='Analysis Request Manager';
--rollback UPDATE core.product SET "path"='crm' WHERE name='CRM';
--rollback UPDATE core.product SET "path"='settings' WHERE name='Printout';
--rollback UPDATE core.product SET "path"='/sm/under-construction' WHERE name='Shipment Tool';
--rollback UPDATE core.product SET "path"='/sm/under-construction' WHERE name='Service Catalog';