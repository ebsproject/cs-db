--liquibase formatted sql

--changeset postgres:update_domain_name_and_description context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-937 Update Product names and descriptions



UPDATE core."domain"
SET info='Create and manage germplasm, trials and nurseries.' WHERE id=1; --CB

UPDATE core."domain"
SET name = 'Analytics', info='Connect data to powerful analytics and decision support tools.' WHERE id=3; --BA

UPDATE core."domain"
SET info='Connect to internal and external services including genotypic, quality, and phytosanitary analyses.' WHERE id=4; --SM



--Revert Changes
--rollback UPDATE core."domain" SET info='System Administrator' WHERE id=2;
--rollback UPDATE core."domain" SET info='Breeding Management' WHERE id=1;
--rollback UPDATE core."domain" SET info='Breeding Analytics' WHERE id=3;
--rollback UPDATE core."domain" SET info='Service Management' WHERE id=4;