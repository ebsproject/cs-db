--liquibase formatted sql

--changeset postgres:change_domain_info_column_length context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-937 Update Product names and descriptions



ALTER TABLE core."domain" ALTER COLUMN info TYPE varchar(150);


--Revert Changes
--rollback ALTER TABLE core."domain" ALTER COLUMN info TYPE varchar(50);