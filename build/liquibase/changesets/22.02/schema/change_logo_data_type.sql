--liquibase formatted sql

--changeset postgres:change_logo_data_type context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-933 Change Logo data type in organization and customer tables



ALTER TABLE core.customer ALTER COLUMN logo TYPE bytea USING logo::bytea;

ALTER TABLE core.organization ALTER COLUMN logo TYPE bytea USING logo::bytea;


--Revert Changes
--rollback ALTER TABLE core.customer ALTER COLUMN logo TYPE varchar(50);
--rollback ALTER TABLE core.organization ALTER COLUMN logo TYPE varchar(50);