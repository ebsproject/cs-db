--liquibase formatted sql

--changeset postgres:remove_node_stage_from_product_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-946 Add roles and product functions



ALTER TABLE "security".product_function DROP COLUMN stage_id;

ALTER TABLE "security".product_function DROP COLUMN node_id;

ALTER TABLE "security"."role" DROP COLUMN tenant_id;


--Revert Changes
--rollback ALTER TABLE "security".product_function ADD COLUMN stage_id int4;
--rollback ALTER TABLE "security".product_function ADD COLUMN node_id int4;
--rollback ALTER TABLE "security".product_function ADD CONSTRAINT "FK_product_function_node" FOREIGN KEY (node_id) REFERENCES workflow.node(id);
--rollback ALTER TABLE "security".product_function ADD CONSTRAINT "FK_product_function_stage" FOREIGN KEY (stage_id) REFERENCES workflow.stage(id);
--rollback ALTER TABLE "security"."role" ADD COLUMN tenant_id int4;