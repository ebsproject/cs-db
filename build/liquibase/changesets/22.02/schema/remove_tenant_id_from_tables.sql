--liquibase formatted sql

--changeset postgres:remove_tenant_id_from_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-951 Remove tenant_if from tables in csdb



ALTER TABLE "security".delegation DROP COLUMN tenant_id;
ALTER TABLE "security".functional_unit DROP COLUMN tenant_id;
ALTER TABLE "security".product_authorization DROP COLUMN tenant_id;

ALTER TABLE crm.person_status DROP COLUMN tenant_id;

ALTER TABLE core.alert DROP COLUMN tenant_id;
ALTER TABLE core.printout_template DROP COLUMN tenant_id;
ALTER TABLE core.formula_type DROP COLUMN tenant_id;
ALTER TABLE core.number_sequence_rule DROP COLUMN tenant_id;
ALTER TABLE core.number_sequence_rule_segment DROP COLUMN tenant_id;
ALTER TABLE core.preference DROP COLUMN tenant_id;


--Revert Changes
--rollback ALTER TABLE "security".delegation ADD COLUMN tenant_id int4;
--rollback ALTER TABLE "security".functional_unit ADD COLUMN tenant_id int4;
--rollback ALTER TABLE "security".product_authorization ADD COLUMN tenant_id int4;

--rollback ALTER TABLE crm.person_status ADD COLUMN tenant_id int4;

--rollback ALTER TABLE core.alert ADD COLUMN tenant_id int4;
--rollback ALTER TABLE core.printout_template ADD COLUMN tenant_id int4;
--rollback ALTER TABLE core.formula_type ADD COLUMN tenant_id int4;
--rollback ALTER TABLE core.number_sequence_rule ADD COLUMN tenant_id int4;
--rollback ALTER TABLE core.number_sequence_rule_segment ADD COLUMN tenant_id int4;
--rollback ALTER TABLE core.preference ADD COLUMN tenant_id int4;