--liquibase formatted sql

--changeset postgres:create_hierarchy_product_functions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1067 create hierarchy product_functions



SET session_replication_role = 'replica';

WITH product_id as (select id from core.product where name = 'Hierarchy')

INSERT INTO "security".product_function
(id, description, system_type, "action", creation_timestamp, creator_id, is_void, product_id)
VALUES

(87, 'Create', true, 'Create', now(), 1, false, (select id from product_id)),
(88, 'Modify', true, 'Modify', now(), 1, false, (select id from product_id)),
(89, 'Delete', true, 'Delete', now(), 1, false, (select id from product_id)),
(90, 'Export', true, 'Export', now(), 1, false, (select id from product_id)),
(91, 'Print', true, 'Print', now(), 1, false, (select id from product_id)),
(92, 'Search', true, 'Search', now(), 1, false, (select id from product_id)),
(93, 'Read', true, 'Read', now(), 1, false, (select id from product_id));


INSERT INTO "security".role_product_function
(role_id, product_function_id)
values
(2, 87),
(2, 88),
(2, 89),
(2, 90),
(2, 91),
(2, 92),
(2, 93),

(5, 87),
(5, 88),
(5, 89),
(5, 90),
(5, 91),
(5, 92),
(5, 93),

(4, 87),
(4, 88),
(4, 89),
(4, 90),
(4, 91),
(4, 92),
(4, 93),

(8, 90),
(8, 93);


SELECT setval('"security".product_function_id_seq', (SELECT MAX(id) FROM "security".product_function));

SET session_replication_role = 'origin';