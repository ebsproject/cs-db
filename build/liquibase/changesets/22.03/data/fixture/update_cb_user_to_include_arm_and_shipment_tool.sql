--liquibase formatted sql

--changeset postgres:update_cb_user_to_include_arm_and_shipment_tool context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1010 Update CB User Role to include arm and shipment tool


INSERT INTO "security".role_product_function
(role_id, product_function_id)
VALUES

(11, 72),
(11, 73),
(11, 74),
(11, 75),
(11, 76),
(11, 77),
(11, 78),
(11, 30),
(11, 31),
(11, 32),
(11, 33),
(11, 34),
(11, 35),
(11, 36);

--Revert Changes
--rollback DELETE FROM "security".role_product_function WHERE role_id = 11 and product_function_id IN (72, 73, 74, 75, 76, 77, 78, 30, 31, 32, 33, 34, 35, 36);


