--liquibase formatted sql

--changeset postgres:add_hierarchy_product context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1067 Add hierarchy into product table 



INSERT INTO core.product
("name", description, help, main_entity, icon, creation_timestamp, creator_id, is_void, domain_id, htmltag_id, menu_order, "path")
VALUES('Hierarchy', 'Hierarchy', 'Hierarchy', 'Hierrachy', 'device_hub', now(), 1, false, 2, 1, 5, 'chart');


UPDATE core.product
SET icon='description'
WHERE id=21;


--Revert Changes
--rollback 