--liquibase formatted sql

--changeset postgres:add_hierrarchy_design context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1067 Add hierarchy design 



INSERT INTO core.hierarchy_type
(id, "name", description, creation_timestamp, creator_id, is_void)
VALUES(1, 'Chart', 'Chart', now(), 1, false);


INSERT INTO core."hierarchy"
(id,"name", description, hierarchy_type_id, is_system, is_template, creation_timestamp, creator_id, is_void)
VALUES(1, 'Organization Chart', 'Organization Chart', 1, true, false, now(), 1, false);


INSERT INTO core.hierarchy_design
(id, name, "filter", "level", "number", parent_id, hierarchy_id, entity_reference_id, is_required, creation_timestamp, creator_id, is_void)
VALUES
(1, 'Institution', '', 0, 1, null, 1, null, false, now(), 1, false),
(2, 'Unit', 'Type = U', 1, 1, 1, 1, (select id from core.entity_reference WHERE entity = 'Functional_Unit'), false, now(), 1, false),
(3, 'Program', '', 2, 1, 2, 1, (select id from core.entity_reference WHERE entity = 'Program'), false, now(), 1, false),
(4, 'Team', '', 3, 1, 3, 1, null, false, now(), 1, false),
(5, 'Contact', '', 4, 1, 4, 1, (select id from core.entity_reference WHERE entity = 'Contact' ), false, now(), 1, false);


SELECT setval('core.hierarchy_type_id_seq', (SELECT MAX(id) FROM core.hierarchy_type));
SELECT setval('core.hierarchy_id_seq', (SELECT MAX(id) FROM core."hierarchy"));
SELECT setval('core.hierarchy_design_id_seq', (SELECT MAX(id) FROM core.hierarchy_design));