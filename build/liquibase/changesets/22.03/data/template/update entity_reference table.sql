--liquibase formatted sql

--changeset postgres:update entity_reference table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1067 Update entity_reference table


SELECT setval('core.entity_reference_id_seq', (SELECT MAX(id) FROM core.entity_reference));


INSERT INTO core.entity_reference
(entity, textfield, valuefield, storefield, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
select initcap(table_name),'','id','',1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,false from information_schema."tables" t
where not exists (select id from core.entity_reference e where lower(e.entity) = lower(table_name) ) and table_type ='BASE TABLE' and 
not table_schema in ('pg_catalog','information_schema','public');

SELECT setval('core.attributes_id_seq', (SELECT MAX(id) FROM core."attributes"));
INSERT INTO core."attributes"
("name", description, help, sort_no, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, entityreference_id)
select column_name,column_name,'',c.ordinal_position,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,false,er.id
from   
information_schema.columns c 
--left outer join pg_catalog.pg_description pgd  on pgd.objsubid = c.ordinal_position 
--left outer join pg_catalog.pg_statio_all_tables as st on c.table_schema =st.schemaname  and c.table_name = st.relname 
   inner join core.entity_reference as er on lower(c.table_name) = lower(er.entity) 
where not c.table_schema in ('pg_catalog','information_schema','public');


SELECT setval('core.entity_reference_id_seq', (SELECT MAX(id) FROM core.entity_reference));
SELECT setval('core.attributes_id_seq', (SELECT MAX(id) FROM core."attributes"));


--Revert Changes

