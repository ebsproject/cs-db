--liquibase formatted sql

--changeset postgres:create_chart_functions context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1068 Create functions for chart structure


CREATE OR REPLACE FUNCTION core.generate_json(_id integer, _name character varying, _title character varying)
 RETURNS jsonb
 LANGUAGE plpgsql
AS $function$
BEGIN
  RETURN json_build_object(
    'id',
    _Id,
    'name',
     _name,
    'title',
     _title,
    'children',
    array(
      SELECT core.generate_json(ht.Id,trim(hd."name"),trim(ht.value))
      FROM core.hierarchy_tree ht join core.hierarchy_design hd on ht.hierarchy_design_id = hd.id 
      WHERE ht.parent_id =_id
    ));
END;
$function$
;


CREATE OR REPLACE FUNCTION core.chart_json(hierarchyid integer, designid integer)
 RETURNS jsonb
 LANGUAGE plpgsql
 STABLE
AS $function$ declare 

_id int;
_name varchar;
_title varchar;

begin
		select into				 
			_id,
			_name,
			_title
			ht.id,
			hd."name",
			ht.value
		from
			core.hierarchy_design hd
		join core.hierarchy_tree ht on
			hd.id = ht.hierarchy_design_id
		where
			hd.hierarchy_id = hierarchyId
			and hd."number" = designId
			and "level" = 0;

return jsonb_pretty(core.generate_json(_id, _name, _title));
end;

$function$
;


CREATE OR REPLACE FUNCTION core.dynamic_select(s_name character varying, t_name character varying, f_value character varying, f_text character varying)
 RETURNS TABLE(p_id integer, p_description character varying)
 LANGUAGE plpgsql
 STABLE
AS $function$
declare
m_sql text:='';
begin
   m_sql := 'select ' || quote_ident(f_value) || ',' || quote_ident(f_text) || ' from ' ||  quote_ident(s_name) || '.' || quote_ident(t_name) || ';';
  
  return query execute m_sql;
end; 
$function$
;

CREATE OR REPLACE FUNCTION core.updatetreenodes()
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare
rec record;
m_sql text; 

begin
	
	--auto-insert
	for rec in select hd.id,er.entity,er.entity_schema,er.textfield ,er.valuefield,ht.id parent_id
					   from core.hierarchy_design hd 
			  		   join core.entity_reference er on hd.entity_reference_id =er.id 
			  		   join core.hierarchy_tree ht on hd.parent_id = ht.hierarchy_design_id 
			  		   where hd.is_automatic
	loop 
	-- m_sql := 'select ' || quote_ident(f_value) || ',' || quote_ident(f_text) || ' from ' ||  quote_ident(s_name) || '.' || quote_ident(t_name) || ';';  
	   m_sql := 'INSERT INTO core.hierarchy_tree(record_id,hierarchy_design_id,value,parent_id,creator_id,modifier_id)';
	   
	   m_sql := m_sql  || ' select id,' || rec.id || ',' || quote_ident(rec.textfield) || ',' || rec.parent_id || ',1,1 from ' ||  quote_ident(rec.entity_schema) || '.' || quote_ident(rec.entity) || ' info';
	    m_sql := m_sql || ' where not exists (select * from core.hierarchy_tree ht where ht.record_id=info.id and ht.hierarchy_design_id = ' || rec.id || ');';
	  raise info  'insert %',m_sql;
	 
	  execute m_sql;
	end loop;
	
  
    --update all references
   	for rec in select hd.id,er.entity,er.entity_schema,er.textfield ,er.valuefield 
					   from core.hierarchy_design hd 
			  		   join core.entity_reference er on hd.entity_reference_id =er.id 
			  		   
			  		   
	loop 
	-- m_sql := 'select ' || quote_ident(f_value) || ',' || quote_ident(f_text) || ' from ' ||  quote_ident(s_name) || '.' || quote_ident(t_name) || ';';  
	   m_sql := 'UPDATE core.hierarchy_tree tree ';
	   m_sql := m_sql  || 'SET value= info.' || quote_ident(rec.textfield) || ' ';
	   m_sql := m_sql  || 'FROM ' ||  quote_ident(rec.entity_schema) || '.' || quote_ident(rec.entity) || ' info ';	
	   m_sql := m_sql  || 'WHERE info.id = tree.record_id  and tree.hierarchy_design_id = ' || rec.id ||';';
	  
	    raise info  'update %',m_sql;
	 
	  execute m_sql;
	end loop;

end; 
$function$
;