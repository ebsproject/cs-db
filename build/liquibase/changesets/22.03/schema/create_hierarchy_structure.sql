--liquibase formatted sql

--changeset postgres:create_hierarchy_structure context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1022 Create hierarchy structure in csdb


CREATE TABLE core.hierarchy_tree
(
	id integer NOT NULL   DEFAULT NEXTVAL(('core."hierarchy_tree_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	value varchar(150) NULL,
	parent_id integer NULL,
	record_id integer NULL,
	hierarchy_design_id integer NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE core.hierarchy
(
	id integer NOT NULL   DEFAULT NEXTVAL(('core."hierarchy_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(150) NULL,
	description varchar(300) NULL,
	hierarchy_type_id integer NULL,
	is_system boolean NULL,
	is_template boolean NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE core.hierarchy_design
(
	id integer NOT NULL   DEFAULT NEXTVAL(('core."hierarchy_design_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(150) NULL,
	filter varchar(1000) NULL,
	level integer NULL,
	number integer NULL,
	parent_id integer NULL,
	hierarchy_id integer NULL,
	entity_reference_id integer NULL,
	is_required boolean NULL,
	is_automatic boolean NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE core.hierarchy_type
(
	id integer NOT NULL   DEFAULT NEXTVAL(('core."hierarchy_type_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(150) NULL,
	description varchar(300) NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE SEQUENCE core.hierarchy_tree_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE core.hierarchy_design_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE core.hierarchy_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE core.hierarchy_type_id_seq INCREMENT 1 START 1;

ALTER TABLE core.hierarchy_tree ADD CONSTRAINT "PK_hierarchy_tree"
	PRIMARY KEY (id)
;

ALTER TABLE core.hierarchy ADD CONSTRAINT "PK_hierarchy"
	PRIMARY KEY (id)
;

ALTER TABLE core.hierarchy_design ADD CONSTRAINT "PK_hierarchy_design"
	PRIMARY KEY (id)
;

ALTER TABLE core.hierarchy_type ADD CONSTRAINT "PK_hierarchy_type"
	PRIMARY KEY (id)
;

ALTER TABLE core.hierarchy_tree ADD CONSTRAINT "FK_hierarchy_tree_hierarchy_tree"
	FOREIGN KEY (parent_id) REFERENCES core.hierarchy_tree (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.hierarchy_tree ADD CONSTRAINT "FK_hierarchy_tree_hierarchy_design"
	FOREIGN KEY (hierarchy_design_id) REFERENCES core.hierarchy_design (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.hierarchy ADD CONSTRAINT "FK_hierarchy_hierarchy_type"
	FOREIGN KEY (hierarchy_type_id) REFERENCES core.hierarchy_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.hierarchy_design ADD CONSTRAINT "FK_hierarchy_design_entity_reference"
	FOREIGN KEY (entity_reference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.hierarchy_design ADD CONSTRAINT "FK_hierarchy_design_hierarchy"
	FOREIGN KEY (hierarchy_id) REFERENCES core.hierarchy (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.hierarchy_design ADD CONSTRAINT "FK_hierarchy_design_hierarchy_design"
	FOREIGN KEY (parent_id) REFERENCES core.hierarchy_design (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN core.hierarchy_tree.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.hierarchy_tree.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.hierarchy_tree.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.hierarchy_tree.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.hierarchy_tree.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.hierarchy_tree.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.hierarchy.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.hierarchy.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.hierarchy.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.hierarchy.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.hierarchy.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.hierarchy.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.hierarchy_design.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.hierarchy_design.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.hierarchy_design.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.hierarchy_design.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.hierarchy_design.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.hierarchy_design.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.hierarchy_type.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.hierarchy_type.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.hierarchy_type.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.hierarchy_type.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.hierarchy_type.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.hierarchy_type.modifier_id
	IS 'ID of the user who last modified the record'
;