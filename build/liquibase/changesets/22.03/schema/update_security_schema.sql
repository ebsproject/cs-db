--liquibase formatted sql

--changeset postgres:update_security_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1011 Security Schema changes


ALTER TABLE security.functional_unit_user 
  DROP  CONSTRAINT "FK_functional_unit_user_functional_unit";

ALTER TABLE security.functional_unit_user 
  DROP  CONSTRAINT "FK_functional_unit_user_user";

ALTER TABLE security.functional_unit_user 
  DROP  CONSTRAINT "PK_functionalunit_user";

DROP TABLE IF EXISTS security.product_authorization;

ALTER TABLE security.functional_unit_user 
 DROP COLUMN IF EXISTS user_id;


ALTER TABLE security.functional_unit_user RENAME TO functional_unit_contact;

ALTER TABLE security.functional_unit_contact 
 ADD COLUMN contact_id integer NOT NULL;

ALTER TABLE security.functional_unit_contact ADD CONSTRAINT "PK_functional_unit_contact"
	PRIMARY KEY (functional_unit_id,contact_id)
;

ALTER TABLE security.functional_unit_contact ADD CONSTRAINT "FK_functional_unit_contact_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE security.functional_unit_contact ADD CONSTRAINT "FK_functional_unit_contact_functional_unit"
	FOREIGN KEY (functional_unit_id) REFERENCES security.functional_unit (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN security.functional_unit_contact.contact_id
	IS 'Id Reference to the Contact id'
;

