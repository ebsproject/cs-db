--liquibase formatted sql

--changeset postgres:add_ba_functions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1113 Add BA functions



do $$
declare p_id int;
declare ra_id int;
declare ru_id int;
begin

SELECT id from "security"."role" where name = 'BA Admin' INTO ra_id;
SELECT id from "security"."role" where name = 'BA User' INTO ru_id;
SELECT id from core.product where name = 'Analysis Request Manager' INTO p_id;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('Single_Design', true, 'Single_Design', 1, p_id),
        ('Multi_Design', true, 'Multi_Design', 1, p_id);

INSERT INTO "security".role_product_function
    (role_id, product_function_id)
        (select ru_id, id from "security".product_function WHERE action = 'Single_Design');

INSERT INTO "security".role_product_function
    (role_id, product_function_id)
        (select ra_id, id from "security".product_function WHERE action = 'Single_Design');

INSERT INTO "security".role_product_function
    (role_id, product_function_id)
        (select ra_id, id from "security".product_function WHERE action = 'Multi_Design');

end $$;