--liquibase formatted sql

--changeset postgres:add_crm_functions_and_data_action context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1107 Add CRM functions and data action value



SET session_replication_role = 'replica';

SELECT setval('"security".product_function_id_seq', (SELECT MAX(id) FROM "security".product_function));

do $$
declare p_id int;
begin

SELECT id from core.product where name = 'CRM' INTO p_id;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('New_Person', true, 'New_Person', 1, p_id),
        ('Download_Template_People', true, 'Download_Template_People', 1, p_id),
        ('Import_Template_People', true, 'Import_Template_People',  1, p_id),
        ('New_Institution', true, 'New_Institution', 1, p_id),  
        ('Download_Template_Institution', true, 'Download_Template_Institution', 1, p_id),
        ('Import_Template_Institution', true, 'Import_Template_Institution', 1, p_id),
        ('Assign_New_Contact', true, 'Assign_New_Contact', 1, p_id),
        ('Assign_Contact', true, 'Assign_Contact', 1, p_id);
end $$;

-- SELECT setval('"security".role_product_function_role_id_seq', (SELECT MAX(role_id) FROM "security".role_product_function));

INSERT INTO "security".role_product_function
(role_id, product_function_id)
(select 3, id from "security".product_function WHERE id >= 163);

INSERT INTO "security".role_product_function
(role_id, product_function_id)
(select 6, id from "security".product_function WHERE id >= 163);

UPDATE "security".product_function SET description='Create_Data_Source', "action"='Create_Data_Source' WHERE description='Create data source';
UPDATE "security".product_function SET description='Delete_Report_Backend', "action"='Delete_Report_Backend' WHERE description='Delete report backend';
UPDATE "security".product_function SET description='Upload_File_Backend', "action"='Upload_File_Backend' WHERE description='Upload file backend';
UPDATE "security".product_function SET description='Download_File_Backend', "action"='Download_File_Backend' WHERE description='Download file backend';
UPDATE "security".product_function SET description='Delete_File_Backend', "action"='Delete_File_Backend' WHERE description='Delete file backend';
UPDATE "security".product_function SET description='Import_Backend', "action"='Import_Backend' WHERE description='Import backend';
UPDATE "security".product_function SET description='Export_Backend', "action"='Export_Backend' WHERE description='Export backend';
UPDATE "security".product_function SET description='Download_Setting_Backend', "action"='Download_Setting_Backend' WHERE description='Download setting backend';
UPDATE "security".product_function SET description='Upload_Setting_Backend', "action"='Upload_Setting_Backend' WHERE description='Upload setting backend';
UPDATE "security".product_function SET description='Copy_Url_Backend', "action"='Copy_Url_Backend' WHERE description='Copy url backend';
UPDATE "security".product_function  SET description='Refresh_Backend', "action"='Refresh_Backend' WHERE description='Refresh backend';


UPDATE "security".product_function
SET is_data_Action = TRUE 
WHERE description = 'Create' OR description = 'Modify' OR description = 'Delete' OR description = 'Design' OR description = 'Read'
OR description = 'Create_Data_Source' OR description = 'Delete_Report_Backend' OR description = 'Delete_File_Backend' 
OR description = 'New_Person' OR description = 'New_Institution' OR description = 'Design';

SET session_replication_role = 'origin';