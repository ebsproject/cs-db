--liquibase formatted sql

--changeset postgres:add_functional_units context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1145 Add functional_units



SET session_replication_role = 'replica';

INSERT INTO "security".functional_unit
(description, "type", creator_id, is_void, code, name, program_id)
VALUES
('Global Maize Program', 'Unit', 1, false, 'GMP', 'Global Maize Program', (select id from program.program where code = 'BW')),
('Global Wheat Program', 'Unit', 1, false, 'GWP', 'Global Wheat Program', (select id from program.program where code = 'BW'));



-- Fix Typo in security.role --
UPDATE "security"."role"
SET security_group='EBS Administrators' --Admin
WHERE security_group='EBS_Administrators';

UPDATE "security"."role"
SET description='Core System Administrator'
WHERE description='Cose System Administrator';

SET session_replication_role = 'origin';