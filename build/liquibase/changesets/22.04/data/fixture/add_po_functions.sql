--liquibase formatted sql

--changeset postgres:add_po_functions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1095 Add printout actions to product_function table



SET session_replication_role = 'replica';

do $$
declare p_id int;
begin

SELECT id from core.product where name = 'Printout' INTO p_id;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('Copy', true, 'Copy', 1, p_id),
        ('Design', true, 'Design', 1, p_id),
        ('Download', true, 'Download',  1, p_id),
        ('Create data source', true, 'Export', 1, p_id),
        
        ('Delete report backend', true, 'Delete report backend', 1, p_id),
        ('Upload file backend', true, 'Upload file backend', 1, p_id),
        ('Download file backend', true, 'Download file backend', 1, p_id),
        ('Delete file backend', true, 'Delete file backend', 1, p_id),
        ('Import backend', true, 'Import backend',  1, p_id),
        ('Export backend', true, 'Export backend', 1, p_id),
        ('Download setting backend', true, 'Download setting backend', 1, p_id),
        ('Upload setting backend', true, 'Upload setting backend', 1, p_id),
        ('Copy url backend', true, 'Copy url backend', 1, p_id),
        ('Refresh backend', true, 'Refresh backend', 1, p_id);

end $$;


INSERT INTO "security".role_product_function
(role_id, product_function_id)
(select 2, id from "security".product_function WHERE id >= 149 and product_id <= 162);


INSERT INTO "security".role_product_function
(role_id, product_function_id)
(select 5, id from "security".product_function WHERE id >= 149 and product_id <= 162);

SET session_replication_role = 'origin';