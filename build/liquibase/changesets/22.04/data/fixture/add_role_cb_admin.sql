--liquibase formatted sql

--changeset postgres:add_role_cb_admin context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1092 Create CB Admin role



INSERT INTO "security"."role"
(id, description, security_group, creator_id, is_void, "name")
VALUES(12, 'Core Breeding Admin', 'EBS Administrators', 1, false, 'CB Admin');



INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('Create', true, 'Create', 1, 1),
        ('Modify', true, 'Modify', 1, 1),
        ('Delete', true, 'Delete',  1, 1),
        ('Export', true, 'Export', 1, 1),
        ('Print', true, 'Print', 1, 1),
        ('Search', true, 'Search', 1, 1);

do $$
declare temprow record;
begin

FOR temprow IN
    SELECT id, "name" FROM core.product where domain_id = 1 and id <> 1
LOOP
    INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('Create', true, 'Create', 1, temprow.id),
        ('Modify', true, 'Modify', 1, temprow.id),
        ('Delete', true, 'Delete',  1, temprow.id),
        ('Export', true, 'Export', 1, temprow.id),
        ('Print', true, 'Print', 1, temprow.id),
        ('Search', true, 'Search', 1, temprow.id),
		('Read', true, 'Read', 1, temprow.id);
END LOOP;
end $$;

INSERT INTO "security".role_product_function
(role_id, product_function_id)
(select 12, id from "security".product_function WHERE product_id <= 6);


SELECT setval('"security".role_id_seq', (SELECT MAX(id) FROM "security"."role"));
