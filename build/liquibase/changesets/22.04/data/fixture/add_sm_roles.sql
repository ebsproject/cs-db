--liquibase formatted sql

--changeset postgres:add_sm_roles context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1129 Add SM roles to csdb



INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, "name")
VALUES
('Breeder', 'SM Users', 1, false, 'Breeder'),
('Molecular Breeder', 'SM Users', 1, false, 'Molecular Breeder'),
('Lab Manager', 'SM Users', 1, false, 'Lab Manager'),
('Data Manager', 'SM Users', 1, false, 'Data Manager');

do $$
declare b_id int;
declare mb_id int;
declare lm_id int;
declare dm_id int;
declare temprow record;

begin

SELECT id from "security"."role" where name = 'Breeder' INTO b_id;
SELECT id from "security"."role" where name = 'Molecular Breeder' INTO mb_id;
SELECT id from "security"."role" where name = 'Lab Manager' INTO lm_id;
SELECT id from "security"."role" where name = 'Data Manager' INTO dm_id;


FOR temprow IN
    SELECT id, name FROM core.product where name = 'Request Manager' or name = 'Genotyping Service Manager'
LOOP
    INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('View', true, 'View', 1, temprow.id);
    
        IF temprow.name = 'Genotyping Service Manager'
            THEN
                INSERT INTO "security".product_function (description, system_type, "action", creator_id, product_id)
                    VALUES
                        ('Accept', true, 'Accept', 1, temprow.id),
                        ('Reject', true, 'Reject', 1, temprow.id);
        END IF;
END LOOP;


FOR temprow IN
    SELECT id FROM "security".product_function where product_id = 14
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select b_id, temprow.id);
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select mb_id, temprow.id);
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select dm_id, temprow.id);
END LOOP;

FOR temprow IN
    SELECT id FROM "security".product_function where product_id = 20
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select lm_id, temprow.id);
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select dm_id, temprow.id);
END LOOP;

end $$;
