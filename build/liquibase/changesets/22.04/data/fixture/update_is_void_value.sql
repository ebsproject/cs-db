--liquibase formatted sql

--changeset postgres:update_is_void_value context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1117 Update is_void value for user management product_functions



UPDATE "security".product_function
SET is_void = false WHERE id=1;

UPDATE "security".product_function
SET is_void = false WHERE id=2;

UPDATE "security".product_function
SET is_void = false WHERE id=3;