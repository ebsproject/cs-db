--liquibase formatted sql

--changeset postgres:update_role_to_admin_user context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS2-107 Modify the admin@ebsproject.org Role

UPDATE "security".user_role
SET role_id=12
WHERE user_id=1;


UPDATE "security"."user"
SET default_role_id = 12
WHERE user_name='admin@ebsproject.org';
