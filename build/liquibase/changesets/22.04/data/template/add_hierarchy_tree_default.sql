--liquibase formatted sql

--changeset postgres:add_hierarchy_tree_default context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1130 Add default hierarchy_tree



INSERT INTO core.hierarchy_tree
(id, value, hierarchy_design_id, creator_id, is_void)
VALUES(1, 'CIMMYT', 1, 1, false);
