--liquibase formatted sql

--changeset postgres:add_role_level_to_hierarchy_design context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1078 Add role level to hierarchy_design table



INSERT INTO core.hierarchy_design
(id, name, "filter", "level", "number", parent_id, hierarchy_id, entity_reference_id, is_required, creation_timestamp, creator_id, is_void)
VALUES
(6, 'Role', '', 4, 1, 4, 1, (select id from core.entity_reference WHERE entity = 'Role'), false, now(), 1, false);

UPDATE core.hierarchy_design
SET "level"=5, "number"=1, parent_id=6 WHERE id=5;

SELECT setval('core.hierarchy_design_id_seq', (SELECT MAX(id) FROM core.hierarchy_design));