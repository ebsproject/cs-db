--liquibase formatted sql

--changeset postgres:set_role_not_visible context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1084 Set role not visible


UPDATE core.hierarchy_design
SET is_visible = false WHERE name = 'Role';

