--liquibase formatted sql

--changeset postgres:update_entity_reference_textfield_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1123 Update entity_reference textfield value



UPDATE core.entity_reference
SET textfield='email', entity_schema='crm'
WHERE entity='Contact';

UPDATE core.entity_reference
SET textfield='code', entity_schema='security'
WHERE entity='Functional_Unit';

UPDATE core.entity_reference
SET  entity_schema='program'
WHERE entity='Program';

UPDATE core.entity_reference
SET textfield='name', entity_schema='security'
WHERE entity='Role';
