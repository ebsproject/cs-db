--liquibase formatted sql

--changeset postgres:update_hierarchy_tree_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1145 Update hierarchy_tree value



UPDATE core.hierarchy_tree
SET value = 'Institution'
WHERE value = 'CIMMYT';


