--liquibase formatted sql

--changeset postgres:add_column_is_visible_to_hierarchy_design context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1084 Add column is_visible to hierarchy_design table



ALTER TABLE core.hierarchy_design 
 ADD COLUMN is_visible boolean NULL DEFAULT true;


COMMENT ON COLUMN core.hierarchy_design.is_visible
	IS 'Determines if the level of the hierarchy is visible in the interface or not';