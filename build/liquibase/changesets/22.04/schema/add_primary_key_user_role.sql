--liquibase formatted sql

--changeset postgres:add_primary_key_user_role context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1141 Create primary key user_role



ALTER TABLE security.user_role ADD CONSTRAINT "PK_user_role"
	PRIMARY KEY (role_id,user_id);