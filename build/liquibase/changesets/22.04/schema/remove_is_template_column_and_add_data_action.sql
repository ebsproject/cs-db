--liquibase formatted sql

--changeset postgres:remove_is_template_column_and_add_data_action context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1076 Remove column in hierarchy and add data_action column



ALTER TABLE core.hierarchy 
 DROP COLUMN IF EXISTS is_template;


ALTER TABLE security.product_function 
 ADD COLUMN is_data_action boolean NOT NULL   DEFAULT false;