--liquibase formatted sql

--changeset postgres:update_dynamic_select_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1097 Update dynamic_select function in CSDB



DROP FUNCTION core.dynamic_select(character varying,character varying,character varying,character varying);

CREATE OR REPLACE FUNCTION core.dynamic_select(s_name character varying, t_name character varying, f_value character varying, f_text character varying)
 RETURNS TABLE(id integer, description character varying)
 LANGUAGE plpgsql
 STABLE
AS $function$
declare
m_sql text:='';
begin
   m_sql := 'select ' || quote_ident(f_value) || ',' || quote_ident(f_text) || ' from ' ||  quote_ident(s_name) || '.' || quote_ident(t_name) || ';';
  return query execute m_sql;
end; 
$function$
;
