--liquibase formatted sql

--changeset postgres:add_sm_service_provider context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1219 Add SM Service Providers




do $$
declare purpose int;
declare category int;
declare ct_id int;

begin 

SELECT id from crm.purpose where name = 'Lab Services' INTO purpose;
SELECT id from crm.category where name = 'Internal Unit' INTO category;
SELECT id from crm.contact_type where name = 'Service Provider' INTO ct_id;


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'cimmytindia@cimmyt.org', 4, 2)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id)
VALUES ('CIMMYT India', 'CIMMYT India', (select id from contact), 1);


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'cimmytkenya@cimmyt.org', 4, 2)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id)
VALUES ('CIMMYT Kenya', 'CIMMYT Kenya', (select id from contact), 1);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'wmbl@cimmyt.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('WMBL', 'CIMMYT Mexico Wheat Molecular Laboratory', (select id from contact), 1, 1);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'asm@cimmyt.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('ASM', 'CIMMYT India Maize Molecular Laboratory', (select id from contact), 1, 2);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'afm@cimmyt.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('AFM', 'CIMMYT Kenya Maize Molecular Laboratory', (select id from contact), 1, 3);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'lam@cimmyt.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('LAM', 'CIMMYT Mexico Maize Molecular Laboratory', (select id from contact), 1, 4);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'shlm@cimmyt.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('SHLM', 'CIMMYT Mexico Maize Seed Health Laboratory', (select id from contact), 1, 5);


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'shlw@cimmyt.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('SHLW', 'CIMMYT Mexico Wheat Seed Health Laboratory', (select id from contact), 1, 6);


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'mzq@cimmyt.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('MZQ', 'CIMMYT Mexico Maize Quality Laboratory', (select id from contact), 1, 7);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'wql@cimmyt.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('WQL', 'CIMMYT Mexico Wheat Quality Laboratory', (select id from contact), 1, 8);


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'sdu@cimmyt.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('SDU', 'CIMMYT Mexico Seed Distribution Unit', (select id from contact), 1, 9);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'gsl@irri.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('GSL', 'IRRI Philippines Genotyping Service Lab', (select id from contact), 1, 10);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'shu@irri.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('SHU', 'IRRI Philippines Seed Health Unit', (select id from contact), 1, 11);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, purpose_id, category_id)
VALUES
    (1, 'gqnsl@irri.org', purpose, category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('GQNSL', 'IRRI Philippines Grain Quality Nutrition Service Lab', (select id from contact), 1, 12);



INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'WMBL'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'ASM'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'AFM'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'LAM'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'SHLM'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'SHLW'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'MZQ'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'WQL'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'SDU'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'GSL'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'SHU'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'GQNSL'));


--- Add hierarchy
INSERT INTO crm."hierarchy"
    (contact_id, parent_id, is_principal_contact, creator_id)
VALUES
    ((select id from crm.contact where email = 'gqnsl@irri.org'), 74, true, 1),
    ((select id from crm.contact where email = 'shu@irri.org'), 74, true, 1),
    ((select id from crm.contact where email = 'gsl@irri.org'), 74, true, 1),

    ((select id from crm.contact where email = 'sdu@cimmyt.org'), 73, true, 1),
    ((select id from crm.contact where email = 'wql@cimmyt.org'), 73, true, 1),
    ((select id from crm.contact where email = 'mzq@cimmyt.org'), 73, true, 1),
    ((select id from crm.contact where email = 'shlw@cimmyt.org'), 73, true, 1),
    ((select id from crm.contact where email = 'shlm@cimmyt.org'), 73, true, 1),
    ((select id from crm.contact where email = 'lam@cimmyt.org'), 73, true, 1),
    ((select id from crm.contact where email = 'wmbl@cimmyt.org'), 73, true, 1),

    ((select id from crm.contact where email = 'afm@cimmyt.org'), (select id from crm.contact where email = 'cimmytkenya@cimmyt.org'), true, 1),
    ((select id from crm.contact where email = 'asm@cimmyt.org'), (select id from crm.contact where email = 'cimmytindia@cimmyt.org'), true, 1)
    ;
   
end $$

