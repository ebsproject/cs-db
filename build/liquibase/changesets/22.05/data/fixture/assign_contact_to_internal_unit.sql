--liquibase formatted sql

--changeset postgres:assign_contact_to_internal_unit context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1239 Assign contacts to internal units



do $$
declare c_id int;
declare p_id int;

begin
select id from crm.contact where email = 'a.caballero@cimmyt.org' INTO c_id;
select id from crm.contact where email = 'wmbl@cimmyt.org' INTO c_id;

INSERT INTO crm."hierarchy"
(contact_id, parent_id, creator_id)
VALUES(c_id, p_id, 1);

end $$