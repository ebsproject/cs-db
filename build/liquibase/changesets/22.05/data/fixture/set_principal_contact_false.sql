--liquibase formatted sql

--changeset postgres:set_principal_contact_false context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1238 Create partial index in crm.hierarchy



UPDATE crm.hierarchy 
SET is_principal_contact = false;