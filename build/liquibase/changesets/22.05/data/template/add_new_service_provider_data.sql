--liquibase formatted sql

--changeset postgres:add_new_service_provider_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1219 Add Service Provider as a Contact type
--validChecksum: 8:8153881dfc0635bfed8cf19c74cd06e4


--Create a new category: “Internal Unit”
INSERT INTO crm.category
("name", description, creator_id, is_void)
VALUES('Internal Unit', 'Internal Unit', 1, false);


--Create a new purpose: “Lab Services“
INSERT INTO crm.purpose
("name", creator_id, is_void, category_id)
VALUES('Lab Services', 1, false, (select id from crm.category where name = 'Internal Unit'));


--Create a new Contact type : “Service Provider“
INSERT INTO crm.contact_type
("name", creator_id, is_void, category_id)
VALUES('Service Provider', 1, false, 2);
