--liquibase formatted sql

--changeset postgres:update_program_textfield context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1210 Update entity_reference textfield for Program



UPDATE core.entity_reference
SET textfield='code'
WHERE entity='Program';

update crm.contact 
set email='tmp@tmp.com' 
where email is null;
