--liquibase formatted sql

--changeset postgres:add_new_colun_external_code context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1219 Add new column external code on Institution table




ALTER TABLE crm.institution 
 ADD COLUMN external_code integer NULL;

 