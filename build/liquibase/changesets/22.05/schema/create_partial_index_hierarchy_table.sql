--liquibase formatted sql

--changeset postgres:create_partial_index_hierarchy_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1238 Create partial index in crm.hierarchy



create unique index unq_principal_contact
    on crm.hierarchy(parent_id)
    where is_principal_contact;