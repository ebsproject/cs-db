--liquibase formatted sql

--changeset postgres:create_relation_instance_crop context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1136 Create relation crop - instance



ALTER TABLE core.instance 
 ADD COLUMN crop_id integer NULL;

 ALTER TABLE core.instance ADD CONSTRAINT "FK_instance_crop"
	FOREIGN KEY (crop_id) REFERENCES program.crop (id) ON DELETE No Action ON UPDATE No Action;


COMMENT ON COLUMN core.instance.crop_id
	IS 'Reference id of the Crop related to the Instance';
