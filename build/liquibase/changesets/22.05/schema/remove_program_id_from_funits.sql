--liquibase formatted sql

--changeset postgres:remove_program_id_from_funits context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1145 Remove column program_id


ALTER TABLE "security".functional_unit
 DROP COLUMN IF EXISTS "program_id"; 


