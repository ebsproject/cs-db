--liquibase formatted sql

--changeset postgres:remove_user_preference_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1154 Remove tables related to user preferences



DROP TABLE IF EXISTS core.preference;

DROP TABLE IF EXISTS core.style_theme;

ALTER TABLE security."user" 
 ADD COLUMN preference jsonb NULL;