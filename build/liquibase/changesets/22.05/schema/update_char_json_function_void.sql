--liquibase formatted sql

--changeset postgres:update_char_json_function_void context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1130 Update chart_json function



DROP FUNCTION core.chart_json(integer,integer);

CREATE OR REPLACE FUNCTION core.chart_json(hierarchyid integer, designid integer)
RETURNS varchar
LANGUAGE plpgsql
STABLE
AS $function$ declare

_id int;
_name varchar;
_title varchar;

begin
    select into
        _id,
        _name,
        _title
        ht.id,
        hd."name",
        ht.value
    from
        core.hierarchy_design hd
    join core.hierarchy_tree ht on
        hd.id = ht.hierarchy_design_id
    where
        hd.hierarchy_id = hierarchyId
        and hd."number" = designId
        and "level" = 0 and ht.is_void = false;

return jsonb_pretty(core.generate_json(_id, _name, _title));
end;

$function$
;
