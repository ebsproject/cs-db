--liquibase formatted sql

--changeset postgres:add_data_explorer_functions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS2-216 Add BA Data Explorer Functions





do $$
declare r_admin int;
declare r_ba_user int;
declare r_ba_admin int;
declare d_exp int;
declare temprow record;

begin

SET session_replication_role = 'replica';

SELECT id FROM "security"."role" where name = 'Admin' INTO r_admin;
SELECT id FROM "security"."role" where name = 'BA User' INTO r_ba_user;
SELECT id FROM "security"."role" where name = 'BA Admin' INTO r_ba_admin;
SELECT id FROM core.product where name = 'Data Explorer' INTO d_exp;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
VALUES
    ('Create', true, 'Create', 1, d_exp),
    ('Modify', true, 'Modify', 1, d_exp),
    ('Delete', true, 'Delete', 1, d_exp),
    ('Export', true, 'Export', 1, d_exp),
    ('Print', true, 'Print', 1, d_exp),
    ('Search', true, 'Search', 1, d_exp),
    ('Read', true, 'Read', 1, d_exp);


FOR temprow IN
    SELECT id FROM "security".product_function where product_id = d_exp
LOOP
    -- INSERT INTO "security".role_product_function (role_id, product_function_id)
    --     (select r_admin, temprow.id);
    -- INSERT INTO "security".role_product_function (role_id, product_function_id)
    --     (select r_ba_admin, temprow.id);
    -- INSERT INTO "security".role_product_function (role_id, product_function_id)
    --     (select r_ba_user, temprow.id);
END LOOP;

SET session_replication_role = 'origin';

end $$
