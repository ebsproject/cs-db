--liquibase formatted sql

--changeset postgres:add_default_crop context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1281 Add default Crop and link it to instance


SET session_replication_role = 'replica';

-- INSERT INTO "program".crop
-- (id, code, name, description, notes, creator_id)
-- VALUES(0, 'Default', 'Default', 'Default', 'Default Crop', 1);

SELECT setval('"program".crop_id_seq', (SELECT MAX(id) FROM "program".crop));

UPDATE core."instance"
SET crop_id= 0
WHERE name = 'Default';

SET session_replication_role = 'origin';

