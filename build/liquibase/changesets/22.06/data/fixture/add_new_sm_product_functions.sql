--liquibase formatted sql

--changeset postgres:add_new_sm_product_functions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1230 Add new sm Product Functions



--Add product_functions
do $$
declare bm_id int;
declare bc_id int;

begin

SELECT id from core.product where name = 'Batch Manager' INTO bm_id;

SET session_replication_role = 'replica';

-- INSERT INTO "security".product_function
-- (description, system_type, "action", creator_id, is_void, product_id)
-- VALUES
-- ('Create', true, 'Create', 1, false, bm_id),
-- ('Modify', true, 'Modify', 1, false, bm_id),
-- ('Delete', true, 'Delete', 1, false, bm_id),
-- ('Export', true, 'Export', 1, false, bm_id),
-- ('Print', true, 'Print', 1, false, bm_id),
-- ('Search', true, 'Search', 1, false, bm_id),
-- ('Read', true, 'Read', 1, false, bm_id);

end $$;


--Insert relation role - Product_function
do $$
declare mb_id int;
declare lm_id int;
declare dm_id int;
declare ad_id int;
declare sma_id int;
declare temprow record;

begin

SELECT id from "security"."role" where name = 'Molecular Breeder' INTO mb_id;
SELECT id from "security"."role" where name = 'Lab Manager' INTO lm_id;
SELECT id from "security"."role" where name = 'Data Manager' INTO dm_id;
SELECT id from "security"."role" where name = 'Admin' INTO ad_id;
SELECT id from "security"."role" where name = 'SM Admin' INTO sma_id;

FOR temprow IN
    SELECT pf.id
    FROM "security".product_function pf
    inner join core.product p
    on pf.product_id = p.id
    where p.name = 'Batch Manager'
LOOP
    -- INSERT INTO "security".role_product_function (role_id, product_function_id)
    --     (select mb_id, temprow.id);
    -- INSERT INTO "security".role_product_function (role_id, product_function_id)
    --     (select lm_id, temprow.id);
    -- INSERT INTO "security".role_product_function (role_id, product_function_id)
    --     (select dm_id, temprow.id);
    -- INSERT INTO "security".role_product_function (role_id, product_function_id)
    --     (select ad_id, temprow.id);
    -- INSERT INTO "security".role_product_function (role_id, product_function_id)
    --     (select sma_id, temprow.id);
END LOOP;

SET session_replication_role = 'origin';
end $$;
