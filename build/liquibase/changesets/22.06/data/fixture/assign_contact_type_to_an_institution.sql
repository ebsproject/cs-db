--liquibase formatted sql

--changeset postgres:assign_contact_type_to_an_institution context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1285 Assign contact_type to an Institution



do $$
declare temprow record;
declare ct int;

begin
SELECT id FROM crm.contact_type WHERE name = 'Customer' INTO ct;

FOR temprow IN SELECT id, contact_id FROM crm.institution
    loop    
        INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
        VALUES(ct, temprow.contact_id);
    end loop;
end $$
