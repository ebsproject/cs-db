--liquibase formatted sql

--changeset postgres:include_ba_functions_to_admin_role context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1113 Include BA functions to role admin



do $$
declare r_admin int;

begin

SET session_replication_role = 'replica';

SELECT id FROM "security"."role" where name = 'Admin' INTO r_admin;


-- INSERT INTO "security".role_product_function
--     (role_id, product_function_id)
--         (select r_admin, id from "security".product_function WHERE action = 'Single_Design');

-- INSERT INTO "security".role_product_function
--     (role_id, product_function_id)
--         (select r_admin, id from "security".product_function WHERE action = 'Multi_Design');

SET session_replication_role = 'origin';

end $$