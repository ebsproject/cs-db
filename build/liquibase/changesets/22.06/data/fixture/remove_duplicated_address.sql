--liquibase formatted sql

--changeset postgres:remove_duplicated_address context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1280 Remove duplicated adress



DELETE FROM crm.address 
WHERE id > 3;

UPDATE crm.address
SET "location"='Mexico'
WHERE id=2;

UPDATE crm.address
SET "location"='Philippines' 
WHERE id=3;
