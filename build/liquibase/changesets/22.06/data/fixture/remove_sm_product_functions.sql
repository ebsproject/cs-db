--liquibase formatted sql

--changeset postgres:remove_sm_product_functions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1230 Remove sm Product Functions


--Remove data from role_product_function
do $$
declare temprow record;

begin

FOR temprow IN
    SELECT pf.id
    FROM "security".product_function pf
    inner join core.product p
    on pf.product_id = p.id
    where p.name = 'Inspect' or p.name = 'Design' or p.name = 'Vendor'
LOOP
    DELETE FROM "security".role_product_function WHERE product_function_id = temprow.id;
END LOOP;

end $$;



--Remove product_functions
DELETE FROM "security".product_function
WHERE product_id = (select id from core.product where name = 'Inspect');

DELETE FROM "security".product_function
WHERE product_id = (select id from core.product where name = 'Design');

DELETE FROM "security".product_function
WHERE product_id = (select id from core.product where name = 'Vendor');


