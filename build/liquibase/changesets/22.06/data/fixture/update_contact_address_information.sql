--liquibase formatted sql

--changeset postgres:update_contact_address_information context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1280 Update contact_address information



do $$
declare temprow record;

begin
FOR temprow IN SELECT id, email FROM crm.contact
	loop
		INSERT INTO crm.contact_address (contact_id, address_id)
        VALUES
            (temprow.id, 
                CASE 
                    WHEN split_part(temprow.email, '@', 2) = 'irri.org' THEN 3
                    WHEN split_part(temprow.email, '@', 2) = 'gmail.com' THEN 3
                    else 2
                END);
	end loop;

end $$
