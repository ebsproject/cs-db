--liquibase formatted sql

--changeset postgres:add_cross_manager_tool_in_product_tale context:template splitStatements:false rollbackSplitStatements:false
--comment: CS2-231 Printout - Add Cross Manager Tool in the Product List




INSERT INTO core.product
(name, description, help, main_entity, icon, creator_id, domain_id, htmltag_id, menu_order, "path")
VALUES('Cross Manager', 'The Cross Manager(CM) Tool enables the users to (a) create new cross lists to allow importing of parents from various experiments/occurrences and (b) update existing cross lists for Intentional Crossing Nurseries to allow amendments to planned crosses after the experiments have been created.', 
'Cross Manager', 'Cross', '', 1, 1, 1, 9, '/');

