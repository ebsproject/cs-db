--liquibase formatted sql

--changeset postgres:add_data_explorer_tool_in_product_table context:template splitStatements:false rollbackSplitStatements:false
--comment: CS2-216 Add BA Data Explorer Tool in the Product List



INSERT INTO core.product
    (name, description, help, main_entity, icon, creator_id, domain_id, htmltag_id, menu_order, "path")
VALUES
    ('Data Explorer', 'Data Explorer', 'Data Explorer', 'Explorer', '', 1, (select id from core.domain where name = 'Analytics'), 1, 3, 'data-explorer')
    ;

