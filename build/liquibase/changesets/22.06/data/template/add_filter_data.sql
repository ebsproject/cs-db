--liquibase formatted sql

--changeset postgres:add_filter_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1272 Add filter data



INSERT INTO core."filter"
(id, description, is_graph_service, api_url, param_path, body, param_query, "method", is_system, tooltip, hierarchy_design_id, tenant_id, creator_id)
VALUES(1, 'Program', true, 'https://csapi-dev.ebsproject.org/graphql', NULL, 'query{findProgramList{content{id code name}}}', NULL, 'post', true, NULL, NULL, 1,1);
INSERT INTO core."filter"
(id, description, is_graph_service, api_url, param_path, body, param_query, "method", is_system, tooltip, hierarchy_design_id, tenant_id, creator_id)
VALUES(2, 'Experiment Name', false, 'https://cbapi-qa.ebsproject.org/v3/experiments/', NULL, NULL, NULL, 'get', true, NULL, NULL, 1, 1);
INSERT INTO core."filter"
(id, description, is_graph_service, api_url, param_path, body, param_query, "method", is_system, tooltip, hierarchy_design_id, tenant_id, creator_id)
VALUES(3, 'Service Request Type', true, 'https://smapi-dev.ebsproject.org/graphql', NULL, 'query{findServiceTypeList{content{id name}}}', NULL, 'post', true, NULL, NULL, 1, 1);
INSERT INTO core."filter"
(id, description, is_graph_service, api_url, param_path, body, param_query, "method", is_system, tooltip, hierarchy_design_id, tenant_id, creator_id)
VALUES(4, 'Type of Sample List', false, 'https://cbapi-qa.ebsproject.org/v3/experiment-packages-search', NULL, NULL, NULL, 'get', true, NULL, NULL, 1, 1);
INSERT INTO core."filter"
(id, description, is_graph_service, api_url, param_path, body, param_query, "method", is_system, tooltip, hierarchy_design_id, tenant_id, creator_id)
VALUES(5, 'Name of Sample List', true, 'https://smapi-dev.ebsproject.org/graphql', NULL, 'query{findSampleList{content{id sampleNumber}}}', NULL, 'post', true, NULL, NULL, 1, 1);
INSERT INTO core."filter"
(id, description, is_graph_service, api_url, param_path, body, param_query, "method", is_system, tooltip, hierarchy_design_id, tenant_id, creator_id)
VALUES(7, 'Year', false, '', NULL, '', NULL, 'post', true, NULL, NULL, 1, 1);
INSERT INTO core."filter"
(id, description, is_graph_service, api_url, param_path, body, param_query, "method", is_system, tooltip, hierarchy_design_id, tenant_id, creator_id)
VALUES(8, 'Tenant', true, 'https://csapi-dev.ebsproject.org/graphql', NULL, 'query{findTenantList{content{id name}}}', NULL, 'post', true, NULL, NULL, 1, 1);


INSERT INTO core.filter_domain
(filter_id, domain_id)
VALUES(1, 4);
INSERT INTO core.filter_domain
(filter_id, domain_id)
VALUES(2, 4);
INSERT INTO core.filter_domain
(filter_id, domain_id)
VALUES(3, 4);
INSERT INTO core.filter_domain
(filter_id, domain_id)
VALUES(4, 4);
INSERT INTO core.filter_domain
(filter_id, domain_id)
VALUES(5, 4);
INSERT INTO core.filter_domain
(filter_id, domain_id)
VALUES(1, 3);
INSERT INTO core.filter_domain
(filter_id, domain_id)
VALUES(8, 2);
INSERT INTO core.filter_domain
(filter_id, domain_id)
VALUES(7, 4);


SELECT setval('core.filter_id_seq', (SELECT MAX(id) FROM core."filter"));