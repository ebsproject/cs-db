--liquibase formatted sql

--changeset postgres:add_new_purpose_laboratory context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1275 Add new purpose Laboratory



INSERT INTO crm.purpose
("name", creator_id, category_id)
VALUES('Laboratory', 1, (select id from crm.category where name = 'Person'));