--liquibase formatted sql

--changeset postgres:remove_batch_creation context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1230 Remove SM Products



DELETE FROM core.product
WHERE name = 'Batch Creation';