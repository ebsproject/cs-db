--liquibase formatted sql

--changeset postgres:remove_and_add_sm_products context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1230 Remove SM Products



--Remove SM Products
DELETE FROM core.product
WHERE name = 'Inspect';
DELETE FROM core.product
WHERE name = 'Design';
DELETE FROM core.product
WHERE name = 'Vendor';


--Add Batch Products
INSERT INTO core.product
("name", description, help, main_entity, icon, is_void, domain_id, htmltag_id, menu_order, "path", creator_id)
VALUES
('Batch Creation', ' Batch Creation', 'Batch Creation', 'Batch', ' ', false, 4, 1, 4, 'BatchCreation', 1),
('Batch Manager', ' Batch Manager', 'Batch Manager', 'Batch', ' ', false, 4, 1, 5, 'BatchManager', 1)
;

