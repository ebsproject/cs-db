--liquibase formatted sql

--changeset postgres:set_default_preference_to_users context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1284 Set default preference to users



UPDATE "security"."user"
SET preference='{"filters": [], "language": [], "favorites": {"domain": [], "columns": [], "products": []}}'::jsonb;
