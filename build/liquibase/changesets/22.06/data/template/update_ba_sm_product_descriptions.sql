--liquibase formatted sql

--changeset postgres:update_ba_sm_product_descriptions context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1265 Update BA and SM Product Descriptions



UPDATE core.product
SET description='Create new request for genotyping service for one or more germplasm list'
WHERE name = 'Genotyping Service Manager';

UPDATE core.product
SET description='Create different types of services aimed at helping breeders and their teams to produce data for faster and more accurate decision making'
WHERE name = 'Request Manager';

UPDATE core.product
SET description='Shipment management and reporting for different vendors'
WHERE name = 'Shipment Tool';
	
UPDATE core.product
SET description='Catalog mangement for  samples, services and forms'
WHERE name = 'Service Catalog';

UPDATE core.product
SET description='Create a batch for one or more requests'
WHERE name = 'Batch Manager';
	
UPDATE core.product
SET description='Assists the creation and management of Analysis Request for breeding trials.'
WHERE name = 'Analysis Request Manager';

UPDATE core.product
SET description='Gives the user the ability to explore collected trait data uploaded into Core Breeding.'
WHERE name = 'Data Explorer';
