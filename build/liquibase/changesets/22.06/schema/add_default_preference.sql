--liquibase formatted sql

--changeset postgres:add_default_preference context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1284 Add default value for column preference in security.user table



ALTER TABLE "security"."user"
 ALTER COLUMN preference SET DEFAULT '{"filters": [], "language": [], "favorites": {"domain": [], "columns": [], "products": []}}';

