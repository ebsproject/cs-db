--liquibase formatted sql

--changeset postgres:create_filter_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1250 Update model to support filter component



CREATE TABLE core.filter
(
	id integer NOT NULL   DEFAULT NEXTVAL(('core."filter_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	description varchar(250) NULL,
	is_graph_service boolean NOT NULL   DEFAULT true,
	api_url varchar(500) NOT NULL,
	param_path varchar(250) NULL,
	body varchar(500) NULL,
	param_query varchar(500) NULL,
	method varchar(50) NOT NULL   DEFAULT 'get',
	is_system boolean NOT NULL   DEFAULT true,
	tooltip varchar(500) NULL,
	hierarchy_design_id integer NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE core.filter_domain
(
	filter_id integer NOT NULL,
	domain_id integer NOT NULL
)
;

CREATE SEQUENCE core.filter_id_seq INCREMENT 1 START 1;

ALTER TABLE core.filter ADD CONSTRAINT "PK_filter"
	PRIMARY KEY (id)
;

ALTER TABLE core.filter ADD CONSTRAINT "FK_filter_hierarchy_design"
	FOREIGN KEY (hierarchy_design_id) REFERENCES core.hierarchy_design (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.filter_domain ADD CONSTRAINT "PK_filter_domain"
	PRIMARY KEY (filter_id,domain_id)
;

ALTER TABLE core.filter_domain ADD CONSTRAINT "FK_filter_domain_domain"
	FOREIGN KEY (domain_id) REFERENCES core.domain (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.filter_domain ADD CONSTRAINT "FK_filter_domain_filter"
	FOREIGN KEY (filter_id) REFERENCES core.filter (id) ON DELETE No Action ON UPDATE No Action
;


COMMENT ON COLUMN core.filter.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.filter.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.filter.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.filter.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.filter.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.filter.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.filter.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;