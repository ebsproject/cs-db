--liquibase formatted sql

--changeset postgres:create_global_address_structure context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1280 Modify model to support a global address list



ALTER TABLE crm.address 
  DROP  CONSTRAINT "FK_address_contact";

ALTER TABLE crm.address 
 DROP COLUMN IF EXISTS contact_id;

ALTER TABLE crm.address 
 DROP COLUMN IF EXISTS is_default;

ALTER TABLE core.customer 
 ADD COLUMN address_id integer NULL;

ALTER TABLE core.organization 
 ADD COLUMN address_id integer NULL;

CREATE TABLE crm.contact_address
(
	address_id integer NOT NULL,
	contact_id integer NOT NULL
)
;

ALTER TABLE crm.contact_address ADD CONSTRAINT "PK_contact_address"
	PRIMARY KEY (address_id,contact_id)
;

ALTER TABLE core.customer ADD CONSTRAINT "FK_customer_address"
	FOREIGN KEY (address_id) REFERENCES crm.address (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.organization ADD CONSTRAINT "FK_organization_address"
	FOREIGN KEY (address_id) REFERENCES crm.address (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact_address ADD CONSTRAINT "FK_contact_address_address"
	FOREIGN KEY (address_id) REFERENCES crm.address (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact_address ADD CONSTRAINT "FK_contact_address_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN core.customer.address_id
	IS 'Id Reference to the default address of the Custumer'
;

COMMENT ON COLUMN core.organization.address_id
	IS 'Id Reference to the default address of the Organization'
;
