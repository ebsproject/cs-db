--liquibase formatted sql

--changeset postgres:update_generate_json_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1258 Update core.generate_json function



CREATE OR REPLACE FUNCTION core.generate_json(_id integer, _name character varying, _title character varying)
 RETURNS jsonb
 LANGUAGE plpgsql
AS $function$
BEGIN
  RETURN json_build_object(
    'id',
    _Id,
    'name',
     _name,
    'title',
     _title,
    'children',
    array(
      SELECT core.generate_json(ht.Id,trim(hd."name"),trim(ht.value))
      FROM core.hierarchy_tree ht join core.hierarchy_design hd on ht.hierarchy_design_id = hd.id 
      WHERE ht.parent_id =_id and ht.is_void = false
    ));
END;
$function$
;
