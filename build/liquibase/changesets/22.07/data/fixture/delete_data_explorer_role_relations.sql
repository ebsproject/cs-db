--liquibase formatted sql

--changeset postgres:delete_data_explorer_role_relations context:fixture splitStatements:false rollbackSplitStatements:false
--comment: Delete BA Data Explorer role relations



do $$
declare temprow record;

begin

FOR temprow IN
    SELECT pf.id
    FROM "security".product_function pf
    inner join core.product p
    on pf.product_id = p.id
    where p.name = 'Data Explorer'
LOOP
    DELETE FROM "security".role_product_function WHERE product_function_id = temprow.id;
END LOOP;

end $$;
