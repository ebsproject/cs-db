--liquibase formatted sql

--changeset postgres:void_data_explorer_actions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: Void BA Data Explorer actions



do $$
declare temprow record;

begin

FOR temprow IN
    SELECT pf.id
    FROM "security".product_function pf
    inner join core.product p
    on pf.product_id = p.id
    where p.name = 'Data Explorer'
LOOP
    UPDATE "security".product_function SET is_void = true WHERE id = temprow.id;
END LOOP;

end $$;
