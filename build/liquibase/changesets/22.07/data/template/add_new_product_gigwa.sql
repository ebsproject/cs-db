--liquibase formatted sql

--changeset postgres:add_new_product_gigwa context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1343 Add new product gigwa for SM



INSERT INTO core.product
("name", description, help, main_entity, icon, creator_id, domain_id, htmltag_id, menu_order, "path")
VALUES
('GIGWA', 'Genotype Investigator for Genome-Wide Analyses', 'GIGWA', 'GIGWA', '',  1, 
(select id from core.domain where prefix = 'sm'), 1, 11, 'gigwa');


do $$
declare r_admin int;
declare r_sm_user int;
declare r_sm_admin int;
declare p_gigwa int;
declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO r_admin;
SELECT id FROM "security"."role" where name = 'SM User' INTO r_SM_user;
SELECT id FROM "security"."role" where name = 'SM Admin' INTO r_sm_admin;
SELECT id FROM core.product where name = 'GIGWA' INTO p_gigwa;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
VALUES
    ('Create', true, 'Create', 1, p_gigwa),
    ('Modify', true, 'Modify', 1, p_gigwa),
    ('Delete', true, 'Delete', 1, p_gigwa),
    ('Export', true, 'Export', 1, p_gigwa),
    ('Print', true, 'Print', 1, p_gigwa),
    ('Search', true, 'Search', 1, p_gigwa),
    ('Read', true, 'Read', 1, p_gigwa);


FOR temprow IN
    SELECT id FROM "security".product_function where product_id = p_gigwa
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select r_admin, temprow.id);
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select r_sm_admin, temprow.id);
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select r_sm_user, temprow.id);
END LOOP;

end $$