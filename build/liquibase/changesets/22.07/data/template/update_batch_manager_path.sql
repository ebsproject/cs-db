--liquibase formatted sql

--changeset postgres:update_batch_manager_path context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1324 Update Batch Manager path



UPDATE core.product
SET "path"='batchmanager/view'
WHERE "name"='Batch Manager';
