--liquibase formatted sql

--changeset postgres:void_data_explorer_product context:template splitStatements:false rollbackSplitStatements:false
--comment:  Void Data Explorer Product


UPDATE core.product
SET is_void = true
WHERE name='Data Explorer';
