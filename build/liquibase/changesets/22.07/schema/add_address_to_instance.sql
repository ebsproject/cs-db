--liquibase formatted sql

--changeset postgres:add_address_to_instance context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS2-314 Allow an address to be assigned to an instance



ALTER TABLE core.instance 
 ADD COLUMN address_id integer NULL;

 ALTER TABLE core.instance ADD CONSTRAINT "FK_instance_address"
	FOREIGN KEY (address_id) REFERENCES crm.address (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN core.instance.address_id
	IS 'Reference to the address'
;
