--liquibase formatted sql

--changeset postgres:create_tenant_relations context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1282 Update model to support Shared -Tenant Concept



CREATE TABLE core.tenant_contact
(
	contact_id integer NOT NULL,
	tenant_id integer NOT NULL
)
;

CREATE TABLE core.tenant_hierarchy
(
	hierarchy_id integer NOT NULL,
	tenant_id integer NOT NULL
)
;

CREATE TABLE core.tenant_printout_template
(
	printout_template_id integer NOT NULL,
	tenant_id integer NOT NULL
)
;


ALTER TABLE core.tenant_contact ADD CONSTRAINT "PK_tenant_contact"
	PRIMARY KEY (contact_id,tenant_id)
;

ALTER TABLE core.tenant_hierarchy ADD CONSTRAINT "PK_tenant_hierarchy"
	PRIMARY KEY (hierarchy_id,tenant_id)
;

ALTER TABLE core.tenant_printout_template ADD CONSTRAINT "PK_tenant_printout_template"
	PRIMARY KEY (printout_template_id,tenant_id)
;

ALTER TABLE core.tenant_contact ADD CONSTRAINT "FK_tenant_contact_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.tenant_contact ADD CONSTRAINT "FK_tenant_contact_tenant"
	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.tenant_hierarchy ADD CONSTRAINT "FK_tenant_hierarchy_hierarchy"
	FOREIGN KEY (hierarchy_id) REFERENCES core.hierarchy (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.tenant_hierarchy ADD CONSTRAINT "FK_tenant_hierarchy_tenant"
	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.tenant_printout_template ADD CONSTRAINT "FK_tenant_printout_template_printout_template"
	FOREIGN KEY (printout_template_id) REFERENCES core.printout_template (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.tenant_printout_template ADD CONSTRAINT "FK_tenant_printout_template_tenant"
	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action
;
