--liquibase formatted sql

--changeset postgres:put_fixture_data_back context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1341 Put fixture data back


INSERT INTO core.instance ("server", port, creator_id, modifier_id, id, tenant_id, "name", crop_id, address_id)
SELECT "server", port, creator_id, modifier_id, id, tenant_id, "name", crop_id, address_id 
FROM core.instance_temp;

INSERT INTO core.domain_instance
(id, instance_id, domain_id, context, creation_timestamp, creator_id, is_void, modification_timestamp, modifier_id, tenant_id, sg_context, is_mfe)
SELECT id, instance_id, domain_id, context, creation_timestamp, creator_id, is_void, modification_timestamp, modifier_id, tenant_id, sg_context, is_mfe
FROM core.domain_instance_temp;


DROP TABLE crm.hierarchy_temp;
DROP TABLE core.instance_temp;
DROP TABLE core.domain_instance_temp;


SELECT setval('core.domain_instance_id_seq', (SELECT MAX(id) FROM core.domain_instance));
SELECT setval('core.instance_id_seq', (SELECT MAX(id) FROM core.instance));