--liquibase formatted sql

--changeset postgres:remove_data_from_fixture_version context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1341 Remove CS fixture data



-----------------------------------------------------------
CREATE TABLE core.domain_instance_temp (
	instance_id int4 NOT NULL,
	domain_id int4 NOT NULL,
	context varchar(500) NOT NULL,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	creator_id int4 NOT NULL,
	id serial NOT NULL,
	is_void bool NOT NULL DEFAULT false,
	modification_timestamp timestamp NULL,
	modifier_id int4 NULL,
	tenant_id int4 NOT NULL,
	sg_context varchar(1000) NULL,
	is_mfe bool NOT NULL DEFAULT true
);

INSERT INTO core.domain_instance_temp
(id, instance_id, domain_id, context, creation_timestamp, creator_id, is_void, modification_timestamp, modifier_id, tenant_id, sg_context, is_mfe)
SELECT id, instance_id, domain_id, context, creation_timestamp, creator_id, is_void, modification_timestamp, modifier_id, tenant_id, sg_context, is_mfe
FROM core.domain_instance 
WHERE instance_id <> 1;

DELETE FROM core.domain_instance;

-------------------------------------------------------
CREATE TABLE core.instance_temp (
	"server" varchar(300) NULL,
	port int4 NOT NULL,
	creator_id int4 NOT NULL,
	modifier_id int4 NULL,
	id serial NOT NULL,
	tenant_id int4 NOT NULL,
	"name" varchar(100) NOT NULL,
	crop_id int4 NULL,
	address_id int4 NULL
);

INSERT INTO core.instance_temp ("server", port, creator_id, modifier_id, id, tenant_id, "name", crop_id, address_id)
SELECT "server", port, creator_id, modifier_id, id, tenant_id, "name", crop_id, address_id 
FROM core.instance
WHERE "name" <> 'Default';

DELETE FROM core."instance";
------------------------------------------------------------


--Remove role related data

DELETE FROM "security".role_product_function;

DELETE FROM  "security".user_role 
WHERE role_id = 12 and user_id = 1;

DELETE FROM "security".product_function;

DELETE FROM "security".tenant_user
WHERE user_id = 1 and tenant_id = 1;

DELETE FROM "security".user_role
WHERE user_id = 1;

DELETE FROM "security"."user"
WHERE user_name = 'admin@ebsproject.org';

DELETE FROM crm.person
WHERE gender = 'System';

DELETE FROM  "security".role 
WHERE id > 16;

DELETE FROM crm.contact_contact_type
WHERE contact_type_id = 10 and contact_id = 0;
DELETE FROM crm.contact_contact_type
WHERE contact_type_id = 9 and contact_id = 0;



do $$
declare temprow record;

begin
FOR temprow IN SELECT id FROM crm.contact WHERE purpose_id IN (9)
    loop    
        DELETE FROM crm.contact_address WHERE contact_id = temprow.id;
    end loop;
end $$;

DELETE FROM crm.contact_address WHERE contact_id = (SELECT id FROM crm.contact WHERE EMAIL= 'cimmytindia@cimmyt.org');
DELETE FROM crm.contact_address WHERE contact_id = (SELECT id FROM crm.contact WHERE EMAIL= 'cimmytkenya@cimmyt.org');



DELETE FROM crm.person_status
WHERE "name" = 'Active';

DELETE FROM "program".crop
 WHERE code = 'Default';


DELETE FROM crm.contact_contact_type WHERE contact_type_id = (SELECT id FROM crm.contact_type WHERE name = 'Customer' and category_id = 1);


CREATE TABLE crm.hierarchy_temp (
	contact_id int4 NOT NULL,
	parent_id int4 NOT NULL
);

INSERT INTO crm.hierarchy_temp (contact_id, parent_id)
SELECT contact_id, parent_id from crm."hierarchy";

DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'gqnsl@irri.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'shu@irri.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'gsl@irri.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'sdu@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'wql@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'mzq@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'shlw@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'shlm@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'lam@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'wmbl@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'afm@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'asm@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'cimmytindia@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE contact_id = (select id from crm.contact where email = 'cimmytkenya@cimmyt.org');

DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'cimmytindia@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'cimmytkenya@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'gqnsl@irri.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'shu@irri.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'gsl@irri.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'sdu@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'wql@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'mzq@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'shlw@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'shlm@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'lam@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'wmbl@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'afm@cimmyt.org');
DELETE FROM crm."hierarchy" WHERE parent_id = (select id from crm.contact where email = 'asm@cimmyt.org');


DELETE FROM crm.contact_contact_type WHERE contact_type_id = (SELECT id FROM crm.contact_type where name = 'Service Provider');

DELETE FROM crm.institution WHERE common_name = 'CIMMYT India';
DELETE FROM crm.institution WHERE common_name = 'CIMMYT Kenya';
DELETE FROM crm.institution WHERE common_name = 'WMBL';
DELETE FROM crm.institution WHERE common_name = 'ASM';
DELETE FROM crm.institution WHERE common_name = 'AFM';
DELETE FROM crm.institution WHERE common_name = 'LAM';
DELETE FROM crm.institution WHERE common_name = 'SHLM';
DELETE FROM crm.institution WHERE common_name = 'SHLW';
DELETE FROM crm.institution WHERE common_name = 'MZQ';
DELETE FROM crm.institution WHERE common_name = 'WQL';
DELETE FROM crm.institution WHERE common_name = 'SDU';
DELETE FROM crm.institution WHERE common_name = 'GSL';
DELETE FROM crm.institution WHERE common_name = 'SHU';
DELETE FROM crm.institution WHERE common_name = 'GQNSL';

DELETE FROM crm.contact WHERE email = 'cimmytindia@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'cimmytkenya@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'wmbl@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'asm@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'afm@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'lam@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'shlm@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'shlw@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'mzq@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'wql@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'sdu@cimmyt.org';
DELETE FROM crm.contact WHERE email = 'gsl@irri.org';
DELETE FROM crm.contact WHERE email = 'shu@irri.org';
DELETE FROM crm.contact WHERE email = 'gqnsl@irri.org';

DELETE FROM crm.contact_address;
DELETE FROM crm.address WHERE id IN (2,3);


SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));
--SELECT setval('program.crop_id_seq', (SELECT MAX(id) FROM program.crop));
SELECT setval('"security".role_id_seq', (SELECT MAX(id) FROM "security"."role"));
--SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));
SELECT setval('crm.person_status_id_seq', (SELECT MAX(id) FROM crm.person_status));
SELECT setval('core.instance_id_seq', (SELECT MAX(id) FROM core.instance));
SELECT setval('core.domain_instance_id_seq', (SELECT MAX(id) FROM core.domain_instance));
SELECT setval('crm.address_id_seq', (SELECT MAX(id) FROM crm.address));
SELECT setval('"program".crop_program_id_seq', (SELECT MAX(id) FROM "program".crop_program));
SELECT setval('crm.contact_id_seq', (SELECT MAX(id) FROM crm.contact));
SELECT setval('crm.institution_id_seq', (SELECT MAX(id) FROM crm.institution));
