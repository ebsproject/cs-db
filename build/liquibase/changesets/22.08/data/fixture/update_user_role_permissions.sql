--liquibase formatted sql

--changeset postgres:update_user_role_permissions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1370 Update permissions for user role in csdb



do $$
declare temprow record;
declare _user_role_id int;

begin
SELECT id FROM "security"."role" WHERE name = 'User' INTO _user_role_id;

DELETE FROM "security".role_product_function WHERE role_id = _user_role_id;

FOR temprow IN SELECT id, "action" FROM "security".product_function WHERE "action" = 'Read'
    loop    
        INSERT INTO "security".role_product_function (role_id, product_function_id)
        VALUES(_user_role_id, temprow.id);
    end loop;
end $$;