--liquibase formatted sql

--changeset postgres:update_roles_and_product_function context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1341 Update CS template data


UPDATE "security".user_role SET role_id = 17 WHERE role_id = 1;
UPDATE "security".user_role SET role_id = 18 WHERE role_id = 2;
UPDATE "security".user_role SET role_id = 19 WHERE role_id = 3;
UPDATE "security".user_role SET role_id = 20 WHERE role_id = 4;
UPDATE "security".user_role SET role_id = 21 WHERE role_id = 5;
UPDATE "security".user_role SET role_id = 22 WHERE role_id = 6;
UPDATE "security".user_role SET role_id = 23 WHERE role_id = 7;
UPDATE "security".user_role SET role_id = 24 WHERE role_id = 8;
UPDATE "security".user_role SET role_id = 25 WHERE role_id = 9;
UPDATE "security".user_role SET role_id = 26 WHERE role_id = 10;
UPDATE "security".user_role SET role_id = 27 WHERE role_id = 11;
UPDATE "security".user_role SET role_id = 28 WHERE role_id = 12;
UPDATE "security".user_role SET role_id = 29 WHERE role_id = 13;
UPDATE "security".user_role SET role_id = 30 WHERE role_id = 14;
UPDATE "security".user_role SET role_id = 31 WHERE role_id = 15;
UPDATE "security".user_role SET role_id = 32 WHERE role_id = 16;

UPDATE "security".user SET default_role_id = 17 WHERE default_role_id = 1;
UPDATE "security".user SET default_role_id = 18 WHERE default_role_id = 2;
UPDATE "security".user SET default_role_id = 19 WHERE default_role_id = 3;
UPDATE "security".user SET default_role_id = 20 WHERE default_role_id = 4;
UPDATE "security".user SET default_role_id = 21 WHERE default_role_id = 5;
UPDATE "security".user SET default_role_id = 22 WHERE default_role_id = 6;
UPDATE "security".user SET default_role_id = 23 WHERE default_role_id = 7;
UPDATE "security".user SET default_role_id = 24 WHERE default_role_id = 8;
UPDATE "security".user SET default_role_id = 25 WHERE default_role_id = 9;
UPDATE "security".user SET default_role_id = 26 WHERE default_role_id = 10;
UPDATE "security".user SET default_role_id = 27 WHERE default_role_id = 11;
UPDATE "security".user SET default_role_id = 28 WHERE default_role_id = 12;
UPDATE "security".user SET default_role_id = 29 WHERE default_role_id = 13;
UPDATE "security".user SET default_role_id = 30 WHERE default_role_id = 14;
UPDATE "security".user SET default_role_id = 31 WHERE default_role_id = 15;
UPDATE "security".user SET default_role_id = 32 WHERE default_role_id = 16;

DELETE FROM "security"."role" WHERE id <=16;

INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('default', 'default', 1, false, 1, 'Default');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('admin', 'EBS Administrators', 1, false, 2, 'Admin');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('guest', 'Other', 1, false, 3, 'Guest');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('user', 'EBS User', 1, false, 4, 'User');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Core System Administrator', 'EBS Administrators', 1, false, 5, 'CS Admin');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Breeding Analytics Administrator', 'EBS Administrators', 1, false, 6, 'BA Admin');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Service Management Administrator', 'EBS Administrators', 1, false, 7, 'SM Admin');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Core System User', 'EBS User', 1, false, 8, 'CS User');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Breeding Analytics User', 'EBS User', 1, false, 9, 'BA User');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Service Management User', 'EBS User', 1, false, 10, 'SM User');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Core Breeding User', 'EBS User', 1, false, 11, 'CB User');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Core Breeding Admin', 'EBS Administrators', 1, false, 12, 'CB Admin');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Breeder', 'SM Users', 1, false, 13, 'Breeder');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Molecular Breeder', 'SM Users', 1, false, 14, 'Molecular Breeder');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Lab Manager', 'SM Users', 1, false, 15, 'Lab Manager');
INSERT INTO "security"."role" (description, security_group, creator_id, is_void, id, "name")
VALUES('Data Manager', 'SM Users', 1, false, 16, 'Data Manager');
 


UPDATE "security".user_role SET role_id = 1 WHERE role_id = 17;
UPDATE "security".user_role SET role_id = 2 WHERE role_id = 18;
UPDATE "security".user_role SET role_id = 3 WHERE role_id = 19;
UPDATE "security".user_role SET role_id = 4 WHERE role_id = 20;
UPDATE "security".user_role SET role_id = 5 WHERE role_id = 21;
UPDATE "security".user_role SET role_id = 6 WHERE role_id = 22;
UPDATE "security".user_role SET role_id = 7 WHERE role_id = 23;
UPDATE "security".user_role SET role_id = 8 WHERE role_id = 24;
UPDATE "security".user_role SET role_id = 9 WHERE role_id = 25;
UPDATE "security".user_role SET role_id = 10 WHERE role_id = 26;
UPDATE "security".user_role SET role_id = 11 WHERE role_id = 27;
UPDATE "security".user_role SET role_id = 12 WHERE role_id = 28;
UPDATE "security".user_role SET role_id = 13 WHERE role_id = 29;
UPDATE "security".user_role SET role_id = 14 WHERE role_id = 30;
UPDATE "security".user_role SET role_id = 15 WHERE role_id = 31;
UPDATE "security".user_role SET role_id = 16 WHERE role_id = 32;


UPDATE "security".user SET default_role_id = 1 WHERE default_role_id = 17;
UPDATE "security".user SET default_role_id = 2 WHERE default_role_id = 18;
UPDATE "security".user SET default_role_id = 3 WHERE default_role_id = 19;
UPDATE "security".user SET default_role_id = 4 WHERE default_role_id = 20;
UPDATE "security".user SET default_role_id = 5 WHERE default_role_id = 21;
UPDATE "security".user SET default_role_id = 6 WHERE default_role_id = 22;
UPDATE "security".user SET default_role_id = 7 WHERE default_role_id = 23;
UPDATE "security".user SET default_role_id = 8 WHERE default_role_id = 24;
UPDATE "security".user SET default_role_id = 9 WHERE default_role_id = 25;
UPDATE "security".user SET default_role_id = 10 WHERE default_role_id = 26;
UPDATE "security".user SET default_role_id = 11 WHERE default_role_id = 27;
UPDATE "security".user SET default_role_id = 12 WHERE default_role_id = 28;
UPDATE "security".user SET default_role_id = 13 WHERE default_role_id = 29;
UPDATE "security".user SET default_role_id = 14 WHERE default_role_id = 30;
UPDATE "security".user SET default_role_id = 15 WHERE default_role_id = 31;
UPDATE "security".user SET default_role_id = 16 WHERE default_role_id = 32;

DELETE FROM "security"."role" WHERE id >=17;

DELETE FROM "security".product_function WHERE description = 'Create Request';
DELETE FROM "security".product_function WHERE description = 'List Request';
DELETE FROM "security".product_function WHERE description = 'Download Request';


INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 1, 9, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 2, 9, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 3, 9, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 4, 9, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 5, 9, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 6, 9, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 11, 10, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 12, 10, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 13, 10, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 19, 11, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 20, 11, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 21, 11, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 26, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 27, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 28, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 33, 12, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 34, 12, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 35, 12, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 40, 14, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 41, 14, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 42, 14, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 47, 20, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 48, 20, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 49, 20, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 75, 19, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 76, 19, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 77, 19, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 82, 16, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 83, 16, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 84, 16, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 90, 25, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 91, 25, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 92, 25, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 97, 1, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 98, 1, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 99, 1, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 103, 2, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 104, 2, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 105, 2, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 110, 4, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 111, 4, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 112, 4, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 117, 5, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 118, 5, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 119, 5, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 124, 3, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 125, 3, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 126, 3, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 131, 6, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 132, 6, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 133, 6, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 138, 7, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 139, 7, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 140, 7, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 145, 8, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 146, 8, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 147, 8, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Copy', true, 'Copy', NULL, 1, NULL, false, 149, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Download', true, 'Download', NULL, 1, NULL, false, 151, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Download_Template_People', true, 'Download_Template_People', NULL, 1, NULL, false, 164, 11, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Import_Template_People', true, 'Import_Template_People', NULL, 1, NULL, false, 165, 11, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Download_Template_Institution', true, 'Download_Template_Institution', NULL, 1, NULL, false, 167, 11, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Import_Template_Institution', true, 'Import_Template_Institution', NULL, 1, NULL, false, 168, 11, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Assign_New_Contact', true, 'Assign_New_Contact', NULL, 1, NULL, false, 169, 11, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Assign_Contact', true, 'Assign_Contact', NULL, 1, NULL, false, 170, 11, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Upload_File_Backend', true, 'Upload_File_Backend', NULL, 1, NULL, false, 154, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Download_File_Backend', true, 'Download_File_Backend', NULL, 1, NULL, false, 155, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Import_Backend', true, 'Import_Backend', NULL, 1, NULL, false, 157, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export_Backend', true, 'Export_Backend', NULL, 1, NULL, false, 158, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Download_Setting_Backend', true, 'Download_Setting_Backend', NULL, 1, NULL, false, 159, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Upload_Setting_Backend', true, 'Upload_Setting_Backend', NULL, 1, NULL, false, 160, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Copy_Url_Backend', true, 'Copy_Url_Backend', NULL, 1, NULL, false, 161, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Refresh_Backend', true, 'Refresh_Backend', NULL, 1, NULL, false, 162, 21, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 7, 9, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 8, 10, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 9, 10, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 10, 10, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 14, 10, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 15, 11, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 16, 11, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 18, 11, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 22, 11, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 23, 21, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 24, 21, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 25, 21, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 29, 21, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 30, 12, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 31, 12, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 32, 12, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 36, 12, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 37, 14, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 38, 14, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 39, 14, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 43, 14, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 44, 20, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 45, 20, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 46, 20, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 50, 20, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, true, 177, 27, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, true, 178, 27, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, true, 179, 27, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, true, 180, 27, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, true, 181, 27, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, true, 182, 27, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, true, 183, 27, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 72, 19, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 73, 19, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 74, 19, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 78, 19, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 79, 16, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 80, 16, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 81, 16, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 85, 16, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 86, 1, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 87, 25, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 88, 25, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 89, 25, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 93, 25, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 94, 1, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 95, 1, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 96, 1, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 100, 2, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 101, 2, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 102, 2, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 106, 2, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 107, 4, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 108, 4, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 109, 4, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 113, 4, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 114, 5, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 115, 5, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 116, 5, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 120, 5, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 121, 3, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 122, 3, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 123, 3, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 127, 3, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 128, 6, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 129, 6, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 130, 6, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 134, 6, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 135, 7, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 136, 7, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 137, 7, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 141, 7, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 142, 8, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 143, 8, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 144, 8, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 148, 8, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Design', true, 'Design', NULL, 1, NULL, false, 150, 21, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('New_Person', true, 'New_Person', NULL, 1, NULL, false, 163, 11, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('New_Institution', true, 'New_Institution', NULL, 1, NULL, false, 166, 11, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create_Data_Source', true, 'Create_Data_Source', NULL, 1, NULL, false, 152, 21, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete_Report_Backend', true, 'Delete_Report_Backend', NULL, 1, NULL, false, 153, 21, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete_File_Backend', true, 'Delete_File_Backend', NULL, 1, NULL, false, 156, 21, true);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Single_Design', true, 'Single_Design', NULL, 1, NULL, false, 171, 12, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Multi_Design', true, 'Multi_Design', NULL, 1, NULL, false, 172, 12, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('View', true, 'View', NULL, 1, NULL, false, 173, 20, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Accept', true, 'Accept', NULL, 1, NULL, false, 174, 20, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Reject', true, 'Reject', NULL, 1, NULL, false, 175, 20, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('View', true, 'View', NULL, 1, NULL, false, 176, 14, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Create', true, 'Create', NULL, 1, NULL, false, 184, 29, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Modify', true, 'Modify', NULL, 1, NULL, false, 185, 29, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Delete', true, 'Delete', NULL, 1, NULL, false, 186, 29, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Export', true, 'Export', NULL, 1, NULL, false, 187, 29, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Print', true, 'Print', NULL, 1, NULL, false, 188, 29, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Search', true, 'Search', NULL, 1, NULL, false, 189, 29, false);
INSERT INTO "security".product_function
(description, system_type, "action", modification_timestamp, creator_id, modifier_id, is_void, id, product_id, is_data_action)
VALUES('Read', true, 'Read', NULL, 1, NULL, false, 190, 29, false);


INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 1); 
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 2);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 3);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 4);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 5);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 6);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 7);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 8);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 9);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 10);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 11);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 12);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 13);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 14);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 15);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 16);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 18);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 19);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 20);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 21);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 22);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 23);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 24);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 25);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 26);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 27);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 28);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 29);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 30);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 31);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 32);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 33);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 34);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 35);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 36);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 37);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 38);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 39);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 40);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 41);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 42);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 43);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 44);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 45);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 46);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 47);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 48);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 49);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 50);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 72);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 73);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 74);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 75);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 76);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 77);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 78);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 79);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 80);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 81);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 82);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 83);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 84);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 85);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 86);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 1);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 2);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 3);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 4);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 5);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 6);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 7);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 8);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 9);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 10);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 11);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 12);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 13);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 14);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 15);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 16);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 18);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 19);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 20);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 21);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 22);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 23);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 24);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 25);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 26);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 27);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 28);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 29);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(6, 30);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(6, 31);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(6, 32);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(6, 33);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(6, 34);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(6, 35);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(6, 36);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 37);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 38);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 39);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 40);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 41);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 42);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 43);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 44);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 45);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 46);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 47);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 48);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 49);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 50);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 72);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 73);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 74);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 75);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 76);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 77);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 78);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 79);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 80);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 81);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 82);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 83);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 84);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 85);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 4);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 11);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 19);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 26);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 7);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 14);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 22);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 29);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(9, 33);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(9, 36);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(10, 40);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(10, 47);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(10, 75);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(10, 82);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(10, 43);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(10, 50);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(10, 78);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(10, 85);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 86);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 30);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 31);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 32);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 33);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 34);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 35);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 36);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 37);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 38);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 39);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 40);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 41);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 42);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 43);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 44);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 45);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 46);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 47);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 48);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 49);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 50);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 72);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 73);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 74);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 75);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 76);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 77);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 78);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 79);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 80);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 81);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 82);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 83);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 84);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 85);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 86);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 72);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 73);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 74);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 75);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 76);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 77);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 78);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 30);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 31);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 32);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 33);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 34);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 35);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(11, 36);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 87);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 88);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 89);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 90);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 91);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 92);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 93);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 87);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 88);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 89);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 90);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 91);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 92);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 93);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 87);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 88);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 89);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 90);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 91);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 92);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(4, 93);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 90);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(8, 93);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 86);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 94);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 95);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 96);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 97);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 98);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 99);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 100);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 101);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 102);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 103);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 104);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 105);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 106);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 107);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 108);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 109);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 110);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 111);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 112);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 113);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 114);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 115);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 116);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 117);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 118);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 119);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 120);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 121);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 122);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 123);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 124);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 125);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 126);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 127);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 128);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 129);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 130);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 131);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 132);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 133);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(12, 134);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 149);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 150);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 151);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 152);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 153);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 154);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 155);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 156);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 157);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 158);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 159);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 160);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 161);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 162);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 149);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 150);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 151);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 152);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 153);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 154);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 155);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 156);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 157);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 158);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 159);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 160);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 161);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 162);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 163);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 164);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 165);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 166);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 167);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 168);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 169); 
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 170);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 163);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 164);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 165);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 166);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 167);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 168);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 169);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(5, 170);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(9, 171);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(6, 171);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(6, 172);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(13, 40);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 40);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 40);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(13, 41);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 41);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 41);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(13, 42);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 42);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 42);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(13, 37);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 37);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 37);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(13, 38);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 38);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 38);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(13, 39);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 39);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 39);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(13, 43);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 43);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 43);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(13, 176);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 176);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 176);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 47);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 47);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 48);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 48);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 49);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 49);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 44);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 44);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 45);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 45);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 46);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 46);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 50);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 50);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 173);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 173);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 174);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 174);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 175);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 175);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 171);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 172);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 184);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 184);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 184);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 184);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 184);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 185);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 185);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 185);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 185);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 185);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 186);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 186);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 186);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 186);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 186);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 187);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 187);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 187);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 187);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 187);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 188);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 188);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 188);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 188);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 188);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 189);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 189);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 189);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 189);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 189);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(14, 190);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(15, 190);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(16, 190);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(2, 190);
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES(7, 190);



SELECT setval('"security".role_id_seq', (SELECT MAX(id) FROM "security"."role"));
SELECT setval('"security".product_function_id_seq', (SELECT MAX(id) FROM "security".product_function));
