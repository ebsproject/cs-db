--liquibase formatted sql

--changeset postgres:update_template_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1341 Update CS template data



INSERT INTO "program".crop
(code, "name", description, notes, creator_id, is_void, id)
VALUES('Default', 'Default', 'Default', 'Default Crop', 1, false, 0);

INSERT INTO "program".crop_program
(id, code, "name", description, notes, tenant_id, creator_id, crop_id, is_void, organization_id)
VALUES(0, 'Default', 'Default', 'Default', NULL, 1, 1, 0, false, 1);


INSERT INTO core."instance"
("server", port, creator_id, is_void, id, tenant_id, "name", notes, crop_id)
VALUES('172.18.8.1', 22,  1, false, 1, 1, 'Default', 'Default instace', 0);


INSERT INTO core.domain_instance
(instance_id, domain_id, context, creator_id, id, is_void, tenant_id, sg_context, is_mfe)
VALUES(1, 3, 'https://dev.ebsproject.org', 1, 6, false, 1, 'https://baapi-dev.ebsproject.org/v1/', true);
INSERT INTO core.domain_instance
(instance_id, domain_id, context, creator_id, id, is_void, tenant_id, sg_context, is_mfe)
VALUES(1, 4, 'https://dev.ebsproject.org', 1, 4, false, 1, 'https://smapi-dev.ebsproject.org/', true);
INSERT INTO core.domain_instance
(instance_id, domain_id, context, creator_id, id, is_void, tenant_id, sg_context, is_mfe)
VALUES(1, 1, 'https://cb-dev.ebsproject.org', 1, 1, false, 1, 'https://cbapi-dev.ebsproject.org/v3/', false);
INSERT INTO core.domain_instance
(instance_id, domain_id, context, creator_id, id, is_void, tenant_id, sg_context, is_mfe)
VALUES(1, 2, 'https://dev.ebsproject.org', 1, 5, false, 1, 'https://csapi-dev.ebsproject.org/', true);


INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('default', 'default', 1, false, 17, 'Default');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('admin', 'EBS Administrators', 1, false, 18, 'Admin');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('guest', 'Other', 1, false, 19, 'Guest');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('user', 'EBS User', 1, false, 20, 'User');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Core System Administrator', 'EBS Administrators', 1, false, 21, 'CS Admin');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Breeding Analytics Administrator', 'EBS Administrators', 1, false, 22, 'BA Admin');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Service Management Administrator', 'EBS Administrators', 1, false, 23, 'SM Admin');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Core System User', 'EBS User', 1, false, 24, 'CS User');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Breeding Analytics User', 'EBS User', 1, false, 25, 'BA User');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Service Management User', 'EBS User', 1, false, 26, 'SM User');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Core Breeding User', 'EBS User', 1, false, 27, 'CB User');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Core Breeding Admin', 'EBS Administrators', 1, false, 28, 'CB Admin');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Breeder', 'SM Users', 1, false, 29, 'Breeder');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Molecular Breeder', 'SM Users', 1, false, 30, 'Molecular Breeder');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Lab Manager', 'SM Users', 1, false, 31, 'Lab Manager');
INSERT INTO "security"."role"
(description, security_group, creator_id, is_void, id, "name")
VALUES('Data Manager', 'SM Users', 1, false, 32, 'Data Manager');


INSERT INTO "security"."user"
(user_name, last_access, default_role_id, is_is, creator_id, is_void, id, external_id, contact_id, is_active)
VALUES('admin@ebsproject.org', NULL, 28, 1, 1, false, 1, NULL, 0, true);

INSERT INTO "security".tenant_user
(user_id, tenant_id)
VALUES(1, 1);

INSERT INTO "security".user_role
(role_id, user_id)
VALUES(28, 1);


INSERT INTO crm.person
(family_name, given_name, additional_name, gender, job_title, knows_about, creator_id, is_void, id, language_id, contact_id)
VALUES('System', 'Account', NULL, 'System', 'System Account', 'IT', 1, false, 0, 1, 0);

INSERT INTO crm.person_status
("name", description, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Active', 'Active person', '2022-07-05 12:34:31.746', NULL, 1, NULL, false, 1);


INSERT INTO crm.contact_contact_type
(contact_type_id, contact_id)
VALUES
(10, 0),
(9,0);


UPDATE crm.country
SET "name"='Curacao' WHERE id=244;

INSERT INTO crm.address
("location", region, zip_code, street_address, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, country_id, purpose_id)
VALUES('Mexico', 'R', '9999', 'Mexico-Veracruz, El Batan Km. 45, 56237 Mex.', '2022-07-05 12:34:32.345', NULL, 1, NULL, false, 2, 137, 7);
INSERT INTO crm.address
("location", region, zip_code, street_address, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, country_id, purpose_id)
VALUES('Philippines', 'R', '9999', 'Pili Drive, University of the Philippines Los Baños, Los Baños, 4030 Laguna, Filipinas', '2022-07-05 12:34:32.345', NULL, 1, NULL, false, 3, 167, 7);


SELECT setval('"security".user_id_seq', (SELECT MAX(id) FROM "security"."user"));
--SELECT setval('program.crop_id_seq', (SELECT MAX(id) FROM program.crop));
SELECT setval('"security".role_id_seq', (SELECT MAX(id) FROM "security"."role"));
--SELECT setval('crm.person_id_seq', (SELECT MAX(id) FROM crm.person));
SELECT setval('crm.person_status_id_seq', (SELECT MAX(id) FROM crm.person_status));
SELECT setval('core.instance_id_seq', (SELECT MAX(id) FROM core.instance));
SELECT setval('core.domain_instance_id_seq', (SELECT MAX(id) FROM core.domain_instance));
SELECT setval('crm.address_id_seq', (SELECT MAX(id) FROM crm.address));
--SELECT setval('"program".crop_program_id_seq', (SELECT MAX(id) FROM "program".crop_program));
