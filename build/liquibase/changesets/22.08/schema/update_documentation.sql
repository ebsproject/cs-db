--liquibase formatted sql

--changeset postgres:update_documentation context:schema splitStatements:false rollbackSplitStatements:false
--comment: Update table documentation of CSDB



COMMENT ON TABLE core.domain_instance IS 'Stores the relation of the instance to the domain it belongs to';
COMMENT ON TABLE core.filter IS	'Contains the list of available filters in the system that help users to visualize specific information according to their needs.';
COMMENT ON TABLE core.filter_domain	IS 'Relates a filter to the domain it belongs to';
COMMENT ON TABLE core.tenant_contact IS 'Ralates a contact to the Tenant it belongs to';
COMMENT ON TABLE core.tenant_hierarchy IS 'Ralates a hierarchy to the Tenant it belongs to';
COMMENT ON TABLE core.tenant_printout_template IS 'Stores the relation of a printot template to the tenant it belongs to';
COMMENT ON TABLE crm.category IS 'Stores the categories that can be assigned to a contact they can be:  Person, Institution, Internal Unit.';
COMMENT ON TABLE crm.country IS	'Stores a list of all countries in the world';
COMMENT ON TABLE crm.contact_address IS 'Relation between contact and address that allow a contact to have more than one address';
COMMENT ON TABLE program.project IS	'Collaborative enterprises that are planned and designed to achieve a set of goals';
COMMENT ON TABLE program.season	IS 'Season or cycle at which the experiment is being evaluated, which depends on the location';
COMMENT ON TABLE core.hierarchy_tree IS 'Contains the values of the entities defined in the hierarchy design';
COMMENT ON TABLE core.hierarchy IS 'List of available hierarchies that can be used in the system e.g Organization Chart, Place Manager, etc.';
COMMENT ON TABLE core.hierarchy_design IS 'Contains the levels of the hierarchies created';
COMMENT ON TABLE core.hierarchy_type IS 'Stores the different categories that a hierarchy can adopt, e.g. chart, location, etc.';
COMMENT ON TABLE core.printout_template IS 'Stores the metadata of templates that can be labels or reports';

COMMENT ON COLUMN core.filter.description IS 'General description of the filter';
COMMENT ON COLUMN core.filter.is_graph_service IS 'Url of the API related to that filter';
COMMENT ON COLUMN core.filter.api_url IS 'Url of the API related to that filter';
COMMENT ON COLUMN core.filter.param_path IS 'Contains the parameters of the path when they are required';
COMMENT ON COLUMN core.filter.body IS 'In case of Graph Service, this column contains the query to be executed in the server';
COMMENT ON COLUMN core.filter.param_query IS 'Contains the parameters of a query when they are required';
COMMENT ON COLUMN core.filter.method IS 'Type of method to be used in the API (get, post)';
COMMENT ON COLUMN core.filter.is_system IS 'Determines if the filter exists as part of the system, if not, it would be a filter created by the user';
COMMENT ON COLUMN core.filter.tooltip IS 'Text or description displayed to help users when using the UI';
COMMENT ON COLUMN core.filter.hierarchy_design_id IS 'Id reference to the hierarchy design_table';

COMMENT ON COLUMN core.filter_domain.filter_id IS 'Id reference to the filter table';
COMMENT ON COLUMN core.filter_domain.domain_id IS 'Id reference to the domain table';
COMMENT ON COLUMN core.tenant_contact.contact_id IS 'Id reference to the contact table';
COMMENT ON COLUMN core.tenant_contact.tenant_id IS 'Id reference to the tenant table';
COMMENT ON COLUMN core.tenant_hierarchy.hierarchy_id IS 'Id reference to the hierarchy table';
COMMENT ON COLUMN core.tenant_hierarchy.tenant_id IS 'Id reference to the tenant table';

COMMENT ON COLUMN core.tenant_printout_template.printout_template_id IS 'Id reference to the printout_template table';
COMMENT ON COLUMN core.tenant_printout_template.tenant_id IS 'Id reference to the tenant table';

COMMENT ON COLUMN crm.country.name IS 'Name of the Country';
COMMENT ON COLUMN crm.country.iso IS 'Official abbreviation of the country';

COMMENT ON COLUMN crm.contact_address.address_id IS 'Id reference to the address table';
COMMENT ON COLUMN crm.contact_address.contact_id IS 'Id reference to the contact table';

COMMENT ON COLUMN core.hierarchy_tree.value IS 'Actual value of the level defined e.g. Level:Institution - value:CIMMYT';
COMMENT ON COLUMN core.hierarchy_tree.record_id IS 'Id reference to the record where the level is stored';

COMMENT ON COLUMN core.hierarchy.name IS 'Name of the hierarchy';
COMMENT ON COLUMN core.hierarchy.description IS 'General description of the hierarchy';
COMMENT ON COLUMN core.hierarchy.is_system IS 'If its a system hierarchy, means that it was created as part of the System and not by the user.';

COMMENT ON COLUMN core.hierarchy_design.name IS 'Name of the hierarchy level';
COMMENT ON COLUMN core.hierarchy_design.is_required IS 'Determines if the level is required within the hierarchy that it belongs to';
COMMENT ON COLUMN core.hierarchy_design.filter IS 'Contains a filter in case the design needs it to obtain specific information according to what is requested';
COMMENT ON COLUMN core.hierarchy_design.level IS 'Defines the order of the level within the hierarchy e.g (1-Institution, 2-Program, 3-Unit, 4-Role)';
COMMENT ON COLUMN core.hierarchy_design.is_visible IS 'Determines if the level of the hierarchy is visible in the interface or not';

COMMENT ON COLUMN core.hierarchy_type.name IS 'Name of the hierarchy type (Chart, location, etc)';
COMMENT ON COLUMN core.hierarchy_type.description IS 'General description of the hierarchy type.';
