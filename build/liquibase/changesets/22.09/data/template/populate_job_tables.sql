--liquibase formatted sql

--changeset postgres:populate_job_tables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1425 Populate job tables



INSERT INTO core.job_type
(id, "name", description, tenant_id, creator_id)
VALUES
(1, 'Alert', 'User simple message', 1, 1),
(2, 'Action', 'User Alert to take action', 1, 1),
(3, 'Email', 'The User will receive an alert by email', 1, 1);

INSERT INTO core."translation"
("translation", language_to, creator_id, id, language_id)
VALUES('Mensaje', 27, 1, 1, 1);

INSERT INTO core.job_workflow
(id, job_type_id, product_function_id, translation_id, "name", description, tenant_id, creator_id)
VALUES(1, 2, 37, 1, 'Request Manager', 'Send message after creating a reques in Service Management', 1, 1);

SELECT setval('core.job_type_id_seq', (SELECT MAX(id) FROM core.job_type)); 
SELECT setval('core.translation_id_seq', (SELECT MAX(id) FROM core."translation")); 
SELECT setval('core.job_workflow_id_seq', (SELECT MAX(id) FROM core.job_workflow)); 
