--liquibase formatted sql

--changeset postgres:update_qa_endpoit context:template labels:qa splitStatements:false rollbackSplitStatements:false
--comment: DB-1421 Update endpoint for QA and UAT environments


UPDATE core.domain_instance
SET context='https://qa.ebsproject.org', sg_context='https://baapi-qa.ebsproject.org/v1/'
WHERE id=6;

UPDATE core.domain_instance
SET context='https://qa.ebsproject.org', sg_context='https://smapi-qa.ebsproject.org/'
WHERE id=4;

UPDATE core.domain_instance
SET context='https://cb-qa.ebsproject.org', sg_context='https://cbapi-qa.ebsproject.org/v3/'
WHERE id=1;

UPDATE core.domain_instance
SET context='https://qa.ebsproject.org', sg_context='https://csapi-qa.ebsproject.org/'
WHERE id=5;
