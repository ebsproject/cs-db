--liquibase formatted sql

--changeset postgres:update_uat_endpoit context:template labels:uat splitStatements:false rollbackSplitStatements:false
--comment: DB-1421 Update endpoint for QA and UAT environments



UPDATE core.domain_instance
SET context='https://uat.ebsproject.org', sg_context='https://baapi-uat.ebsproject.org/v1/'
WHERE id=6;

UPDATE core.domain_instance
SET context='https://uat.ebsproject.org', sg_context='https://smapi-uat.ebsproject.org/'
WHERE id=4;

UPDATE core.domain_instance
SET context='https://cb-uat.ebsproject.org', sg_context='https://cbapi-uat.ebsproject.org/v3/'
WHERE id=1;

UPDATE core.domain_instance
SET context='https://uat.ebsproject.org', sg_context='https://csapi-uat.ebsproject.org/'
WHERE id=5;
