--liquibase formatted sql

--changeset postgres:add_audit_columns_file_object context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1427 Add audit columns to file_object table



    ALTER TABLE core.file_object
    ADD COLUMN creation_timestamp timestamp NOT NULL DEFAULT now(),
	ADD COLUMN creator_id int4 NOT NULL,
	ADD COLUMN modification_timestamp timestamp NULL,
	ADD COLUMN modifier_id int4 NULL,
	ADD COLUMN is_void bool NOT NULL DEFAULT false;