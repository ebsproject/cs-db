--liquibase formatted sql

--changeset postgres:add_external_id_to_contact context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1397 Add external_id to contact table



ALTER TABLE crm.contact 
 ADD COLUMN external_id integer NULL;	-- References the id of the original source.


 COMMENT ON COLUMN crm.contact.external_id 
 	IS 'References the id of the original source.';