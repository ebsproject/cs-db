--liquibase formatted sql

--changeset postgres:create_file_object_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1424 Create file object table in CSDB



CREATE TABLE core.file_object (
	id uuid NOT NULL,
	"name" varchar NOT NULL,
	owner_id int4 NOT NULL,
	"size" int8 NOT NULL
);
CREATE UNIQUE INDEX file_object_id_idx ON core.file_object USING btree (id);