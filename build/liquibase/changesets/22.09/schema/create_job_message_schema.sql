--liquibase formatted sql

--changeset postgres:create_job_message_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1396  Create Job - Message Model for CSDB



CREATE TABLE core.job_log
(
	id integer NOT NULL   DEFAULT NEXTVAL(('core."job_log_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	job_workflow_id integer NOT NULL,
	job_track_id uuid NULL,
	translation_id integer NULL,
	message jsonb NOT NULL,
	start_time timestamp without time zone NULL,
	end_time timestamp without time zone NULL,
	status varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE core.job_log_contact
(
	contact_id integer NOT NULL,
	job_log_id integer NOT NULL
)
;

CREATE TABLE core.job_type
(
	id integer NOT NULL   DEFAULT NEXTVAL(('core."job_type_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(250) NULL,	-- Type name of the job e.g. Simple, email, action, etc.
	description varchar(500) NULL,	-- Description of the type
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE core.job_workflow
(
	id integer NOT NULL   DEFAULT NEXTVAL(('core."job_workflow_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	job_type_id integer NOT NULL,
	product_function_id integer NOT NULL,
	translation_id integer NULL,
	name varchar(250) NULL,
	description varchar(500) NULL,
	request_code integer NULL,
	notes varchar(500) NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE SEQUENCE core.job_log_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE core.job_type_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE core.job_workflow_id_seq INCREMENT 1 START 1;

ALTER TABLE core.job_log ADD CONSTRAINT "PK_job_log"
	PRIMARY KEY (id)
;

ALTER TABLE core.job_log_contact ADD CONSTRAINT "PK_job_log_contact"
	PRIMARY KEY (contact_id,job_log_id)
;

ALTER TABLE core.job_type ADD CONSTRAINT "PK_job_type"
	PRIMARY KEY (id)
;

ALTER TABLE core.job_workflow ADD CONSTRAINT "PK_job_workflow"
	PRIMARY KEY (id)
;

ALTER TABLE core.job_log ADD CONSTRAINT "FK_job_log_job_workflow"
	FOREIGN KEY (job_workflow_id) REFERENCES core.job_workflow (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.job_log_contact ADD CONSTRAINT "FK_job_log_contact_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.job_log_contact ADD CONSTRAINT "FK_job_log_contact_job_log"
	FOREIGN KEY (job_log_id) REFERENCES core.job_log (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.job_workflow ADD CONSTRAINT "FK_job_workflow_job_type"
	FOREIGN KEY (job_type_id) REFERENCES core.job_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.job_workflow ADD CONSTRAINT "FK_job_workflow_product_function"
	FOREIGN KEY (product_function_id) REFERENCES security.product_function (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.job_workflow ADD CONSTRAINT "FK_job_workflow_translation"
	FOREIGN KEY (translation_id) REFERENCES core.translation (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.job_log ADD CONSTRAINT "FK_job_log_translation"
	FOREIGN KEY (translation_id) REFERENCES core.translation (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON TABLE core.job_type
	IS 'Contains the different types of a job'
;

COMMENT ON COLUMN core.job_log.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.job_log.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.job_log.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.job_log.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.job_log.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.job_log.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.job_log.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.job_type.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.job_type.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.job_type.description
	IS 'Description of the type'
;

COMMENT ON COLUMN core.job_type.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.job_type.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.job_type.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.job_type.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.job_type.name
	IS 'Type name of the job e.g. Simple, email, action, etc.'
;

COMMENT ON COLUMN core.job_type.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.job_workflow.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.job_workflow.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.job_workflow.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.job_workflow.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.job_workflow.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.job_workflow.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.job_workflow.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;