--liquibase formatted sql

--changeset postgres:remove_not_null_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1396 Remove not null constraint in job_og message



ALTER TABLE core.job_log 
 ALTER COLUMN message DROP NOT NULL;

 