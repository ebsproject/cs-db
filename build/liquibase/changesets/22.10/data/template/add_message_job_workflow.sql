--liquibase formatted sql

--changeset postgres:add_message_job_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-488 Add messaging to job_workflow table



do $$
declare _m integer;
begin

SELECT id FROM core.job_workflow WHERE description = 'Direct Messaging' INTO _m;
IF NOT FOUND THEN
    INSERT INTO core.job_workflow (job_type_id, product_function_id, translation_id, "name", description, request_code, notes, tenant_id, creator_id)
    VALUES(3, 198, 1, 'Request Manager', 'Direct Messaging', NULL, NULL, 1, 1);
END IF;

end $$