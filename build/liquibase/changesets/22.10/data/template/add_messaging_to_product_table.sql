--liquibase formatted sql

--changeset postgres:add_messaging_to_product_table context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-488 Add messaging and Notification to products 



WITH _messaging as (
INSERT INTO core.product
    ("name", description, help, main_entity, icon, creator_id, domain_id, htmltag_id, menu_order, "path")
VALUES
    ('Messaging', 'Messaging', 'Messaging', 'Messaging', 'Messaging', 1, (select id from core.domain where prefix = 'cs'), 1, 0, '/')
RETURNING id
),

_chat_view as (
INSERT INTO "security".product_function 
    (description, system_type, "action", creator_id, product_id)
VALUES 
    ('Chat_View', true, 'Chat_View', 1, (select id from _messaging))
RETURNING id
)

INSERT INTO "security".role_product_function
(role_id, product_function_id)
VALUES(2, (select id from _chat_view));



WITH _notification as (
INSERT INTO core.product
    ("name", description, help, main_entity, icon, creator_id, domain_id, htmltag_id, menu_order, "path")
VALUES
    ('Notification', 'Notification', 'Notification', 'Notification', 'Notification', 1, (select id from core.domain where prefix = 'cs'), 1, 0, '/')
RETURNING id
),

_not_view as (
INSERT INTO "security".product_function 
    (description, system_type, "action", creator_id, product_id)
VALUES 
    ('Notification_View', true, 'Notification_View', 1, (select id from _notification))
RETURNING id 
)

INSERT INTO "security".role_product_function
(role_id, product_function_id)
VALUES(2, (select id from _not_view));