--liquibase formatted sql

--changeset postgres:set_template_null_endpoints context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-411 Create labels for CS DB that will apply the correct URLs  



UPDATE core.domain_instance
SET context='', sg_context=''
WHERE id=6;

UPDATE core.domain_instance
SET context='', sg_context=''
WHERE id=4;

UPDATE core.domain_instance
SET context='', sg_context=''
WHERE id=1;

UPDATE core.domain_instance
SET context='', sg_context=''
WHERE id=5;
