--liquibase formatted sql

--changeset postgres:add_new_columns_in_file_object context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1474 Add new columns in file object table



ALTER TABLE core.file_object ADD tags varchar NULL;
ALTER TABLE core.file_object ADD tenant_id int4 NOT NULL;
