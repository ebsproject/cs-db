--liquibase formatted sql

--changeset postgres:add_status_column_file_object context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1494 Add column status in file_object table



ALTER TABLE core.file_object ADD status int4 NOT NULL DEFAULT 1;