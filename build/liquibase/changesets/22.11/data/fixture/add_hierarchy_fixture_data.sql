--liquibase formatted sql

--changeset postgres:add_hierarchy_fixture_data design context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1547 Update institution hierarchy design



WITH _ht AS (
    INSERT INTO core.hierarchy_tree
        (value, parent_id, record_id, hierarchy_design_id, creator_id)
    VALUES
        ('Multi-Crop', NULL, NULL, 0, 1)
RETURNING id
), 

_ht1 AS (
        INSERT INTO core.hierarchy_tree
        (value, parent_id, record_id, hierarchy_design_id, creator_id)
    VALUES
        ('Global Maize Program', (select id from _ht), 0, 1, 1)
RETURNING id
),

_ht2 AS (

INSERT INTO core.hierarchy_tree
    (value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES
    ('CIMMYT', (select id from _ht1), (select id from crm.institution where common_name = 'CIMMYT'),2, 1)
RETURNING id
),

_ht3 AS (
INSERT INTO core.hierarchy_tree
    (value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES
    ('CIMMYT Mexico Wheat Molecular Laboratory', (select id from _ht2), (select id from crm.institution where common_name = 'WMBL'), 4, 1)
RETURNING id
), 

_ht4 AS (
    INSERT INTO core.hierarchy_tree
        (value, parent_id, record_id, hierarchy_design_id, creator_id)
    VALUES
    ('Lab Molecular Team', (select id from _ht3), 0, 5, 1)
RETURNING id
), 

_ht5 AS (
    INSERT INTO core.hierarchy_tree
        (value, parent_id, record_id, hierarchy_design_id, creator_id)
    VALUES
    ('Lab Manager', (select id from _ht4), (select id from security.role where description = 'Lab Manager'), 6, 1)
RETURNING id
)

    INSERT INTO core.hierarchy_tree
        (value, parent_id, record_id, hierarchy_design_id, creator_id)
    VALUES
    ('Ernesto Briones', (select id from _ht5),0, 7, 1),
    ('Global Wheat Program', (select id from _ht), 0, 1, 1),
    ('Global Rice Program', (select id from _ht), 0, 1, 1),
    ('CIMMYT Kenya', (select id from _ht1), (select id from crm.institution where common_name = 'CIMMYT Kenya'), 2, 1),
    ('DW', (select id from _ht2), 0, 3, 1)
;


INSERT INTO core.hierarchy_tree
    (value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES
    ('DW team', (select id from core.hierarchy_tree WHERE value = 'DW'), 0, 5, 1);
  