--liquibase formatted sql

--changeset postgres:add_service_providers_data design context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1527 Add Service Provider to fixture version


SELECT setval('crm.contact_id_seq', (SELECT MAX(id) FROM crm.person));

INSERT INTO crm.contact
(creation_timestamp, creator_id, is_void, email, category_id)
VALUES
(now(), 0, false, 'cimmyt@cgiar.org', 2),
(now(), 0, false, 'irri@irri.org', 2);

INSERT INTO crm.contact_purpose
(contact_id, purpose_id)
VALUES
((select id from crm.contact where email = 'cimmyt@cgiar.org'), 4),
((select id from crm.contact where email = 'irri@irri.org'), 4)
;

INSERT INTO crm.institution
(common_name, legal_name, contact_id, creation_timestamp, creator_id, is_void)
VALUES
('CIMMYT', 'CIMMYT', (SELECT id from crm.contact where email = 'cimmyt@cgiar.org'), now(), 1, false),
('IRRI', 'IRRI', (SELECT id from crm.contact where email = 'irri@irri.org'), now(), 1, false);


do $$
declare purpose int;
declare category int;
declare ct_id int;
declare _cimmyt int;
declare _irri int;

begin 

SELECT id from crm.purpose where name = 'Lab Services' INTO purpose;
SELECT id from crm.category where name = 'Internal Unit' INTO category;
SELECT id from crm.contact_type where name = 'Service Provider' INTO ct_id;


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'cimmytindia@cimmyt.org', 2)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id)
VALUES ('CIMMYT India', 'CIMMYT India', (select id from contact), 1);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'cimmytindia@cimmyt.org'), 4);


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'cimmytkenya@cimmyt.org', 2)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id)
VALUES ('CIMMYT Kenya', 'CIMMYT Kenya', (select id from contact), 1);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'cimmytkenya@cimmyt.org'), 4);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'wmbl@cimmyt.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('WMBL', 'CIMMYT Mexico Wheat Molecular Laboratory', (select id from contact), 1, 1);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'wmbl@cimmyt.org'), purpose);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'asm@cimmyt.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('ASM', 'CIMMYT India Maize Molecular Laboratory', (select id from contact), 1, 2);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'asm@cimmyt.org'), purpose);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'afm@cimmyt.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('AFM', 'CIMMYT Kenya Maize Molecular Laboratory', (select id from contact), 1, 3);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'afm@cimmyt.org'), purpose);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'lam@cimmyt.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('LAM', 'CIMMYT Mexico Maize Molecular Laboratory', (select id from contact), 1, 4);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'lam@cimmyt.org'), purpose);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'shlm@cimmyt.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('SHLM', 'CIMMYT Mexico Maize Seed Health Laboratory', (select id from contact), 1, 5);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'shlm@cimmyt.org'), purpose);


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'shlw@cimmyt.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('SHLW', 'CIMMYT Mexico Wheat Seed Health Laboratory', (select id from contact), 1, 6);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'shlw@cimmyt.org'), purpose);


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'mzq@cimmyt.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('MZQ', 'CIMMYT Mexico Maize Quality Laboratory', (select id from contact), 1, 7);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'mzq@cimmyt.org'), purpose);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'wql@cimmyt.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('WQL', 'CIMMYT Mexico Wheat Quality Laboratory', (select id from contact), 1, 8);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'wql@cimmyt.org'), purpose);


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'sdu@cimmyt.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('SDU', 'CIMMYT Mexico Seed Distribution Unit', (select id from contact), 1, 9);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'sdu@cimmyt.org'), purpose);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'gsl@irri.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('GSL', 'IRRI Philippines Genotyping Service Lab', (select id from contact), 1, 10);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'gsl@irri.org'), purpose);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'shu@irri.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('SHU', 'IRRI Philippines Seed Health Unit', (select id from contact), 1, 11);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'shu@irri.org'), purpose);



WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'gqnsl@irri.org', category)
RETURNING id
)

INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('GQNSL', 'IRRI Philippines Grain Quality Nutrition Service Lab', (select id from contact), 1, 12);

INSERT INTO crm.contact_purpose (contact_id, purpose_id)
VALUES ((select id from crm.contact where email = 'gqnsl@irri.org'), purpose);



INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'WMBL'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'ASM'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'AFM'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'LAM'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'SHLM'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'SHLW'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'MZQ'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'WQL'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'SDU'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'GSL'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'SHU'));

INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select contact_id from crm.institution where common_name = 'GQNSL'));


--- Add hierarchy

SELECT id from crm.contact where email = 'cimmyt@cgiar.org' INTO _cimmyt;
SELECT id from crm.contact where email = 'irri@irri.org' INTO _irri;

INSERT INTO crm."hierarchy"
    (contact_id, parent_id, is_principal_contact, creator_id)
VALUES
    ((select id from crm.contact where email = 'gqnsl@irri.org'), _irri, false, 1),
    ((select id from crm.contact where email = 'shu@irri.org'), _irri, false, 1),
    ((select id from crm.contact where email = 'gsl@irri.org'), _irri, false, 1),

    ((select id from crm.contact where email = 'sdu@cimmyt.org'), _cimmyt, false, 1),
    ((select id from crm.contact where email = 'wql@cimmyt.org'), _cimmyt, false, 1),
    ((select id from crm.contact where email = 'mzq@cimmyt.org'), _cimmyt, false, 1),
    ((select id from crm.contact where email = 'shlw@cimmyt.org'), _cimmyt, false, 1),
    ((select id from crm.contact where email = 'shlm@cimmyt.org'), _cimmyt, false, 1),
    ((select id from crm.contact where email = 'lam@cimmyt.org'), _cimmyt, false, 1),
    ((select id from crm.contact where email = 'wmbl@cimmyt.org'), _cimmyt, false, 1),

    ((select id from crm.contact where email = 'afm@cimmyt.org'), (select id from crm.contact where email = 'cimmytkenya@cimmyt.org'), false, 1),
    ((select id from crm.contact where email = 'asm@cimmyt.org'), (select id from crm.contact where email = 'cimmytindia@cimmyt.org'), false, 1)
    ;
   
end $$;

do $$
declare temprow record;
declare ct int;

begin
SELECT id FROM crm.contact_type WHERE name = 'Customer' INTO ct;

FOR temprow IN SELECT id, contact_id FROM crm.institution
    loop    
        INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
        VALUES(ct, temprow.contact_id);
        INSERT INTO crm.contact_address (contact_id, address_id)
        VALUES (temprow.contact_id, 2);
    end loop;
end $$;

