--liquibase formatted sql

--changeset postgres:set_val_institution_sequence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1527 Set max id to institution



SELECT setval('crm.address_id_seq', (SELECT MAX(id) FROM crm.address));