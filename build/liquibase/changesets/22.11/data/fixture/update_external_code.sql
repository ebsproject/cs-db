--liquibase formatted sql

--changeset postgres:update_gsl_external_code context:template splitStatements:false rollbackSplitStatements:false
--comment: Update gsl external code


UPDATE crm.institution
SET external_code=13
WHERE common_name = 'GSL';