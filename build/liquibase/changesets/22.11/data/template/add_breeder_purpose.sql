--liquibase formatted sql

--changeset postgres:add_breeder_purpose context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1125 Add Breeder purpose


INSERT INTO crm.purpose
("name", creator_id, category_id)
VALUES('Breeder', 1, (SELECT id FROM crm.category WHERE name = 'Person'));
