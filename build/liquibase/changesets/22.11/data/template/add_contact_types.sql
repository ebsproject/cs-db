--liquibase formatted sql

--changeset postgres:add_contact_types context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1252 Add nes contact types


INSERT INTO crm.contact_type
("name", creator_id, category_id)
VALUES

('Department', 1, 2),
('Gene Bank', 1, 2),
('Governmental', 1, 2),
('Individual', 1, 1),
('Institution', 1, 2),
('International Center', 1, 2),
('Int Agriculture Center', 1, 2),
('National Agriculture Center', 1, 2),
('National Center', 1, 2),
('Non-Governmental', 1, 2),
('Private Company', 1, 2),
('Regional Organization', 1, 2),
('University', 1, 2),
('Other', 1, 1),
('Unknown', 1, 1)
;
