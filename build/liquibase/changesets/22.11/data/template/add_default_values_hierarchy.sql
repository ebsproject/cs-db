--liquibase formatted sql

--changeset postgres:add_default_values_hierarchy context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1547 Add default values in hierarchy



ALTER TABLE core.entity_reference ALTER COLUMN graphql 
SET DEFAULT 
'findContactList{
    content{
      id
      institution {
        legalName
      }
    }
  }';

UPDATE core.hierarchy_design
SET "filter"='{col: "category.name",mod: EQ,val: "Institution"}'
WHERE "name"='Institution';

UPDATE core.hierarchy_design
SET "filter"='{col: "category.name",mod: EQ,val: "Internal Unit"}'
WHERE "name"='Unit';

UPDATE core.entity_reference
SET textfield='institution.legalName'
WHERE entity='Institution';
