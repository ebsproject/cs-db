--liquibase formatted sql

--changeset postgres:add_functions_to_shipment_tool context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1553 Add product functions to shipment tool product



do $$
declare r_admin int;
declare r_sm_user int;
declare r_sm_admin int;
declare p_ship int;
declare temprow record;

begin

UPDATE core.product SET "name" = 'Shipment Manager' where "name" = 'Shipment Tool';

SELECT id FROM "security"."role" where name = 'Admin' INTO r_admin;
SELECT id FROM "security"."role" where name = 'SM Admin' INTO r_sm_admin;
SELECT id FROM core.product where name = 'Shipment Manager' INTO p_ship;

WITH _approve as (
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
VALUES
    ('Approve', true, 'Approve', 1, p_ship)
RETURNING id
)

INSERT INTO "security".role_product_function (role_id, product_function_id)
VALUES
    (r_admin, (SELECT id FROM _approve)),
    (r_sm_admin, (SELECT id FROM _approve))
;

--------------
WITH _reject as (
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
VALUES
    ('Reject', true, 'Reject', 1, p_ship)
RETURNING id
)

INSERT INTO "security".role_product_function (role_id, product_function_id)
VALUES
    (r_admin, (SELECT id FROM _reject)),
    (r_sm_admin, (SELECT id FROM _reject))
;


--------------
WITH _submit as (
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
VALUES
    ('Submit', true, 'Submit', 1, p_ship)
RETURNING id
)

INSERT INTO "security".role_product_function (role_id, product_function_id)
VALUES
    (r_admin, (SELECT id FROM _submit)),
    (r_sm_admin, (SELECT id FROM _submit))
;

end $$