--liquibase formatted sql

--changeset postgres:add_functions_to_shm context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1252 Add functions to Shipment Manager



do $$
declare _admin int;
declare _sm_admin int;
declare _shm int;
declare _user int;
declare _approver int;
declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'SM Admin' INTO _sm_admin;
SELECT id FROM "security"."role" where name = 'User' INTO _user;
SELECT id FROM "security"."role" where name = 'Approver' INTO _approver;
SELECT id FROM core.product where name = 'Shipment Manager' INTO _shm;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
VALUES
    ('Approve', true, 'Approve', 1, _shm),
    ('Create', true, 'Create', 1, _shm),
    ('Delete', true, 'Delete', 1, _shm),
    ('Export', true, 'Export', 1, _shm),
    ('Modify', true, 'Modify', 1, _shm),
    ('Print', true, 'Print', 1, _shm),
    ('Read', true, 'Read', 1, _shm),
    ('Reject', true, 'Reject', 1, _shm),
    ('Search', true, 'Search', 1, _shm),
    ('Submit', true, 'Submit', 1, _shm),
    ('View', true, 'View', 1, _shm);


FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _shm
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select _admin, temprow.id);
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select _sm_admin, temprow.id);
END LOOP;


    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select _user, (select id from "security".product_function where product_id = _shm and action = 'Read'));
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select _user, (select id from "security".product_function where product_id = _shm and action = 'View'));
    INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select _approver, (select id from "security".product_function where product_id = _shm and action = 'Approve'));


end $$
