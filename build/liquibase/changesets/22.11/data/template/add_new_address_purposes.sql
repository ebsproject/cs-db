--liquibase formatted sql

--changeset postgres:add_new_address_purposes context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1475 Add new purposes in purpose table



INSERT INTO crm.purpose
("name", creator_id, category_id, is_required)
VALUES
('Office', 1, 3, true),
('Field', 1, 3, true);

UPDATE crm.purpose
SET "name"='Mailing', is_required=true
WHERE "name"='Mail';

UPDATE crm.purpose
SET "name"='Shipment', is_required=true
WHERE "name"='Shipping';