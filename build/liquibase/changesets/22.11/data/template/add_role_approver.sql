--liquibase formatted sql

--changeset postgres:add_role_approver context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1584 Add role "Approver" in CSDB



WITH _role AS (
    INSERT INTO "security"."role"
        (description, security_group, creator_id, "name")
    VALUES
        ('Approver', 'EBS Users', 1, 'Approver')

RETURNING id
)

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES    ((SELECT id FROM _role), (SELECT id FROM security.product_function WHERE description = 'Approve'));
