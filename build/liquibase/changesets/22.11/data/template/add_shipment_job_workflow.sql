--liquibase formatted sql

--changeset postgres:add_shipment_job_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1556 Add Shipment Job Workflow



INSERT INTO core.job_workflow
(job_type_id, product_function_id, translation_id, "name", description, tenant_id, creator_id)
VALUES(1, (SELECT pf.id FROM "security".product_function pf 	
            inner join core.product p on p.id = pf.product_id
            where p.name= 'Shipment Manager' AND pf.action = 'Submit'),
        1, 'Shipment Manager', 'Shipment Manager Notification', 1, 1);
