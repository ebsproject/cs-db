--liquibase formatted sql

--changeset postgres:add_shipment_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1556 Add Shipment Workflow



do $$
declare _sh_wf integer;

begin

WITH _wf as (
INSERT INTO workflow.workflow
(title, "name", description, help, sort_no, definition, icon, creator_id, is_void, entityreference_id, tenant_id)
VALUES('Shipment Seed Material', 'Shipment Seed Material', 'Shipment Seed Material', 'Shipment Seed Material', 1, 'Allows to send Material to Vendors', '', 1, false, 80, 1)
RETURNING id
)
SELECT id from _wf INTO _sh_wf;

-----
WITH _ph1 AS (
INSERT INTO workflow.phase
("name", description, help, "sequence", tenant_id, creator_id, workflow_id)
VALUES
('Create', 'Creating Phase', 'First Phase of the process', 1, 1, 1, _sh_wf)
RETURNING id
)

INSERT INTO workflow.stage
("name", description, help, "sequence", tenant_id, creator_id, phase_id)
VALUES
('Draft', 'Draft', 'Draft', 1, 1, 1, (select id from _ph1)),
('Created', 'Created', 'Created', 2, 1, 1, (select id from _ph1)),
('Submitted', 'Submitted', 'Submitted', 3, 1, 1, (select id from _ph1));

-----
WITH _ph2 AS (
INSERT INTO workflow.phase
("name", description, help, "sequence", tenant_id, creator_id, workflow_id)
VALUES
('Approval', 'Approval Process', 'Approval Process', 2, 1, 1, _sh_wf)
RETURNING id
)

INSERT INTO workflow.stage
("name", description, help, "sequence", tenant_id, creator_id, phase_id)
VALUES
('Approved', 'Approved', 'Approved', 1, 1, 1, (select id from _ph2)),
('Rejected', 'Rejected', 'Rejected', 2, 1, 1, (select id from _ph2)),
('Done', 'Done', 'Done', 3, 1, 1, (select id from _ph2));

-----
WITH _ph3 AS (
INSERT INTO workflow.phase
("name", description, help, "sequence", tenant_id, creator_id, workflow_id)
VALUES
('Dispatched', 'Dispatched', 'Dispatched', 3, 1, 1, _sh_wf)
RETURNING id
)

INSERT INTO workflow.stage
("name", description, help, "sequence", tenant_id, creator_id, phase_id)
VALUES
('Sent', 'Sent', 'Sent', 1, 1, 1, (select id from _ph3));

-----
WITH _ph4 AS (
INSERT INTO workflow.phase
("name", description, help, "sequence", tenant_id, creator_id, workflow_id)
VALUES
('Waiting for Result', 'Waiting for Result', 'Waiting for Result', 4, 1, 1, _sh_wf)
RETURNING id
)

INSERT INTO workflow.stage
("name", description, help, "sequence", tenant_id, creator_id, phase_id)
VALUES
('Received', 'Received', 'Received', 1, 1, 1, (select id from _ph4)),
('Completed', 'Completed', 'Completed', 2, 1, 1, (select id from _ph4))
;

end $$