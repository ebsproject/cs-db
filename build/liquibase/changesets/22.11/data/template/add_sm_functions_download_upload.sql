--liquibase formatted sql

--changeset postgres:add_sm_functions_download_upload context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-985 Create functions Upload Result File and Download Result file



do $$
declare _a_id int;
declare _sma_id int;
declare _b_id int;
declare _lm_id int;

declare _bm_id int;
declare _rm_id int;


begin

SELECT id from "security"."role" where name = 'Admin' INTO _a_id;
SELECT id from "security"."role" where name = 'SM Admin' INTO _sma_id;
SELECT id from "security"."role" where name = 'Breeder' INTO _b_id;
SELECT id from "security"."role" where name = 'Lab Manager' INTO _lm_id;

SELECT id from core.product where name = 'Batch Manager' INTO _bm_id;
SELECT id from core.product where name = 'Request Manager' INTO _rm_id;


WITH _upload AS (
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('Upload Result File', true, 'Upload Result File', 1, _bm_id)
RETURNING id
)

    INSERT INTO "security".role_product_function
    (role_id, product_function_id)
    VALUES 
        (_a_id, (select id from _upload)),
        (_sma_id, (select id from _upload)),
        (_b_id, (select id from _upload)),
        (_lm_id, (select id from _upload))
    ;


WITH _download AS (
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('Download Result file', true, 'Download Result file', 1, _rm_id)
RETURNING id
)

    INSERT INTO "security".role_product_function
    (role_id, product_function_id)
    VALUES 
        (_a_id, (select id from _download)),
        (_sma_id, (select id from _download)),
        (_b_id, (select id from _download)),
        (_lm_id, (select id from _download))
    ;

end $$