--liquibase formatted sql

--changeset postgres:add_view_action_to_all_products context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1553 Add view functions to all products



do $$
declare _admin int;
declare _guest int;
declare _user int;
declare _sm_user int;
declare _sm_admin int;
declare _cs_user int;
declare _cs_admin int;
declare _ba_user int;
declare _ba_admin int;
declare _cb_user int;
declare _cb_admin int;
declare _breeder int;
declare _molecular int;
declare _lab_manager int;
declare _data_manager int;
declare myid int;
declare temprow record;

begin
SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'Guest' INTO _guest;
SELECT id FROM "security"."role" where name = 'User' INTO _user;

SELECT id FROM "security"."role" where name = 'SM Admin' INTO _sm_user;
SELECT id FROM "security"."role" where name = 'SM User' INTO _sm_admin;
SELECT id FROM "security"."role" where name = 'CS Admin' INTO _cs_user;
SELECT id FROM "security"."role" where name = 'CS User' INTO _cs_admin;
SELECT id FROM "security"."role" where name = 'BA Admin' INTO _ba_user;
SELECT id FROM "security"."role" where name = 'BA User' INTO _ba_admin;
SELECT id FROM "security"."role" where name = 'CB Admin' INTO _cb_user;
SELECT id FROM "security"."role" where name = 'CB User' INTO _cb_admin;

SELECT id FROM "security"."role" where name = 'Molecular Breeder' INTO _molecular;
SELECT id FROM "security"."role" where name = 'Lab Manager' INTO _lab_manager;
SELECT id FROM "security"."role" where name = 'Data Manager' INTO _data_manager;
SELECT id FROM "security"."role" where name = 'Breeder' INTO _breeder;


FOR temprow IN
select distinct p.id, p.name, d.prefix
    FROM "security".product_function pf 	
    inner join core.product p on p.id = pf.product_id
    inner join core.domain d on p.domain_id = d.id
    where p.name <> 'Analysis Request Manager' and p.name <> 'Genotyping Service Manager' and p.name <> 'Data Explorer' and p.name <> 'Notification' and p.name <> 'Messaging'  
LOOP
 
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id)
    VALUES 
        ('View', true, 'View', 1, temprow.id)
    RETURNING id INTO myid;
    
        IF temprow.prefix = 'cs' 
            THEN
                    INSERT INTO "security".role_product_function (role_id, product_function_id)
                    VALUES
                        (_cs_admin, myid),
                        (_cs_user, myid),
                        (_admin, myid),
                        (_user, myid),
                        (_guest, myid);  
        ELSEIF temprow.prefix = 'cb' 
            THEN
                    INSERT INTO "security".role_product_function (role_id, product_function_id)
                    VALUES
                        (_cb_admin, myid),
                        (_cb_user, myid),
                        (_admin, myid),
                        (_user, myid),
                        (_guest, myid);    
        ELSEIF temprow.prefix = 'ba' 
            THEN
                    INSERT INTO "security".role_product_function (role_id, product_function_id)
                    VALUES
                        (_ba_admin, myid),
                        (_ba_user, myid),
                        (_admin, myid),
                        (_user, myid),
                        (_guest, myid);             
        ELSEIF temprow.prefix = 'sm' 
            THEN
                    INSERT INTO "security".role_product_function (role_id, product_function_id)
                    VALUES
                        (_sm_admin, myid),
                        (_sm_user, myid),
                        (_breeder, myid),
                        (_molecular, myid),
                        (_lab_manager, myid),
                        (_data_manager, myid),
                        (_admin, myid),
                        (_user, myid),
                        (_guest, myid); 
        END IF;  
END LOOP;
end $$;