--liquibase formatted sql

--changeset postgres:populate_contact_purpose_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1557 Create a new relation contact_purpose


INSERT INTO crm.contact_purpose 
(contact_id, purpose_id)
(select id, purpose_id from crm.contact);
