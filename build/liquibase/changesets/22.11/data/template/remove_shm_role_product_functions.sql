--liquibase formatted sql

--changeset postgres:remove_shm_role_product_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1252 Remove role product functions related to shm


DELETE FROM "security".role_product_function WHERE product_function_id IN 
(SELECT pf.id FROM "security".product_function pf inner join core.product p on p.id = pf.product_id
where p.name= 'Shipment Manager')