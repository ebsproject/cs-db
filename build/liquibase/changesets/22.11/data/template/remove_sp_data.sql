--liquibase formatted sql

--changeset postgres:remove_sp_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1527 Remove Service Provider from template version



do $$
declare temprow record;
begin
for temprow IN SELECT id FROM crm.contact WHERE category_id in (2,4) and is_void = false
	loop
        DELETE FROM crm.institution WHERE contact_id = temprow.id ;
        DELETE FROM crm.contact_contact_type WHERE contact_id = temprow.id;
        DELETE FROM crm."hierarchy" h WHERE contact_id = temprow.id;
        DELETE FROM crm."hierarchy" h WHERE parent_id = temprow.id;
        DELETE FROM crm.contact_address WHERE contact_id = temprow.id;
        DELETE FROM crm.contact_purpose WHERE contact_id = temprow.id;
        DELETE FROM core.tenant_contact WHERE contact_id = temprow.id;
        DELETE FROM crm.contact WHERE id = temprow.id;
	end loop;

END $$;
