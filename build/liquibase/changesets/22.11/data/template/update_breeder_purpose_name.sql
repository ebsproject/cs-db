--liquibase formatted sql

--changeset postgres:update_breeder_purpose_name context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1125 Update Breeder purpose


UPDATE crm.purpose SET "name"= 'Breeding' WHERE "name"= 'Breeder';
