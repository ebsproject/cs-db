--liquibase formatted sql

--changeset postgres:update_entity_reference_in_hierarchy context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1547 Update entity_reference_id in hierarchy design



UPDATE core.hierarchy_design
SET entity_reference_id = (select id from core.entity_reference WHERE entity='Program')
WHERE "name"='Program';

UPDATE core.hierarchy_design
SET entity_reference_id = (select id from core.entity_reference WHERE entity='Institution')
WHERE "name"='Institution';

UPDATE core.hierarchy_design
SET entity_reference_id = (select id from core.entity_reference WHERE entity='Institution')
WHERE "name"='Unit';


UPDATE core.entity_reference
SET graphql = 'findContactList{
    content{
      id
      institution {
        legalName
      }
    }
  }'
WHERE entity='Institution';

