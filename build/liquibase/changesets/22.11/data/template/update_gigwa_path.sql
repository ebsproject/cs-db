--liquibase formatted sql

--changeset postgres:update_gigwa_path context:template splitStatements:false rollbackSplitStatements:false
--comment: Update GIGWA path



UPDATE core.product
SET  "path" = 'https://fileapi-uat.ebsproject.org'
WHERE  "name"='GIGWA';

