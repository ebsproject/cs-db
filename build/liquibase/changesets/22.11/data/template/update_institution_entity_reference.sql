--liquibase formatted sql

--changeset postgres:update_institution_entity_reference design context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1547 Update institution hierarchy design


UPDATE core.entity_reference
SET textfield='legal_name', entity_schema='crm'
WHERE entity = 'Institution';
