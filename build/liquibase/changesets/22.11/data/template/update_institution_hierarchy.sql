--liquibase formatted sql

--changeset postgres:Update_institution_hierarchy_design context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1547 Update institution hierarchy design


DELETE FROM core.hierarchy_tree;

DELETE FROM core.hierarchy_design;

INSERT INTO core.hierarchy_design
(id, "name", "filter", "level", "number", parent_id, hierarchy_id, entity_reference_id, is_required, creator_id, is_visible)
VALUES
(0, 'Tenant', '', 0, 1, NULL, 1, NULL, true, 1, false),
(1, 'Breeding Program', '', 1, 1, 0, 1, NULL, true, 1, true),
(2, 'Institution', '', 2, 1, 1, 1, 77, true, 1, true),
(3, 'Program', '', 3, 1, 2, 1, 48, true, 1, true),
(4, 'Unit', '', 3, 1, 2, 1, 77, true, 1, true),
(5, 'Team', '', 4, 1, 3, 1, NULL, false, 1, true),
(6, 'Role', '', 5, 1, 4, 1, NULL, false, 1, false),
(7, 'Member', '', 6, 1, 5, 1, 80, false, 1, false);


SELECT setval('core.hierarchy_design_id_seq', (SELECT MAX(id) FROM core.hierarchy_design));

alter sequence core.hierarchy_tree_id_seq restart with 1;
