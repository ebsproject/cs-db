--liquibase formatted sql

--changeset postgres:updates_in_shipment_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1252 Add column address_id in shipment table


UPDATE core.product
SET name='Shipment Tool'
WHERE name='Shipment Manager';



INSERT INTO core.product
("name", description, help, main_entity, icon, creator_id, domain_id, htmltag_id, menu_order, "path")
VALUES
('Shipment Manager', 'Shipment management and reporting for different vendors', 'Shipment Manager', 'Shm', '', 1, 4, 1, 9, 'shm/shipment');
