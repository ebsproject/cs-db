--liquibase formatted sql

--changeset postgres:add_graphql_column_in_entity_reference context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1554 Add new column graphql in entity_reference table



ALTER TABLE core.entity_reference
 ADD COLUMN graphql text NULL;