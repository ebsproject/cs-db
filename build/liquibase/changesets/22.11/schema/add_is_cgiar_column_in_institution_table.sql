--liquibase formatted sql

--changeset postgres:add_is_cgiar_column_in_institution_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1554 Add new column is_cgiar in Institution table



ALTER TABLE crm.institution 
 ADD COLUMN is_cgiar boolean NOT NULL DEFAULT FALSE;

ALTER TABLE crm.person 
 ADD COLUMN salutation varchar(10);