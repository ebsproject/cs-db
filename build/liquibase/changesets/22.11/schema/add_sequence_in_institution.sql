--liquibase formatted sql

--changeset postgres:add_sequence_in_institution context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1527 Add sequence to institution table


CREATE SEQUENCE crm.institution_id_seq INCREMENT 1 START 1;

ALTER TABLE crm.institution ALTER COLUMN id SET DEFAULT NEXTVAL(('crm."institution_id_seq"'::text)::regclass);

