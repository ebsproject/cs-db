--liquibase formatted sql

--changeset postgres:address_purpose_changes context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-581 Add more Address categories


--DB-1470 Create new table address_purpose
CREATE TABLE crm.address_purpose
(
	purpose_id integer NOT NULL,
	address_id integer NOT NULL
)
;

ALTER TABLE crm.address_purpose ADD CONSTRAINT "PK_address_purpose"
	PRIMARY KEY (purpose_id,address_id)
;

ALTER TABLE crm.address_purpose ADD CONSTRAINT "FK_address_purpose_address"
	FOREIGN KEY (address_id) REFERENCES crm.address (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.address_purpose ADD CONSTRAINT "FK_address_purpose_purpose"
	FOREIGN KEY (purpose_id) REFERENCES crm.purpose (id) ON DELETE No Action ON UPDATE No Action
;

--DB-1472 Add new column in contact_address table
ALTER TABLE crm.contact_address 
 ADD COLUMN is_default boolean NOT NULL DEFAULT false;


--DB-1471 Add new column in purpose table
ALTER TABLE crm.purpose 
 ADD COLUMN is_required boolean NOT NULL DEFAULT false;

ALTER TABLE crm.address 
 DROP COLUMN IF EXISTS purpose_id;

 COMMENT ON TABLE crm.contact_address
	IS 'Relation between a contact and its address, allows a contact to have more than one address associated'
;

COMMENT ON COLUMN crm.contact_address.address_id
	IS 'Referece to the id in the address table'
;

COMMENT ON COLUMN crm.contact_address.contact_id
	IS 'Referece to the id in the contact table'
;

COMMENT ON COLUMN crm.contact_address.is_default
	IS 'Defines if the address associated is the default for the contact'
;

COMMENT ON COLUMN crm.purpose.is_required
	IS 'An address has to be related to at least one of the purposes marked as required'
;
