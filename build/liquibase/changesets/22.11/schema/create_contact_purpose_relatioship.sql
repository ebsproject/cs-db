--liquibase formatted sql

--changeset postgres:create_contact_purpose_relatioship context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1557 Create a new relation contact_purpose



CREATE TABLE crm.contact_purpose
(
	contact_id integer NULL,
	purpose_id integer NULL
)
;

ALTER TABLE crm.contact_purpose ADD CONSTRAINT "FK_contact_purpose_contact"
	FOREIGN KEY (contact_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact_purpose ADD CONSTRAINT "FK_contact_purpose_purpose"
	FOREIGN KEY (purpose_id) REFERENCES crm.purpose (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE crm.contact_purpose ADD CONSTRAINT "PK_contact_purpose"
	PRIMARY KEY (contact_id,purpose_id)
;

