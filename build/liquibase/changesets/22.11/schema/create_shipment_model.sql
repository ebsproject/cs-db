--liquibase formatted sql

--changeset postgres:create_shipment_model context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1252 Create a structure for shipment in CSDB


CREATE schema shm;

CREATE TABLE shm.shipment
(
	id integer NOT NULL   DEFAULT NEXTVAL(('shm."shipment_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	wf_instance_id integer NOT NULL,
	program_id integer NOT NULL,
	project_id integer NULL,
	sender_id integer NOT NULL,
	processor_id integer NULL,
	requester_id integer NULL,
	recipient_id integer NULL,
	code varchar(150) NOT NULL,
	name varchar(150) NOT NULL,
	shu_reference_number varchar(150) NULL,
	purpose text NULL,
	material_type varchar(32) NOT NULL,
	material_subtype varchar(32) NULL,
	document_generated_date date NULL,
	document_signed_date date NULL,
	authorized_signatory varchar(64) NULL,
	shipped_date date NULL,
	received_date date NULL,
	airway_bill_number varchar(150) NULL,
	remarks text NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE shm.shipment_file
(
	id integer NOT NULL   DEFAULT NEXTVAL(('shm."shipment_file_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	shipment_id integer NOT NULL,
	file_object_id uuid NOT NULL,
	remarks varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE shm.shipment_item
(
	id integer NOT NULL   DEFAULT NEXTVAL(('shm."shipment_item_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	shipment_id integer NOT NULL,
	germplasm_id integer NOT NULL,
	seed_id integer NOT NULL,
	package_id integer NULL,
	number integer NOT NULL,
	code varchar(150) NOT NULL,
	status varchar(50) NOT NULL   DEFAULT 'passed',
	weight integer NOT NULL,
	package_unit varchar(32) NOT NULL,
	package_count integer NOT NULL,
	test_code varchar(150) NULL,
	mta_status varchar(64) NULL,
	availability varchar(64) NULL,
	use varchar(64) NULL,
	smta_id varchar(256) NULL,
	mls_ancestors varchar(50) NULL,
	genetic_stock varchar(50) NULL,
	remarks text NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE SEQUENCE shm.shipment_file_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE shm.shipment_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE shm.shipment_item_id_seq INCREMENT 1 START 1;


ALTER TABLE shm.shipment ADD CONSTRAINT "PK_shipment"
	PRIMARY KEY (id)
;

ALTER TABLE shm.shipment_file ADD CONSTRAINT "PK_shipment_file"
	PRIMARY KEY (id)
;

ALTER TABLE shm.shipment_item ADD CONSTRAINT "PK_shipment_item"
	PRIMARY KEY (id)
;

ALTER TABLE shm.shipment ADD CONSTRAINT "FK_shipment_contact_processor"
	FOREIGN KEY (processor_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE shm.shipment ADD CONSTRAINT "FK_shipment_contact_sender"
	FOREIGN KEY (sender_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE shm.shipment ADD CONSTRAINT "FK_shipment_contact_requester"
	FOREIGN KEY (requester_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE shm.shipment ADD CONSTRAINT "FK_shipment_contact_recipient"
	FOREIGN KEY (recipient_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE shm.shipment ADD CONSTRAINT "FK_shipment_program"
	FOREIGN KEY (program_id) REFERENCES program.program (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE shm.shipment ADD CONSTRAINT "FK_shipment_wf_instance"
	FOREIGN KEY (wf_instance_id) REFERENCES workflow.wf_instance (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE shm.shipment_file ADD CONSTRAINT "FK_shipment_file_file_object"
	FOREIGN KEY (file_object_id) REFERENCES core.file_object (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE shm.shipment_file ADD CONSTRAINT "FK_shipment_file_shipment"
	FOREIGN KEY (shipment_id) REFERENCES shm.shipment (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE shm.shipment_item ADD CONSTRAINT "FK_shipment_item_shipment"
	FOREIGN KEY (shipment_id) REFERENCES shm.shipment (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN shm.shipment.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN shm.shipment.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN shm.shipment.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN shm.shipment.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN shm.shipment.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN shm.shipment.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN shm.shipment.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN shm.shipment_file.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN shm.shipment_file.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN shm.shipment_file.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN shm.shipment_file.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN shm.shipment_file.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN shm.shipment_file.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN shm.shipment_file.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN shm.shipment_item.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN shm.shipment_item.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN shm.shipment_item.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN shm.shipment_item.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN shm.shipment_item.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN shm.shipment_item.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN shm.shipment_item.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;
