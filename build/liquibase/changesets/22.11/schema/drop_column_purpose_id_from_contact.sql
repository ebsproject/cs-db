--liquibase formatted sql

--changeset postgres:drop_column_purpose_id_from_contact context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1557 Drop column purpose_id from Contact table



ALTER TABLE crm.contact 
 DROP COLUMN IF EXISTS purpose_id;