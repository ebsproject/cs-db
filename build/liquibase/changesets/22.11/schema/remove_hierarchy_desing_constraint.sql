--liquibase formatted sql

--changeset postgres:remove_hierarchy_desing_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1547 Drop constraint parent_id


ALTER TABLE core.hierarchy_design DROP CONSTRAINT "FK_hierarchy_design_hierarchy_design";

