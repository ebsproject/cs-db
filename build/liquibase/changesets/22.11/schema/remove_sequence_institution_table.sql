--liquibase formatted sql

--changeset postgres:remove_sequence_institution_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1527 Remove sequence from institution table


ALTER TABLE crm.institution ALTER COLUMN id DROP DEFAULT;

DROP SEQUENCE crm.institution_id_seq;

