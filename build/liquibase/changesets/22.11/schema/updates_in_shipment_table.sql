--liquibase formatted sql

--changeset postgres:updates_in_shipment_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1252 Add column address_id in shipment table



ALTER TABLE shm.shipment 
    ADD COLUMN address_id int4 NULL;


ALTER TABLE shm.shipment ADD CONSTRAINT "FK_shipment_contact_address_address"
    FOREIGN KEY (address_id, recipient_id) REFERENCES crm.contact_address (address_id, contact_id) ON DELETE No Action ON UPDATE No Action
;
