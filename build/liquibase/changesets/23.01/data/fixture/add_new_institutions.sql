--liquibase formatted sql

--changeset postgres:add_new_institutions design context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1268 Add missing institutions in CS fixture data


SELECT setval('crm.institution_id_seq', (SELECT MAX(id) FROM crm.institution));

WITH contact AS (
INSERT INTO crm.contact
(creator_id, email, category_id)
VALUES(1, 'africarice@cgiar.org', 2)
RETURNING id
)

INSERT INTO crm.institution
(common_name, legal_name, contact_id, creator_id)
VALUES('AfricaRice', 'AfricaRice', (select id from contact), 1);


WITH contact AS (
INSERT INTO crm.contact
(creator_id, email, category_id)
VALUES(1, 'iita@cgiar.org', 2)
RETURNING id
)

INSERT INTO crm.institution
(common_name, legal_name, contact_id, creator_id)
VALUES('IITA', 'International Institute of Tropical Agriculture', (select id from contact), 1);


WITH contact AS (
INSERT INTO crm.contact
(creator_id, email, category_id)
VALUES(1, 'icarda@cgiar.org', 2)
RETURNING id
)

INSERT INTO crm.institution
(common_name, legal_name, contact_id, creator_id)
VALUES('ICARDA', 'International Center for Agricultural Research in the Dry Areas', (select id from contact), 1);
