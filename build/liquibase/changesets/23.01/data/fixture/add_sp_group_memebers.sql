--liquibase formatted sql

--changeset postgres:add_sp_group_memebers context:fixture splitStatements:false rollbackSplitStatements:false
--comment: SM-1001 Create the fixture hierarchy data to get the members of Service Provider Group



WITH _lab AS (
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('CIMMYT Mexico Maize Molecular Laboratory', 
(SELECT id FROM core.hierarchy_tree WHERE value = 'CIMMYT'), 
(SELECT contact_id FROM crm.institution WHERE common_name = 'LAM'), 4, 1)
RETURNING id
),

_team AS ( 
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('Maize Molecular Laboratory Team', (select id from _lab), 0, 5, 1)
RETURNING id
),

_role AS (
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('Lab Manager', (select id from _team), (SELECT id FROM "security".role WHERE description = 'Lab Manager'), 6, 1)
RETURNING id
)

INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES
('Luis Puebla Luna', (select id from _role), 0, 7, 1),
('Cesar petroli', (select id from _role), 0, 7, 1),
('Irene Mutesi', (select id from _role), 0, 7, 1),
('Marko Karkkainen', (select id from _role), 0, 7, 1);



WITH _lab AS (
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('CIMMYT Mexico Maize Seed Health Laboratory', 
(SELECT id FROM core.hierarchy_tree WHERE value = 'CIMMYT'), 
(SELECT contact_id FROM crm.institution WHERE common_name = 'SHLM'), 4, 1)
RETURNING id
)

INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('Maize Seed Health Laboratory Team', (select id from _lab), 0, 5, 1);


WITH _lab AS (
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('CIMMYT Mexico Maize Quality Laboratory', 
(SELECT id FROM core.hierarchy_tree WHERE value = 'CIMMYT'), 
(SELECT contact_id FROM crm.institution WHERE common_name = 'MZQ'), 4, 1)
RETURNING id
)

INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('Maize Quality Laboratory Team', (select id from _lab), 0, 5, 1);
