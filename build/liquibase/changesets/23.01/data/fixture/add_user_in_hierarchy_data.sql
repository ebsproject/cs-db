--liquibase formatted sql

--changeset postgres:add_user_in_hierarchy_data design context:fixture splitStatements:false rollbackSplitStatements:false
--comment: SM-1080 Add user in hierarchy data



WITH _role AS (
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('Breeder', 2, 13, 6, 1)
RETURNING id
)

INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('Juan Carlos Moreno Sanchez', (select id from _role), 0, 7, 1);
