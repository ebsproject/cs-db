--liquibase formatted sql

--changeset postgres:update_record_id_in_hierarchy design context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1295 Update record_ id for CIMMYT and CIMMYT Kenya in hierarchy table



UPDATE core.hierarchy_tree
SET record_id = (SELECT id FROM crm.contact WHERE email = 'cimmyt@cgiar.org')
WHERE value='CIMMYT';


UPDATE core.hierarchy_tree
SET record_id = (SELECT id FROM crm.contact WHERE email = 'cimmytkenya@cimmyt.org')
WHERE value='CIMMYT Kenya';

