--liquibase formatted sql

--changeset postgres:add_shipment_job_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1079 Create a new job workflow for Batch Manager notification



INSERT INTO core.job_workflow
(job_type_id, product_function_id, translation_id, "name", description, tenant_id, creator_id)
VALUES(1, (SELECT pf.id FROM "security".product_function pf 	
            inner join core.product p on p.id = pf.product_id
            where p.name= 'Batch Manager' AND pf.action = 'Upload Result File'),
        1, 'Batch Manager', 'Batch Manager notification', 1, 1);


--comment: SM-1084 Create a new job workflow for Genotyping Manager

INSERT INTO core.job_workflow
(job_type_id, product_function_id, translation_id, "name", description, tenant_id, creator_id)
VALUES(1, (SELECT pf.id FROM "security".product_function pf 	
            inner join core.product p on p.id = pf.product_id
            where p.name= 'Genotyping Service Manager' AND pf.action = 'Accept'),
        1, 'Genotyping Service Manager', 'Genotyping Service Manager Notification', 1, 1);

INSERT INTO core.job_workflow
(job_type_id, product_function_id, translation_id, "name", description, tenant_id, creator_id)
VALUES(1, (SELECT pf.id FROM "security".product_function pf 	
            inner join core.product p on p.id = pf.product_id
            where p.name= 'Genotyping Service Manager' AND pf.action = 'Reject'),
        1, 'Genotyping Service Manager', 'Genotyping Service Manager Notification', 1, 1);


