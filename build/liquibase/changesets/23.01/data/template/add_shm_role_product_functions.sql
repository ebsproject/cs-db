--liquibase formatted sql

--changeset postgres:add_shm_role_product_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1147 Add role_product_functions for Shipment Manager



do $$
declare _admin int;
declare _sm_admin int;
declare _user int;
declare _approver int;
declare _data int;
declare _sp int;
declare _shm int;
declare _pf_id int;
declare temprow record;

begin

INSERT INTO "security"."role"
(description, security_group, creator_id, "name")
VALUES('Service Provider', 'SM Users', 1, 'Service Provider');


SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'SM Admin' INTO _sm_admin;
SELECT id FROM "security"."role" where name = 'User' INTO _user;
SELECT id FROM "security"."role" where name = 'Approver' INTO _approver;
SELECT id FROM "security"."role" where name = 'Data Manager' INTO _data;
SELECT id FROM "security"."role" where name = 'Service Provider' INTO _sp;
SELECT id FROM core.product where name = 'Shipment Manager' INTO _shm;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
VALUES
    ('Processing', true, 'Processing', 1, _shm),
    ('On hold', true, 'On hold', 1, _shm);


FOR temprow IN
    SELECT id, "action" FROM "security".product_function where product_id = _shm
LOOP

    INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
        (_admin, temprow.id);
    INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
        (_sm_admin, temprow.id);
    IF temprow.action <> 'Submit' AND temprow.action <> 'Processing'
        THEN
            INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
                (_approver, temprow.id);
    END IF;
END LOOP;


SELECT id FROM "security".product_function where product_id = _shm and "action" = 'Create' INTO _pf_id;
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
                (_data, _pf_id);

SELECT id FROM "security".product_function where product_id = _shm and "action" = 'Modify' INTO _pf_id;
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
                (_data, _pf_id);

SELECT id FROM "security".product_function where product_id = _shm and "action" = 'Print' INTO _pf_id;
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
                (_data, _pf_id),
                (_sp, _pf_id);

SELECT id FROM "security".product_function where product_id = _shm and "action" = 'Read' INTO _pf_id;
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
                (_data, _pf_id),
                (_sp, _pf_id),
                (_user, _pf_id);

SELECT id FROM "security".product_function where product_id = _shm and "action" = 'Search' INTO _pf_id;
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
                (_data, _pf_id),
                (_sp, _pf_id);

SELECT id FROM "security".product_function where product_id = _shm and "action" = 'Submit' INTO _pf_id;
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
                (_data, _pf_id);

SELECT id FROM "security".product_function where product_id = _shm and "action" = 'View' INTO _pf_id;
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
                (_data, _pf_id),
                (_sp, _pf_id),
                (_user, _pf_id);

SELECT id FROM "security".product_function where product_id = _shm and "action" = 'Processing' INTO _pf_id;
INSERT INTO "security".role_product_function (role_id, product_function_id) VALUES
                (_sp, _pf_id);

end $$;
