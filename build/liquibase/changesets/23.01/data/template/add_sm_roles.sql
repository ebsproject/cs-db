--liquibase formatted sql

--changeset postgres:add_sm_roles context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1265 Add new SM roles



INSERT INTO "security"."role"
(description, security_group, creator_id, "name")
VALUES
('Lab Technician', 'SM Users', 1, 'Lab Technician'),
('Lab Team Member', 'SM Users', 1, 'Lab Team Member')
;

