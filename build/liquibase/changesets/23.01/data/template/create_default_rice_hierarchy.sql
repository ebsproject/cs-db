--liquibase formatted sql

--changeset postgres:create_default_rice_hierarchy context:template labels:rice splitStatements:false rollbackSplitStatements:false
--comment: CS-1271  Create default Rice Institution and hierarchy



do $$
declare _inst_id int;

begin

WITH _contact AS (
INSERT INTO crm.contact
(creator_id, email, category_id)
VALUES(1, 'irri@cgiar.org', (SELECT id FROM crm.category WHERE name = 'Institution'))
RETURNING id
)

INSERT INTO crm.institution
(common_name, legal_name, contact_id, creator_id, is_cgiar)
VALUES('IRRI', 'International Rice Research Institute', (SELECT id FROM _contact), 1, true);

SELECT id FROM crm.institution WHERE common_name = 'IRRI' INTO _inst_id;


WITH _tenant AS (
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES
('Multi-Crop', NULL, NULL, (SELECT id FROM core.hierarchy_design WHERE name = 'Tenant'), 1)
RETURNING id
),

_program AS (
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES   
('Global Program', (SELECT id FROM _tenant), 0, (SELECT id FROM core.hierarchy_design WHERE name = 'Breeding Program'), 1)
RETURNING id
)

INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES ('IRRI', (SELECT id FROM _program), _inst_id, (SELECT id FROM core.hierarchy_design WHERE name = 'Institution'), 1);

end $$