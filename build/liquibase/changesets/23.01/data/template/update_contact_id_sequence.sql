--liquibase formatted sql

--changeset postgres:create_default_maize_hierarchy context:template splitStatements:false rollbackSplitStatements:false
--comment: Update contact_id sequence



do $$
declare _contact_id int;
begin

SELECT MAX(id) FROM crm.contact INTO _contact_id;

IF _contact_id = 0 THEN
    ALTER SEQUENCE crm.contact_id_seq RESTART WITH 1;
ELSE
    PERFORM setval('crm.contact_id_seq', (SELECT MAX(id) FROM crm.contact));
END IF;

end $$
