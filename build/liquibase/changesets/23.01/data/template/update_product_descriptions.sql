--liquibase formatted sql

--changeset postgres:update_product_descriptions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1294 Update product descriptions



UPDATE core.product
SET description='The Printout is a product that enables users to create print-out templates and use the templates from within other EBS products to generate printout reports in various formats.'
WHERE "name"='Printout';

UPDATE core.product
SET description='Ability to create and manage users and user roles. User accounts enable the end user to log in to the system. The roles enable role-based access control where the permissions to EBS products are managed via roles. The current version provides the required basic functionalities.'
WHERE "name"='User Management';

UPDATE core.product
SET description='Enables a user with permission to perform various actions around tenants and their existence. Existence of the organization is a single tenancy, which is to be enhanced to multiple tenancy provisions.'
WHERE "name"='Tenant Management';

UPDATE core.product
SET description='EBS tool that enables the management and usage of contact information. Creating new, managing existing contact information, and creating relationships between contact entities.'
WHERE "name"='CRM';

UPDATE core.product
SET description='Enables set up and complex configurations around multi crop instances, Institutions , programs, Units, and teams.'
WHERE "name"='Hierarchy';

UPDATE core.product
SET description='EBS is an information management system where actors conduct data production tasks in business processes. Different types of messages are generated as part of the data production activities.'
WHERE "name"='Messaging';
