--liquibase formatted sql

--changeset postgres:update_request_manager_job_type context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1079 Update Request Manager Job type



UPDATE core.job_workflow
SET job_type_id= (SELECT id FROM core.job_type WHERE name = 'Alert')
WHERE id=1;
