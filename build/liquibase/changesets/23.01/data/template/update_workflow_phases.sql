--liquibase formatted sql

--changeset postgres:update_workflow_phases context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1253 Create Data Flow for Shipment Status


DELETE FROM workflow.stage WHERE id > 3;

DELETE FROM workflow.phase
WHERE name = 'Waiting for Result';

UPDATE workflow.phase
SET "name"='Process', description='Shipment Data Processing', help='Shipment Data Processing'
WHERE "name"='Approval';

UPDATE workflow.phase
SET "name"='Shipment', description= 'Dispatching Process', help= 'Dispatching Process'
WHERE "name"='Dispatched';


UPDATE workflow.stage
SET "sequence"=1, phase_id=2
WHERE "name"='Submitted';


SELECT setval('workflow.stage_id_seq', (SELECT MAX(id) FROM workflow.stage));

INSERT INTO workflow.stage
("name", description, help, "sequence", phase_id, tenant_id, creator_id)
VALUES
('Processing', 'Processing', 'Processing', 2, 2, 1, 1),
('On Hold', 'On Hold', 'On Hold', 3, 2, 1, 1),
('Sent', 'Sent', 'Sent', 1, 3, 1, 1),
('Done', 'Done', 'Done', 2, 3, 1, 1)
;

