--liquibase formatted sql

--changeset postgres:add_columns_in_shipment_item context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1344 Add new columns in shipment item table


ALTER TABLE shm.shipment_item 
 ADD COLUMN package_name varchar(100) NULL;

ALTER TABLE shm.shipment_item 
 ADD COLUMN package_label varchar(100) NULL;

ALTER TABLE shm.shipment_item 
 ADD COLUMN seed_name varchar(100) NULL;

ALTER TABLE shm.shipment_item 
 ADD COLUMN seed_code varchar(100) NULL;

ALTER TABLE shm.shipment_item 
 ADD COLUMN designation varchar(100) NULL;

ALTER TABLE shm.shipment_item 
 ADD COLUMN parentage varchar(100) NULL;

ALTER TABLE shm.shipment_item 
 ADD COLUMN taxonomy_name varchar(100) NULL;






