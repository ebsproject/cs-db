--liquibase formatted sql

--changeset postgres:add_description_column_in_file_object_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1313 Add description column in file_object table



ALTER TABLE core.file_object
 ADD COLUMN description text NULL;