--liquibase formatted sql

--changeset postgres:add_program_irsea_in_hierarchy context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-387 Add program IRSEA to hierarchy_tree table



WITH _program AS (
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('IRSEA', 
(SELECT id FROM core.hierarchy_tree WHERE value = 'CIMMYT'), 0, 3, 1)
RETURNING id
),

_team AS ( 
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('IRSEA Team', (select id from _program), 0, 5, 1)
RETURNING id
),

_role AS (
INSERT INTO core.hierarchy_tree
(value, parent_id, record_id, hierarchy_design_id, creator_id)
VALUES('Lab Manager', (select id from _team), (SELECT id FROM "security".role WHERE description = 'Lab Manager'), 6, 1)
RETURNING id
)

UPDATE core.hierarchy_tree
SET parent_id = (SELECT id from _role)
WHERE value = 'Juan Carlos Moreno Sanchez';
