--liquibase formatted sql

--changeset postgres:move_crop_list_to_template_version context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1471 Remove crop list from fixture version



SET session_replication_role = 'replica';

DELETE FROM "program".crop WHERE id > 0 and id < 4;

SET session_replication_role = 'origin';


