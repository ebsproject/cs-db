--liquibase formatted sql

--changeset postgres:update_institutions_address context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1557 Update address for India and Kenya Institutions




WITH _address AS (
INSERT INTO crm.address
("location", region, zip_code, street_address, creator_id, country_id)
VALUES('India', 'New Delhi, Delhi', '110012', 'Dev Prakash Shastri Marg', 1, (select id from crm.country where "name" = 'India'))
RETURNING id
)

UPDATE crm.contact_address
SET address_id = (SELECT id FROM _address)
WHERE contact_id = (select id from crm. contact where email = 'cimmytindia@cimmyt.org');
 

WITH _address AS (
INSERT INTO crm.address
("location", region, zip_code, street_address, creator_id, country_id)
VALUES('Kenya', 'Nairobi', '00100', 'United Nations Avenue, Gigiri', 1, (select id from crm.country where "name" = 'Kenya'))
RETURNING id
)

UPDATE crm.contact_address
SET address_id = (SELECT id FROM _address)
WHERE contact_id = (select id from crm.contact where email = 'cimmytkenya@cimmyt.org');