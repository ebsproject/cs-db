--liquibase formatted sql

--changeset postgres:update_sp_external_code context:fixture splitStatements:false rollbackSplitStatements:false
--comment: SM-1001 Update external code for SM Service Providers



UPDATE crm.institution
SET external_code= 77 WHERE common_name='WMBL';

UPDATE crm.institution
SET external_code= 78 WHERE common_name='ASM';

UPDATE crm.institution
SET external_code= 79 WHERE common_name='AFM';

UPDATE crm.institution
SET external_code= 80 WHERE common_name='LAM';

UPDATE crm.institution
SET external_code= 81 WHERE common_name='SHLM';

UPDATE crm.institution
SET external_code= 82 WHERE common_name='SHLW';

UPDATE crm.institution
SET external_code= 83 WHERE common_name='MZQ';

UPDATE crm.institution
SET external_code= 84 WHERE common_name='WQL';

UPDATE crm.institution
SET external_code= 85 WHERE common_name='SDU';

UPDATE crm.institution
SET external_code= 87 WHERE common_name='SHU';

UPDATE crm.institution
SET external_code= 88 WHERE common_name='GQNSL';

UPDATE crm.institution
SET external_code= 86 WHERE common_name='GSL';
