--liquibase formatted sql

--changeset postgres:add_crop_list_to_template_version context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1471 Add crop list to template version



UPDATE "program".crop
SET code='Default', "name"='Multi-crop', description='Default Multi-crop', notes='Default Multi-crop', creator_id=1
WHERE code='Default';

INSERT INTO "program".crop
(id, code, "name", description, creator_id)
VALUES
(1, 'RICE', 'Rice', 'Rice crop', 1),
(2, 'WHEAT', 'Wheat', 'Wheat crop', 1),
(3, 'MAIZE', 'Maize', 'Maize crop', 1);


SELECT setval('"program".crop_id_seq', (SELECT MAX(id) FROM "program".crop));

