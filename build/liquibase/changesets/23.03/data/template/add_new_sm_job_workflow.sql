--liquibase formatted sql

--changeset postgres:add_new_sm_job_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1239 Create a new job workflows for Batch Manager notification



INSERT INTO core.job_workflow
(job_type_id, product_function_id, translation_id, "name", description, tenant_id, creator_id)
VALUES(1, (SELECT pf.id FROM "security".product_function pf 	
            inner join core.product p on p.id = pf.product_id
            where p.name= 'Batch Manager' AND pf.action = 'Create'),
        1, 'Batch Manager', 'Create Batch Notification', 1, 1);

-- Inssert new product function and link it to different roles

do $$
declare _a_id int;
declare _sma_id int;
declare _b_id int;
declare _lm_id int;
declare _bm_id int;


begin

SELECT id from "security"."role" where name = 'Admin' INTO _a_id;
SELECT id from "security"."role" where name = 'SM Admin' INTO _sma_id;
SELECT id from "security"."role" where name = 'Breeder' INTO _b_id;
SELECT id from "security"."role" where name = 'Lab Manager' INTO _lm_id;

SELECT id from core.product where name = 'Batch Manager' INTO _bm_id;


WITH _upload AS (
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('Clean', true, 'Clean', 1, _bm_id)
RETURNING id
)

    INSERT INTO "security".role_product_function
    (role_id, product_function_id)
    VALUES 
        (_a_id, (select id from _upload)),
        (_sma_id, (select id from _upload)),
        (_b_id, (select id from _upload)),
        (_lm_id, (select id from _upload))
    ;

end $$;

-- Create job_workflow for Clean action in Batch Manager

INSERT INTO core.job_workflow
(job_type_id, product_function_id, translation_id, "name", description, tenant_id, creator_id)
VALUES(1, (SELECT pf.id FROM "security".product_function pf 	
            inner join core.product p on p.id = pf.product_id
            where p.name= 'Batch Manager' AND pf.action = 'Clean'),
        1, 'Batch Manager', 'Clean Batch Notification', 1, 1);