--liquibase formatted sql

--changeset postgres:assign_multi_design_to_ba_user_role context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-862 Add the multi-design analysis privilege to the BA_USER role



do $$
declare ru_id int;
begin

SELECT id from "security"."role" where name = 'BA User' INTO ru_id;


INSERT INTO "security".role_product_function
    (role_id, product_function_id)
        (select ru_id, id from "security".product_function WHERE action = 'Multi_Design');

end $$;