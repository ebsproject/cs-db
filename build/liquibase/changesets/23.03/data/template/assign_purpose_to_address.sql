--liquibase formatted sql

--changeset postgres:assign_purpose_to_address context:template splitStatements:false rollbackSplitStatements:false
--comment: Assign purpose to address


do $$
begin

PERFORM * FROM crm.address_purpose WHERE 
purpose_id = (select id from crm.purpose where name = 'Office') and address_id = (select id from crm.address where location = 'Mexico');
IF NOT FOUND THEN
    INSERT INTO crm.address_purpose (purpose_id, address_id)
    VALUES
    ((select id from crm.purpose where name = 'Office'), (select id from crm.address where location = 'Mexico'));
END IF;


PERFORM * FROM crm.address_purpose WHERE 
purpose_id = (select id from crm.purpose where name = 'Field') and address_id = (select id from crm.address where location = 'Mexico');
IF NOT FOUND THEN
    INSERT INTO crm.address_purpose (purpose_id, address_id)
    VALUES
    ((select id from crm.purpose where name = 'Field'), (select id from crm.address where location = 'Mexico'));
END IF;

PERFORM * FROM crm.address_purpose WHERE 
purpose_id = (select id from crm.purpose where name = 'Mailing') and address_id = (select id from crm.address where location = 'Mexico');
IF NOT FOUND THEN
    INSERT INTO crm.address_purpose (purpose_id, address_id)
    VALUES
    ((select id from crm.purpose where name = 'Mailing'), (select id from crm.address where location = 'Mexico'));
END IF;

PERFORM * FROM crm.address_purpose WHERE 
purpose_id = (select id from crm.purpose where name = 'Shipment') and address_id = (select id from crm.address where location = 'Mexico');
IF NOT FOUND THEN
    INSERT INTO crm.address_purpose (purpose_id, address_id)
    VALUES
    ((select id from crm.purpose where name = 'Shipment'), (select id from crm.address where location = 'Mexico'));
END IF;

end $$