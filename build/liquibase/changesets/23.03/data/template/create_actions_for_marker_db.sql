--liquibase formatted sql

--changeset postgres:create_actions_for_marker_db context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1184 Create Actions for MarkerDB Product



INSERT INTO core.product
(name, description, help, main_entity, icon, creator_id, domain_id, menu_order, "path", htmltag_id)
VALUES('MarkerDb', 'Markerdb', 'Markerdb', 'Marker', 'Markerdb', 1, (select id from core.domain where prefix = 'sm'), 12, 'marker',1);


do $$
declare _a_id int;
declare _sma_id int;
declare _b_id int;
declare _lm_id int;
declare _mdb int;
declare _smu_id int;
declare _dm_id int;
declare temprow record;


begin

SELECT id from "security"."role" where name = 'Admin' INTO _a_id;
SELECT id from "security"."role" where name = 'SM Admin' INTO _sma_id;
SELECT id from "security"."role" where name = 'Breeder' INTO _b_id;
SELECT id from "security"."role" where name = 'Lab Manager' INTO _lm_id;
SELECT id from "security"."role" where name = 'SM User' INTO _smu_id;
SELECT id from "security"."role" where name = 'Data Manager' INTO _dm_id;

SELECT id from core.product where name = 'MarkerDb' INTO _mdb;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Create', true, 'Create', 1, _mdb, true),
    ('Modify', true, 'Modify', 1, _mdb, true),
    ('Delete', true, 'Delete', 1, _mdb, true),
    ('Export', true, 'Export', 1, _mdb, false),
    ('Print', true, 'Print', 1, _mdb, false),
    ('Search', true, 'Search', 1, _mdb, false),
    ('Read', true, 'Read', 1, _mdb, false);


FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _mdb
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_a_id, (select temprow.id)),
        (_sma_id, (select temprow.id)),
        (_b_id, (select temprow.id)),
        (_lm_id, (select temprow.id)),
        (_smu_id, (select temprow.id)),
        (_dm_id, (select temprow.id));
END LOOP;

end $$;

