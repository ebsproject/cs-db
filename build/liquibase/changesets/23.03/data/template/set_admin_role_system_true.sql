--liquibase formatted sql

--changeset postgres:set_admin_role_system_true context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1468 Add column is_system in role table



UPDATE "security"."role"
SET is_system = true 
WHERE "name" = 'Admin';