--liquibase formatted sql

--changeset postgres:update_mee_endpoint context:template labels:mee splitStatements:false rollbackSplitStatements:false
--comment: Update MEE endpoints


UPDATE core.domain_instance
SET context='https://mee.ebsproject.org/', sg_context='https://baapi-mee.ebsproject.org/v1/'
WHERE id=6;

UPDATE core.domain_instance
SET context='https://mee.ebsproject.org/', sg_context='https://smapi-mee.ebsproject.org/'
WHERE id=4;

UPDATE core.domain_instance
SET context='https://cb-mee.ebsproject.org', sg_context='https://cbapi-mee.ebsproject.org/v3'
WHERE id=1;

UPDATE core.domain_instance
SET context='https://mee.ebsproject.org/', sg_context='https://csapi-mee.ebsproject.org/'
WHERE id=5;
