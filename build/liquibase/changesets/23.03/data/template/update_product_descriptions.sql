--liquibase formatted sql

--changeset postgres:update_product_descriptions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1563 Update product descriptions



UPDATE core.product
SET description='Create print-out templates to generate reports in various formats.'
WHERE "name"='Printout';

UPDATE core.product
SET description='Creation and management of contacts. Creation of relationships between contact entities.'
WHERE "name"='CRM';

UPDATE core.product
SET description='Ability to create and manage users and user roles.'
WHERE "name"='User Management';

UPDATE core.product
SET description='Allows you to perform various actions around the tenants and their existence.'
WHERE "name"='Tenant Management';


UPDATE core.product
SET description='Allows visualization around instances of multiple crops, institutions, programs, units, and teams.'
WHERE "name"='Hierarchy';
