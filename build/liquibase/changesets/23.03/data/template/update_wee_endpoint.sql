--liquibase formatted sql

--changeset postgres:update_wee_endpoint context:template labels:wee splitStatements:false rollbackSplitStatements:false
--comment: Update WEE Endpoints


UPDATE core.domain_instance
SET context='https://wee.ebsproject.org/', sg_context='https://baapi-wee.ebsproject.org/v1/'
WHERE id=6;

UPDATE core.domain_instance
SET context='https://wee.ebsproject.org/', sg_context='https://smapi-wee.ebsproject.org/'
WHERE id=4;

UPDATE core.domain_instance
SET context='https://cb-wee.ebsproject.org', sg_context='https://cbapi-wee.ebsproject.org/v3'
WHERE id=1;

UPDATE core.domain_instance
SET context='https://wee.ebsproject.org/', sg_context='https://csapi-wee.ebsproject.org/'
WHERE id=5;
