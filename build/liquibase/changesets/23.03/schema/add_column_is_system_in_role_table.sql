--liquibase formatted sql

--changeset postgres:add_column_is_system_in_role_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1468 Add column is_system in role table



ALTER TABLE security.role 
 ADD COLUMN is_system boolean NOT NULL DEFAULT false;

 