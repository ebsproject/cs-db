--liquibase formatted sql

--changeset postgres:add_column_recipient_to_shipment_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1548 Add column recipient_institution to shipment table



ALTER TABLE shm.shipment
 ADD COLUMN recipient_institution varchar (32) NULL;

 