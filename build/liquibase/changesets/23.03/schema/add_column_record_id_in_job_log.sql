--liquibase formatted sql

--changeset postgres:add_column_record_id_in_job_log context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1492 Add new field record_id in job_log table



ALTER TABLE core.job_log 
 ADD COLUMN record_id integer NULL;

 