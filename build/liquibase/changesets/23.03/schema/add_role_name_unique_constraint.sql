--liquibase formatted sql

--changeset postgres:add_role_name_unique_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1522 Add role.name unique constraint



ALTER TABLE security.role 
  ADD CONSTRAINT unique_role UNIQUE (name);
