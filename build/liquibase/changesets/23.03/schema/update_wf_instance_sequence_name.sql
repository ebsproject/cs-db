--liquibase formatted sql

--changeset postgres:update_wf_instance_sequence_name context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1464 Update wf_instance sequence name



ALTER SEQUENCE workflow.instance_id_seq RENAME TO wf_instance_id_seq;

ALTER TABLE workflow.wf_instance ALTER COLUMN id SET DEFAULT NEXTVAL(('workflow."wf_instance_id_seq"'::text)::regclass);