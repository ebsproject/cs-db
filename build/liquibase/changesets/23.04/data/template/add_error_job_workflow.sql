--liquibase formatted sql

--changeset postgres:add_error_job_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-880 Create new job workflow for Batch Edit




do $$
declare _a_id int;
declare _sma_id int;
declare _bm_id int;


begin

SELECT id from "security"."role" where name = 'Admin' INTO _a_id;
SELECT id from "security"."role" where name = 'SM Admin' INTO _sma_id;

SELECT id from core.product where name = 'Batch Manager' INTO _bm_id;


WITH _function AS (
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id)
    VALUES
        ('Upload Error', true, 'Upload Error', 1, _bm_id)
RETURNING id
)

    INSERT INTO "security".role_product_function
    (role_id, product_function_id)
    VALUES 
        (_a_id, (select id from _function)),
        (_sma_id, (select id from _function))
    ;

end $$;

-- Create job_workflow for Clean action in Batch Manager

INSERT INTO core.job_workflow
(id, job_type_id, product_function_id, translation_id, "name", description, tenant_id, creator_id)
VALUES(10, 1, (SELECT pf.id FROM "security".product_function pf 	
            inner join core.product p on p.id = pf.product_id
            where p.name= 'Batch Manager' AND pf.action = 'Upload Error'),
        1, 'Batch Manager', 'Upload Error Batch Notification', 1, 1);


SELECT setval('core.job_workflow_id_seq', (SELECT MAX(id) FROM core.job_workflow)); 