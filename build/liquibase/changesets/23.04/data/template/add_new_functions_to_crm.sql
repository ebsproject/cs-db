--liquibase formatted sql

--changeset postgres:add_new_functions_to_crm context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1596 Add new product functions to CRM



do $$
declare _admin_id int;
declare _cs_admin_id int;
declare _prod_id int;
declare temprow record;

begin

SELECT id from "security"."role" where name = 'Admin' INTO _admin_id;
SELECT id from "security"."role" where name = 'CS Admin' INTO _cs_admin_id;

SELECT id from core.product where name = 'CRM' INTO _prod_id;

WITH _new_unit AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('New_Unit', true, 'New_Unit', 1, _prod_id, true)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _new_unit)),
        (_cs_admin_id, (SELECT id FROM _new_unit));


WITH _manage_unit AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Manage_Unit', true, 'Manage_Unit', 1, _prod_id, true)
RETURNING id
)

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _manage_unit)),
        (_cs_admin_id, (SELECT id FROM _manage_unit));


end $$;
