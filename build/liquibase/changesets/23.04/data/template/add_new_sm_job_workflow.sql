--liquibase formatted sql

--changeset postgres:add_new_sm_job_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1327 Create new job workflow for Batch Edit



INSERT INTO core.job_workflow
(job_type_id, product_function_id, translation_id, "name", description, tenant_id, creator_id)
VALUES(1, (SELECT pf.id FROM "security".product_function pf 	
            inner join core.product p on p.id = pf.product_id
            where p.name= 'Batch Manager' AND pf.action = 'Modify'),
        1, 'Batch Manager', 'Edit Batch Notification', 1, 1);

