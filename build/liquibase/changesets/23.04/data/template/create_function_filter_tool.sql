--liquibase formatted sql

--changeset postgres:create_function_filter_tool context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1364 Create function filter_tool


do $$
declare _crm_prod_id int;
declare _um_prod_id int;

begin

SELECT id from core.product where name = 'CRM' INTO _crm_prod_id;
SELECT id from core.product where name = 'User Management' INTO _um_prod_id;


INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Filter_Toolbar', true, 'Filter_Foolbar', 1, _crm_prod_id, FALSE),
    ('Filter_Toolbar', true, 'Filter_Foolbar', 1, _um_prod_id, FALSE);    


end $$;
