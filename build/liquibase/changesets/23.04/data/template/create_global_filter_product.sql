--liquibase formatted sql

--changeset postgres:create_global_filter_product context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1595 Add product global_filter under Core System domain



INSERT INTO core.product
(name, description, help, main_entity, icon, creator_id, domain_id, menu_order, "path", htmltag_id)
VALUES('Global Filter', 'Global Filter', 'Global Filter', 'Filter', 'Global_Filter', 1, 
        (SELECT id FROM core.domain WHERE prefix = 'cs'), 
        (SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'cs')), 'cs/globalfilter', 1);


do $$
declare _a_id int;
declare _prod_id int;
declare temprow record;

begin

SELECT id from "security"."role" where name = 'Admin' INTO _a_id;

SELECT id from core.product where name = 'Global Filter' INTO _prod_id;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('View', true, 'View', 1, _prod_id, false),
    ('Read', true, 'Read', 1, _prod_id, false),
    ('Apply', true, 'Apply', 1, _prod_id, true),
    ('Save', true, 'Save', 1, _prod_id, true);


FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _prod_id
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_a_id, (select temprow.id));
END LOOP;

end $$;
