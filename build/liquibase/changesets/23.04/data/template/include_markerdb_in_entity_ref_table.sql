--liquibase formatted sql

--changeset postgres:include_markerdb_in_entity_ref_table context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1326 Incliude markerdb tables entity_reference



WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Marker_Group', 'code', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('modification_timestamp', 'modification_timestamp', 8, 1, (SELECT id FROM _entity)),
    ('type', 'type', 6, 1, (SELECT id FROM _entity)),
    ('name', 'name', 4, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('code', 'code', 3, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 9, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 7, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 11, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 10, 1, (SELECT id FROM _entity)),
    ('crop_id', 'crop_id', 2, 1, (SELECT id FROM _entity)),
    ('comments', 'comments', 5, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Allele', 'name', 'id', 'marker', 1, 1)
RETURNING id
)  

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('modifier_id', 'modifier_id', 9, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 7, 1, (SELECT id FROM _entity)),
    ('name', 'name', 3, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('status', 'status', 4, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 6, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 8, 1, (SELECT id FROM _entity)),
    ('comments', 'comments', 5, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 10, 1, (SELECT id FROM _entity)),
    ('marker_group_id', 'marker_group_id', 2, 1, (SELECT id FROM _entity));

    

WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Allele_Definition', 'id', 'id', 'marker', 1, 1)
RETURNING id
)  

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('creation_timestamp', 'creation_timestamp', 6, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 8, 1, (SELECT id FROM _entity)),
    ('allele_id', 'allele_id', 3, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 7, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 9, 1, (SELECT id FROM _entity)),
    ('call', 'call', 4, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('marker_id', 'marker_id', 2, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 10, 1, (SELECT id FROM _entity)),
    ('comments', 'comments', 5, 1, (SELECT id FROM _entity));

   

WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Marker', 'name', 'id', 'marker', 1, 1)
RETURNING id
)  

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('variant', 'variant', 6, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 8, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 9, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 7, 1, (SELECT id FROM _entity)),
    ('target', 'target', 3, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('source', 'source', 4, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 10, 1, (SELECT id FROM _entity)),
    ('name', 'name', 2, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 11, 1, (SELECT id FROM _entity)),
    ('category', 'category', 5, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Technology_Service_Provider', 'code', 'id', 'marker', 1, 1)
RETURNING id
)    

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('code', 'code', 4, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 7, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 9, 1, (SELECT id FROM _entity)),
    ('technology_id', 'technology_id', 3, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 8, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 6, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 5, 1, (SELECT id FROM _entity)),
    ('service_provider_id', 'service_provider_id', 2, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Assay', 'name', 'id', 'marker', 1, 1)
RETURNING id
)    

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('modifier_id', 'modifier_id', 11, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 12, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 10, 1, (SELECT id FROM _entity)),
    ('marker_id', 'marker_id', 2, 1, (SELECT id FROM _entity)),
    ('conditions', 'conditions', 5, 1, (SELECT id FROM _entity)),
    ('strand', 'strand', 6, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 8, 1, (SELECT id FROM _entity)),
    ('technology_service_provider_id', 'technology_service_provider_id', 3, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 9, 1, (SELECT id FROM _entity)),
    ('comments', 'comments', 7, 1, (SELECT id FROM _entity)),
    ('name', 'name', 4, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Marker_Group_Marker', 'code', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('marker_group_id', 'marker_group_id', 2, 1, (SELECT id FROM _entity)),
    ('marker_id', 'marker_id', 1, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Marker_Position', 'id', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('modification_timestamp', 'modification_timestamp', 7, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 9, 1, (SELECT id FROM _entity)),
    ('reference_genome_id', 'reference_genome_id', 3, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('position', 'position', 4, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 6, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 8, 1, (SELECT id FROM _entity)),
    ('chromosome_id', 'chromosome_id', 5, 1, (SELECT id FROM _entity)),
    ('marker_id', 'marker_id', 2, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 10, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Reference_Genome', 'name', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('creation_timestamp', 'creation_timestamp', 5, 1, (SELECT id FROM _entity)),
    ('name', 'name', 2, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('crop_id', 'crop_id', 4, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 9, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 7, 1, (SELECT id FROM _entity)),
    ('comments', 'comments', 3, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 8, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 6, 1, (SELECT id FROM _entity));

 

WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Chromosome', 'name', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('modification_timestamp', 'modification_timestamp', 5, 1, (SELECT id FROM _entity)),
    ('name', 'name', 2, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 7, 1, (SELECT id FROM _entity)),
    ('comments', 'comments', 3, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 4, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 6, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 8, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Oligo', 'primer_name', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('assay_id', 'assay_id', 2, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 10, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 11, 1, (SELECT id FROM _entity)),
    ('tm', 'tm', 5, 1, (SELECT id FROM _entity)),
    ('comments', 'comments', 6, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 8, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 7, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 9, 1, (SELECT id FROM _entity)),
    ('primer_name', 'primer_name', 3, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('primer_sequence', 'primer_sequence', 4, 1, (SELECT id FROM _entity));

  

WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
     ('Crop', 'name', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('is_void', 'is_void', 8, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 6, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 4, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('description', 'description', 3, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 7, 1, (SELECT id FROM _entity)),
    ('name', 'name', 2, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 5, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
    ('Service_Provider', 'code', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('code', 'code', 2, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 5, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 6, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 8, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 7, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 9, 1, (SELECT id FROM _entity)),
    ('name', 'name', 3, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('type', 'type', 4, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
    ('Technology', 'name', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('is_void', 'is_void', 9, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 7, 1, (SELECT id FROM _entity)),
    ('description', 'description', 3, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('density', 'density', 4, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 6, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 8, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 5, 1, (SELECT id FROM _entity)),
    ('name', 'name', 2, 1, (SELECT id FROM _entity));



WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
    ('Trait', 'code', 'id', 'marker', 1, 1)
RETURNING id
)
    
INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('crop_id', 'crop_id', 2, 1, (SELECT id FROM _entity)),
    ('modifier_id', 'modifier_id', 10, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 11, 1, (SELECT id FROM _entity)),
    ('description', 'description', 5, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 8, 1, (SELECT id FROM _entity)),
    ('category', 'category', 6, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity)),
    ('name', 'name', 4, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 7, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 9, 1, (SELECT id FROM _entity)),
    ('code', 'code', 3, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
    ('Trait_Marker_Group', '', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('trait_id', 'trait_id', 2, 1, (SELECT id FROM _entity)),
    ('marker_group_id', 'marker_group_id', 1, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
    ('Entity_Reference', 'id', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
    ('modifier_id', 'modifier_id', 11, 1, (SELECT id FROM _entity)),
    ('is_void', 'is_void', 12, 1, (SELECT id FROM _entity)),
    ('creator_id', 'creator_id', 10, 1, (SELECT id FROM _entity)),
    ('entity', 'entity', 2, 1, (SELECT id FROM _entity)),
    ('graphql', 'graphql', 13, 1, (SELECT id FROM _entity)),
    ('storefield', 'storefield', 5, 1, (SELECT id FROM _entity)),
    ('entity_schema', 'entity_schema', 6, 1, (SELECT id FROM _entity)),
    ('creation_timestamp', 'creation_timestamp', 8, 1, (SELECT id FROM _entity)),
    ('textfield', 'textfield', 3, 1, (SELECT id FROM _entity)),
    ('modification_timestamp', 'modification_timestamp', 9, 1, (SELECT id FROM _entity)),
    ('tenant_id', 'tenant_id', 7, 1, (SELECT id FROM _entity)),
    ('valuefield', 'valuefield', 4, 1, (SELECT id FROM _entity)),
    ('id', 'id', 1, 1, (SELECT id FROM _entity));


WITH _entity AS (
INSERT INTO core.entity_reference
(entity, textfield, valuefield, entity_schema, tenant_id, creator_id)
VALUES
    ('Attributes', 'name', 'id', 'marker', 1, 1)
RETURNING id
)

INSERT INTO core."attributes"
("name", description, sort_no, creator_id, entityreference_id)
VALUES
('att_component', 'att_component', 6, 1, (SELECT id FROM _entity)),
('is_required', 'is_required', 8, 1, (SELECT id FROM _entity)),
('modification_timestamp', 'modification_timestamp', 14, 1, (SELECT id FROM _entity)),
('description', 'description', 3, 1, (SELECT id FROM _entity)),
('is_multiline', 'is_multiline', 7, 1, (SELECT id FROM _entity)),
('default_value', 'default_value', 9, 1, (SELECT id FROM _entity)),
('help', 'help', 4, 1, (SELECT id FROM _entity)),
('modifier_id', 'modifier_id', 16, 1, (SELECT id FROM _entity)),
('id', 'id', 1, 1, (SELECT id FROM _entity)),
('is_void', 'is_void', 17, 1, (SELECT id FROM _entity)),
('md', 'md', 11, 1, (SELECT id FROM _entity)),
('name', 'name', 2, 1, (SELECT id FROM _entity)),
('sm', 'sm', 10, 1, (SELECT id FROM _entity)),
('lg', 'lg', 12, 1, (SELECT id FROM _entity)),
('entityreference_id', 'entityreference_id', 18, 1, (SELECT id FROM _entity)),
('htmltag_id', 'htmltag_id', 19, 1, (SELECT id FROM _entity)),
('creator_id', 'creator_id', 15, 1, (SELECT id FROM _entity)),
('sort_no', 'sort_no', 5, 1, (SELECT id FROM _entity)),
('creation_timestamp', 'creation_timestamp', 13, 1, (SELECT id FROM _entity));
