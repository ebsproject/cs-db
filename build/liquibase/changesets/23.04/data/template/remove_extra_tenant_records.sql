--liquibase formatted sql

--changeset postgres:remove_extra_tenant_records context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1550 Remove, extra tenant records



do $$
DECLARE temprow record;
BEGIN

FOR temprow IN SELECT user_id, tenant_id
    FROM 
        (SELECT user_id, tenant_id,
         ROW_NUMBER() OVER( PARTITION BY user_id
        ORDER BY  user_id ) AS row_num
        FROM "security".tenant_user ) t
        WHERE t.row_num > 1 
    LOOP    
        DELETE FROM "security".tenant_user
            WHERE user_id = temprow.user_id AND tenant_id = temprow.tenant_id;
    END LOOP;
END $$;

UPDATE "security".tenant_user
SET tenant_id= (SELECT id FROM core.tenant WHERE "name" = 'Default');


UPDATE core.tenant SET is_void = TRUE WHERE "name" <> 'Default';

