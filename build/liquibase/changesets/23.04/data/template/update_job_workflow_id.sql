--liquibase formatted sql

--changeset postgres:update_job_workflow_id context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1327 Update job workflow id for Batch Edit



do $$
declare _temp integer;
begin

SELECT id FROM core.job_workflow WHERE id = 9 INTO _temp;
IF NOT FOUND THEN
    UPDATE core.job_workflow
    SET id = 9 WHERE description = 'Edit Batch Notification';
END IF;

end $$;

SELECT setval('core.job_workflow_id_seq', (SELECT MAX(id) FROM core.job_workflow)); 