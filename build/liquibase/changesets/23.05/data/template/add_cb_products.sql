--liquibase formatted sql

--changeset postgres:add_cb_products context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1770 Add CB Products in CS Product table



INSERT INTO core.product
("name", description, help, main_entity, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
VALUES
('Data Collection', 'Facilitates the validation of uploaded trait measurements from the field and the committing of validated data to the experiment.', 'Data Collection', 'Data', '',  1, 
(select id from core.domain where prefix = 'cb'), 
(SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'cb')), 1, '/', 'DATA_COLLECTION');

INSERT INTO "security"."role"
(description, security_group, creator_id, "name")
VALUES
('Team Member', 'CB Users', 1, 'Team Member'),
('Technician', 'CB Users', 1, 'Technician'),
('Collaborator', 'CB Users', 1, 'Collaborator');


do $$
declare _tm int;
declare _tec int;
declare _coll int; 
declare _prod int;
declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Team Member' INTO _tm;
SELECT id FROM "security"."role" where name = 'Technician' INTO _tec;
SELECT id FROM "security"."role" where name = 'Collaborator' INTO _coll;
SELECT id FROM core.product where name = 'Data Collection' INTO _prod;

INSERT INTO "security".product_function
    ("action", system_type, description, creator_id, product_id)
VALUES
    ('Upload Data', true, 'Upload Data Collection file', 1, _prod),
    ('Quality Control Data', true, 'Conduct Quality Control', 1, _prod),
    ('Commit Transaction', true, 'Complete Data Collection transaction', 1, _prod),
    ('Download Transaction File', true, 'Dowload Data Collection transaction file', 1, _prod),
    ('Delete Transaction', true, 'Delete Data Collection transaction', 1, _prod);

FOR temprow IN
    SELECT id, "action" FROM "security".product_function where product_id = _prod
LOOP
    IF temprow.action = 'Upload Data' THEN
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_tm, temprow.id),
        (_tec, temprow.id),
        (_coll, temprow.id);
    
    ELSEIF temprow.action = 'Quality Control Data' THEN
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_tm, temprow.id),
        (_tec, temprow.id),
        (_coll, temprow.id);
    
    ELSEIF temprow.action = 'Download Transaction File' THEN
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_tm, temprow.id),
        (_tec, temprow.id),
        (_coll, temprow.id);

    ELSE
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_tm, temprow.id);
END IF;
END LOOP;
end $$