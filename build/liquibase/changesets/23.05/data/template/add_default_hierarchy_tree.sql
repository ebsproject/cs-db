--liquibase formatted sql

--changeset postgres:add_default_hierarchy_tree context:template splitStatements:false rollbackSplitStatements:false
--comment: Hot Fix Add default hierarchy tree



do $$
begin
PERFORM id FROM core.hierarchy_tree WHERE hierarchy_design_id = 0;
IF NOT FOUND THEN
    INSERT INTO core.hierarchy_tree
		(value, record_id, hierarchy_design_id, creator_id)
	VALUES
		('Default', 1, 0, 1);
END IF;
END $$
