--liquibase formatted sql

--changeset postgres:add_email_template_product context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1751 Add email_template product



INSERT INTO core.product
("name", description, help, main_entity, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
VALUES
    ('Email Template', 'Creation and management of templates for institutional Emails', 'Email Template', 'Email', '',  1, 
    (select id from core.domain where prefix = 'cs'), 
    (SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'cs')), 1, 'email-template', 'EMAIL_TEMPLATE')
    ;

do $$
declare _admin int;
declare _csadmin int;
declare _prod int;
declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'CS Admin' INTO _csadmin;
SELECT id FROM core.product where name = 'Email Template' INTO _prod;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Create', true, 'Create', 1, _prod, true),
    ('Modify', true, 'Modify', 1, _prod, true),
    ('Delete', true, 'Delete', 1, _prod, true),
    ('Export', true, 'Export', 1, _prod, false),
    ('Print', true, 'Print', 1, _prod, false),
    ('Search', true, 'Search', 1, _prod, false),
    ('Read', true, 'Read', 1, _prod, true);

FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _prod
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_admin, temprow.id),
        (_csadmin, temprow.id)
        ;
END LOOP;
end $$