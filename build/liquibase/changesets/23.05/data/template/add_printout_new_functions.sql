--liquibase formatted sql

--changeset postgres:add_printout_new_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1576 Add new functions to printout product



do $$
declare _admin int;
declare _cs_admin int;
declare _product int;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'CS Admin' INTO _cs_admin;
SELECT id FROM core.product where name = 'Printout' INTO _product;

WITH _function_delete as(
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Delete Connection', true, 'Delete Connection', 1, _product, false)
RETURNING id
)
        INSERT INTO "security".role_product_function (role_id, product_function_id)
	        VALUES
                (_admin, (select id FROM _function_delete)),
                (_cs_admin, (select id FROM _function_delete))
                ;


WITH _function_edit as(
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Edit Connection', true, 'Edit Connection', 1, _product, false)
RETURNING id
)
        INSERT INTO "security".role_product_function (role_id, product_function_id)
	        VALUES
                (_admin, (select id FROM _function_edit)),
                (_cs_admin, (select id FROM _function_edit))
                ;

end $$
