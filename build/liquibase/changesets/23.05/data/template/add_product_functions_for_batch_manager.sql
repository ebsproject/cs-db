--liquibase formatted sql

--changeset postgres:add_product_functions_for_batch_manager context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1428 Create product functions for Batch Manager



do $$
declare _admin int;
declare _dm int;
declare _mb int;
declare _lm int;
declare _batch int;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'Data Manager' INTO _dm;
SELECT id FROM "security"."role" where name = 'Molecular Breeder' INTO _mb;
SELECT id FROM "security"."role" where name = 'Lab Manager' INTO _lm;
SELECT id FROM core.product where name = 'Batch Manager' INTO _batch;

WITH _report as(
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Download Vendor Report', true, 'Download Vendor Report', 1, _batch, false)
RETURNING id
)
        INSERT INTO "security".role_product_function (role_id, product_function_id)
	        VALUES
                (_admin, (select id FROM _report)),
                (_dm, (select id FROM _report)),
                (_mb, (select id FROM _report)),
                (_lm, (select id FROM _report))
                ;


WITH _report as(
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Download Genotyping File', true, 'Download Genotyping File', 1, _batch, false)
RETURNING id
)
        INSERT INTO "security".role_product_function (role_id, product_function_id)
	        VALUES
                (_admin, (select id FROM _report)),
                (_dm, (select id FROM _report)),
                (_mb, (select id FROM _report)),
                (_lm, (select id FROM _report))
                ;

end $$