--liquibase formatted sql

--changeset postgres:add_wf_products context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1749 Add Workflow Products



INSERT INTO core.product
("name", description, help, main_entity, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
VALUES
    ('WF Designer', '', 'WF Designer', 'Workflow', '',  1, 
    (select id from core.domain where prefix = 'cs'), 
    (SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'cs')), 1, 'designer', 'WF_DESIGNER'),
    ('WF Request', '', 'WF Request', 'Request', '',  1, 
    (select id from core.domain where prefix = 'sm'), 
    (SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'sm')), 1, 'request', 'WF_REQUEST')
;

do $$
declare _admin int;
declare _designer int;
declare _request int;
declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM core.product where name = 'WF Designer' INTO _designer;
SELECT id FROM core.product where name = 'WF Request' INTO _request;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Create', true, 'Create', 1, _designer, true),
    ('Modify', true, 'Modify', 1, _designer, true),
    ('Delete', true, 'Delete', 1, _designer, true),
    ('Export', true, 'Export', 1, _designer, false),
    ('Print', true, 'Print', 1, _designer, false),
    ('Search', true, 'Search', 1, _designer, false),
    ('Read', true, 'Read', 1, _designer, true),
    
    ('Create', true, 'Create', 1, _request, true),
    ('Modify', true, 'Modify', 1, _request, true),
    ('Delete', true, 'Delete', 1, _request, true),
    ('Export', true, 'Export', 1, _request, false),
    ('Print', true, 'Print', 1, _request, false),
    ('Search', true, 'Search', 1, _request, false),
    ('Read', true, 'Read', 1, _request, true)
    ;

FOR temprow IN
    SELECT id FROM "security".product_function where product_id IN (_request, _designer) 
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_admin, temprow.id);
END LOOP;
end $$