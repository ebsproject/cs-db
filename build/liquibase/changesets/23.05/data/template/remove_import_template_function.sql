--liquibase formatted sql

--changeset postgres:remove_import_template_function context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1752 Remove import_template function from CS Domain



do $$
declare _temp_p int;
declare _temp_i int;

begin
select id from "security".product_function where description = 'Import_Template_Institution' INTO _temp_i;
select id from "security".product_function where description = 'Import_Template_People' INTO _temp_p;

DELETE FROM "security".role_product_function
WHERE product_function_id=_temp_i;

DELETE FROM "security".role_product_function
WHERE product_function_id=_temp_p;

end $$