--liquibase formatted sql

--changeset postgres:remove_service_catalog_product context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1416 Remove Service Catalog product and functions



UPDATE core.product
SET is_void = true
WHERE name='Service Catalog';


DELETE FROM "security".role_product_function
WHERE product_function_id IN 
	(SELECT id FROM "security".product_function WHERE product_id = (SELECT id FROM core.product WHERE name = 'Service Catalog'));


DELETE FROM "security".product_function 
    WHERE product_id = (SELECT id FROM core.product WHERE name = 'Service Catalog');