--liquibase formatted sql

--changeset postgres:update_cb_functions_name context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1770 Update CB Product actions name



UPDATE "security".product_function
    SET "action" = 'UPLOAD_DATA' WHERE "action" = 'Upload Data';

UPDATE "security".product_function
    SET "action" = 'QUALITY_CONTROL_DATA' WHERE "action" = 'Quality Control Data';

UPDATE "security".product_function
    SET "action" = 'COMMIT_TRANSACTION' WHERE "action" = 'Commit Transaction';

UPDATE "security".product_function
    SET "action" = 'DOWNLOAD_TRANSACTION_FILE' WHERE "action" = 'Download Transaction File';

UPDATE "security".product_function
    SET "action" = 'DELETE_TRANSACTION' WHERE "action" = 'Delete Transaction';
