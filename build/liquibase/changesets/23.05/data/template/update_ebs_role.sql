--liquibase formatted sql

--changeset postgres:update_ebs_role context:template splitStatements:false rollbackSplitStatements:false
--comment: Hot Fix Update role for ebs user



UPDATE "security".user_role
SET role_id = (SELECT id FROM "security"."role" WHERE "name" = 'Admin')
WHERE user_id=1;

