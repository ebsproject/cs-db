--liquibase formatted sql

--changeset postgres:update_sm_role_permissions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1416 Review role permissions for SM Domain




DELETE FROM "security".role_product_function
WHERE product_function_id  IN  
(SELECT id FROM "security".product_function WHERE product_id IN (SELECT id FROM core.product WHERE "name" = 'MarkerDb' OR "name" = 'GIGWA'))
AND role_id = (SELECT id FROM "security".role WHERE "name" = 'Breeder');
