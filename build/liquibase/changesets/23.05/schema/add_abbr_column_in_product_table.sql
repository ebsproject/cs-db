--liquibase formatted sql

--changeset postgres:add_abbr_column_in_product_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1769 Add new column abbreviation in Product table



ALTER TABLE core.product 
 ADD COLUMN IF NOT EXISTS abbreviation varchar(50) NULL;

COMMENT ON COLUMN core.product.abbreviation
	IS 'Short name to identify a Product'
; 
