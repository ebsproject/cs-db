--liquibase formatted sql

--changeset postgres:add_column_sync_in_tenant_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1628 Add new column sync in Tenant table



ALTER TABLE core.tenant 
 ADD COLUMN IF NOT EXISTS sync boolean NULL DEFAULT FALSE;