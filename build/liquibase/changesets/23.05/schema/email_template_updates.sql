--liquibase formatted sql

--changeset postgres:email_template_updates context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1751 Update email_template model



ALTER TABLE core.email_template 
 DROP COLUMN IF EXISTS tenant_id;

 CREATE TABLE core.tenant_email_template
(
	email_template_id integer NOT NULL,
	tenant_id integer NOT NULL
)
;

ALTER TABLE core.tenant_email_template ADD CONSTRAINT "PK_tenant_email_template"
	PRIMARY KEY (email_template_id,tenant_id)
;

ALTER TABLE core.tenant_email_template ADD CONSTRAINT "FK_tenant_email_template_email_template"
	FOREIGN KEY (email_template_id) REFERENCES core.email_template (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.tenant_email_template ADD CONSTRAINT "FK_tenant_email_template_tenant"
	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action
;

