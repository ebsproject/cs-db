--liquibase formatted sql

--changeset postgres:increase_parentage_column_lenght context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1363 Update parentage column lenght



ALTER TABLE shm.shipment_item 
 ALTER COLUMN parentage TYPE text;
