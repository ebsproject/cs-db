--liquibase formatted sql

--changeset postgres:update_template_column_lenght context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1751 Update core.email_template column lenght



ALTER TABLE core.email_template 
    ALTER COLUMN "template" TYPE varchar(5000) USING "template"::varchar;
