--liquibase formatted sql

--changeset postgres:update_template_column_type context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1751 Update core.email_template column type



ALTER TABLE core.email_template 
    ALTER COLUMN "template" TYPE text;
