--liquibase formatted sql

--changeset postgres:add_node_cf_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1809 Add node_cf data for service workflow



DO $$
DECLARE _cf_type integer;
DECLARE _node_id integer;
DECLARE _ref integer;

BEGIN

SELECT id FROM workflow.cf_type WHERE "name" = 'TextField' INTO _cf_type;
SELECT id FROM workflow.node WHERE "name" = 'Fill out required inputs' INTO _node_id;
SELECT id FROM core.entity_reference WHERE entity = 'Service' and entity_schema = 'workflow' INTO _ref;

PERFORM id FROM workflow.node_cf WHERE name = 'submition_date';
IF NOT FOUND THEN
   INSERT INTO workflow.node_cf
        (name, description, help, required, tenant_id, creator_id, cftype_id, node_id, api_attributes_name, attributes_id)
    VALUES
        ('submition_date', 'Submition Date', 'Submition Date', true, 1, 1, _cf_type, _node_id, 'submitionDate', (SELECT id FROM core.attributes WHERE name = 'submition_date' and entityreference_id = _ref));
END IF;

PERFORM id FROM workflow.node_cf WHERE name = 'charge_account';
IF NOT FOUND THEN
   INSERT INTO workflow.node_cf
        (name, description, help, required, tenant_id, creator_id, cftype_id, node_id, api_attributes_name, attributes_id)
    VALUES
        ('charge_account', 'Charge Account', 'Charge Account', true, 1, 1, _cf_type, _node_id, 'chargeAccount', (SELECT id FROM core.attributes WHERE name = 'charge_account' and entityreference_id = _ref));
END IF;

PERFORM id FROM workflow.node_cf WHERE name = 'admin_contact';
IF NOT FOUND THEN
   INSERT INTO workflow.node_cf
        (name, description, help, required, tenant_id, creator_id, cftype_id, node_id, api_attributes_name, attributes_id)
    VALUES
        ('admin_contact', 'Admin Contact', 'Admin Contact', true, 1, 1, _cf_type, _node_id, 'adminContact', (SELECT id FROM core.attributes WHERE name = 'admin_contact' and entityreference_id = _ref));
END IF;

PERFORM id FROM workflow.node_cf WHERE name = 'request_code';
IF NOT FOUND THEN
   INSERT INTO workflow.node_cf
        (name, description, help, required, tenant_id, creator_id, cftype_id, node_id, api_attributes_name, attributes_id)
    VALUES
        ('request_code', 'Request Code', 'Request Code', true, 1, 1, _cf_type, _node_id, 'requestCode', (SELECT id FROM core.attributes WHERE name = 'request_code' and entityreference_id = _ref));
END IF;

PERFORM id FROM workflow.node_cf WHERE name = 'description';
IF NOT FOUND THEN
   INSERT INTO workflow.node_cf
        (name, description, help, required, tenant_id, creator_id, cftype_id, node_id, api_attributes_name, attributes_id)
    VALUES
        ('description', 'Description', 'Description', true, 1, 1, _cf_type, _node_id, 'description', (SELECT id FROM core.attributes WHERE name = 'description' and entityreference_id = _ref));
END IF;

PERFORM id FROM workflow.node_cf WHERE name = 'requester';
IF NOT FOUND THEN
   INSERT INTO workflow.node_cf
        (name, description, help, required, tenant_id, creator_id, cftype_id, node_id, api_attributes_name, attributes_id)
    VALUES
        ('requester', 'Requester', 'Requester', true, 1, 1, _cf_type, _node_id, 'requester', (SELECT id FROM core.attributes WHERE name = 'requester' and entityreference_id = _ref));
END IF;

END $$