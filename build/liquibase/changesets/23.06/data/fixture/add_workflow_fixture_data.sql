--liquibase formatted sql

--changeset postgres:add_workflow_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update Workflow Model - Add fixture data



do $$
declare _es integer;
declare _en integer;

BEGIN
INSERT INTO core.html_tag
    (tag_name, creator_id)
VALUES
    ('workflow.outgoing.name', 1),
    ('workflow.incoming.name', 1),
    ('workflow.phase.creation', 1),
    ('workflow.phase.processing', 1),
    ('workflow.phase.closing', 1),
    ('workflow.stage.create_request', 1),
    ('workflow.node.fillout', 1);

SELECT id FROM core.language WHERE "name" = 'Spanish' INTO _es;
SELECT id FROM core.language WHERE "name" = 'English' INTO _en;


INSERT INTO core."translation"
    ("translation", language_to, creator_id, htmltag_id, language_id)
VALUES
    ('Salida de Semilla', _es, 1, (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.outgoing.name'), _en),
    ('Introduccion de Semilla', _es, 1, (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.incoming.name'), _en),
    ('En Creacion', _es, 1, (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.phase.creation'), _en),    
    ('En Proceso', _es, 1, (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.phase.processing'), _en),
    ('Terminacion', _es, 1, (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.phase.closing'), _en),
    ('Crear Solicitud', _es, 1, (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.stage.create_request'), _en),
    ('Llenar campos requeridos', _es, 1, (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.node.fillout'), _en)
    ;

UPDATE workflow.workflow
    SET title='Outgoing Seed', "name"='Outgoing Seed', description='Outgoing Seed', help='Outgoing Seed', sort_no=1, icon='', 
        htmltag_id= (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.outgoing.name'), navigation_name='Outgoing Seed', 
        product_id= (SELECT id FROM core.product WHERE "name" = 'Shipment Manager'), "sequence"=NULL, show_menu=true, is_system=true, security_definition=NULL
    WHERE title='Shipment Seed Material';


INSERT INTO workflow.workflow
    (title, "name", description, help, sort_no, icon, creator_id, htmltag_id, navigation_name, product_id, "sequence", show_menu, is_system, security_definition)
    VALUES
        ('Incoming Seed', 'Incoming Seed', 'Incoming shipment is a workflow where a program receives seeds from an external organization, 
        such as a partner institute or a private group.', 'Incoming Seed', 2, NULL, 1, 
        (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.incoming.name'), 'Incoming Seed', 
        (SELECT id FROM core.product WHERE abbreviation = 'REQUEST_NO_GSR'), NULL, true, true, NULL);


INSERT INTO workflow.wf_view_type
("name", notes, tenant_id, creator_id, is_void)
VALUES
('N/A', NULL, 1, 1, false),
('Form', NULL, 1, 1, false),
('TabHorizontal', NULL, 1, 1, false),
('TabVertical', NULL, 1, 1, false),
('Simple', NULL, 1, 1, false);


INSERT INTO workflow.phase
    ("name", description, help, "sequence", tenant_id, creator_id, htmltag_id, workflow_id, depend_on, icon, wf_view_type_id)
VALUES
    ('Creation', 'In Creation process', 'All steps for creation', 1, 1, 1, 
    (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.phase.creation'),
    (SELECT id FROM workflow.workflow WHERE "name" = 'Incoming Seed'), NULL, NULL, 
    (SELECT id FROM workflow.wf_view_type WHERE "name" = 'N/A')),
    
    ('Processing', 'In Process', 'In Process', 2, 1, 1, 
    (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.phase.creation'), 
    (SELECT id FROM workflow.workflow WHERE "name" = 'Incoming Seed'), NULL, NULL, 
    (SELECT id FROM workflow.wf_view_type WHERE "name" = 'N/A')),

    ('Closing', 'In Closing process', 'In Closing process', 3, 1, 1, 
    (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.phase.closing'), 
    (SELECT id FROM workflow.workflow WHERE "name" = 'Incoming Seed'), NULL, NULL, 
    (SELECT id FROM workflow.wf_view_type WHERE "name" = 'N/A'));


INSERT INTO workflow.stage
    ("name", description, help, "sequence", tenant_id, creator_id, htmltag_id, phase_id, depend_on, icon, wf_view_type_id)
VALUES
    ('Create Request', 'Create Request', 'Create Request', 1, 1, 1,
    (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.stage.create_request'), 
    (SELECT id FROM workflow.phase WHERE "name" = 'Creation'), NULL, NULL,
    (SELECT id FROM workflow.wf_view_type WHERE "name" = 'TabVertical'));


INSERT INTO workflow.node
    ("name", description, help, "sequence", require_approval, tenant_id, creator_id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id)
VALUES
    ('Fill out required inputs', 'Fill in required inputs', 'Fill in required inputs', 1, false, 1, 1, 
    (SELECT id FROM core.html_tag WHERE tag_name = 'workflow.node.fillout'), NULL, NULL, NULL, 
    '{}', '{}', NULL, '[{"message": "successfull"}]', '{}', '{"roles": [], "users": []}', '{}', 'javascript', 
    (SELECT id FROM workflow.wf_view_type WHERE "name" = 'Form'));

end $$