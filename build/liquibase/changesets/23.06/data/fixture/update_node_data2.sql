--liquibase formatted sql

--changeset postgres:update_node_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1809 Update node_type_id and process_id



UPDATE workflow.node
SET process_id= (SELECT id FROM core.process WHERE code = 'CHANGE'), 
    node_type_id = (SELECT id FROM workflow.node_type WHERE "name" = 'rowAction') WHERE "name" = 'Submit';

UPDATE workflow.node
SET process_id= (SELECT id FROM core.process WHERE code = 'FORM'),
    node_type_id = (SELECT id FROM workflow.node_type WHERE "name" = 'form') WHERE "name" = 'Fill out required inputs';

