--liquibase formatted sql

--changeset postgres:update_node_type_id context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1809 Update node_type_id


UPDATE workflow.node
    SET node_type_id = 1;