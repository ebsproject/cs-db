--liquibase formatted sql

--changeset postgres:update_wf_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update Workflow fixture data


do $$

declare _cf_type integer;
declare _node integer;

begin

UPDATE workflow.workflow
    SET show_menu = FALSE
WHERE title ='Outgoing Seed';


INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES((SELECT id FROM workflow.stage WHERE "name" = 'Create Request'), (SELECT id FROM workflow.node WHERE "name" = 'Fill out required inputs'));

SELECT id FROM workflow.cf_type WHERE "name" = 'TextField' INTO _cf_type;
SELECT id FROM workflow.node WHERE "name" = 'Fill out required inputs' INTO _node;

INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creator_id, cftype_id, htmltag_id, node_id, field_attributes)
VALUES
    ('Country of origin', 'The name of the country from which the shipment originates from', 'Country of origin', TRUE, 1, 1, _cf_type, NULL, _node, NULL),
    ('Sender organization', 'The name of the organization from which the shipment originates from', 'Sender organization', TRUE, 1, 1, _cf_type, NULL, _node, NULL),
    ('Sender contact person', 'The name of the contact person from the organization from which the shipment originates from', 'Sender contact person', TRUE, 1, 1, _cf_type, NULL, _node, NULL),
    ('Recipient', 'The name of the person who will receive the materials after shipment process has been completed', 'Recipient', TRUE, 1, 1, _cf_type, NULL, _node, NULL),
    ('Program', 'The name of the program that will receive the material', 'Program', TRUE, 1, 1, _cf_type, NULL, _node, NULL),
    ('Notes', 'Notes regarding the shipment', 'Notes', FALSE, 1, 1, _cf_type, NULL, _node, NULL)   
 ;

INSERT INTO core.process
("name", description, code, is_background, db_function, call_report, "path", tenant_id, creator_id)
VALUES
('Generate Form', 'Generate Form', 'FORM', false, '', false, '', 1, 1);

UPDATE workflow.node SET process_id = (SELECT id from core.process WHERE code = 'FROM');

END $$
