--liquibase formatted sql

--changeset postgres:update_workflow_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update Workflow data



INSERT INTO core.process
    ("name", description, code, is_background, call_report, "path", tenant_id, creator_id)
VALUES
    ('Change Status', 'Change Status', 'CHANGE', true, false, '/change-status', 1, 1),
    ('Send Email', 'Send Email', 'EMAIL', true, false, '/send-email', 1, 1),
    ('Send Notification', 'Send Notification', 'NOTIFICATION', true, false, '/send-notification', 1, 1);

INSERT INTO workflow.node
    ("name", description, help, "sequence", require_approval, tenant_id, creator_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id)
VALUES
    ('Submit', 'Submit', 'Submit', 2, false, 1, 1, (SELECT id FROM core.process WHERE code = 'CHANGE'),
    '{}', '{}', 'publish', '[{"message": "successfull"}]', '{"api": "/change-status", "value": {"id": 2, "name": "Submitted"}}', 
    '{"roles": [{"id": "5", "name": "CS Admin", "isSystem": false, "description": "Core System Administrator"}], "users": [{"id": "106", "contact": {"id": 106, "person": {"givenName": "Developer", "familyName": "Developer"}}, "userName": "e.briones@cimmyt.org"}], "program": [{"id": 102, "name": "BW Wheat Breeding Program"}]}', 
    '{}', 'javascript', (SELECT id FROM workflow.wf_view_type WHERE "name" = 'N/A'), 
    (SELECT id FROM workflow.node_type WHERE "name" = 'rowAction'));

UPDATE workflow.node
SET  process_id= (SELECT id FROM core.process WHERE code = 'FORM') WHERE "name" = 'Fill out required inputs';

UPDATE workflow.node_cf
SET cftype_id=(SELECT id FROM workflow.cf_type WHERE "name" = 'select'), field_attributes='{"sizes": [12, 12, 12, 12, 12], "entity": "Country", "apiContent": [{"accessor": "id"}, {"accessor": "name"}]}'
WHERE "name"='Country of origin';

UPDATE workflow.node_cf
SET cftype_id= (SELECT id FROM workflow.cf_type WHERE "name" = 'select'), field_attributes='{"sizes": [12, 12, 12, 12, 12], "entity": "Contact", "filters": {"col": "category.name", "mod": "EQ", "val": "Institution"}, "apiContent": [{"accessor": "id"}, {"accessor": "institution.commonName"}, {"accessor": "institution.legalName"}]}'
WHERE "name"='Sender organization';

UPDATE workflow.node_cf
SET cftype_id= (SELECT id FROM workflow.cf_type WHERE "name" = 'select'), field_attributes='{"sizes": [12, 12, 12, 12, 12], "entity": "Contact", "filters": {"col": "category.name", "mod": "EQ", "val": "Person"}, "apiContent": [{"accessor": "id"}, {"accessor": "person.givenName"}, {"accessor": "person.familyName"}]}'
WHERE "name"='Sender contact person';

UPDATE workflow.node_cf
SET cftype_id= (SELECT id FROM workflow.cf_type WHERE "name" = 'select'), field_attributes='{"sizes": [12, 12, 12, 12, 12], "entity": "Contact", "filters": {"col": "category.name", "mod": "EQ", "val": "Person"}, "apiContent": [{"accessor": "id"}, {"accessor": "person.givenName"}, {"accessor": "person.familyName"}]}'
WHERE "name"='Recipient';

UPDATE workflow.node_cf
SET cftype_id= (SELECT id FROM workflow.cf_type WHERE "name" = 'select'), field_attributes='{"sizes": [12, 12, 12, 12, 12], "entity": "Program", "apiContent": [{"accessor": "id"}, {"accessor": "name"}]}'
WHERE "name"='Program';

UPDATE workflow.node_cf
SET field_attributes='{"sizes": [12, 12, 12, 12, 12]}'
WHERE "name"='Notes';

UPDATE workflow.node_cf
SET cftype_id= (SELECT id FROM workflow.cf_type WHERE "name" = 'datepicker'), field_attributes='{"sizes": [12, 12, 12, 12, 12]}'
WHERE "name"='submition_date';

UPDATE workflow.node_cf
SET field_attributes='{"sizes": [12, 12, 12, 12, 12]}'
WHERE "name"='charge_account';

UPDATE workflow.node_cf
SET field_attributes='{"sizes": [12, 12, 12, 12, 12]}'
WHERE "name"='admin_contact';

UPDATE workflow.node_cf
SET field_attributes='{"sizes": [12, 12, 12, 12, 12]}'
WHERE "name"='request_code';

UPDATE workflow.node_cf
SET field_attributes='{"sizes": [12, 12, 12, 12, 12]}'
WHERE "name"='description';

UPDATE workflow.node_cf
SET field_attributes='{"sizes": [12, 12, 12, 12, 12]}'
WHERE "name"='requester';


INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES((SELECT id FROM workflow.stage WHERE "name" = 'Create Request'), (SELECT id FROM workflow.node WHERE "name" = 'Submit'));

UPDATE workflow.node_type
SET "name"='process', description='Process'
WHERE "name" = 'backend';

INSERT INTO workflow.status_type
    ("name", description, help, tenant_id, creator_id, workflow_id)
VALUES
    ('Created', 'Created', 'Created', 1, 1, (SELECT id FROM workflow.workflow WHERE "name" = 'Incoming Seed')),
    ('Submited', 'Submited', 'Submited', 1, 1, (SELECT id FROM workflow.workflow WHERE "name" = 'Incoming Seed'));

INSERT INTO workflow.tenant_workflow
    (workflow_id, tenant_id)
VALUES
    ((SELECT id FROM workflow.workflow WHERE "name" = 'Incoming Seed'), (SELECT id FROM core.tenant WHERE "name" = 'Default')),
    ((SELECT id FROM workflow.workflow WHERE "name" = 'Outgoing Seed'), (SELECT id FROM core.tenant WHERE "name" = 'Default'));
