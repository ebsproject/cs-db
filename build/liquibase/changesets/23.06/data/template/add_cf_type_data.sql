--liquibase formatted sql

--changeset postgres:add_cf_type_data context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1748 Add cf_type values



INSERT INTO workflow.cf_type
    ("name", description, "type", creator_id)
VALUES
    ('TextField', 'TextField', 'String', 1),
    ('checkbox', 'CheckBox', 'Boolean', 1),
    ('datepicker', 'DatePicker', 'Date', 1),
    ('radiogroup', 'RadioGroup', 'String', 1),
    ('select', 'Select', 'Integer', 1),
    ('switch', 'Switch', 'Boolean', 1),
    ('button', 'Button', 'Submit', 1),
    ('file', 'File', 'File', 1)
    ;
