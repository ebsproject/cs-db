--liquibase formatted sql

--changeset postgres:add_data_to_node_type_table context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1823 Add template data to node_type table


INSERT INTO workflow.node_type
    ("name", description, creator_id)
VALUES
    ('form', 'Form', 1),
    ('rowAction', 'Row Action', 1),
    ('toolbarAction', 'Toolbar Action', 1),
    ('backend', 'Backend', 1),
    ('submit', 'Submit', 1);
