--liquibase formatted sql

--changeset postgres:add_place_hierarchy_design context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1748 Add Place Manager Product



WITH _hierarchy_type AS (  
    INSERT INTO core.hierarchy_type
        ("name", description, creator_id)
    VALUES('Place', 'Place Manager', 1)
RETURNING id
)

INSERT INTO core."hierarchy"
    ("name", description, hierarchy_type_id, is_system, creator_id)
VALUES('Place Manager', 'Place Manager', (select id from _hierarchy_type), true, 1);


WITH _site AS (
    INSERT INTO core.hierarchy_design
        ("name", "level", "number", hierarchy_id, is_required, creator_id)
    VALUES
 
        ('Site', 0, 1, (SELECT id FROM core.hierarchy WHERE "name" = 'Place Manager'), true, 1)
RETURNING id
),

_field AS (
    INSERT INTO core.hierarchy_design
        ("name", "level", "number", parent_id, hierarchy_id, is_required, creator_id)
    VALUES
        ('Field', 1, 1, (SELECT id FROM _site), (SELECT id FROM core.hierarchy WHERE "name" = 'Place Manager'), FALSE, 1)
RETURNING id
)

    INSERT INTO core.hierarchy_design
        ("name", "level", "number", parent_id, hierarchy_id, is_required, creator_id)
    VALUES
        ('Planting Area', 2, 1, (SELECT id FROM _field), (SELECT id FROM core.hierarchy WHERE "name" = 'Place Manager'), FALSE, 1);


