--liquibase formatted sql

--changeset postgres:add_place_hierarchy_levels context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1823 Add new hierarchy leves for Place Manager



INSERT INTO workflow.cf_type
("name", description, help, "type", creator_id)
VALUES
('map', 'Map', NULL, 'Map', 1);


WITH _fac AS (
    INSERT INTO core.hierarchy_design
        ("name", "level", "number", parent_id, hierarchy_id, is_required, creator_id)
    VALUES
        ('Facility', 1, 1, (SELECT id FROM core.hierarchy_design WHERE "name" = 'Site'), (SELECT id FROM core.hierarchy WHERE "name" = 'Place Manager'), FALSE, 1)
RETURNING id
),

_sub AS (
    INSERT INTO core.hierarchy_design
        ("name", "level", "number", parent_id, hierarchy_id, is_required, creator_id)
    VALUES
        ('Sub Facility', 2, 1, (SELECT id FROM _fac), (SELECT id FROM core.hierarchy WHERE "name" = 'Place Manager'), FALSE, 1)
RETURNING id
)

    INSERT INTO core.hierarchy_design
        ("name", "level", "number", parent_id, hierarchy_id, is_required, creator_id)
    VALUES
        ('Container', 3, 1, (SELECT id FROM _sub), (SELECT id FROM core.hierarchy WHERE "name" = 'Place Manager'), FALSE, 1);


INSERT INTO core.hierarchy_design_attributes
    (cf_type_id, value, hierarchy_design_id, creator_id)
VALUES
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'map'), 'Coordinates', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Field'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'map'), 'Coordinates', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Planting Area'), 1),    

    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Country', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Site'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Name', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Site'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Code', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Site'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Type', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Site'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'map'), 'Coordinates', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Site'), 1),

    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Code', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Facility'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Type', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Facility'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'map'), 'Coordinates', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Facility'), 1),

    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Code', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Sub Facility'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Type', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Sub Facility'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'map'), 'Coordinates', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Sub Facility'), 1),
    
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Code', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Container'), 1),
    ((SELECT id FROM workflow.cf_type WHERE "name" = 'TextField'), 'Type', (SELECT id FROM core.hierarchy_design WHERE "name" = 'Container'), 1)
    ;

