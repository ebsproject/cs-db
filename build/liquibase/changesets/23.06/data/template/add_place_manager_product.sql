--liquibase formatted sql

--changeset postgres:add_place_manager_product context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1748 Add Place Manager Product



INSERT INTO core.product
("name", description, help, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
VALUES
    ('Place Manager', '', 'Place Manager', '', 1, 
    (select id from core.domain where prefix = 'cs'), 
    (SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'cs')), 1, 'place-manager', 'PLACE_MANAGER')
    ;

do $$
declare _admin int;
declare _prod int;
declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM core.product where name = 'Place Manager' INTO _prod;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Create', true, 'Create', 1, _prod, true),
    ('Modify', true, 'Modify', 1, _prod, true),
    ('Delete', true, 'Delete', 1, _prod, true),
    ('Export', true, 'Export', 1, _prod, false),
    ('Print', true, 'Print', 1, _prod, false),
    ('Search', true, 'Search', 1, _prod, false),
    ('Read', true, 'Read', 1, _prod, true);
    

FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _prod
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_admin, temprow.id);
END LOOP;
end $$