--liquibase formatted sql

--changeset postgres:add_settings_product context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1561 Add Settings as Product under SM domain



INSERT INTO core.product
("name", description, help, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
VALUES
    ('Settings', 'Settings', 'Settings', '',  1, 
    (select id from core.domain where prefix = 'sm'), 
    (SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'sm')), 1, 'settings', 'SETTINGS')
    ;

do $$
declare _admin int;
declare _sm_admin int;
declare _settings int;
declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'SM Admin' INTO _sm_admin;
SELECT id FROM core.product where name = 'Settings' INTO _settings;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Create', true, 'Create', 1, _settings, true),
    ('Modify', true, 'Modify', 1, _settings, true),
    ('Delete', true, 'Delete', 1, _settings, true),
    ('Export', true, 'Export', 1, _settings, false),
    ('Print', true, 'Print', 1, _settings, false),
    ('Search', true, 'Search', 1, _settings, false),
    ('Read', true, 'Read', 1, _settings, true);

FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _settings
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_admin, temprow.id),
        (_sm_admin, temprow.id);
END LOOP;
end $$