--liquibase formatted sql

--changeset postgres:add_new_process_and_node_type context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Add new process and node_type



UPDATE workflow.node SET process_id = NULL;
UPDATE workflow.node SET node_type_id = NULL;

DELETE FROM core.process;
DELETE FROM workflow.node_type;

UPDATE workflow.node_type
SET "name"='process', description='Process'
WHERE "name" = 'backend';

INSERT INTO core.process
(id, "name", description, code, is_background, call_report, tenant_id, creator_id)
VALUES
(1, 'Generate Form', 'Generate Form','FORM', FALSE, FALSE, 1, 1),
(3, 'Print Preview', 'Print Preview', 'PP', FALSE,	TRUE, 1, 1),
(4, 'Send Email', 'Send Email',	'EMAIL', FALSE, FALSE, 1, 1),
(5, 'Change Status', 'Change Status', 'CHANGE', FALSE,false, 1, 1),
(6, 'Move', 'Move Phase/Stage/Node', 'MOVE', FALSE, FALSE, 1, 1),
(7, 'Send Notification', 'Send Notification', 'NOTIFICATION', TRUE, FALSE, 1, 1);


INSERT INTO workflow.node_type
    (id, "name", description, creator_id)
VALUES
    (1, 'form', 'Form', 1),
    (2, 'rowAction', 'Row Action', 1),
    (3, 'toolbarAction', 'Toolbar Action', 1),
    (4, 'process', 'Process', 1),
    (5, 'submit', 'Submit', 1),
    (6, 'approval', 'Approval Process', 1);

    
SELECT setval('core.process_id_seq', (SELECT MAX(id) FROM core.process));
SELECT setval('workflow.node_type_id_seq', (SELECT MAX(id) FROM workflow.node_type));
