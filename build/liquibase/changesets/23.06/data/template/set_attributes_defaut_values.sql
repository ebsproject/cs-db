--liquibase formatted sql

--changeset postgres:set_attributes_defaut_values context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Set default values in attributes table



UPDATE core.attributes 
 SET is_multiline = TRUE;

UPDATE core.attributes 
 SET is_required = TRUE;

UPDATE core.attributes 
 SET sm = 3;

UPDATE core.attributes 
 SET md = 6;

UPDATE core.attributes 
 SET lg = 12;