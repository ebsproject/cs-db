--liquibase formatted sql

--changeset postgres:update_attributes_values context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1809 Update attributes values



do $$
declare _ent_ref integer;
begin

SELECT id FROM CORE.entity_reference WHERE entity = 'Service' AND entity_schema = 'workflow' INTO _ent_ref;

UPDATE core.attributes 
 SET sm = 3, md = 6, lg = 12 
 WHERE entityreference_id = _ent_ref;

UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'id' AND entityreference_id = _ent_ref;
UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'instance_id' AND entityreference_id = _ent_ref;
UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'person_id' AND entityreference_id = _ent_ref;
UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'wf_instance_id' AND entityreference_id = _ent_ref;
UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'tenant_id' AND entityreference_id = _ent_ref;
UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'creation_timestamp' AND entityreference_id = _ent_ref;
UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'modification_timestamp' AND entityreference_id = _ent_ref;
UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'creator_id' AND entityreference_id = _ent_ref;
UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'modifier_id' AND entityreference_id = _ent_ref;
UPDATE core.attributes SET is_required= FALSE WHERE "name" = 'is_void' AND entityreference_id = _ent_ref;

 end $$