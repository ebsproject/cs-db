--liquibase formatted sql

--changeset postgres:update_cs_admin_role_permissions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update Workflow Model



UPDATE core.product
SET has_workflow = true
WHERE "name"='Request No-GSR';


do $$
declare _cs_admin int;
declare _wf_mng int;
declare _place int;
declare _filter int;
declare _no_gsr int;
declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'CS Admin' INTO _cs_admin;
SELECT id FROM core.product where name = 'Workflow Management' INTO _wf_mng;
SELECT id FROM core.product where name = 'Place Manager' INTO _place;
SELECT id FROM core.product where name = 'Global Filter' INTO _filter;
SELECT id FROM core.product where name = 'Request No-GSR' INTO _no_gsr;

DELETE FROM "security". role_product_function
    WHERE 
        role_id = (SELECT id FROM "security".role WHERE "name" = 'Admin')
    AND 
        product_function_id IN (SELECT id FROM "security".product_function WHERE product_id = _wf_mng);

DELETE FROM "security". role_product_function
    WHERE 
        role_id = (SELECT id FROM "security".role WHERE "name" = 'Admin')
    AND 
        product_function_id IN (SELECT id FROM "security".product_function WHERE product_id = _place);

DELETE FROM "security". role_product_function
    WHERE 
        role_id = (SELECT id FROM "security".role WHERE "name" = 'Admin')
    AND 
        product_function_id IN (SELECT id FROM "security".product_function WHERE product_id = _filter);
    
DELETE FROM "security". role_product_function
    WHERE 
        role_id = (SELECT id FROM "security".role WHERE "name" = 'Admin')
    AND 
        product_function_id IN (SELECT id FROM "security".product_function WHERE product_id = _no_gsr);



FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _wf_mng
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_cs_admin, temprow.id);
END LOOP;

FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _place
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_cs_admin, temprow.id);
END LOOP;

FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _filter
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_cs_admin, temprow.id);
END LOOP;

FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _no_gsr
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_cs_admin, temprow.id);
END LOOP;


end $$
