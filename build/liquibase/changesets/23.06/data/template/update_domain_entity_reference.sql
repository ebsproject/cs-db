--liquibase formatted sql

--changeset postgres:update_domain_entity_reference context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1809 Update domain_entity_reference



DELETE FROM core.domain_entity_reference;

INSERT INTO core.domain_entity_reference
    (entity_reference_id, domain_id)
VALUES
    ((SELECT id FROM core.entity_reference WHERE entity = 'Service' AND entity_schema = 'workflow'), 
     (SELECT id FROM core.domain WHERE prefix = 'sm'));
