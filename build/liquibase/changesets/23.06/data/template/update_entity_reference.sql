--liquibase formatted sql

--changeset postgres:update_entity_reference context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update entity_reference to include service table



SELECT setval('core.entity_reference_id_seq', (SELECT MAX(id) FROM core.entity_reference));
SELECT setval('core.attributes_id_seq', (SELECT MAX(id) FROM core."attributes"));

INSERT INTO core.entity_reference
	(entity, textfield, valuefield, storefield, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
SELECT 
	initcap(table_name),'','id','',1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,FALSE 
FROM 
	information_schema."tables" t
WHERE NOT EXISTS (SELECT id FROM core.entity_reference e WHERE lower(e.entity) = lower(table_name)) 
	AND table_type ='BASE TABLE' AND 
	NOT table_schema IN ('pg_catalog','information_schema','public');
    

INSERT INTO core."attributes"
	("name", description, help, sort_no, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, entityreference_id)
SELECT 
	column_name,column_name,'',c.ordinal_position,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,FALSE,er.id
FROM  
	information_schema.columns c INNER JOIN core.entity_reference AS er ON lower(c.table_name) = lower(er.entity)  
WHERE NOT EXISTS 
	(SELECT a.id FROM core."attributes" a 
	WHERE a.entityreference_id = er.id AND lower(a."name") = lower (c.column_name) )
AND NOT c.table_schema in ('pg_catalog','information_schema','public');

SELECT setval('core.entity_reference_id_seq', (SELECT MAX(id) FROM core.entity_reference));
SELECT setval('core.attributes_id_seq', (SELECT MAX(id) FROM core."attributes"));

WITH ROWS AS (
	SELECT 
		e.id,t.table_schema FROM information_schema."tables" t INNER JOIN core.entity_reference e ON lower(t.table_name) = lower(e.entity)
	WHERE 
		t.table_type ='BASE TABLE' AND
		NOT table_schema IN ('pg_catalog','information_schema')
)
UPDATE 
	core.entity_reference
SET 
	entity_schema= r.table_schema
FROM 
	rows r 
WHERE 
	core.entity_reference.id=r.id
;


