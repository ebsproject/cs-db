--liquibase formatted sql

--changeset postgres:update_markerdb_permissions context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1532 Update MarkerDb permissions



do $$
declare _admin int;
declare _sm_admin int;
declare _lab_man int;
declare _marker int;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'SM Admin' INTO _sm_admin;
SELECT id FROM "security"."role" where name = 'Lab Manager' INTO _lab_man;

SELECT id FROM core.product where name = 'MarkerDb' INTO _marker;

DELETE FROM "security". role_product_function
    WHERE NOT role_id IN (_admin, _sm_admin, _lab_man)
    AND 
        product_function_id IN (SELECT id FROM "security".product_function WHERE product_id = _marker);

END $$