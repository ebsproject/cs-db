--liquibase formatted sql

--changeset postgres:update_product_entity_ref context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update product entity_reference_id



UPDATE core.product
SET entity_reference_id = (SELECT id FROM core.entity_reference WHERE entity = 'Service' AND entity_schema = 'workflow' )
WHERE "name"='Request No-GSR';

