--liquibase formatted sql

--changeset postgres:update_service_attributes_description context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update Service attributes description



do $$

DECLARE _entity integer;

BEGIN

SELECT id FROM core.entity_reference WHERE entity = 'Service' AND entity_schema = 'workflow' INTO _entity;

UPDATE core."attributes" SET "description" = 'adminContact' WHERE "name" = 'admin_contact' and entityreference_id = _entity;
UPDATE core."attributes" SET "description" = 'requestCode' WHERE "name" = 'request_code' and entityreference_id = _entity;
UPDATE core."attributes" SET "description" = 'submitionDate' WHERE "name" = 'submition_date' and entityreference_id = _entity;
UPDATE core."attributes" SET "description" = 'chargeAccount' WHERE "name" = 'charge_account' and entityreference_id = _entity;

end $$