--liquibase formatted sql

--changeset postgres:update_wf_product_name context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1749 Update Workflow Products



UPDATE core.product
    SET "name"='Workflow Management', abbreviation = 'WF_MANAGEMENT' 
    WHERE "name"='WF Designer';

UPDATE core.product
    SET "name"='Request No-GSR', abbreviation = 'REQUEST_NO_GSR' 
    WHERE "name"='WF Request';
