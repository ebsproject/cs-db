--liquibase formatted sql

--changeset postgres:add_columns_in_node_cf_and_status_type context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1809 Add columns in node_cf table and status_type



ALTER TABLE workflow.node_cf 
 ADD COLUMN api_attributes_name varchar(150) NULL;

 ALTER TABLE workflow.node_cf 
 ADD COLUMN attributes_id integer NULL;

 ALTER TABLE workflow.node_cf ADD CONSTRAINT "FK_node_cf_attributes"
	FOREIGN KEY (attributes_id) REFERENCES core.attributes (id) ON DELETE No Action ON UPDATE No Action;

ALTER TABLE workflow.status_type 
 ADD COLUMN workflow_id integer NULL;

 ALTER TABLE workflow.status_type ADD CONSTRAINT "FK_status_type_workflow"
	FOREIGN KEY (workflow_id) REFERENCES workflow.workflow (id) ON DELETE No Action ON UPDATE No Action;
