--liquibase formatted sql

--changeset postgres:add_json_column_in_hierarchy_tree_value context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1807 Add column json value in hierarchy_tree_attributes_values



ALTER TABLE core.hierarchy_tree_attributes_values
 ADD COLUMN "data" jsonb NULL;