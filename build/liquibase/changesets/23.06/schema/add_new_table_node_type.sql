--liquibase formatted sql

--changeset postgres:add_new_table_node_type context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1823 Add new table node_type in workflow schema



ALTER TABLE workflow.node 
 ADD COLUMN node_type_id integer NULL;

CREATE TABLE workflow.node_type
(
	id serial NOT NULL,
	name varchar(150) NULL,
	description varchar(500) NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

ALTER TABLE workflow.node_type ADD CONSTRAINT "PK_node_type"
	PRIMARY KEY (id)
;

ALTER TABLE workflow.node ADD CONSTRAINT "FK_node_node_type"
	FOREIGN KEY (node_type_id) REFERENCES workflow.node_type (id) ON DELETE No Action ON UPDATE No Action
;
