--liquibase formatted sql

--changeset postgres:change_data_type_wf_view context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Change column data type in wf_view_type 



ALTER TABLE workflow.wf_view_type
 ALTER COLUMN "name" TYPE varchar(250);

ALTER TABLE workflow.wf_view_type
 ALTER COLUMN notes TYPE varchar(500);


