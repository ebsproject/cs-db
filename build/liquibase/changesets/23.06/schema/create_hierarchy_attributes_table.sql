--liquibase formatted sql

--changeset postgres:create_hierarchy_attributes_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1748 Create Hierarchy attributes



ALTER TABLE Workflow.cf_type
 DROP COLUMN IF EXISTS tenant_id;


CREATE TABLE core.hierarchy_design_attributes
(
    id serial NOT NULL,	-- Unique identifier of the record within the table.
	cf_type_id integer NULL,
	value varchar(50) NULL,
	hierarchy_design_id integer NULL,
    field_attributes jsonb,
    is_saved boolean DEFAULT TRUE,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE core.hierarchy_tree_attributes_values
(
    id serial NOT NULL,	-- Unique identifier of the record within the table.
    hierarchy_tree_id integer NULL,
    hierarchy_design_attributes_id integer,
	name varchar(50) NULL,
	value varchar(50) NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

ALTER TABLE core.hierarchy_design_attributes ADD CONSTRAINT "PK_hierarchy_design_attributes"
	PRIMARY KEY (id)
;

ALTER TABLE core.hierarchy_tree_attributes_values ADD CONSTRAINT "PK_hierarchy_tree_attributes_values"
	PRIMARY KEY (id)
;

ALTER TABLE core.hierarchy_tree_attributes_values ADD CONSTRAINT "FK_hierarchy_tree_attributes_values_design_attributes"
	FOREIGN KEY (hierarchy_design_attributes_id) REFERENCES core.hierarchy_design_attributes (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.hierarchy_design_attributes ADD CONSTRAINT "FK_hierarchy_design_attributes_cf_type"
	FOREIGN KEY (cf_type_id) REFERENCES workflow.cf_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.hierarchy_design_attributes ADD CONSTRAINT "FK_hierarchy_design_attributes_hierarchy_design"
	FOREIGN KEY (hierarchy_design_id) REFERENCES core.hierarchy_design (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.hierarchy_tree_attributes_values ADD CONSTRAINT "FK_hierarchy_tree_attributes_values_hierarchy_tree"
	FOREIGN KEY (hierarchy_tree_id) REFERENCES core.hierarchy_tree (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN core.hierarchy_design_attributes.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.hierarchy_design_attributes.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.hierarchy_design_attributes.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.hierarchy_design_attributes.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.hierarchy_design_attributes.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.hierarchy_design_attributes.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.hierarchy_design.filter
	IS 'Contains a filter in case the design needs it to obtain specific information according to what is requested'
;


COMMENT ON COLUMN core.hierarchy_design.level
	IS 'Defines the order of the level within the hierarchy e.g (1-Institution, 2-Program, 3-Unit, 4-Role)'
;

COMMENT ON COLUMN core.hierarchy_design.name
	IS 'Name of the hierarchy level'
;

COMMENT ON COLUMN core.hierarchy_tree_attributes_values.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.hierarchy_tree_attributes_values.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.hierarchy_tree_attributes_values.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.hierarchy_tree_attributes_values.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.hierarchy_tree_attributes_values.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.hierarchy_tree_attributes_values.modifier_id
	IS 'ID of the user who last modified the record'
;
