--liquibase formatted sql

--changeset postgres:increase_description_lenght context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update Workflow Model



ALTER TABLE workflow.node_cf
 ALTER COLUMN description TYPE varchar(500);