--liquibase formatted sql

--changeset postgres:increase_text_value_lenght context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1809 Increase text_value lenght


ALTER TABLE workflow.cf_value
 ALTER COLUMN text_value TYPE varchar(500);