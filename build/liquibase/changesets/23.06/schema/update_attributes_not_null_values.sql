--liquibase formatted sql

--changeset postgres:update_attributes_not_null_values context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update not null values in attributes table



ALTER TABLE core.attributes 
 ALTER COLUMN is_multiline SET DEFAULT TRUE;

ALTER TABLE core.attributes 
 ALTER COLUMN is_multiline SET NOT NULL;

ALTER TABLE core.attributes 
 ALTER COLUMN is_required SET DEFAULT TRUE;

ALTER TABLE core.attributes 
 ALTER COLUMN is_required SET NOT NULL;

