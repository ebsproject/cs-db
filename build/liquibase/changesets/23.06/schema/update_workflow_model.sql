--liquibase formatted sql

--changeset postgres:update_workflow_model context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update Workflow Model



ALTER TABLE workflow.node 
  DROP  CONSTRAINT "FK_node_entity_reference";

ALTER TABLE workflow.node_cf 
  DROP  CONSTRAINT "FK_node_cf_entity_reference";

ALTER TABLE workflow.workflow 
  DROP  CONSTRAINT "FK_workflow_entity_reference";

ALTER TABLE workflow.node 
 DROP COLUMN IF EXISTS entityreference_id;

ALTER TABLE workflow.node_cf 
 DROP COLUMN IF EXISTS entityreference_id;

ALTER TABLE workflow.workflow 
 DROP COLUMN IF EXISTS entityreference_id;

ALTER TABLE workflow.cf_value 
 ADD COLUMN event_id integer NULL;

ALTER TABLE workflow.node 
 ADD COLUMN define jsonb NOT NULL;

ALTER TABLE workflow.node 
 ADD COLUMN depend_on jsonb NOT NULL;

ALTER TABLE workflow.node 
 ADD COLUMN icon varchar(50) NULL;

ALTER TABLE workflow.node 
 ADD COLUMN message jsonb NOT NULL;

ALTER TABLE workflow.node 
 ADD COLUMN require_input jsonb NOT NULL;

ALTER TABLE workflow.node 
 ADD COLUMN security_definition jsonb NOT NULL;

ALTER TABLE workflow.node 
 ADD COLUMN validation_code varchar(500) NULL;

ALTER TABLE workflow.node 
 ADD COLUMN validation_type varchar(50) NULL;

ALTER TABLE workflow.node 
 ADD COLUMN wf_view_type_id integer NOT NULL;

ALTER TABLE workflow.phase 
 ADD COLUMN depend_on jsonb NULL;

ALTER TABLE workflow.phase 
 ADD COLUMN icon varchar(50) NULL;

ALTER TABLE workflow.phase 
 ADD COLUMN wf_view_type_id integer NULL;

ALTER TABLE workflow.stage 
 ADD COLUMN depend_on jsonb NULL;

ALTER TABLE workflow.stage 
 ADD COLUMN icon varchar(50) NULL;

ALTER TABLE workflow.stage 
 ADD COLUMN wf_view_type_id integer NULL;

ALTER TABLE workflow.workflow 
 ADD COLUMN api varchar(250) NULL;

ALTER TABLE workflow.workflow 
 ADD COLUMN navigation_icon varchar(50) NULL;

ALTER TABLE workflow.workflow 
 ADD COLUMN navigation_name varchar(50) NULL;

ALTER TABLE workflow.workflow 
 ADD COLUMN product_id integer NULL;

ALTER TABLE workflow.workflow 
 ADD COLUMN sequence integer NULL;

ALTER TABLE workflow.workflow 
 ADD COLUMN show_menu boolean NOT NULL DEFAULT TRUE;

ALTER TABLE core.product 
 ADD COLUMN entity_reference_id integer NULL;

ALTER TABLE core.product 
 DROP COLUMN IF EXISTS main_entity;

CREATE TABLE workflow.role_workflow
(
	workflow_id integer NOT NULL,
	role_id integer NOT NULL
)
;

CREATE TABLE workflow.wf_view_type
(
    id integer NOT NULL   DEFAULT NEXTVAL(('workflow."wf_view_type_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name integer NULL,
	notes integer NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE SEQUENCE workflow.wf_view_type_id_seq INCREMENT 1 START 1;

ALTER TABLE workflow.wf_view_type ADD CONSTRAINT "PK_wf_view_type"
	PRIMARY KEY (id)
;

ALTER TABLE workflow.role_workflow ADD CONSTRAINT "PK_role_workflow"
	PRIMARY KEY (workflow_id,role_id)
;

ALTER TABLE workflow.cf_value ADD CONSTRAINT "FK_cf_value_event"
	FOREIGN KEY (event_id) REFERENCES workflow.event (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.node ADD CONSTRAINT "FK_node_wf_view_type"
	FOREIGN KEY (wf_view_type_id) REFERENCES workflow.wf_view_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.phase ADD CONSTRAINT "FK_phase_wf_view_type"
	FOREIGN KEY (wf_view_type_id) REFERENCES workflow.wf_view_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.role_workflow ADD CONSTRAINT "FK_role_workflow_role"
	FOREIGN KEY (role_id) REFERENCES security.role (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.role_workflow ADD CONSTRAINT "FK_role_workflow_workflow"
	FOREIGN KEY (workflow_id) REFERENCES workflow.workflow (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.stage ADD CONSTRAINT "FK_stage_wf_view_type"
	FOREIGN KEY (wf_view_type_id) REFERENCES workflow.wf_view_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.workflow ADD CONSTRAINT "FK_workflow_product"
	FOREIGN KEY (product_id) REFERENCES core.product (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.product ADD CONSTRAINT "FK_product_entity_reference"
	FOREIGN KEY (entity_reference_id) REFERENCES core.entity_reference (id) ON DELETE No Action ON UPDATE No Action
