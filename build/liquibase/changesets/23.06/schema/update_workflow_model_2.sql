--liquibase formatted sql

--changeset postgres:update_workflow_model_2 context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update Workflow Model



ALTER TABLE core.product 
 ADD COLUMN IF NOT EXISTS has_workflow boolean DEFAULT FALSE;

ALTER TABLE workflow.workflow 
 ADD COLUMN is_system boolean NULL;

ALTER TABLE workflow.workflow
 ALTER COLUMN description TYPE varchar(500);

ALTER TABLE workflow.workflow
 ALTER COLUMN help TYPE varchar(500);

ALTER TABLE workflow.workflow
 DROP COLUMN IF EXISTS "definition";

ALTER TABLE workflow.cf_value DROP CONSTRAINT "FK_cf_value_request";

ALTER TABLE workflow.cf_value
 DROP COLUMN IF EXISTS request_id;

ALTER TABLE workflow.workflow
 DROP COLUMN IF EXISTS tenant_id;

DROP TABLE IF EXISTS workflow.request;

ALTER TABLE core.translation ADD CONSTRAINT "FK_translation_to_language"
	FOREIGN KEY (language_to) REFERENCES core.language (id) ON DELETE No Action ON UPDATE No Action
;

CREATE TABLE workflow.tenant_workflow
(
	workflow_id integer NOT NULL,
	tenant_id integer NOT NULL
)
;

ALTER TABLE workflow.tenant_workflow ADD CONSTRAINT "PK_tenant_workflow"
	PRIMARY KEY (workflow_id,tenant_id)
;
ALTER TABLE workflow.tenant_workflow ADD CONSTRAINT "FK_tenant_workflow_tenant"
	FOREIGN KEY (tenant_id) REFERENCES core.tenant (id) ON DELETE No Action ON UPDATE No Action
;
ALTER TABLE workflow.tenant_workflow ADD CONSTRAINT "FK_tenant_workflow_workflow"
	FOREIGN KEY (workflow_id) REFERENCES workflow.workflow (id) ON DELETE No Action ON UPDATE No Action
;
