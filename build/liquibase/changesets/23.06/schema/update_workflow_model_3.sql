--liquibase formatted sql

--changeset postgres:update_workflow_model_3 context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1762 Update Workflow Model



DROP TABLE IF EXISTS workflow.role_workflow;

ALTER TABLE workflow.workflow 
 ADD COLUMN security_definition jsonb NULL;

ALTER TABLE workflow.node_cf 
 ADD COLUMN field_attributes jsonb NULL;

CREATE TABLE workflow.service (
    id serial NOT NULL,
    instance_id int4 NULL,
	person_id int4 NULL,
	wf_instance_id int4 NULL,
	requester varchar(150) NULL,
	request_code varchar(50) NULL,
	submition_date date NULL,
	admin_contact varchar(150) NULL,
	charge_account varchar(50) NULL,
	description varchar(500) NULL,
	tenant_id int4 NOT NULL,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	modification_timestamp timestamp NULL,
	creator_id int4 NOT NULL,
	modifier_id int4 NULL,
	is_void bool NOT NULL DEFAULT false,

	CONSTRAINT "PK_service" PRIMARY KEY (id)
);

ALTER TABLE workflow.service ADD CONSTRAINT "FK_service_person" FOREIGN KEY (person_id) REFERENCES crm.person(id);
ALTER TABLE workflow.service ADD CONSTRAINT "FK_service_wf_instance" FOREIGN KEY (wf_instance_id) REFERENCES workflow.wf_instance(id);


