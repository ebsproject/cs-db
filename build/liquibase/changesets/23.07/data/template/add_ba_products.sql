--liquibase formatted sql

--changeset postgres:add_ba_products context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1527 Add BA products



INSERT INTO core.product
    ("name", description, help, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
VALUES
    ('Phenotypic Data Manager', '', 'Phenotypic Data Manager', '', 1, 
    (select id from core.domain where prefix = 'ba'), 
    (SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'ba')), 
    1, '/pdm', 'PHENOTYPIC DATA MANAGER')
    ;

INSERT INTO core.product
    ("name", description, help, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
VALUES
    ('Molecular Data Analysis', '', 'Molecular Data Analysis', '', 1, 
    (select id from core.domain where prefix = 'ba'), 
    (SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'ba')), 
    1, '/mda', 'MOLECULAR DATA ANALYSIS')
    ;

UPDATE core.product
SET "path"='/arm', abbreviation = 'ANALYSIS REQUEST MANAGER'
WHERE "name" =  'Analysis Request Manager';

UPDATE core.domain_instance
SET is_mfe = false
WHERE domain_id = (SELECT id FROM core.domain WHERE "name" = 'Analytics');



--rollback SELECT NULL;
