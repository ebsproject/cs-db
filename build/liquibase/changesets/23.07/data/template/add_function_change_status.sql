--liquibase formatted sql

--changeset postgres:add_function_change_status context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1896 Create product function change_status for Shipment Manager Product



do $$
declare _admin int;
declare _approver int;
declare _datam int;
declare _sp int;
declare _prod int;

declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'Approver' INTO _approver;
SELECT id FROM "security"."role" where name = 'Data Manager' INTO _datam;
SELECT id FROM "security"."role" where name = 'Service Provider' INTO _sp;

SELECT id FROM core.product where name = 'Shipment Manager' INTO _prod;

WITH _function as(
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Change Status', true, 'Change Status', 1, _prod, true)
RETURNING id
)
        INSERT INTO "security".role_product_function (role_id, product_function_id)
	        VALUES
                (_admin, (select id FROM _function)),
                (_approver, (select id FROM _function)),
                (_datam, (select id FROM _function)),
                (_sp, (select id FROM _function))
                ;


end $$
