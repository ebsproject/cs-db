--liquibase formatted sql

--changeset postgres:generate_design_ref_uuid_values context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1908 Add column design_ref to phase, stage, and node table



UPDATE workflow.phase 
SET design_ref = uuid_generate_v4()
WHERE design_ref IS NULL;

UPDATE workflow.stage 
SET design_ref = uuid_generate_v4()
WHERE design_ref IS NULL;

UPDATE workflow.node 
SET design_ref = uuid_generate_v4()
WHERE design_ref IS NULL;