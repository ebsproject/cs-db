--liquibase formatted sql

--changeset postgres:set_security_definition_in_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1630 Update security definition for existing workflows



UPDATE 
    workflow.workflow
SET 
    security_definition = '{"roles": [{"id": "5", "name": "CS Admin", "isSystem": false, "description": "Core System Administrator"}], "users": [{"id": "106", "contact": {"id": 106, "person": {"givenName": "Developer", "familyName": "Developer"}}, "userName": "e.briones@cimmyt.org"}], "programs": [{"id": 102, "name": "BW Wheat Breeding Program"}]}' 
WHERE 
    title='Incoming Seed';


UPDATE 
    workflow.workflow
SET 
    security_definition = '{"roles": [{"id": "5", "name": "CS Admin", "isSystem": false, "description": "Core System Administrator"}], "users": [{"id": "106", "contact": {"id": 106, "person": {"givenName": "Developer", "familyName": "Developer"}}, "userName": "e.briones@cimmyt.org"}], "programs": [{"id": 102, "name": "BW Wheat Breeding Program"}]}'
WHERE 
    title= 'Outgoing Seed';


UPDATE 
    workflow.node
SET 
    security_definition = '{"roles": [{"id": "5", "name": "CS Admin", "isSystem": false, "description": "Core System Administrator"}], "users": [{"id": "106", "contact": {"id": 106, "person": {"givenName": "Developer", "familyName": "Developer"}}, "userName": "e.briones@cimmyt.org"}], "programs": [{"id": 102, "name": "BW Wheat Breeding Program"}]}';