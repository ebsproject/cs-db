--liquibase formatted sql

--changeset postgres:add_column_design_ref context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1908 Add column design_ref to phase, stage, and node table



ALTER TABLE workflow.phase 
 ADD COLUMN design_ref uuid NULL;

ALTER TABLE workflow.stage 
 ADD COLUMN design_ref uuid NULL;

ALTER TABLE workflow.node 
 ADD COLUMN design_ref uuid NULL;


ALTER TABLE workflow.phase 
  ADD CONSTRAINT "UQ_phase_design_ref" UNIQUE (design_ref);

ALTER TABLE workflow.stage 
  ADD CONSTRAINT "UQ_stage_design_ref" UNIQUE (design_ref);

ALTER TABLE workflow.node 
  ADD CONSTRAINT "UQ_node_design_ref" UNIQUE (design_ref);

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";