--liquibase formatted sql

--changeset postgres:add_column_origin context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1903 Add column origin to shipment_item table



ALTER TABLE shm.shipment_item
 ADD COLUMN origin varchar(100) NULL;
