--liquibase formatted sql

--changeset postgres:increase_lenght_in_workflow_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1630 Increase title and name lenght in workflow table


ALTER TABLE workflow.workflow 
 ALTER COLUMN title TYPE varchar(500);

ALTER TABLE workflow.workflow 
 ALTER COLUMN name TYPE varchar(255);