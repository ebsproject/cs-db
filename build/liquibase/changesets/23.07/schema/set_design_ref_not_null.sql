--liquibase formatted sql

--changeset postgres:set_design_ref_not_null context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1908 Add column design_ref to phase, stage, and node table



ALTER TABLE workflow.phase  
 ALTER COLUMN design_ref SET NOT NULL;

ALTER TABLE workflow.node  
 ALTER COLUMN design_ref SET NOT NULL;

ALTER TABLE workflow.stage  
 ALTER COLUMN design_ref SET NOT NULL;
