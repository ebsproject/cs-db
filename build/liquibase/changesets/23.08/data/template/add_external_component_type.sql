--liquibase formatted sql

--changeset postgres:add_external_component_type context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1957 Add the external component type 



INSERT INTO core.process
("name", description, code, is_background, call_report, tenant_id, creator_id)
VALUES('External', 'External', 'EXTERNAL', false, false, 1, 1);

INSERT INTO workflow.node_type
("name", description, creator_id)
VALUES('External', 'External Component', 1);
