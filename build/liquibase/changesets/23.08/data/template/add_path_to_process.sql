--liquibase formatted sql

--changeset postgres:add_path_to_process context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1955 Add path value to process linked to a node



UPDATE core.process
SET "path"='workflow/send/email'
WHERE "name"='Send Email';

UPDATE core.process
SET "path"='workflow/change/status'
WHERE "name"='Change Status';

UPDATE core.process
SET "path"='workflow/move/node'
WHERE "name"='Move';

UPDATE core.process
SET "path"='workflow/send/notification'
WHERE "name"='Send Notification';

