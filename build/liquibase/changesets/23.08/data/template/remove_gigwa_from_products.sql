--liquibase formatted sql

--changeset postgres:remove_gigwa_from_products context:template splitStatements:false rollbackSplitStatements:false
--comment: Remove GIGWA from products



DELETE FROM 
    "security".role_product_function 
WHERE 
    product_function_id IN
    (SELECT id 
    FROM 
        "security".product_function
    WHERE 
        product_id IN (SELECT id FROM core.product p WHERE name = 'GIGWA')
    );


UPDATE core.product
SET is_void = true
WHERE name='GIGWA';