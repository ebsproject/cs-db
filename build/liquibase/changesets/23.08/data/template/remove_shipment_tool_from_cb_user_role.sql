--liquibase formatted sql

--changeset postgres:remove_shipment_tool_from_cb_user_role context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1833 Remove Shipment tool from CB User role



do $$
declare _shm_tool integer;

begin

SELECT id FROM core.product WHERE "name" = 'Shipment Tool' INTO _shm_tool;

DELETE FROM "security".role_product_function rpf 
WHERE 
    role_id = (SELECT id FROM "security"."role" WHERE name = 'CB User')
	AND product_function_id IN (SELECT id FROM "security".product_function pf WHERE product_id = _shm_tool);

end $$