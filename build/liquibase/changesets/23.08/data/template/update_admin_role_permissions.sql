--liquibase formatted sql

--changeset postgres:update_admin_role_permissions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1953 update_admin_role_permissions



do $$
declare _cb integer;
declare temprow record;
declare r_admin integer;

begin

SELECT id FROM core."domain" WHERE prefix = 'cb' INTO _cb;
SELECT id FROM "security"."role" where name = 'Admin' INTO r_admin;
UPDATE "security".product_function SET "action"='Filter_Toolbar' WHERE description='Filter_Toolbar';


DELETE FROM 
	"security".product_function pf 	
WHERE 
	product_id = (SELECT id FROM core.product p WHERE name = 'Data Explorer');

DELETE FROM 
    "security".role_product_function 
WHERE 
    product_function_id IN
    (SELECT id 
    FROM 
        "security".product_function
    WHERE 
        product_id IN (SELECT id FROM core.product p WHERE name <> 'Data Collection' AND domain_id = _cb)
    );

DELETE FROM 
	"security".product_function pf 	
WHERE 
	product_id IN (SELECT id FROM core.product p WHERE name <> 'Data Collection' AND domain_id = _cb);


FOR temprow IN 
    SELECT 
	    pf.id
    FROM 
	    "security".product_function pf
    where pf.id NOT IN 
        (
            SELECT 
	            rpf.product_function_id
            FROM 
	            "security".role_product_function rpf
            inner join 
	            "security"."role" r on rpf.role_id = r.id
            inner join 
	            "security".product_function pf 	on rpf.product_function_id = pf.id
            inner join
	            core.product p on p.id = pf.product_id
            inner join
	            core."domain" d on  d.id = p.domain_id
            where 
                r."name"= 'Admin'
        )
LOOP
INSERT INTO "security".role_product_function (role_id, product_function_id)
        (select r_admin, temprow.id);

END LOOP;

END $$;


