--liquibase formatted sql

--changeset postgres:update_sm_md_lg_values context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1808 Update sm, md and lg values in attributes table



UPDATE core.attributes 
 SET sm = 3, md = 6, lg = 12;