--liquibase formatted sql

--changeset postgres:update_stage_node_id context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1768 Update sequence for stage and node tables



do 
$$
declare temprow record;
begin

FOR temprow IN SELECT id FROM workflow.stage
LOOP
    UPDATE workflow.stage
    SET id = temprow.id + 10000 WHERE id = temprow.id;
END LOOP;

FOR temprow IN SELECT id FROM workflow.node
LOOP
    UPDATE workflow.node
    SET id = temprow.id + 20000 WHERE id = temprow.id;
END LOOP;

end $$;

SELECT setval('workflow.stage_id_seq', (SELECT MAX(id) FROM workflow.stage));
SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM workflow.node));