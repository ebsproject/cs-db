--liquibase formatted sql

--changeset postgres:void_gigwa_product_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1970 Void GIGWA Product Functions



UPDATE "security".product_function
SET is_void = true
WHERE product_id IN (SELECT id FROM core.product p WHERE name = 'GIGWA');