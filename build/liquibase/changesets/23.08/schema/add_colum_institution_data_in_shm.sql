--liquibase formatted sql

--changeset postgres:add_colum_institution_data_in_shm context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1887 Add columninstitution_data in shipment table



ALTER TABLE shm.shipment 
 ADD COLUMN institution_data jsonb NULL;