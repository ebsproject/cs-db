--liquibase formatted sql

--changeset postgres:add_requestor_to_service_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1964 Add requester_id and recipient_id columns in service table


ALTER TABLE workflow.service 
 ADD COLUMN requestor_id integer NULL;

ALTER TABLE workflow.service 
 ADD COLUMN sender_id integer NULL;

ALTER TABLE workflow.service 
 ADD COLUMN recipient_id integer NULL;


ALTER TABLE workflow.service ADD CONSTRAINT "FK_service_requestor_contact"
	FOREIGN KEY (requestor_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.service ADD CONSTRAINT "FK_service_sender_contact"
	FOREIGN KEY (requestor_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.service ADD CONSTRAINT "FK_service_recipient_contact"
	FOREIGN KEY (requestor_id) REFERENCES crm.contact (id) ON DELETE No Action ON UPDATE No Action
;