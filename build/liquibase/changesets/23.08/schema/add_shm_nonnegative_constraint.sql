--liquibase formatted sql

--changeset postgres:add_shm_nonnegative_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1854 Prevent user to enter negative values



ALTER TABLE shm.shipment_item
   ADD CONSTRAINT count_nonnegative CHECK (package_count >= 0);

ALTER TABLE shm.shipment_item
   ADD CONSTRAINT weight_nonnegative CHECK ("weight" >= 0);