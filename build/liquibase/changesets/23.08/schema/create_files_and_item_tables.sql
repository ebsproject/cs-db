--liquibase formatted sql

--changeset postgres:create_files_and_item_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1961 Add new table in workflow schema for document files and items



CREATE TABLE workflow.files
(
	id integer NOT NULL   DEFAULT NEXTVAL(('workflow."files_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	service_id integer NOT NULL,
	file_object_id uuid NOT NULL,
	remarks varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE workflow.items
(
	id integer NOT NULL   DEFAULT NEXTVAL(('workflow."items_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	service_id integer NOT NULL,
	germplasm_id integer NOT NULL,
	seed_id integer NOT NULL,
	package_id integer NULL,
	number integer NOT NULL,
	code varchar(150) NOT NULL,
	status varchar(50) NOT NULL   DEFAULT 'passed',
	weight integer NOT NULL,
	package_unit varchar(32) NOT NULL,
	package_count integer NOT NULL,
	test_code varchar(150) NULL,
	mta_status varchar(64) NULL,
	availability varchar(64) NULL,
	use varchar(64) NULL,
	smta_id varchar(256) NULL,
	mls_ancestors varchar(50) NULL,
	genetic_stock varchar(50) NULL,
	remarks text NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE SEQUENCE workflow.files_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE workflow.items_id_seq INCREMENT 1 START 1;


ALTER TABLE workflow.files ADD CONSTRAINT "PK_files"
	PRIMARY KEY (id)
;

ALTER TABLE workflow.items ADD CONSTRAINT "PK_items"
	PRIMARY KEY (id)
;

ALTER TABLE workflow.files ADD CONSTRAINT "FK_files_file_object"
	FOREIGN KEY (file_object_id) REFERENCES core.file_object (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.files ADD CONSTRAINT "FK_files_service"
	FOREIGN KEY (service_id) REFERENCES workflow.service (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE workflow.items ADD CONSTRAINT "FK_items_service"
	FOREIGN KEY (service_id) REFERENCES workflow.service (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN workflow.files.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN workflow.files.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN workflow.files.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.files.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN workflow.files.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN workflow.files.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN workflow.files.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN workflow.items.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN workflow.items.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN workflow.items.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN workflow.items.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN workflow.items.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN workflow.items.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN workflow.items.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;
