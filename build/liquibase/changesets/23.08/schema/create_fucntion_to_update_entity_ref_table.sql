--liquibase formatted sql

--changeset postgres:create_fucntion_to_update_entity_ref_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1808 Create function to update the entity reference table



CREATE OR REPLACE FUNCTION core.update_entity_ref()
returns boolean
language plpgsql
as
$$
declare

begin

PERFORM setval('core.entity_reference_id_seq', (SELECT MAX(id) FROM core.entity_reference));
PERFORM setval('core.attributes_id_seq', (SELECT MAX(id) FROM core."attributes"));

INSERT INTO core.entity_reference
	(entity, textfield, valuefield, storefield, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void,entity_schema)
SELECT 
	initcap(table_name),'','id','',1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,FALSE, table_schema 
FROM 
	information_schema."tables" t
WHERE NOT EXISTS (SELECT id FROM core.entity_reference e WHERE lower(e.entity) = lower(table_name)) 
	AND table_type ='BASE TABLE' AND 
	NOT table_schema IN ('pg_catalog','information_schema','public');
    

INSERT INTO core."attributes"
	("name", description, help, sort_no, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, entityreference_id, sm, md, lg)
SELECT 
	column_name,column_name,'',c.ordinal_position,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,FALSE,er.id,3,6,12
FROM  
	information_schema.columns c INNER JOIN core.entity_reference AS er ON lower(c.table_name) = lower(er.entity)  
WHERE NOT EXISTS 
	(SELECT a.id FROM core."attributes" a 
	WHERE a.entityreference_id = er.id AND lower(a."name") = lower (c.column_name) )
AND NOT c.table_schema in ('pg_catalog','information_schema','public');

PERFORM setval('core.entity_reference_id_seq', (SELECT MAX(id) FROM core.entity_reference));
PERFORM setval('core.attributes_id_seq', (SELECT MAX(id) FROM core."attributes"));

return true;

end;
$$;

SELECT core.update_entity_ref();