--liquibase formatted sql

--changeset postgres:create_relation_email_template_job_workflow context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1923 Create relationship email template and job workflow



ALTER TABLE core.job_workflow 
 ADD COLUMN email_template_id integer NULL;

ALTER TABLE core.job_workflow ADD CONSTRAINT "FK_job_workflow_email_template"
	FOREIGN KEY (email_template_id) REFERENCES core.email_template (id) ON DELETE No Action ON UPDATE No Action;


COMMENT ON COLUMN core.job_workflow.email_template_id
	IS 'Reference to the email template that will be used during the messaging process';