--liquibase formatted sql

--changeset postgres:update_stage_node_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1768 Update sequence for stage and node tables



ALTER TABLE workflow."event" DROP CONSTRAINT "FK_event_stage";
ALTER TABLE workflow.node_stage DROP CONSTRAINT "FK_node_stage_stage";
ALTER TABLE workflow."event" DROP CONSTRAINT "FK_event_node";
ALTER TABLE workflow.node_cf DROP CONSTRAINT "FK_node_cf_node";
ALTER TABLE workflow.workflow DROP CONSTRAINT "FK_workflow_node";
ALTER TABLE workflow.node_stage DROP CONSTRAINT "FK_node_stage_node";


ALTER TABLE workflow."event" ADD CONSTRAINT "FK_event_stage" FOREIGN KEY (stage_id) REFERENCES workflow.stage(id) ON UPDATE CASCADE;
ALTER TABLE workflow.node_stage ADD CONSTRAINT "FK_node_stage_stage" FOREIGN KEY (stage_id) REFERENCES workflow.stage(id) ON UPDATE CASCADE;
ALTER TABLE workflow."event" ADD CONSTRAINT "FK_event_node" FOREIGN KEY (node_id) REFERENCES workflow.node(id) ON UPDATE CASCADE;
ALTER TABLE workflow.node_cf ADD CONSTRAINT "FK_node_cf_node" FOREIGN KEY (node_id) REFERENCES workflow.node(id) ON UPDATE CASCADE;
ALTER TABLE workflow.workflow ADD CONSTRAINT "FK_workflow_node" FOREIGN KEY (node_id) REFERENCES workflow.node(id) ON UPDATE CASCADE;
ALTER TABLE workflow.node_stage ADD CONSTRAINT "FK_node_stage_node" FOREIGN KEY (node_id) REFERENCES workflow.node(id) ON UPDATE CASCADE;
