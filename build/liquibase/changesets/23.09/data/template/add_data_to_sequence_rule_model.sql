--liquibase formatted sql

--changeset postgres:add_data_to_sequence_rule_model context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2037 Add data to Sequence Rule model



DO $$

declare _seq_rule integer;
declare _seg_seq integer;
declare _seg_api integer;

BEGIN

INSERT INTO core.sequence_rule
("name", tenant_id, creator_id)
VALUES('shipments', 1, 1);
SELECT id FROM core.sequence_rule WHERE "name" = 'shipments' INTO _seq_rule;


INSERT INTO core.segment_sequence
(lowest, "increment", "last", reset_condition, tenant_id, creator_id)
VALUES(1, 1, 0, NULL, 1, 1);
SELECT id FROM core.segment_sequence WHERE lowest = 1 INTO _seg_seq;


INSERT INTO core.segment_api
("type", "method", server_url, path_template, response_mapping, body_template, tenant_id, creator_id)
VALUES('GRAPHQL', 'POST', 'https://csapi-dev.ebsproject.org', '/graphql', 'data.findProgram.code', 'query{findProgram(id:{}){id,code}}', 1, 1);
SELECT id FROM core.segment_api WHERE type = 'GRAPHQL' INTO _seg_api;

INSERT INTO core.rule_segment
("name", requires_input, format, formula, data_type, segment_api_id, segment_sequence_id, tenant_id, creator_id)
VALUES
('full-date', false, 'yyyy-MM-dd', NULL, 'DATE', NULL, NULL, 1, 1),
('ship-sequence', false, '00000', NULL, 'NUMBER', NULL, _seg_seq, 1, 1),
('program-code', true, NULL, NULL, 'TEXT', _seg_api, NULL, 1, 1),
('dash', false, NULL, '-', 'TEXT', NULL, NULL, 1, 1);


INSERT INTO core.sequence_rule_segment
("position", segment_family, rule_segment_id, sequence_rule_id, tenant_id, creator_id)
VALUES
(1, 11, (SELECT id FROM core.rule_segment WHERE name = 'program-code'), _seq_rule, 1, 1),
(2, 12, (SELECT id FROM core.rule_segment WHERE name = 'dash'), _seq_rule, 1, 1),
(3, 13, (SELECT id FROM core.rule_segment WHERE name = 'full-date'), _seq_rule, 1, 1),
(4, 14, (SELECT id FROM core.rule_segment WHERE name = 'dash'), _seq_rule, 1, 1),
(5, 15, (SELECT id FROM core.rule_segment WHERE name = 'ship-sequence'), _seq_rule, 1, 1);

end $$
