--liquibase formatted sql

--changeset postgres:add_text_field_to_items context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1997 Update text fields for Items, Tenant and Workflow



UPDATE core.entity_reference
SET textfield = 'code', entity = 'items'
WHERE entity = 'Items';

UPDATE core.entity_reference
SET textfield = 'name', entity = 'tenant'
WHERE entity = 'Tenant';

UPDATE core.entity_reference
SET textfield = 'name', entity = 'workflow'
WHERE entity = 'Workflow';

UPDATE core.entity_reference
SET textfield = 'text_value', entity = 'cf_value'
WHERE entity = 'Cf_Value';

UPDATE core.entity_reference
SET textfield = 'name', entity = 'node_type'
WHERE entity = 'Node_Type';

UPDATE core.entity_reference
SET textfield = 'name', entity = 'node_cf'
WHERE entity = 'Node_Cf';

UPDATE core.entity_reference
SET textfield = 'remarks', entity = 'files'
WHERE entity = 'Files';

UPDATE core.entity_reference
SET textfield = 'value', entity = 'hierarchy_design_attributes'
WHERE entity = 'Hierarchy_Design_Attributes';

UPDATE core.entity_reference
SET textfield = 'workflow_id', entity = 'tenant_workflow'
WHERE entity = 'Tenant_Workflow';

UPDATE core.entity_reference
SET textfield = 'requester', entity = 'service'
WHERE entity = 'Service';

UPDATE core.entity_reference
SET textfield = 'name', entity = 'wf_view_type'
WHERE entity = 'Wf_View_Type';

UPDATE core.entity_reference
SET textfield = 'code', entity = 'shipment'
WHERE entity = 'Shipment';

UPDATE core.entity_reference
SET textfield = 'name', entity = 'job_workflow'
WHERE entity = 'Job_Workflow';

UPDATE core.entity_reference
SET textfield = 'name', entity = 'phase'
WHERE entity = 'Phase';

UPDATE core.entity_reference
SET textfield = 'name', entity = 'status_type'
WHERE entity = 'Status_Type';
