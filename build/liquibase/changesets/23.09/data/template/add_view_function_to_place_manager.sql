--liquibase formatted sql

--changeset postgres:add_view_function_to_place_manager context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1668 Add view function to Place Manager product



do $$
declare _admin int;
declare _cs_admin int;
declare _prod int;

declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'CS Admin' INTO _cs_admin;


SELECT id FROM core.product where name = 'Place Manager' INTO _prod;

WITH _function as(
INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('View', true, 'View', 1, _prod, false)
RETURNING id
)
        INSERT INTO "security".role_product_function (role_id, product_function_id)
	        VALUES
                (_admin, (select id FROM _function)),
                (_cs_admin, (select id FROM _function))
                ;
end $$