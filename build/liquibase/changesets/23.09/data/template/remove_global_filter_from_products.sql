--liquibase formatted sql

--changeset postgres:remove_global_filter_from_products context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2007 Remove Global Filter from Products



DELETE FROM 
    "security".role_product_function 
WHERE 
    product_function_id IN
    (SELECT id 
    FROM 
        "security".product_function
    WHERE 
        product_id IN (SELECT id FROM core.product p WHERE name = 'Global Filter')
    );

UPDATE "security".product_function
SET is_void = true
WHERE product_id IN (SELECT id FROM core.product p WHERE name = 'Global Filter');

UPDATE core.product
SET is_void = true
WHERE name='Global Filter';

