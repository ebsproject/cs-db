--liquibase formatted sql

--changeset postgres:add_columns_in_workflowt_items context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2004 Add new columns in workflow items table


ALTER TABLE workflow.items 
 ADD COLUMN package_name varchar(255) NULL;

ALTER TABLE workflow.items 
 ADD COLUMN package_label varchar(255) NULL;

ALTER TABLE workflow.items 
 ADD COLUMN seed_name varchar(255) NULL;

ALTER TABLE workflow.items 
 ADD COLUMN seed_code varchar(255) NULL;

ALTER TABLE workflow.items 
 ADD COLUMN designation varchar(255) NULL;

ALTER TABLE workflow.items 
 ADD COLUMN parentage text NULL;

ALTER TABLE workflow.items 
 ADD COLUMN taxonomy_name varchar(255) NULL;

ALTER TABLE workflow.items 
 ADD COLUMN origin varchar(100) NULL;

