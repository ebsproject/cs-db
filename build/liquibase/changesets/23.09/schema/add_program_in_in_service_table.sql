--liquibase formatted sql

--changeset postgres:add_program_in_in_service_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1997 Add program_id to Service table



ALTER TABLE workflow.service 
 ADD COLUMN program_id integer NULL;

ALTER TABLE workflow.service ADD CONSTRAINT "FK_service_program"
	FOREIGN KEY (program_id) REFERENCES program.program (id) ON DELETE No Action ON UPDATE No Action;

SELECT core.update_entity_ref();


COMMENT ON COLUMN workflow.service.program_id
	IS 'Reference to the Program id related to the service';