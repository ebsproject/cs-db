--liquibase formatted sql

--changeset postgres:create_workflow_cf_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1997 Create fucntion workflow cf



CREATE OR REPLACE FUNCTION workflow.jsonb_cf_filter(_workflow_id integer, _node_id integer) 
RETURNS jsonb
 LANGUAGE plpgsql
AS $function$
    
BEGIN

CREATE temp TABLE _temp (
    id int,
    field varchar,
    value varchar

) on commit drop;

INSERT INTO _temp(id, field, value) 

select
	case
		when r.id is null then 0
		else r.id
	end as id,
	cf.name as field,
	case
		when not wcfv.text_value is null then coalesce(wcfv.text_value, null::character varying)
		else
		case
			when not wcfv.code_value is null then coalesce(txtValue.value, null)
			else
			case
				when wcfv.flag_value = true then coalesce(wcfv.flag_value::character varying, null::character varying)
				else
				case
					when not wcfv.date_value is null then coalesce(wcfv.date_value::character varying, null::character varying)
					else
					case
						when not wcfv.num_value is null then coalesce(wcfv.num_value::character varying, null::character varying)
						else null::character varying
					end
				end
			end
		end
		 end as value
	from
		workflow.workflow w
	join workflow.node wn on
		wn.workflow_id = w.id
	join workflow.node_cf cf on
		wn.id = cf.node_id
	join workflow.cf_type cft on
		cf.cftype_id = cft.id
	left outer join workflow.wf_instance wi on
		w.id = wi.workflow_id
	left outer join workflow.event wfevent on
		wi.id = wfevent.wf_instance_id
		and wfevent.node_id = wn.id
	left outer join workflow.service r on
		r.id = wfevent.instance_id
	left outer join workflow.cf_value wcfv on
		r.id = wcfv.event_id
		and wcfv.nodecf_id = cf.id
	left outer join core.attributes a on
		cf.attributes_id = a.id
	left outer join core.entity_reference er on
		er.id = a.entityreference_id
	left outer join workflow.getcodetextvalue(coalesce(er.entity_schema,'core') ,
		coalesce(er.entity,'tenant'),
		coalesce(er.textfield,'name'),
		coalesce(wcfv.code_value, 0))  txtValue on
		wcfv.code_value is not null
	WHERE w.id = _workflow_id AND wn.id = _node_id; 

PERFORM workflow.colpivot('_test_pivoted','select * from _temp',array['id'],array['field'],'#.value',null);

RETURN jsonb_pretty(jsonb_agg(query)) from (
    SELECT * FROM workflow.service s 
        INNER JOIN _test_pivoted t ON s.id = t.id
) query;

END;
$function$
;