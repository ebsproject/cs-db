--liquibase formatted sql

--changeset postgres:update_sequence_rule_model context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2037 Update Sequence Rule model



DROP TABLE IF EXISTS core.number_sequence_rule_entity_reference;
DROP TABLE IF EXISTS core.number_sequence_rule_segment;
DROP TABLE IF EXISTS core.number_sequence_rule;
DROP TABLE IF EXISTS core.segment;


CREATE TABLE core.segment_api
(
    id integer NOT NULL   DEFAULT NEXTVAL(('core."segment_api_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	type varchar(50) NOT NULL,
	method varchar(50) NOT NULL,
	server_url varchar(500) NOT NULL,
	path_template varchar(500) NOT NULL,
	response_mapping varchar(500) NOT NULL,
	body_template varchar(500) NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE core.segment_sequence
(
    id integer NOT NULL   DEFAULT NEXTVAL(('core."segment_sequence_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	lowest integer NOT NULL,
	increment integer NOT NULL,
	last integer NOT NULL,
	reset_condition varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;


CREATE TABLE core.rule_segment
(
    id integer NOT NULL   DEFAULT NEXTVAL(('core."rule_segment_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(150) NOT NULL,	-- Descriptive name of the segment.
	requires_input boolean NOT NULL,	-- Short name identifier of the segment.
	format varchar(50) NULL,	-- Specifies if the segment is obtained by calling an API.
	formula varchar(250) NULL,	-- Specifies if the segment is an sequence number value.
	data_type varchar(50) NOT NULL,	-- Specifies if the segment is an static value such as prefix, separator symbol, etc.
	segment_api_id integer NULL,
	segment_sequence_id integer NULL,
    tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;


CREATE TABLE core.sequence_rule
(
    id integer NOT NULL   DEFAULT NEXTVAL(('core."sequence_rule_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(150) NOT NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE TABLE core.sequence_rule_segment
(
    id integer NOT NULL   DEFAULT NEXTVAL(('core."sequence_rule_segment_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	position integer NOT NULL,
	segment_family integer NULL,
    rule_segment_id integer NULL,
	sequence_rule_id integer NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE SEQUENCE core.rule_segment_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE core.segment_api_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE core.segment_sequence_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE core.sequence_rule_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE core.sequence_rule_segment_id_seq INCREMENT 1 START 1;

ALTER TABLE core.rule_segment ADD CONSTRAINT "PK_rule_segment"
	PRIMARY KEY (id)
;

ALTER TABLE core.segment_api ADD CONSTRAINT "PK_segment_api"
	PRIMARY KEY (id)
;

ALTER TABLE core.segment_sequence ADD CONSTRAINT "PK_segment_sequence"
	PRIMARY KEY (id)
;

ALTER TABLE core.sequence_rule ADD CONSTRAINT "PK_sequence_rule"
	PRIMARY KEY (id)
;

ALTER TABLE core.sequence_rule_segment ADD CONSTRAINT "PK_sequence_rule_segment"
	PRIMARY KEY (id)
;

ALTER TABLE core.rule_segment ADD CONSTRAINT "FK_rule_segment_segment_api"
	FOREIGN KEY (segment_api_id) REFERENCES core.segment_api (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.rule_segment ADD CONSTRAINT "FK_rule_segment_segment_sequence"
	FOREIGN KEY (segment_sequence_id) REFERENCES core.segment_sequence (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.sequence_rule_segment ADD CONSTRAINT "FK_sequence_rule_segment_rule_segment"
	FOREIGN KEY (rule_segment_id) REFERENCES core.rule_segment (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE core.sequence_rule_segment ADD CONSTRAINT "FK_sequence_rule_segment_sequence_rule"
	FOREIGN KEY (sequence_rule_id) REFERENCES core.sequence_rule (id) ON DELETE No Action ON UPDATE No Action
;



COMMENT ON COLUMN core.segment_api.type
	IS 'It can be graphql or rest.'
;

COMMENT ON COLUMN core.segment_api.method
	IS 'Reading method, it can be GET or POST.'
;

COMMENT ON COLUMN core.segment_api.server_url
	IS 'protocol+domain+port. E.g: https://csapi-sdx.ebsproject.org:8082.'
;

COMMENT ON COLUMN core.segment_api.path_template
	IS 'Context path of the service + variables placeholders. E.g: /graphql, /rest/crop/{id}.'
;

COMMENT ON COLUMN core.segment_api.response_mapping
	IS 'route within the service response to extract the value for the segment.'
;

COMMENT ON COLUMN core.segment_api.body_template
	IS 'Request body + variables placeholders. Example: {"id":${id}}.'
;

COMMENT ON COLUMN core.segment_sequence.lowest
	IS 'Counter initial value.'
;

COMMENT ON COLUMN core.segment_sequence.increment
	IS 'Increment to obtain the next value. Default: 1.'
;

COMMENT ON COLUMN core.segment_sequence.last
	IS 'Last value used.'
;

COMMENT ON COLUMN core.segment_sequence.reset_condition
	IS 'Condition for the counter to return to the initial value.'
;

COMMENT ON COLUMN core.rule_segment.requires_input
	IS 'If an api/formula requires some value to extract the segment. Ex: id of the entity searched.'
;

COMMENT ON COLUMN core.rule_segment.format
	IS 'E.g: zero-padding, date format, maximum/minimum string length.'
;

COMMENT ON COLUMN core.rule_segment.formula
	IS 'for static values such as dashes, periods, constants (e.g. -, _, /, CIM, IRRI) or fixed functions (e.g. Monday->1, Tuesday->2, Wednesday->3).'
;

COMMENT ON COLUMN core.rule_segment.segment_api_id
	IS 'Id reference to the segment_api related.'
;

COMMENT ON COLUMN core.rule_segment.segment_sequence_id
	IS 'Id reference to the segment_sequence related.'
;

COMMENT ON COLUMN core.sequence_rule.name
	IS 'Specific name of teh rule.'
;

COMMENT ON COLUMN core.sequence_rule_segment.position
	IS 'segment order within the sequence rule.'
;

COMMENT ON COLUMN core.sequence_rule_segment.segment_family
	IS 'to use a segment within a group given the context of the sequence rule e.g.: if list is plots>plot code, if it is package->package code. Only one segment of each family is used.'
;

COMMENT ON COLUMN core.sequence_rule_segment.rule_segment_id
	IS 'Id reference to the rule_segment.'
;

COMMENT ON COLUMN core.sequence_rule_segment.sequence_rule_id
	IS 'Id reference to the sequence_rule.'
;

COMMENT ON TABLE core.rule_segment
	IS 'Manages the sections of the codes used in the sequence rules.'
;

COMMENT ON COLUMN core.rule_segment.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.rule_segment.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.rule_segment.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.rule_segment.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.rule_segment.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.rule_segment.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.rule_segment.name
	IS 'Descriptive name of the segment.'
;

COMMENT ON COLUMN core.rule_segment.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.segment_api.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.segment_api.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.segment_api.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.segment_api.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.segment_api.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.segment_api.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.segment_api.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.segment_sequence.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.segment_sequence.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.segment_sequence.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.segment_sequence.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.segment_sequence.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.segment_sequence.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.segment_sequence.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.sequence_rule.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.sequence_rule.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.sequence_rule.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.sequence_rule.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.sequence_rule.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.sequence_rule.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.sequence_rule.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN core.sequence_rule_segment.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN core.sequence_rule_segment.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN core.sequence_rule_segment.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN core.sequence_rule_segment.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN core.sequence_rule_segment.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN core.sequence_rule_segment.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN core.sequence_rule_segment.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

