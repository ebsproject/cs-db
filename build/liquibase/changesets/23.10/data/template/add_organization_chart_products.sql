--liquibase formatted sql

--changeset postgres:add_organization_chart_products context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2115 Add new product Organization Chart in CSDB



INSERT INTO core.product
("name", description, help, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
VALUES
    ('Organization Chart', 'Organization Chart', 'Organization Chart', '',  1, 
    (select id from core.domain where prefix = 'cs'), 
    (SELECT MAX (menu_order) + 1 FROM core.product WHERE domain_id = (SELECT id FROM core.domain WHERE prefix = 'cs')), 1, 'organization-units', 'ORGANIZATION_CHART')
    ;

do $$
declare _admin int;
declare _cs_admin int;
declare _product int;
declare temprow record;

begin

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'CS Admin' INTO _cs_admin;
SELECT id FROM core.product where name = 'Organization Chart' INTO _product;

INSERT INTO "security".product_function
    (description, system_type, "action", creator_id, product_id, is_data_action)
VALUES
    ('Create', true, 'Create', 1, _product, true),
    ('Modify', true, 'Modify', 1, _product, true),
    ('Delete', true, 'Delete', 1, _product, true),
    ('Export', true, 'Export', 1, _product, false),
    ('Print', true, 'Print', 1, _product, false),
    ('Search', true, 'Search', 1, _product, false),
    ('Read', true, 'Read', 1, _product, true);

FOR temprow IN
    SELECT id FROM "security".product_function where product_id = _product
LOOP
    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_admin, temprow.id),
        (_cs_admin, temprow.id);
END LOOP;
end $$