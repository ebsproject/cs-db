--liquibase formatted sql

--changeset postgres:add_sequence_rule_data context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1624 Add data to Sequence Rule model



TRUNCATE 
    core.sequence_rule_segment,
    core.rule_segment, 
    core.segment_api,
    core.segment_sequence, 
    core.sequence_rule  
RESTART IDENTITY;


INSERT INTO core.sequence_rule
(id, "name", tenant_id, creator_id)
VALUES
(1, 'shipments', 1, 1),
(2, 'sm-batch', 1, 1),
(3, 'sm-request', 1, 1),
(4, 'sm-plate', 1, 1),
(5, 'vendor-file', 1, 1);

INSERT INTO core.segment_sequence
(id, lowest, "increment", "last", reset_condition, tenant_id, creator_id)
VALUES
(1, 1, 1, 20, NULL, 1, 1),
(2, 1, 1, 4, NULL, 1, 1),
(3, 1, 1, 0, NULL, 1, 1),
(4, 1, 1, 0, NULL, 1, 1);


INSERT INTO core.segment_api
(id, "type", "method", server_url, path_template, response_mapping, body_template, tenant_id, creator_id)
VALUES
(1, 'GRAPHQL', 'POST', 'CS', 'graphql', 'data.findProgram.code', 'query{findProgram(id:{}){id,code}}', 1, 1),
(2, 'GRAPHQL', 'POST', 'SM', 'graphql', 'data.findPurpose.code', 'query{findPurpose(id:{}){id,code}}', 1, 1),
(3, 'GRAPHQL', 'POST', 'SM', 'graphql', 'data.findBatch.name', 'query{findBatch(id:{}){id,name}}', 1, 1),
(4, 'GRAPHQL', 'POST', 'SM', 'graphql', 'data.findVendor.code', 'query{findVendor(id:{}){id,code}}', 1, 1),
(5, 'GRAPHQL', 'POST', 'SM', 'graphql', 'data.findBatch.createdOn', 'query{findBatch(id:{}){id,createdOn}}', 1, 1),
(6, 'GRAPHQL', 'POST', 'CS', 'graphql', 'data.findCrop.name', 'query{findCrop(id:{}){id,name}}', 1, 1),
(7, 'GRAPHQL', 'POST', 'CS', 'graphql', 'data.findCrop.code', 'query{findCrop(id:{}){id,code}}', 1, 1),
(8, 'GRAPHQL', 'POST', 'SM', 'graphql', 'data.findRequest.createdOn', 'query{findRequest(id:{}){id,createdOn}}', 1, 1),
(9, 'GRAPHQL', 'POST', 'SM', 'graphql', 'data.findServiceProvider.code', 'query{findServiceProvider(id:{}){id,code}}', 1, 1),
(10, 'GRAPHQL', 'POST', 'SM', 'graphql', 'data.findServiceProvider.name', 'query{findServiceProvider(id:{}){id,name}}', 1,1);


INSERT INTO core.rule_segment
(id, "name", requires_input, format, formula, data_type, segment_api_id, segment_sequence_id, tenant_id, creator_id)
VALUES
(1, 'full-date', false, 'yyyy-MM-dd', NULL, 'DATE', NULL, NULL, 1, 1),
(2, 'ship-sequence', false, '0000000', NULL, 'NUMBER', NULL, 1, 1, 1),
(3, 'program-code', true, NULL, NULL, 'TEXT', 1, NULL, 1, 1),
(4, 'dash', false, NULL, '-', 'TEXT', NULL, NULL, 1, 1),
(5, 'batch-prefix', false, NULL, 'B', 'TEXT', NULL, NULL, 1, 1),
(6, 'purpose-code', true, NULL, NULL, 'TEXT', 2, NULL, 1, 1),
(7, 'underscore', false, NULL, '_', 'TEXT', NULL, NULL, 1, 1),
(8, 'batch-sequence', false, NULL, NULL, 'NUMBER', NULL, 2, 1, 1),
(9, 'request-prefix', false, NULL, 'RLB', 'TEXT', NULL, NULL, 1, 1),
(10, 'full-date-onlynumbers', false, 'yyyyMMdd', NULL, 'DATE', NULL, NULL, 1, 1),
(11, 'request-sequence', false, NULL, NULL, 'NUMBER', NULL, 3, 1, 1),
(12, 'batch-code', true, NULL, NULL, 'TEXT', 3, NULL, 1, 1),
(13, 'plate-prefix', false, NULL, 'P', 'TEXT', NULL, NULL, 1, 1),
(14, 'plate-sequence', false, NULL, NULL, 'TEXT', NULL, 4, 1, 1),
(15, 'current-year', false, 'yyyy', NULL, 'DATE', NULL, NULL, 1, 1),
(16, 'vendor-code', true, NULL, NULL, 'TEXT', 4, NULL, 1, 1),
(17, 'batch-creation-date', true, 'yyyy-MM-dd', NULL, 'DATE', 5, NULL, 1, 1),
(18, 'crop-name', true, NULL, NULL, 'TEXT', 6, NULL, 1, 1),
(19, 'crop-code', true, NULL, NULL, 'TEXT', 7, NULL, 1, 1),
(20, 'request-creation-date', true, 'yyyy-MM-dd', NULL, 'DATE', 8, NULL, 1, 1),
(21, 'service-provider-code', true, NULL, NULL, 'TEXT', 9, NULL, 1, 1),
(22, 'service-provider-name', true, NULL, NULL, 'TEXT', 10, NULL, 1, 1),
(23, 'download-timestamp', false, 'yyyy-MM-dd_hh:mm:ss', NULL, 'DATETIME', NULL, NULL, 1, 1);

INSERT INTO core.sequence_rule_segment
(id, "position", segment_family, rule_segment_id, sequence_rule_id, tenant_id, creator_id)
VALUES
(1, 1, 11, 3, 1, 1, 1),
(2, 2, 12, 4, 1, 1, 1),
(3, 3, 13, 1, 1, 1, 1),
(4, 4, 14, 4, 1, 1, 1),
(5, 5, 15, 2, 1, 1, 1),
(6, 1, 11, 5, 2, 1, 1),
(7, 2, 12, 3, 2, 1, 1),
(8, 3, 13, 6, 2, 1, 1),
(9, 4, 14, 7, 2, 1, 1),
(10, 5, 15, 8, 2, 1, 1),
(11, 1, 11, 9, 3, 1, 1),
(12, 2, 12, 4, 3, 1, 1),
(13, 3, 13, 10, 3, 1, 1),
(14, 4, 14, 4, 3, 1, 1),
(15, 5, 15, 11, 3, 1, 1),
(16, 1, 11, 12, 4, 1, 1),
(17, 2, 12, 4, 4, 1, 1),
(18, 3, 13, 13, 4, 1, 1),
(19, 4, 14, 14, 4, 1, 1),
(20, 1, 11, 12, 5, 1, 1),
(21, 3, 13, 21, 5, 1, 1),
(22, 5, 15, 22, 5, 1, 1),
(23, 7, 17, 16, 5, 1, 1),
(24, 9, 19, 23, 5, 1, 1),
(25, 2, 12, 4, 5, 1, 1),
(26, 4, 14, 4, 5, 1, 1),
(27, 6, 16, 4, 5, 1, 1),
(28, 8, 18, 4, 5, 1, 1);


SELECT setval('core.sequence_rule_id_seq', (SELECT MAX(id) FROM core.sequence_rule));
SELECT setval('core.segment_sequence_id_seq', (SELECT MAX(id) FROM core.segment_sequence));
SELECT setval('core.segment_api_id_seq', (SELECT MAX(id) FROM core.segment_api));
SELECT setval('core.rule_segment_id_seq', (SELECT MAX(id) FROM core.rule_segment));
SELECT setval('core.sequence_rule_segment_id_seq', (SELECT MAX(id) FROM core.sequence_rule_segment));
