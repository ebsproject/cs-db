--liquibase formatted sql

--changeset postgres:update_ba_context_mee context:template labels:mee splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2811 Update template databases for all environments with the correct URL for BA


UPDATE core.domain_instance
SET context='https://ba-mee.ebsproject.org'
WHERE domain_id = (SELECT id FROM core.domain WHERE "name" = 'Analytics');