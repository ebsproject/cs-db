--liquibase formatted sql

--changeset postgres:update_cb_products_and_permissions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2121 Update CB products and role permissions in CSDB



do $$
declare _domain int;
declare _admin int;
declare _tm int;
declare _coll int;
declare _cb_admin int;
declare temprow record;
declare _action varchar;
declare _desc varchar;
declare _prod_id integer;

begin

       DELETE FROM core.printout_template_product WHERE product_id IN (
    SELECT id FROM core.product WHERE 
        "name" = 'Pollination Instructions'
        OR "name" = 'Search Seeds'
        OR "name" = 'Seed Inventory.'
        OR "name" = 'KDX data transfer');

DELETE FROM core.product
WHERE "name" = 'Pollination Instructions';

DELETE FROM core.product
WHERE "name" = 'Search Seeds';

DELETE FROM core.product
WHERE "name" = 'Seed Inventory.';

DELETE FROM core.product
WHERE "name" = 'KDX data transfer';

SELECT id FROM "security"."role" where name = 'Admin' INTO _admin;
SELECT id FROM "security"."role" where name = 'CB Admin' INTO _cb_admin;
SELECT id FROM "security"."role" where name = 'Team Member' INTO _tm;
SELECT id FROM "security"."role" where name = 'Collaborator' INTO _coll;
SELECT id FROM core.domain where prefix = 'cb' INTO _domain;

INSERT INTO core.product
("name", description, help, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation, external_id)
VALUES
    ('Germplasm', 'Germplasm', 'Germplasm', '',  1, _domain, 7, 1, 'germplasm/default', 'GERMPLASM', 61),
    ('Inventory search', 'Inventory search', 'Inventory search', '',  1, _domain, 8, 1, 'seedInventory/find-seeds', 'INVENTORY-SEARCH', 54),
    ('Inventory manager', 'Inventory manager', 'Inventory manager', '',  1, _domain, 9, 1, 'inventoryManager', 'INVENTORY-MANAGER', 68),
    ('Traits', 'Traits', 'Traits', '',  1, _domain, 10, 1, 'traits/default', 'TRAITS', 63),
    ('List manager', 'List manager', 'List manager', '',  1, _domain, 11, 1, 'account/list', 'LIST-MANAGER', 53);


UPDATE core.product SET menu_order = 1, external_id = 51, path = 'experimentCreation/create', abbreviation = 'EXPERIMENT_CREATION', help = 'Experiment Creation' WHERE "name" = 'Experiment Creation';
UPDATE core.product SET menu_order = 2, external_id = 62, path = 'occurrence', abbreviation = 'OCCURRENCES' WHERE "name" = 'Experiment Manager';
UPDATE core.product SET menu_order = 3, external_id = 67, path = 'crossManager', abbreviation = 'CROSS_MANAGER' WHERE "name" = 'Cross Manager';
UPDATE core.product SET menu_order = 4, external_id = 64, path = 'plantingInstruction', abbreviation = 'PLANTING_INSTRUCTIONS_MANAGER' WHERE "name" = 'Planting Instruction Manager';
UPDATE core.product SET menu_order = 5, external_id = 12, path = 'dataCollection/terminal' WHERE "name" = 'Data Collection';
UPDATE core.product SET menu_order = 6, external_id = 60, path = 'harvestManager/occurrence-selection', abbreviation = 'HARVEST_MANAGER' WHERE "name" = 'Harvest Manager';


FOR temprow IN
    SELECT id, "name" FROM core.product where domain_id = _domain and "name" <> 'Data Collection'
LOOP 
    IF 
        temprow.name = 'Experiment Creation' THEN 
			_desc := 'Browse experiments'; 
			_action := 'VIEW_EXPERIMENTS';
    ELSEIF 
        temprow.name = 'Experiment Manager' THEN 
			_desc := 'Browse occurrences';
			 _action := 'VIEW_OCCURRENCES';
    ELSEIF 
        temprow.name = 'Cross Manager' THEN 
			_desc := 'Browse cross lists';
			_action := 'VIEW_CROSS_LISTS';
    ELSEIF 
        temprow.name = 'Planting Instruction Manager' THEN 
			_desc :=  'Browse packing and planting jobs';
			_action := 'VIEW_PACKING_JOBS';
    ELSEIF 
        temprow.name = 'Harvest Manager' THEN 
			_desc :=  'Browse HM landing page';
			_action := 'VIEW_HARVEST_DATA';
    ELSEIF 
        temprow.name = 'Germplasm' THEN 
			_desc :=  'Browse germplasm list';
			_action := 'VIEW_GERMPLASM';
    ELSEIF 
        temprow.name = 'Inventory search' THEN 
			_desc :=  'Browse IS landing page';
			_action := 'VIEW_SEARCH_RESULT';
    ELSEIF 
        temprow.name = 'Inventory manager' THEN 
			_desc :=  'Browse seed/packages upload';
			_action := 'VIEW_SEED_PACKAGES';
    ELSEIF 
        temprow.name = 'Traits' THEN 
			_desc :=  'Browse traits';
			_action := 'VIEW_TRAITS';
    ELSEIF 
        temprow.name = 'List manager' THEN 
			_desc :=  'Browse lists';
			_action := 'VIEW_LISTS';
    END IF; 

WITH _prod AS (
    INSERT INTO 
        "security".product_function (description, system_type, "action", creator_id, product_id, is_data_action)
	VALUES
        (_desc, true, _action, 1, temprow.id, false)    
RETURNING id
)

    INSERT INTO "security".role_product_function (role_id, product_function_id)
	VALUES
        (_admin, (select id from _prod)),
        (_cb_admin, (select id from _prod)),
        (_tm, (select id from _prod)),
        (_coll, (select id from _prod))
    ;
           
END LOOP;
end $$