--liquibase formatted sql

--changeset postgres:update_product_name context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2115 Update product name to Organizational Units



UPDATE 
    core.product
SET 
    "name" = 'Organizational Units', description = 'Organizational Units', help = 'Organizational Units', abbreviation = 'ORGANIZATIONAL_UNITS'
WHERE 
    "name" ='Organization Chart';