--liquibase formatted sql

--changeset postgres:void_organizational_units context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2115 Add new product Organization Chart in CSDB



do $$
declare _product int;

begin

UPDATE core.product
SET is_void = true where "name"= 'Organizational Units';

SELECT id FROM core.product where name = 'Organizational Units' INTO _product;

UPDATE "security".product_function
    SET is_void = true WHERE product_id = _product;


DELETE FROM "security".role_product_function 
WHERE product_function_id IN (SELECT id FROM "security".product_function where product_id = _product);

end $$