--liquibase formatted sql

--changeset postgres:add_external_id_to_hierarchy_products_and_role context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2114 Add external_id to Hierarchy and Product tables



ALTER TABLE core.product
ADD COLUMN external_id INTEGER NULL;

ALTER TABLE core.product
ADD COLUMN is_redirect BOOLEAN DEFAULT FALSE;

ALTER TABLE core.product
ADD COLUMN is_external BOOLEAN DEFAULT FALSE;

ALTER TABLE core.hierarchy_tree
ADD COLUMN external_record_id INTEGER NULL;


COMMENT ON COLUMN core.product.is_redirect
	IS 'Determines if the action of the navigation redirects to an EBS Application';

COMMENT ON COLUMN core.product.is_external
	IS 'Determines if the action of the navigation redirects to an Application outside the domain e.g GIGWA';