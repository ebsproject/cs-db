--liquibase formatted sql

--changeset postgres:02_add_status context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2210 Update  Worflow definition fixtureDB

SET session_replication_role = 'replica';

INSERT INTO workflow.status_type ("name",description,help,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,workflow_id) VALUES
	 ('On Hold','On Hold','On Hold',1,'2023-10-19 15:44:26.477',NULL,122,NULL,false,301,496),
	 ('Proccessing','Processing','Processing',1,'2023-10-19 15:44:40.642',NULL,122,NULL,false,302,496),
	 ('Sent','Sent','Sent',1,'2023-10-19 15:44:49.897',NULL,122,NULL,false,303,496),
	 ('Done','Done','Done',1,'2023-10-19 15:44:59.408',NULL,122,NULL,false,304,496),
	 ('Draft','Draft','Draft',1,'2023-10-19 15:43:45.625',NULL,122,NULL,false,298,496),
	 ('Created','Created','Created',1,'2023-10-19 15:43:58.18',NULL,122,NULL,false,299,496),
	 ('Submitted','Submitted','Submitted',1,'2023-10-19 15:44:13.158',NULL,122,NULL,false,300,496),
	 ('Submitted','Submitted','Submitted',1,'2023-10-24 16:31:33.724',NULL,122,NULL,false,332,529),
	 ('On Hold','On Hold','On Hold',1,'2023-10-24 16:32:28.025',NULL,122,NULL,false,334,529),
	 ('Processing Completed','Processing Completed','Processing Completed',1,'2023-10-24 16:32:56.668',NULL,122,NULL,false,335,529),
	 ('Data Management Completed','Data Management Completed','Data Management Completed',1,'2023-10-24 16:33:16.66',NULL,122,NULL,false,336,529),
	 ('Draft','Draft','Draft',1,'2023-10-24 16:31:17.756',NULL,122,NULL,false,331,529),
	 ('Processing','Processing','Processing',1,'2023-10-24 16:31:49.012',NULL,122,NULL,false,333,529),
	 ('Created','Created','Created',1,'2023-11-13 16:37:09.848',NULL,122,NULL,false,366,562),
	 ('On Hold','On Hold','On Hold',1,'2023-11-28 15:11:09.479',NULL,122,NULL,false,368,562),
	 ('Done','Done','Done',1,'2023-11-28 15:11:22.253',NULL,122,NULL,false,369,562),
	 ('Processing','Processing','Processing',1,'2023-11-28 15:11:40.691',NULL,122,NULL,false,370,562),
	 ('Draft','Draft','Draft',1,'2023-11-13 16:36:48.939',NULL,122,NULL,false,364,562),
	 ('Submitted','Submitted','Submitted',1,'2023-11-13 16:36:59.214',NULL,122,NULL,false,365,562),
	 ('Sent','Sent','Sent',1,'2023-11-28 15:10:58.587',NULL,122,NULL,false,367,562);


	 
SET session_replication_role = 'origin';