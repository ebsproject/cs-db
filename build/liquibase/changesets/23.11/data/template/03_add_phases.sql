--liquibase formatted sql

--changeset postgres:03_add_phases context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2210 Update  Worflow definition fixtureDB

SET session_replication_role = 'replica';


INSERT INTO workflow.phase ("name",description,help,"sequence",tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,htmltag_id,workflow_id,depend_on,icon,wf_view_type_id,design_ref) VALUES
	 ('On Process','On Process Phase','On Process Phase',2,1,'2023-10-24 16:54:59.824','2023-10-24 22:29:53.698',122,122,false,530,1,529,'{"edges": ["529"], "width": 800, "height": 400, "position": {"x": -45.211007140393576, "y": -174.91366994705118}}',NULL,1,'5f2acb36-9b02-456c-a972-b70119db9a23'),
	 ('On Close','On Close Phase','On Close Phase',2,1,'2023-10-24 16:55:27.921','2023-10-24 22:29:53.709',122,122,false,531,1,529,'{"edges": ["530"], "width": 800, "height": 400, "position": {"x": -408.52444641201123, "y": 377.03636927516004}}',NULL,1,'7aec1d9c-454f-4c2c-9dcb-c5b0354df7dd'),
	 ('Close','Close','Close',3,1,'2023-11-10 20:18:22.228','2023-11-13 18:13:50.44',122,122,false,564,1,562,'{"edges": ["563"], "width": 800, "height": 400, "position": {"x": 1210.9686921283705, "y": 470.8050141846571}}',NULL,1,'8b760e58-0d87-47fe-9ea6-20d95576fa1c'),
	 ('Processing','Processing','Processing',2,1,'2023-11-10 20:18:05.01','2023-11-13 18:13:50.91',122,122,false,563,1,562,'{"edges": ["562"], "width": 800, "height": 400, "position": {"x": 190.55695291191273, "y": 456.32388612750333}}',NULL,1,'8edf5465-024c-4f49-889c-8f5a10fca520'),
	 ('On Process','Phase for request processing','Phase for request processing',2,1,'2023-10-19 15:48:56.717','2023-11-13 15:34:26.992',122,122,false,497,1,496,'{"edges": ["496"], "width": 800, "height": 400, "position": {"x": -824.1622597044848, "y": -322.4807110195856}}',NULL,1,'bd385247-acae-4f33-b420-bc40b5b8dc3e'),
	 ('On Close','Phase for request close','Phase for request close',3,1,'2023-10-19 15:49:31.214','2023-11-13 15:36:08.681',122,122,false,498,1,496,'{"edges": ["497"], "width": 800, "height": 400, "position": {"x": -593.387579587067, "y": 97.35213288347819}}',NULL,1,'40148ce0-b8d6-4619-888b-7dc082459369'),
	 ('On Create','On Create Phase','On Create Phase',1,1,'2023-10-24 16:54:34.561','2023-10-24 22:29:53.699',122,122,false,529,1,529,'{"edges": [], "width": 800, "height": 400, "position": {"x": -354.6436920130193, "y": -672.7127976108636}}',NULL,1,'ada30af0-43d7-41df-ba76-8dd1c0c328ab'),
	 ('Creation','Creation','Creation',1,1,'2023-11-10 20:17:44.888','2023-11-13 18:13:50.912',122,122,false,562,1,562,'{"edges": [], "width": 800, "height": 400, "position": {"x": 510.6809539537304, "y": -51.151874456923224}}',NULL,1,'f53dff05-49b7-49ad-9452-ae20d4d82ff9'),
	 ('On Creation','Phase for request creation','Phase for request creation',1,1,'2023-10-19 15:45:08.387','2023-11-13 15:36:08.686',122,122,false,496,1,496,'{"edges": [], "width": 800, "height": 400, "position": {"x": -1022.0770314100544, "y": -740.2485166693223}}',NULL,1,'7415f5e7-6ca1-4d59-b82d-a61b13e70f9f');

	 SET session_replication_role = 'origin';