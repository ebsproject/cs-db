
--liquibase formatted sql

--changeset postgres:add_fields_hierarchy_level context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2078 As a user with permissions, I should be able to create Teams for a UNIT in an organization of choice


INSERT INTO core.hierarchy_design_attributes
(cf_type_id, value, hierarchy_design_id, is_saved, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES 
(5, 'UnitLeadContact', 4, true, now(), now(), 0, 0, false),
(5, 'UnitAdminContact', 4, true, now(), now(), 0, 0, false),
(1, 'CommentsAdminContact', 4, true, now(), now(), 0, 0, false),
(1, 'CommentsLeadContact', 4, true, now(), now(), 0, 0, false),
(5, 'UnitLeadContact', 3, true, now(), now(), 0, 0, false),
(5, 'UnitAdminContact', 3, true, now(), now(), 0, 0, false),
(1, 'CommentsAdminContact',3, true, now(),now(), 0, 0, false),
(1, 'CommentsLeadContact', 3, true, now(), now(), 0, 0, false)