--liquibase formatted sql

--changeset postgres:add_fulladdress_sequencerule context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment

SET session_replication_role = 'replica';
INSERT INTO core.sequence_rule (id,"name",tenant_id,creator_id)	VALUES (8,'cs-fulladdress',1,1);

INSERT INTO core.segment_api (id,"type","method",server_url,path_template,response_mapping,body_template,tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (14,'GRAPHQL','POST','CS','graphql','data.findAddress.location','query{findAddress(id:{}){location}}',1,'now()',1,false),
	       (15,'GRAPHQL','POST','CS','graphql','data.findAddress.region','query{findAddress(id:{}){region}}',1,'now()',1,false),
	       (16,'GRAPHQL','POST','CS','graphql','data.findAddress.zipCode','query{findAddress(id:{}){zipCode}}',1,'now()',1,false),
	       (17,'GRAPHQL','POST','CS','graphql','data.findAddress.streetAddress','query{findAddress(id:{}){streetAddress}}',1,'now()',1,false);

INSERT INTO core.rule_segment (id,"name",requires_input,data_type,segment_api_id,tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (30,'address-location',true,'TEXT',14,1,'now()',1,false),
	       (31,'address-region',true,'TEXT',15,1,'now()',1,false),
	       (32,'address-zipCode',true,'TEXT',16,1,'now()',1,false),
	       (33,'address-streetAddress',true,'TEXT',17,1,'now()',1,false);

INSERT INTO core.sequence_rule_segment (id,"position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creator_id,is_required)
	VALUES (39,1,0,33,8,1,1,true),
	       (40,2,0,26,8,1,1,true),
	       (41,3,0,25,8,1,1,true),
	       (42,4,0,32,8,1,1,false),
	       (43,5,0,25,8,1,1,true),
	       (44,6,0,31,8,1,1,false),
	       (45,7,0,25,8,1,1,true),
	       (46,8,0,30,8,1,1,false);


SET session_replication_role = 'origin';