--liquibase formatted sql

--changeset postgres:add_fullname_sequencerule context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment...

SET session_replication_role = 'replica';
-- add full name
ALTER TABLE core.sequence_rule_segment ADD is_required bool NOT NULL DEFAULT true;
COMMENT ON COLUMN core.sequence_rule_segment.is_required IS 'defines if the segment can be ignored when its value is not resolved or is NULL';

INSERT INTO core.sequence_rule (id,"name",tenant_id,creator_id) VALUES (7,'cs-fullname',1,1);


INSERT INTO core.segment_api (id,"type","method",server_url,path_template,response_mapping,body_template,tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (11,'GRAPHQL','POST','CS','graphql','data.findContact.person.familyName','query{findContact(id:{}){id,person{familyName}}}',1,now(),1,false),
	       (12,'GRAPHQL','POST','CS','graphql','data.findContact.person.givenName','query{findContact(id:{}){id,person{givenName}}}',1,now(),1,false),
	       (13,'GRAPHQL','POST','CS','graphql','data.findContact.person.additionalName','query{findContact(id:{}){id,person{additionalName}}}',1,now(),1,false);

INSERT INTO core.rule_segment (id,"name",requires_input,formula,data_type,tenant_id,creator_id,is_void)
	VALUES (25,'blank',false,' ','TEXT',1,1,false),
	       (26,'colon',false,',','TEXT',1,1,false);
INSERT INTO core.rule_segment (id,"name",requires_input,data_type,segment_api_id,tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (27,'contact-familyName',true,'TEXT',11,1,'now()',1,false),
	       (28,'contact-givenName',true,'TEXT',12,1,'now()',1,false),
	       (29,'contact-additionalName',true,'TEXT',13,1,'now()',1,false);


INSERT INTO core.sequence_rule_segment (id,"position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creation_timestamp,creator_id,is_void,is_displayed, is_required)
	VALUES (32,1,0,27,7,1,'now()',1,false,true,true),
	       (33,2,0,26,7,1,'now()',1,false,true,true),
	       (34,3,0,25,7,1,'now()',1,false,true,true),
	       (35,4,0,28,7,1,'now()',1,false,true,true),
	       (36,5,0,26,7,1,'now()',1,false,true,true),
	       (37,6,0,25,7,1,'now()',1,false,true,true),
	       (38,7,0,29,7,1,'now()',1,false,true,false);


   SET session_replication_role = 'origin';