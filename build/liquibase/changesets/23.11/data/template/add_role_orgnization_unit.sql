--liquibase formatted sql

--changeset postgres:add_role_organization_unit context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment

insert into "security".role_product_function (role_id,product_function_id) 
	  	   select coalesce(rpf.role_id,2) as rid, pf.id  from core.product p 
	        join "security".product_function pf on p.id =pf.product_id 
	        left outer join "security".role_product_function rpf on pf.id =rpf.product_function_id and rpf.role_id =2
	   		where "path" = 'organization-units' and rpf.role_id  is null;

insert into "security".role_product_function (role_id,product_function_id) 
	  	   select coalesce(rpf.role_id,5) as rid, pf.id  from core.product p 
	        join "security".product_function pf on p.id =pf.product_id 
	        left outer join "security".role_product_function rpf on pf.id =rpf.product_function_id and rpf.role_id =5
	   		where "path" = 'organization-units' and rpf.role_id  is null;