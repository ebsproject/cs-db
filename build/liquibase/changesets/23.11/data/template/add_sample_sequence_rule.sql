--liquibase formatted sql

--changeset postgres:add_sample_sequence_rule context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1816 Build Rules Engine in CS

SET session_replication_role = 'replica';

INSERT INTO core.sequence_rule (id,"name",tenant_id,creator_id) VALUES (6,'sm-sample',1,1);
INSERT INTO core.segment_sequence (id,lowest,"increment","last",tenant_id,creator_id,is_void)
	VALUES (10,1,1,0,1,1,false);
INSERT INTO core.rule_segment (id,"name",requires_input,data_type,segment_sequence_id,tenant_id,creation_timestamp,creator_id,is_void, format)
	VALUES (24,'sample-sequence',false,'TEXT',10,1,'2023-11-21 19:11:08.996',1,false, '000000');

INSERT INTO core.sequence_rule_segment (id,"position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creator_id,is_displayed)
	VALUES (30,1,0,24,6,1,1,true),
		(31,2,1,21,6,1,1,false);

UPDATE core.sequence_rule_segment SET rule_segment_id=7 WHERE id in(17,25,28);

 SET session_replication_role = 'origin';