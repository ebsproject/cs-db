--liquibase formatted sql

--changeset postgres:add_workflow_types context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2210 Update  Worflow definition fixtureDB


SET session_replication_role = 'replica';

INSERT INTO workflow.wf_view_type (id,"name",notes,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void) VALUES
	 (1,'N/A',NULL,1,'2023-11-06 12:20:11.54835',NULL,1,NULL,false),
	 (2,'Form',NULL,1,'2023-11-06 12:20:11.54835',NULL,1,NULL,false),
	 (3,'TabHorizontal',NULL,1,'2023-11-06 12:20:11.54835',NULL,1,NULL,false),
	 (4,'TabVertical',NULL,1,'2023-11-06 12:20:11.54835',NULL,1,NULL,false),
	 (5,'Simple',NULL,1,'2023-11-06 12:20:11.54835',NULL,1,NULL,false)	 
	   ON CONFLICT ON CONSTRAINT "PK_wf_view_type" DO NOTHING;

SET session_replication_role = 'origin';