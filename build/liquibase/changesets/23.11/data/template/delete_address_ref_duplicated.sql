--liquibase formatted sql

--changeset postgres:delete_address_ref_duplicated context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment


delete from crm.contact_address a
using (select ca.address_id, count(*),min(ca.contact_id) as minContact from crm.contact c inner join crm.institution i on c.id =i.contact_id 
inner join crm.contact_address ca on c.id = ca.contact_id 
where c.category_id = 2 
group by ca.address_id 
having count(*)> 1) todel
where a.contact_id > todel.minContact and a.address_id = todel.address_id;