--liquibase formatted sql

--changeset postgres:enable_organization_unit_module context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment

	  update core.product 
	  set is_void =false
	  where "path" = 'organization-units';

    	   update "security".product_function pf
	   set is_void = false 
	   from core.product p 	        
	   where p."path" = 'organization-units' and pf.product_id = p.id ;