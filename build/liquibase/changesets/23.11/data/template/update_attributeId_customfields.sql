--liquibase formatted sql

--changeset postgres:update_attributeid_customfields context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment..


with queryAtt as (
select a.id,
case when a."name" = 'submition_date' then 'submitionDate'
     when a."name" = 'sender_id' then 'sender'
     when a."name" = 'requestor_id' then 'requestor'
     when a."name" = 'recipient_id' then 'recipient'
     when a."name" = 'serviceprovider_id' then 'serviceProvider'
     when a."name" = 'program_id' then 'program'
     when a."name" = 'request_code' then 'requestCode'
     else a."name" 
end as refName
from core.entity_reference er 
		 join core."attributes" a on er.id = a.entityreference_id 
where er.entity ='service' and er.entity_schema ='workflow'
 ) 
 update workflow.node_cf 
 set attributes_id = queryAtt.id
 from queryAtt
 where queryAtt.refName = node_cf."name";