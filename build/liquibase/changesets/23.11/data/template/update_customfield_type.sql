--liquibase formatted sql

--changeset postgres:update_customfield_type context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment


update workflow.cf_type
set "type"='String';