--liquibase formatted sql

--changeset postgres:update_entity_ref context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment

SELECT core.update_entity_ref();