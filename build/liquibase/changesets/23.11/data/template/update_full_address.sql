--liquibase formatted sql

--changeset postgres:update_full_address context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment

update crm.address
set full_address = concat(street_address,',',zip_code,' ',region, ' ', location );