--liquibase formatted sql

--changeset postgres:update_full_name context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment

update crm.person
set full_name= concat(family_name,', ', given_name, ',', coalesce(additional_name,''));