--liquibase formatted sql

--changeset postgres:update_sequence_for_rules context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1816 Build Rules Engine in CS


SELECT setval('core.sequence_rule_id_seq', (SELECT MAX(id) FROM core.sequence_rule));
SELECT setval('core.segment_sequence_id_seq', (SELECT MAX(id) FROM core.segment_sequence));
SELECT setval('core.rule_segment_id_seq', (SELECT MAX(id) FROM core.rule_segment));
 