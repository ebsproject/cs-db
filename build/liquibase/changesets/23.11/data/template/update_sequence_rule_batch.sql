
--liquibase formatted sql

--changeset postgres:update_sequence_rule_batch context:template splitStatements:false rollbackSplitStatements:false
--comment:  SM-1990 implement rule for batch code

UPDATE core.rule_segment SET format='yyyy_MM_dd_hh:mm:ss' WHERE id=23;

DELETE FROM core.sequence_rule_segment WHERE id in(21,22,26,27);

INSERT INTO core.sequence_rule_segment (id,"position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creation_timestamp,creator_id,is_void,is_displayed)
	VALUES (29,6,1,21,2,1,now(),1,false,false);