--liquibase formatted sql

--changeset postgres:update_sequences context:template splitStatements:false rollbackSplitStatements:false
--comment:  SM-1988 generate sequence segment able to keep multiple sequences based on arbitrary segment values

update core.sequence_rule_segment set segment_family = 0;