--liquibase formatted sql

--changeset postgres:add_contact_workflow context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2213 Create relationship between service provider and workflow



CREATE TABLE "crm"."contact_workflow" ("id" SERIAL NOT NULL, "contact_id" INTEGER NOT NULL, "workflow_id" INTEGER NOT NULL, "provide" BOOLEAN DEFAULT TRUE NOT NULL, "creation_timestamp" TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW() NOT NULL, "modification_timestamp" TIMESTAMP WITHOUT TIME ZONE, "creator_id" INTEGER NOT NULL, "modifier_id" INTEGER, "is_void" BOOLEAN DEFAULT FALSE NOT NULL, CONSTRAINT "PK_{table}" PRIMARY KEY ("id"));

ALTER TABLE "crm"."contact_workflow" ADD CONSTRAINT "fk_contact_workflow_contact" FOREIGN KEY ("contact_id") REFERENCES "crm"."contact" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "crm"."contact_workflow" ADD CONSTRAINT "fk_contact_workflow_workflow" FOREIGN KEY ("workflow_id") REFERENCES "workflow"."workflow" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

