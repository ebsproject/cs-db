--liquibase formatted sql

--changeset postgres:add_fields_service context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment


ALTER TABLE "workflow"."service" ADD "serviceprovider_id" INTEGER;

COMMENT ON COLUMN "workflow"."service"."serviceprovider_id" IS 'Relationshipt with service unit';

ALTER TABLE "workflow"."service" ADD CONSTRAINT "fk_service_contact" FOREIGN KEY ("serviceprovider_id") REFERENCES "crm"."contact" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

