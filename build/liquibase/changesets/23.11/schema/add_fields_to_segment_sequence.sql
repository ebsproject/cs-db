--liquibase formatted sql

--changeset postgres:add_fields_to_segment_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1988 generate sequence segment able to keep multiple sequences based on arbitrary segment values


ALTER TABLE core.sequence_rule_segment ADD is_displayed bool NOT NULL DEFAULT true;
ALTER TABLE core.segment_sequence ADD parent_id int NULL;
ALTER TABLE core.segment_sequence ADD family_key varchar(100) NULL;

ALTER TABLE core.segment_sequence ADD CONSTRAINT segment_sequence_segment_sequence_fk FOREIGN KEY (parent_id) REFERENCES core.segment_sequence(id);