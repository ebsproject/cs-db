--liquibase formatted sql

--changeset postgres:add_file_type_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2105 Add file type in Shipment Manager


CREATE TABLE "workflow"."file_type" ("id" SERIAL NOT NULL, "description" VARCHAR(50), "creation_timestamp" TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW() NOT NULL, "modification_timestamp" TIMESTAMP WITHOUT TIME ZONE, "creator_id" INTEGER NOT NULL, "modifier_id" INTEGER, "is_void" BOOLEAN DEFAULT FALSE NOT NULL, "workflow_id" INTEGER NOT NULL, CONSTRAINT "PK_file_type" PRIMARY KEY ("id"));

COMMENT ON COLUMN "workflow"."file_type"."workflow_id" IS 'relationship between workflow and file types';

ALTER TABLE "workflow"."files" ADD "file_type_id" INTEGER;

COMMENT ON COLUMN "workflow"."files"."file_type_id" IS 'relationship between file_type and files';

ALTER TABLE "workflow"."file_type" ADD CONSTRAINT "fk_file_type_workflow" FOREIGN KEY ("workflow_id") REFERENCES "workflow"."workflow" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "workflow"."files" ADD CONSTRAINT "fk_files_file_type" FOREIGN KEY ("file_type_id") REFERENCES "workflow"."file_type" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

