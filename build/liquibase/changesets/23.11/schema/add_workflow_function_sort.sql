--liquibase formatted sql

--changeset postgres:add_workflow_function_sort context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment


drop function IF EXISTS workflow.sortpredicate;


CREATE OR REPLACE FUNCTION workflow.sortpredicate(_sorts json)
 RETURNS text
 LANGUAGE plpgsql
AS $$

	declare recSort record;
		   strSort text:= '';
	BEGIN
	
 
		
 for recSort in 
    select * from json_to_recordset(_sorts) as x(col text, mod text)
 loop  
    
   if strpos(strSort ,'tmp_query') >0 then
     strSort := strSort || ' , ';
  end if;
 
   --add sql SORTS
   if recSort.mod = 'ASC' then
  		strSort := strSort  || ' tmp_query.' || quote_ident(recSort.col) || '  ASC';	  	   
   end if;
  
   if recSort.mod = 'DESC' then
  		strSort := strSort || ' tmp_query.' || quote_ident(recSort.col) || ' DESC ';	  	   
   end if;
  
  
   raise notice 'where SORT: %', strSort;
  
 end loop;

return strSort;
	END;
$$
;