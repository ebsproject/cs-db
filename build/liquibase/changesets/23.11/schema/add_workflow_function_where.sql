--liquibase formatted sql

--changeset postgres:add_workflow_function_where context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-1868 generate and display derived information of the shipment...


drop function IF EXISTS workflow.wherepredicate;

CREATE OR REPLACE FUNCTION workflow.wherepredicate(_filters json)
 RETURNS text
 LANGUAGE plpgsql
AS $$

declare recFilter record;
	 strWhere text:= '';
	 
	BEGIN
	
 
		
 for recFilter in 
    select * from json_to_recordset(_filters) as x(col text, mod text, val text)
 loop  
 
   
   --add sql conditionals
  
  if strpos(strWhere ,'tmp_query') >0 then
     strWhere := strWhere || ' and ';
  end if;
  
   if recFilter.mod = 'EQ' then
   		
   		--or strpos(recFilter.col,'requestor') > 0  or strpos(recFilter.col,'recipient') > 0 or  strpos(recFilter.col,'serviceProvider') > 0  or  strpos(recFilter.col,'program') > 0
   		if( strpos(recFilter.col,'.') > 0  ) then
   	    	strWhere := strWhere  || ' tmp_query.' || quote_ident(split_part(recFilter.col,'.',1)) || ' ->> ' || quote_literal(split_part(recFilter.col,'.',2)) || '  = ' || quote_literal(recFilter.val);
   		else
   		strWhere := strWhere  || ' tmp_query.' || quote_ident(recFilter.col) || '  = ' || quote_literal(recFilter.val);
   		end if;
   	
  			  	   
   end if;
  
     if recFilter.mod = 'LK' then
		if( strpos(recFilter.col,'.') > 0  ) then
		strWhere := strWhere  || ' tmp_query.' || quote_ident(split_part(recFilter.col,'.',1)) || ' ->> ' || quote_literal(split_part(recFilter.col,'.',2)) || '  LIKE ' ||  quote_literal('%' || recFilter.val || '%');	
	   		
	   		else
	  		strWhere := strWhere || ' tmp_query.' || quote_ident(recFilter.col) || ' LIKE ' || quote_literal('%' || recFilter.val || '%');	  	   
	  	end if;
   end if;
  
  
  
   raise notice 'where clausula: %', strWhere;
  
 end loop;

return strWhere;
	END;
$$
;