--liquibase formatted sql

--changeset postgres:update_sequences_restore context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2210 Update  Worflow definition fixtureDB



SELECT setval('workflow.workflow_id_seq', (SELECT MAX(id) FROM workflow."workflow"));
SELECT setval('workflow.phase_id_seq', (SELECT MAX(id) FROM workflow."phase"));
SELECT setval('workflow.stage_id_seq', (SELECT MAX(id) FROM workflow."stage"));
SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM workflow."node"));
SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM workflow."node_cf"));
SELECT setval('workflow.status_type_id_seq', (SELECT MAX(id) FROM workflow."status_type"));

SELECT setval('workflow.event_id_seq', (SELECT MAX(id) FROM workflow."event"));
SELECT setval('workflow.wf_instance_id_seq', (SELECT MAX(id) FROM workflow."wf_instance"));
SELECT setval('workflow.service_id_seq', (SELECT MAX(id) FROM workflow."service"));
SELECT setval('workflow.items_id_seq', (SELECT MAX(id) FROM workflow."items"));
SELECT setval('workflow.files_id_seq', (SELECT MAX(id) FROM workflow."files"));
SELECT setval('workflow.file_type_id_seq', (SELECT MAX(id) FROM workflow."file_type"));
SELECT setval('crm.contact_workflow_id_seq', (SELECT MAX(id) FROM crm."contact_workflow"));

