--liquibase formatted sql

--changeset postgres:workflow_clean_restore_03 context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:CONTINUE onError:WARN
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT id FROM public.databasechangelog WHERE id = 'workflow_clean_restoretwo') WHEN TRUE THEN 1 ELSE 0 END;
--comment: CS-2210 Update  Worflow definition fixtureDB


--delete  data shipment from old module
delete from shm.shipment_file;
delete from shm.shipment_item;
delete from shm.shipment;

--delete transactions
delete from workflow.items;
delete from workflow.files;
delete from workflow.service;


--delete events
delete from workflow.cf_value;
delete from workflow."event";
delete from workflow.status;
delete from workflow.wf_instance;

-- delete design
delete from workflow.status_type ;
delete from workflow.file_type;

delete from workflow.node_cf;
delete from workflow.node_stage;
delete from workflow.node;

delete from workflow.stage;
delete from workflow.phase;
delete from workflow.tenant_workflow;
delete from crm.contact_workflow;
delete from workflow.workflow;

