--liquibase formatted sql

--changeset postgres:01_modify_address_rule context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2295 Update Naming Rule for Address adding additionalStreetAddress

SET session_replication_role = 'replica';


INSERT INTO core.segment_api (id,"type","method",server_url,path_template,response_mapping,body_template,tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (18,'GRAPHQL','POST','CS','graphql','data.findAddress.additionalStreetAddress','query{findAddress(id:{}){additionalStreetAddress}}',1,'now()',1,false);
SELECT setval('core.segment_api_id_seq', (SELECT MAX(id) FROM core.segment_api));


INSERT INTO core.rule_segment (id,"name",requires_input,data_type,segment_api_id,tenant_id,creator_id,is_void)
	VALUES (34,'address-additionalStreetAddress',true,'TEXT',18,1,1,false);
SELECT setval('core.rule_segment_id_seq', (SELECT MAX(id) FROM core.rule_segment));

INSERT INTO core.sequence_rule_segment (id,"position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creation_timestamp,creator_id,is_void,is_displayed,is_required)
	VALUES (47,2,0,25,8,1,'now()',1,false,true,false);
INSERT INTO core.sequence_rule_segment (id,"position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creation_timestamp,creator_id,is_void,is_displayed,is_required)
	VALUES (48,3,0,34,8,1,'now()',1,false,true,false);

UPDATE core.sequence_rule_segment	SET "position"=8	WHERE id=44;
UPDATE core.sequence_rule_segment	SET "position"=9	WHERE id=45;
UPDATE core.sequence_rule_segment	SET "position"=10	WHERE id=46;
UPDATE core.sequence_rule_segment	SET "position"=7	WHERE id=43;
UPDATE core.sequence_rule_segment	SET "position"=6	WHERE id=42;
UPDATE core.sequence_rule_segment	SET "position"=5	WHERE id=41;
UPDATE core.sequence_rule_segment	SET "position"=4	WHERE id=40;

SELECT setval('core.sequence_rule_segment_id_seq', (SELECT MAX(id) FROM core.sequence_rule_segment));
	 
SET session_replication_role = 'origin';