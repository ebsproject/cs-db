--liquibase formatted sql

--changeset postgres:02_add_blank_sample_rule context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2296 add blank sample rule

do $$
	declare seq_id integer; 
begin
	
	INSERT INTO core.sequence_rule (id,"name",tenant_id,creation_timestamp,creator_id,is_void)
		VALUES (9,'sm-sample-blank',1,'now()',1,false);
	perform setval('core.sequence_rule_id_seq', (SELECT MAX(id) FROM core.sequence_rule));
	
	select max(id) + 1 into seq_id from core.segment_sequence;
	INSERT INTO core.segment_sequence (id,lowest,"increment","last",tenant_id,creation_timestamp,creator_id,is_void)
		VALUES (seq_id,1,1,0,1,'now()',1,false);
	perform setval('core.segment_sequence_id_seq', (SELECT MAX(id) FROM core.segment_sequence));
	
	INSERT INTO core.rule_segment (id,"name",requires_input,format,data_type,segment_sequence_id,tenant_id,creation_timestamp,creator_id,is_void)
		VALUES (35,'sample-blank-sequence',false,'000000','TEXT',seq_id,1,'now()',1,false);
	perform setval('core.rule_segment_id_seq', (SELECT MAX(id) FROM core.rule_segment));
	
	INSERT INTO core.sequence_rule_segment (id,"position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creation_timestamp,creator_id,is_void,is_displayed,is_required)
		VALUES (49,1,0,35,9,1,'now()',1,false,true,true);
	perform setval('core.sequence_rule_segment_id_seq', (SELECT MAX(id) FROM core.sequence_rule_segment));


end $$