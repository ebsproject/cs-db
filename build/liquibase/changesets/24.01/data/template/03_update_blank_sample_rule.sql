--liquibase formatted sql

--changeset postgres:03_update_blank_sample_rule context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2296 add blank sample rule update

SET session_replication_role = 'replica';

INSERT INTO core.rule_segment (id,"name",requires_input,format,data_type,tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (36,'sample-blank-prefix',false,'ZZ_Blank_','TEXT',1,'now()',1,false);
UPDATE core.rule_segment set
  data_type='NUMBER',
  format=null
WHERE id=35;
select setval('core.rule_segment_id_seq', (SELECT MAX(id) FROM core.rule_segment));

INSERT INTO core.sequence_rule_segment (id,"position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creation_timestamp,creator_id,is_void,is_displayed,is_required)
	VALUES (50,1,0,36,9,1,'now()',1,false,true,true);
UPDATE core.sequence_rule_segment set
  "position"=2
WHERE id=49;
select setval('core.sequence_rule_segment_id_seq', (SELECT MAX(id) FROM core.sequence_rule_segment));

SET session_replication_role = 'origin';