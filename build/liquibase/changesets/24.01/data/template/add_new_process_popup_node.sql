

--liquibase formatted sql

--changeset postgres:add_new_process_popup_node context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2304 Add new process record to be used in nodes of type Modal


SET session_replication_role = 'replica';

INSERT INTO core.process
(id, "name", description, code, is_background, call_report, tenant_id, creator_id)
VALUES
(9, 'Modal Popup', 'Modal Popup','POPUP', FALSE, FALSE, 1, 1);

SET session_replication_role = 'origin';

SELECT setval('core.process_id_seq', (SELECT MAX(id) FROM core.process));