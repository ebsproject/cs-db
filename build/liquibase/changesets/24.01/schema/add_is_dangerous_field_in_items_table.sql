--liquibase formatted sql

--changeset postgres:add_is_dangerous_field_in_items_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2152 Ability to mark a product as a dangerous good when creating a shipment

ALTER TABLE workflow.items ADD is_dangerous bool NOT NULL DEFAULT false;
COMMENT ON COLUMN workflow.items.is_dangerous IS 'Indicator whether the item is dangerous(true) or not(false)';