--liquibase formatted sql

--changeset postgres:add_institution_crop_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-2320 add institution crop data

do $$

declare _wheat int;
declare _maize int;
declare _rice int;
declare _barley int;

begin

SELECT id from core.crop where code = 'WHEAT' into _wheat;
SELECT id from core.crop where code = 'MAIZE' into _maize;
SELECT id from core.crop where code = 'RICE' into _rice;
SELECT id from core.crop where code = 'BARLEY' into _barley;

INSERT INTO crm.institution_crop 
	(institution_id, crop_id)
VALUES 
    ((SELECT id from crm.institution where legal_name = 'IRRI'), _rice),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT'), _wheat),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT'), _maize),
	((SELECT id from crm.institution where legal_name = 'IRRI Philippines Seed Health Unit'), _rice),
    ((SELECT id from crm.institution where legal_name = 'IRRI Philippines Genotyping Service Lab'), _rice),
    ((SELECT id from crm.institution where legal_name = 'IRRI Philippines Grain Quality Nutrition Service Lab'), _rice),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Mexico Seed Distribution Unit'), _wheat),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Mexico Seed Distribution Unit'), _maize),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Mexico Wheat Quality Laboratory'), _wheat),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Mexico Maize Quality Laboratory'), _maize),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Mexico Wheat Seed Health Laboratory'), _wheat),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Mexico Maize Seed Health Laboratory'), _maize),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Mexico Maize Molecular Laboratory'), _maize),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Kenya Maize Molecular Laboratory'), _maize),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT India Maize Molecular Laboratory'), _maize),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Mexico Wheat Molecular Laboratory'), _wheat),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Kenya'), _wheat),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT India'), _maize),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT Kenya'), _maize),
    ((SELECT id from crm.institution where legal_name = 'CIMMYT India'), _wheat),
    ((SELECT id from crm.institution where legal_name = 'Irrigated South-East Asia'), _rice),
    ((SELECT id from crm.institution where legal_name = 'Wheat Breeding Program'), _wheat),
    ((SELECT id from crm.institution where legal_name = 'Maize Breeding Program'), _maize),
    ((SELECT id from crm.institution where legal_name = 'Barley Test Program'), _barley);

end $$;