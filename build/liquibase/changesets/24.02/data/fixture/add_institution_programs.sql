--liquibase formatted sql

--changeset postgres:add_institution_programs context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-2320 add program data

do $$
declare purpose int;
declare category int;
declare ct_id int;

begin 

SELECT id from crm.purpose where name = 'Lab Services' INTO purpose;
SELECT id from crm.category where name = 'Internal Unit' INTO category;
SELECT id from crm.contact_type where name = 'Service Provider' INTO ct_id;


WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'irsea@cgiar.org', 2)
RETURNING id
)
INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('IRSEA', 'Irrigated South-East Asia', (select id from contact), 1, 101);
INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select id from crm.contact where email = 'irsea@cgiar.org'));
INSERT INTO crm.contact_hierarchy (contact_id, parent_id, is_principal_contact, creator_id)
VALUES ((select id from crm.contact where email = 'irsea@cgiar.org'), 74, false, 1);

WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'bwprogram@cgiar.org', 2)
RETURNING id
)
INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('BW', 'Wheat Breeding Program', (select id from contact), 1, 102);
INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select id from crm.contact where email = 'bwprogram@cgiar.org'));
INSERT INTO crm.contact_hierarchy (contact_id, parent_id, is_principal_contact, creator_id)
VALUES ((select id from crm.contact where email = 'bwprogram@cgiar.org'), 73, false, 1);

WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'keprogram@cgiar.org', 2)
RETURNING id
)
INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('KE', 'Maize Breeding Program', (select id from contact), 1, 103);
INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select id from crm.contact where email = 'keprogram@cgiar.org'));
INSERT INTO crm.contact_hierarchy (contact_id, parent_id, is_principal_contact, creator_id)
VALUES ((select id from crm.contact where email = 'keprogram@cgiar.org'), 73, false, 1);

WITH contact as (
INSERT INTO crm.contact
    (creator_id, email, category_id)
VALUES
    (1, 'btpprogram@cgiar.org', 2)
RETURNING id
)
INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code)
VALUES ('BTP', 'Barley Test Program', (select id from contact), 1, 104);
INSERT INTO crm.contact_contact_type (contact_type_id, contact_id)
VALUES (ct_id,(select id from crm.contact where email = 'btpprogram@cgiar.org'));
INSERT INTO crm.contact_hierarchy (contact_id, parent_id, is_principal_contact, creator_id)
VALUES ((select id from crm.contact where email = 'btpprogram@cgiar.org'), 73, false, 1);

end $$;