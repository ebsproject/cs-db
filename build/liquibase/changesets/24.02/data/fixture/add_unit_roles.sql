--liquibase formatted sql

--changeset postgres:add_unit_roles context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- create new unit_admin_contact and unit_lead roles

INSERT INTO "security"."role"
(description, security_group, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, "name", is_system)
VALUES('Unit Administrative Contact', 'EBS User', now(), null, 1, null, false, nextval('security."role_id_seq"'::text::regclass), 'Unit Administrative Contact', false);

INSERT INTO "security"."role"
(description, security_group, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, "name", is_system)
VALUES('Unit Leader', 'EBS User', now(), null, 1, null, false, nextval('security."role_id_seq"'::text::regclass), 'Unit Leader', false);