--liquibase formatted sql

--changeset postgres:insert_unit_type_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- Populate crm.unit_type

INSERT INTO crm.unit_type
(id, "name")
VALUES(1, 'Program'),
(2, 'Service Unit');