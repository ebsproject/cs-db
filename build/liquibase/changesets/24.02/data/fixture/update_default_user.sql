--liquibase formatted sql

--changeset postgres:update_default_user context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- update default user

update "security".user set is_admin = true where id = 1;