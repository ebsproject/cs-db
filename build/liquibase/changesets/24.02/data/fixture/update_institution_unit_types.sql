--liquibase formatted sql

--changeset postgres:update_institution_unit_types context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-2320 update institution unit types

UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'IRRI Philippines Seed Health Unit';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'IRRI Philippines Genotyping Service Lab';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'IRRI Philippines Grain Quality Nutrition Service Lab';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT Mexico Seed Distribution Unit';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT Mexico Seed Distribution Unit';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT Mexico Wheat Quality Laboratory';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT Mexico Maize Quality Laboratory';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT Mexico Wheat Seed Health Laboratory';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT Mexico Maize Seed Health Laboratory';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT Mexico Maize Molecular Laboratory';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT Kenya Maize Molecular Laboratory';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT India Maize Molecular Laboratory';
UPDATE crm.institution SET unit_type_id = 2 where legal_name = 'CIMMYT Mexico Wheat Molecular Laboratory';
UPDATE crm.institution SET unit_type_id = 1 where legal_name = 'Irrigated South-East Asia';
UPDATE crm.institution SET unit_type_id = 1 where legal_name = 'Wheat Breeding Program';
UPDATE crm.institution SET unit_type_id = 1 where legal_name = 'Maize Breeding Program';
UPDATE crm.institution SET unit_type_id = 1 where legal_name = 'Barley Test Program';