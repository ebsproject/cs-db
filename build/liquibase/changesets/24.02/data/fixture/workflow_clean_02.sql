--liquibase formatted sql

--changeset postgres:workflow_clean_02_01 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: Clean Workflow Transactions
--delete  data shipment from old module
delete from shm.shipment_file;
delete from shm.shipment_item;
delete from shm.shipment;
--delete transactions
delete from workflow.items;
delete from workflow.files;
delete from workflow.service;

--delete events
delete from workflow.cf_value;
delete from workflow."event";
delete from workflow.status;
delete from workflow.wf_instance;

