--liquibase formatted sql

--changeset postgres:add_node_stage_relationship context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2337 Add Node definition for modal popup in Cimmyt and Irri workflows
SET session_replication_role = 'replica';

INSERT INTO  workflow.node_stage (stage_id,node_id) 
VALUES (10734,21500),
	   (10566,21501);

SET session_replication_role = 'origin';