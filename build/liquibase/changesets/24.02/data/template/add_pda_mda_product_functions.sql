--liquibase formatted sql

--changeset postgres:add_pda_mda_product_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2205 Add product functions to BA Tools PDA and MDA

do $$

declare _admin_id int;
declare _ba_admin_id int;
declare _pda_prod_id int;
declare _mda_prod_id int;

begin

SELECT id from "security"."role" where name = 'Admin' INTO _admin_id;
SELECT id from "security"."role" where name = 'BA Admin' INTO _ba_admin_id;
SELECT id from core.product where name = 'Phenotypic Data Manager' INTO _pda_prod_id;
SELECT id from core.product where name = 'Molecular Data Analysis' INTO _mda_prod_id;

WITH _pda_search AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Search', true, 'Search', 1, _pda_prod_id, false)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _pda_search)),
        (_ba_admin_id, (SELECT id FROM _pda_search));

WITH _pda_print AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Print', true, 'Print', 1, _pda_prod_id, false)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _pda_print)),
        (_ba_admin_id, (SELECT id FROM _pda_print));
       
WITH _pda_export AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Export', true, 'Export', 1, _pda_prod_id, false)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _pda_export)),
        (_ba_admin_id, (SELECT id FROM _pda_export));

WITH _pda_delete AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Delete', true, 'Delete', 1, _pda_prod_id, true)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _pda_delete)),
        (_ba_admin_id, (SELECT id FROM _pda_delete));
       
WITH _pda_modify AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Modify', true, 'Modify', 1, _pda_prod_id, true)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _pda_modify)),
        (_ba_admin_id, (SELECT id FROM _pda_modify));
       
WITH _pda_read AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Read', true, 'Read', 1, _pda_prod_id, true)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _pda_read)),
        (_ba_admin_id, (SELECT id FROM _pda_read));
       
WITH _pda_create AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Create', true, 'Create', 1, _pda_prod_id, true)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _pda_create)),
        (_ba_admin_id, (SELECT id FROM _pda_create));

WITH _mda_search AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Search', true, 'Search', 1, _mda_prod_id, false)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _mda_search)),
        (_ba_admin_id, (SELECT id FROM _mda_search));

WITH _mda_print AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Print', true, 'Print', 1, _mda_prod_id, false)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _mda_print)),
        (_ba_admin_id, (SELECT id FROM _mda_print));
       
WITH _mda_export AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Export', true, 'Export', 1, _mda_prod_id, false)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _mda_export)),
        (_ba_admin_id, (SELECT id FROM _mda_export));

WITH _mda_delete AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Delete', true, 'Delete', 1, _mda_prod_id, true)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _mda_delete)),
        (_ba_admin_id, (SELECT id FROM _mda_delete));
       
WITH _mda_modify AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Modify', true, 'Modify', 1, _mda_prod_id, true)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _mda_modify)),
        (_ba_admin_id, (SELECT id FROM _mda_modify));
       
WITH _mda_read AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Read', true, 'Read', 1, _mda_prod_id, true)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _mda_read)),
        (_ba_admin_id, (SELECT id FROM _mda_read));
       
WITH _mda_create AS (
    INSERT INTO "security".product_function
        (description, system_type, "action", creator_id, product_id, is_data_action)
    VALUES
        ('Create', true, 'Create', 1, _mda_prod_id, true)
RETURNING id
)
    INSERT INTO "security".role_product_function (role_id, product_function_id)
    VALUES
        (_admin_id, (SELECT id FROM _mda_create)),
        (_ba_admin_id, (SELECT id FROM _mda_create));

end $$;