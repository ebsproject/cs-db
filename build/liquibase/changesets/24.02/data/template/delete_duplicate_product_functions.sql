--liquibase formatted sql

--changeset postgres:delete_duplicate_product_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2172 There are two View permissions in RM and it is unclear if they refer to the same or different permissions
do $$

declare _assign_id int;

begin

SELECT id from "security"."product_function" where action = 'Assign_New_Contact' INTO _assign_id;

UPDATE "security".product_function
	SET is_void=true
	WHERE id=_assign_id;
UPDATE "security".product_function
	SET is_void=true
	WHERE id=176; 

end $$;
