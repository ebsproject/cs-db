--liquibase formatted sql

--changeset postgres:insert_core_crop_values_template context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- insert crop values to core.crop

INSERT INTO "core".crop (id, code, "name", description, notes, creator_id, is_void) VALUES
	 (0,'Default','Multi-crop','Default Multi-crop','Default Multi-crop',1,false),
	 (1,'RICE','Rice','Rice crop',NULL,1,false),
	 (2,'WHEAT','Wheat','Wheat crop',NULL,1, false),
	 (3,'MAIZE','Maize','Maize crop',NULL,1, false),
	 (4,'BARLEY','Barley','Barley crop',NULL,1,false) ON CONFLICT DO NOTHING;