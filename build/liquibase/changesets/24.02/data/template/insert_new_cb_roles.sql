--liquibase formatted sql

--changeset postgres:insert_new_cb_roles_03 context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2323 Add CB new roles and permissions/actions to CS template DB

INSERT INTO "security"."role" (description,security_group,creation_timestamp,creator_id,is_void,"name",is_system)
	select 'Data Collector','CB Users','2024-02-12 09:53:49.838',1,false,'Data Collector',false 
WHERE NOT EXISTS (SELECT id FROM "security"."role" r WHERE r.name = 'Data Collector'); 
INSERT INTO "security"."role" (description,security_group,creation_timestamp,creator_id,is_void,"name",is_system)
	select 'QC Person','CB Users','2024-02-12 09:53:49.838',1,false,'QC Person',false 
WHERE NOT EXISTS (SELECT id FROM "security"."role" r WHERE r.name = 'QC Person');
INSERT INTO "security"."role" (description,security_group,creation_timestamp,creator_id,is_void,"name",is_system)
	select 'Unit QC','CB Users','2024-02-12 09:53:49.838',1,false,'Unit QC',false 
WHERE NOT EXISTS (SELECT id FROM "security"."role" r WHERE r.name = 'Unit QC');
INSERT INTO "security"."role" (description,security_group,creation_timestamp,creator_id,is_void,"name",is_system)
	select 'Unit Member','CB Users','2024-02-12 09:53:49.838',1,false,'Unit Member',false 
WHERE NOT EXISTS (SELECT id FROM "security"."role" r WHERE r.name = 'Unit Member');
INSERT INTO "security"."role" (description,security_group,creation_timestamp,creator_id,is_void,"name",is_system)
	select 'Data Curator','CB Users','2024-02-12 09:53:49.838',1,false,'Data Curator',false 
WHERE NOT EXISTS (SELECT id FROM "security"."role" r WHERE r.name = 'Data Curator');	
INSERT INTO "security"."role" (description,security_group,creation_timestamp,creator_id,is_void,"name",is_system)
	select 'Seed Manager','CB Users','2024-02-12 09:53:49.838',1,false,'Seed Manager',false 
WHERE NOT EXISTS (SELECT id FROM "security"."role" r WHERE r.name = 'Seed Manager'); 
INSERT INTO "security"."role" (description,security_group,creation_timestamp,creator_id,is_void,"name",is_system)
	select 'Unit Leader CB','CB Users','2024-02-12 09:53:49.838',1,false,'Unit Leader CB',false 
WHERE NOT EXISTS (SELECT id FROM "security"."role" r WHERE r.name = 'Unit Leader CB'); 
