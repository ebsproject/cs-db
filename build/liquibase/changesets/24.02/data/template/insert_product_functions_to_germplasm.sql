--liquibase formatted sql

--changeset postgres:insert_product_functions_to_germplasm_02 context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2323 Add CB new roles and permissions/actions to CS template DB
do $$

declare _gm_id int;

begin

SELECT id from "core"."product" where name = 'Germplasm' INTO _gm_id;

INSERT INTO "security".product_function  (description,system_type,"action",creation_timestamp,creator_id,is_void,product_id,is_data_action)
	select 'Access to all SEARCH tab functionalities',true,'GERMPLASM_SEARCH','2024-02-12 09:53:50.826',1,false,_gm_id,true
WHERE NOT EXISTS (SELECT id FROM "security".product_function r WHERE r.action = 'GERMPLASM_SEARCH'); 

INSERT INTO "security".product_function  (description,system_type,"action",creation_timestamp,creator_id,is_void,product_id,is_data_action)
	select 'Access to all UPDATE tab functionalities',true,'GERMPLASM_UPDATE','2024-02-12 09:53:50.826',1,false,_gm_id,true
WHERE NOT EXISTS (SELECT id FROM "security".product_function r WHERE r.action = 'GERMPLASM_UPDATE'); 

INSERT INTO "security".product_function  (description,system_type,"action",creation_timestamp,creator_id,is_void,product_id,is_data_action)
	select 'Access to all MERGE tab functionalities',true,'GERMPLASM_MERGE','2024-02-12 09:53:50.826',1,false,_gm_id,true
WHERE NOT EXISTS (SELECT id FROM "security".product_function r WHERE r.action = 'GERMPLASM_MERGE'); 

INSERT INTO "security".product_function  (description,system_type,"action",creation_timestamp,creator_id,is_void,product_id,is_data_action)
	select 'Access to all CREATE tab functionalities',true,'GERMPLASM_CREATE','2024-02-12 09:53:50.826',1,false,_gm_id,true
WHERE NOT EXISTS (SELECT id FROM "security".product_function r WHERE r.action = 'GERMPLASM_CREATE'); 

end $$;