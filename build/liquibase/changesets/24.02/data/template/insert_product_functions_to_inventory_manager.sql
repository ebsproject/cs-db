
--liquibase formatted sql

--changeset postgres:insert_product_functions_to_inventory_manager_02 context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2323 Add CB new roles and permissions/actions to CS template DB
do $$

declare _im_id int;

begin

SELECT id from "core"."product" where name = 'Inventory manager' INTO _im_id;

INSERT INTO "security".product_function  (description,system_type,"action",creation_timestamp,creator_id,is_void,product_id,is_data_action)
	select 'Create seeds and packages',true,'Create','2024-02-12 09:53:50.826',1,false,_im_id,false
WHERE NOT EXISTS (SELECT id FROM "security".product_function r WHERE r.description = 'Create seeds and packages'); 

INSERT INTO "security".product_function  (description,system_type,"action",creation_timestamp,creator_id,is_void,product_id,is_data_action)
	select 'Update seeds and packages',true,'Update','2024-02-12 09:53:50.826',1,false,_im_id,false
WHERE NOT EXISTS (SELECT id FROM "security".product_function r WHERE r.description = 'Update seeds and packages'); 

INSERT INTO "security".product_function  (description,system_type,"action",creation_timestamp,creator_id,is_void,product_id,is_data_action)
	select 'Transfer seed and packages between programs',true,'Seed Transfer','2024-02-12 09:53:50.826',1,false,_im_id,false
WHERE NOT EXISTS (SELECT id FROM "security".product_function r WHERE r.description = 'Transfer seed and packages between programs'); 
	
end $$;