--liquibase formatted sql

--changeset postgres:insert_role_product_function_03 context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2323 Add CB new roles and permissions/actions to CS template DB
do $$

declare _admin_id int;
declare _unit_leader_id int;
declare _unit_member_id int;
declare _collaborator_id int;
declare _data_curator_id int;
declare _seed_manager_id int;
declare _data_collector_id int;
declare _qc_person_id int;
declare _unit_qc_id int;
declare _team_member_id int;
declare _technician_id int;

declare _gm_search_id int;
declare _gm_update_id int;
declare _gm_merge_id int;
declare _gm_create_id int;
declare _im_create_id int;
declare _im_update_id int;
declare _im_seed_transfer_id int;
declare _dc_upload_id int;
declare _dc_quality_id int;
declare _dc_commit_id int;
declare _dc_download_id int;
declare _dc_delete_id int;

begin

SELECT id from "security"."role" where name = 'CB Admin' INTO _admin_id;
SELECT id from "security"."role" 
where name = 'Unit Leader CB' and "role".security_group = 'CB Users'
INTO _unit_leader_id;
SELECT id from "security"."role" where name = 'Unit Member' INTO _unit_member_id;
SELECT id from "security"."role" where name = 'Collaborator' INTO _collaborator_id;
SELECT id from "security"."role" where name = 'Data Curator' INTO _data_curator_id;
SELECT id from "security"."role" where name = 'Seed Manager' INTO _seed_manager_id;
SELECT id from "security"."role" where name = 'Team Member' INTO _team_member_id;
SELECT id from "security"."role" where name = 'Data Collector' INTO _data_collector_id;
SELECT id from "security"."role" where name = 'QC Person' INTO _qc_person_id;
SELECT id from "security"."role" where name = 'Unit QC' INTO _unit_qc_id;
SELECT id from "security"."role" where name = 'Technician' INTO _technician_id;

SELECT id from "security"."product_function" where action = 'GERMPLASM_SEARCH' INTO _gm_search_id;
SELECT id from "security"."product_function" where action = 'GERMPLASM_UPDATE' INTO _gm_update_id;
SELECT id from "security"."product_function" where action = 'GERMPLASM_MERGE' INTO _gm_merge_id;
SELECT id from "security"."product_function" where action = 'GERMPLASM_CREATE' INTO _gm_create_id;

SELECT id from "security"."product_function" where description = 'Create seeds and packages' INTO _im_create_id;
SELECT id from "security"."product_function" where description = 'Update seeds and packages' INTO _im_update_id;
SELECT id from "security"."product_function" where description = 'Transfer seed and packages between programs' INTO _im_seed_transfer_id;

SELECT id from "security"."product_function" where action = 'UPLOAD_DATA' INTO _dc_upload_id;
SELECT id from "security"."product_function" where action = 'QUALITY_CONTROL_DATA' INTO _dc_quality_id;
SELECT id from "security"."product_function" where action = 'COMMIT_TRANSACTION' INTO _dc_commit_id;
SELECT id from "security"."product_function" where action = 'DOWNLOAD_TRANSACTION_FILE' INTO _dc_download_id;
SELECT id from "security"."product_function" where action = 'DELETE_TRANSACTION' INTO _dc_delete_id;


INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _gm_search_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _gm_search_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _gm_update_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _gm_update_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _gm_merge_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _gm_merge_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _gm_create_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _gm_create_id );


INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_leader_id, _gm_search_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_leader_id and product_function_id = _gm_search_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_leader_id, _gm_update_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_leader_id and product_function_id = _gm_update_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_leader_id, _gm_merge_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_leader_id and product_function_id = _gm_merge_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_leader_id, _gm_create_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_leader_id and product_function_id = _gm_create_id );



INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_member_id, _gm_search_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_member_id and product_function_id = _gm_search_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_member_id, _gm_update_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_member_id and product_function_id = _gm_update_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_member_id, _gm_merge_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_member_id and product_function_id = _gm_merge_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_member_id, _gm_create_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_member_id and product_function_id = _gm_create_id );



INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _collaborator_id, _gm_search_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _collaborator_id and product_function_id = _gm_search_id );




INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_curator_id, _gm_search_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_curator_id and product_function_id = _gm_search_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_curator_id, _gm_update_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_curator_id and product_function_id = _gm_update_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_curator_id, _gm_merge_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_curator_id and product_function_id = _gm_merge_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_curator_id, _gm_create_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_curator_id and product_function_id = _gm_create_id );




INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _im_create_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _im_create_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _im_update_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _im_update_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _im_seed_transfer_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _im_seed_transfer_id );



INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _seed_manager_id, _im_create_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _seed_manager_id and product_function_id = _im_create_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _seed_manager_id, _im_update_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _seed_manager_id and product_function_id = _im_update_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _seed_manager_id, _im_seed_transfer_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _seed_manager_id and product_function_id = _im_seed_transfer_id );



INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _team_member_id, _im_seed_transfer_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _team_member_id and product_function_id = _im_seed_transfer_id );




INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_collector_id, _dc_upload_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_collector_id and product_function_id = _dc_upload_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_collector_id, _dc_quality_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_collector_id and product_function_id = _dc_quality_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_collector_id, _dc_commit_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_collector_id and product_function_id = _dc_commit_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_collector_id, _dc_download_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_collector_id and product_function_id = _dc_download_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_collector_id, _dc_delete_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_collector_id and product_function_id = _dc_delete_id );




INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _qc_person_id, _dc_upload_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _qc_person_id and product_function_id = _dc_upload_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _qc_person_id, _dc_quality_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _qc_person_id and product_function_id = _dc_quality_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _qc_person_id, _dc_commit_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _qc_person_id and product_function_id = _dc_commit_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _qc_person_id, _dc_download_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _qc_person_id and product_function_id = _dc_download_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _qc_person_id, _dc_delete_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _qc_person_id and product_function_id = _dc_delete_id );




INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_qc_id, _dc_upload_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_qc_id and product_function_id = _dc_upload_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_qc_id, _dc_quality_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_qc_id and product_function_id = _dc_quality_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_qc_id, _dc_commit_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_qc_id and product_function_id = _dc_commit_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_qc_id, _dc_download_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_qc_id and product_function_id = _dc_download_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_qc_id, _dc_delete_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_qc_id and product_function_id = _dc_delete_id );



INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_leader_id, _dc_upload_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_leader_id and product_function_id = _dc_upload_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_leader_id, _dc_quality_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_leader_id and product_function_id = _dc_quality_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_leader_id, _dc_commit_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_leader_id and product_function_id = _dc_commit_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_leader_id, _dc_download_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_leader_id and product_function_id = _dc_download_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_leader_id, _dc_delete_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_leader_id and product_function_id = _dc_delete_id );



INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_member_id, _dc_upload_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_member_id and product_function_id = _dc_upload_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_member_id, _dc_quality_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_member_id and product_function_id = _dc_quality_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_member_id, _dc_commit_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_member_id and product_function_id = _dc_commit_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_member_id, _dc_download_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_member_id and product_function_id = _dc_download_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _unit_member_id, _dc_delete_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _unit_member_id and product_function_id = _dc_delete_id );



INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_curator_id, _dc_upload_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_curator_id and product_function_id = _dc_upload_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_curator_id, _dc_quality_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_curator_id and product_function_id = _dc_quality_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_curator_id, _dc_commit_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_curator_id and product_function_id = _dc_commit_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_curator_id, _dc_download_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_curator_id and product_function_id = _dc_download_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _data_curator_id, _dc_delete_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _data_curator_id and product_function_id = _dc_delete_id );

end $$;