--liquibase formatted sql

--changeset postgres:updateBatchRule context:template labels:ree splitStatements:false rollbackSplitStatements:false
--comment: CS-2377 Update batch rule in rules engine for REE

UPDATE core.rule_segment SET formula='GSL_B' WHERE id=5;
DELETE FROM core.sequence_rule_segment WHERE sequence_rule_id =2 AND rule_segment_id NOT IN (5,8);