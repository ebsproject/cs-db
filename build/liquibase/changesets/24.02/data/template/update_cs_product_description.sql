--liquibase formatted sql

--changeset postgres:update_cs_product_description_01 context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2049 Include summary details in the tiles displayed related to this Tools
do $$

declare _wf_id int;
declare _ou_id int;
declare _pm_id int;

begin

SELECT id from "core"."product" where name = 'Workflow Management' INTO _wf_id;
SELECT id from "core"."product" where name = 'Organizational Units' INTO _ou_id;
SELECT id from "core"."product" where name = 'Place Manager' INTO _pm_id;

UPDATE core.product SET description='Enables the end user to create new and modify existing sites and facilities and associate geolocation and attribute data.' WHERE id=_pm_id;
UPDATE core.product SET description='Enables creation and management of workflows for both Administrative and end user interface utilization in breeding and service operations.' WHERE id=_wf_id;
UPDATE core.product SET description='Enables to create and manage units, their members and roles, for each institution.' WHERE id=_ou_id;
	
end $$;