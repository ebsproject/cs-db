--liquibase formatted sql

--changeset postgres:update_node_row_action_sequence context:template splitStatements:false rollbackSplitStatements:false
--comment: Node add

UPDATE workflow.node SET "sequence"=3 WHERE id=21104;
UPDATE workflow.node SET "sequence"=2 WHERE id=21102;
UPDATE workflow.node SET "sequence"=1 WHERE id=21099;


