--liquibase formatted sql

--changeset postgres:update_node_sequences context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2337 Add Node definition for modal popup in Cimmyt and Irri workflows

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM workflow."node"));