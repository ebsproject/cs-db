--liquibase formatted sql

--changeset postgres:update_product_functions_cb_data_actions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2350 Inventory Manager and Germplasm tool permissions are not reflected in the actions list
do $$

declare _gm_update_id int;
declare _gm_merge_id int;
declare _gm_create_id int;
declare _gm_search_id int;
declare _im_view_id int;
declare _im_create_id int;
declare _im_transfer_id int;
declare _im_update_id int;

begin

SELECT id from "security"."product_function" where action = 'GERMPLASM_UPDATE' INTO _gm_update_id;
SELECT id from "security"."product_function" where action = 'GERMPLASM_MERGE' INTO _gm_merge_id;
SELECT id from "security"."product_function" where action = 'GERMPLASM_CREATE' INTO _gm_create_id;
SELECT id from "security"."product_function" where action = 'GERMPLASM_SEARCH' INTO _gm_search_id;

SELECT id from "security"."product_function" where description = 'Browse seed/packages upload' INTO _im_view_id;
SELECT id from "security"."product_function" where description = 'Create seeds and packages' INTO _im_create_id;
SELECT id from "security"."product_function" where description = 'Transfer seed and packages between programs' INTO _im_transfer_id;
SELECT id from "security"."product_function" where description = 'Update seeds and packages' INTO _im_update_id;

UPDATE "security".product_function
	SET is_data_action=false
	WHERE id=_gm_update_id;
UPDATE "security".product_function
	SET is_data_action=false
	WHERE id=_gm_merge_id;
UPDATE "security".product_function
	SET is_data_action=false
	WHERE id=_gm_create_id;
UPDATE "security".product_function
	SET is_data_action=false
	WHERE id=_gm_search_id;

UPDATE "security".product_function
	SET is_data_action=false
	WHERE id=_im_view_id;
UPDATE "security".product_function
	SET is_data_action=false
	WHERE id=_im_create_id;
UPDATE "security".product_function
	SET is_data_action=false
	WHERE id=_im_transfer_id;
UPDATE "security".product_function
	SET is_data_action=false
	WHERE id=_gm_update_id;
UPDATE "security".product_function
	SET is_data_action=false
	WHERE id=_im_update_id;

end $$;
