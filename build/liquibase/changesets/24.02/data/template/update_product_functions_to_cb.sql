--liquibase formatted sql

--changeset postgres:update_product_functions_to_cb_01 context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2323 Add CB new roles and permissions/actions to CS template DB
do $$

declare _gm_view_id int;
declare _im_view_id int;
declare _im_create_id int;
declare _im_transfer_id int;
declare _im_update_id int;
declare _dc_delete_id int;
declare _dc_upload_id int;
declare _dc_quality_id int;
declare _dc_commit_id int;
declare _dc_download_id int;
declare _cb_admin_id int;
begin

SELECT id from "security"."product_function" where description = 'Browse germplasm list' INTO _gm_view_id;
SELECT id from "security"."product_function" where description = 'Browse seed/packages upload' INTO _im_view_id;
SELECT id from "security"."product_function" where description = 'Create seeds and packages' INTO _im_create_id;
SELECT id from "security"."product_function" where description = 'Transfer seed and packages between programs' INTO _im_transfer_id;
SELECT id from "security"."product_function" where description = 'Update seeds and packages' INTO _im_update_id;

SELECT id from "security"."role" where name = 'CB Admin' INTO _cb_admin_id;

SELECT id from "security"."product_function" where action = 'DELETE_TRANSACTION' INTO _dc_delete_id;
SELECT id from "security"."product_function" where action = 'UPLOAD_DATA' INTO _dc_upload_id;
SELECT id from "security"."product_function" where action = 'QUALITY_CONTROL_DATA' INTO _dc_quality_id;
SELECT id from "security"."product_function" where action = 'COMMIT_TRANSACTION' INTO _dc_commit_id;
SELECT id from "security"."product_function" where action = 'DOWNLOAD_TRANSACTION_FILE' INTO _dc_download_id;


UPDATE "security".product_function
	SET is_void=true
	WHERE id=_gm_view_id;
    UPDATE "security".product_function
	SET is_void=true
	WHERE id=_im_view_id;
UPDATE "security".product_function
	SET "action"='INVENTORY_MANAGER_CREATE'
	WHERE id=_im_create_id;
UPDATE "security".product_function
	SET "action"='INVENTORY_MANAGER_SEED_TRANSFER'
	WHERE id=_im_transfer_id;
UPDATE "security".product_function
	SET "action"='INVENTORY_MANAGER_UPDATE'
	WHERE id=_im_update_id;

-- INSERT INTO "security".role_product_function (role_id,product_function_id)
-- 	VALUES 
--     (_cb_admin_id,_dc_delete_id),
--     (_cb_admin_id,_dc_upload_id),
--     (_cb_admin_id,_dc_quality_id),
--     (_cb_admin_id,_dc_commit_id),
--     (_cb_admin_id,_dc_download_id);



end $$;