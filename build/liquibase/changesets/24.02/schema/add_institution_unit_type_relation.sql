--liquibase formatted sql

--changeset postgres:add_institution_unit_type_relation context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- create institution - unit_type relation

ALTER TABLE crm.institution ADD unit_type_id int4 NULL;
ALTER TABLE crm.institution ADD CONSTRAINT "FK_institution_unit_type" FOREIGN KEY (unit_type_id) REFERENCES crm.unit_type(id);