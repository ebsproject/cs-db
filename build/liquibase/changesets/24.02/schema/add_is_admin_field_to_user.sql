--liquibase formatted sql

--changeset postgres:add_is_admin_field_to_user context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- add isAdmin field to security.user table

ALTER TABLE "security"."user" ADD is_admin bool DEFAULT false NOT NULL;
COMMENT ON COLUMN "security"."user".is_admin IS 'Specifies if the user is an administrator';