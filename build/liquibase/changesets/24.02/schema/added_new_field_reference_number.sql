--liquibase formatted sql

--changeset postgres:added_new_field_reference_number context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2192 CS-shipment Manager: As a SHU user in CIMMYT , I should be able to view the GH reference Number column in the ITEMS list table/schedule

ALTER TABLE workflow.items ADD reference_number varchar(150) NULL;