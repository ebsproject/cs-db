--liquibase formatted sql

--changeset postgres:create_core_crop_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- "core".crop definition

CREATE TABLE core.crop (
	code varchar(50) NULL,
	"name" varchar(50) NULL,
	description varchar(500) NULL,
	notes varchar(50) NULL,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	modification_timestamp timestamp NULL,
	creator_id int4 NOT NULL,
	modifier_id int4 NULL,
	is_void bool NOT NULL DEFAULT false,
	id serial4 NOT NULL,
	CONSTRAINT "PK_crop" PRIMARY KEY (id)
);