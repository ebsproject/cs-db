--liquibase formatted sql

--changeset postgres:create_crm_institution_crop_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- "crm".institution_crop

CREATE TABLE crm.institution_crop (
	institution_id int4 NOT NULL,
	crop_id int4 NOT NULL,
	CONSTRAINT "PK_institution_crop" PRIMARY KEY (institution_id, crop_id),
	CONSTRAINT "FK_institution_crop_institution" FOREIGN KEY (institution_id) REFERENCES crm.institution(id),
	CONSTRAINT "FK_institution_crop_crop" FOREIGN KEY (crop_id) REFERENCES core.crop(id)
);