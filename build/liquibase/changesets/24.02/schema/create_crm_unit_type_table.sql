--liquibase formatted sql

--changeset postgres:create_crm_unit_type_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- crm.unit_type definition

CREATE TABLE crm.unit_type (
	id serial4 NOT NULL,
	"name" varchar NOT NULL,
	CONSTRAINT unit_type_pk PRIMARY KEY (id)
);