--liquibase formatted sql

--changeset postgres:create_security_role_contact_hierarchy_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- security.role_contact_hierarchy

CREATE TABLE security.role_contact_hierarchy (
	role_id int4 NOT NULL,
	contact_hierarchy_id int4 NOT NULL,
	CONSTRAINT "PK_role_contact_hierarchy" PRIMARY KEY (role_id, contact_hierarchy_id),
	CONSTRAINT "FK_role_contact_hierarchy_role" FOREIGN KEY (role_id) REFERENCES security.role(id),
	CONSTRAINT "FK_role_contact_hierarchy_contact_hierarchy" FOREIGN KEY (contact_hierarchy_id) REFERENCES crm.contact_hierarchy(id)
);