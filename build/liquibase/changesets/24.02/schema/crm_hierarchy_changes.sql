--liquibase formatted sql

--changeset postgres:crm_hierarchy_changes context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- table crm.hierarchy changes

ALTER TABLE crm."hierarchy" RENAME TO contact_hierarchy;
ALTER TABLE crm.contact_hierarchy ADD id serial4 NOT NULL;
ALTER TABLE crm.contact_hierarchy DROP CONSTRAINT "PK_hierarchy";
ALTER TABLE crm.contact_hierarchy ADD CONSTRAINT pk_contact_hierarchy PRIMARY KEY (id);
ALTER TABLE crm.contact_hierarchy RENAME CONSTRAINT "FK_hierarchy_contact" TO "FK_contact_hierarchy_contact";
ALTER TABLE crm.contact_hierarchy RENAME CONSTRAINT "FK_hierarchy_parent_contact" TO "FK_contact_hierarchy_parent";