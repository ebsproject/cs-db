--liquibase formatted sql

--changeset postgres:update_programs_category context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

do $$
declare purpose int;
declare category int;
declare ct_id int;

begin 

SELECT id from crm.category where name = 'Internal Unit' INTO category;

UPDATE crm.contact SET category_id = category where email = 'irsea@cgiar.org';
UPDATE crm.contact SET category_id = category where email = 'bwprogram@cgiar.org';
UPDATE crm.contact SET category_id = category where email = 'keprogram@cgiar.org';
UPDATE crm.contact SET category_id = category where email = 'btpprogram@cgiar.org';

end $$;