--liquibase formatted sql

--changeset postgres:add_naming_rule_for_positive_controls context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-2114 Add Naming Rule for Positive Controls
SET session_replication_role = 'replica';

INSERT INTO core.sequence_rule (id,"name",tenant_id,creator_id,is_void)
	VALUES (10,'sm-sample-control',1,1,false);

INSERT INTO core.rule_segment (id,"name",requires_input,format,data_type,tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (37,'sample-control-prefix',false,'Positive','TEXT',1,'now()',1,false);

INSERT INTO core.sequence_rule_segment ("position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creator_id)
	VALUES (1,0,37,10,1,1);


SET session_replication_role = 'origin';


