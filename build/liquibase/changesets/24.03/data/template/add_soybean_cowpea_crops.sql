--liquibase formatted sql

--changeset postgres:add_soybean_cowpea_crops context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2320 Add soybean and cowpea crops

INSERT INTO core.crop (id, code, name, description, creator_id)
SELECT 5, 'COWPEA', 'Cowpea', 'Cowpea crop', 1
WHERE NOT EXISTS (SELECT id FROM core.crop WHERE id = 5);

INSERT INTO core.crop (id, code, name, description, creator_id)
SELECT 6, 'SOYBEAN', 'Soybean', 'Soybean crop', 1
WHERE NOT EXISTS (SELECT id FROM core.crop WHERE id = 6);