--liquibase formatted sql

--changeset postgres:assign_cb_user_role context:template splitStatements:false rollbackSplitStatements:false
--comment: hotfix/assign-contact-roles

do $$
declare rec record;
declare cb_user_role_id int;
begin
	select id from security.role where name = 'CB User' into cb_user_role_id;
	for rec in (select ch.id from crm.contact_hierarchy ch
					join crm.contact c on c.id = ch.contact_id
					join crm.category cat on c.category_id = cat.id and cat.name = 'Person'
					where ch.id not in (select contact_hierarchy_id from security.role_contact_hierarchy) and ch.is_void = false) loop
		if not exists (select * from security.role_contact_hierarchy where role_id = cb_user_role_id and contact_hierarchy_id = rec.id)
		then 
			insert into security.role_contact_hierarchy (role_id, contact_hierarchy_id)
			values(cb_user_role_id, rec.id);
		end if;
	end loop;
end
$$;