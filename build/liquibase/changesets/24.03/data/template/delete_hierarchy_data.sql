--liquibase formatted sql

--changeset postgres:delete_hierarchy_data_03 context:template splitStatements:false rollbackSplitStatements:false
--comment:  CS-2373 Remove Hierarchy Product from DB, API and UI

DELETE FROM "security".role_product_function rpf 
using (
        SELECT pf.id FROM security.product_function  pf
        JOIN core.product p ON p.id = pf.product_id
        WHERE p."name" = 'Hierarchy'
) del
WHERE rpf.product_function_id = del.id;

DELETE FROM "core".printout_template_product WHERE product_id IN (SELECT id FROM core.product WHERE "name" = 'Hierarchy');

DELETE FROM "security".product_function  WHERE product_id IN (SELECT id FROM core.product WHERE "name" = 'Hierarchy');


DELETE FROM core.product WHERE "name" ='Hierarchy';

DELETE FROM workflow.cf_value WHERE nodecf_id IN (SELECT id FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Type')));

DELETE FROM workflow.cf_value WHERE nodecf_id IN (SELECT id FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy')));

DELETE FROM workflow.cf_value WHERE nodecf_id IN (SELECT id FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Tree')));

DELETE FROM workflow.cf_value WHERE nodecf_id IN (SELECT id FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Design')));

DELETE FROM workflow.cf_value WHERE nodecf_id IN (SELECT id FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Tree_Attributes_Values')));

DELETE FROM workflow.cf_value WHERE nodecf_id IN (SELECT id FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'hierarchy_design_attributes')));

DELETE FROM workflow.cf_value WHERE nodecf_id IN (SELECT id FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Tenant_Hierarchy')));


DELETE FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Type'));

DELETE FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy'));

DELETE FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Tree'));

DELETE FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Design'));

DELETE FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Tree_Attributes_Values'));

DELETE FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'hierarchy_design_attributes'));

DELETE FROM workflow.node_cf WHERE attributes_id IN (SELECT id FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Tenant_Hierarchy'));

DELETE FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Type');

DELETE FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy');

DELETE FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Tree');

DELETE FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Design');

DELETE FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Hierarchy_Tree_Attributes_Values');

DELETE FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'hierarchy_design_attributes');

DELETE FROM core."attributes" WHERE entityreference_id IN (SELECT id FROM core.entity_reference WHERE entity = 'Tenant_Hierarchy');


DELETE FROM core.hierarchy_tree_attributes_values;

DELETE FROM core.hierarchy_design_attributes;

DELETE FROM core.hierarchy_tree;

DELETE FROM core.hierarchy_design;

DELETE FROM core."hierarchy";

DELETE FROM core.hierarchy_type;

DELETE FROM core.entity_reference WHERE entity = 'Hierarchy_Design';

DELETE FROM core.entity_reference WHERE entity = 'Hierarchy';

DELETE FROM core.entity_reference WHERE entity = 'Hierarchy_Tree';

DELETE FROM core.entity_reference WHERE entity = 'Hierarchy_Type';

DELETE FROM core.entity_reference WHERE entity = 'Tenant_Hierarchy';

DELETE FROM core.entity_reference WHERE entity = 'Hierarchy_Tree_Attributes_Values';

DELETE FROM core.entity_reference WHERE entity = 'hierarchy_design_attributes';


