--liquibase formatted sql

--changeset postgres:inset_new_node_stage context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2331 CS-Shipment Manager CIMMYT and IRRI: Improve the notification message format reflected in the notification feature for Shipment transactions

SET session_replication_role = 'replica';

INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10734, 21502);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10566, 21503);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10569, 21504);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10735, 21505);


SET session_replication_role = 'origin';