--liquibase formatted sql

--changeset postgres:insert_print_preview_node_stage context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2351 Closing the printout reports in shipment manager after utilising displays an error and should be improved


SET session_replication_role = 'replica';

INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10568, 21507);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10737, 21508);

SET session_replication_role = 'origin';