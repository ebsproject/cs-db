--liquibase formatted sql

--changeset postgres:insert_stage_nodes_relationship context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2351 Closing the printout reports in shipment manager after utilising displays an error and should be improved
SET session_replication_role = 'replica';

INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10731, 21519);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10501, 21520);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10732, 21521);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10736, 21522);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10733, 21523);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10735, 21524);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10502, 21525);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10567, 21526);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10533, 21527);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10569, 21528);

SET session_replication_role = 'origin';