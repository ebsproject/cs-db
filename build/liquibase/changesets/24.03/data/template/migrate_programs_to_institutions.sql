--liquibase formatted sql

--changeset postgres:migrate_programs_to_institutions context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2320 migrate program records from program.program to crm.institution

do $$
declare rec record;
begin
	update crm.institution set external_code = NULL where unit_type_id = 1;
	for rec in (select * from program.program) loop
		if exists (select id from crm.institution where common_name = rec.code and unit_type_id = 1)
		then 
			update crm.institution set legal_name = rec.name, external_code = rec.id, unit_type_id = 1 where common_name = rec.code and unit_type_id = 1;
		else 
			with contact as (
				insert into crm.contact (creator_id, email, category_id) values (1, LOWER(rec.code) || '@cgiar.com', 4)
				returning id
			)
			INSERT INTO crm.institution (common_name, legal_name, contact_id, creator_id, external_code, unit_type_id)
			VALUES (rec.code, rec.name, (select id from contact), 1, rec.id, 1);
		end if;
	end loop;
end
$$;