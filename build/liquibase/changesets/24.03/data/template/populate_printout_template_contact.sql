--liquibase formatted sql

--changeset postgres:populate_printout_template_contact_0 context:template splitStatements:false rollbackSplitStatements:false
--comment: Populate table core.printout_template_contact

do $$
declare 
	rec record;
begin
for rec in (select printout_template_id as po, program_id as pr from core.printout_template_program) loop
	if exists (select contact_id from crm.institution where external_code = rec.pr and unit_type_id = 1 and is_void = false)
	then 
		insert into core.printout_template_contact (printout_template_id, program_id)
		values (rec.po, (select contact_id from crm.institution where external_code = rec.pr and unit_type_id = 1 and is_void = false))
		on conflict do nothing;
	end if;
end loop;
end $$;