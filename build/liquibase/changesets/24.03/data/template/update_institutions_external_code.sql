--liquibase formatted sql

--changeset postgres:update_institutions_external_code context:template splitStatements:false rollbackSplitStatements:false
--comment: hotfix/update-external-codes

update crm.institution set external_code = null where contact_id in (select id from crm.contact where category_id = 2)