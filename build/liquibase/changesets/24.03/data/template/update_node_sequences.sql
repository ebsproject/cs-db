--liquibase formatted sql

--changeset postgres:update_node_sequences context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2331 CS-Shipment Manager CIMMYT and IRRI: Improve the notification message format reflected in the notification feature for Shipment transactions

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM workflow."node"));