--liquibase formatted sql

--changeset postgres:update_node_sequences_after_insert context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2351 Closing the printout reports in shipment manager after utilising  displays an error and should be improved

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM workflow."node"));