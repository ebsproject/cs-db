--liquibase formatted sql

--changeset postgres:update_product_functions_to_cb context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2350 Inventory Manager and Germplasm tool permissions are not reflected in the actions list
do $$

declare _admin_id int;

declare _gm_create_id int;
declare _gm_update_id int;
declare _gm_merge_id int;

declare _im_create_id int;
declare _im_transfer_id int;
declare _im_update_id int;

declare _dc_delete_id int;
declare _dc_upload_id int;
declare _dc_quality_id int;
declare _dc_commit_id int;
declare _dc_download_id int;

begin

SELECT id from "security"."role" where name = 'CB Admin' INTO _admin_id;

SELECT id from "security"."product_function" where action = 'GERMPLASM_CREATE' INTO _gm_create_id;
SELECT id from "security"."product_function" where action = 'GERMPLASM_UPDATE' INTO _gm_update_id;
SELECT id from "security"."product_function" where action = 'GERMPLASM_MERGE' INTO _gm_merge_id;

SELECT id from "security"."product_function" where description = 'Create seeds and packages' INTO _im_create_id;
SELECT id from "security"."product_function" where description = 'Transfer seed and packages between programs' INTO _im_transfer_id;
SELECT id from "security"."product_function" where description = 'Update seeds and packages' INTO _im_update_id;


SELECT id from "security"."product_function" where action = 'DELETE_TRANSACTION' INTO _dc_delete_id;
SELECT id from "security"."product_function" where action = 'UPLOAD_DATA' INTO _dc_upload_id;
SELECT id from "security"."product_function" where action = 'QUALITY_CONTROL_DATA' INTO _dc_quality_id;
SELECT id from "security"."product_function" where action = 'COMMIT_TRANSACTION' INTO _dc_commit_id;
SELECT id from "security"."product_function" where action = 'DOWNLOAD_TRANSACTION_FILE' INTO _dc_download_id;



UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_gm_create_id;
UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_gm_update_id;
UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_gm_merge_id;
UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_im_create_id;
UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_im_transfer_id;
UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_im_update_id;
UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_dc_delete_id;
UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_dc_upload_id;
UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_dc_quality_id;
UPDATE "security".product_function
	SET is_data_action=true
	WHERE id=_dc_commit_id;

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _dc_upload_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _dc_upload_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _dc_quality_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _dc_quality_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _dc_commit_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _dc_commit_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _dc_download_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _dc_download_id );

INSERT INTO "security".role_product_function (role_id, product_function_id)
SELECT _admin_id, _dc_delete_id 
WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _admin_id and product_function_id = _dc_delete_id );

end $$;