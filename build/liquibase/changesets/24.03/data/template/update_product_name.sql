--liquibase formatted sql

--changeset postgres:update_product_name context:template splitStatements:false rollbackSplitStatements:false
--comment: CS-2362 Rename the component, tenant manager, to global settings
do $$
declare _tenant_product_id int;
begin
SELECT id from "core"."product" where name = 'Tenant Management' INTO _tenant_product_id;
UPDATE core.product
	SET "path"='global-settings',
		"name"='Global Settings',
		"help"='Global Settings', 
		"description"='Enable manage centers and customers at general level'
	WHERE id=_tenant_product_id;
end $$;