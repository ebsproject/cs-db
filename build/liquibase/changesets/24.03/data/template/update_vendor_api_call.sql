--liquibase formatted sql

--changeset postgres:update_vendor_api_call context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-2117 retrieve vendor from smmarkerdb api

UPDATE core.segment_api
	SET body_template='query{findServiceProvider(id:{}){id,code}}',server_url='MARKERDB',response_mapping='data.findServiceProvider.code'
	WHERE id=4;