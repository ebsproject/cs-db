--liquibase formatted sql

--changeset postgres:create_printout_template_contact_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: printout_template_contact_table

-- create table core.printout_template_contact

CREATE TABLE core.printout_template_contact (
	program_id int4 NOT NULL,
	printout_template_id int4 NOT NULL,
	CONSTRAINT "PK_printout_template_contact" PRIMARY KEY (program_id, printout_template_id),
	CONSTRAINT "FK_printout_template_contact_printout_template" FOREIGN KEY (printout_template_id) REFERENCES core.printout_template(id),
	CONSTRAINT "FK_printout_template_contact_contact" FOREIGN KEY (program_id) REFERENCES "crm"."contact"(id)
);