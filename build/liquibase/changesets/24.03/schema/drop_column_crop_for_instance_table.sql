--liquibase formatted sql

--changeset postgres:drop_column_crop_for_instance_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2365 Delete the database table that relates the crops id to the institutions

ALTER TABLE core."instance"
  DROP  CONSTRAINT "FK_instance_crop";
ALTER TABLE core."instance"
  DROP COLUMN crop_id;
