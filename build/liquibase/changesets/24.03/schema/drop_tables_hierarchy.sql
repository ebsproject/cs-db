--liquibase formatted sql

--changeset postgres:drop_tables_hierarchy context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2373 Remove Hierarchy Product from DB, API and UI


DROP TABLE IF EXISTS core.hierarchy_type CASCADE;

DROP TABLE IF EXISTS core.tenant_hierarchy CASCADE;

DROP TABLE IF EXISTS core.hierarchy_tree_attributes_values CASCADE;

DROP TABLE IF EXISTS core.hierarchy_design_attributes CASCADE;

DROP TABLE IF EXISTS core.hierarchy_tree CASCADE;

DROP TABLE IF EXISTS core.hierarchy_design CASCADE;

DROP TABLE IF EXISTS core."hierarchy" CASCADE;
