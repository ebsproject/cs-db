--liquibase formatted sql

--changeset postgres:modify_service_program_relation context:schema splitStatements:false rollbackSplitStatements:false
--comment: CS-2320

-- change workflow.service program relation

ALTER TABLE workflow.service DROP CONSTRAINT "FK_service_program";
ALTER TABLE workflow.service ADD CONSTRAINT "FK_service_program" FOREIGN KEY (program_id) REFERENCES crm.contact(id);