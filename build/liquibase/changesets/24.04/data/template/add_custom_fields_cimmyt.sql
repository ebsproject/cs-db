
--liquibase formatted sql

--changeset postgres:add_custom_fields_cimmyt context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-319  Shipment Manager- As a user at CIMMYT in Mexico I should be able to input additional information in the Basic Info tab for outgoing shipments

SET session_replication_role = 'replica';

INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('desiredShippingDate', 'Desired Shipping Date', 'Desired Shipping Date', false, 1, '2024-04-22 15:18:34.066', NULL, 1, NULL, false, 1020, 3, 1, 20662, '{"id": 3, "name": "desiredShippingDate", "sort": 6.1, "rules": {"required": ""}, "sizes": [12, 12, 6, 4, 4], "helper": {"title": "Desired Shipping Date", "placement": "top"}, "inputProps": {"label": "Desired Shipping Date", "variant": "outlined"}, "showInGrid": true, "defaultValue": ""}'::jsonb, '', NULL);
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('otherMaterial', 'Other Material', 'Other Material', false, 1, '2024-04-22 15:20:17.773', NULL, 1, NULL, false, 1021, 1, 1, 20662, '{"id": 1, "name": "otherMaterial", "sort": 6.2, "rules": {"required": ""}, "sizes": [12, 12, 6, 4, 4], "helper": {"title": "Other Material", "placement": "top"}, "inputProps": {"rows": 1, "label": "Other Material", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}}, "defaultValue": ""}'::jsonb, '', NULL);
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('mtaProcess', 'MTA Process', 'MTA Process', false, 1, '2024-04-22 15:24:08.909', NULL, 1, NULL, false, 1022, 5, 1, 20662, '{"id": 5, "sort": 6.3, "rules": {"required": ""}, "sizes": [12, 12, 6, 4, 4], "entity": "Contact", "helper": {"title": "MTA Process", "placement": "top"}, "columns": [{"accessor": ""}], "filters": [{"col": "", "mod": "EQ", "val": ""}], "apiContent": ["id", "name"], "inputProps": {"color": "primary", "label": "MTA Process", "variant": "outlined"}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "person", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "category.name", "customFilters": [], "parentControl": "control name"}, "defaultOptions": [{"label": "Click Wrap", "value": 1}, {"label": "Shrink Wrap", "value": 2}, {"label": "Signature", "value": 3}]}'::jsonb, '', NULL);

SET session_replication_role = 'origin';