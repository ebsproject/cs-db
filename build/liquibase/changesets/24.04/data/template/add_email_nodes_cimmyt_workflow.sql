
--liquibase formatted sql

--changeset postgres:add_email_nodes_cimmyt_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-314 CS-Shipment Manager-IRRI & CIMMYT: Update to this Email format for  emails sent out on Shipment Manager Transactions

SET session_replication_role = 'replica';

INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email submitted cimmyt', 'Email notification', 'Email notification', 4, false, 1, '2024-04-25 21:32:31.975', '2024-04-25 21:51:58.635', 1, 1, false, 21629, 1, NULL, 496, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "11", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["20798"], "width": 100, "height": 50, "position": {"x": 76, "y": 109.36734970343969}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, '90b8ea49-1316-4c9c-b70b-61c6afb3167e'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email processing cimmyt', 'Email status processing', 'Email status processing', 3, false, 1, '2024-04-25 21:42:24.680', '2024-04-25 21:51:58.747', 1, 1, false, 21630, 1, NULL, 496, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["20797"], "width": 100, "height": 50, "position": {"x": 166.99999999999977, "y": 130}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "93", "code": "BW", "name": "BW Wheat Breeding Program", "__typename": "Program"}, {"id": "94", "code": "KE", "name": "KE Maize Breeding Program", "__typename": "Program"}, {"id": "95", "code": "BTP", "name": "Barley Test Program", "__typename": "Program"}, {"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, '8572b8cf-0b83-4be4-ae16-ff2a99d7b7cd'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email on-hold cimmyt', 'Email status on-hold', 'Email status on-hold', 3, false, 1, '2024-04-25 21:44:33.968', '2024-04-25 21:51:58.732', 1, 1, false, 21631, 1, NULL, 496, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["20802"], "width": 100, "height": 50, "position": {"x": 660.1632689283215, "y": 118.92190129982214}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}, {"id": "93", "code": "BW", "name": "BW Wheat Breeding Program", "__typename": "Program"}, {"id": "94", "code": "KE", "name": "KE Maize Breeding Program", "__typename": "Program"}, {"id": "95", "code": "BTP", "name": "Barley Test Program", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, 'c6e4d90a-47b0-4469-b938-c94d27d206aa'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email processing completed cimmyt', 'Email status processing completed', 'Email status processing completed', 4, false, 1, '2024-04-25 21:46:25.547', '2024-04-25 21:51:58.740', 1, 1, false, 21632, 1, NULL, 496, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["20801"], "width": 100, "height": 50, "position": {"x": 93.12899837869236, "y": 128.68049208922707}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, 'fc4abdfb-c48d-4d2c-84d8-3638db1f7a4c'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email rejected cimmyt', 'Email status rejected', 'Email status rejected', 3, false, 1, '2024-04-25 21:46:30.337', '2024-04-25 21:51:58.740', 1, 1, false, 21633, 1, NULL, 496, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["20794"], "width": 100, "height": 50, "position": {"x": 429.6035156257801, "y": 124.7219683569084}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, '2b4eaa60-7b71-48dd-8a20-4b576856f5d3'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email sent cimmyt', 'Email status sent', 'Email status sent', 2, false, 1, '2024-04-25 21:49:38.304', '2024-04-25 21:51:58.702', 1, 1, false, 21634, 1, NULL, 496, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["20960"], "width": 100, "height": 50, "position": {"x": 389.9999999999998, "y": 80}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, '356a8d8e-a506-4b39-8298-4a800b1a163c'::uuid);

SET session_replication_role = 'origin';

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM "workflow"."node"));


UPDATE workflow.node
	SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "20",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=20696;



INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10567, 21629);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10533, 21630);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10566, 21631);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10566, 21632);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10566, 21633);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10569, 21634);