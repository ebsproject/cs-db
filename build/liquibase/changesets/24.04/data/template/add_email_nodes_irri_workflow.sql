
--liquibase formatted sql

--changeset postgres:add_email_nodes_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-314 CS-Shipment Manager-IRRI & CIMMYT: Update to this Email format for  emails sent out on Shipment Manager Transactions

SET session_replication_role = 'replica';

INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email submitted irri', 'Send email submitted irri', 'Send email submitted irri', 4, false, 1, '2024-04-25 22:15:42.282', '2024-04-25 22:26:29.785', 1, 1, false, 21635, 1, NULL, 562, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "11", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21070"], "width": 100, "height": 50, "position": {"x": 112.99999999999989, "y": 101}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, '2944f883-4dc7-4f6a-950f-8fa523042617'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email processing irri', 'Send email processing irri', 'Send email processing irri', 3, false, 1, '2024-04-25 22:16:52.634', '2024-04-25 22:26:29.704', 1, 1, false, 21636, 1, NULL, 562, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21027"], "width": 100, "height": 50, "position": {"x": 116.20716872212711, "y": 109.19743830413654}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, 'f9e1c099-f86d-4d08-b91a-67a73b47828a'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email processing completed irri', 'Send email processing completed irri', 'Send email processing completed irri', 4, false, 1, '2024-04-25 22:18:14.356', '2024-04-25 22:26:29.707', 1, 1, false, 21637, 1, NULL, 562, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21099"], "width": 100, "height": 50, "position": {"x": 117.97764536972113, "y": 126.55390493500886}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, 'a935d7bd-e15a-4fa4-aefb-f62e4c4aa037'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email rejected irri', 'Send email rejected irri', 'Send email rejected irri', 4, false, 1, '2024-04-25 22:18:16.214', '2024-04-25 22:26:29.709', 1, 1, false, 21638, 1, NULL, 562, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21104"], "width": 100, "height": 50, "position": {"x": 374.13737853405996, "y": 130}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, '99775475-108b-4095-9e25-d8a3a844cbbe'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email on-hold irri', 'Send email on-hold irri', 'Send email on-hold irri', 4, false, 1, '2024-04-25 22:18:18.322', '2024-04-25 22:26:29.711', 1, 1, false, 21639, 1, NULL, 562, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21102"], "width": 100, "height": 50, "position": {"x": 663.6093639933126, "y": 130}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, '19fa08e0-7baf-4516-8b63-6836650487c9'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send email sent irri', 'Send email sent irri', 'Send email sent irri', 3, false, 1, '2024-04-25 22:21:46.784', '2024-04-25 22:26:29.681', 1, 1, false, 21640, 1, NULL, 562, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "10", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contactEmail": ""}], "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21108"], "width": 100, "height": 50, "position": {"x": 289.6802486597272, "y": 113.43491774985182}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}]}'::jsonb, NULL, 'javascript', 1, 4, '685af9b7-5a75-47b1-a26c-55e6004cd638'::uuid);

SET session_replication_role = 'origin';

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM "workflow"."node"));

-- Auto-generated SQL script #202404251629
UPDATE workflow.node
	SET "name"='Send created email irri',define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "20",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21031;

INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10736, 21635);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10733, 21636);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10734, 21637);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10734, 21638);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10734, 21639);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10735, 21640);
