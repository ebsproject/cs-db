
--liquibase formatted sql

--changeset postgres:add_email_template_create_updated context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-314 CS-Shipment Manager-IRRI & CIMMYT: Update to this Email format for  emails sent out on Shipment Manager Transactions

SET session_replication_role = 'replica';

INSERT INTO core.email_template
("name", subject, "template", creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, person_id)
VALUES('Shipment Created Template', 'New Shipment Seed Outgoing request', '<p class="ql-align-center">Hi,</p><p class="ql-align-center">[date] A new Outgoing Seed Shipment [requestCode] was created by [currentUser]</p><p class="ql-align-center"><br></p><p class="ql-align-center">Enterprise Breeding System.</p>', '2024-04-24 14:06:21.601', '2024-04-24 14:15:13.291', 1, 1, false, 20, NULL);
INSERT INTO core.email_template
("name", subject, "template", creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, person_id)
VALUES('Shipment Updated Template', 'Shipment Seed Outgoing updated', '<p class="ql-align-center">Hi,</p><p class="ql-align-center">[date] The Outgoing Seed Shipment [requestCode] was modified by [currentUser].</p><p class="ql-align-center">Enterprise Breeding System</p>', '2024-04-24 14:08:30.708', '2024-04-24 14:15:54.707', 1, 1, false, 21, NULL);

SET session_replication_role = 'origin';

SELECT setval('core.email_template_id_seq', (SELECT MAX(id) FROM "core"."email_template"));