
--liquibase formatted sql

--changeset postgres:add_email_templates context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-314 CS-Shipment Manager-IRRI & CIMMYT: Update to this Email format for  emails sent out on Shipment Manager Transactions

SET session_replication_role = 'replica';

INSERT INTO core.email_template
("name", subject, "template", creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, person_id)
VALUES('Shipment Status Template', 'Shipment Outgoing Seed status changed', '<h2 class="ql-align-center">Hi,</h2><h2 class="ql-align-center"><strong>[date]</strong> The status of the Outgoing seed shipment has been changed to <strong>[status]</strong> by <strong>[currentUser]</strong></h2><h1 class="ql-align-center">Enterprise Breeding System</h1>', '2024-04-23 20:14:05.130', NULL, 1, NULL, false, 10, NULL);
INSERT INTO core.email_template
("name", subject, "template", creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, person_id)
VALUES('Shipment Submitted Template', 'Shipment Outgoing Submitted', '<h2 class="ql-align-center">Hi,</h2><h2 class="ql-align-center"><strong>[date]</strong> Outgoing seed Shipment <strong>[requestCode]</strong> has been Submitted by <strong>[currentUser].</strong></h2><p class="ql-align-center"><br></p><h1 class="ql-align-center"><strong>Enterprise Breeding System.</strong></h1>', '2024-04-23 20:10:49.629', '2024-04-23 20:14:22.811', 1, 1, false, 11, NULL);


SET session_replication_role = 'origin';