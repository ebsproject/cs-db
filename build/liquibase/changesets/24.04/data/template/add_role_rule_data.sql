--liquibase formatted sql

--changeset postgres:add_role_rule_data context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-318  BDS-670 Modify Workflow CF function to add security rules filter

do
$$
declare
	admin_role_id integer;
	data_manager_role_id integer;
	lab_manager_role_id integer;
	created_by_any_user_rule_id integer;
	created_by_me_rule_id integer;
	submitted_to_me_rule_id integer;
	created_by_unit_member_rule_id integer;
	submitted_to_unit_member_rule_id integer;
	rec record;
begin
	select id from security.role where name = 'Admin' into admin_role_id;
	select id from security.role where name = 'Data Manager' into data_manager_role_id;
	select id from security.role where name = 'Lab Manager' into lab_manager_role_id;
	select id from security.rule where name = '@CreateByAnyUser' into created_by_any_user_rule_id;
	select id from security.rule where name = '@CreatedByMe' into created_by_me_rule_id;
	select id from security.rule where name = '@CreatedByUnitMember' into created_by_unit_member_rule_id;
	select id from security.rule where name = '@SubmittedToMe' into submitted_to_me_rule_id;
	select id from security.rule where name = '@SubmittedToUnitMember' into submitted_to_unit_member_rule_id;
	for rec in (select id from security.role) loop 
		insert into security.rule_role (rule_id, role_id, creator_id) 
			select created_by_me_rule_id, rec.id, 1
			where not exists 
				(select role_id from security.rule_role where role_id = rec.id and rule_id = created_by_me_rule_id);
		insert into security.rule_role (rule_id, role_id, creator_id) 
			select submitted_to_me_rule_id, rec.id, 1
			where not exists 
				(select role_id from security.rule_role where role_id = rec.id and rule_id = submitted_to_me_rule_id);
	end loop;
	insert into security.rule_role (rule_id, role_id, creator_id) 
		select created_by_any_user_rule_id, admin_role_id, 1
		where not exists 
			(select role_id from security.rule_role where role_id = admin_role_id and rule_id = created_by_any_user_rule_id);
	insert into security.rule_role (rule_id, role_id, creator_id) 
		select created_by_unit_member_rule_id, admin_role_id, 1
		where not exists 
			(select role_id from security.rule_role where role_id = admin_role_id and rule_id = created_by_unit_member_rule_id);
	insert into security.rule_role (rule_id, role_id, creator_id) 
		select submitted_to_unit_member_rule_id, admin_role_id, 1
		where not exists 
			(select role_id from security.rule_role where role_id = admin_role_id and rule_id = submitted_to_unit_member_rule_id);
	insert into security.rule_role (rule_id, role_id, creator_id) 
		select created_by_any_user_rule_id, data_manager_role_id, 1
		where not exists 
			(select role_id from security.rule_role where role_id = data_manager_role_id and rule_id = created_by_any_user_rule_id);
	insert into security.rule_role (rule_id, role_id, creator_id) 
		select created_by_unit_member_rule_id, data_manager_role_id, 1
		where not exists 
			(select role_id from security.rule_role where role_id = data_manager_role_id and rule_id = created_by_unit_member_rule_id);
	insert into security.rule_role (rule_id, role_id, creator_id) 
		select submitted_to_unit_member_rule_id, data_manager_role_id, 1
		where not exists 
			(select role_id from security.rule_role where role_id = data_manager_role_id and rule_id = submitted_to_unit_member_rule_id);
	insert into security.rule_role (rule_id, role_id, creator_id) 
		select created_by_unit_member_rule_id, lab_manager_role_id, 1
		where not exists 
			(select role_id from security.rule_role where role_id = lab_manager_role_id and rule_id = created_by_unit_member_rule_id);
	insert into security.rule_role (rule_id, role_id, creator_id) 
		select submitted_to_unit_member_rule_id, lab_manager_role_id, 1
		where not exists 
			(select role_id from security.rule_role where role_id = lab_manager_role_id and rule_id = submitted_to_unit_member_rule_id);
end
$$;