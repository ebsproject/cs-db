--liquibase formatted sql

--changeset postgres:add_rule_data context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-318  Create Security Rules for Workflow Engine

SET session_replication_role = 'replica';

INSERT INTO "security"."rule" (id,"name",description,used_workflow,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void)
VALUES (1,'@CreatedByMe','Show any record created by the user',true,'2024-04-15 16:23:22.889027',NULL,1,1,false) ON CONFLICT DO NOTHING;

INSERT INTO "security"."rule" (id,"name",description,used_workflow,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void)
VALUES (2,'@CreatedByUnitMember','Show any record created by any member of the unit',true,'2024-04-15 16:24:18.241697',NULL,1,NULL,false) ON CONFLICT DO NOTHING;

INSERT INTO "security"."rule" (id,"name",description,used_workflow,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void)
VALUES (3,'@CreateByAnyUser','Show any record created by any user',true,'2024-04-15 16:25:15.55521',NULL,1,1,false) ON CONFLICT DO NOTHING;

INSERT INTO "security"."rule" (id,"name",description,used_workflow,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void)
VALUES (4,'@SubmittedToMe','Show any record submitted to me',true,'2024-04-15 16:26:21.711886',NULL,1,1,false) ON CONFLICT DO NOTHING;

INSERT INTO "security"."rule" (id,"name",description,used_workflow,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void)
VALUES (5,'@SubmittedToUnitMember','Show any record submitted to any Unit Member',true,'2024-04-15 16:27:44.446756',NULL,1,NULL,false) ON CONFLICT DO NOTHING;

SET session_replication_role = 'origin';