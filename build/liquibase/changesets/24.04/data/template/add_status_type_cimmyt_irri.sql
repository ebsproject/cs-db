
--liquibase formatted sql

--changeset postgres:add_status_type_cimmyt_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-356  As a user with permissions, (APPROVER, ACTOR) , I should be able to view Rejected status on a shipment that has been  processed to REJECT

SET session_replication_role = 'replica';


INSERT INTO workflow.status_type
("name", description, help, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, workflow_id)
VALUES('Rejected', 'Request Rejected', 'Request Rejected', 1, '2024-04-22 20:55:17.346', NULL, 1, NULL, false, 400, 562);
INSERT INTO workflow.status_type
("name", description, help, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, workflow_id)
VALUES('Rejected', 'Request Rejected', 'Request Rejected', 1, '2024-04-22 20:55:45.163', NULL, 1, NULL, false, 401, 496);


SET session_replication_role = 'origin';