
--liquibase formatted sql

--changeset postgres:disable_product_shipment_manager context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-469 Disable the old shipment manager from the database

do $$
declare sh_manager_product_id int;

begin
	SELECT id from core.product where name = 'Shipment Manager' into sh_manager_product_id;

    UPDATE core.product
	SET is_void=true
	WHERE id=sh_manager_product_id;

end$$;