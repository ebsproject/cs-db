--liquibase formatted sql

--changeset postgres:update_approve_node_cimmyt_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-468 CS-Shipment Manager : IRRI and CIMMYT: I was able to process a shipment without Updating the status of the items to released.


UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Processing Completed"
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "validateItems",
      "onSuccess": ""
    }
  },
  "status": "303",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=20801;
UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Processing Completed"
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "validateItems",
      "onSuccess": ""
    }
  },
  "status": "367",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21099;
