
--liquibase formatted sql

--changeset postgres:update_body_email_template context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-314 CS-Shipment Manager-IRRI & CIMMYT: Update to this Email format for  emails sent out on Shipment Manager Transactions


UPDATE core.email_template
SET "name"='Shipment Status Template', subject='Shipment Outgoing Seed status changed', "template"='<h3 class="ql-align-center">Hi,</h3><h3 class="ql-align-center"><strong>[date]</strong> The status of the Outgoing Seed Shipment [requestCode] has been changed to <strong>[status]</strong> by <strong>[currentUser]</strong></h3><h3 class="ql-align-center">Enterprise Breeding System</h3>', creation_timestamp='2024-04-23 20:14:05.130', modification_timestamp='2024-04-24 14:11:45.313', creator_id=1, modifier_id=1, is_void=false, person_id=NULL
WHERE id=10;
UPDATE core.email_template
SET "name"='Shipment Submitted Template', subject='Shipment Outgoing Submitted', "template"='<h4 class="ql-align-center">Hi,</h4><h4 class="ql-align-center"><strong>[date]</strong> The Outgoing Seed Shipment <strong>[requestCode]</strong> has been Submitted by <strong>[currentUser].</strong></h4><h4 class="ql-align-center"><br></h4><h4 class="ql-align-center"><strong>Enterprise Breeding System.</strong></h4>', creation_timestamp='2024-04-23 20:10:49.629', modification_timestamp='2024-04-24 14:16:17.795', creator_id=1, modifier_id=1, is_void=false, person_id=NULL
WHERE id=11;