--liquibase formatted sql

--changeset postgres:update_cf_node_definition_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-311 CS-Shipment Manager: Review and Include any missing data priority fields that are needed to support fixture usage and presence

UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 1,
  "name": "shipmentPurposeTx",
  "sort": 4.1,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    6,
    4,
    4
  ],
  "helper": {
    "title": "Other Purpose",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Other Purpose",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false,
    "placeholder": "Please specify if other"
  },
  "showInGrid": false,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.fiel"
      }
    ],
    "applyRules": false,
    "showIfValue": "Other",
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "shipmentPurposeDd",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
  WHERE id=765;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 1,
  "name": "shipmentPurpose",
  "sort": 4.1,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "helper": {
    "title": "Shipment Purpose",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Other Purpose",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false,
    "placeholder": "Please enter the purpose if other"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.value"
      }
    ],
    "applyRules": false,
    "showIfValue": "Other",
    "columnFilter": "id",
    "parentControl": "shipmentPurposeV2",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
  WHERE id=530;
-- Auto-generated SQL script #202404261359
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 1,
  "name": "trackingRemarks",
  "sort": 123,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    6,
    6,
    6
  ],
  "helper": {
    "title": "",
    "placement": "top"
  },
  "inputProps": {
    "rows": 3,
    "label": "Tracking Remarks",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": true
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
  WHERE id=906;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 1,
  "name": "dispatchRemarks",
  "sort": 122,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    12,
    12,
    12
  ],
  "helper": {
    "title": "Dispatch Remarks",
    "placement": "top"
  },
  "inputProps": {
    "rows": 3,
    "label": "Dispatch Remarks",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": true
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
  WHERE id=907;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 1,
  "name": "acknowledgementRemarks",
  "sort": 124,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    6,
    6,
    6
  ],
  "helper": {
    "title": "Acknowledgement Remarks",
    "placement": "top"
  },
  "inputProps": {
    "rows": 3,
    "label": "Acknowledgement Remarks",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": true
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
  WHERE id=913;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 1,
  "name": "airBillNumber",
  "sort": 121,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    6,
    6,
    6
  ],
  "helper": {
    "title": "Air Bill Number",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Air Bill Number",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "showIfValue": "By Air",
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "typeOfCuries",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
  WHERE id=908;