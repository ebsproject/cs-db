--liquibase formatted sql

--changeset postgres:update_node_print_preview_and_stages context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-290  CS-Shipment Manager: IRRI: As a user with permissions, I would like to generate a dispatch note for a submitted and approved shipment.


DELETE FROM workflow.node_stage
  WHERE stage_id=10731 AND node_id=21519;
DELETE FROM workflow.node_stage
  WHERE stage_id=10732 AND node_id=21521;
DELETE FROM workflow.node_stage
  WHERE stage_id=10736 AND node_id=21522;


INSERT INTO workflow.node_stage (stage_id,node_id)
  VALUES (10734,21519);
INSERT INTO workflow.node_stage (stage_id,node_id)
  VALUES (10737,21521);


UPDATE workflow.node
  SET is_void=true
WHERE id=21522;

UPDATE workflow.node
  SET help='Print Dispatch Note', define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "template": "sys_dispatch_note_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
  WHERE id=21523;


UPDATE workflow.node
  SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "template": "sys_dispatch_note_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
  WHERE id=21524;

  UPDATE workflow.node
  SET "sequence"= 4,help='Print Dispatch Note', define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "template": "sys_dispatch_note_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb 
  WHERE id=21519;

UPDATE workflow.node
  SET description='Print Package List', help='Print Package List',
   define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "template": "sys_package_list_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
  WHERE id=21508;

UPDATE workflow.node
  SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "template": "sys_dispatch_note_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb, description='Dispatch Note', "name"='Print Preview Done Note', help='Print Dispatch Note'
  WHERE id=21521;
