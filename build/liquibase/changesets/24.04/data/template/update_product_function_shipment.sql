--liquibase formatted sql

--changeset postgres:update_product_function_shipment context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-469 Disable the old shipment manager from the database

do $$
declare sh_manager_product_id int;

begin
	SELECT id from core.product where name = 'Shipment Manager' into sh_manager_product_id;

    UPDATE "security".product_function
	SET is_void=true
	WHERE product_id=sh_manager_product_id;

end$$;