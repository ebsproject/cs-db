
--liquibase formatted sql

--changeset postgres:update_rejected_nodes_cimmyt_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-356  As a user with permissions, (APPROVER, ACTOR) , I should be able to view Rejected status on a shipment that has been  processed to REJECT


UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request was rejected. All shipment items has been changed to REJECTED"
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "ERROR",
      "functions": "markAllItemsAsRejected",
      "onSuccess": "OK"
    }
  },
  "status": "400",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21104;
UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request was rejected. All shipment items has been changed to REJECTED"
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "ERROR",
      "functions": "markAllItemsAsRejected",
      "onSuccess": "OK"
    }
  },
  "status": "401",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=20794;
