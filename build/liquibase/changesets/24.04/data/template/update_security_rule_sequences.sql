--liquibase formatted sql

--changeset postgres:update_security_rule_sequences context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-318  Create Security Rules for Workflow Engine

SELECT setval('security.rule_id_seq', (SELECT MAX(id) FROM "security"."rule"));