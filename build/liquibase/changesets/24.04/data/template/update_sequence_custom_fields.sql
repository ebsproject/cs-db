
--liquibase formatted sql

--changeset postgres:update_sequence_custom_fields_cimmyt context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-319  Shipment Manager- As a user at CIMMYT in Mexico I should be able to input additional information in the Basic Info tab for outgoing shipments

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));