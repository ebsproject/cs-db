
--liquibase formatted sql

--changeset postgres:update_sequences_email_template context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-314 CS-Shipment Manager-IRRI & CIMMYT: Update to this Email format for  emails sent out on Shipment Manager Transactions

SELECT setval('core.email_template_id_seq', (SELECT MAX(id) FROM "core"."email_template"));