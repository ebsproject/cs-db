
--liquibase formatted sql

--changeset postgres:update_sequences_status_type context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-356 As a user with permissions, (APPROVER, ACTOR) , I should be able to view Rejected status on a shipment that has been  processed to REJECTED

SELECT setval('workflow.status_type_id_seq', (SELECT MAX(id) FROM "workflow"."status_type"));