--liquibase formatted sql

--changeset postgres:update_submit_node_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-421 Shipment Manager IRRI: Enable shipment submission for GSL transactions regardless of MTA status

UPDATE workflow.node
SET define='{"id": 5, "after": {"executeNode": "", "sendNotification": {"send": true, "message": "The request has been changed to Submitted"}}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "ERROR", "functions": "validateMTA", "onSuccess": "SUCCESS"}}, "status": "365", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb
WHERE id=21070;