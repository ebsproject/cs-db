--liquibase formatted sql

--changeset postgres:update_system_roles context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-318  BDS-670 Modify Workflow CF function to add security rules filter

update security.role set is_system = true where name = 'Guest';
update security.role set is_system = true where name = 'User';
update security.role set is_system = true where name = 'CS Admin';
update security.role set is_system = true where name = 'BA Admin';
update security.role set is_system = true where name = 'SM Admin';
update security.role set is_system = true where name = 'CB Admin';
update security.role set is_system = true where name = 'CS User';
update security.role set is_system = true where name = 'BA User';
update security.role set is_system = true where name = 'SM User';
update security.role set is_system = true where name = 'CB User';
update security.role set is_system = true where name = 'Lab Manager';
update security.role set is_system = true where name = 'Data Manager';