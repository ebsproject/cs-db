--liquibase formatted sql

--changeset postgres:create_get_partners_user_ids_function_0 context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-670 Modify Workflow CF function to add security rules filter

-- DROP FUNCTION crm.getpartnersuserids(int4);

CREATE OR REPLACE FUNCTION crm.getpartnersuserids(_contact_id integer)
 RETURNS integer[]
 LANGUAGE plpgsql
AS $function$    
declare  
	unit_rec record;
	member_rec record;
	user_rec record;
	member_user_id integer;
	member_ids integer[];
begin
	for unit_rec in (select parent_id from crm.contact_hierarchy where contact_id = _contact_id and is_void = false) loop 
		for member_rec in (select contact_id from crm.contact_hierarchy where parent_id = unit_rec.parent_id and is_void = false) loop 
			for user_rec in (select id from security.user where contact_id = member_rec.contact_id) loop 
				member_ids := array_remove(member_ids, user_rec.id);
				member_ids := array_append(member_ids, user_rec.id);
			end loop;
		end loop;
	end loop;
	return member_ids;
END;
$function$
;