--liquibase formatted sql

--changeset postgres:create_rule_role_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-318  Create Security Rules for Workflow Engine


CREATE TABLE "security".rule_role (
	id integer NOT NULL   DEFAULT NEXTVAL(('security."rule_role_id_seq"'::text)::regclass),
	role_id int4 NOT NULL,
	rule_id int4 NOT NULL,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	modification_timestamp timestamp NULL,
	creator_id int4 NOT NULL,
	modifier_id int4 NULL,
	is_void bool NOT NULL DEFAULT false,
	CONSTRAINT "PK_securityrulerole" PRIMARY KEY (id)
);


CREATE SEQUENCE security.rule_role_id_seq INCREMENT 1 START 1;

ALTER TABLE "security".rule_role ADD CONSTRAINT fk_rule_role_role FOREIGN KEY (role_id) REFERENCES security.role(id);
ALTER TABLE "security".rule_role ADD CONSTRAINT fk_rule_role_rule FOREIGN KEY (rule_id) REFERENCES security.rule(id);

