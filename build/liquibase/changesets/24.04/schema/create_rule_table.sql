--liquibase formatted sql

--changeset postgres:create_rule_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-318  Create Security Rules for Workflow Engine

CREATE TABLE "security"."rule" (
	id integer NOT NULL  DEFAULT NEXTVAL(('security."rule_id_seq"'::text)::regclass),
	"name" varchar(150) NOT NULL,
	description varchar(500) NOT NULL,
	used_workflow bool NOT NULL DEFAULT false,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	modification_timestamp timestamp NULL,
	creator_id int4 NOT NULL,
	modifier_id int4 NULL,
	is_void bool NOT NULL DEFAULT false,
	CONSTRAINT "PK_securityrule" PRIMARY KEY (id)
);


CREATE SEQUENCE security.rule_id_seq INCREMENT 1 START 1;

