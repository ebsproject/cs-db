--liquibase formatted sql

--changeset postgres:drop_table_user_role context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-635 Delete the database table that relates users to roles

DROP TABLE IF EXISTS "security".user_role CASCADE ;