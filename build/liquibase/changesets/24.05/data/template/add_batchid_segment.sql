--liquibase formatted sql

--changeset postgres:add_batchid_segment context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1233 add batch.id in a rule to create globally unique names for genotyping batches

SELECT setval('core.segment_api_id_seq', (SELECT MAX(id) FROM "core"."segment_api"));
SELECT setval('core.rule_segment_id_seq', (SELECT MAX(id) FROM "core"."rule_segment"));

INSERT INTO core.segment_api ("type","method",server_url,path_template,response_mapping,body_template,tenant_id,creation_timestamp,creator_id,is_void)
VALUES ('GRAPHQL','POST','SM','graphql','data.findBatch.id','query{findBatch(id:{}){id}}',1,'now()',1,false);

INSERT INTO core.rule_segment (id,"name",requires_input,data_type,segment_api_id,tenant_id,creation_timestamp,creator_id,is_void)
VALUES (39,'batch-id',true,'TEXT',(select id from core.segment_api where response_mapping = 'data.findBatch.id'),1,'now()',1,false);
