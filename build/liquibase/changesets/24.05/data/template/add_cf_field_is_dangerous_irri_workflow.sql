--liquibase formatted sql

--changeset postgres:add_cf_field_is_dangerous_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-792 Shipment Outgoing CIMMYT: remove check box Is dangerous from the basic info and the Items list schedule

UPDATE workflow.node_cf
  SET  node_id=20992, is_void=false,
 field_attributes='{
  "id": 2,
  "sort": 24,
  "sizes": [
    12,
    12,
    6,
    6,
    6
  ],
  "helper": {
    "title": "Check if there are dangerous goods",
    "placement": "top"
  },
  "checked": false,
  "inputProps": {
    "label": "Mark if the product is dangerous and should be handled with extra care"
  }
}'::jsonb
  WHERE id=871;

