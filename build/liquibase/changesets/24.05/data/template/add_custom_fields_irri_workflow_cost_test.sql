
--liquibase formatted sql

--changeset postgres:add_custom_fields_irri_workflow_cost_test context:template splitStatements:false rollbackSplitStatements:false
--comment:BDS-1202 SM-Printouts: Create IRRI SHU service Agreement form template for Shipment outgoing IRRI

SET session_replication_role = 'replica';

INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('specificTest', 'Specific Test', 'Specific Test', false, 1, '2024-05-30 14:34:39.862', '2024-05-30 14:35:57.836', 1, 1, false, 1290, 1, 1, 21107, '{"id": 1, "name": "specificTest", "sort": 105.1, "rules": {"required": ""}, "sizes": [12, 12, 6, 6, 6], "helper": {"title": "Specific test to be done", "placement": "top"}, "inputProps": {"rows": 1, "label": "Specific Test", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}}, "defaultValue": ""}'::jsonb, '', NULL);
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('cost', 'cost', 'cost', false, 1, '2024-05-30 14:38:09.698', NULL, 1, NULL, false, 1291, 4, 1, 21107, '{"id": 4, "row": false, "sort": 105.2, "rules": {"required": ""}, "sizes": [12, 12, 6, 6, 6], "helper": {"title": "Cost", "placement": "top"}, "options": [{"label": "With cost", "value": "with-cost"}, {"label": "Without cost", "value": "no-cost"}], "inputProps": {"label": "Cost"}, "showInGrid": true, "defaultValue": "no-cost"}'::jsonb, '', NULL);


SET session_replication_role = 'origin';

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));