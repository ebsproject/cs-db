
--liquibase formatted sql

--changeset postgres:add_extra_custom_fields_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-290 CS-Shipment Manager: IRRI: As a user with permissions, I would like to generate a dispatch note for a submitted and approved shipment.

SET session_replication_role = 'replica';

INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('tariffCode', 'Tariff Code', 'Tariff Code', false, 1, '2024-05-27 15:53:39.009', NULL, 1, NULL, false, 1236, 1, 1, 21107, '{"id": 1, "name": "tariffCode", "sort": 111.1, "rules": {"required": ""}, "sizes": [12, 12, 6, 4, 4], "helper": {"title": "Tariff Code", "placement": "top"}, "inputProps": {"rows": 1, "label": "Tariff Code", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}}, "defaultValue": ""}'::jsonb, '', NULL);

INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('unitValue', 'Unit Value', 'Unit Value', false, 1, '2024-05-27 15:53:39.009', NULL, 1, NULL, false, 1237, 1, 1, 21107, '{"id": 1, "name": "unitValue", "sort": 111.2, "rules": {"required": ""}, "sizes": [12, 12, 6, 4, 4], "helper": {"title": "Unit Value", "placement": "top"}, "inputProps": {"rows": 1, "label": "Unit Value", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}}, "defaultValue": ""}'::jsonb, '', NULL);


INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('hsCode', 'HS Code', 'HS Code', false, 1, '2024-05-27 15:53:39.009', NULL, 1, NULL, false, 1238, 1, 1, 21107, '{"id": 1, "name": "hsCode", "sort": 111.3, "rules": {"required": ""}, "sizes": [12, 12, 6, 4, 4], "helper": {"title": "HS Code", "placement": "top"}, "inputProps": {"rows": 1, "label": "HS Code", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}}, "defaultValue": ""}'::jsonb, '', NULL);


INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('species', 'Species', 'Species', false, 1, '2024-05-27 15:53:39.009', NULL, 1, NULL, false, 1239, 1, 1, 21107, '{"id": 1, "name": "species", "sort": 111.4, "rules": {"required": ""}, "sizes": [12, 12, 6, 4, 4], "helper": {"title": "Species", "placement": "top"}, "inputProps": {"rows": 1, "label": "Species", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}}, "defaultValue": ""}'::jsonb, '', NULL);



SET session_replication_role = 'origin';

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));

-- Auto-generated SQL script #202405271001
UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "valueOfProduct",
  "sort": 111,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    6,
    4,
    4
  ],
  "helper": {
    "title": "Total value of the Product",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Total value of the Product",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false,
    "startAdornment": "$"
  },
  "modalPopup": {
    "show": false,
    "componentUI": ""
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=1125;
