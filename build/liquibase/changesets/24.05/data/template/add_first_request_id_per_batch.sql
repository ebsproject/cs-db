--liquibase formatted sql

--changeset postgres:add_first_request_id_per_batch context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1233 create first request id rule to make batch codes unique

SELECT setval('core.segment_api_id_seq', (SELECT MAX(id) FROM "core"."segment_api"));
SELECT setval('core.rule_segment_id_seq', (SELECT MAX(id) FROM "core"."rule_segment"));

INSERT INTO core.segment_api ("type","method",server_url,path_template,response_mapping,body_template,tenant_id,creation_timestamp,creator_id,is_void)
VALUES ('REST','GET','SM','batch/list-request-byBatchID?batchId={}','result.data[0].id',null,1,'now()',1,false);

INSERT INTO core.rule_segment (id,"name",requires_input,data_type,segment_api_id,tenant_id,creation_timestamp,creator_id,is_void)
VALUES (40,'first_request_id_per_batch',true,'TEXT',(select id from core.segment_api where response_mapping = 'result.data[0].id'),1,'now()',1,false);
