--liquibase formatted sql

--changeset postgres:add_service_processor_action context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1143 Change recipient condition on submitted to me/unit member security rule

do
$$
declare
	_sh_pid int;
	_admin_id int;
	_labm_id int;
	_pf_id int;
begin
	select id from core.product where name = 'Shipment Manager' into _sh_pid;
	if (_sh_pid is null) then
		return;
	else
		select id from security.role where name = 'Admin' into _admin_id;
		select id from security.role where name = 'Lab Manager' into _labm_id;
		with _pf as (insert into security.product_function
	    	(description, system_type, action, creator_id, product_id, is_data_action)
		values
		    ('Service Processor', true, 'Service Processor', 1, _sh_pid, false)
		returning id)
		select id from _pf into _pf_id;
		if (_admin_id is not null) then
			insert into security.role_product_function (role_id, product_function_id)
				values (_admin_id, _pf_id);
		end if;
		if (_labm_id is not null) then
			insert into security.role_product_function (role_id, product_function_id)
				values (_labm_id, _pf_id);
		end if;
	end if;
end
$$;