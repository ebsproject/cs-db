--liquibase formatted sql

--changeset postgres:add_show_menu_pf_roles context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1370 Add SHOW_MENU action to determine if the product should be visible in the UI

do
$$
declare
	_role_id int;
	rec record;
begin 
	for rec in (select r.id as role_id, p.id as product_id, (select id from security.product_function pf2 where pf2.product_id = p.id and action = 'SHOW_MENU') as pf_id
		from security.role r
		join security.role_product_function rpf on r.id = rpf.role_id 
		join security.product_function pf on pf.id = rpf.product_function_id 
		join core.product p on pf.product_id = p.id 
		where p.is_void = false and pf.is_void = false and r.is_void = false and pf.action <> 'Read'
		group by r.id, p.id
		order by r.id, p.id) 
	loop 
		insert into security.role_product_function (role_id, product_function_id)
			select rec.role_id, rec.pf_id
		where not exists (select role_id from security.role_product_function where role_id = rec.role_id and product_function_id = rec.pf_id);
	end loop;	
end
$$;