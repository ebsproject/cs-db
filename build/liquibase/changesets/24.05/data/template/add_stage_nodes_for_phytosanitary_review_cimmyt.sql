
--liquibase formatted sql

--changeset postgres:add_stage_nodes_for_phytosanitary_review_cimmyt context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1156 Shipment Manager CIMMYT & IRRI:  Improve at submit to Trigger email send to notify SHU for option "phytosanitary  review for a submitted shipment



SET session_replication_role = 'replica';

INSERT INTO workflow.stage
("name", description, help, "sequence", parent_id, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, phase_id, depend_on, icon, wf_view_type_id, design_ref)
VALUES('Send to Phytosanitary Review CIMMYT', 'Send to Phytosanitary Review CIMMYT', 'Send to Phytosanitary Review CIMMYT', 4, NULL, 1, '2024-06-05 22:13:11.072', '2024-06-05 22:21:42.255', 1, 1, false, 10769, 1, 496, '{"edges": [], "width": 800, "height": 180, "position": {"x": 243.3705081650685, "y": 620}}'::jsonb, NULL, 1, 'd168c916-a68f-4291-aeae-bdbccf8de99d'::uuid);


INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send to Phytosanitary Review', 'Send to Phytosanitary Review CIMMYT', 'Send to Phytosanitary Review CIMMYT', 1, false, 1, '2024-06-05 22:13:39.263', '2024-06-05 22:21:42.259', 1, 1, false, 21786, 1, NULL, 496, 6, '{"id": 6, "node": "20797", "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "action": "", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "disabled": false, "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": [], "width": 100, "height": 50, "position": {"x": 10, "y": 50}}'::jsonb, 'Mail', '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 2, '0950c44e-4ab9-4a63-8247-f59a57da72f4'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Email Phytosanitary review CIMMYT', 'Email Phytosanitary review CIMMYT', 'Email Phytosanitary review CIMMYT', 2, false, 1, '2024-06-05 22:14:28.353', '2024-06-05 22:21:42.327', 1, 1, false, 21787, 1, NULL, 496, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "42", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contact": ""}], "disabled": false, "sendToSHU": true, "inputProps": {"sourceNodes": []}, "attachments": {"attach": true, "templates": [{"format": "pdf", "template": "sys_data_items_cimmyt"}]}, "outputProps": {"targetNodes": []}, "sendToSender": true, "sendToRecipient": true, "sendToRequestor": true}'::jsonb, '{"edges": ["21786"], "width": 100, "height": 50, "position": {"x": 264.73959971536647, "y": 104.26405674410171}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, '92a22089-9e9c-489c-aaa5-53e12686cb45'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Notification Phytosanitary Review CIMMYT', 'Notification Phytosanitary Review CIMMYT', 'Notification Phytosanitary Review CIMMYT', 2, false, 1, '2024-06-05 22:14:30.055', '2024-06-05 22:21:42.327', 1, 1, false, 21788, 1, NULL, 496, 7, '{"id": 7, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "title": "Phytosanitary Review", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "message": "An Email notification was sent for Phytosanitary Review for [requestCode]. Report was attached.", "disabled": false, "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21786"], "width": 100, "height": 50, "position": {"x": 261.7249298962495, "y": 27.389976356624288}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, '37f7d966-6ec3-4cf7-9a36-a5bc61532c78'::uuid);

INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10769, 21786);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10769, 21787);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10769, 21788);

SET session_replication_role = 'origin';

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM "workflow"."node"));
SELECT setval('workflow.stage_id_seq', (SELECT MAX(id) FROM "workflow"."stage"));


UPDATE workflow.node
SET "name"='Move to Phytosanitary Review', description='Move to Phytosanitary Review', help='Move to Phytosanitary Review', "sequence"=3, define='{"id": 6, "node": "21786", "after": {"executeNode": "", "sendNotification": {"send": true, "message": "The request has been submitted successfully"}}, "action": "", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "disabled": false, "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, depend_on='{"edges": ["20798"], "width": 100, "height": 50, "position": {"x": 159.05232524963094, "y": 6.325674988805815}}'::jsonb, icon=NULL, message='{}'::jsonb, require_input='{}'::jsonb, security_definition='{"roles": [{"id": "2", "name": "Admin", "isSystem": true, "__typename": "Role"}], "users": null, "programs": null}'::jsonb, validation_code=NULL, validation_type='javascript', wf_view_type_id=1, node_type_id=4, design_ref='51d4ae90-fbcd-4c6a-9e1c-850021268f7d'::uuid
WHERE id=20799;