--liquibase formatted sql

--changeset postgres:add_stage_nodes_for_phytosanitary_review_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1156 Shipment Manager CIMMYT & IRRI:  Improve at submit to Trigger email send to notify SHU for option "phytosanitary  review for a submitted shipment

UPDATE workflow.node
SET "name"='MOVE TO PHYTOSANITARY REVIEW', description='MOVE TO PHYTOSANITARY REVIEW', help='MOVE TO PHYTOSANITARY REVIEW', "sequence"=3, require_approval=false, tenant_id=1, creation_timestamp='2023-11-23 19:21:33.989', modification_timestamp='2024-06-05 20:53:33.603', creator_id=122, modifier_id=1, is_void=false, htmltag_id=1, product_id=NULL, workflow_id=562, process_id=6, define='{
  "id": 6,
  "node": "21783",
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": "No"
    }
  },
  "action": "",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb, depend_on='{"edges": ["21070"], "width": 100, "height": 50, "position": {"x": 124.2679251235968, "y": 0}}'::jsonb, icon=NULL, message='{}'::jsonb, require_input='{}'::jsonb, security_definition='{"roles": [{"id": "2", "name": "Admin", "isSystem": true, "__typename": "Role"}], "users": null, "programs": null}'::jsonb, validation_code=NULL, validation_type='javascript', wf_view_type_id=1, node_type_id=4, design_ref='7d182f13-ed59-4891-b561-1d54168075b5'::uuid
WHERE id=21071;


SET session_replication_role = 'replica';

INSERT INTO workflow.stage
("name", description, help, "sequence", parent_id, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, phase_id, depend_on, icon, wf_view_type_id, design_ref)
VALUES('Phytosanitary Review IRRI', 'Phytosanitary Review IRRI', 'Phytosanitary Review IRRI', 4, NULL, 1, '2024-06-05 20:00:34.474', '2024-06-05 20:17:38.440', 1, 1, false, 10768, 1, 562, '{"edges": [], "width": 800, "height": 180, "position": {"x": 42.12825099196152, "y": 580.4177011387388}}'::jsonb, NULL, 1, 'dd2e6d1f-4377-44e1-885b-f88301f6d5f1'::uuid);


INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send to Phytosanitary Review', 'Send to Phytosanitary Review', 'Send to Phytosanitary Review', 1, false, 1, '2024-06-05 20:41:05.720', '2024-06-05 20:53:33.586', 1, 1, false, 21783, 1, NULL, 562, 6, '{"id": 6, "node": "21027", "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "action": "", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "disabled": false, "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": [], "width": 100, "height": 50, "position": {"x": 20.206065651716017, "y": 48.54199062118346}}'::jsonb, 'Mail', '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 2, 'f3ed8395-81c8-44de-835d-7169e39a749b'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('PHYTOSANITARY REVIEW NOTIFICATION', 'PHYTOSANITARY REVIEW NOTIFICATION', 'PHYTOSANITARY REVIEW NOTIFICATION', 1, false, 1, '2024-06-05 20:42:58.922', '2024-06-05 20:53:33.587', 1, 1, false, 21784, 1, NULL, 562, 7, '{"id": 7, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "title": "Phytosanitary Review", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "message": "An Email notification was sent for Phytosanitary Review for [requestCode]. Report was attached.", "disabled": false, "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21783"], "width": 100, "height": 50, "position": {"x": 215.57932241313756, "y": 7.717728014319164}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, '17c059f7-09fc-4ee4-abab-6fd92ce9235c'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('EMAIL PHYTOSANITARY IRRI', 'EMAIL PHYTOSANITARY IRRI', 'EMAIL PHYTOSANITARY IRRI', 2, false, 1, '2024-06-05 20:47:48.862', '2024-06-05 20:53:33.590', 1, 1, false, 21785, 1, NULL, 562, 4, '{"id": 4, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "email": "42", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "contacts": [{"contact": ""}], "disabled": false, "sendToSHU": false, "inputProps": {"sourceNodes": []}, "attachments": {"attach": true, "templates": [{"format": "pdf", "template": "sys_data_items_irri"}]}, "outputProps": {"targetNodes": []}, "sendToSender": true, "sendToRecipient": true, "sendToRequestor": true}'::jsonb, '{"edges": ["21783"], "width": 100, "height": 50, "position": {"x": 220.76081322089476, "y": 98.04106771946863}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, '0c2201f1-ec65-48de-8d12-744524094027'::uuid);


INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10768, 21783);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10768, 21784);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10768, 21785);


INSERT INTO core.email_template
("name", subject, "template", creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, person_id)
VALUES('Shipment Phytosanitary Review', 'Phytosanitary Review', '<p>Hi,</p><p>[date] The Shipment Outgoing Seed [requestCode] requires a Phytosanitary Review. Please review the attached document.</p><p>Best Regards.</p>', '2024-06-05 20:53:05.288', NULL, 1, NULL, false, 42, NULL);


SET session_replication_role = 'origin';

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM "workflow"."node"));
SELECT setval('workflow.stage_id_seq', (SELECT MAX(id) FROM "workflow"."stage"));
SELECT setval('core.email_template_id_seq', (SELECT MAX(id) FROM "core"."email_template"));