
--liquibase formatted sql

--changeset postgres:add_workflow_genotyping_service context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1238 Populate Service Providers in User Profile to show available programs/units the user request services from

SET session_replication_role = 'replica';

INSERT INTO workflow.workflow
(title, "name", description, help, sort_no, icon, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, node_id, api, navigation_icon, navigation_name, product_id, "sequence", show_menu, is_system, security_definition)
VALUES('Genotyping Service', 'Genotyping Service', 'Genotyping Service', 'Genotyping Service', 5, 'na', '2024-05-09 15:55:08.570', NULL, 1, NULL, false, 583, 1, NULL, 'https://smapi-dev.ebsproject.org/', 'na', 'Genotyping Service', 38, 5, false, true, '{"roles": [{"id": "13", "name": "Breeder", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}], "isSystem": false, "__typename": "Role", "description": "Breeder"}, {"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}, {"id": "15", "name": "Lab Manager", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "Lab Manager"}], "users": [{"id": "1", "contact": {"id": 0, "person": {"givenName": "Account", "__typename": "Person", "familyName": "System"}, "__typename": "Contact"}, "userName": "admin@ebsproject.org", "__typename": "User"}], "programs": [{"id": "92", "code": "IRSEA", "name": "Irrigated South-East Asia", "__typename": "Program"}, {"id": "93", "code": "BW", "name": "BW Wheat Breeding Program", "__typename": "Program"}]}'::jsonb);


SET session_replication_role = 'origin';

SELECT setval('workflow.workflow_id_seq', (SELECT MAX(id) FROM "workflow"."workflow"));