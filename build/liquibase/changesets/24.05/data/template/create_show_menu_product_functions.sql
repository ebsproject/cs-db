--liquibase formatted sql

--changeset postgres:create_show_menu_product_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1370 Add SHOW_MENU action to determine if the product should be visible in the UI

do
$$
declare
	rec record;
	_admin_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_id;
	for rec in (select id from core.product where is_void = false) loop 
		with _pf as (insert into security.product_function
	    	(description, system_type, action, creator_id, product_id, is_data_action)
		values
		    ('Show product in UI', true, 'SHOW_MENU', 1, rec.id, false)
		returning id)
		select id from _pf into _pf_id;
		if (_admin_id is not null) then
			insert into security.role_product_function (role_id, product_function_id)
				values (_admin_id, _pf_id);
		end if;
	end loop;
end
$$;