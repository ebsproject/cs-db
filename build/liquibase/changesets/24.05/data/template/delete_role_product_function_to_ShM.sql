--liquibase formatted sql

--changeset postgres:delete_role_product_function_to_ShM_1 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1283 Update the landing page tiles to have the new shipment manager connection
do $$
declare sh_manager_product_id int;
declare sh_pf_Approve int;
declare sh_pf_Create int;
declare sh_pf_Delete int;
declare sh_pf_Export int;
declare sh_pf_Modify int;
declare sh_pf_Print int;
declare sh_pf_Read int;
declare sh_pf_Reject int;
declare sh_pf_Search int;
declare sh_pf_Submit int;
declare sh_pf_View int;
declare sh_pf_Processing int;
declare sh_pf_On_hold int;
declare sh_pf_Change_Status int;

declare role_admin int;
declare role_SM_Admin int;
declare role_Approver int;
declare role_Data_Manager int;
declare role_Service_Provider int;
declare role_User int;


begin
	SELECT id from core.product where name = 'Shipment Manager' into sh_manager_product_id;
    SELECT id from "security".product_function where action = 'Approve' AND product_id = sh_manager_product_id into sh_pf_Approve;
    SELECT id from "security".product_function where action = 'Create' AND product_id = sh_manager_product_id into sh_pf_Create;
    SELECT id from "security".product_function where action = 'Delete' AND product_id = sh_manager_product_id into sh_pf_Delete;
    SELECT id from "security".product_function where action = 'Export' AND product_id = sh_manager_product_id into sh_pf_Export;
    SELECT id from "security".product_function where action = 'Modify' AND product_id = sh_manager_product_id into sh_pf_Modify;
    SELECT id from "security".product_function where action = 'Print' AND product_id = sh_manager_product_id into sh_pf_Print;
    SELECT id from "security".product_function where action = 'Read' AND product_id = sh_manager_product_id into sh_pf_Read;
    SELECT id from "security".product_function where action = 'Reject' AND product_id = sh_manager_product_id into sh_pf_Reject;
    SELECT id from "security".product_function where action = 'Search' AND product_id = sh_manager_product_id into sh_pf_Search;
    SELECT id from "security".product_function where action = 'Submit' AND product_id = sh_manager_product_id into sh_pf_Submit;
    SELECT id from "security".product_function where action = 'View' AND product_id = sh_manager_product_id into sh_pf_View;
    SELECT id from "security".product_function where action = 'Processing' AND product_id = sh_manager_product_id into sh_pf_Processing;
    SELECT id from "security".product_function where action = 'On hold' AND product_id = sh_manager_product_id into sh_pf_On_hold;
    SELECT id from "security".product_function where action = 'Change Status' AND product_id = sh_manager_product_id into sh_pf_Change_Status;

    SELECT id from "security"."role" where name = 'Admin' into role_admin;
    SELECT id from "security"."role" where name = 'SM Admin' into role_SM_Admin;
    SELECT id from "security"."role" where name = 'Approver' into role_Approver;
    SELECT id from "security"."role" where name = 'Data Manager' into role_Data_Manager;
    SELECT id from "security"."role" where name = 'Service Provider' into role_Service_Provider;
    SELECT id from "security"."role" where name = 'User' into role_User;

DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Approve AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Approve AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Approve AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Create AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Create AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Create AND role_id=role_Data_Manager;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Create AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Delete AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Delete AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Delete AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Export AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Export AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Export AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Modify AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Modify AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Modify AND role_id=role_Data_Manager;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Modify AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Print AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Print AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Print AND role_id=role_Data_Manager;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Print AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Print AND role_id=role_Service_Provider;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Read AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Read AND role_id=role_User;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Read AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Read AND role_id=role_Data_Manager;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Read AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Read AND role_id=role_Service_Provider;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Reject AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Reject AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Reject AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Search AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Search AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Search AND role_id=role_Data_Manager;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Search AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Search AND role_id=role_Service_Provider;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Submit AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Submit AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Submit AND role_id=role_Data_Manager;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_View AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_View AND role_id=role_User;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_View AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_View AND role_id=role_Data_Manager;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_View AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_View AND role_id=role_Service_Provider;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Processing AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Processing AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Processing AND role_id=role_Service_Provider;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_On_hold AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_On_hold AND role_id=role_SM_Admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_On_hold AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Change_Status AND role_id=role_admin;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Change_Status AND role_id=role_Data_Manager;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Change_Status AND role_id=role_Approver;
DELETE FROM "security".role_product_function
	WHERE product_function_id=sh_pf_Change_Status AND role_id=role_Service_Provider;

end$$;