--liquibase formatted sql

--changeset postgres:update_first_request_id_per_batch context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1233 create first request id rule to make batch codes unique

UPDATE core.segment_api
	SET "type"='GRAPHQL',path_template='graphql',"method"='POST',response_mapping='data.findRequest.id',body_template='query{findRequest(id:{}){id,requestCode}}'
	WHERE response_mapping='result.data[0].id';

