--liquibase formatted sql

--changeset postgres:update_node_email_definition_irri_cimmyt context:template splitStatements:false rollbackSplitStatements:false
--comment: Update Email Node definition

UPDATE workflow.node
	SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "11",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": "j.ramirez@cimmyt.org"
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
	WHERE id=21635;

UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "20",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": "OK"
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": false,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21031;

UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "20",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": "OK"
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": false,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=20696;

UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21631;
UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21639;

UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21630;
UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21632;
UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21637;
UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21636;
UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21633;
UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21638;
UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21634;
UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "10",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21640;

UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "11",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21629;
UPDATE workflow.node
  SET define='{
  "id": 4,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "email": "11",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "contacts": [
    {
      "contact": ""
    }
  ],
  "disabled": false,
  "sendToSHU": true,
  "inputProps": {
    "sourceNodes": []
  },
  "attachments": {
    "attach": false,
    "templates": [
      {
        "format": "",
        "template": ""
      }
    ]
  },
  "outputProps": {
    "targetNodes": []
  },
  "sendToSender": true,
  "sendToRecipient": true,
  "sendToRequestor": true
}'::jsonb
  WHERE id=21635;
