--liquibase formatted sql

--changeset postgres:update_node_items_irri_cimmyt_01 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-792 Shipment Outgoing CIMMYT: remove check box Is dangerous from the basic info and the Items list schedule

UPDATE workflow.node
	SET define='{
  "id": 8,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "columns": [
      {
        "name": "testCode",
        "alias": "GHT",
        "hidden": false
      },
      {
        "name": "isDangerous",
        "hidden": true
      },
        {
        "name": "mtaStatus",
        "hidden": true
      },
            {
        "name": "geneticStock",
        "hidden": true
      },
            {
        "name": "origin",
        "hidden": true
      }
    ],
    "bulkOptions": [
      "Status",
      "Quantities",
      "Seed Received Date",
      "Sync from CB"
    ]
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "component": "Items",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=20665;
UPDATE workflow.node
	SET define='{
  "id": 8,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "columns": [
      {
        "name": "testCode",
        "alias": "RSHT",
        "hidden": false
      },
      {
        "name": "referenceNumber",
        "hidden": true
      },{
        "name": "receivedDate",
        "hidden": true
      }
    ],
    "bulkOptions": [
      "Status",
      "Quantities",
      "MTA Status",
      "Dangerous",
      "Sync from CB"
    ]
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "component": "Items",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21025;

UPDATE workflow.workflow
	SET security_definition='{
  "roles": [
    {
      "id": "2",
      "name": "Admin",
      "isSystem": true,
      "__typename": "Role",
      "description": "admin"
    }
  ]
}'::jsonb
	WHERE id=496;
UPDATE workflow.workflow
	SET security_definition='{
  "roles": [
    {
      "id": "2",
      "name": "Admin",
      "isSystem": true,
      "__typename": "Role",
      "description": "admin"
    }
  ]
}'::jsonb
	WHERE id=583;
UPDATE workflow.workflow
	SET security_definition='{
  "roles": [
    {
      "id": "2",
      "name": "Admin",
      "isSystem": true,
      "__typename": "Role",
      "description": "admin"
    }
  ]
}'::jsonb
	WHERE id=562;

UPDATE workflow.node
  SET security_definition='{
  "roles": [
    {
      "id": "2",
      "name": "Admin",
      "isSystem": true,
      "__typename": "Role"
    }
  ]
}'::jsonb;
