--liquibase formatted sql

--changeset postgres:update_print_preview_nodes_definition_irri context:template splitStatements:false rollbackSplitStatements:false
--comment:  BDS-1321 Add all Print Templates Nodes for IRRI dedicated workflow

UPDATE workflow.node
	SET "name"='Print Shipment Cross Boundary',description='Print Shipment Cross Boundary',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_shipment_cross_boundary_cimmyt",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,help='Print Shipment Cross Boundary'
	WHERE id=21520;
UPDATE workflow.node
	SET "name"='Print Shipment Cross Boundary',description='Print Shipment Cross Boundary',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_shipment_cross_boundary_cimmyt",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,help='Print Shipment Cross Boundary'
	WHERE id=21526;
UPDATE workflow.node
	SET "name"='Print Shipment Cross Boundary',description='Print Shipment Cross Boundary',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_shipment_cross_boundary_cimmyt",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,help='Print Shipment Cross Boundary'
	WHERE id=21525;
UPDATE workflow.node
	SET "name"='Print Shipment Cross Boundary',description='Print Shipment Cross Boundary',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_shipment_cross_boundary_cimmyt",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,help='Print Shipment Cross Boundary'
	WHERE id=21527;
UPDATE workflow.node
	SET "name"='Print Shipment Cross Boundary',description='Print Shipment Cross Boundary',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_shipment_cross_boundary_cimmyt",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,help='Print Shipment Cross Boundary'
	WHERE id=21528;
UPDATE workflow.node
	SET "name"='Print Shipment Cross Boundary',description='Print Shipment Cross Boundary',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_shipment_cross_boundary_cimmyt",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,help='Print Shipment Cross Boundary'
	WHERE id=21507;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "specificMaterials",
        "columnValue": "GMO"
      }
    ],
    "validationFunction": "validateTemplateShow"
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_package_list_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21508;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "specificMaterials",
        "columnValue": "GMO"
      }
    ],
    "validationFunction": "validateTemplateShow"
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
   "allFlow": true,
  "allPhase": true,
  "disabled": false,
  "template": "sys_gm_certificate",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21742;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": true,
  "allPhase": true,
  "disabled": false,
  "template": "sys_inspection_or_phytosanitary_certificate",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21745;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_dispatch_note_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21521;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
   "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_inspection_or_phytosanitary_certificate",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21747;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_dispatch_note_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21523;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
   "allFlow": true,
  "allPhase": true,
  "disabled": false,
  "template": "sys_irri_transgenic_material_transfer_form",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21746;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": true,
  "allPhase": true,
  "disabled": false,
  "template": "sys_non_gm_certificate_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21741;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": true,
  "allPhase": true,
  "disabled": false,
  "template": "sys_irri_shu_service_agreement_form",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21743;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_irri_transgenic_material_transfer_form",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21748;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_dispatch_note_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21519;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": true,
  "allPhase": true,
  "disabled": false,
  "template": "sys_phytosanitary_certificate_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21744;
UPDATE workflow.node
	SET define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
    "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_dispatch_note_irri",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21524;
