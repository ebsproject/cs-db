--liquibase formatted sql

--changeset postgres:update_service_processor_product context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1143 Change recipient condition on submitted to me/unit member security rule

do
$$
declare
	_no_gsr_product_id int;
begin 
	select id from core.product where name = 'Request No-GSR' into _no_gsr_product_id;
	if (_no_gsr_product_id is not null) then
		update security.product_function set product_id = _no_gsr_product_id where action = 'Service Processor';
	end if;
end
$$;