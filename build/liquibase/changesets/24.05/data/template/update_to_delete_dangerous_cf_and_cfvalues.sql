--liquibase formatted sql

--changeset postgres:update_to_delete_dangerous_cf_and_cfvalues_01 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-792 Shipment Outgoing CIMMYT: remove check box Is dangerous from the basic info and the Items list schedule

UPDATE workflow.node_cf
  SET is_void=true
  WHERE id=871;

UPDATE workflow.cf_value
  SET is_void=true
  WHERE nodecf_id=871;