--liquibase formatted sql

--changeset postgres:add_is_dangerous_field_irri_workflow context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-792 Shipment Outgoing CIMMYT: remove check box Is dangerous from the basic info and the Items list schedule

DO $$ 
BEGIN
    BEGIN
        ALTER TABLE workflow.items ADD COLUMN is_dangerous boolean default false;
    EXCEPTION
        WHEN duplicate_column THEN
            RAISE NOTICE 'Column already exists';
    END;
END $$;