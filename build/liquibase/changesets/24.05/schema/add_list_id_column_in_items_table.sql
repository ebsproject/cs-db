--liquibase formatted sql

--changeset postgres:add_list_id_column_in_items_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-1144 Shipment outgoing CIMMYT: Improve the Update catalogue in the items list schedule csv file "Download csv items template  to include  3 new columns in the csv file 

DO $$ 
BEGIN
    BEGIN
        ALTER TABLE workflow.items ADD COLUMN list_id int4 NULL;
    EXCEPTION
        WHEN duplicate_column THEN
            RAISE NOTICE 'Column already exists';
    END;
END $$;