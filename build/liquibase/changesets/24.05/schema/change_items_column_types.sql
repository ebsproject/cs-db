--liquibase formatted sql

--changeset postgres:change_items_column_types context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-1135

ALTER TABLE workflow.items ALTER COLUMN mls_ancestors TYPE varchar USING mls_ancestors::varchar;
ALTER TABLE workflow.items ALTER COLUMN genetic_stock TYPE varchar USING genetic_stock::varchar;
