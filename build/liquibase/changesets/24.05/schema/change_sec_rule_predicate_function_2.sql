--liquibase formatted sql

--changeset postgres:change_sec_rule_predicate_function_2 context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-1143 Change recipient condition on submitted to me/unit member security rule

-- DROP FUNCTION "security".secrulepredicate(int4);

CREATE OR REPLACE FUNCTION security.secrulepredicate(_userid integer)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
	rec record;
	secRuleWhere text default '(';
	adminUser bool default false;
	contactId int;
	memberContactId int;
	_user_id int;
	service_provider_ids INTEGER[];
	duplicateRule bool default false;
	isServiceProcessor bool default false;
begin
	select contact_id from security.user where id = _userId into contactId;
	select is_admin from security.user where id = _userId into adminUser;
	if (contactId is null)
	then
		return 'No contact/user found';
	end if;
	if (adminUser)
	then
		secRuleWhere := secRuleWhere || '1=1' || ')';
		return secRuleWhere;
	end if;
	select array_agg(parent_id) 
		from crm.contact_hierarchy 
		where contact_id = contactId
			and is_void = false 
		into service_provider_ids;
	if (service_provider_ids is null) then
		return '(1=2)';
	end if;
	for rec in (
		select distinct(rule.name) as rule_name
		from security.user u
		join crm.contact cont on u.contact_id = cont.id
		join crm.contact_hierarchy ch on ch.contact_id = cont.id 
		join security.role_contact_hierarchy rch on rch.contact_hierarchy_id = ch.id 
		join security.role role on role.id = rch.role_id 
		join security.rule_role rr on rr.role_id = role.id 
		join security.rule rule on rr.rule_id = rule.id 
		where u.id = _userId
	) loop
		if (rec.rule_name = '@CreateByAnyUser')
		then
			secRuleWhere := secRuleWhere || '1=1' || ')';
			return secRuleWhere;
		else 
			if (rec.rule_name = '@CreatedByUnitMember')
			then
				secRuleWhere = secRuleWhere || '((';
				foreach _user_id in ARRAY (select * from crm.getPartnersUserIds(contactId)) loop
					select contact_id from security.user where id = _user_id into memberContactId;
					secRuleWhere := secRuleWhere || 's.creator_id=' || _user_id || ' or '
					|| 's.requestor_id=' || memberContactId || ' or '
					|| 's.sender_id=' || memberContactId || ' or ';
				end loop;
				secRuleWhere := left(secRuleWhere, -3);
				secRuleWhere := secRuleWhere || ') and s.serviceprovider_id = any(''{'|| array_to_string(service_provider_ids, ',','0') || '}''::int[])) or ';
			end if;
			if ((rec.rule_name = '@SubmittedToUnitMember' or rec.rule_name = '@SubmittedToMe') and not duplicateRule)
			then
				duplicateRule = true;
					secRuleWhere = secRuleWhere || '((select true from crm.contact_hierarchy ch join security.role_contact_hierarchy rch on ch.id = rch.contact_hierarchy_id join security.role_product_function rpf on rch.role_id = rpf.role_id  join security.product_function pf on rpf.product_function_id = pf.id where ch.contact_id = ' || contactId || ' and pf.action = ''Service Processor'' and ch.is_void = false and ch.parent_id = s.serviceprovider_id) and s.submition_date is not null ';
					secRuleWhere := secRuleWhere || ' and s.serviceprovider_id = any(''{'|| array_to_string(service_provider_ids, ',','0') || '}''::int[])) or ';
			end if;
			if (rec.rule_name = '@CreatedByMe')
			then
				secRuleWhere := secRuleWhere || 's.creator_id=' || _userId || ' or '
					|| 's.requestor_id=' || contactId || ' or '
					|| 's.sender_id=' || contactId || ' or ';
			end if;
		end if;
	end loop;
	if (secRuleWhere = '(') then
		return '(1=2)';
	end if;
	secRuleWhere := left(secRuleWhere, -3);
	secRuleWhere := secRuleWhere || ')';
return secRuleWhere;
END;
$function$
;