--liquibase formatted sql

--changeset postgres:change_servicetable_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-1143 Change recipient condition on submitted to me/unit member security rule

CREATE OR REPLACE FUNCTION workflow.servicetable(_workflowid integer)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare strType text;
        strInsert text;
		strInsertCol text;
		strInsertValue text;
		strCmd text;
	    numIdx int;
        numIteration int;
		recCols record;
		recRows record;
	    recServices record;
	   	hasRecords bool;
BEGIN
	strType:= '(id int4,"wfInstance" int4,';
	for recCols in  select
			w.id as WorkflowId,
 		    cf.name as ColumnName,
			ct."type" ,
			ct."name",
			case when ct."type" = 'String' then 'varchar(300)' else 'int4' end ColumnType
			from workflow.workflow w
			join workflow.node wn on wn.workflow_id = w.id
			join workflow.node_cf cf on	wn.id = cf.node_id 
			join workflow.cf_type ct on cf.cftype_id =ct.id 
			where w.id=_workflowId and cf.attributes_id is null and cf.is_void=false
	loop
  		strType:= strType || '  ' || quote_ident(recCols.ColumnName) || ' ' || recCols.ColumnType || ',';
	end loop;
 	strType := substr(strType,1,length(strType)-1);
 	strType := strType || ')';
	drop table if exists _x;
	strCmd := 'CREATE TEMP TABLE _servicetable ' || strType || ' on commit drop';
	execute(strCmd);
	strInsertCol := '"id","wfInstance"';
	strInsertValue := '';
	numIdx := 0;
	for recServices in
		select
			s.id,
			wi.id as wf_instance
		from
			workflow.workflow w
		join workflow.wf_instance wi on
			w.id = wi.workflow_id
		join workflow."event" e on
			wi.id = e.wf_instance_id
		join workflow.service s on
			e.instance_id = s.id
		where
			w.id = _workflowId
		group by
			s.id,
			wi.id
	loop 
		select
			true
		from 
			workflow.workflow w
			join workflow.node wn on wn.workflow_id = w.id
			join workflow.node_cf cf on wn.id = cf.node_id
			join workflow.cf_type cft on cf.cftype_id = cft.id
			join workflow.wf_instance wi on w.id = wi.workflow_id
			join workflow.event wfevent on wi.id = wfevent.wf_instance_id and wfevent.node_id = wn.id
			join workflow.service r on r.id = wfevent.instance_id
			join workflow.cf_value wcfv on wfevent.id = wcfv.event_id and wcfv.nodecf_id = cf.id
		where
			w.id = _workflowId
			and r.id = recServices.id
			and cf.is_void = false
		order by
			r.id
		limit 1 into hasRecords;
		if (hasRecords) 
			then
				for recRows in 
					select
						r.id as id,
						cf.name as field,
						case when not wcfv.text_value is null 
							then coalesce(wcfv.text_value, null::character varying)
							else
								case when not wcfv.code_value is null 
									then wcfv.code_value::character varying
									else
										case when wcfv.flag_value = true
											then coalesce(wcfv.flag_value::character varying, null::character varying)
											else
												case when not wcfv.date_value is null
													then coalesce(wcfv.date_value::character varying, null::character varying)
													else 
														case when not wcfv.num_value is null
															then coalesce(wcfv.num_value::character varying, null::character varying)
															else null::character varying
														end
												end
										end
								end
						end as value
					from
						workflow.workflow w
						join workflow.node wn on wn.workflow_id = w.id
						join workflow.node_cf cf on wn.id = cf.node_id
						join workflow.cf_type cft on cf.cftype_id = cft.id
						join workflow.wf_instance wi on w.id = wi.workflow_id
						join workflow.event wfevent on wi.id = wfevent.wf_instance_id and wfevent.node_id = wn.id
						join workflow.service r on r.id = wfevent.instance_id
						join workflow.cf_value wcfv on wfevent.id = wcfv.event_id and wcfv.nodecf_id = cf.id
					where
						w.id = _workflowId
						and r.id = recServices.id
						and cf.is_void = false
					order by
						r.id
				loop     
					if numIdx = 0 then 
						strInsertValue := strInsertValue || '(' || quote_literal(coalesce(recRows.id::character varying,
							null::character varying));
						strInsertValue := strInsertValue || ',' || quote_literal(coalesce(recServices.wf_instance::character varying,
							null::character varying));
						numIdx := 1;
					end if;
					strInsertCol := strInsertCol || ',' || quote_ident(recRows.field);
					
					strInsertValue := strInsertValue || ',' || quote_literal(coalesce(recRows.value,
					null::character varying));
				end loop;
				strInsert := 'INSERT INTO _servicetable (' || strInsertCol || ') VALUES ' || strInsertValue || ')';
				strInsertCol := '"id","wfInstance"';
				strInsertValue := '';
				numIdx := 0;
			else
				strInsertValue := strInsertValue || '(' || quote_literal(coalesce(recServices.id::character varying,
					null::character varying));
				strInsertValue := strInsertValue || ',' || quote_literal(coalesce(recServices.wf_instance::character varying,
					null::character varying));
				strInsert := 'INSERT INTO _servicetable (' || strInsertCol || ') VALUES ' || strInsertValue || ')';
				strInsertCol := '"id","wfInstance"';
				strInsertValue := '';
				numIdx := 0;
		end if;
		execute(strInsert);
	end loop;
end;
$function$
;