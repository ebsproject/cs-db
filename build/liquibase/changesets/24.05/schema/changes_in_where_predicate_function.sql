--liquibase formatted sql

--changeset postgres:changes_in_where_predicate_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-362 CS-Shipment Manager: items - fix the column filtering and sorting

CREATE OR REPLACE FUNCTION workflow.wherepredicate(_filters json)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

declare recFilter record;
	 strWhere text:= '';
	 
	BEGIN
	
 
		
 for recFilter in 
    select * from json_to_recordset(_filters) as x(col text, mod text, val text)
 loop  
 
   
   
  
  if strpos(strWhere ,'tmp_query') >0 then
     strWhere := strWhere || ' and ';
  end if;
  
   if recFilter.mod = 'EQ' then
   		
   		
   		if( strpos(recFilter.col,'.') > 0  ) then
   	    	strWhere := strWhere  || ' tmp_query.' || quote_ident(split_part(recFilter.col,'.',1)) || ' ->> ' || quote_literal(split_part(recFilter.col,'.',2)) || '  = ' || quote_literal(recFilter.val);
   		else
   		strWhere := strWhere  || ' tmp_query.' || quote_ident(recFilter.col) || '  = ' || quote_literal(recFilter.val);
   		end if;
   	
  			  	   
   end if;
  
     if recFilter.mod = 'LK' then
		if( strpos(recFilter.col,'.') > 0  ) then
		strWhere := strWhere  || ' LOWER(tmp_query.' || quote_ident(split_part(recFilter.col,'.',1)) || ' ->> ' || quote_literal(split_part(recFilter.col,'.',2)) || ')  LIKE ' ||  quote_literal('%' || recFilter.val || '%');	
	   		
	   		else
	  		strWhere := strWhere || ' LOWER(tmp_query.' || quote_ident(recFilter.col) || ') LIKE ' || quote_literal('%' || recFilter.val || '%');	  	   
	  	end if;
   end if;
  
  
  
   raise notice 'where clausula: %', strWhere;
  
 end loop;

return strWhere;
	END;
$function$
;
