--liquibase formatted sql

--changeset postgres:changes_main_function_items_count context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-362 CS-Shipment Manager: items - fix the column filtering and sorting

CREATE OR REPLACE FUNCTION workflow.jsonb_cf_filter(_workflow_id integer, _user_id integer, _filters_txt text, _sorts_txt text, pagenumber integer DEFAULT 1, pagesize integer DEFAULT 100)
 RETURNS jsonb
 LANGUAGE plpgsql
AS $function$    
declare  strQuery text;
  		 strWhere text;
  		 strSorts text;
  		 strSecRuleWhere text;
  		 _filters json;
  		 _sorts json;
 		 pageOffset INTEGER :=0;
begin
if (_user_id is null) then
	return row_to_json(res) 
			from (select 0 as totalelements,
				(select array_to_json('{}'::int[])) as content) 
			res;
end if;
_filters := (SELECT cast(_filters_txt AS json));
_sorts := (SELECT cast(_sorts_txt AS json));	
perform workflow.servicetable(_workflow_id);
strQuery:= 'CREATE TEMPORARY TABLE tmp_query on commit drop AS
    SELECT 
			status.status,
		    status.statusid,
			t.*,
           (
				select row_to_json(s) from 
						( 
							 select p.contact_id as "value", p.full_name as "label" from crm.person p where p.contact_id = s.sender_id
					) s
			) as "sender", 
    	    (
				select row_to_json(r) from 
						( 
							 select p.contact_id as "value", p.full_name as "label" from crm.person p where p.contact_id = s.requestor_id
					) r
			) as "requestor",     	       
    	       (
				select row_to_json(r) from 
						( 
							 select p.contact_id as "value", p.full_name as "label" from crm.person p where p.contact_id = s.recipient_id
					) r
			) as "recipient",      
           (
				select row_to_json(sp) from 
						( 
							 select c.id as value,
								 ''('' || i.common_name || '')-'' || i.legal_name as label
                              from crm.contact c join crm.institution i on c.id = i.contact_id 
				where c.category_id =4 and c.id=s.serviceprovider_id
					) sp
			) as "serviceProvider",
    	   i.external_code as "programCode",
    	   (
				select row_to_json(p) from 
						( 
							 select contact_id as "value", p.legal_name as "label" from crm.institution p where p.contact_id = s.program_id
					) p
			) as "program", 
           s.submition_date::TEXT   as "submitionDate",
		   s.request_code as "requestCode",
		   s.description,
		   coalesce(SummaryItems."totalItems", 0)::TEXT as "totalItems",
           coalesce(SummaryItems."totalPackages",0)::TEXT as "totalPackages",
           coalesce(SummaryItems."totalWeight",0)::TEXT as "totalWeight",
           coalesce(SummaryItems."packageMaxWeight",0)::TEXT as "packageMaxWeight"
    FROM workflow.service s 
    			  inner join crm.person requestor on s.requestor_id = requestor.contact_id 
    			  inner join crm.person sender on s.sender_id = sender.contact_id
    			  inner join crm.person recipient on s.recipient_id = recipient.contact_id
         		  inner join "crm"."institution" i on s.program_id = i.contact_id 
		          inner join _servicetable t ON s.id = t.id
                  left outer join (select c.id, i.common_name,i.legal_name from crm.contact c join crm.institution i on c.id = i.contact_id 
				where c.category_id =4) as sp on s.serviceprovider_id = sp.id
				  left outer join (select service_id ,
									  count(*) as "totalItems",
									  sum(i.package_count) as "totalPackages",
									  sum(weight) as "totalWeight",
									  max(weight) as "packageMaxWeight"
								from workflow.items i where i.is_void = false
								group by service_id) as SummaryItems on SummaryItems.service_id  = s.id
				 left outer join (select s.wf_instance_id , s.instance_id as serviceId, st.description as status, st.id as statusid 
								  from workflow.status_type st join workflow.status s on st.id =s.statustype_id and s.completion_date is null) status on t."wfInstance"=status.wf_instance_id and t.id=status.serviceId
    where s.is_void =false';
   strSecRuleWhere := security.secRulePredicate(_user_id);
  	if (strSecRuleWhere = 'No contact/user found' or strSecRuleWhere is null)
  	THEN
  		return row_to_json(res) 
			from (select 0 as totalelements,
				(select array_to_json('{}'::int[])) as content) 
			res;
  	else
  		strQuery := strQuery || ' and ' || strSecRuleWhere; 
  	end if;
    execute(strQuery);
   if  json_array_length(_filters) > 0 or json_array_length(_sorts) >0  then     
	   strQuery:= 'CREATE TEMPORARY TABLE tmp_query_with_predictate on commit drop AS SELECT * FROM tmp_query ';	  
	if  json_array_length(_filters) > 0 then
    	strWhere := workflow.wherepredicate(_filters);  
	    strQuery:= strQuery ||' WHERE ' ||  strWhere;
	end if;  
       strQuery:= strQuery ||' ORDER BY ';
       pageOffset := ((pageNumber-1) * pageSize);   
    if  json_array_length(_sorts) = 0 then    	
	    strQuery:= strQuery || ' id' ;
	end if;      
	if  json_array_length(_sorts) > 0 then
    	strSorts := workflow.sortpredicate(_sorts);  
	    strQuery:= strQuery || ' ' || strSorts;
	end if;  
   strQuery:= strQuery ||' LIMIT ' || pageSize || ' OFFSET ' || pageOffset ;
raise notice '%', strQuery;
  	execute(strQuery);
    RETURN row_to_json(res) from (						 
 						select coalesce(count(*),0) as totalelements,(
 							   select array_to_json(array_agg(d)) from (select * from tmp_query_with_predictate) as d
 							    ) as "content"	
 						from tmp_query_with_predictate as st
					) res;
  end if;
  strQuery:= strQuery ||' ORDER BY id LIMIT ' || pageSize || ' OFFSET ' || pageOffset;
RETURN row_to_json(res) from (						 
 						select coalesce(count(*),0) as totalelements,(
 							   select array_to_json(array_agg(d)) from (select * from tmp_query) as d
 							    ) as "content"	
 						from tmp_query as st
					) res;
END;
$function$
;
