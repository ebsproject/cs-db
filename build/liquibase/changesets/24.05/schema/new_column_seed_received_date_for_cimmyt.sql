--liquibase formatted sql

--changeset postgres:new_column_seed_received_date_for_cimmyt context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-673 Shipment Outgoing CIMMYT: Items - add new GRID column Seed received date

DO $$ 
BEGIN
    BEGIN
        ALTER TABLE workflow.items ADD COLUMN received_date timestamp NULL;
    EXCEPTION
        WHEN duplicate_column THEN
            RAISE NOTICE 'Column already exists';
    END;
END $$;