--liquibase formatted sql

--changeset postgres:remove_dangerous_column_in_items context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-792 Shipment Outgoing CIMMYT: remove check box Is dangerous from the basic info and the Items list schedule


ALTER TABLE workflow.items 
 DROP COLUMN IF EXISTS is_dangerous;