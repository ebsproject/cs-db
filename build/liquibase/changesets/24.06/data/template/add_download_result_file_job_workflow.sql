--liquibase formatted sql

--changeset postgres:add_download_result_file_job_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: add_download_result_file_job_workflow

INSERT INTO core.job_workflow (id,job_type_id,product_function_id,translation_id,"name",description,tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (11,1,273,1,'Batch Manager','Result File Upload Batch Notification',1,now(),1,false);

SELECT setval('"core".job_workflow_id_seq', (SELECT MAX(id) FROM "core"."job_workflow"));
