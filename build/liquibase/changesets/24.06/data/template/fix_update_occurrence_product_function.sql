--liquibase formatted sql

--changeset postgres:fix_update_occurrence_product_function context:template splitStatements:false rollbackSplitStatements:false
--comment: hotfix/2563

update security.product_function set action = 'UPDATE_OCCURRENCE' where action = 'UPDATE_OCCURENCE';