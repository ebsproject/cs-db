--liquibase formatted sql

--changeset postgres:removed_item_id_column_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: Added hidden rule in column id in items irri workflow

UPDATE workflow.node
	SET define='{
  "id": 8,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": "OK"
    }
  },
  "rules": {
    "columns": [
      {
        "name": "testCode",
        "alias": "RSHT",
        "hidden": false
      },
      {
        "name": "referenceNumber",
        "hidden": true
      },
      {
        "name": "receivedDate",
        "hidden": true
      },
      {
        "name": "id",
        "hidden": true
      }
    ],
    "bulkOptions": [
      "Status",
      "Quantities",
      "Dangerous",
      "Sync from CB"
    ]
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "component": "Items",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21025;