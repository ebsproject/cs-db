--liquibase formatted sql

--changeset postgres:update_system_roles_product_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: update_system_roles_product_functions
do $$
declare _crm_product_id int;
declare _printout_product_id int;
declare _email_product_id int;

declare _crm_pf_Read int;
declare _printout_pf_Read int;
declare _email_pf_Read int;

declare _role_admin int;
declare _role_BA_Admin int;
declare _role_BA_User int;
declare _role_CB_Admin int;
declare _role_CB_User int;
declare _role_CS_Admin int;
declare _role_CS_User int;
declare _role_Data_Manager int;
declare _role_Guest int;
declare _role_Lab_Manager int;
declare _role_SM_Admin int;
declare _role_SM_User int;
declare _role_User int;


begin
	SELECT id from core.product where name = 'CRM' into _crm_product_id;
    SELECT id from core.product where name = 'Printout' into _printout_product_id;
    SELECT id from core.product where name = 'Email Template' into _email_product_id;

    SELECT id from "security".product_function where action = 'Read' AND product_id = _crm_product_id into _crm_pf_Read;
    SELECT id from "security".product_function where action = 'Read' AND product_id = _printout_product_id into _printout_pf_Read;
    SELECT id from "security".product_function where action = 'Read' AND product_id = _email_product_id into _email_pf_Read;

    SELECT id from "security"."role" where name = 'Admin' into _role_admin;
    SELECT id from "security"."role" where name = 'BA Admin' into _role_BA_Admin;
    SELECT id from "security"."role" where name = 'BA User' into _role_BA_User;
    SELECT id from "security"."role" where name = 'CB Admin' into _role_CB_Admin;
    SELECT id from "security"."role" where name = 'CB User' into _role_CB_User;
    SELECT id from "security"."role" where name = 'CS Admin' into _role_CS_Admin;
    SELECT id from "security"."role" where name = 'CS User' into _role_CS_User;
    SELECT id from "security"."role" where name = 'Data Manager' into _role_Data_Manager;
    SELECT id from "security"."role" where name = 'Guest' into _role_Guest;
    SELECT id from "security"."role" where name = 'Lab Manager' into _role_Lab_Manager;
    SELECT id from "security"."role" where name = 'SM Admin' into _role_SM_Admin;
    SELECT id from "security"."role" where name = 'SM User' into _role_SM_User;
    SELECT id from "security"."role" where name = 'User' into _role_User;


    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_admin, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_admin and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_BA_Admin, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_BA_Admin and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_BA_User, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_BA_User and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CB_Admin, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CB_Admin and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CB_User, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CB_User and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CS_Admin, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CS_Admin and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CS_User, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CS_User and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_Data_Manager, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_Data_Manager and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_Guest, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_Guest and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_Lab_Manager, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_Lab_Manager and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_SM_Admin, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_SM_Admin and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_SM_User, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_SM_User and product_function_id = _crm_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_User, _crm_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_User and product_function_id = _crm_pf_Read );




    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_admin, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_admin and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_BA_Admin, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_BA_Admin and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_BA_User, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_BA_User and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CB_Admin, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CB_Admin and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CB_User, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CB_User and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CS_Admin, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CS_Admin and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CS_User, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CS_User and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_Data_Manager, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_Data_Manager and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_Guest, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_Guest and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_Lab_Manager, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_Lab_Manager and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_SM_Admin, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_SM_Admin and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_SM_User, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_SM_User and product_function_id = _printout_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_User, _printout_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_User and product_function_id = _printout_pf_Read );




    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_admin, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_admin and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_BA_Admin, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_BA_Admin and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_BA_User, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_BA_User and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CB_Admin, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CB_Admin and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CB_User, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CB_User and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CS_Admin, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CS_Admin and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_CS_User, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_CS_User and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_Data_Manager, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_Data_Manager and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_Guest, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_Guest and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_Lab_Manager, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_Lab_Manager and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_SM_Admin, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_SM_Admin and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_SM_User, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_SM_User and product_function_id = _email_pf_Read );

    INSERT INTO "security".role_product_function (role_id, product_function_id)
    SELECT _role_User, _email_pf_Read 
    WHERE NOT EXISTS (SELECT role_id FROM  "security".role_product_function WHERE role_id = _role_User and product_function_id = _email_pf_Read );



end$$;