--liquibase formatted sql

--changeset postgres:add_list_member_id_column_in_items_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2089 SM- Shipment Outgoing IRRI: some data from CB are not properly populated

DO $$ 
BEGIN
    BEGIN
        ALTER TABLE workflow.items ADD list_member_id int4 NULL;
    EXCEPTION
        WHEN duplicate_column THEN
            RAISE NOTICE 'Column already exists';
    END;
END $$;