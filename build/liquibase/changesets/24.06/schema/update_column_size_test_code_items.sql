--liquibase formatted sql

--changeset postgres:update_size_test_code_column_items context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2089 SM- Shipment Outgoing IRRI: some data from CB are not properly populated

ALTER TABLE workflow.items ALTER COLUMN test_code TYPE varchar USING test_code::varchar;
