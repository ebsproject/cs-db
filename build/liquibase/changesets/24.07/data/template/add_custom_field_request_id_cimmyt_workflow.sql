--liquibase formatted sql

--changeset postgres:add_custom_field_request_id_cimmyt_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2404 Outgoing Seed Shipment CIMMYT: Improve reflect a new Request ID data field on the basic information section

SET session_replication_role = 'replica';

INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('requestId', 'Request ID', 'Request ID', false, 1, '2024-07-08 15:10:54.248', NULL, 1, NULL, false, 1392, 1, 1, 20662,
 '{"id": 1, "name": "requestId", "sort": 19.1, "rules": {"required": ""}, "sizes": [12, 12, 6, 4, 3], "helper": {"title": "Request ID", "placement": "top"}, "inputProps": {"rows": 1, "label": "Request ID", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}}, "defaultValue": ""}'::jsonb, '', NULL) ON CONFLICT DO NOTHING;

SET session_replication_role = 'origin';

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));