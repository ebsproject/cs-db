--liquibase formatted sql

--changeset postgres:add_custom_field_same_as_requestor_control_irri_and_cimmyt_workflows context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1071 shipment manager CIMMYT and IRRI - Have checkbox to select if Requestor is the same as Recipient.

SET session_replication_role = 'replica';

INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('sameAsRequestor', 'Set Same As Requestor', 'Set Same As Requestor', false, 1, '2024-07-09 18:37:12.805', '2024-07-09 19:54:00.710', 1, 1, false, 1493, 6, 1, 20992, '{"id": 6, "sort": 22.1, "rules": {"required": ""}, "sizes": [12, 12, 12, 12, 12], "helper": {"title": "Set Recipient same as requestor", "placement": "top"}, "function": "setRecipientSameAsRequestor", "inputProps": {"color": "primary", "label": "Set Requestor Information to Recipient", "disabled": false}, "showInGrid": false, "defaultValue": false}'::jsonb, '', NULL);
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('sameAsRequestor', 'Set Same As Requestor', 'Set Same As Requestor', false, 1, '2024-07-09 18:37:12.805', '2024-07-09 19:54:00.710', 1, 1, false, 1494, 6, 1, 20662, '{"id": 6, "sort": 18.1, "rules": {"required": ""}, "sizes": [12, 12, 12, 12, 12], "helper": {"title": "Set Recipient same as requestor", "placement": "top"}, "function": "setRecipientSameAsRequestor", "inputProps": {"color": "primary", "label": "Set Requestor Information to Recipient", "disabled": false}, "showInGrid": false, "defaultValue": false}'::jsonb, '', NULL);


SET session_replication_role = 'origin';

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));