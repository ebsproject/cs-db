--liquibase formatted sql

--changeset postgres:add_germplasm_coding_product_function context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2407 Add GERMPLASM_CODING product function

do $$
declare
	_admin_role_id int;
	_dm_role_id int;
	_ul_role_id int;
	_ul_cb_role_id int;
	_germplasm_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'Data Manager' into _dm_role_id;
	select id from security.role where name = 'Unit Leader' into _ul_role_id;
	select id from security.role where name = 'Unit Leader CB' into _ul_cb_role_id;
	select id from core.product where name = 'Germplasm' into _germplasm_p_id;
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Germplasm Coding', true, 'GERMPLASM_CODING', 1, _germplasm_p_id, true
	where not exists (select id from security.product_function where action = 'GERMPLASM_CODING' and product_id = _germplasm_p_id);
	select id from security.product_function where action = 'GERMPLASM_CODING' and product_id = _germplasm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_dm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _dm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_ul_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _ul_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_ul_cb_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _ul_cb_role_id and product_function_id = _pf_id);
end $$;