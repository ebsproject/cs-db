--liquibase formatted sql

--changeset postgres:add_new_status_done_in_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2432 Shipment Outgoing IRRI: Apply validation and remove column/step

SET session_replication_role = 'replica';

INSERT INTO workflow.status_type
("name", description, help, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, workflow_id)
VALUES('Done', 'Done', 'Done', 1, '2024-07-16 19:14:58.539', NULL, 1, NULL, false, 452, 562) ON CONFLICT DO NOTHING;

SET session_replication_role = 'origin';

SELECT setval('workflow.status_type_id_seq', (SELECT MAX(id) FROM "workflow"."status_type"));

UPDATE workflow.node
  SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Done."
    }
  },
  "rules": {
    "target": "sender,requestor,recipient",
    "allowEdit": false,
    "allowView": true,
    "allowDelete": false,
    "allowAddNotes": false,
    "allowViewProgress": true
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "status": "452",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
  WHERE id=21110;
