--liquibase formatted sql

--changeset postgres:add_role_actions_product_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2465 Add role actions product functions

do $$
declare
	_um_product_id int;
	_admin_id int;
begin
	select id from core.product where name = 'User Management' into _um_product_id;
	select id from security.role where name = 'Admin' into _admin_id;
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Create role',true,'Create_Role',1,_um_product_id,true
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Create_Role' and product_id = _um_product_id);
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Create_Role' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Create_Role' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Modify role',true,'Modify_Role',1,_um_product_id,true
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Modify_Role' and product_id = _um_product_id);
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Modify_Role' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Modify_Role' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Delete role',true,'Delete_Role',1,_um_product_id,true
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Delete_Role' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Delete_Role' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Delete_Role' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Read role records',true,'Read_Role',1,_um_product_id,true
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Read_Role' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Read_Role' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Read_Role' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Export role records',true,'Export_Role',1,_um_product_id,false
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Export_Role' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Export_Role' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Export_Role' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Print role records',true,'Print_Role',1,_um_product_id,false
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Print_Role' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Print_Role' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Print_Role' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Search role records',true,'Search_Role',1,_um_product_id,false
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Search_Role' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Search_Role' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Search_Role' and product_id = _um_product_id));
end $$;