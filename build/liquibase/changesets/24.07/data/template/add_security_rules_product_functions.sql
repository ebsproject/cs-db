--liquibase formatted sql

--changeset postgres:add_security_rules_product_functions context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2465 Add security rules product functions

do $$
declare
	_um_product_id int;
	_admin_id int;
begin
	select id from core.product where name = 'User Management' into _um_product_id;
	select id from security.role where name = 'Admin' into _admin_id;
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Create security rule',true,'Create_Security_Rule',1,_um_product_id,true
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Create_Security_Rule' and product_id = _um_product_id);
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Create_Security_Rule' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Create_Security_Rule' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Modify security rule',true,'Modify_Security_Rule',1,_um_product_id,true
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Modify_Security_Rule' and product_id = _um_product_id);
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Modify_Security_Rule' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Modify_Security_Rule' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Delete security rule',true,'Delete_Security_Rule',1,_um_product_id,true
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Delete_Security_Rule' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Delete_Security_Rule' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Delete_Security_Rule' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Read security rule records',true,'Read_Security_Rule',1,_um_product_id,true
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Read_Security_Rule' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Read_Security_Rule' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Read_Security_Rule' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Export security rule records',true,'Export_Security_Rule',1,_um_product_id,false
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Export_Security_Rule' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Export_Security_Rule' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Export_Security_Rule' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Print security rule records',true,'Print_Security_Rule',1,_um_product_id,false
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Print_Security_Rule' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Print_Security_Rule' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Print_Security_Rule' and product_id = _um_product_id));
	INSERT INTO 
	security.product_function  
	(
		description,
		system_type,
		action,
		creator_id,
		product_id,
		is_data_action
	)
	select 'Search security rule records',true,'Search_Security_Rule',1,_um_product_id,false
	WHERE NOT EXISTS (SELECT id FROM security.product_function WHERE action = 'Search_Security_Rule' and product_id = _um_product_id); 
	INSERT INTO 
	security.role_product_function (role_id, product_function_id)
    select _admin_id, (SELECT id FROM security.product_function WHERE action = 'Search_Security_Rule' and product_id = _um_product_id)
    where not exists (select role_id from security.role_product_function where role_id = _admin_id and product_function_id = (SELECT id FROM security.product_function WHERE action = 'Search_Security_Rule' and product_id = _um_product_id));
end $$;