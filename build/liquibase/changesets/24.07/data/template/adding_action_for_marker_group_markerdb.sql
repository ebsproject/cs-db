--liquibase formatted sql

--changeset postgres:adding_action_for_marker_group_markerdb context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2233 Adding new action for marker group on grid VIEW EDIT DELETE
do $$

declare _gm_id int;

begin

SELECT id from "core"."product" where name = 'MarkerDb' INTO _gm_id;


INSERT INTO 
	"security".product_function  
	(
		description,
		system_type,
		"action",
		creation_timestamp,
		creator_id,
		is_void,
		product_id,
		is_data_action
	)
	select 'View',true,'View','2024-07-04 09:53:50.826',1,false,_gm_id,true
WHERE NOT EXISTS (SELECT id FROM "security".product_function r WHERE r.action = 'View' and r.product_id = _gm_id); 


end $$;