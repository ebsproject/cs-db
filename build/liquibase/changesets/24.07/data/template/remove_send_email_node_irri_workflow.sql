--liquibase formatted sql

--changeset postgres:remove_send_email_node_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2296 Shipment Outgoing IRRI: Improve email  automation


DELETE FROM workflow.node_stage
WHERE stage_id=10698 AND node_id=21031;

UPDATE workflow.node
  SET is_void=true
  WHERE id=21031;
