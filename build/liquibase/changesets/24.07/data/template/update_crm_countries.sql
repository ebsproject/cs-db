--liquibase formatted sql

--changeset postgres:update_crm_countries context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1856 Update crm countries

update crm.country set iso = 'CD' where name = 'Congo Democratic Republic of';

update crm.country set name = 'Bosnia and Herzegovina' where name = 'Bosnia and Herzegowina';

update crm.country set name = 'Congo, Democratic Republic of' where name = 'Congo Democratic Republic of';

update crm.country set name = 'Republic of North Macedonia'  where name = 'Macedonia, The Former Yugoslav Republic of';

update crm.country set name = 'Korea, Republic of' where name = 'South Korea';

update crm.country set name = 'Slovakia' where name = 'Slovakia(Slovak Republic)';

update crm.country set name = 'Saint Pierre and Miquelon' where name = 'St. Pierre and Miquelon';

update crm.country set name = 'Svalbard and Jan Mayen' where name = 'Svalbard and Jan Mayen Islands';

update crm.country set name = 'Eswatini' where name = 'Swaziland';

insert into crm.country (name, iso, creator_id)
select 'Bonaire, Sint Eustatius and Saba', 'BQ', 1
where not exists (select id from crm.country where name = 'Bonaire, Sint Eustatius and Saba');

insert into crm.country (name, iso, creator_id)
select 'Congo', 'CG', 1
where not exists (select id from crm.country where name = 'Congo');

insert into crm.country (name, iso, creator_id)
select 'Saint Barthelemy', 'BL', 1
where not exists (select id from crm.country where name = 'Saint Barthelemy');

insert into crm.country (name, iso, creator_id)
select 'Saint Martin (French part)', 'MF', 1
where not exists (select id from crm.country where name = 'Saint Martin (French part)');

insert into crm.country (name, iso, creator_id)
select 'Sint Maarten (Dutch part)', 'SX', 1
where not exists (select id from crm.country where name = 'Sint Maarten (Dutch part)');

insert into crm.country (name, iso, creator_id)
select 'South Sudan', 'SS', 1
where not exists (select id from crm.country where name = 'South Sudan');