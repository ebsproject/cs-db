--liquibase formatted sql

--changeset postgres:update_items_node_irri_workflow_hide_mlsAncestors context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2239 Shipment Outgoing IRRI - remove 'mls_ancestors' column from the ITEMS GRID field

UPDATE workflow.node
  SET define='{
  "id": 8,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": "OK"
    }
  },
  "rules": {
    "columns": [
      {
        "name": "testCode",
        "alias": "RSHT",
        "hidden": false
      },
      {
        "name": "referenceNumber",
        "hidden": true
      },
      {
        "name": "receivedDate",
        "hidden": true
      },
      {
        "name": "id",
        "hidden": true
      },
      {
        "name": "mlsAncestors",
        "hidden": true
      }
    ],
    "bulkOptions": [
      "Status",
      "Quantities",
      "Dangerous",
      "Sync from CB"
    ]
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "component": "Items",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
  WHERE id=21025;