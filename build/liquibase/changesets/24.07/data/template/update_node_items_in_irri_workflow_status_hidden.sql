--liquibase formatted sql

--changeset postgres:update_node_items_in_irri_workflow_status_hidden context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2432 Shipment Outgoing IRRI: Apply validation and remove column/step
UPDATE workflow.node
  SET define='{
  "id": 8,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": "All Ok"
    }
  },
  "rules": {
    "columns": [
      {
        "name": "testCode",
        "alias": "RSHT",
        "hidden": false
      },
      {
        "name": "referenceNumber",
        "hidden": true
      },
      {
        "name": "receivedDate",
        "hidden": true
      },
      {
        "name": "id",
        "hidden": true
      },
      {
        "name": "mlsAncestors",
        "hidden": true
      },
      {
        "name": "status",
        "hidden": true
      }
    ],
    "bulkOptions": [
      "Status",
      "Quantities",
      "Dangerous",
      "Sync from CB"
    ]
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "component": "Items",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
  WHERE id=21025;