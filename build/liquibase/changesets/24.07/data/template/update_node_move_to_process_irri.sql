--liquibase formatted sql

--changeset postgres:update_node_move_to_process_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2432 Shipment Outgoing IRRI: Apply validation and remove column/step

UPDATE workflow.node
  SET "name"='MOVE TO PROCESS IRRI',help='MOVE TO PROCESS IRRI',define='{
  "id": 6,
  "node": "21027",
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "action": "",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,description='MOVE TO PROCESS IRRI'
  WHERE id=21071;

  UPDATE workflow.node
  SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Submitted"
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "ERROR",
      "functions": "validateItems",
      "onSuccess": "SUCCESS"
    }
  },
  "status": "365",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
  WHERE id=21070;
