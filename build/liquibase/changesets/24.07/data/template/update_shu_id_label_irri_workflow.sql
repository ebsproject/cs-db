--liquibase formatted sql

--changeset postgres:update_shu_id_label_and_properties_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1524 Shipment manager IRRI: Improve Legal documents generation action to allow a user key in data in the SHU request ID field so that it is reflected on the SMTA doc in generation

UPDATE workflow.node_cf
  SET description='SHU-ID',help='SHU-ID',required=true,field_attributes='{
  "id": 1,
  "name": "shuId",
  "sort": 3,
  "rules": {
    "required": "The SHU ID is required"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "helper": {
    "title": "SHU-ID",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "SHU-ID",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb,"name"='shuId'
  WHERE id=762;
