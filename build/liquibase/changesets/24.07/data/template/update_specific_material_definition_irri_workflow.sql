--liquibase formatted sql

--changeset postgres:update_specific_material_definition_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2403 Outgoing seed shipment IRRI: Improve the specific materials catalogue to reflect Transgenic as one of the choices in the drop down menu

UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 5,
  "rules": {
    "required": "This field is required"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "helper": {
    "title": "Specific Materials",
    "placement": "top"
  },
  "inputProps": {
    "color": "primary",
    "label": "Specific Materials",
    "variant": "outlined"
  },
  "showInGrid": false,
  "defaultOptions": [
    {
      "label": "Seeds and genetic resources",
      "value": 1
    },
    {
      "label": "GMO",
      "value": 2
    },
    {
      "label": "Non seeds",
      "value": 3
    },
    {
      "label": "Transgenic",
      "value": 4
    },
    {
      "label": "Other",
      "value": 5
    }
  ]
}'::jsonb
  WHERE id=764;