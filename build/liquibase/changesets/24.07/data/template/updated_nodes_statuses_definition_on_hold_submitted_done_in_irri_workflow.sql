--liquibase formatted sql

--changeset postgres:updated_nodes_statuses_definition_on_hold_submitted_done_in_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2432 Shipment Outgoing IRRI: Apply validation and remove column/step

UPDATE workflow.node
  SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Submitted"
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "ERROR",
      "functions": "validateItems",
      "onSuccess": "SUCCESS"
    }
  },
  "status": "365",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "Sender",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true
    },
    {
      "actor": "Requestor",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false
    },
    {
      "actor": "Recipient",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false
    }
  ]
}'::jsonb
  WHERE id=21070;

UPDATE workflow.node
  SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to On Hold"
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "status": "368",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "Sender",
      "allowEdit": true,
      "allowView": true,
      "allowDelete": true,
      "allowAddNotes": true,
      "allowViewProgress": true
    },
    {
      "actor": "Recipient",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true
    },
    {
      "actor": "SHU",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true
    },
    {
      "actor": "Requestor",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true
    }
  ]
}'::jsonb
  WHERE id=21102;

UPDATE workflow.node
  SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Done"
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "status": "452",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "sender",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "Requestor",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "Recipient",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "SHU",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": false
    }
  ]
}'::jsonb
  WHERE id=21110;