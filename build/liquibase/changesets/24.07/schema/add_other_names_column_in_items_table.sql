--liquibase formatted sql

--changeset postgres:add_other_names_column_in_items_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2240 Shipment Outgoing IRRI  - create excel report that consist all the columns of the items GRID and mls_ancestors and other_names 

DO $$ 
BEGIN
    BEGIN
        ALTER TABLE workflow.items ADD COLUMN other_names varchar NULL;
    EXCEPTION
        WHEN duplicate_column THEN
            RAISE NOTICE 'Column already exists';
    END;
END $$;