--liquibase formatted sql

--changeset postgres:add_breeding_program_manager_product_and_actions context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2200 Add Breeding Program Manager product and product functions

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_tm_role_id int;
	_bpm_p_id int;
	_pf_id int;
	_cb_domain int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'CB Admin' into _cb_admin_role_id;
	select id from security.role where name = 'Team Member' into _tm_role_id;
	select id from core.domain where prefix = 'cb' into _cb_domain;
	insert into core.product
		("name", description, help, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
	select
		'Breeding Program Manager', 'Browse and manage pipelines, market segments, and experiments.', 'Breeding Program Manager', '',  1, _cb_domain, 12, 1, 'breedingProgramManager/default/index', 'BREEDING_PROGRAM_MANAGER'
	where not exists (select id from core.product where name = 'Breeding Program Manager');
	select id from core.product where name = 'Breeding Program Manager' into _bpm_p_id;
	-- BPM_EXPERIMENTS_UPDATE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Update Experiment information', true, 'BPM_EXPERIMENTS_UPDATE', 1, _bpm_p_id, true
	where not exists (select id from security.product_function where action = 'BPM_EXPERIMENTS_UPDATE' and product_id = _bpm_p_id);
	select id from security.product_function where action = 'BPM_EXPERIMENTS_UPDATE' and product_id = _bpm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- BPM_PIPELINES_TAB product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View Pipelines tab', true, 'BPM_PIPELINES_TAB', 1, _bpm_p_id, false
	where not exists (select id from security.product_function where action = 'BPM_PIPELINES_TAB' and product_id = _bpm_p_id);
	select id from security.product_function where action = 'BPM_PIPELINES_TAB' and product_id = _bpm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- BPM_PIPELINES_VIEW product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View Pipelines information', true, 'BPM_PIPELINES_VIEW', 1, _bpm_p_id, false
	where not exists (select id from security.product_function where action = 'BPM_PIPELINES_VIEW' and product_id = _bpm_p_id);
	select id from security.product_function where action = 'BPM_PIPELINES_VIEW' and product_id = _bpm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- BPM_MARKET_SEGMENTS_TAB product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View Market Segments tab', true, 'BPM_MARKET_SEGMENTS_TAB', 1, _bpm_p_id, false
	where not exists (select id from security.product_function where action = 'BPM_MARKET_SEGMENTS_TAB' and product_id = _bpm_p_id);
	select id from security.product_function where action = 'BPM_MARKET_SEGMENTS_TAB' and product_id = _bpm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- BPM_MARKET_SEGMENTS_VIEW product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View Market Segment information', true, 'BPM_MARKET_SEGMENTS_VIEW', 1, _bpm_p_id, false
	where not exists (select id from security.product_function where action = 'BPM_MARKET_SEGMENTS_VIEW' and product_id = _bpm_p_id);
	select id from security.product_function where action = 'BPM_MARKET_SEGMENTS_VIEW' and product_id = _bpm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- BPM_EXPERIMENTS_TAB product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View Experiments tab', true, 'BPM_EXPERIMENTS_TAB', 1, _bpm_p_id, false
	where not exists (select id from security.product_function where action = 'BPM_EXPERIMENTS_TAB' and product_id = _bpm_p_id);
	select id from security.product_function where action = 'BPM_EXPERIMENTS_TAB' and product_id = _bpm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- BPM_EXPERIMENTS_VIEW product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View Experiments information', true, 'BPM_EXPERIMENTS_VIEW', 1, _bpm_p_id, false
	where not exists (select id from security.product_function where action = 'BPM_EXPERIMENTS_VIEW' and product_id = _bpm_p_id);
	select id from security.product_function where action = 'BPM_EXPERIMENTS_VIEW' and product_id = _bpm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
end $$;