--liquibase formatted sql

--changeset postgres:add_em_product_function context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2200 Add experiment manager upload occurrence data collection action

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_collaborator_role_id int;
	_tm_role_id int;
	_em_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'CB Admin' into _cb_admin_role_id;
	select id from security.role where name = 'Team Member' into _tm_role_id;
	select id from security.role where name = 'Collaborator' into _collaborator_role_id;
	select id from core.product where name = 'Experiment Manager' into _em_p_id;
	-- EXPERIMENT_MANAGER_UPLOAD_OCCURRENCE_DATA_COLLECTION product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Upload - Occurrence data collection', true, 'EXPERIMENT_MANAGER_UPLOAD_OCCURRENCE_DATA_COLLECTION', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_UPLOAD_OCCURRENCE_DATA_COLLECTION' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_UPLOAD_OCCURRENCE_DATA_COLLECTION' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_collaborator_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _collaborator_role_id and product_function_id = _pf_id);
end $$;