--liquibase formatted sql

--changeset postgres:add_new_cf_for_inputs_mta_options_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2481 Outgoing seed shipment IRRI: As a user with permissions, I should be able to select NO MTA or Other MTA


SET session_replication_role = 'replica';
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('pud2', 'PUD 2', 'PUD 2', false, 1, '2024-08-26 22:30:07.216', '2024-08-26 23:08:31.942', 1, 1, false, 1858, 2, 1, 20992, '{"id": 2, "sort": 27, "sizes": [12, 12, 3, 3, 3], "helper": {"title": "PUD 2", "placement": "top"}, "checked": false, "disabled": false, "inputProps": {"label": "PUD 2"}, "showInGrid": false, "defaultRules": {"showControlRules": {"expectedValue": "other-mta,no-mta", "parentControl": "mtaOptions"}}}'::jsonb, '', NULL) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('mlsCheckbox', 'MLS Checkbox', 'MLS Checkbox', false, 1, '2024-08-26 22:31:27.791', '2024-08-27 14:35:55.370', 1, 1, false, 1859, 2, 1, 20992, '{"id": 2, "sort": 25, "sizes": [12, 12, 3, 3, 3], "helper": {"title": "MLS", "placement": "top"}, "checked": false, "disabled": false, "inputProps": {"label": "MLS"}, "showInGrid": false, "defaultRules": {"showControlRules": {"expectedValue": "other-mta,no-mta", "parentControl": "mtaOptions"}}}'::jsonb, '', NULL) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('pudpud2', 'PUD & PUD 1', 'PUD & PUD 1', false, 1, '2024-08-26 22:32:32.227', '2024-08-26 23:08:31.941', 1, 1, false, 1860, 2, 1, 20992, '{"id": 2, "sort": 26, "sizes": [12, 12, 3, 3, 3], "helper": {"title": "PUD & PUD 1", "placement": "top"}, "checked": false, "disabled": false, "inputProps": {"label": "PUD & PUD 1"}, "showInGrid": false, "defaultRules": {"showControlRules": {"expectedValue": "other-mta,no-mta", "parentControl": "mtaOptions"}}}'::jsonb, '', NULL) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('mlsInput', 'MLS Input', 'MLS Input', false, 1, '2024-08-26 22:53:41.561', '2024-08-26 23:04:14.944', 1, 1, false, 1861, 1, 1, 20992, '{"id": 1, "name": "mlsInput", "sort": 25.1, "rules": {"required": ""}, "sizes": [12, 12, 9, 9, 9], "helper": {"title": "SMTA ID", "placement": "top"}, "inputProps": {"rows": 1, "label": "SMTA ID", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": false, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}, "showControlRules": {"expectedValue": "true", "parentControl": "mlsCheckbox"}}, "defaultValue": ""}'::jsonb, '', NULL) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('pudpud1Input', 'PUD & PUD 1 Input', 'PUD & PUD 1 Input', false, 1, '2024-08-26 22:55:10.588', '2024-08-26 23:04:14.944', 1, 1, false, 1862, 1, 1, 20992, '{"id": 1, "name": "pudpud1Input", "sort": 26.1, "rules": {"required": ""}, "sizes": [12, 12, 9, 9, 9], "helper": {"title": "SMTA ID", "placement": "top"}, "inputProps": {"rows": 1, "label": "SMTA ID", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}, "showControlRules": {"expectedValue": "true", "parentControl": "pudpud2"}}, "defaultValue": ""}'::jsonb, '', NULL) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('pud2Input', 'PUD 2 Input', 'PUD 2 Input', false, 1, '2024-08-26 22:56:18.099', '2024-08-26 23:04:14.944', 1, 1, false, 1863, 1, 1, 20992, '{"id": 1, "name": "pud2Inpud", "sort": 27.1, "rules": {"required": ""}, "sizes": [12, 12, 9, 9, 9], "helper": {"title": "SMTA ID", "placement": "top"}, "inputProps": {"rows": 1, "label": "SMTA ID", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}, "showControlRules": {"expectedValue": "true", "parentControl": "pud2"}}, "defaultValue": ""}'::jsonb, '', NULL) ON CONFLICT DO NOTHING;

SET session_replication_role = 'origin';

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));

UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 4,
  "row": false,
  "sort": 24,
  "rules": {
    "required": "Please select a MTA option."
  },
  "sizes": [
    12,
    12,
    12,
    12,
    12
  ],
  "helper": {
    "title": "MTA Options",
    "placement": "top"
  },
  "options": [
    {
      "label": "No MTA",
      "value": "no-mta"
    },
    {
      "label": "Other MTA",
      "value": "other-mta"
    },
    {
      "label": "SMTA ID not required",
      "value": "no-required"
    }
  ],
  "disabled": false,
  "inputProps": {
    "label": "MTA Options"
  },
  "showInGrid": false,
  "defaultValue": "no-mta"
}'::jsonb
	WHERE id=1757;
