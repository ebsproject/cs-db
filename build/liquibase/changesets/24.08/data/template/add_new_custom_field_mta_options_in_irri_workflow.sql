--liquibase formatted sql

--changeset postgres:add_new_custom_field_mta_options_in_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2481 Outgoing seed shipment IRRI: As a user with permissions, I should be able to select NO MTA or Other MTA

SET session_replication_role = 'replica';
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('mtaOptions', 'MTA Options', 'MTA Options', true, 1, '2024-08-22 20:07:31.001', '2024-08-22 20:09:04.570', 1, 1, false, 1757, 4, 1, 20992, '{"id": 4, "row": false, "sort": 24, "rules": {"required": "Please select a MTA option"}, "sizes": [12, 12, 6, 3, 3], "helper": {"title": "MTA Options", "placement": "top"}, "options": [{"label": "No MTA", "value": "no-mta"}, {"label": "Other MTA", "value": "other-mta"}], "disabled": false, "inputProps": {"label": "MTA Options"}, "showInGrid": true, "defaultValue": ""}'::jsonb, '', NULL);

SET session_replication_role = 'origin';

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));

UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 1,
  "name": "shipmentPurposeTx",
  "sort": 4.1,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    6,
    4,
    4
  ],
  "helper": {
    "title": "Other Purpose",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Other Purpose",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false,
    "placeholder": "Please specify if other"
  },
  "showInGrid": false,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.fiel"
      }
    ],
    "applyRules": false,
        "showControlRules": {
      "expectedValue": "Other",
      "parentControl": "shipmentPurposeDd"
    },
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "shipmentPurposeDd",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
  WHERE id=765;
