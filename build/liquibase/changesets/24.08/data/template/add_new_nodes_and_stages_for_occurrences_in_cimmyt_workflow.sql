--liquibase formatted sql

--changeset postgres:add_new_nodes_and_stages_for_occurrences_in_cimmyt_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2457 New from occurrences -  As a user with permissions, I should be able to proceed to the basic information tab when done selecting shipment option -Create one shipment.

SET session_replication_role = 'replica';

INSERT INTO workflow.cf_type
("name", description, help, "type", creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('dialog', 'Dialog', NULL, 'String', '2024-07-22 13:20:09.542', NULL, 1, NULL, false, 10) ON CONFLICT DO NOTHING;

INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('byOccurrence', 'By Occurrence', 'By Occurrence', false, 1, '2024-08-14 18:48:46.232', NULL, 1, NULL, false, 1705, 2, 1, 20662, '{"id": 2, "sort": 1.21, "sizes": [12, 12, 12, 12, 12], "helper": {"title": "Check a value", "placement": "top"}, "checked": false, "disabled": false, "inputProps": {"label": "Check if you want to create a request base on Occurrences"}, "showInGrid": false}'::jsonb, 'byOccurrence', 2571) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('occurrencesCSVFile', 'Occurrences CSV File', 'Occurrences CSV File', false, 1, '2024-08-12 22:22:17.028', '2024-08-14 18:59:40.409', 1, 1, false, 1706, 8, 1, 20662, '{"id": 8, "name": "occurencesCSVFile", "sort": 1.23, "label": "Validate", "rules": {"required": "the csv file is required"}, "sizes": [12, 12, 12, 12, 12], "helper": {"title": "CSv File", "placement": "top"}, "disabled": false, "showInGrid": false, "customProps": {"size": "large", "color": "primary", "label": "Upload a CSV File", "classes": "", "disabled": false, "fullWidth": false, "maxFileSize": 5000000, "showPreviews": true, "acceptedFiles": [".txt", ".csv"], "cancelButtonText": "Cancel", "submitButtonText": "OK", "showFileNamesInPreview": true}, "defaultRules": {"showIfValue": "true", "parentControl": "byOccurrence"}, "defaultValue": ""}'::jsonb, '', NULL) ON CONFLICT DO NOTHING;

INSERT INTO core.process
("name", description, code, is_background, db_function, call_report, "path", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Call API', 'Execute an API', 'API', true, NULL, false, 'importFiles/occurrenceShipment', 1, '2024-08-14 13:43:10.462', NULL, 1, NULL, false, 10) ON CONFLICT DO NOTHING;

INSERT INTO core.process
("name", description, code, is_background, db_function, call_report, "path", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Conditional', 'Take a decision in the flow', 'COND', true, NULL, false, '', 1, '2024-08-14 13:43:10.462', NULL, 1, NULL, false, 11) ON CONFLICT DO NOTHING;

INSERT INTO workflow.stage
("name", description, help, "sequence", parent_id, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, phase_id, depend_on, icon, wf_view_type_id, design_ref)
VALUES('Occurrences', 'Occurrences', 'Occurrences', 3, NULL, 1, '2024-08-15 18:23:02.282', '2024-08-19 16:58:10.873', 1, 1, false, 10870, 1, 496, '{"edges": [], "width": 800, "height": 130, "position": {"x": 26.756169993459025, "y": 130.40276497056664}}'::jsonb, NULL, 1, 'd79ce81a-22ff-46d4-995c-2fe30bb21a22'::uuid) ON CONFLICT DO NOTHING;

INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Create Occurrences', 'Create Occurrences from CSV', 'Create Occurrences from CSV', 4, false, 1, '2024-08-15 18:09:50.900', '2024-08-19 16:58:10.901', 1, 1, false, 21889, 1, NULL, 496, 10, '{"id": 10, "path": "importFiles/occurrenceShipment", "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "field": "occurrencesCSVFile", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "method": "post", "bodyType": "form-data", "disabled": false, "sgcontext": "cs", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}, "queryParameters": ""}'::jsonb, '{"edges": [], "width": 100, "height": 50, "position": {"x": 278.2918836368899, "y": 70}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, 'c4fabd1a-4c08-4375-aaf3-f316630dd944'::uuid) ON CONFLICT DO NOTHING;

INSERT INTO workflow.node
(id, "name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES(21890, 'Occurrences', 'Occurrences', 'Occurrences', 1, false, 1, '2024-08-15 18:23:18.443', '2024-08-19 21:48:49.342', 1, 1, false, 1, NULL, 496, 8, '{"id": 8, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "rules": {"columns": [{"name": "", "alias": "", "hidden": false}]}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "disabled": false, "component": "Occurrences", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": [], "width": 100, "height": 50, "position": {"x": 16.01701562461642, "y": 37.652560060404596}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 7, '3d930f40-936d-4dd6-bc17-dfc88f0b9650'::uuid) ON CONFLICT DO NOTHING; 

INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Conditional target', 'Conditional target', 'Conditional target', 4, false, 1, '2024-08-15 19:06:48.377', '2024-08-19 16:58:10.884', 1, 1, false, 21892, 1, NULL, 496, 11, '{"id": 11, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "field": "byOccurrence", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "disabled": false, "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}, "expectedValue": "true", "executeNodeIfTrue": "21889", "executeNodeIfFalse": "20664"}'::jsonb, '{"edges": ["20662"], "width": 100, "height": 50, "position": {"x": 129.80291411466487, "y": 66.55390493500897}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, 'bc05ab8b-fa1b-48a1-8aaf-094742f14fa6'::uuid) ON CONFLICT DO NOTHING;

INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Move to OCC', 'Move to OCC', 'Move to OCC', 6, false, 1, '2024-08-15 19:11:44.818', '2024-08-19 16:58:10.886', 1, 1, false, 21894, 1, NULL, 496, 6, '{"id": 6, "node": "21890", "after": {"executeNode": "", "sendNotification": {"send": true, "message": "Please review the imported occurrences"}}, "action": "", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "disabled": false, "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21889"], "width": 100, "height": 50, "position": {"x": 409.74702753896815, "y": 70}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, '21276ee7-22d1-4045-b3e5-7b31e70119bc'::uuid) ON CONFLICT DO NOTHING;

INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Move to Docs', 'Move to Docs', 'Move to Docs', 1, false, 1, '2024-08-19 21:46:56.225', '2024-08-19 21:48:49.340', 1, 1, false, 21895, 1, NULL, 496, 6, '{"id": 6, "node": "20729", "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "action": "", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "disabled": false, "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"edges": ["21890"], "width": 100, "height": 50, "position": {"x": 158.74882952557584, "y": 24.864712466677588}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe", "__typename": "SecurityRule"}, {"id": "4", "name": "@SubmittedToMe", "__typename": "SecurityRule"}, {"id": "3", "name": "@CreateByAnyUser", "__typename": "SecurityRule"}, {"id": "2", "name": "@CreatedByUnitMember", "__typename": "SecurityRule"}, {"id": "5", "name": "@SubmittedToUnitMember", "__typename": "SecurityRule"}], "isSystem": true, "__typename": "Role", "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, '4448864d-77c4-44f4-9d15-bf878d38e8b5'::uuid) ON CONFLICT DO NOTHING;

SET session_replication_role = 'origin';

INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10500, 21889) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10870, 21890) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10500, 21892) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10500, 21894) ON CONFLICT DO NOTHING;
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10870, 21895) ON CONFLICT DO NOTHING;

UPDATE workflow.node
	SET depend_on='{
  "edges": [],
  "width": 100,
  "height": 50,
  "position": {
    "x": 480.65134066484205,
    "y": 66.14869835499701
  }
}'::jsonb
	WHERE id=20664;

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));
SELECT setval('workflow.cf_type_id_seq', (SELECT MAX(id) FROM "workflow"."cf_type"));
SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM "workflow"."node"));
SELECT setval('workflow.stage_id_seq', (SELECT MAX(id) FROM "workflow"."stage"));
SELECT setval('core.process_id_seq', (SELECT MAX(id) FROM "core"."process"));