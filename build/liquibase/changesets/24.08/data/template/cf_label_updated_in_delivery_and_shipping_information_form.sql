--liquibase formatted sql

--changeset postgres:cf_label_updated_in_delivery_and_shipping_information_form context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2910 Shipment Outgoing IRRI: Remove some fields in the Delivery and shipping information section

UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "valueOfProduct",
  "sort": 111,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    6,
    4,
    4
  ],
  "helper": {
    "title": "Total value of the Product",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Total value of the Product(Value only for customs purpose)",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false,
    "startAdornment": "$"
  },
  "modalPopup": {
    "show": false,
    "componentUI": ""
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=1125;