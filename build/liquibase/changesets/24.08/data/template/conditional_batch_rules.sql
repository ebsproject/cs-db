--liquibase formatted sql

--changeset postgres:create_default_maize_hierarchy context:template labels:maize,wheat splitStatements:false rollbackSplitStatements:false
--comment: BDS-2823 rules engine support for conditional codes

DO $$
DECLARE
 _rule_segment_id int;
begin
	
/*update service provider api extractor*/
UPDATE core.segment_api
	SET response_mapping='data.findInstitutionList.content[0].commonName',
	    server_url='CS',
	    body_template='query{findInstitutionList(filters:[{col:"externalCode" val:"{}" mod:EQ}]){ content{ id commonName externalCode }}}'
	WHERE response_mapping ='data.findServiceProvider.code' and server_url ='SM';
UPDATE core.segment_api
	SET response_mapping='data.findInstitutionList.content[0].legalName',
	    server_url='CS',
	    body_template='query{findInstitutionList(filters:[{col:"externalCode" val:"{}" mod:EQ}]){ content{ id legalName externalCode }}}'
	WHERE response_mapping ='data.findServiceProvider.name' and server_url ='SM';


/* insert mixed-purpose-code segment*/
SELECT max(id)+1 FROM core.rule_segment INTO _rule_segment_id;

IF NOT EXISTS (SELECT * FROM core.rule_segment WHERE name= 'mixed_purpose_code') THEN
INSERT INTO core.rule_segment (id,"name",requires_input,formula,data_type,tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (_rule_segment_id,'mixed_purpose_code',false,'YY','TEXT',1,'now()',1,false);

PERFORM setval('"core".rule_segment_id_seq', (SELECT MAX(id) FROM "core".rule_segment));
ELSE 
	SELECT id FROM core.rule_segment WHERE name= 'mixed_purpose_code' INTO _rule_segment_id;
END if;

/* update skip rules for program*/
update core.sequence_rule_segment set "skip" = '$input.split(",").length > 1'
where sequence_rule_id = 2 /*batch rule*/
  and rule_segment_id = 3; /*program*/

INSERT INTO core.sequence_rule_segment ("position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creator_id,is_void,is_displayed,is_required, "skip",skip_input_rule_segment_id)
select "position",segment_family,(select id from core.rule_segment where name='service-provider-code'),sequence_rule_id,tenant_id,creator_id,is_void,is_displayed,is_required, '$input.split(",").length <= 1',rule_segment_id
  from core.sequence_rule_segment
  where sequence_rule_id = 2 /*batch rule*/
    and rule_segment_id = 3 /*program*/
    and not exists (select * from core.sequence_rule_segment srs where srs.sequence_rule_id=2 and srs.rule_segment_id = (select id from core.rule_segment where name='service-provider-code'));

PERFORM setval('"core".sequence_rule_segment_id_seq', (SELECT MAX(id) FROM "core".sequence_rule_segment));

/* update skip rules for purpose*/
update core.sequence_rule_segment set "skip" = '$input.split(",").length > 1'
where sequence_rule_id = 2 /*batch rule*/
  and rule_segment_id = 6; /*purpose*/
  
INSERT INTO core.sequence_rule_segment ("position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creator_id,is_void,is_displayed,is_required, "skip",skip_input_rule_segment_id)
select "position",segment_family,_rule_segment_id,sequence_rule_id,tenant_id,creator_id,is_void,is_displayed,is_required, '$input.split(",").length <= 1',rule_segment_id
  from core.sequence_rule_segment
  where sequence_rule_id = 2 /*batch rule*/
    and rule_segment_id = 6 /*purpose*/
    and not exists (select * from core.sequence_rule_segment srs where srs.sequence_rule_id=2 and srs.rule_segment_id = _rule_segment_id);

PERFORM setval('"core".sequence_rule_segment_id_seq', (SELECT MAX(id) FROM "core".sequence_rule_segment));   
   
END $$