--liquibase formatted sql

--changeset postgres:create_invoice_rule context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3113 Create Rule for Invoice Codes

DO $$
DECLARE
 _seq_rule_id int;
 _segment_seq_id int;
 _rule_segment_id int;
BEGIN
	SELECT max(id)+1 FROM core.sequence_rule INTO _seq_rule_id; 
	INSERT INTO core.sequence_rule (id, "name",tenant_id,creator_id,is_void)
	VALUES (_seq_rule_id,'cs-invoice-number',1,1,false);


	SELECT max(id)+1 FROM core.segment_sequence INTO _segment_seq_id; 
	INSERT INTO core.segment_sequence (id,lowest,"increment","last",tenant_id,creation_timestamp,creator_id,is_void)
	VALUES (_segment_seq_id,1,1,0,1,'now()',1,false);

	SELECT max(id)+1 FROM core.rule_segment INTO _rule_segment_id;
	INSERT INTO core.rule_segment (id,"name",requires_input,data_type,segment_sequence_id,tenant_id,creator_id,is_void)
	VALUES (_rule_segment_id, 'invoice-sequence',false,'NUMBER',_segment_seq_id,1,1,false);

	INSERT INTO core.sequence_rule_segment ("position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creation_timestamp,creator_id,is_void,is_displayed,is_required)
	VALUES (1,0,_rule_segment_id,_seq_rule_id,1,now(),1,false,true,false);

	perform setval('core.sequence_rule_id_seq', (SELECT MAX(id) FROM core.sequence_rule));
	perform setval('core.segment_sequence_id_seq', (SELECT MAX(id) FROM core.segment_sequence));
	perform setval('core.rule_segment_id_seq', (SELECT MAX(id) FROM core.rule_segment));
END $$