--liquibase formatted sql

--changeset postgres:cross_manager_actions_update context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2200 Cross Manager actions

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_tm_role_id int;
	_cm_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'CB Admin' into _cb_admin_role_id;
	select id from security.role where name = 'Team Member' into _tm_role_id;
	select id from core.product where name = 'Cross Manager' into _cm_p_id;
	-- CROSS_MANAGER_CREATE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Create cross list and other related records', true, 'CROSS_MANAGER_CREATE', 1, _cm_p_id, true
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_CREATE' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_CREATE' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- CROSS_MANAGER_DELETE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Delete cross list and other related records', true, 'CROSS_MANAGER_DELETE', 1, _cm_p_id, true
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_DELETE' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_DELETE' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- CROSS_MANAGER_HM_CROSSES product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Harvest crosses', true, 'CROSS_MANAGER_HM_CROSSES', 1, _cm_p_id, true
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_HM_CROSSES' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_HM_CROSSES' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- CROSS_MANAGER_UPDATE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Edit cross list information', true, 'CROSS_MANAGER_UPDATE', 1, _cm_p_id, true
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_UPDATE' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_UPDATE' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- CROSS_MANAGER_UPDATE_PROTOCOLS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Update cross list protocol data', true, 'CROSS_MANAGER_UPDATE_PROTOCOLS', 1, _cm_p_id, true
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_UPDATE_PROTOCOLS' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_UPDATE_PROTOCOLS' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- CROSS_MANAGER_UPLOAD_CROSS_DATA product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Upload cross data', true, 'CROSS_MANAGER_UPLOAD_CROSS_DATA', 1, _cm_p_id, true
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_UPLOAD_CROSS_DATA' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_UPLOAD_CROSS_DATA' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- CROSS_MANAGER_DOWNLOAD_CROSS_DC_FILE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Download file for cross data collection', true, 'CROSS_MANAGER_DOWNLOAD_CROSS_DC_FILE', 1, _cm_p_id, false
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_DOWNLOAD_CROSS_DC_FILE' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_DOWNLOAD_CROSS_DC_FILE' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- CROSS_MANAGER_DOWNLOAD_FILES product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Download cross list', true, 'CROSS_MANAGER_DOWNLOAD_FILES', 1, _cm_p_id, false
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_DOWNLOAD_FILES' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_DOWNLOAD_FILES' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- CROSS_MANAGER_PRINTOUTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Printouts entry point', true, 'CROSS_MANAGER_PRINTOUTS', 1, _cm_p_id, false
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_PRINTOUTS' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_PRINTOUTS' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- CROSS_MANAGER_VIEW product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View basic info and crosses', true, 'CROSS_MANAGER_VIEW', 1, _cm_p_id, false
	where not exists (select id from security.product_function where action = 'CROSS_MANAGER_VIEW' and product_id = _cm_p_id);
	select id from security.product_function where action = 'CROSS_MANAGER_VIEW' and product_id = _cm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
end $$;