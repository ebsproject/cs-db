--liquibase formatted sql

--changeset postgres:custom_field_definition_update_with_label_properties_in_designer context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2457 New from occurrences -  As a user with permissions, I should be able to proceed to the basic information tab when done selecting shipment option -Create one shipment.


UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 15,
  "rules": {
    "required": "The recipient name is required."
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "entity": "Contact",
  "helper": {
    "title": "Recipient Email",
    "placement": "top"
  },
  "labels": [
    "id",
    "person.fullNameWithCoopkey"
  ],
  "columns": [
    {
      "name": "Recipient Name",
      "accessor": "recipient.person.givenName"
    },
    {
      "name": "Recipient Last Name",
      "accessor": "recipient.person.familyName"
    },
    {
      "name": "Recipient Email",
      "accessor": "recipient.email"
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "person.givenName",
    "person.familyName",
    "person.fullNameWithCoopkey"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Recipient Name",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "parentControl": "control name"
  }
}'::jsonb
  WHERE id=542;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 11,
  "rules": {
    "required": "The requestor name is required"
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "entity": "Contact",
  "helper": {
    "title": "Requestor Name",
    "placement": "top"
  },
  "labels": [
    "id",
    "person.fullName"
  ],
  "columns": [
    {
      "name": "Requestor Name",
      "accessor": "requestor.person.givenName"
    },
    {
      "name": "Requestor Last Name",
      "accessor": "requestor.person.familyName"
    },
    {
      "name": "Requestor Email",
      "accessor": "requestor.email"
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "person.givenName",
    "person.familyName",
    "person.fullName",
    "person.fullNameWithCoopkey"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Requestor Name",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "parentControl": "control name",
    "showControlRules": {
      "expectedValue": "false",
      "parentControl": "byOccurrence"
    }
  }
}'::jsonb
  WHERE id=538;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 14,
  "rules": {
    "required": "This field is required."
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "entity": "Contact",
  "helper": {
    "title": "Requestor Name",
    "placement": "top"
  },
  "labels": [
    "id",
    "person.fullName"
  ],
  "columns": [
    {
      "name": "Requestor Name",
      "accessor": "requestor.person.givenName"
    },
    {
      "name": "Requestor Last Name",
      "accessor": "requestor.person.familyName"
    },
    {
      "name": "Requestor Email",
      "accessor": "requestor.email"
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "person.familyName",
    "person.givenName",
    "person.fullName"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Requestor Name",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "customFilters": [],
    "parentControl": "control name"
  }
}'::jsonb
  WHERE id=796;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 1,
  "rules": {
    "required": "The program is required."
  },
  "sizes": [
    12,
    12,
    6,
    4,
    4
  ],
  "entity": "Program",
  "helper": {
    "title": "Program",
    "placement": "top"
  },
  "labels": [
    "name"
  ],
  "columns": [
    {
      "name": "Program",
      "accessor": "program.name"
    }
  ],
  "filters": [],
  "apiContent": [
    "id",
    "name",
    "code"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Program",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "code",
      "name"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "parentControl": "control name"
  }
}'::jsonb
  WHERE id=496;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 7,
  "rules": {
    "required": "The sender Name is required."
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "entity": "Contact",
  "helper": {
    "title": "Sender Name",
    "placement": "top"
  },
  "labels": [
    "id",
    "person.fullName"
  ],
  "columns": [
    {
      "name": "Sender Name",
      "accessor": "sender.person.givenName"
    },
    {
      "name": "Sender Last Name",
      "accessor": "sender.person.familyName"
    },
    {
      "name": "Sender Email",
      "accessor": "sender.email"
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "person.givenName",
    "person.familyName",
    "email",
    "id",
    "person.fullName"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Sender Name",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "parentControl": "control name"
  }
}'::jsonb
  WHERE id=534;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 19,
  "rules": {
    "required": "The recipient type is required."
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "entity": "ContactType",
  "helper": {
    "title": "Recipient Type",
    "placement": "top"
  },
  "labels": [
    "name"
  ],
  "columns": [],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Recipient Type",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "parentControl": "control name"
  }
}'::jsonb
  WHERE id=546;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 1,
  "rules": {
    "required": "The program is required."
  },
  "sizes": [
    12,
    12,
    6,
    4,
    4
  ],
  "entity": "Program",
  "helper": {
    "title": "Program",
    "placement": "top"
  },
  "labels": [
    "name"
  ],
  "columns": [],
  "filters": [],
  "apiContent": [
    "id",
    "name",
    "code"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Program",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "customFilters": [],
    "parentControl": "control name"
  }
}'::jsonb
  WHERE id=760;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 8,
  "rules": {
    "required": "The sender name is required."
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "entity": "Contact",
  "helper": {
    "title": "Sender Name",
    "placement": "top"
  },
  "labels": [
    "id",
    "person.fullName"
  ],
  "columns": [
    {
      "name": "Sender Name",
      "accessor": "sender.person.givenName"
    },
    {
      "name": "Sender Last Name",
      "accessor": "sender.person.familyName"
    },
    {
      "name": "Sender Email",
      "accessor": "sender.email"
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "person.familyName",
    "person.givenName",
    "person.fullName"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Sender Name",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "customFilters": [],
    "parentControl": "control name"
  }
}'::jsonb
  WHERE id=769;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 19,
  "rules": {
    "required": "This field is required."
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "entity": "Contact",
  "helper": {
    "title": "Recipient Name",
    "placement": "top"
  },
  "labels": [
    "id",
    "person.fullName"
  ],
  "columns": [
    {
      "name": "Recipient Name",
      "accessor": "recipient.person.givenName"
    },
    {
      "name": "Recipient Last Name",
      "accessor": "recipient.person.familyName"
    },
    {
      "name": "Recipient Email",
      "accessor": "recipient.email"
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "person.givenName",
    "person.familyName",
    "person.fullName"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Recipient Name",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "customFilters": [],
    "parentControl": "control name"
  }
}'::jsonb
  WHERE id=800;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 23,
  "rules": {
    "required": "This field is required."
  },
  "sizes": [
    12,
    12,
    6,
    6,
    6
  ],
  "entity": "ContactType",
  "helper": {
    "title": "Recipient Type",
    "placement": "top"
  },
  "labels": [
    "name"
  ],
  "columns": [
    {
      "accessor": ""
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Recipient Type",
    "variant": "outlined"
  },
  "showInGrid": false,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "customFilters": [],
    "parentControl": "control name"
  }
}'::jsonb
  WHERE id=804;