--liquibase formatted sql

--changeset postgres:data_collection_actions_update context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2200 Data Collection actions

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_tm_role_id int;
	_dc_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'CB Admin' into _cb_admin_role_id;
	select id from security.role where name = 'Team Member' into _tm_role_id;
	select id from core.product where name = 'Data Collection' into _dc_p_id;
	-- DATA_COLLECTION_INPUT_POST_HARVEST_DATA product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Input Post Harvest information', true, 'DATA_COLLECTION_INPUT_POST_HARVEST_DATA', 1, _dc_p_id, true
	where not exists (select id from security.product_function where action = 'DATA_COLLECTION_INPUT_POST_HARVEST_DATA' and product_id = _dc_p_id);
	select id from security.product_function where action = 'DATA_COLLECTION_INPUT_POST_HARVEST_DATA' and product_id = _dc_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
end $$;