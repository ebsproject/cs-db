--liquibase formatted sql

--changeset postgres:definition_update_in_occurrencecsv_custom_field context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2457 New from occurrences -  As a user with permissions, I should be able to proceed to the basic information tab when done selecting shipment option -Create one shipment.

UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 8,
  "name": "occurrencesCSVFile",
  "sort": 1.23,
  "label": "Validate",
  "rules": {
    "required": "The csv file is required"
  },
  "sizes": [
    12,
    12,
    12,
    12,
    12
  ],
  "helper": {
    "title": "Attach a CSV File",
    "placement": "top"
  },
  "disabled": false,
  "showInGrid": false,
  "customProps": {
    "size": "large",
    "color": "primary",
    "label": "Upload a CSV File",
    "classes": "",
    "disabled": false,
    "fullWidth": false,
    "maxFileSize": 5000000,
    "showPreviews": true,
    "acceptedFiles": [
      ".txt",
      ".csv"
    ],
    "cancelButtonText": "Cancel",
    "submitButtonText": "OK",
    "showFileNamesInPreview": true
  },
  "defaultRules": {
    "showIfValue": "true",
    "parentControl": "byOccurrence"
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=1706;
