--liquibase formatted sql

--changeset postgres:experiment_creation_actions_update context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2200 Experiment Creation actions

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_tm_role_id int;
	_ec_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'CB Admin' into _cb_admin_role_id;
	select id from security.role where name = 'Team Member' into _tm_role_id;
	select id from core.product where name = 'Experiment Creation' into _ec_p_id;
	-- EXPERIMENT_CREATION_COPY_EXPERIMENT product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Copy data of a created experiment', true, 'EXPERIMENT_CREATION_COPY_EXPERIMENT', 1, _ec_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_CREATION_COPY_EXPERIMENT' and product_id = _ec_p_id);
	select id from security.product_function where action = 'EXPERIMENT_CREATION_COPY_EXPERIMENT' and product_id = _ec_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_CREATION_CREATE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Create experiment and other related records', true, 'EXPERIMENT_CREATION_CREATE', 1, _ec_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_CREATION_CREATE' and product_id = _ec_p_id);
	select id from security.product_function where action = 'EXPERIMENT_CREATION_CREATE' and product_id = _ec_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_CREATION_DELETE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Delete experiment and other related records', true, 'EXPERIMENT_CREATION_DELETE', 1, _ec_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_CREATION_DELETE' and product_id = _ec_p_id);
	select id from security.product_function where action = 'EXPERIMENT_CREATION_DELETE' and product_id = _ec_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_CREATION_UPDATE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Edit data within experiment creation workflow', true, 'EXPERIMENT_CREATION_UPDATE', 1, _ec_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_CREATION_UPDATE' and product_id = _ec_p_id);
	select id from security.product_function where action = 'EXPERIMENT_CREATION_UPDATE' and product_id = _ec_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_CREATION_VIEW product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View basic, entry list, and occurrence records', true, 'EXPERIMENT_CREATION_VIEW', 1, _ec_p_id, false
	where not exists (select id from security.product_function where action = 'EXPERIMENT_CREATION_VIEW' and product_id = _ec_p_id);
	select id from security.product_function where action = 'EXPERIMENT_CREATION_VIEW' and product_id = _ec_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
end $$;