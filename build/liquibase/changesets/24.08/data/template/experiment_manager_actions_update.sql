--liquibase formatted sql

--changeset postgres:experiment_manager_actions_update context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2200 Experiment Manager actions

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_tm_role_id int;
	_collaborator_role_id int;
	_em_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'CB Admin' into _cb_admin_role_id;
	select id from security.role where name = 'Team Member' into _tm_role_id;
	select id from security.role where name = 'Collaborator' into _collaborator_role_id;
	select id from core.product where name = 'Experiment Manager' into _em_p_id;
	-- EXPERIMENT_MANAGER_DELETE_OCCURRENCE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Delete occurrence', true, 'EXPERIMENT_MANAGER_DELETE_OCCURRENCE', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_DELETE_OCCURRENCE' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_DELETE_OCCURRENCE' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_GEN_ENTCODE_EXPERIMENT product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Generate Entry code - Experiment level', true, 'EXPERIMENT_MANAGER_GEN_ENTCODE_EXPERIMENT', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_GEN_ENTCODE_EXPERIMENT' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_GEN_ENTCODE_EXPERIMENT' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_GEN_ENTCODE_OCCURRENCE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Generate Entry code - Occurrence level', true, 'EXPERIMENT_MANAGER_GEN_ENTCODE_OCCURRENCE', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_GEN_ENTCODE_OCCURRENCE' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_GEN_ENTCODE_OCCURRENCE' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_GEN_PLOTCODE_EXPERIMENT product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Generate Plot code - Experiment level', true, 'EXPERIMENT_MANAGER_GEN_PLOTCODE_EXPERIMENT', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_GEN_PLOTCODE_EXPERIMENT' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_GEN_PLOTCODE_EXPERIMENT' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_GEN_PLOTCODE_OCCURRENCE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Generate Plot code - Occurrence level', true, 'EXPERIMENT_MANAGER_GEN_PLOTCODE_OCCURRENCE', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_GEN_PLOTCODE_OCCURRENCE' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_GEN_PLOTCODE_OCCURRENCE' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_HM_CROSSES product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Access Breeding Tools - PIM - Harvest crosses', true, 'EXPERIMENT_MANAGER_HM_CROSSES', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_HM_CROSSES' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_HM_CROSSES' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_HM_PLOTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Access Breeding Tools - PIM - Harvest plots', true, 'EXPERIMENT_MANAGER_HM_PLOTS', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_HM_PLOTS' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_HM_PLOTS' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_MANAGE_OCC_PERMISSION product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Manage experiment permissions (only creator of the record or the experiment steward)', true, 'EXPERIMENT_MANAGER_MANAGE_OCC_PERMISSION', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_MANAGE_OCC_PERMISSION' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_MANAGE_OCC_PERMISSION' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_PIM_CREATE_PACKING_JOB product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Access Breeding Tools - PIM - Create a packing job', true, 'EXPERIMENT_MANAGER_PIM_CREATE_PACKING_JOB', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_PIM_CREATE_PACKING_JOB' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_PIM_CREATE_PACKING_JOB' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_REGENERATE_DESIGN product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Regenerate occurrence design', true, 'EXPERIMENT_MANAGER_REGENERATE_DESIGN', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_REGENERATE_DESIGN' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_REGENERATE_DESIGN' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_UPDATE_OCC_PROTOCOL product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Update occurrence protocols', true, 'EXPERIMENT_MANAGER_UPDATE_OCC_PROTOCOL', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_UPDATE_OCC_PROTOCOL' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_UPDATE_OCC_PROTOCOL' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_DOWNLOAD_FILES product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Download files', true, 'EXPERIMENT_MANAGER_DOWNLOAD_FILES', 1, _em_p_id, false
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_DOWNLOAD_FILES' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_DOWNLOAD_FILES' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_collaborator_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _collaborator_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_DOWNLOAD_FILES_FORMAT product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Download files in CSV or JSON format', true, 'EXPERIMENT_MANAGER_DOWNLOAD_FILES_FORMAT', 1, _em_p_id, false
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_DOWNLOAD_FILES_FORMAT' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_DOWNLOAD_FILES_FORMAT' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_collaborator_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _collaborator_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_PRINTOUTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Printouts', true, 'EXPERIMENT_MANAGER_PRINTOUTS', 1, _em_p_id, false
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_PRINTOUTS' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_PRINTOUTS' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_collaborator_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _collaborator_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_UPLOAD_MAPPED_PLOTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Upload - Mapped plots', true, 'EXPERIMENT_MANAGER_UPLOAD_MAPPED_PLOTS', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_UPLOAD_MAPPED_PLOTS' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_UPLOAD_MAPPED_PLOTS' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_collaborator_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _collaborator_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_UPLOAD_TRAIT_DATA product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Upload - Trait data collection', true, 'EXPERIMENT_MANAGER_UPLOAD_TRAIT_DATA', 1, _em_p_id, true
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_UPLOAD_TRAIT_DATA' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_UPLOAD_TRAIT_DATA' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_collaborator_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _collaborator_role_id and product_function_id = _pf_id);
	-- EXPERIMENT_MANAGER_VIEW_OCCURRENCE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View occurrence', true, 'EXPERIMENT_MANAGER_VIEW_OCCURRENCE', 1, _em_p_id, false
	where not exists (select id from security.product_function where action = 'EXPERIMENT_MANAGER_VIEW_OCCURRENCE' and product_id = _em_p_id);
	select id from security.product_function where action = 'EXPERIMENT_MANAGER_VIEW_OCCURRENCE' and product_id = _em_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_collaborator_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _collaborator_role_id and product_function_id = _pf_id);
end $$;