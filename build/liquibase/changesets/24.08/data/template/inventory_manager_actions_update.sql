--liquibase formatted sql

--changeset postgres:inventory_manager_actions_update context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2200 Inentory Manager actions

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_tm_role_id int;
	_seed_manager_role_id int;
	_im_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'CB Admin' into _cb_admin_role_id;
	select id from security.role where name = 'Team Member' into _tm_role_id;
	select id from security.role where name = 'Seed Manager' into _seed_manager_role_id;
	select id from core.product where name = 'Inventory manager' into _im_p_id;
	-- VIEW_CREATE_TAB product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Provide access to the create tab', true, 'VIEW_CREATE_TAB', 1, _im_p_id, false
	where not exists (select id from security.product_function where action = 'VIEW_CREATE_TAB' and product_id = _im_p_id);
	select id from security.product_function where action = 'VIEW_CREATE_TAB' and product_id = _im_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_seed_manager_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _seed_manager_role_id and product_function_id = _pf_id);
	-- VIEW_UPDATE_TAB product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Provide access to the update tab', true, 'VIEW_UPDATE_TAB', 1, _im_p_id, false
	where not exists (select id from security.product_function where action = 'VIEW_UPDATE_TAB' and product_id = _im_p_id);
	select id from security.product_function where action = 'VIEW_UPDATE_TAB' and product_id = _im_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_seed_manager_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _seed_manager_role_id and product_function_id = _pf_id);
	-- VIEW_SEED_TRANSFER_TAB product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Provide access to the seed transfer tab', true, 'VIEW_SEED_TRANSFER_TAB', 1, _im_p_id, true
	where not exists (select id from security.product_function where action = 'VIEW_SEED_TRANSFER_TAB' and product_id = _im_p_id);
	select id from security.product_function where action = 'VIEW_SEED_TRANSFER_TAB' and product_id = _im_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_seed_manager_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _seed_manager_role_id and product_function_id = _pf_id);
	-- CREATE_SEED_AND_PACKAGE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Allow seed and package creation', true, 'CREATE_SEED_AND_PACKAGE', 1, _im_p_id, true
	where not exists (select id from security.product_function where action = 'CREATE_SEED_AND_PACKAGE' and product_id = _im_p_id);
	select id from security.product_function where action = 'CREATE_SEED_AND_PACKAGE' and product_id = _im_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_seed_manager_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _seed_manager_role_id and product_function_id = _pf_id);
	-- CREATE_PACKAGE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Allow package creation', true, 'CREATE_PACKAGE', 1, _im_p_id, true
	where not exists (select id from security.product_function where action = 'CREATE_PACKAGE' and product_id = _im_p_id);
	select id from security.product_function where action = 'CREATE_PACKAGE' and product_id = _im_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_seed_manager_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _seed_manager_role_id and product_function_id = _pf_id);
	-- UPDATE_SEED product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Allow seed update', true, 'UPDATE_SEED', 1, _im_p_id, true
	where not exists (select id from security.product_function where action = 'UPDATE_SEED' and product_id = _im_p_id);
	select id from security.product_function where action = 'UPDATE_SEED' and product_id = _im_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_seed_manager_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _seed_manager_role_id and product_function_id = _pf_id);
	-- UPDATE_PACKAGE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Allows package update', true, 'UPDATE_PACKAGE', 1, _im_p_id, true
	where not exists (select id from security.product_function where action = 'UPDATE_PACKAGE' and product_id = _im_p_id);
	select id from security.product_function where action = 'UPDATE_PACKAGE' and product_id = _im_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_seed_manager_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _seed_manager_role_id and product_function_id = _pf_id);
	-- TRANSFER_SEED product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Allow internal seed transfer', true, 'TRANSFER_SEED', 1, _im_p_id, true
	where not exists (select id from security.product_function where action = 'TRANSFER_SEED' and product_id = _im_p_id);
	select id from security.product_function where action = 'TRANSFER_SEED' and product_id = _im_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_seed_manager_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _seed_manager_role_id and product_function_id = _pf_id);
	-- delete INVENTORY_MANAGER_CREATE
	select id from security.product_function where action = 'INVENTORY_MANAGER_CREATE' into _pf_id;
	delete from security.role_product_function where product_function_id = _pf_id;
	update security.product_function set is_void = true where action = 'INVENTORY_MANAGER_CREATE';
	-- delete INVENTORY_MANAGER_UPDATE
	select id from security.product_function where action = 'INVENTORY_MANAGER_UPDATE' into _pf_id;
	delete from security.role_product_function where product_function_id = _pf_id;
	update security.product_function set is_void = true where action = 'INVENTORY_MANAGER_UPDATE';
	-- delete INVENTORY_MANAGER_SEED_TRANSFER
	select id from security.product_function where action = 'INVENTORY_MANAGER_SEED_TRANSFER' into _pf_id;
	delete from security.role_product_function where product_function_id = _pf_id;
	update security.product_function set is_void = true where action = 'INVENTORY_MANAGER_SEED_TRANSFER';
end $$;