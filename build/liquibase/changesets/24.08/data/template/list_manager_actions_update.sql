--liquibase formatted sql

--changeset postgres:list_manager_actions_update context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2200 List manager actions

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_tm_role_id int;
	_lm_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'CB Admin' into _cb_admin_role_id;
	select id from security.role where name = 'Team Member' into _tm_role_id;
	select id from core.product where name = 'List manager' into _lm_p_id;
	-- LIST_MANAGER_UPDATE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Edit list information and data', true, 'LIST_MANAGER_UPDATE', 1, _lm_p_id, true
	where not exists (select id from security.product_function where action = 'LIST_MANAGER_UPDATE' and product_id = _lm_p_id);
	select id from security.product_function where action = 'LIST_MANAGER_UPDATE' and product_id = _lm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- LIST_MANAGER_CREATE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Create a list', true, 'LIST_MANAGER_CREATE', 1, _lm_p_id, true
	where not exists (select id from security.product_function where action = 'LIST_MANAGER_CREATE' and product_id = _lm_p_id);
	select id from security.product_function where action = 'LIST_MANAGER_CREATE' and product_id = _lm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- LIST_MANAGER_MERGE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Merge list', true, 'LIST_MANAGER_MERGE', 1, _lm_p_id, true
	where not exists (select id from security.product_function where action = 'LIST_MANAGER_MERGE' and product_id = _lm_p_id);
	select id from security.product_function where action = 'LIST_MANAGER_MERGE' and product_id = _lm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- LIST_MANAGER_SPLIT product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Split list', true, 'LIST_MANAGER_SPLIT', 1, _lm_p_id, true
	where not exists (select id from security.product_function where action = 'LIST_MANAGER_SPLIT' and product_id = _lm_p_id);
	select id from security.product_function where action = 'LIST_MANAGER_SPLIT' and product_id = _lm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- LIST_MANAGER_DELETE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Delete list', true, 'LIST_MANAGER_DELETE', 1, _lm_p_id, true
	where not exists (select id from security.product_function where action = 'LIST_MANAGER_DELETE' and product_id = _lm_p_id);
	select id from security.product_function where action = 'LIST_MANAGER_DELETE' and product_id = _lm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- LIST_MANAGER_ADD_ITEMS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Add items to list', true, 'LIST_MANAGER_ADD_ITEMS', 1, _lm_p_id, true
	where not exists (select id from security.product_function where action = 'LIST_MANAGER_ADD_ITEMS' and product_id = _lm_p_id);
	select id from security.product_function where action = 'LIST_MANAGER_ADD_ITEMS' and product_id = _lm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- LIST_MANAGER_SHARE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Share list', true, 'LIST_MANAGER_SHARE', 1, _lm_p_id, true
	where not exists (select id from security.product_function where action = 'LIST_MANAGER_SHARE' and product_id = _lm_p_id);
	select id from security.product_function where action = 'LIST_MANAGER_SHARE' and product_id = _lm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- LIST_MANAGER_VIEW product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View list information', true, 'LIST_MANAGER_VIEW', 1, _lm_p_id, false
	where not exists (select id from security.product_function where action = 'LIST_MANAGER_VIEW' and product_id = _lm_p_id);
	select id from security.product_function where action = 'LIST_MANAGER_VIEW' and product_id = _lm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	-- LIST_MANAGER_DOWNLOAD product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Download containing list', true, 'LIST_MANAGER_DOWNLOAD', 1, _lm_p_id, false
	where not exists (select id from security.product_function where action = 'LIST_MANAGER_DOWNLOAD' and product_id = _lm_p_id);
	select id from security.product_function where action = 'LIST_MANAGER_DOWNLOAD' and product_id = _lm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_cb_admin_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id)
	select 
		_tm_role_id, _pf_id
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
end $$;