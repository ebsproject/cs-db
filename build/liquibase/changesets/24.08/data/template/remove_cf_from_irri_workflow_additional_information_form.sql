--liquibase formatted sql

--changeset postgres:remove_cf_from_irri_workflow_additional_information_form context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2910 Shipment Outgoing IRRI: Remove some fields in the Delivery and shipping information section

UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=906;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1126;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1129;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1130;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1131;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1132;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1133;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1135;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=907;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=913;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1236;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1238;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1128;
