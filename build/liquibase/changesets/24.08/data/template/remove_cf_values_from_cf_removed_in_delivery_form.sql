--liquibase formatted sql

--changeset postgres:remove_cf_values_from_cf_removed_in_delivery_form context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2910 Shipment Outgoing IRRI: Remove some fields in the Delivery and shipping information section

update workflow.cf_value set is_void = true 
where nodecf_id in (906,1126,1129,1130,1131,1132,1133,1135,907,913,1236,1238,1128,871);