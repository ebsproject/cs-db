--liquibase formatted sql

--changeset postgres:remove_dangerous_field_from_cf_in_general_information_form context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2520 Shipment Outgoing IRRI: Remove all dangerous fields and checkbox

UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=871;

UPDATE workflow.cf_value
	SET is_void=true
	WHERE nodecf_id=871;
