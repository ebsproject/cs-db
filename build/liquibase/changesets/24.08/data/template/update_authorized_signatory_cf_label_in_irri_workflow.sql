--liquibase formatted sql

--changeset postgres:update_authorized_signatory_cf_label_in_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2525 Do not prefilled the dates upon clicking APPROVE action

UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "authorizedSignatory",
  "sort": 104,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    6,
    6,
    6
  ],
  "helper": {
    "title": "IRRI Authorized Signatory",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "IRRI Authorized Signatory",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=914;
