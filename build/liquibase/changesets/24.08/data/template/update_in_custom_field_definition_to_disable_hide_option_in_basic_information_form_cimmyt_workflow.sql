--liquibase formatted sql

--changeset postgres:update_in_custom_field_definition_to_disable_hide_option_in_basic_information_form_cimmyt_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2457 New from occurrences -  As a user with permissions, I should be able to proceed to the basic information tab when done selecting shipment option -Create one shipment.


UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 5,
  "sort": 11,
  "rules": {
    "required": "The requestor name is required"
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "entity": "Contact",
  "helper": {
    "title": "Requestor Name",
    "placement": "top"
  },
  "labels": [
    "id",
    "person.fullName"
  ],
  "columns": [
    {
      "name": "Requestor Name",
      "accessor": "requestor.person.givenName"
    },
    {
      "name": "Requestor Last Name",
      "accessor": "requestor.person.familyName"
    },
    {
      "name": "Requestor Email",
      "accessor": "requestor.email"
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "person.givenName",
    "person.familyName",
    "person.fullName",
    "person.fullNameWithCoopkey"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Requestor Name",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "parentControl": "control name"
  }
}'::jsonb
	WHERE id=538;
UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "",
  "sort": 26,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    12,
    12,
    12
  ],
  "helper": {
    "title": "",
    "placement": "top"
  },
  "inputProps": {
    "rows": 2,
    "label": "REQUESTOR ADDITIONAL INFORMATION",
    "variant": "standard",
    "disabled": true,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": false,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=698;
UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 5,
  "sort": 13,
  "rules": {
    "required": "The requestor address is required"
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "entity": "",
  "helper": {
    "title": "Requestor Address",
    "placement": "top"
  },
  "columns": [
    {
      "accessor": " "
    }
  ],
  "filters": [],
  "apiContent": [],
  "inputProps": {
    "color": "primary",
    "label": "Requestor Address",
    "variant": "outlined"
  },
  "modalPopup": {
    "show": true,
    "componentUI": "AddressForm",
    "parentControl": "requestorId"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "addresses",
    "label": [
      "streetAddress",
      "zipCode",
      "country.name",
      "location"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "addresses.id"
      },
      {
        "accessor": "addresses.streetAddress"
      },
      {
        "accessor": "addresses.country.name"
      },
      {
        "accessor": "addresses.zipCode"
      },
      {
        "accessor": "addresses.location"
      }
    ],
    "applyRules": true,
    "columnFilter": "id",
    "parentControl": "requestorId"
  }
}'::jsonb
	WHERE id=540;
UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "",
  "sort": 28,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    4,
    4,
    4
  ],
  "helper": {
    "title": "Requestor Department",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Requestor Department",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=700;
UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "requestorEmail",
  "sort": 12,
  "rules": {
    "required": "The requestor email is required."
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "helper": {
    "title": "Requestor Email",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Requestor Email",
    "variant": "outlined",
    "disabled": true,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "email",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "email"
      }
    ],
    "applyRules": true,
    "columnFilter": "id",
    "parentControl": "requestorId",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    },
    "dependencyControl": "byOccurrence"
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=539;
UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "requestorInstitution",
  "sort": 14,
  "rules": {
    "required": "The requestor insitution is required."
  },
  "sizes": [
    12,
    12,
    6,
    3,
    3
  ],
  "helper": {
    "title": "Requestor Institution",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Requestor Institution",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "institution.legalName",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "institution.legalName"
      }
    ],
    "applyRules": true,
    "columnFilter": "addresses.id",
    "customFilters": [
      {
        "col": "category.name",
        "mod": "EQ",
        "val": "Institution"
      }
    ],
    "parentControl": "requestorAddress",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=541;
UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "",
  "sort": 27,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    4,
    4,
    4
  ],
  "helper": {
    "title": "Requestor Phone Number",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Requestor Phone Number",
    "variant": "outlined",
    "disabled": true,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "value",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "ContactInfo",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "value"
      }
    ],
    "applyRules": true,
    "columnFilter": "contact.id",
    "customFilters": [
      {
        "col": "contactInfoType.name",
        "mod": "EQ",
        "val": "Phone number"
      }
    ],
    "parentControl": "requestorId",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=699;
UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "",
  "sort": 29,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    4,
    4,
    4
  ],
  "helper": {
    "title": "Requestor Position",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Requestor Position",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "person.jobTitle",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "person.jobTitle"
      }
    ],
    "applyRules": true,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "requestorId",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=727;
UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 6,
  "sort": 18.1,
  "rules": {
    "required": ""
  },
  "sizes": [
    12,
    12,
    12,
    12,
    12
  ],
  "helper": {
    "title": "Set Recipient same as requestor",
    "placement": "top"
  },
  "function": "setRecipientSameAsRequestor",
  "inputProps": {
    "color": "primary",
    "label": "Set Requestor Information to Recipient",
    "disabled": false
  },
  "showInGrid": false,
  "defaultValue": false
}'::jsonb
	WHERE id=1494;
