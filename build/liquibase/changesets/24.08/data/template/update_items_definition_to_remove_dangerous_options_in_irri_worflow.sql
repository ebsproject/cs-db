--liquibase formatted sql

--changeset postgres:update_items_definition_to_remove_dangerous_options_in_irri_worflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2520 Shipment Outgoing IRRI: Remove all dangerous fields and checkbox
UPDATE workflow.node
	SET define='{
  "id": 8,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": "All Ok."
    }
  },
  "rules": {
    "columns": [
      {
        "name": "testCode",
        "alias": "RSHT",
        "hidden": false
      },
      {
        "name": "referenceNumber",
        "hidden": true
      },
      {
        "name": "receivedDate",
        "hidden": true
      },
      {
        "name": "id",
        "hidden": true
      },
      {
        "name": "mlsAncestors",
        "hidden": true
      },
      {
        "name": "status",
        "hidden": true
      },
      {
        "name": "isDangerous",
        "hidden": true
      }
    ],
    "bulkOptions": [
      "Quantities",
      "Sync from CB"
    ]
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "component": "Items",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21025;
