--liquibase formatted sql

--changeset postgres:update_node_done_definition_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2589 Create new template for Shipment outgoing IRRI (Standard Material Transfer Agreement-Signed document)
UPDATE workflow.node
	SET node_type_id=2,
     define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Done."
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "status": "452",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "sender",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "Requestor",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "Recipient",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "SHU",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": false
    }
  ]
}'::jsonb
	WHERE id=21110;

UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 1,
  "name": "invoiceNumber",
  "sort": 110,
  "rules": {
    "required": "The invoice number is required."
  },
  "sizes": [
    12,
    12,
    6,
    4,
    4
  ],
  "helper": {
    "title": "Invoice Number",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "Invoice Number",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false
  },
  "modalPopup": {
    "show": false,
    "componentUI": ""
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": true,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "cs-invoice-number",
      "applyRule": true
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=1124;
