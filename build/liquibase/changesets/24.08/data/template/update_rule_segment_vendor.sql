--liquibase formatted sql

--changeset postgres:update_format_sample_blank_prefix_rule_segment context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:CONTINUE onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM core.rule_segment WHERE format = 'ZZ_BLANK_') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-3041 Update rule_segment



UPDATE core.rule_segment
    SET format = 'ZZ_BLANK_'
    WHERE name = 'sample-blank-prefix';



-- revert changes
--rollback UPDATE core.rule_segment
--rollback    SET format = 'ZZ_Blank_'
--rollback    WHERE name = 'sample-blank-prefix'
--rollback ;


--liquibase formatted sql

--changeset postgres:update_data_type_sample_and_plate_sequence_prefix_rule_segment context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:CONTINUE onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT * FROM core.rule_segment WHERE data_type = 'NUMBER' AND (name = 'sample-sequence' OR name = 'plate-sequence')) WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-3041 Update rule_segment



UPDATE core.rule_segment
    SET data_type = 'NUMBER'
    WHERE name = 'sample-sequence';

UPDATE core.rule_segment
    SET data_type = 'NUMBER'
    WHERE name = 'plate-sequence';



-- revert changes
--rollback UPDATE core.rule_segment
--rollback    SET data_type = 'TEXT'
--rollback    WHERE name = 'sample-sequence'
--rollback ;
--rollback
--rollback UPDATE core.rule_segment
--rollback    SET data_type = 'TEXT'
--rollback    WHERE name = 'plate-sequence'
--rollback ;



--liquibase formatted sql

--changeset postgres:update_format_plate_sequence_rule_segment context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:CONTINUE onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM core.rule_segment WHERE format = '000' AND name = 'plate-sequence') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-3041 Update rule_segment



UPDATE core.rule_segment
    SET format = '000'
    WHERE name = 'plate-sequence';



-- revert changes
--rollback UPDATE core.rule_segment
--rollback    SET format = NULL
--rollback    WHERE name = 'plate-sequence'
--rollback ;