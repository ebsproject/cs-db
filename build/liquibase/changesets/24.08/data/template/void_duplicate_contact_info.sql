--liquibase formatted sql

--changeset postgres:void_duplicate_contact_info context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1412

do
$$
declare
	_ci record;
begin
	for _ci in (select id,value,info_type_id from crm.contact_info order by id) loop
		if ((select is_void from crm.contact_info where id = _ci.id)=false) then
			update crm.contact_info set is_void = true where lower(trim(value)) = lower(trim(_ci.value)) and info_type_id = _ci.info_type_id and id <> _ci.id;
		end if;
	end loop;
end
$$;