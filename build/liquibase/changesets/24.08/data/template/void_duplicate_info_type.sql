--liquibase formatted sql

--changeset postgres:void_duplicate_info_type context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1412

do
$$
declare
	_info_type_id int;
	_info_types record;
	_contact_infos record;
begin
	for _info_types in (select id,name from crm.info_type order by id) loop
		if ((select is_void from crm.info_type where id = _info_types.id)=false) then
			update crm.info_type set is_void = true where lower(trim(name)) = lower(trim(_info_types.name)) and id <> _info_types.id;
		else
			for _contact_infos in (select id from crm.contact_info where info_type_id = _info_types.id) loop
				update crm.contact_info set is_void = true where info_type_id = _info_types.id;
			end loop;	
		end if;
	end loop;
end
$$;