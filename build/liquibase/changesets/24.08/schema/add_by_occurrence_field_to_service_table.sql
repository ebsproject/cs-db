--liquibase formatted sql

--changeset postgres:add_by_occurrence_field_to_service_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2985 Database model for the new shipment occurrence workflow

ALTER TABLE workflow.service ADD by_occurrence bool DEFAULT false NOT NULL;