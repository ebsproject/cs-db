--liquibase formatted sql

--changeset postgres:add_default_format_column_printout_template context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2864 Add default_format field to core.printout_template table

ALTER TABLE core.printout_template ADD default_format varchar DEFAULT 'pdf' NOT NULL;