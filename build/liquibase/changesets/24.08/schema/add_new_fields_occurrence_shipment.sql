--liquibase formatted sql

--changeset postgres:add_new_fields_occurrence_shipment context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2985 Add weight, test_code, status fields to workflow.occurrence_shipment

ALTER TABLE workflow.occurrence_shipment ADD weight int4 NULL;
ALTER TABLE workflow.occurrence_shipment ADD test_code varchar NULL;
ALTER TABLE workflow.occurrence_shipment ADD status varchar(50) NULL;