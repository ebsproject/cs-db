--liquibase formatted sql

--changeset postgres:add_skip_columns context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2987 Add support for conditional segments

ALTER TABLE core.sequence_rule_segment ADD "skip" varchar NULL;
COMMENT ON COLUMN core.sequence_rule_segment."skip" IS 'defines special conditions to discard or not this segment based on input value';

ALTER TABLE core.sequence_rule_segment ADD skip_input_rule_segment_id int4 NULL;
COMMENT ON COLUMN core.sequence_rule_segment.skip_input_rule_segment_id IS 'defines the rule segment''s input to provide to the skip function';

ALTER TABLE core.sequence_rule_segment ADD CONSTRAINT "FK_sequence_rule_segment_rule_segment2" FOREIGN KEY (skip_input_rule_segment_id) REFERENCES core.rule_segment(id);
