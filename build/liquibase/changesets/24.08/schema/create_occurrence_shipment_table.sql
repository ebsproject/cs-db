--liquibase formatted sql

--changeset postgres:create_occurrence_shipment_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2985 Database model for the new shipment occurrence workflow

CREATE TABLE workflow.occurrence_shipment (
	id serial4 NOT NULL,
	service_id int4 NOT NULL,
	occurrence_id int4 NOT NULL,
	occurrence_number int4 NOT NULL,
	occurrence_name varchar(128) NOT NULL,
	experiment_name varchar(128) NOT NULL,
	experiment_code varchar(64) NOT NULL,
	experiment_year int4 NOT NULL,
	experiment_season varchar(64) NOT NULL,
	coopkey varchar(64) NOT NULL,
	recipient_id int4 NULL,
    is_validated bool DEFAULT false NOT NULL,
	creation_timestamp timestamp DEFAULT now() NOT NULL,
	modification_timestamp timestamp NULL,
	creator_id int4 NOT NULL,
	modifier_id int4 NULL,
	is_void bool DEFAULT false NOT NULL,
	CONSTRAINT "PK_occurrence_shipment" PRIMARY KEY (id),
	CONSTRAINT "FK_occurrence_shipment_service" FOREIGN KEY (service_id) REFERENCES workflow.service(id),
	CONSTRAINT "FK_occurrence_shipment_recipient_contact" FOREIGN KEY (recipient_id) REFERENCES crm.contact(id)
);

ALTER TABLE workflow.items ADD occurrence_shipment_id int4 NULL;
ALTER TABLE workflow.items ADD CONSTRAINT "FK_items_occurrence_shipment" FOREIGN KEY (occurrence_shipment_id) REFERENCES workflow.occurrence_shipment(id);