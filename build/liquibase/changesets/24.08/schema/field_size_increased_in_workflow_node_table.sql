--liquibase formatted sql

--changeset postgres:field_size_increased_in_workflow_node_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2587  name, description, and help size increased in node table


ALTER TABLE workflow.node ALTER COLUMN name TYPE varchar(150) USING name::varchar(150);
ALTER TABLE workflow.node ALTER COLUMN description TYPE varchar(500) USING description::varchar(500);
ALTER TABLE workflow.node ALTER COLUMN help TYPE varchar(500) USING help::varchar(500);
