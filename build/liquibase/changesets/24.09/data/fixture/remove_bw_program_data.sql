--liquibase formatted sql

--changeset postgres:remove_bw_program_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2647 CS-UM: Remove/void BW program (replaced with BW-CIMMYT) in fixture

do $$
declare
	_bw_contact_id int;
	_bw_institution_id int;
	rec record;
begin
	select id from crm.institution where common_name = 'BW' into _bw_institution_id;
	if (_bw_institution_id is not null) then
		select contact_id from crm.institution where id = _bw_institution_id into _bw_contact_id;
		delete from crm.contact_address where contact_id = _bw_contact_id;
		delete from crm.contact_contact_type where contact_id = _bw_contact_id;
		delete from crm.contact_purpose where contact_id = _bw_contact_id;
		delete from crm.contact_info where contact_id = _bw_contact_id;
		delete from crm.contact_workflow where contact_id = _bw_contact_id;
		delete from core.tenant_contact where contact_id = _bw_contact_id;
		delete from core.printout_template_contact where program_id = _bw_contact_id;
		delete from core.job_log_contact where contact_id = _bw_contact_id;
		delete from crm.institution_crop where institution_id = _bw_institution_id;
		update workflow.service set program_id = null where program_id = _bw_contact_id;
		update workflow.service set serviceprovider_id = null where serviceprovider_id = _bw_contact_id;
		for rec in (select id from crm.contact_hierarchy where contact_id = _bw_contact_id or parent_id = _bw_contact_id) loop
			delete from security.role_contact_hierarchy where contact_hierarchy_id = rec.id;
		end loop;
		delete from crm.contact_hierarchy where contact_id = _bw_contact_id or parent_id = _bw_contact_id;
		delete from crm.institution where id = _bw_institution_id;
		delete from crm.contact where id = _bw_contact_id;
	end if;
end $$;