--liquibase formatted sql

--changeset postgres:add_batch_manager_email_template context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3121 Add Blank Well Batch Manager email template

SET session_replication_role = 'replica';

INSERT INTO core.email_template
("name", subject, "template", creator_id, is_void, id)
select 'Batch Manager - blank well', 'Plate Layout has been changed', '<p>Dear <strong>[ownerName]</strong>.</p><p><br></p><p>Greetings!</p><p><br></p><p>Please note that Plate Layout has been changed, with batch name <strong>[batchCode]</strong> , on <strong>[dateNow] </strong> by <strong>[submitterName] </strong></p><p>with the sample code <strong>[sampleCode] </strong>in the position <strong>[wellPosition] </strong></p><p><br></p><p>Contact <strong><a href="[submitterEmail]" rel="noopener noreferrer" target="_blank">[submitterEmail]</a></strong> case you require further information.</p><p><br></p><p>Thanks and Regards.</p>', 1, false, 43
where not exists
(select id from core.email_template where id = 43);

insert into core.tenant_email_template (tenant_id, email_template_id)
select 1, 43
where not exists 
(select tenant_id from core.tenant_email_template where tenant_id = 1 and email_template_id = 43);

SET session_replication_role = 'origin';

SELECT setval('core.email_template_id_seq', (SELECT MAX(id) FROM "core"."email_template"));