--liquibase formatted sql

--changeset postgres:add_is_traits_actions context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3431 Remove unused actions for CB Tools

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_tm_role_id int;
	_collaborator_role_id int;
	_traits_p_id int;
	_is_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'CB Admin' into _cb_admin_role_id;
	select id from security.role where name = 'Team Member' into _tm_role_id;
	select id from security.role where name = 'Collaborator' into _collaborator_role_id;
	select id from core.product where name = 'Traits' into _traits_p_id;
	select id from core.product where name = 'Inventory search' into _is_p_id;
	-- TRAITS_SEARCH_RECORDS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'TRAITS_SEARCH_RECORDS', true, 'TRAITS_SEARCH_RECORDS', 1, _traits_p_id, false
	where not exists (select id from security.product_function where action = 'TRAITS_SEARCH_RECORDS' and product_id = _traits_p_id);
	select id from security.product_function where action = 'TRAITS_SEARCH_RECORDS' and product_id = _traits_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_cb_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_tm_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_collaborator_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _collaborator_role_id and product_function_id = _pf_id);
	-- SEED_INVENTORY_SEARCH_RECORDS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'SEED_INVENTORY_SEARCH_RECORDS', true, 'SEED_INVENTORY_SEARCH_RECORDS', 1, _is_p_id, false
	where not exists (select id from security.product_function where action = 'SEED_INVENTORY_SEARCH_RECORDS' and product_id = _is_p_id);
	select id from security.product_function where action = 'SEED_INVENTORY_SEARCH_RECORDS' and product_id = _is_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_cb_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _cb_admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_tm_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _tm_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_collaborator_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _collaborator_role_id and product_function_id = _pf_id);
end $$;