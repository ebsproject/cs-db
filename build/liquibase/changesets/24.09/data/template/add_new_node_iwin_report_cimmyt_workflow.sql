--liquibase formatted sql

--changeset postgres:add_new_node_iwin_report_cimmyt_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2454 New from occurrences - As a user, I should be able to create one shipment for the multi occurrences csv report generated.

SET session_replication_role = 'replica';

INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('IWIN occurrences report', 'IWIN occurrences report', 'IWIN occurrences report', 1, false, 1, '2024-09-12 22:36:31.733', '2024-09-13 20:14:08.558', 1, 1, false, 23107, 1, NULL, 496, 3, '{"id": 3, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "rules": {"parameters": [{"columnName": "", "columnValue": ""}], "validationFunction": ""}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "allFlow": true, "allPhase": true, "disabled": false, "template": "sys_occurrence_by_coopkey", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}, "globalAction": true}'::jsonb, '{"size": {"width": 80, "height": 30}, "edges": [], "position": {"x": 475.51989407604833, "y": 0}}'::jsonb, 'Assignment', '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe"}, {"id": "4", "name": "@SubmittedToMe"}, {"id": "3", "name": "@CreateByAnyUser"}, {"id": "2", "name": "@CreatedByUnitMember"}, {"id": "5", "name": "@SubmittedToUnitMember"}], "isSystem": true, "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 3, '4b52672b-00b4-4d95-aa43-563d7ea908bf'::uuid) ON CONFLICT DO NOTHING;

SET session_replication_role = 'origin';
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(10500, 23107) ON CONFLICT DO NOTHING;

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM "workflow"."node"));