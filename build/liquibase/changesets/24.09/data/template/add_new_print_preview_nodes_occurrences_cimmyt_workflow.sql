--liquibase formatted sql

--changeset postgres:add_new_print_preview_nodes_occurrences_cimmyt_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment:BDS-1349 added print preview nodes cimmyt workflow

SET session_replication_role = 'replica';

INSERT INTO workflow.stage
("name", description, help, "sequence", parent_id, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, phase_id, depend_on, icon, wf_view_type_id, design_ref)
VALUES('Print Nodes Cimmyt', 'Print Nodes Cimmyt', 'Print Nodes Cimmyt', 1, NULL, 1, '2024-09-23 21:43:22.415', '2024-09-23 21:55:03.274', 1, 1, false, 11073, 1, 496, '{"size": {"width": 657, "height": 80}, "edges": [], "position": {"x": 79.98094884123384, "y": 719.5196153198228}}'::jsonb, NULL, 1, '90b3c0f8-e034-4201-a205-80e9d8447752'::uuid);


INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send Via Address Shipment Label', 'Send Via Address Shipment Label', 'Send Via Address Shipment Label', 4, false, 1, '2024-09-23 21:44:22.527', '2024-09-23 21:55:03.409', 1, 1, false, 23208, 1, NULL, 496, 3, '{"id": 3, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "rules": {"parameters": [{"columnName": "", "columnValue": ""}], "validationFunction": ""}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "allFlow": true, "allPhase": true, "disabled": false, "template": "sys_send_via_ddress_shipment_label", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"size": {"width": 80, "height": 30}, "edges": [], "position": {"x": 338.0000000000002, "y": 9.5}}'::jsonb, 'PrintRounded', '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe"}, {"id": "4", "name": "@SubmittedToMe"}, {"id": "3", "name": "@CreateByAnyUser"}, {"id": "2", "name": "@CreatedByUnitMember"}, {"id": "5", "name": "@SubmittedToUnitMember"}], "isSystem": true, "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 2, 'b89610bc-6abb-4d99-ac9c-dca82da9e621'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('IWIN Packing List Occurrence', 'IWIN Packing List Occurrence', 'IWIN Packing List Occurrence', 1, false, 1, '2024-09-23 21:44:24.892', '2024-09-23 21:55:03.416', 1, 1, false, 23209, 1, NULL, 496, 3, '{"id": 3, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "rules": {"parameters": [{"columnName": "", "columnValue": ""}], "validationFunction": ""}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "allFlow": true, "allPhase": true, "disabled": false, "template": "sys_iwin_packing_list_occurrence", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"size": {"width": 80, "height": 30}, "edges": [], "position": {"x": 70, "y": 39.5}}'::jsonb, 'PrintRounded', '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe"}, {"id": "4", "name": "@SubmittedToMe"}, {"id": "3", "name": "@CreateByAnyUser"}, {"id": "2", "name": "@CreatedByUnitMember"}, {"id": "5", "name": "@SubmittedToUnitMember"}], "isSystem": true, "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 2, 'cbe98bab-2907-483f-9af0-780725243d0f'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Packaging List Occurrence', 'Packaging List Occurrence', 'Packaging List Occurrence', 2, false, 1, '2024-09-23 21:44:27.509', '2024-09-23 21:55:03.425', 1, 1, false, 23210, 1, NULL, 496, 3, '{"id": 3, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "rules": {"parameters": [{"columnName": "", "columnValue": ""}], "validationFunction": ""}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "allFlow": true, "allPhase": true, "disabled": false, "template": "sys_packaging_list_occurrence", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"size": {"width": 80, "height": 30}, "edges": [], "position": {"x": 203.00000000000023, "y": 4.500000000000028}}'::jsonb, 'PrintRounded', '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe"}, {"id": "4", "name": "@SubmittedToMe"}, {"id": "3", "name": "@CreateByAnyUser"}, {"id": "2", "name": "@CreatedByUnitMember"}, {"id": "5", "name": "@SubmittedToUnitMember"}], "isSystem": true, "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 2, 'b656c75f-eb95-4405-a983-8eb6f2bc4d63'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send Directly Address Label', 'Send Directly Address Label', 'Send Directly Address Label', 3, false, 1, '2024-09-23 21:44:33.346', '2024-09-23 21:55:03.438', 1, 1, false, 23211, 1, NULL, 496, 3, '{"id": 3, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "rules": {"parameters": [{"columnName": "", "columnValue": ""}], "validationFunction": ""}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "allFlow": true, "allPhase": true, "disabled": false, "template": "sys_send_directly_address_label", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"size": {"width": 80, "height": 30}, "edges": [], "position": {"x": 213.00000000000023, "y": 46.5}}'::jsonb, 'PrintRounded', '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe"}, {"id": "4", "name": "@SubmittedToMe"}, {"id": "3", "name": "@CreateByAnyUser"}, {"id": "2", "name": "@CreatedByUnitMember"}, {"id": "5", "name": "@SubmittedToUnitMember"}], "isSystem": true, "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 2, '1be25df7-ea2b-43db-b6f3-8cd4044cb500'::uuid);
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Send Via Package List', 'Send Via Package List', 'Send Via Package List', 5, false, 1, '2024-09-23 21:44:36.350', '2024-09-23 21:55:03.443', 1, 1, false, 23212, 1, NULL, 496, 3, '{"id": 3, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "rules": {"parameters": [{"columnName": "", "columnValue": ""}], "validationFunction": ""}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "allFlow": true, "allPhase": true, "disabled": false, "template": "sys_send_via_package_list", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"size": {"width": 80, "height": 30}, "edges": [], "position": {"x": 335.5000000000002, "y": 44.5}}'::jsonb, 'PrintRounded', '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe"}, {"id": "4", "name": "@SubmittedToMe"}, {"id": "3", "name": "@CreateByAnyUser"}, {"id": "2", "name": "@CreatedByUnitMember"}, {"id": "5", "name": "@SubmittedToUnitMember"}], "isSystem": true, "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 2, '907951fe-3113-406c-9d0c-06ff19eb3af9'::uuid);


SET session_replication_role = 'origin';

INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11073, 23208);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11073, 23209);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11073, 23210);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11073, 23211);
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11073, 23212);

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM "workflow"."node"));
SELECT setval('workflow.stage_id_seq', (SELECT MAX(id) FROM "workflow"."stage"));

