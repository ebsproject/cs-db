--liquibase formatted sql

--changeset postgres:add-scale-values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3374 add scale values

ALTER TABLE core.rule_segment ADD "scale" jsonb NULL;
COMMENT ON COLUMN core.rule_segment."scale" IS 'Defines an array of categorical values valid as input and output';
