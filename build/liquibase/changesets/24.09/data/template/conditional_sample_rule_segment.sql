--liquibase formatted sql

--changeset postgres:conditional-sample-rule-segment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2823 conditional sample rule



/* update skip rules for program*/
update core.sequence_rule_segment set "skip" = '$input.split(",").length > 1'
where sequence_rule_id = 6 /*sample rule*/
  and rule_segment_id = 3; /*program*/

INSERT INTO core.sequence_rule_segment ("position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creator_id,is_void,is_displayed,is_required, "skip",skip_input_rule_segment_id)
select "position",segment_family,(select id from core.rule_segment where name='service-provider-code'),sequence_rule_id,tenant_id,creator_id,is_void,is_displayed,is_required, '$input.split(",").length <= 1',rule_segment_id
  from core.sequence_rule_segment
  where sequence_rule_id = 6 /*sample rule*/
    and rule_segment_id = 3; /*program*/

/* update skip rules for purpose*/
update core.sequence_rule_segment set "skip" = '$input.split(",").length > 1'
where sequence_rule_id = 6 /*sample rule*/
  and rule_segment_id = 6; /*purpose*/
  
INSERT INTO core.sequence_rule_segment ("position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creator_id,is_void,is_displayed,is_required, "skip",skip_input_rule_segment_id)
select "position",segment_family,(select id from core.rule_segment where name='mixed_purpose_code'),sequence_rule_id,tenant_id,creator_id,is_void,is_displayed,is_required, '$input.split(",").length <= 1',rule_segment_id
  from core.sequence_rule_segment
  where sequence_rule_id = 6 /*sample rule*/
    and rule_segment_id = 6; /*purpose*/
