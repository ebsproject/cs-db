--liquibase formatted sql

--changeset postgres:custom_field_update_by_occurrence_cimmyt_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2457 custom field definition updated

UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 2,
  "sort": 1.21,
  "sizes": [
    12,
    12,
    12,
    12,
    12
  ],
  "helper": {
    "title": "Please check a value",
    "placement": "top"
  },
  "checked": false,
  "disabled": false,
  "inputProps": {
    "label": "Check if you want to create a request base on Occurrences"
  },
  "showInGrid": false,
  "disabledAfterCreation": true
}'::jsonb
	WHERE id=1705;
