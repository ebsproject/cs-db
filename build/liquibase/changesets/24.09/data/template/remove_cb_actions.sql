--liquibase formatted sql

--changeset postgres:remove_cb_actions context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3431 Remove unused actions for CB Tools

do $$
declare
	_admin_role_id int;
	_cb_admin_role_id int;
	_pf_id int;
begin
	select id from security.product_function where action = 'VIEW_CROSS_LISTS' and product_id = (select id from core.product where name = 'Cross Manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Cross Manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'VIEW_SEARCH_RESULT' and product_id = (select id from core.product where name = 'Inventory search') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Inventory search') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'VIEW_TRAITS' and product_id = (select id from core.product where name = 'Traits') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Traits') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'List manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'VIEW_EXPERIMENTS' and product_id = (select id from core.product where name = 'Experiment Creation') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Experiment Creation') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'VIEW_OCCURRENCES' and product_id = (select id from core.product where name = 'Experiment Manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Experiment Manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'VIEW_PACKING_JOBS' and product_id = (select id from core.product where name = 'Planting Instruction Manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Planting Instruction Manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'VIEW_HARVEST_DATA' and product_id = (select id from core.product where name = 'Harvest Manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Harvest Manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Germplasm') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Data Collection') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
	select id from security.product_function where action = 'SHOW_MENU' and product_id = (select id from core.product where name = 'Inventory manager') into _pf_id;
	update security.role_product_function set is_void = true where product_function_id = _pf_id;
	update security.product_function set is_void = true where id = _pf_id;
end $$;