--liquibase formatted sql

--changeset postgres:update_create_occurrences_api_call_node context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3318 Added notifications in occurrences nodes

UPDATE workflow.node
	SET define='{
  "id": 10,
  "path": "importFiles/occurrenceShipment",
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The CSV was uploaded successfully"
    }
  },
  "field": "occurrencesCSVFile",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "method": "post",
  "bodyType": "form-data",
  "disabled": false,
  "sgcontext": "cs",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "queryParameters": ""
}'::jsonb
	WHERE id=21889;
