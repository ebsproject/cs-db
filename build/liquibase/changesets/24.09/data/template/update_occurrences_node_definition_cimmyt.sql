--liquibase formatted sql

--changeset postgres:update_occurrences_node_definition_cimmyt context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3318 Added notifications in occurrences nodes

UPDATE workflow.node
	SET define='{
  "id": 6,
  "node": "20666",
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "Please review the documents tab; you can upload documents as needed."
    }
  },
  "action": "",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21895;
UPDATE workflow.node
	SET define='{
  "id": 6,
  "node": "21890",
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "Please review the occurrences tab; you can re-import the CSV file as needed."
    }
  },
  "action": "",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21894;
