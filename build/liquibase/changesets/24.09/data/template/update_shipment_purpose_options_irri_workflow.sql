--liquibase formatted sql

--changeset postgres:update_shipment_purpose_options_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2401 Outgoing Seed shipment IRRI : Improve the separate the Research and Education as separate choices reflected on the shipment purposes catalogue in the basic info Section

UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 5,
  "sort": 4,
  "rules": {
    "required": "This field is required!"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "entity": "Contact",
  "helper": {
    "title": "Shipment Purpose",
    "placement": "top"
  },
  "columns": [
    {
      "accessor": ""
    }
  ],
  "filters": [
    {
      "col": "",
      "mod": "EQ",
      "val": ""
    }
  ],
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Shipment Purpose",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "customFilters": [],
    "parentControl": "control name"
  },
  "defaultOptions": [
    {
      "label": "Breeding",
      "value": 1
    },
    {
      "label": "Testing",
      "value": 2
    },
    {
      "label": "Research",
      "value": 3
    },
    {
      "label": "Education",
      "value": 4
    },
    {
      "label": "Commercial analysis",
      "value": 5
    },
    {
      "label": "Other",
      "value": "6"
    }
  ]
}'::jsonb
	WHERE id=763;
