--liquibase formatted sql

--changeset postgres:update_stage_sequence_print_nodes_cimmyt context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1349 added print preview nodes cimmyt workflow

UPDATE workflow.stage
	SET "sequence"=5
	WHERE id=11073;
