--liquibase formatted sql

--changeset postgres:add_audit_columns_role_product_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2025

ALTER TABLE security.role_product_function ADD id serial4 NOT NULL;
ALTER TABLE security.role_product_function DROP CONSTRAINT "PK_role_product_function";
ALTER TABLE security.role_product_function ADD CONSTRAINT "PK_role_product_function" PRIMARY KEY (id);
ALTER TABLE security.role_product_function ADD creator_id integer;
UPDATE security.role_product_function SET creator_id=1;
ALTER TABLE security.role_product_function ALTER COLUMN creator_id SET NOT NULL;
ALTER TABLE security.role_product_function ADD modifier_id integer NULL;
ALTER TABLE security.role_product_function ADD creation_timestamp timestamp without time zone NOT NULL DEFAULT now();
ALTER TABLE security.role_product_function ADD modification_timestamp timestamp without time zone NULL;
ALTER TABLE security.role_product_function ADD is_void boolean NOT NULL DEFAULT false;
ALTER TABLE security.role_product_function ADD CONSTRAINT role_product_function_unique UNIQUE (role_id,product_function_id);