--liquibase formatted sql

--changeset postgres:add_label_column_printout_template context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2117 able to update the name of the printout template

ALTER TABLE core.printout_template ADD label varchar;