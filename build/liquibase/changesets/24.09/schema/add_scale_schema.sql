--liquibase formatted sql

--changeset postgres:add-scale-schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-3374 add scale schema

ALTER TABLE core.rule_segment ADD IF NOT EXISTS "scale" jsonb NULL;
COMMENT ON COLUMN core.rule_segment."scale" IS 'Defines an array of categorical values valid as input and output';
