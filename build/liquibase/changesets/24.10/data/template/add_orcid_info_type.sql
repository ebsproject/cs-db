--liquibase formatted sql

--changeset postgres:add_orcid_info_type context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1359 Add ORCID as a cs.crm.info_type in the template database

update crm.info_type set name = 'ORCID', description = 'Open Researcher and Contributor ID' where trim(lower(name)) = 'orcid';

insert into crm.info_type (name, description, creator_id)
select 'ORCID', 'Open Researcher and Contributor ID', 1
where not exists (select id from crm.info_type where name = 'ORCID');