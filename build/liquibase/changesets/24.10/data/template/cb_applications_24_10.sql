--liquibase formatted sql

--changeset postgres:cb_applications_24_10 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3574 Update the list of CB applications in CS-DB to match with CB-DB

update core.product set abbreviation = 'LISTS' where name = 'List manager';
update core.product set abbreviation = 'FIND_SEEDS' where name = 'Inventory search';
update core.product set abbreviation = 'QUALITY_CONTROL' where name = 'Data Collection';
update core.product set abbreviation = 'GERMPLASM_CATALOG' where name = 'Germplasm';
update core.product set abbreviation = 'INVENTORY_MANAGER' where name = 'Inventory manager';
insert into core.product (name, abbreviation, description, help, icon, creator_id, htmltag_id, domain_id, menu_order, path)
select 'Seasons', 'MANAGE_SEASONS', 'Seasons', 'Seasons', '', 1, 1, (select id from core.domain where name = 'Core Breeding'), 13, 'seasons'
where not exists (select id from core.product where name = 'Seasons');
update core.product set abbreviation = 'MANAGE_SEASONS' where name = 'Seasons';