--liquibase formatted sql

--changeset postgres:update_custom_field_definition_on_shipment_delivery_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2402  Outgoing seed shipment IRRI: Update the catalogue on the shipment delivery option to reflect menu choice Foreign instead of cross boundary

UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 5,
  "sort": 7.1,
  "rules": {
    "required": "This field is required"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "entity": "Contact",
  "helper": {
    "title": "Shipment Delivery",
    "placement": "top"
  },
  "columns": [
    {
      "accessor": ""
    }
  ],
  "filters": [
    {
      "col": "",
      "mod": "EQ",
      "val": ""
    }
  ],
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Shipment Delivery",
    "variant": "outlined"
  },
  "showInGrid": false,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "customFilters": [],
    "parentControl": "control name"
  },
  "defaultOptions": [
    {
      "label": "Foreign",
      "value": 1
    },
    {
      "label": "Domestic",
      "value": 2
    }
  ]
}'::jsonb
	WHERE id=794;
