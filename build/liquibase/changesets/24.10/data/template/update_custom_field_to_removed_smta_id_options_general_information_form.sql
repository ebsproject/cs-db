--liquibase formatted sql

--changeset postgres:update_custom_field_to_removed_smta_id_options_general_information_form context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3770 Improve to have the MTA options should be in DOCUMENTS tab and only visible to SHU Lab members (service provider)

UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1859;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1861;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1757;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=766;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1858;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1863;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1862;
UPDATE workflow.node_cf
	SET is_void=true
	WHERE id=1860;
UPDATE workflow.cf_value
	SET is_void=true
where nodecf_id in (1859,1861,1757,766,1858,1863,1862,1860);