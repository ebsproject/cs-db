--liquibase formatted sql

--changeset postgres:update_legal_documents_list_in_documents_component_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2480 Out going Seed shipment IRRI: As a user with permissions, I would like to specify the document type to generate for the Legal SMTA document

UPDATE workflow.node
	SET define='{
  "id": 8,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "columns": [
      {
        "name": "",
        "alias": "",
        "hidden": false
      }
    ],
    "legalDocuments": {
      "security": {
        "actors": {
          "shu": true,
          "sender": false,
          "creator": false,
          "recipient": false,
          "requestor": false
        },
        "allowedStatus": [
          {
            "statusId": 365
          },
          {
            "statusId": "370"
          }
        ]
      },
      "documents": [
        {
          "type": "mls",
          "label": "Shrink Wrap",
          "value": "SMTA_MLS_Shrink-Wrap",
          "active": true,
          "template": "sys_legal_smta_mls"
        },
        {
          "type": "mls",
          "label": "Signed",
          "value": "SMTA_MLS_Signed",
          "active": true,
          "template": "sys_standard_material_transfer_agreement_signed_rel1"
        },
        {
          "type": "pud1",
          "label": "Shrink Wrap",
          "value": "SMTA_PUD_Shrink-Wrap",
          "active": true,
          "template": "sys_legal_smta_pud"
        },
        {
          "type": "pud1",
          "label": "Signed",
          "value": "SMTA_PUD_Signed",
          "active": true,
          "template": "sys_standard_material_transfer_agreement_signed_pud1"
        },
        {
          "type": "pud1",
          "label": "HRDC_OMTA",
          "value": "HRDC_OMTA",
          "active": true,
          "template": "sys_hrdc_omta"
        },
        {
          "type": "pud1",
          "label": "JIRCAS_OMTA",
          "value": "JIRCAS_OMTA",
          "active": true,
          "template": "sys_jircas_omta"
        },
        {
          "type": "pud1",
          "label": "NARVI_OMTA",
          "value": "NARVI_OMTA",
          "active": true,
          "template": "sys_narvi_omta"
        },
        {
          "type": "pud1",
          "label": "VRAP_OMTA",
          "value": "VRAP_OMTA",
          "active": true,
          "template": "sys_legal_smta_pud"
        },
        {
          "type": "pud1",
          "label": "IRRI_INGER_OMTA",
          "value": "IRRI_INGER_OMTA",
          "active": true,
          "template": "sys_irri_inger_omta"
        },
        {
          "type": "pud1",
          "label": "ASEAN RiceNet-OMTA",
          "value": "ASEAN_RICENET_OMTA",
          "active": true,
          "template": "sys_asean_omta"
        },
        {
          "type": "pud1",
          "label": "IRRI_OMTA",
          "value": "IRRI_OMTA",
          "active": true,
          "template": "sys_irri_omta"
        },
        {
          "type": "pud2",
          "label": "Shrink Wrap",
          "value": "SMTA_PUD2_Shrink-Wrap",
          "active": true,
          "template": "sys_legal_smta_pud2"
        },
        {
          "type": "pud2",
          "label": "Signed",
          "value": "SMTA_PUD2_Signed",
          "active": true,
          "template": "sys_standard_material_transfer_agreement_signed_pud2"
        },
        {
          "type": "pud2",
          "label": "Biological MTA",
          "value": "BIOLOGICAL_MTA_PUD2",
          "active": true,
          "template": "sys_biological_mta"
        },
        {
          "type": "pud2",
          "label": "Global MTA",
          "value": "GLOBAL_MTA_PUD2",
          "active": true,
          "template": "sys_global_mta_pud2"
        },
        {
          "type": "pud1",
          "label": "Global MTA",
          "value": "GLOBAL_MTA_PUD",
          "active": true,
          "template": "sys_global_mta_pud"
        },
        {
          "type": "pud2",
          "label": "IRRI CMTA",
          "value": "IRRI_CMTA_PUD2",
          "active": true,
          "template": "sys_irri_cmta"
        },
        {
          "type": "pud2",
          "label": "Native Traits CMTA",
          "value": "NATIVE_TRAITS_CMTA_PUD2",
          "active": true,
          "template": "sys_native_traits_cmta"
        },
        {
          "type": "pud2",
          "label": "HRDC CMTA",
          "value": "HRDC_CMTA_PUD2",
          "active": true,
          "template": "sys_hrdc_cmta"
        },
        {
          "type": "pud2",
          "label": "MTA for outgoing transgenic materials",
          "value": "MTA_OUTGOING_TRANSGENIC_PUD2",
          "active": true,
          "template": "sys_mta_for_outgoing_transgenic_materials"
        },
        {
          "type": "pud2",
          "label": "MTA for Golden Rice",
          "value": "MTA_GOLDEN_RICE_PUD2",
          "active": true,
          "template": "sys_mta_golden_rice_lead_event_to_gr_licensees"
        }
      ]
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "component": "UploadDocuments",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb
	WHERE id=21026;
