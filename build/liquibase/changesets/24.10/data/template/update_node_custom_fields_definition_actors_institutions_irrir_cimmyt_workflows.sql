--liquibase formatted sql

--changeset postgres:update_node_custom_fields_definition_actors_institutions_irrir_cimmyt_workflows context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3823 Shipment Outgoing IRRI - Adding Requestor name > missing institution

UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 18,
  "rules": {
    "required": "The recipient institution is required"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "entity": "Contact",
  "helper": {
    "title": "Recipient Institution",
    "placement": "top"
  },
  "columns": [
    {
      "accessor": ""
    }
  ],
  "filters": [],
  "disabled": false,
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Recipient Institution",
    "variant": "outlined"
  },
  "modalPopup": {
    "show": false,
    "componentUI": ""
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "",
    "label": [
      "institution.institution.legalName",
      "institution.institution.commonName"
    ],
    "entity": "Hierarchy",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "institution.institution.legalName"
      },
      {
        "accessor": "institution.institution.commonName"
      }
    ],
    "applyRules": true,
    "columnFilter": "contact.id",
    "customFilters": [
      {
        "col": "institution.category.name",
        "mod": "EQ",
        "val": "Institution"
      }
    ],
    "parentControl": "recipientId"
  }
}'::jsonb,cftype_id=5
  WHERE id=545;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 22,
  "rules": {
    "required": "The recipient institution is required"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "entity": "Contact",
  "helper": {
    "title": "Recipient Institution",
    "placement": "top"
  },
  "columns": [
    {
      "accessor": ""
    }
  ],
  "filters": [],
  "disabled": false,
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Recipient Institution",
    "variant": "outlined"
  },
  "modalPopup": {
    "show": false,
    "componentUI": ""
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "",
    "label": [
      "institution.institution.legalName",
      "institution.institution.commonName"
    ],
    "entity": "Hierarchy",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "institution.institution.legalName"
      },
      {
        "accessor": "institution.institution.commonName"
      }
    ],
    "applyRules": true,
    "columnFilter": "contact.id",
    "customFilters": [
      {
        "col": "institution.category.name",
        "mod": "EQ",
        "val": "Institution"
      }
    ],
    "parentControl": "recipientId"
  }
}'::jsonb,cftype_id=5
  WHERE id=803;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 14,
  "rules": {
    "required": "The requestor institution is required"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "entity": "Contact",
  "helper": {
    "title": "Requestor Institution",
    "placement": "top"
  },
  "columns": [
    {
      "accessor": ""
    }
  ],
  "filters": [],
  "disabled": false,
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Requestor Institution",
    "variant": "outlined"
  },
  "modalPopup": {
    "show": false,
    "componentUI": ""
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "",
    "label": [
      "institution.institution.legalName",
      "institution.institution.commonName"
    ],
    "entity": "Hierarchy",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "institution.institution.legalName"
      },
      {
        "accessor": "institution.institution.commonName"
      }
    ],
    "applyRules": true,
    "columnFilter": "contact.id",
    "customFilters": [
      {
        "col": "institution.category.name",
        "mod": "EQ",
        "val": "Institution"
      }
    ],
    "parentControl": "requestorId"
  }
}'::jsonb,cftype_id=5
  WHERE id=541;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 18,
  "rules": {
    "required": "The requestor institution is required"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "entity": "Contact",
  "helper": {
    "title": "Requestor Institution",
    "placement": "top"
  },
  "columns": [
    {
      "accessor": ""
    }
  ],
  "filters": [],
  "disabled": false,
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Requestor Institution",
    "variant": "outlined"
  },
  "modalPopup": {
    "show": false,
    "componentUI": ""
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "",
    "label": [
      "institution.institution.legalName",
      "institution.institution.commonName"
    ],
    "entity": "Hierarchy",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "institution.institution.legalName"
      },
      {
        "accessor": "institution.institution.commonName"
      }
    ],
    "applyRules": true,
    "columnFilter": "contact.id",
    "customFilters": [
      {
        "col": "institution.category.name",
        "mod": "EQ",
        "val": "Institution"
      }
    ],
    "parentControl": "requestorId"
  }
}'::jsonb,cftype_id=5
  WHERE id=799;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 10,
  "rules": {
    "required": "The sender institution is required"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "entity": "Contact",
  "helper": {
    "title": "Sender Institution",
    "placement": "top"
  },
  "columns": [
    {
      "accessor": ""
    }
  ],
  "filters": [],
  "disabled": false,
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Sender Institution",
    "variant": "outlined"
  },
  "modalPopup": {
    "show": false,
    "componentUI": ""
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "",
    "label": [
      "institution.institution.legalName",
      "institution.institution.commonName"
    ],
    "entity": "Hierarchy",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "institution.institution.legalName"
      },
      {
        "accessor": "institution.institution.commonName"
      }
    ],
    "applyRules": true,
    "columnFilter": "contact.id",
    "customFilters": [
      {
        "col": "institution.category.name",
        "mod": "EQ",
        "val": "Institution"
      }
    ],
    "parentControl": "senderId"
  }
}'::jsonb,cftype_id=5
  WHERE id=537;
UPDATE workflow.node_cf
  SET field_attributes='{
  "id": 5,
  "sort": 13,
  "rules": {
    "required": "The sender institution is required"
  },
  "sizes": [
    12,
    12,
    6,
    2,
    2
  ],
  "entity": "Contact",
  "helper": {
    "title": "Sender Institution",
    "placement": "top"
  },
  "columns": [
    {
      "accessor": ""
    }
  ],
  "filters": [],
  "disabled": false,
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Sender Institution",
    "variant": "outlined"
  },
  "modalPopup": {
    "show": false,
    "componentUI": ""
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "cs",
    "field": "",
    "label": [
      "institution.institution.legalName",
      "institution.institution.commonName"
    ],
    "entity": "Hierarchy",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "institution.institution.legalName"
      },
      {
        "accessor": "institution.institution.commonName"
      }
    ],
    "applyRules": true,
    "columnFilter": "contact.id",
    "customFilters": [
      {
        "col": "institution.category.name",
        "mod": "EQ",
        "val": "Institution"
      }
    ],
    "parentControl": "senderId"
  }
}'::jsonb,cftype_id=5
  WHERE id=793;
 
