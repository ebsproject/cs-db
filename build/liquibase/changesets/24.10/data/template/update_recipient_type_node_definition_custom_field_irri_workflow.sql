--liquibase formatted sql

--changeset postgres:update_recipient_type_node_definition_custom_field_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3552 Replace "Recipient type" list data


UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 5,
  "sort": 23,
  "rules": {
    "required": "This field is required."
  },
  "sizes": [
    12,
    12,
    6,
    6,
    6
  ],
  "entity": "ContactType",
  "helper": {
    "title": "Recipient Type",
    "placement": "top"
  },
  "labels": [
    "name"
  ],
  "columns": [
    {
      "accessor": "label"
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "color": "primary",
    "label": "Recipient Type",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "customFilters": [],
    "parentControl": "control name"
  },
  "defaultOptions": [
    {
      "label": "PRI - Private Companies",
      "value": 1
    },
    {
      "label": "AI - Academic Institutions",
      "value": 2
    },
    {
      "label": "ARI - Advance Research Institutions",
      "value": 3
    },
    {
      "label": "CGCP - CGIAR Challenge Program",
      "value": 4
    },
    {
      "label": "DO - Development Organizations",
      "value": 5
    },
    {
      "label": "FI - Financing Institutions",
      "value": 6
    },
    {
      "label": "FO - Foundations",
      "value": 7
    },
    {
      "label": " GO - Government",
      "value": 8
    },
    {
      "label": " IARC - International Agricultural Research Centers",
      "value": 9
    },
    {
      "label": "IO - International Organizations, Individual, Media",
      "value": 10
    },
    {
      "label": "Individual",
      "value": 11
    },
    {
      "label": "Media",
      "value": 12
    },
    {
      "label": "NARES - National Agriculture Research and Extension Systems",
      "value": 13
    },
    {
      "label": " NGO - Non governmental organizations",
      "value": 14
    },
    {
      "label": "SRO - Subregional organization",
      "value": 15
    },
    {
      "label": "Others",
      "value": 16
    }
  ]
}'::jsonb
	WHERE id=804;
