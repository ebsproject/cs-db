--liquibase formatted sql

--changeset postgres:update_statuses_nodes_custom_security_rules_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3827 Shipment Outgoing IRRI - Clients/creators browser > There should be no update and delete icons once the transaction becomes SUBMITTED

UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Processing Completed."
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "validateItems",
      "onSuccess": ""
    }
  },
  "status": "367",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "Sender",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true
    },
    {
      "actor": "Requestor",
      "allowEdit": false,
      "allowDelete": false
    },
    {
      "actor": "Recipient",
      "allowEdit": false,
      "allowDelete": false
    },
    {
      "actor": "Creator",
      "allowEdit": false,
      "allowDelete": false
    },
    {
      "actor": "SHU",
      "allowView": false,
      "allowViewProgress": false
    }
  ]
}'::jsonb
	WHERE id=21099;
UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Sent."
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "status": "369",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "Sender",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true
    },
    {
      "actor": "Requestor",
      "allowEdit": false,
      "allowDelete": false
    },
    {
      "actor": "Recipient",
      "allowEdit": false,
      "allowDelete": false
    },
    {
      "actor": "Creator",
      "allowEdit": false,
      "allowDelete": false
    },
    {
      "actor": "SHU",
      "allowDelete": false
    }
  ]
}'::jsonb
	WHERE id=21108;
UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Done status."
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "status": "452",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "sender",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "Requestor",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "Recipient",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "SHU",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": true
    },
    {
      "actor": "Creator",
      "allowEdit": false,
      "allowDelete": false,
      "allowAddNotes": false
    }
  ]
}'::jsonb
	WHERE id=21110;
UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The Shipment status has changed to Processing."
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "validateRequiredDocuments",
      "onSuccess": ""
    }
  },
  "status": "370",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "Sender",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true
    }
  ]
}'::jsonb
	WHERE id=21027;
UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Submitted."
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "ERROR",
      "functions": "validateItems",
      "onSuccess": "SUCCESS"
    }
  },
  "status": "365",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "Sender",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true
    },
    {
      "actor": "Requestor",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false
    },
    {
      "actor": "Recipient",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": false,
      "allowViewProgress": false
    },
    {
      "actor": "Creator",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false
    },
    {
      "actor": "SHU",
      "allowEdit": true,
      "allowView": false,
      "allowDelete": true,
      "allowAddNotes": true,
      "allowViewProgress": false
    }
  ]
}'::jsonb
	WHERE id=21070;
