--liquibase formatted sql

--changeset postgres:update_to_enable_other_material_custom_field context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3806 Shipment Outgoing IRRI - Items tab > Download CSV > missing source study column


UPDATE workflow.node_cf
	SET is_void=false
	WHERE id=766;