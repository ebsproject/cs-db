
--liquibase formatted sql

--changeset postgres:add_new_column_unit_in_occurrences_shipment context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-3485 Improve the occurrences GRID by including the unit as kg.

DO $$ 
BEGIN
    BEGIN
        ALTER TABLE workflow.occurrence_shipment ADD COLUMN unit varchar(10) NULL;
    EXCEPTION
        WHEN duplicate_column THEN
            RAISE NOTICE 'Column already exists';
    END;
END $$;