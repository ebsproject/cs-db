
--liquibase formatted sql

--changeset postgres:add_new_field_source_study_in_items_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-3806 Shipment Outgoing IRRI - Items tab > Download CSV > missing source study column

DO $$ 
BEGIN
    BEGIN
        ALTER TABLE workflow.items ADD COLUMN source_study varchar(250) NULL;
    EXCEPTION
        WHEN duplicate_column THEN
            RAISE NOTICE 'Column already exists';
    END;
END $$;