--liquibase formatted sql

--changeset postgres:add_label_column_printout_template_1 context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2117 able to update the name of the printout template

ALTER TABLE core.printout_template ALTER COLUMN default_format TYPE varchar(10) USING default_format::varchar(10);
ALTER TABLE core.printout_template ALTER COLUMN "label" TYPE varchar(50) USING "label"::varchar(50);

UPDATE core.printout_template
	SET "label"= printout_template."name" 
	WHERE "label" is null ;
