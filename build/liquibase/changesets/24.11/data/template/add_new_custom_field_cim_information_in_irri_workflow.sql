--liquibase formatted sql

--changeset postgres:add_new_custom_field_cim_information_in_irri_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-687 Shipment Outgoing IRRI:  I should be able to select Type of courier By Railway when generating a package list to use for dispatch

SET session_replication_role = 'replica';

INSERT INTO workflow.node_cf
("name", description, help, required, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, cftype_id, htmltag_id, node_id, field_attributes, api_attributes_name, attributes_id)
VALUES('CIMInformation', 'CIM Information', 'CIM Information', true, 1, '2024-11-19 18:47:51.785', '2024-11-19 18:55:16.322', 1, 1, false, 1951, 1, 1, 21107, '{"id": 1, "name": "CIMInformation", "sort": 121.1, "rules": {"required": "This field is required"}, "sizes": [12, 12, 6, 6, 6], "helper": {"title": "IM Information", "placement": "top"}, "inputProps": {"rows": 1, "label": "CIM Information", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "typeOfCuries", "sequenceRules": {"ruleName": "", "applyRule": false}, "showControlRules": {"expectedValue": "By Rail", "parentControl": "typeOfCuries"}}, "defaultValue": ""}'::jsonb, '', NULL);
SET session_replication_role = 'origin';

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));