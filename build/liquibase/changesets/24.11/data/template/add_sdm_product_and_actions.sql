--liquibase formatted sql

--changeset postgres:add_sdm_product_and_actions context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4023 Statistical Design Models product and actions

do $$
declare
	_admin_role_id int;
	_ba_admin_role_id int;
	_sdm_p_id int;
	_pf_id int;
	_ba_domain int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'BA Admin' into _ba_admin_role_id;
	select id from core.domain where prefix = 'ba' into _ba_domain;
	insert into core.product
		("name", description, help, icon, creator_id, domain_id, menu_order, htmltag_id, "path", abbreviation)
	select
		'Statistical Design Models', 'Statistical Design Models (SDM) are a catalog of experimental designs that randomize treatments into plots and trials following a specific set of rules', 'Statistical Design Models (SDM) are a catalog of experimental designs that randomize treatments into plots and trials following a specific set of rules', '',  1, _ba_domain, 6, 1, '/sdm', 'SDM'
	where not exists (select id from core.product where name = 'Statistical Design Models');
	select id from core.product where name = 'Statistical Design Models' into _sdm_p_id;
	-- STATISTICAL_DESIGN_MODELS_VALIDATE_UPLOADED_RANDOMIZATION_DESIGNS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Validate uploaded randomization designs', true, 'STATISTICAL_DESIGN_MODELS_VALIDATE_UPLOADED_RANDOMIZATION_DESIGNS', 1, _sdm_p_id, true
	where not exists (select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_VALIDATE_UPLOADED_RANDOMIZATION_DESIGNS' and product_id = _sdm_p_id);
	select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_VALIDATE_UPLOADED_RANDOMIZATION_DESIGNS' and product_id = _sdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- STATISTICAL_DESIGN_MODELS_DOWNLOAD_RAW_AND_VALIDATED_UPLOADED_DESIGN_FILES product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Download raw and validated uploaded design files', true, 'STATISTICAL_DESIGN_MODELS_DOWNLOAD_RAW_AND_VALIDATED_UPLOADED_DESIGN_FILES', 1, _sdm_p_id, false
	where not exists (select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_DOWNLOAD_RAW_AND_VALIDATED_UPLOADED_DESIGN_FILES' and product_id = _sdm_p_id);
	select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_DOWNLOAD_RAW_AND_VALIDATED_UPLOADED_DESIGN_FILES' and product_id = _sdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- STATISTICAL_DESIGN_MODELS_GENERATE_RANDOMIZATION_FOR_UPLOADED_DESIGNS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Generate randomization for uploaded designs for sparse and non-sparse', true, 'STATISTICAL_DESIGN_MODELS_GENERATE_RANDOMIZATION_FOR_UPLOADED_DESIGNS', 1, _sdm_p_id, true
	where not exists (select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_GENERATE_RANDOMIZATION_FOR_UPLOADED_DESIGNS' and product_id = _sdm_p_id);
	select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_GENERATE_RANDOMIZATION_FOR_UPLOADED_DESIGNS' and product_id = _sdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- STATISTICAL_DESIGN_MODELS_RETRIEVE_DESIGN_SCRIPT_INPUTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Retrieve design script inputs', true, 'STATISTICAL_DESIGN_MODELS_RETRIEVE_DESIGN_SCRIPT_INPUTS', 1, _sdm_p_id, true
	where not exists (select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_RETRIEVE_DESIGN_SCRIPT_INPUTS' and product_id = _sdm_p_id);
	select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_RETRIEVE_DESIGN_SCRIPT_INPUTS' and product_id = _sdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- STATISTICAL_DESIGN_MODELS_GENERATE_RANDOMIZATION_USING_EBS_SCRIPTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Generate randomization using EBS scripts for sparse and non-sparse', true, 'STATISTICAL_DESIGN_MODELS_GENERATE_RANDOMIZATION_USING_EBS_SCRIPTS', 1, _sdm_p_id, true
	where not exists (select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_GENERATE_RANDOMIZATION_USING_EBS_SCRIPTS' and product_id = _sdm_p_id);
	select id from security.product_function where action = 'STATISTICAL_DESIGN_MODELS_GENERATE_RANDOMIZATION_USING_EBS_SCRIPTS' and product_id = _sdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
end $$;