--liquibase formatted sql

--changeset postgres:add_smta_rule context:template splitStatements:false rollbackSplitStatements:false
--comment: DBS-2479 Add smta rule

do $$
  DECLARE _rule_id int;
  DECLARE _sequence_id int;
  DECLARE _segment_prefix_id int;
  DECLARE _segment_sequence_id int;
BEGIN
	ALTER TABLE core.segment_sequence DROP COLUMN reset_condition;
	ALTER TABLE core.segment_sequence ADD column reset_condition jsonb NULL;
	COMMENT ON COLUMN core.segment_sequence.reset_condition IS 'logic for restarting a sequence, if any';

	INSERT INTO core.sequence_rule ("name",tenant_id,creator_id,is_void) VALUES ('cs-smta',1,1,false);
	select id from core.sequence_rule where name = 'cs-smta' into _rule_id;

	select max(id)+1 from core.segment_sequence into _sequence_id;
	INSERT INTO core.segment_sequence (id, lowest,"increment","last",tenant_id,creator_id,is_void, reset_condition)
	VALUES (_sequence_id,1,1,0,1,1,false, '{
	    "reset-condition": " $input != new Date().getFullYear()",
	    "reset-value":"2024",
	    "reset-value-function":"new Date().getFullYear()"
	}');
	perform setval('core.segment_sequence_id_seq', _sequence_id);

	INSERT INTO core.rule_segment ("name",requires_input,formula,format,data_type,segment_sequence_id,tenant_id,creator_id,is_void) VALUES 
		('smta-prefix',false,'SMTA',null,'TEXT',null,1,1,false),
		('smta-sequence',false,null,'0000','NUMBER',_sequence_id,1,1,false);

	select id from core.rule_segment where name = 'smta-prefix' into _segment_prefix_id;
	select id from core.rule_segment where name = 'smta-sequence' into _segment_sequence_id;

	INSERT INTO core.sequence_rule_segment ("position", rule_segment_id, sequence_rule_id, segment_family, tenant_id, creation_timestamp, creator_id, is_displayed, is_required) VALUES
		(1, _segment_prefix_id, _rule_id, 0, 1, now(), 1, true, true),
		(2, (select id from core.rule_segment where name = 'current-year'), _rule_id, 0, 1, now(), 1, true, true),
		(3, (select id from core.rule_segment where name = 'dash'), _rule_id, 0, 1, now(), 1, true, true),
		(4, _segment_sequence_id, _rule_id, 0, 1, now(), 1, true, true);
	
end $$;
