--liquibase formatted sql

--changeset postgres:arm_actions_update context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4023 Analysis Request Manager actions

do $$
declare
	_admin_role_id int;
	_ba_admin_role_id int;
	_arm_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'BA Admin' into _ba_admin_role_id;
	select id from core.product where name = 'Analysis Request Manager' into _arm_p_id;
	-- ANALYSIS_REQUEST_MANAGER_GO_TO_ANALYSIS_REQUEST_MANAGER product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Go to Analysis Request Manager', true, 'ANALYSIS_REQUEST_MANAGER_GO_TO_ANALYSIS_REQUEST_MANAGER', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_GO_TO_ANALYSIS_REQUEST_MANAGER' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_GO_TO_ANALYSIS_REQUEST_MANAGER' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_SOA_SUBMIT_SINGLE_OCCURRENCE_ANALYSIS_REQUEST product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'SOA: Submit single-occurrence analysis (SOA) request', true, 'ANALYSIS_REQUEST_MANAGER_SOA_SUBMIT_SINGLE_OCCURRENCE_ANALYSIS_REQUEST', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_SUBMIT_SINGLE_OCCURRENCE_ANALYSIS_REQUEST' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_SUBMIT_SINGLE_OCCURRENCE_ANALYSIS_REQUEST' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_SOA_SUPPRESS_TRAIT_DATA_BY_RESIDUAL product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'SOA: Suppress trait data by residual', true, 'ANALYSIS_REQUEST_MANAGER_SOA_SUPPRESS_TRAIT_DATA_BY_RESIDUAL', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_SUPPRESS_TRAIT_DATA_BY_RESIDUAL' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_SUPPRESS_TRAIT_DATA_BY_RESIDUAL' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_SOA_BULK_SUPPRESS_TRAIT_DATA product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'SOA: Bulk suppress trait data', true, 'ANALYSIS_REQUEST_MANAGER_SOA_BULK_SUPPRESS_TRAIT_DATA', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_BULK_SUPPRESS_TRAIT_DATA' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_BULK_SUPPRESS_TRAIT_DATA' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_SOA_VIEW_RESULTS_AND_DATA_VISUALIZATIONS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'SOA: View results and data visualizations', true, 'ANALYSIS_REQUEST_MANAGER_SOA_VIEW_RESULTS_AND_DATA_VISUALIZATIONS', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_VIEW_RESULTS_AND_DATA_VISUALIZATIONS' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_VIEW_RESULTS_AND_DATA_VISUALIZATIONS' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_SOA_REANALYZE_ANALYSIS_JOB_RESULTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'SOA: Reanalyze analysis job results', true, 'ANALYSIS_REQUEST_MANAGER_SOA_REANALYZE_ANALYSIS_JOB_RESULTS', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_REANALYZE_ANALYSIS_JOB_RESULTS' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_REANALYZE_ANALYSIS_JOB_RESULTS' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_SOA_VOID_ANALYSIS_JOB_RESULTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'SOA: Void analysis job results', true, 'ANALYSIS_REQUEST_MANAGER_SOA_VOID_ANALYSIS_JOB_RESULTS', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_VOID_ANALYSIS_JOB_RESULTS' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_VOID_ANALYSIS_JOB_RESULTS' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_SOA_FINALIZE_ANALYSIS_JOB_RESULTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'SOA: Finalize analysis job results', true, 'ANALYSIS_REQUEST_MANAGER_SOA_FINALIZE_ANALYSIS_JOB_RESULTS', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_FINALIZE_ANALYSIS_JOB_RESULTS' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_FINALIZE_ANALYSIS_JOB_RESULTS' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_SOA_DOWNLOAD_ANALYSIS_RESULTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'SOA: Download analysis results', true, 'ANALYSIS_REQUEST_MANAGER_SOA_DOWNLOAD_ANALYSIS_RESULTS', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_DOWNLOAD_ANALYSIS_RESULTS' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_SOA_DOWNLOAD_ANALYSIS_RESULTS' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_MOA_SUBMIT_MULTI_OCCURRENCE_ANALYSIS_REQUEST product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'MOA: Submit multi-occurrence analysis (MOA) request', true, 'ANALYSIS_REQUEST_MANAGER_MOA_SUBMIT_MULTI_OCCURRENCE_ANALYSIS_REQUEST', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_MOA_SUBMIT_MULTI_OCCURRENCE_ANALYSIS_REQUEST' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_MOA_SUBMIT_MULTI_OCCURRENCE_ANALYSIS_REQUEST' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_MOA_EXTRACT_DATA_FOR_ANALYSIS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'MOA: Extract data for analysis', true, 'ANALYSIS_REQUEST_MANAGER_MOA_EXTRACT_DATA_FOR_ANALYSIS', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_MOA_EXTRACT_DATA_FOR_ANALYSIS' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_MOA_EXTRACT_DATA_FOR_ANALYSIS' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_MOA_VIEW_RESULTS_AND_DATA_VISUALIZATIONS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'MOA: View results and data visualizations', true, 'ANALYSIS_REQUEST_MANAGER_MOA_VIEW_RESULTS_AND_DATA_VISUALIZATIONS', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_MOA_VIEW_RESULTS_AND_DATA_VISUALIZATIONS' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_MOA_VIEW_RESULTS_AND_DATA_VISUALIZATIONS' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- ANALYSIS_REQUEST_MANAGER_MOA_DOWNLOAD_ANALYSIS_RESULTS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'MOA: Download analysis results', true, 'ANALYSIS_REQUEST_MANAGER_MOA_DOWNLOAD_ANALYSIS_RESULTS', 1, _arm_p_id, true
	where not exists (select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_MOA_DOWNLOAD_ANALYSIS_RESULTS' and product_id = _arm_p_id);
	select id from security.product_function where action = 'ANALYSIS_REQUEST_MANAGER_MOA_DOWNLOAD_ANALYSIS_RESULTS' and product_id = _arm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
end $$;