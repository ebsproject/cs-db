--liquibase formatted sql

--changeset postgres:change_ba_domain_name context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4023 Change BA domain name

update core.domain set name = 'Breeding Analytics' where name = 'Analytics';