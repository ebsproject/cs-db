--liquibase formatted sql

--changeset postgres:gsm_product_rename context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2976 Update template database from Genotyping Service Manager to Genotypic Request Manager

update core.product set name = 'Genotypic Request Manager', help = 'Genotypic Request Manager' where name = 'Genotyping Service Manager';