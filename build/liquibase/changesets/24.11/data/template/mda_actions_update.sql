--liquibase formatted sql

--changeset postgres:mda_actions_update context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4023 Molecular Data Analysis actions

do $$
declare
	_admin_role_id int;
	_ba_admin_role_id int;
	_mda_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'BA Admin' into _ba_admin_role_id;
	select id from core.product where name = 'Molecular Data Analysis' into _mda_p_id;
	-- MOLECULAR_DATA_ANALYSIS_GO_TO_MOLECULAR_DATA_ANALYSIS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Go to Molecular Data Analysis', true, 'MOLECULAR_DATA_ANALYSIS_GO_TO_MOLECULAR_DATA_ANALYSIS', 1, _mda_p_id, true
	where not exists (select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_GO_TO_MOLECULAR_DATA_ANALYSIS' and product_id = _mda_p_id);
	select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_GO_TO_MOLECULAR_DATA_ANALYSIS' and product_id = _mda_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- MOLECULAR_DATA_ANALYSIS_VIEW_GENOTYPING_REQUEST product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View genotyping request', true, 'MOLECULAR_DATA_ANALYSIS_VIEW_GENOTYPING_REQUEST', 1, _mda_p_id, true
	where not exists (select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_VIEW_GENOTYPING_REQUEST' and product_id = _mda_p_id);
	select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_VIEW_GENOTYPING_REQUEST' and product_id = _mda_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- MOLECULAR_DATA_ANALYSIS_DOWNLOAD_GENOTYPING_REQUEST_DATA product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Download genotyping request data', true, 'MOLECULAR_DATA_ANALYSIS_DOWNLOAD_GENOTYPING_REQUEST_DATA', 1, _mda_p_id, true
	where not exists (select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_DOWNLOAD_GENOTYPING_REQUEST_DATA' and product_id = _mda_p_id);
	select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_DOWNLOAD_GENOTYPING_REQUEST_DATA' and product_id = _mda_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- MOLECULAR_DATA_ANALYSIS_GO_TO_PARENTAL_PURITY_TESTING product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Go to Parental Purity testing', true, 'MOLECULAR_DATA_ANALYSIS_GO_TO_PARENTAL_PURITY_TESTING', 1, _mda_p_id, true
	where not exists (select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_GO_TO_PARENTAL_PURITY_TESTING' and product_id = _mda_p_id);
	select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_GO_TO_PARENTAL_PURITY_TESTING' and product_id = _mda_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- MOLECULAR_DATA_ANALYSIS_VIEW_PARENTAL_PURITY_RESULTS_AND_STATISTICS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View Parental Purity results and statistics', true, 'MOLECULAR_DATA_ANALYSIS_VIEW_PARENTAL_PURITY_RESULTS_AND_STATISTICS', 1, _mda_p_id, true
	where not exists (select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_VIEW_PARENTAL_PURITY_RESULTS_AND_STATISTICS' and product_id = _mda_p_id);
	select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_VIEW_PARENTAL_PURITY_RESULTS_AND_STATISTICS' and product_id = _mda_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- MOLECULAR_DATA_ANALYSIS_DOWNLOAD_PARENTAL_PURITY_RESULTS_AND_STATISTICS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Download Parental Purity results and statistics', true, 'MOLECULAR_DATA_ANALYSIS_DOWNLOAD_PARENTAL_PURITY_RESULTS_AND_STATISTICS', 1, _mda_p_id, false
	where not exists (select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_DOWNLOAD_PARENTAL_PURITY_RESULTS_AND_STATISTICS' and product_id = _mda_p_id);
	select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_DOWNLOAD_PARENTAL_PURITY_RESULTS_AND_STATISTICS' and product_id = _mda_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- MOLECULAR_DATA_ANALYSIS_GO_TO_F1_HYBRIDITY_TESTING product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Go to F1 Hybridity testing', true, 'MOLECULAR_DATA_ANALYSIS_GO_TO_F1_HYBRIDITY_TESTING', 1, _mda_p_id, true
	where not exists (select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_GO_TO_F1_HYBRIDITY_TESTING' and product_id = _mda_p_id);
	select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_GO_TO_F1_HYBRIDITY_TESTING' and product_id = _mda_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- MOLECULAR_DATA_ANALYSIS_VIEW_F1_HYBRIDITY_RESULTS_AND_STATISTICS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View F1 Hybridity results and statistics', true, 'MOLECULAR_DATA_ANALYSIS_VIEW_F1_HYBRIDITY_RESULTS_AND_STATISTICS', 1, _mda_p_id, true
	where not exists (select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_VIEW_F1_HYBRIDITY_RESULTS_AND_STATISTICS' and product_id = _mda_p_id);
	select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_VIEW_F1_HYBRIDITY_RESULTS_AND_STATISTICS' and product_id = _mda_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- MOLECULAR_DATA_ANALYSIS_DOWNLOAD_F1_HYBRIDITY_RESULTS_AND_STATISTICS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Download F1 Hybridity results and statistics', true, 'MOLECULAR_DATA_ANALYSIS_DOWNLOAD_F1_HYBRIDITY_RESULTS_AND_STATISTICS', 1, _mda_p_id, false
	where not exists (select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_DOWNLOAD_F1_HYBRIDITY_RESULTS_AND_STATISTICS' and product_id = _mda_p_id);
	select id from security.product_function where action = 'MOLECULAR_DATA_ANALYSIS_DOWNLOAD_F1_HYBRIDITY_RESULTS_AND_STATISTICS' and product_id = _mda_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
end $$;