--liquibase formatted sql

--changeset postgres:moved_custom_fields_to_general_information_form context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4220 General information fields

UPDATE workflow.node_cf
	SET field_attributes='{
  "id": 5,
  "sort": 23,
  "rules": {
    "required": "This field is required"
  },
  "sizes": [
    12,
    12,
    12,
    12,
    12
  ],
  "entity": "ContactType",
  "helper": {
    "title": "Recipient Type",
    "placement": "top"
  },
  "labels": [
    "name"
  ],
  "columns": [
    {
      "accessor": "label"
    }
  ],
  "filters": [
    {
      "col": "category.name",
      "mod": "EQ",
      "val": "Person"
    }
  ],
  "apiContent": [
    "id",
    "name"
  ],
  "inputProps": {
    "sx": {
      "width": "50%"
    },
    "color": "primary",
    "label": "Recipient Type",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "person",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "category.name",
    "customFilters": [],
    "parentControl": "control name"
  },
  "defaultOptions": [
    {
      "label": "PRI - Private Companies",
      "value": 1
    },
    {
      "label": "AI - Academic Institutions",
      "value": 2
    },
    {
      "label": "ARI - Advance Research Institutions",
      "value": 3
    },
    {
      "label": "CGCP - CGIAR Challenge Program",
      "value": 4
    },
    {
      "label": "DO - Development Organizations",
      "value": 5
    },
    {
      "label": "FI - Financing Institutions",
      "value": 6
    },
    {
      "label": "FO - Foundations",
      "value": 7
    },
    {
      "label": " GO - Government",
      "value": 8
    },
    {
      "label": " IARC - International Agricultural Research Centers",
      "value": 9
    },
    {
      "label": "IO - International Organizations, Individual, Media",
      "value": 10
    },
    {
      "label": "Individual",
      "value": 11
    },
    {
      "label": "Media",
      "value": 12
    },
    {
      "label": "NARES - National Agriculture Research and Extension Systems",
      "value": 13
    },
    {
      "label": " NGO - Non governmental organizations",
      "value": 14
    },
    {
      "label": "SRO - Subregional organization",
      "value": 15
    },
    {
      "label": "Others",
      "value": 16
    }
  ]
}'::jsonb
	WHERE id=804;
UPDATE workflow.node_cf
	SET node_id=20992, field_attributes='{
  "id": 3,
  "name": "documentSignedDate",
  "sort": 101.5,
  "rules": {
    "required": "No"
  },
  "sizes": [
    6,
    6,
    6,
    6,
    6
  ],
  "helper": {
    "title": "Document Signed Date",
    "placement": "top"
  },
  "inputProps": {
    "sx": {
      "width": "50%"
    },
    "label": "Document Signed Date",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultValue": ""
}'::jsonb
	WHERE id=911;
UPDATE workflow.node_cf
	SET node_id=20992, field_attributes='{
  "id": 3,
  "name": "documentGeneratedDate",
  "sort": 101.3,
  "rules": {
    "required": "No"
  },
  "sizes": [
    6,
    6,
    6,
    6,
    6
  ],
  "helper": {
    "title": "DocumentGenerated Date",
    "placement": "top"
  },
  "inputProps": {
    "sx": {
      "width": "50%"
    },
    "label": "Document Generated Date",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultValue": ""
}'::jsonb
	WHERE id=905;
UPDATE workflow.node_cf
	SET node_id=20992, field_attributes='{
  "id": 1,
  "name": "authorizedSignatory",
  "sort": 101.1,
  "rules": {
    "required": "No"
  },
  "sizes": [
    6,
    6,
    6,
    6,
    6
  ],
  "helper": {
    "title": "IRRI Authorized Signatory",
    "placement": "top"
  },
  "inputProps": {
    "rows": 1,
    "label": "IRRI Authorized Signatory",
    "variant": "outlined",
    "disabled": false,
    "fullWidth": true,
    "multiline": false
  },
  "showInGrid": true,
  "defaultRules": {
    "uri": "",
    "field": "",
    "label": [
      "familyName",
      "givenName"
    ],
    "entity": "Contact",
    "apiContent": [
      {
        "accessor": "id"
      },
      {
        "accessor": "another.field"
      }
    ],
    "applyRules": false,
    "columnFilter": "id",
    "customFilters": [],
    "parentControl": "control name",
    "sequenceRules": {
      "ruleName": "",
      "applyRule": false
    }
  },
  "defaultValue": ""
}'::jsonb
	WHERE id=914;
UPDATE workflow.node_cf
	SET node_id=20992, field_attributes='{
  "id": 3,
  "name": "receivedDate",
  "sort": 101,
  "rules": {
    "required": "No"
  },
  "sizes": [
    6,
    6,
    6,
    6,
    6
  ],
  "helper": {
    "title": "Received Date",
    "placement": "top"
  },
  "inputProps": {
    "sx": {
      "width": "50%"
    },
    "label": "Received Date",
    "variant": "outlined"
  },
  "showInGrid": true,
  "defaultValue": ""
}'::jsonb
	WHERE id=912;

SET session_replication_role = 'replica';
INSERT INTO workflow.node_cf
("name", node_id, field_attributes, is_void, required, description, help, tenant_id, creation_timestamp, id, modification_timestamp, creator_id, modifier_id, cftype_id, htmltag_id, api_attributes_name, attributes_id)
VALUES('recipientSignatory', 20992, '{"id": 1, "name": "recipientSignatory", "sort": 101.4, "rules": {"required": ""}, "sizes": [6, 6, 6, 6, 6], "helper": {"title": "Recipient Signatory", "placement": "top"}, "inputProps": {"rows": 1, "label": "Recipient Signatory", "variant": "outlined", "disabled": false, "fullWidth": true, "multiline": false}, "modalPopup": {"show": false, "componentUI": ""}, "showInGrid": true, "defaultRules": {"uri": "", "field": "", "label": ["familyName", "givenName"], "entity": "Contact", "apiContent": [{"accessor": "id"}, {"accessor": "another.field"}], "applyRules": false, "columnFilter": "id", "customFilters": [], "parentControl": "control name", "sequenceRules": {"ruleName": "", "applyRule": false}}, "defaultValue": ""}'::jsonb, false, false, 'Recipient Signatory', 'Recipient Signatory', 1, '2024-11-15 18:48:38.264', 1900, NULL, 1, NULL, 1, 1, '', NULL);

SET session_replication_role = 'origin';

SELECT setval('workflow.node_cf_id_seq', (SELECT MAX(id) FROM "workflow"."node_cf"));