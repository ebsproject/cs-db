--liquibase formatted sql

--changeset postgres:pdm_actions_update context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4023 Phenotypic Data Manager actions

do $$
declare
	_admin_role_id int;
	_ba_admin_role_id int;
	_pdm_p_id int;
	_pf_id int;
begin
	select id from security.role where name = 'Admin' into _admin_role_id;
	select id from security.role where name = 'BA Admin' into _ba_admin_role_id;
	select id from core.product where name = 'Phenotypic Data Manager' into _pdm_p_id;
	-- PHENOTYPIC_DATA_MANAGER_GO_TO_PHENOTYPIC_DATA_MANAGER product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Go to Phenotypic Data Manager', true, 'PHENOTYPIC_DATA_MANAGER_GO_TO_PHENOTYPIC_DATA_MANAGER', 1, _pdm_p_id, true
	where not exists (select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_GO_TO_PHENOTYPIC_DATA_MANAGER' and product_id = _pdm_p_id);
	select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_GO_TO_PHENOTYPIC_DATA_MANAGER' and product_id = _pdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- PHENOTYPIC_DATA_MANAGER_OPEN_AN_OCCURRENCE_WITH_COLLECTED_TRAIT_DATA product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Open an occurrence with collected trait data', true, 'PHENOTYPIC_DATA_MANAGER_OPEN_AN_OCCURRENCE_WITH_COLLECTED_TRAIT_DATA', 1, _pdm_p_id, true
	where not exists (select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_OPEN_AN_OCCURRENCE_WITH_COLLECTED_TRAIT_DATA' and product_id = _pdm_p_id);
	select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_OPEN_AN_OCCURRENCE_WITH_COLLECTED_TRAIT_DATA' and product_id = _pdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- PHENOTYPIC_DATA_MANAGER_VIEW_TRAIT_DATA_TABLE_AND_STATISTICS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'View trait data table and statistics', true, 'PHENOTYPIC_DATA_MANAGER_VIEW_TRAIT_DATA_TABLE_AND_STATISTICS', 1, _pdm_p_id, true
	where not exists (select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_VIEW_TRAIT_DATA_TABLE_AND_STATISTICS' and product_id = _pdm_p_id);
	select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_VIEW_TRAIT_DATA_TABLE_AND_STATISTICS' and product_id = _pdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- PHENOTYPIC_DATA_MANAGER_GENERATE_DATA_VISUALIZATIONS product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Generate data visualizations', true, 'PHENOTYPIC_DATA_MANAGER_GENERATE_DATA_VISUALIZATIONS', 1, _pdm_p_id, true
	where not exists (select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_GENERATE_DATA_VISUALIZATIONS' and product_id = _pdm_p_id);
	select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_GENERATE_DATA_VISUALIZATIONS' and product_id = _pdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- PHENOTYPIC_DATA_MANAGER_SELECT_TRAIT_DATA_QC_CODE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Select trait data QC code', true, 'PHENOTYPIC_DATA_MANAGER_SELECT_TRAIT_DATA_QC_CODE', 1, _pdm_p_id, true
	where not exists (select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_SELECT_TRAIT_DATA_QC_CODE' and product_id = _pdm_p_id);
	select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_SELECT_TRAIT_DATA_QC_CODE' and product_id = _pdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- PHENOTYPIC_DATA_MANAGER_SUPPRESS_TRAIT_DATA_BY_TRAIT_OR_PLOT product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Suppress trait data by trait or plot', true, 'PHENOTYPIC_DATA_MANAGER_SUPPRESS_TRAIT_DATA_BY_TRAIT_OR_PLOT', 1, _pdm_p_id, true
	where not exists (select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_SUPPRESS_TRAIT_DATA_BY_TRAIT_OR_PLOT' and product_id = _pdm_p_id);
	select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_SUPPRESS_TRAIT_DATA_BY_TRAIT_OR_PLOT' and product_id = _pdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- PHENOTYPIC_DATA_MANAGER_CHANGE_QC_CODE_BY_TRAIT_IN_BULK product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Change QC code by trait in bulk', true, 'PHENOTYPIC_DATA_MANAGER_CHANGE_QC_CODE_BY_TRAIT_IN_BULK', 1, _pdm_p_id, true
	where not exists (select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_CHANGE_QC_CODE_BY_TRAIT_IN_BULK' and product_id = _pdm_p_id);
	select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_CHANGE_QC_CODE_BY_TRAIT_IN_BULK' and product_id = _pdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- PHENOTYPIC_DATA_MANAGER_CLOSE_TRANSACTION product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Close transaction', true, 'PHENOTYPIC_DATA_MANAGER_CLOSE_TRANSACTION', 1, _pdm_p_id, true
	where not exists (select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_CLOSE_TRANSACTION' and product_id = _pdm_p_id);
	select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_CLOSE_TRANSACTION' and product_id = _pdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
	-- PHENOTYPIC_DATA_MANAGER_ANALYZE_TRAIT_DATA_OF_OCCURRENCE product function
	insert into security.product_function
    	(description, system_type, action, creator_id, product_id, is_data_action)
	select
	    'Analyze trait data of occurrence', true, 'PHENOTYPIC_DATA_MANAGER_ANALYZE_TRAIT_DATA_OF_OCCURRENCE', 1, _pdm_p_id, true
	where not exists (select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_ANALYZE_TRAIT_DATA_OF_OCCURRENCE' and product_id = _pdm_p_id);
	select id from security.product_function where action = 'PHENOTYPIC_DATA_MANAGER_ANALYZE_TRAIT_DATA_OF_OCCURRENCE' and product_id = _pdm_p_id into _pf_id;
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _admin_role_id and product_function_id = _pf_id);
	insert into security.role_product_function 
		(role_id, product_function_id, creator_id)
	select 
		_ba_admin_role_id, _pf_id, 1
	where not exists (select role_id from security.role_product_function where role_id = _ba_admin_role_id and product_function_id = _pf_id);
end $$;