--liquibase formatted sql

--changeset postgres:update_list_document_shipment_irri context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-4236 Missing system reports template (sys_proforma_invoice)

UPDATE workflow.node
	SET "name"='Print Proforma Invoice',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_proforma_invoice",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,description='Print Proforma Invoice',help='Print Proforma Invoice'
	WHERE id=21524;

UPDATE workflow.node
	SET "name"='Print Proforma Invoice',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_proforma_invoice",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,description='Print Proforma Invoice',help='Print Proforma Invoice'
	WHERE id=21521;

UPDATE workflow.node
	SET "name"='Print Proforma Invoice',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_proforma_invoice",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,description='Print Proforma Invoice',help='Print Proforma Invoice'
	WHERE id=21519;

UPDATE workflow.node
	SET "name"='Print Proforma Invoice',define='{
  "id": 3,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "rules": {
    "parameters": [
      {
        "columnName": "",
        "columnValue": ""
      }
    ],
    "validationFunction": ""
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "Error",
      "functions": "",
      "onSuccess": ""
    }
  },
  "allFlow": false,
  "allPhase": false,
  "disabled": false,
  "template": "sys_proforma_invoice",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,description='Print Proforma Invoice',help='Print Proforma Invoice'
	WHERE id=21523;
