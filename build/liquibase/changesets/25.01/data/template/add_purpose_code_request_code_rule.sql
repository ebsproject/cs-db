--liquibase formatted sql

--changeset postgres:add_purpose_code_request_code_rule context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3582

INSERT INTO core.sequence_rule_segment ("position",segment_family,rule_segment_id,sequence_rule_id,tenant_id,creation_timestamp,creator_id,is_void,is_displayed,is_required)
select 6,0,6,3,1,'now()',1,false,true,true
where not exists
(select id from core.sequence_rule_segment where rule_segment_id = 6 and sequence_rule_id = 3);