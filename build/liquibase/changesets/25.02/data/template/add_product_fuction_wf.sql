--liquibase formatted sql

--changeset postgres:add_product_fuction_wf context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5046
do $$
declare p_id int;
declare rol_id int;
begin
SELECT id from core.product where name = 'Workflow Management' INTO p_id;
SELECT id from "security"."role" where name = 'Admin' INTO rol_id;
INSERT INTO "security".product_function (description,system_type,"action",creation_timestamp,creator_id,is_void,product_id,is_data_action)
	VALUES ('Import_Export',true,'Import_Export','2024-10-11 17:04:56.622',1,false,p_id,false);

INSERT INTO "security".role_product_function 
(role_id,product_function_id,creator_id,creation_timestamp,is_void)
VALUES(rol_id,(select id from "security".product_function WHERE action = 'Import_Export'),1,'2024-10-11 17:04:56.622',false);
end $$;