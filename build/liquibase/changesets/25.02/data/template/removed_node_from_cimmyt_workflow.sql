--liquibase formatted sql

--changeset postgres:removed_node_from_cimmyt_workflow context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5081 Shipment outgoing IRRI - role not working

UPDATE workflow.node
	SET is_void=true
	WHERE id=21033;
