--liquibase formatted sql

--changeset postgres:revert_gsm_product_name context:template splitStatements:false rollbackSplitStatements:false
--comment: Change Genotypic Request Manager product name to Genotyping Service Manager

update core.product set name = 'Genotyping Service Manager', help = 'Genotyping Service Manager' where name = 'Genotypic Request Manager';