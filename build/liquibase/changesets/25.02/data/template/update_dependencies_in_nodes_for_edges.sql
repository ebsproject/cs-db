--liquibase formatted sql

--changeset postgres:update_dependencies_in_nodes_for_edges context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5040 Create SQL script to update dependencies in the already created workflows

ALTER TABLE workflow.phase DROP CONSTRAINT "UQ_phase_design_ref";
ALTER TABLE workflow.stage DROP CONSTRAINT "UQ_stage_design_ref";
ALTER TABLE workflow.node DROP CONSTRAINT "UQ_node_design_ref";


do
$$
declare
	edges_ids_nodes record;
begin
	for edges_ids_nodes in (select depend_on, id,name,jsonb_array_elements_text(depend_on->'edges') AS edge_id from workflow.node where is_void = false order by id) loop
		if ((select is_void from workflow.node where id = edges_ids_nodes.id)= false and edges_ids_nodes.edge_id ~ '^[-+]?[0-9]+(\.[0-9]+)?$' ) then
			update workflow.node set depend_on = jsonb_set(depend_on, '{edges}',
			COALESCE(
			(select to_jsonb(array_agg(design_ref::text)) from workflow.node where id = cast(edges_ids_nodes.edge_id as int)), '[]'::jsonb
			)
			)
		     where id = edges_ids_nodes.id;
		end if;
	end loop;
end
$$;

do
$$
declare
	edges_ids_phases record;
begin
	for edges_ids_phases in (select depend_on, id,name,jsonb_array_elements_text(depend_on->'edges') AS edge_id from workflow.phase where is_void = false order by id) loop
		if ((select is_void from workflow.phase where id = edges_ids_phases.id)= false and edges_ids_phases.edge_id ~ '^[-+]?[0-9]+(\.[0-9]+)?$'  ) then
			update workflow.phase set depend_on = jsonb_set(depend_on, '{edges}',
			COALESCE(
			(select to_jsonb(array_agg(design_ref::text)) from workflow.phase where id = cast(edges_ids_phases.edge_id as int)), '[]'::jsonb
			)
			)
		     where id = edges_ids_phases.id;
				
		end if;
	end loop;
end
$$;

do
$$
declare
	edges_ids_stages record;
begin
	for edges_ids_stages in (select depend_on, id,name,jsonb_array_elements_text(depend_on->'edges') AS edge_id from workflow.stage where is_void = false order by id) loop
		if ((select is_void from workflow.stage where id = edges_ids_stages.id)= false and edges_ids_phases.edge_id ~ '^[-+]?[0-9]+(\.[0-9]+)?$' ) then
			update workflow.stage set depend_on = jsonb_set(depend_on, '{edges}',
			COALESCE(
			(select to_jsonb(array_agg(design_ref::text)) from workflow.stage where id = cast(edges_ids_stages.edge_id as int)), '[]'::jsonb
			)
			)
		     where id = edges_ids_stages.id;
				
		end if;
	end loop;
end
$$;