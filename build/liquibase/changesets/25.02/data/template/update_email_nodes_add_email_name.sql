--liquibase formatted sql

--changeset postgres:update_email_nodes_add_email_name context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5040 Create SQL script to update dependencies in the already created workflows


do
$$
declare
	email_nodes record;
begin
	for email_nodes in (select id,name
	, (define->>'email'):: text AS template_id 
	from workflow.node where is_void = false and process_id = 4 order by id) loop
		if ((select is_void from workflow.node where id = email_nodes.id)= false  ) then
			update workflow.node n set define = jsonb_set(n.define, '{emailName}',
			COALESCE(
		    to_jsonb((
        SELECT email_template.name
        FROM core.email_template
        WHERE id = cast(email_nodes.template_id as int)
    )), '""'::jsonb
    )
    ) 
		     where id = email_nodes.id;
		end if;
	end loop;
end
$$;