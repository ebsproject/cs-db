--liquibase formatted sql

--changeset postgres:update_move_node_add_node_name context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5040 Create SQL script to update dependencies in the already created workflows

do
$$
declare
	move_nodes record;
begin
	for move_nodes in (select id,name
	, (define->>'node'):: text AS node_id 
	from workflow.node where is_void = false and process_id = 6 order by id) loop
		if ((select is_void from workflow.node where id = move_nodes.id) = false and move_nodes.node_id ~ '^[-+]?[0-9]+(\.[0-9]+)?$' ) then
			update workflow.node n set define = jsonb_set(n.define, '{nodeName}',
			COALESCE(
		    to_jsonb((
        SELECT node.name 
        FROM workflow.node  
        WHERE id = cast(move_nodes.node_id as int)
    )), '""'::jsonb
    )
    ) 
		     where id = move_nodes.id;
		end if;
	end loop;
end
$$;