
--liquibase formatted sql

--changeset postgres:update_node_conditional_add_node_target_name context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5040 Create SQL script to update dependencies in the already created workflows

--executeNodeIfTrueName
--executeNodeIfFalseName

do
$$
declare
	execute_nodes record;
begin
	for execute_nodes in (select id,name
	, (define->>'executeNodeIfTrue'):: text AS node_id 
	from workflow.node where is_void = false and process_id = 11 order by id) loop
		if ((select is_void from workflow.node where id = execute_nodes.id)= false and execute_nodes.node_id ~ '^[-+]?[0-9]+(\.[0-9]+)?$' ) then
			update workflow.node n set define = jsonb_set(n.define, '{executeNodeIfTrueName}',
			COALESCE(
		    to_jsonb((
        SELECT node.name
        FROM workflow.node
        WHERE id = cast(execute_nodes.node_id as int)
    )), '""'::jsonb
    )
    ) 
		     where id = execute_nodes.id;
		end if;
	end loop;
end
$$;

do
$$
declare
	execute_nodes record;
begin
	for execute_nodes in (select id,name
	, (define->>'executeNodeIfFalse'):: text AS node_id 
	from workflow.node where is_void = false and process_id = 11 order by id) loop
		if ((select is_void from workflow.node where id = execute_nodes.id)= false and execute_nodes.node_id ~ '^[-+]?[0-9]+(\.[0-9]+)?$' ) then
			update workflow.node n set define = jsonb_set(n.define, '{executeNodeIfFalseName}',
			COALESCE(
		    to_jsonb((
        SELECT node.name
        FROM workflow.node
        WHERE id = cast(execute_nodes.node_id as int)
    )), '""'::jsonb
    )
    ) 
		     where id = execute_nodes.id;
		end if;
	end loop;
end
$$;