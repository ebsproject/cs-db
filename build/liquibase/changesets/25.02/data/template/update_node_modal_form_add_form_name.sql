--liquibase formatted sql

--changeset postgres:update_node_modal_form_add_form_name context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5040 Create SQL script to update dependencies in the already created workflows


do
$$
declare
	modal_nodes record;
begin
	for modal_nodes in (select id,name
	, (define->>'form'):: text AS node_id 
	from workflow.node where is_void = false and process_id = 9 order by id) loop
		if ((select is_void from workflow.node where id = modal_nodes.id)= false and modal_nodes.node_id ~ '^[-+]?[0-9]+(\.[0-9]+)?$' ) then
			update workflow.node n set define = jsonb_set(n.define, '{formName}',
			COALESCE(
		    to_jsonb((
        SELECT node.name
        FROM workflow.node
        WHERE id = cast(modal_nodes.node_id as int)
    )), '""'::jsonb
    )
    ) 
		     where id = modal_nodes.id;
		end if;
	end loop;
end
$$;