--liquibase formatted sql

--changeset postgres:update_node_on_hold_status_irri_workflow_01 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5081 Shipment outgoing IRRI - role not working

UPDATE workflow.node
	SET help='BACK TO SUBMIT',define='{
  "id": 6,
  "node": "21070",
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request is on Hold status"
    }
  },
  "action": "",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "nodeName": "SUBMIT",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,"name"='BACK TO SUBMIT',description='BACK TO SUBMIT'
	WHERE id=21106;
