--liquibase formatted sql

--changeset postgres:update_nodes_in_sent_stage_irri_workflow_01 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5081 Shipment outgoing IRRI - role not working


UPDATE workflow.node
	SET is_void=true
WHERE id=21108;

DELETE FROM workflow.node_stage
	WHERE stage_id=10735 AND node_id=21505;
DELETE FROM workflow.node_stage
	WHERE stage_id=10735 AND node_id=21640;
DELETE FROM workflow.node_stage
	WHERE stage_id=10735 AND node_id=21108;

SET session_replication_role = 'replica';

INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Sent', 'Sent', 'Sent', 3, false, 1, '2025-02-24 15:56:01.119', '2025-02-24 16:07:50.479', 1, 1, false, 23313, 1, NULL, 562, 5, '{"id": 5, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "status": "369", "disabled": false, "inputProps": {"sourceNodes": []}, "statusName": "Sent", "outputProps": {"targetNodes": []}, "customSecurityRules": [{"actor": "SHU", "allowEdit": true, "allowView": true, "allowDelete": true, "allowAddNotes": true, "allowViewProgress": true},{"actor": "Creator", "allowEdit": true, "allowView": true,"allowDelete": true}]}'::jsonb, '{"size": {"width": 80, "height": 30}, "edges": [], "position": {"x": 254.50000000000023, "y": 21}}'::jsonb, 'Forward', '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe"}, {"id": "4", "name": "@SubmittedToMe"}, {"id": "3", "name": "@CreateByAnyUser"}, {"id": "2", "name": "@CreatedByUnitMember"}, {"id": "5", "name": "@SubmittedToUnitMember"}], "isSystem": true, "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 2, 'eaa3df4f-531d-445e-b8ad-c77d3facc22f'::uuid) on conflict do nothing;
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Move to done', 'Move to done', 'Move to done', 1, false, 1, '2025-02-24 16:04:32.507', '2025-02-24 16:07:50.481', 1, 1, false, 23314, 1, NULL, 562, 6, '{"id": 6, "node": "21110", "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "action": "", "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "disabled": false, "nodeName": "Done", "inputProps": {"sourceNodes": []}, "outputProps": {"targetNodes": []}}'::jsonb, '{"size": {"width": 80, "height": 30}, "edges": ["eaa3df4f-531d-445e-b8ad-c77d3facc22f"], "position": {"x": 492, "y": 73.5}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe"}, {"id": "4", "name": "@SubmittedToMe"}, {"id": "3", "name": "@CreateByAnyUser"}, {"id": "2", "name": "@CreatedByUnitMember"}, {"id": "5", "name": "@SubmittedToUnitMember"}], "isSystem": true, "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, '80952c63-1057-4620-972d-5d9768c32485'::uuid) on conflict do nothing;
INSERT INTO workflow.node
("name", description, help, "sequence", require_approval, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, product_id, workflow_id, process_id, define, depend_on, icon, message, require_input, security_definition, validation_code, validation_type, wf_view_type_id, node_type_id, design_ref)
VALUES('Set status sent', 'Set status sent', 'Set status sent', 5, false, 1, '2025-02-24 16:05:33.887', '2025-02-24 16:07:50.485', 1, 1, false, 23315, 1, NULL, 562, 5, '{"id": 5, "after": {"executeNode": "", "sendNotification": {"send": false, "message": ""}}, "before": {"validate": {"code": "", "type": "javascript", "valid": false, "onError": "", "functions": "", "onSuccess": ""}}, "status": "369", "disabled": false, "inputProps": {"sourceNodes": []}, "statusName": "Sent", "outputProps": {"targetNodes": []}, "customSecurityRules": [{"actor": "SHU", "allowEdit": true, "allowView": true, "allowDelete": true, "allowAddNotes": true, "allowViewProgress": true}]}'::jsonb, '{"size": {"width": 80, "height": 30}, "edges": ["eaa3df4f-531d-445e-b8ad-c77d3facc22f"], "position": {"x": 495.5, "y": 108}}'::jsonb, NULL, '{}'::jsonb, '{}'::jsonb, '{"roles": [{"id": "2", "name": "Admin", "rules": [{"id": "1", "name": "@CreatedByMe"}, {"id": "4", "name": "@SubmittedToMe"}, {"id": "3", "name": "@CreateByAnyUser"}, {"id": "2", "name": "@CreatedByUnitMember"}, {"id": "5", "name": "@SubmittedToUnitMember"}], "isSystem": true, "description": "admin"}], "users": null, "programs": null}'::jsonb, NULL, 'javascript', 1, 4, '934b3cba-41c8-4baf-96d3-4d80c3258520'::uuid) on conflict do nothing;


INSERT INTO workflow.stage
("name", description, help, "sequence", parent_id, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, htmltag_id, phase_id, depend_on, icon, wf_view_type_id, design_ref)
VALUES('Move Done Stage', 'Move Done Stage', 'Move Done Stage', 5, NULL, 1, '2025-02-24 16:31:27.972', '2025-02-24 16:37:46.279', 1, 1, false, 11174, 1, 563, '{"size": {"width": 711, "height": 136}, "edges": [], "width": 711, "height": 136, "position": {"x": 52.06792707090506, "y": 568.6475707754795}}'::jsonb, 'CheckCircle', 1, '86fdd229-2223-499a-8f77-1d9109c5b46d'::uuid) on conflict do nothing;

SET session_replication_role = 'origin';

INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11174, 23313) on conflict do nothing;
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11174, 23314) on conflict do nothing;
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11174, 23315) on conflict do nothing;
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11174, 21640) on conflict do nothing;
INSERT INTO workflow.node_stage
(stage_id, node_id)
VALUES(11174, 21505) on conflict do nothing;

SELECT setval('workflow.node_id_seq', (SELECT MAX(id) FROM "workflow"."node"));
SELECT setval('workflow.stage_id_seq', (SELECT MAX(id) FROM "workflow"."stage"));

-- add depend on new edge
UPDATE workflow.node
	SET depend_on='{
  "size": {
    "width": 80,
    "height": 30
  },
  "edges": [
    "eaa3df4f-531d-445e-b8ad-c77d3facc22f"
  ],
  "position": {
    "x": 489.92869755609036,
    "y": 1.9190533625203443
  }
}'::jsonb
	WHERE id=21505;

UPDATE workflow.node
	SET depend_on='{
  "size": {
    "width": 80,
    "height": 30
  },
  "edges": [
    "eaa3df4f-531d-445e-b8ad-c77d3facc22f"
  ],
  "position": {
    "x": 490.3291482697655,
    "y": 35.69141458231229
  }
}'::jsonb
	WHERE id=21640;

UPDATE workflow.node
	SET help='Move To Sent',"name"='Move To Sent',define='{
  "id": 6,
  "node": "23313",
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": false,
      "message": ""
    }
  },
  "action": "",
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "",
      "onSuccess": ""
    }
  },
  "disabled": false,
  "nodeName": "Sent",
  "inputProps": {
    "sourceNodes": []
  },
  "outputProps": {
    "targetNodes": []
  }
}'::jsonb,description='Move To Sent'
	WHERE id=21109;
