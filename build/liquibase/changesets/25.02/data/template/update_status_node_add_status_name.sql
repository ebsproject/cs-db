--liquibase formatted sql

--changeset postgres:update_status_node_add_status_name context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5040 Create SQL script to update dependencies in the already created workflows


do
$$
declare
	define_nodes record;
begin
	for define_nodes in (select id,name
	, (define->>'status'):: text AS status_id 
	from workflow.node where is_void = false and process_id = 5 order by id) loop
		if ((select is_void from workflow.node where id = define_nodes.id)= false ) then
			update workflow.node n set define = jsonb_set(n.define, '{statusName}',
			COALESCE(
		    to_jsonb((
        SELECT status_type.name 
        FROM workflow.status_type  
        WHERE id = cast(define_nodes.status_id as int)
    )), '""'::jsonb
    )
    ) 
		     where id = define_nodes.id;
		end if;
	end loop;
end
$$;
