--liquibase formatted sql

--changeset postgres:update_custom_security_rules_in_processing_completed_status context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-5081 Shipment outgoing IRRI - role not working


-- Auto-generated SQL script #202503031115
UPDATE workflow.node
	SET define='{
  "id": 5,
  "after": {
    "executeNode": "",
    "sendNotification": {
      "send": true,
      "message": "The request has been changed to Processing Completed"
    }
  },
  "before": {
    "validate": {
      "code": "",
      "type": "javascript",
      "valid": false,
      "onError": "",
      "functions": "validateItems",
      "onSuccess": ""
    }
  },
  "status": "367",
  "disabled": false,
  "inputProps": {
    "sourceNodes": []
  },
  "statusName": "Processing Completed",
  "outputProps": {
    "targetNodes": []
  },
  "customSecurityRules": [
    {
      "actor": "Sender",
      "allowEdit": false,
      "allowView": true,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true
    },
    {
      "actor": "Requestor",
      "allowEdit": false,
      "allowDelete": false
    },
    {
      "actor": "Recipient",
      "allowEdit": false,
      "allowDelete": false
    },
    {
      "actor": "Creator",
      "allowEdit": false,
      "allowDelete": false,
      "allowAddNotes": true,
      "allowViewProgress": true,
      "allowView": true
    },
    {
      "actor": "SHU",
      "allowEdit": true,
      "allowView": true,
      "allowDelete": true,
      "allowAddNotes": true,
      "allowViewProgress": true
    }
  ]
}'::jsonb
	WHERE id=21099;
