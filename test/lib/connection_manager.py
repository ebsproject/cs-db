import psycopg2 as pg
import sys

class ConnectionManager:

    def __init__(self):
        pass

    def connect(self, **kwargs):
        try:
            return pg.connect(host=kwargs['host'], 
                    port=kwargs['port'], 
                    database=kwargs['database'], 
                    user=kwargs['user'], 
                    password=kwargs['password'])

        except (Exception, pg.Error) as error:
            print ('!ERROR:', error)
            exit()