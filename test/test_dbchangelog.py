import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_client_from_file(path):
    list_client = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        client = row.strip()
        list_client.append(client)

    return np.sort(np.array(list_client))

def get_list_client_from_db(**kwargs):
    cur = kwargs['cursor']
    query = "SELECT id FROM databasechangelog ORDER BY orderexecuted;"
    cur.execute(query)
    records = cur.fetchall()
    result = [client[0] for client in records]

    return np.sort(np.array(result))

path = 'reference/expected_dbchangelog.txt'
expected_values = get_list_client_from_file(path=path)
result_values = get_list_client_from_db(cursor=get_cursor(), variables=expected_values)
missing_values = np.setdiff1d(expected_values, result_values)


@pytest.mark.parametrize("a, b, c",[(expected_values,result_values,missing_values)])
def test_complete_template_dbchangelog(a,b,c):
    assert np.array_equal(a,b), "There are missing dbchangelog IDs "+str(c)
